import { languageTranslate } from "service/actionType";

export const arabTranslate = (data) => (dispatch, getState) => {
	return new Promise((resolve, reject) => {
		dispatch({
			type: languageTranslate.languageArab,
			payload: data,
		});
	});
};

export const layoutHandler = (data) => (dispatch, getState) => {
	return new Promise((resolve, reject) => {
		dispatch({
			type: languageTranslate.layoutPosition,
			payload: data,
		});
	});
};

export const requestedServiceSelector = (data) => (dispatch, getState) => {
	return new Promise((resolve, reject) => {
		dispatch({
			type: languageTranslate.requestedService,
			payload: data,
		});
	});
};

export const updateLanguage = (data) => (dispatch, getState) => {
  return new Promise((resolve, reject) => {
    localStorage.setItem("language", data);
    dispatch({
      type: languageTranslate.language,
      payload: data,
    });
  });
};

export const updateLayout = (data) => (dispatch, getState) => {
	return new Promise((resolve, reject) => {
	  dispatch({
		type: languageTranslate.updateLayout,
		payload: data,
	  });
	});
  };
