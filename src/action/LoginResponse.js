import { loginResponse } from "service/actionType";

export const setLoginResponse = (data) => {
    return {
        type: loginResponse,
        payload: data
    }
}