import {
  policyInfo,
  medicalPolicyDetails,
  motorPolicyDetails,
  travelPolicyDetails,
  serviceDetails,
  medApprovalDetails,
  renewalPolicyDetail,
  accidentDetail,
	newsUpdatesDetails,
} from "service/actionType";

export const setPolicyInformation = (data) => {
	return {
		type: policyInfo,
		payload: data,
	};
};

export const setMedicalPolicyDetails = (data) => {
	return {
		type: medicalPolicyDetails,
		payload: data,
	};
};

export const setMotorPolicyDetails = (data) => {
	return {
		type: motorPolicyDetails,
		payload: data,
	};
};

export const setTravelPolicyDetails = (data) => {
	return {
		type: travelPolicyDetails,
		payload: data,
	};
};

export const setServiceDetails = (data) => {
	return {
		type: serviceDetails,
		payload: data
	}
}

export const setMedApprovalDetails = (data) => {
	return {
		type: medApprovalDetails,
		payload: data
	}
}

export const setRenewalPolicyDetail = (data) => {
  return {
    type: renewalPolicyDetail,
    payload: data,
  };
};

export const setAccidentDetail = (data) => {
  return {
    type: accidentDetail,
    payload: data,
  };
};

export const setNewsUpdatesDetails = (data) => {
	return {
		type: newsUpdatesDetails,
		payload: data,
	}
}