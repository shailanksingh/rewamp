import React, {useEffect, useState} from "react";

/**
 * Hook that alerts clicks outside the passed ref
 */
function DetectOutsideClicks(ref) {
  const [isClickOutside, setIsClickOutside] = useState(null)
  useEffect(() => {
    /**
     * Alert if clicked on outside of element
     */
    function handleClickOutside(event) {
      if (ref.current && !ref.current.contains(event.target)) {
        setIsClickOutside(true)
      } else {
        setIsClickOutside(false)
      }
    }

    // Bind the event listener
    document.addEventListener("mousedown", handleClickOutside);
    return () => {
      // Unbind the event listener on clean up
      document.removeEventListener("mousedown", handleClickOutside);
    };
  }, [ref]);
  return isClickOutside
}

export default DetectOutsideClicks;
