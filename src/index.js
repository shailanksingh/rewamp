import React from "react";
import {createRoot} from 'react-dom/client';
import Routes from "./routes";
import * as serviceWorker from "./serviceWorker";
import {Provider} from "react-redux";
import {store} from "service/helpers";
import "./assets/scss/index.scss";
import "react-notifications/lib/notifications.css";
import "react-datepicker/dist/react-datepicker.css";
import "bootstrap/dist/css/bootstrap.css";
import "./assets/fonts/GE_SS/GE_SS_light.otf";
import './i18n';

createRoot(
  document.getElementById("root"),
)
  .render(
    <Provider store={store}>
      <Routes/>
    </Provider>,
  );

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
