import { combineReducers } from "redux";
import languageReducer from "./languageReducer";
import dashboardInformationReducer from "./dashboardInformationReducer";
import loginDetailsReducer from "./loginDetailsReducer";

export const reducers = combineReducers({
	languageReducer,
	dashboardInformationReducer,
	loginDetailsReducer,
});
