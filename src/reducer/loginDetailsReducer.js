import { loginResponse } from "service/actionType";

const initialState = {
	loginResponse: {},
};

const loginDetailsReducer = (state = initialState, action) => {
	switch (action.type) {
		case loginResponse:
			return {
				...state,
				loginResponse: action.payload,
			};
		default:
			return state;
	}
};

export default loginDetailsReducer;
