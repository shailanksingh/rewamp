import {
	policyInfo,
	medicalPolicyDetails,
	motorPolicyDetails,
	travelPolicyDetails,
	serviceDetails,
	medApprovalDetails,
	renewalPolicyDetail,
	accidentDetail,
	newsUpdatesDetails,
} from "service/actionType";

const initialState = {
	policyInformation: {},
	medicalPolicyDetails: [],
	motorPolicyDetails: [],
	travelPolicyDetails: [],
	serviceDetails: {},
	accidentDetail: [],
	renewalPolicyDetail: {},
	medApprovalDetails: [],
	newsUpdates: [],
};

const dashboardInformationReducer = (state = initialState, action) => {
	switch (action.type) {
		case policyInfo:
			return {
				...state,
				policyInformation: action.payload,
			};

		case medicalPolicyDetails:
			return {
				...state,
				medicalPolicyDetails: action.payload,
			};
		case motorPolicyDetails:
			return {
				...state,
				motorPolicyDetails: [...state.motorPolicyDetails, action.payload],
			};
		case travelPolicyDetails:
			return {
				...state,
				travelPolicyDetails: [...state.travelPolicyDetails, action.payload],
			};
		case serviceDetails:
			return {
				...state,
				serviceDetails: action.payload,
			};
		case medApprovalDetails:
			return {
				...state,
				medApprovalDetails: action.payload,
			};
		case accidentDetail:
			return {
				...state,
				accidentDetail: [...state.accidentDetail, action.payload],
			};
		case renewalPolicyDetail:
			return {
				...state,
				renewalPolicyDetail: action.payload,
			};
		case newsUpdatesDetails:
			return {
				...state,
				newsUpdates: action.payload,
			}
		default:
			return state;
	}
};

export default dashboardInformationReducer;
