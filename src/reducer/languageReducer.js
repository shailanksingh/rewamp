import { languageTranslate } from "service/actionType";

const initialState = {
  languageArab: false,
  layoutPosition: {},
  language: null,
};
const languageReducer = (state = initialState, action) => {
  switch (action.type) {
    case languageTranslate.languageArab:
      return {
        ...state,
        languageArab: action.payload,
      };
    case languageTranslate.layoutPosition:
      return {
        ...state,
        layoutPosition: action.payload,
      };
    case languageTranslate.requestedService:
      return {
        ...state,
        requestedService: action.payload,
      };
    case languageTranslate.language:
      return {
        ...state,
        language: action.payload,
      };
    case languageTranslate.updateLayout:
      return {
        ...state,
        language: action.payload,
      };
    default:
      return state;
  }
};

export default languageReducer;
