import React, { useEffect, useState } from "react";
import { history } from "service/helpers";
import { InsuranceNavBar } from "component/common/InsuranceNavBar";
import { NormalOffCanvas } from "component/common/NormalOffCanvas";
import { ToggleBanner } from "component/common/ToggleBanner";
import {
  EmergencyCard,
  LanguageCard,
  SupportCard,
  GetStartedCard,
} from "component/HomePage/LandingComponent/NavbarCards";
import { CommonFooter } from "component/common/CommonFooter";
import { createTheme, ThemeProvider } from "@material-ui/core";
import { CommonBanner } from "component/common/CommonBanner";
import { MoreProductCard } from "../component/HomePage/LandingComponent/NavbarCards/MoreProductCard";
import { CommonBreadCrumb } from "component/common/CommonBreadCrumb";
import { Navbar } from "component/common/Navbar";
const theme = createTheme({
  palette: {
    common: {},
    primary: {
      main: "#EE7500",
    },
    secondary: {
      main: "#FFFFFF",
    },
    // redColor: palette.augmentColor({ color: red }),
  },
});

export const InsuranceLayout = (props) => {
  const [toggle, setToggle] = useState(false);

  const [languageCard, setLanguageCard] = useState(false);

  const [supportCard, setSupportCard] = useState(false);

  const [emergencyCard, setEmergencyCard] = useState(false);

  const [getStartedCard, setGetStartedCard] = useState(false);

  const [productToggle, setProductToggle] = useState(false);

  const handleToggler = () => {
    setToggle(true);
  };

  const closeToggler = () => {
    setToggle(false);
  };

  const toggleLanguageCard = () => {
    setLanguageCard(!languageCard);
  };

  const toggleSupportCard = () => {
    setSupportCard(!supportCard);
  };

  const toggleEmergencyCard = () => {
    setEmergencyCard(!emergencyCard);
  };

  const toggleGetStartedCard = () => {
    setGetStartedCard(!getStartedCard);
  };

  const toggleProductCard = () => {
    setProductToggle(true);
  };

  const navHomeContent = [
    {
      id: 0,
      navText: "Individuals",
    },
    {
      id: 1,
      navText: "Corporate",
    },
    {
      id: 2,
      navText: "Investor",
    },
    {
      id: 3,
      navText: "Claims",
    },
  ];

  const historyLinks = [
    {
      id: 0,
      routeLink: "/products/motor",
    },
    {
      id: 1,
      routeLink: "/products/medicalmalpracticepage",
    },
    {
      id: 2,
      routeLink: "/products/individuals/homeinsurance",
    },
  ];

  useEffect(() => {
    window.scrollTo(0, 0);
  }, [props.location]);

  return (
    <>
      <ThemeProvider theme={theme}>
        {toggle ? (
          <div className="row">
            <div className="col-12 tawuniyaCanvasContainer vh-100">
              <NormalOffCanvas closeToggler={closeToggler} />
            </div>
          </div>
        ) : (
          <React.Fragment>
            {productToggle ? <MoreProductCard /> : null}
            <div
              className={
                productToggle
                  ? "mainContainer container-fluid blurBackground vh-100"
                  : "mainContainer container-fluid vh-100"
              }
              onClick={
                productToggle
                  ? () => productToggle && setProductToggle(false)
                  : supportCard
                  ? () => supportCard && setSupportCard(false)
                  : languageCard
                  ? () => languageCard && setLanguageCard(false)
                  : getStartedCard
                  ? () => getStartedCard && setGetStartedCard(false)
                  : emergencyCard
                  ? () => emergencyCard && setEmergencyCard(false)
                  : ""
              }
            >
              {historyLinks.map(({ routeLink }) => {
                return (
                  <>
                    {history.location.pathname === routeLink && (
                      <ToggleBanner />
                    )}
                  </>
                );
              })}
              <div className="row">
                <div className="col-12 insuranceNavBg">
                  <Navbar
                    handleToggler={handleToggler}
                    toggleLanguageCard={toggleLanguageCard}
                    toggleSupportCard={toggleSupportCard}
                    toggleEmergencyCard={toggleEmergencyCard}
                    toggleGetStartedCard={toggleGetStartedCard}
                    navContent={navHomeContent}
                    isNavBg={true}
                  />

                  <InsuranceNavBar
                    handleToggler={handleToggler}
                    toggleLanguageCard={toggleLanguageCard}
                    toggleSupportCard={toggleSupportCard}
                    toggleEmergencyCard={toggleEmergencyCard}
                    toggleGetStartedCard={toggleGetStartedCard}
                    navContent={navHomeContent}
                  />
                </div>
                <div className="col-12 paddingContainer landingContainer">
                  {languageCard ? <LanguageCard isAlign={true} /> : null}
                  {supportCard ? <SupportCard isAlign={true} /> : null}
                  {emergencyCard ? <EmergencyCard isAlign={true} /> : null}
                  {getStartedCard ? <GetStartedCard isAlign={true} /> : null}

                  <div className="paddingContainer">
                    {history.location.pathname === "/home" && (
                      <CommonBanner
                        header="Insure yourself within minutes!"
                        para="Buying Motor, Medical, Travel and other insurance products for your
            needs has never been this easy"
                        toggleProductCard={toggleProductCard}
                      />
                    )}
                    <CommonBreadCrumb />
                    {props.children}
                  </div>
                </div>
              </div>
              <CommonFooter />
            </div>
          </React.Fragment>
        )}
      </ThemeProvider>
    </>
  );
};
