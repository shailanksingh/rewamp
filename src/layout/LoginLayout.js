import React, { useState } from "react";
import { useDispatch } from "react-redux";
import { arabTranslate } from "action/LanguageAct";
import { createTheme, ThemeProvider } from "@material-ui/core";
import { Navbar } from "component/common/Navbar";
import { NormalOffCanvas } from "component/common/NormalOffCanvas";
import {
	LanguageCard,
	SupportCard,
	GetStartedCard,
	EmergencyCard,
} from "component/HomePage/LandingComponent/NavbarCards";
import { AuthFooter } from "component/common/AuthFooter";
import { MoreProductCard } from "../component/HomePage/LandingComponent/NavbarCards";
const theme = createTheme({
	palette: {
		common: {},
		primary: {
			main: "#EE7500",
		},
		secondary: {
			main: "#FFFFFF",
		},
		// redColor: palette.augmentColor({ color: red }),
	},
});

export const LoginLayout = (props) => {
	const [toggle, setToggle] = useState(false);

	const [languageCard, setLanguageCard] = useState(false);

	const [supportCard, setSupportCard] = useState(false);

	const [getStartedCard, setGetStartedCard] = useState(false);

	const [emergencyCard, setEmergencyCard] = useState(false);

	const [productToggle, setProductToggle] = useState(false);

	const [arabToggle, setArabToggle] = useState(false);

	const dispatch = useDispatch();

	const translateHandler = () => {
		setArabToggle(true);
		dispatch(arabTranslate(true));
	};

	console.log(arabToggle, "arabToggle");
	const handleToggler = () => {
		setToggle(true);
	};

	const closeToggler = () => {
		setToggle(false);
	};

	const toggleLanguageCard = () => {
		setLanguageCard(!languageCard);
	};

	const toggleSupportCard = () => {
		setSupportCard(!supportCard);
	};

	const toggleGetStartedCard = () => {
		setGetStartedCard(!getStartedCard);
	};

	const toggleEmergencyCard = () => {
		setEmergencyCard(!emergencyCard);
	};

	const navAuthContent = [
		{
			id: 0,
			navText: "Individuals",
			navTextAr: "المطالبات",
		},
		{
			id: 1,
			navText: "Corporate",
			navTextAr: "المستثمرين",
		},
		{
			id: 2,
			navText: "Investor",
			navTextAr: "الشركات",
		},
		{
			id: 3,
			navText: "Claims",
			navTextAr: "الافراد",
		},
	];

	return (
		<>
			<ThemeProvider theme={theme}>
				{toggle ? (
					<div className="row">
						<div className="col-12 tawuniyaCanvasContainer vh-100">
							<NormalOffCanvas closeToggler={closeToggler} />
						</div>
					</div>
				) : (
					<React.Fragment>
						{productToggle ? <MoreProductCard /> : null}
						<div
							className={
								productToggle
									? "container-fluid blurBackground vh-100"
									: "container-fluid authContainer vh-100"
							}
							onClick={
								productToggle
									? () => productToggle && setProductToggle(false)
									: supportCard
									? () => supportCard && setSupportCard(false)
									: languageCard
									? () => languageCard && setLanguageCard(false)
									: getStartedCard
									? () => getStartedCard && setGetStartedCard(false)
									: emergencyCard
									? () => emergencyCard && setEmergencyCard(false)
									: ""
							}
						>
							<div className="row">
								<div className="col-12 sticky">
									<Navbar
										handleToggler={handleToggler}
										toggleLanguageCard={toggleLanguageCard}
										toggleSupportCard={toggleSupportCard}
										toggleEmergencyCard={toggleEmergencyCard}
										toggleGetStartedCard={toggleGetStartedCard}
										navContent={navAuthContent}
									/>
								</div>
								<div className="col-12 px-0 paddingContainer">
									{languageCard ? (
										<LanguageCard
											arabToggle={arabToggle}
											translateHandler={translateHandler}
										/>
									) : null}
									{supportCard ? <SupportCard /> : null}
									{getStartedCard ? <GetStartedCard /> : null}
									{emergencyCard ? <EmergencyCard /> : null}
									<div className="authBgContainer">{props.children}</div>
								</div>
							</div>
							<AuthFooter />
						</div>
					</React.Fragment>
				)}
			</ThemeProvider>
		</>
	);
};
