import React, {useEffect, useState} from "react";
import {history} from "service/helpers";
import {Navbar} from "component/common/Navbar";
import {NormalOffCanvas} from "component/common/NormalOffCanvas";
import {HomeBanner} from "component/HomePage/LandingComponent";
import {
  EmergencyCard,
  LanguageCard,
  SupportCard,
  GetStartedCard,
} from "component/HomePage/LandingComponent/NavbarCards";
import {CommonFooter} from "component/common/CommonFooter";
import {createTheme, ThemeProvider} from "@material-ui/core";
import {CommonBanner} from "component/common/CommonBanner";
import {MoreProductCard} from "../component/HomePage/LandingComponent/NavbarCards";
import {CommonBreadCrumb} from "component/common/CommonBreadCrumb";
import house from "assets/images/mobile/house.png";
import file from "assets/images/mobile/file.png";
import bag from "assets/images/mobile/bag.png";
import user from "assets/images/mobile/user.png";
import greenball from "assets/images/mobile/greenball.png";
import "./MyLayoutStyle.scss";
import LoginPageMobile from "component/Auth/LoginPageMobile";
import {useDispatch, useSelector} from "react-redux";
import {updateLanguage} from "action/LanguageAct";
import {useTranslation} from "react-i18next";


const theme = createTheme({
  palette: {
    common: {},
    primary: {
      main: "#EE7500",
    },
    secondary: {
      main: "#FFFFFF",
    },
    // redColor: palette.augmentColor({ color: red }),
  },
});

export const MyLayout = (props) => {
  const [toggle, setToggle] = useState(false);
  const dispatch = useDispatch();
  const [languageCard, setLanguageCard] = useState(false);

  const [supportCard, setSupportCard] = useState(false);

  const [emergencyCard, setEmergencyCard] = useState(false);

  const [getStartedCard, setGetStartedCard] = useState(false);

  const [productToggle, setProductToggle] = useState(false);

  const [isOpenLoginModel, setIsOpenLoginModel] = useState(false);

  const selectedLanguage = useSelector((data) => data.languageReducer.language);

  const {t, i18n} = useTranslation();

  const handleToggler = () => {
    setToggle(true);
  };

  const closeToggler = () => {
    setToggle(false);
  };

  const toggleLanguageCard = () => {
    setLanguageCard(!languageCard);
  };

  const toggleSupportCard = () => {
    setSupportCard(!supportCard);
  };

  const toggleEmergencyCard = () => {
    setEmergencyCard(!emergencyCard);
  };

  const toggleGetStartedCard = () => {
    setGetStartedCard(!getStartedCard);
  };

  const toggleProductCard = () => {
    setProductToggle(!productToggle);
  };

  const navHomeContent = [
    {
      id: 0,
      navText: t('navbar.individuals'),
    },
    {
      id: 1,
      navText: t('navbar.corporate'),
    },
    {
      id: 2,
      navText: t('navbar.investor'),
    },
    {
      id: 3,
      navText: t('navbar.claims'),
    },
  ];

  const historyLinks = [
    {
      id: 0,
      routeLink: "/home",
    },
    {
      id: 1,
      routeLink: "/home/motorpage",
    },
  ];


  useEffect(() => {
    let language = localStorage.getItem("language");
    if (language) {
      dispatch(updateLanguage(language));
    } else {
      dispatch(updateLanguage("english"));
    }
  }, []);

  useEffect(() => {
    if (selectedLanguage === "arabic") {
      document
        .getElementsByTagName("body")[0]
        .classList.add("language-alignment-right");
      i18n.changeLanguage('ar')
    } else {
      document
        .getElementsByTagName("body")[0]
        .classList.remove("language-alignment-right");
      i18n.changeLanguage('en')
    }
  }, [selectedLanguage]);

  useEffect(() => {
    window.scrollTo(0, 0);
  }, [props.location]);

  const openLoginPage = () => {
    return setIsOpenLoginModel(true);
  };


  return (
    <>
      {window.innerWidth > 600 ? (
        <ThemeProvider theme={theme}>
          {toggle ? (
            <div className="row">
              <div className="col-12 tawuniyaCanvasContainer vh-100">
                <NormalOffCanvas closeToggler={closeToggler}/>
              </div>
            </div>
          ) : (
            <React.Fragment>
              {productToggle ? <MoreProductCard isPillLayout={true}/> : null}
              <div
                className={
                  productToggle
                    ? "blurBackground vh-100"
                    : "vh-100"
                }
                onClick={
                  productToggle
                    ? () => productToggle && setProductToggle(false)
                    : supportCard
                      ? () => supportCard && setSupportCard(false)
                      : languageCard
                        ? () => languageCard && setLanguageCard(false)
                        : getStartedCard
                          ? () => getStartedCard && setGetStartedCard(false)
                          : emergencyCard
                            ? () => emergencyCard && setEmergencyCard(false)
                            : ""
                }
              >
                {historyLinks.map(({routeLink}) => history.location.pathname === routeLink &&
                  <HomeBanner key={routeLink}/>)}
                <div className="row">
                  <div className="col-12">
                    <Navbar
                      handleToggler={handleToggler}
                      toggleLanguageCard={toggleLanguageCard}
                      toggleSupportCard={toggleSupportCard}
                      toggleEmergencyCard={toggleEmergencyCard}
                      toggleGetStartedCard={toggleGetStartedCard}
                      navContent={navHomeContent}
                    />
                  </div>
                  <div className="col-12 paddingContainer landingContainer">
                    {languageCard ? <LanguageCard/> : null}
                    {supportCard ? <SupportCard/> : null}
                    {emergencyCard ? <EmergencyCard/> : null}
                    {getStartedCard ? <GetStartedCard/> : null}

                    <div className="paddingContainer">
                      {history.location.pathname === "/home" && (
                        <CommonBanner
                          header={t('commonBanner.header')}
                          para={t('commonBanner.paragraph')}
                          toggleProductCard={toggleProductCard}
                          isPillLayout={true}
                        />
                      )}
                      <CommonBreadCrumb/>
                      {props.children}
                    </div>
                  </div>
                </div>
                <CommonFooter/>
              </div>
            </React.Fragment>
          )}
        </ThemeProvider>
      ) : (
        <>
          {props.children}
          <div className="mobile_sticky_bottom_menu">
            <div class="card-body p-20 bottom-icons  third-footer bg2">
              <div className="footer-icons">
                <div className="footer_body">
                  <div className="text-center">
                    <img src={house} alt="Home"/>
                    <p className="active_menu">Home</p>
                  </div>
                  <div className="text-center third-click">
                    <img src={file} width="40%" alt="Claims"/>
                    <p className="">Claims</p>
                  </div>
                  <div className="text-center greenball-placing">
                    <img className="green-img" src={greenball} alt="Green"/>
                  </div>
                  <div
                    className="text-center"
                    onClick={() => history.push("/home/all-products")}
                  >
                    <img src={bag} alt="Products"/>
                    <p className="">Products</p>
                  </div>
                  <div className="text-center" onClick={() => openLoginPage()}>
                    <img src={user} alt="Profile"/>
                    <p className="">Profile</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <LoginPageMobile
            isOpenLoginModel={isOpenLoginModel}
            setIsOpenLoginModel={setIsOpenLoginModel}
          />
        </>
      )}
    </>
  );
};
