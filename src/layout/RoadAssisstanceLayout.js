import React, { useState, useEffect } from "react";
import { history } from "service/helpers";
import { RoadAssisstantSideBar } from "component/common/RoadAssisstantSideBar";
import { NeedHelpContainer } from "component/common/NeedHelpContainer";
import { CommonBreadCrumb } from "component/common/CommonBreadCrumb";
import { RecentFeeds } from "component/common/RecentFeeds";

export const RoadAssisstanceLayout = (props) => {
	const footerData = [
		"Terms and Conditions",
		"Privacy Policy",
		"Cookie Policy",
	];

	const layerOne = <RecentFeeds />;

	const layerTwo = <NeedHelpContainer />;

	const dashBoardLayerLink = [
		{
			id: 0,
			link: ["/dashboard", "/dashboard/service"],
			comp: layerOne,
		},
		{
			id: 1,
			link: [
				"/dashboard/service/request-telemedicine",
				"/dashboard/service/tele-medicine/new-request",
				"/dashboard/service/tele-medicine/request-confirmation/1",
			],
			comp: layerTwo,
		},
		{
			id: 2,
			link: ["/dashboard/tele-medicine/request-detail"],
			comp: layerTwo,
		},
		{
			id: 4,
			link: ["/dashboard/tele-request"],
			comp: layerTwo,
		},
		{
			id: 4,
			link: [
				"/dashboard/service/road-assistance",
				"/dashboard/service/road-assisstance/new-request",
				"/dashboard/service/road-assisstance/request-confirmation/1",
			],
			comp: layerTwo,
		},
		{
			id: 5,
			link: ["/dashboard/productsandofferings"],
			comp: layerOne,
		},
		{
			id: 6,
			link: [
				"/dashboard/service/periodic-inspection",
				"/dashboard/service/periodic-inspection/new-request",
				"/dashboard/service/periodic-inspection/request-confirmation/1",
			],
			comp: layerTwo,
		},
		{
			id: 7,
			link: ["/dashboard/service/car-wash"],
			comp: layerTwo,
		},
		{
			id: 8,
			link: [
				"/dashboard/service/car-maintainance",
				"/dashboard/service/car-maintainance/new-request",
				"/dashboard/service/car-maintainance/request-confirmation/1",
			],
			comp: layerTwo,
		},
		{
			id: 9,
			link: ["/dashboard/service/pregnancy-program"],
			comp: layerTwo,
		},
		{
			id: 10,
			link: ["/dashboard/service/medical-reimbursment"],
			comp: layerTwo,
		},
		{ id: 11, link: ["/dashboard/supportcenter"], comp: layerOne },

		{
			id: 12,
			link: ["/dashboard/claims"],
			comp: layerOne,
		},
		{
			id: 13,
			link: ["/dashboard/requestsandapprovals"],
			comp: layerOne,
		},
		{ id: 14, link: ["/dashboard/notifications"], comp: layerOne },
		{
			id: 15,
			link: [
				"/dashboard/service/eligibility-letter",
				"/dashboard/service/eligibility-letter/new-request",
				"/dashboard/service/eligibility-letter/request-confirmation/1",
			],
			comp: layerTwo,
		},
		{
			id: 16,
			link: ["/dashboard/your-policies"],
			comp: layerOne,
		},
		{
			id: 17,
			link: ["/dashboard/policydetails/health"],
			comp: layerOne,
		},
		{
			id: 18,
			link: ["/dashboard/policydetails/motor"],
			comp: layerOne,
		},
		{
			id: 20,
			link: ["/dashboard/policydetails/travel"],
			comp: layerOne,
		},
		{
			id: 21,
			link: ["/dashboard/eligibility-letter"],
			comp: layerTwo,
		},
	];

	useEffect(() => {
		window.scrollTo(0, 0);
	}, [props.location]);

	return (
		<div className="mainContainer container-fluid vh-100 pr-0">
			<div className="d-flex flex-row dashboard-RightContainer">
				<div className="flexDashboardOne">
					<RoadAssisstantSideBar />
				</div>
				<div className="flexDashboardTwo">
					<CommonBreadCrumb />
					{props.children}
					<div className="row">
						<div className="col-12 footer-road-container">
							<div className="d-flex justify-content-start pb-4">
								<div>
									<span className="fs-12 fw-400 road-copyrights">
										Copyright © Tawuniya 2022, all rights reserved
									</span>
									<div className="d-flex flex-row">
										{footerData.map((items) => {
											return (
												<div className="pr-2">
													<span className="fs-12 fw-400 road-copyrights">
														{items}
													</span>
												</div>
											);
										})}
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div className="flexDashboardThree">
					{dashBoardLayerLink.map((item) => {
						return (
							<React.Fragment>
								{item?.link?.map((i) => {
									return <>{history.location.pathname === i && item.comp}</>;
								})}
							</React.Fragment>
						);
					})}
				</div>
			</div>
		</div>
	);
};
