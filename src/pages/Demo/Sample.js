import { SampleComp } from "component/Demo";
import React, { useEffect } from "react";
import { callHelloApi } from "service/renewel-service";

const Sample = () => {
  return (
    <div>
      <SampleComp />
    </div>
  );
};

export default Sample;
