import React from "react";
import { HealthInsuranceVisitVisa } from "component/Products/Individuals/PropertyAndCasuality";

const HealthInsuranceVisitVisaPage = () => {
    return (
        <HealthInsuranceVisitVisa />
    );
}

export default HealthInsuranceVisitVisaPage;