import React from "react";
import { InternationalTravel } from "component/Products/Individuals/PropertyAndCasuality";

const InternationalTravelPage = () => {
	return (
		<React.Fragment>
			<InternationalTravel />
		</React.Fragment>
	);
};

export default InternationalTravelPage;
