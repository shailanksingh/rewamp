import React from "react";
import { HealthInsuranceMyFamily } from "component/Products/Individuals/PropertyAndCasuality";

const HealthInsuranceMyFamilyPage = () => {
    return (
        <HealthInsuranceMyFamily />
    );
}

export default HealthInsuranceMyFamilyPage;