import { MedicalMalPractice } from "component/Products/Individuals/PropertyAndCasuality";
import React from "react";

const MedicalMalPracticeNewPage = () => {
	return (
		<React.Fragment>
			{window.innerWidth < 600 ? "This is for mobile" : <MedicalMalPractice />}
		</React.Fragment>
	);
};

export default MedicalMalPracticeNewPage;
