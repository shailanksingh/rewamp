import React from "react";
import { HomeInsurance } from "component/Products/Individuals/PropertyAndCasuality";
import HomeInsurancePageMobile from "component/Homeinsurance/HomeInsurancePageMobile";

const HomeInsurancePage = () => {
  return (
    <React.Fragment>
      {window.innerWidth < 600 ? (
        <HomeInsurancePageMobile />
      ) : (
        <HomeInsurance />
      )}
    </React.Fragment>
  );
};

export default HomeInsurancePage;
