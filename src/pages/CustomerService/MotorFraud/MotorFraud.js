import React from "react";
import { MotorFrauds } from "component/CustomerService";
import { MotorFraudMobile } from "component/CustomerService/MotorFraudMobile/MotorFraudMobile";
const MotorFraud = () => {
	return (
		<React.Fragment>
			{" "}
			{console.log(window.innerWidth, "window.innerWidth")}
			{window.innerWidth < 600 ? <MotorFraudMobile /> : <MotorFrauds />}
		</React.Fragment>
	);
};

export default MotorFraud;
