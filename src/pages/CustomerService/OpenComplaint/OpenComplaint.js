import React from "react";
import { OpenComplaints } from "component/CustomerService";
import SupportCenterMobile from "component/SupportCenter/SupportCenterMobile";

const OpenComplaint = () => {
  return (
    <div>
      {window.innerWidth > 600 ? <OpenComplaints /> : <SupportCenterMobile />}
    </div>
  );
};

export default OpenComplaint;
