import React from "react";
import { SupportRequestConfirmation } from "component/CustomerService";

const SupportRequestConfirmationpage = () => {
  return (
    <div>
      <SupportRequestConfirmation />
    </div>
  );
};

export default SupportRequestConfirmationpage;
