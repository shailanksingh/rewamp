import React from "react";
import { CustomerServiceLanding } from "component/CustomerService";
import { CustomerServiceLandingMobile } from "component/CustomerService/CustomerServiceLanding/CustomerServiceLanding/mobile";

const CustomerServiceLandingPage = () => {
  return (
    <div>
      {window.innerWidth < 600 ? (
        <CustomerServiceLandingMobile />
      ) : (
        <CustomerServiceLanding />
      )}
    </div>
  );
};

export default CustomerServiceLandingPage;
