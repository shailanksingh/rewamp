import React from "react";
import { ServiceCategory } from "component/CustomerService";

const ServicesCategory = () => {
	return (
		<div>
			<ServiceCategory />
		</div>
	);
};

export default ServicesCategory;
