import React from 'react';
import { SupportRequests }  from "component/CustomerService";
import { MotorFraudMobile } from 'component/CustomerService';

const SupportRequest = () =>{
    return(
      <div>
        {window.innerWidth > 600 ? <SupportRequests/> : <MotorFraudMobile/> }
        </div>
    )
}

export default SupportRequest