import TrackRequestMobile from "component/CustomerService/TrackRequestMobile/TrackRequestMobile";
import React from "react";

const TrackRequest = () => {
  return (
    <div>
      {window.innerWidth > 600 ? <h1>Desktop</h1> : <TrackRequestMobile />}
    </div>
  );
};

export default TrackRequest;
