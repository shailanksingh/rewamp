import React from "react";
import { HomeRefillMedicinePage } from "component/RefillMedicinePage";

const HomeRefillTeleMedicinesPage = () => {
    return (
      <React.Fragment>
         <HomeRefillMedicinePage />
      </React.Fragment>
    );
  };
  
export default HomeRefillTeleMedicinesPage;