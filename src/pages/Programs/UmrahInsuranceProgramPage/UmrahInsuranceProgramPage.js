import React from "react";
import UmrahInsuranceProgram from "../../../component/Programs/UmrahInsuranceProgram";

const UmrahInsuranceProgramPage = () => {
	return (
		<div>
            <UmrahInsuranceProgram />
		</div>
	);
};

export default UmrahInsuranceProgramPage;
