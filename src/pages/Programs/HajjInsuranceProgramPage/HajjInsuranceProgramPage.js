import React from "react";
import HajjInsuranceProgram from "../../../component/Programs/HajjInsuranceProgram";

const HajjInsuranceProgramPage = () => {
	return (
		<div>
            <HajjInsuranceProgram />
		</div>
	);
};

export default HajjInsuranceProgramPage;
