import React from "react";
import EmergencySupport from "component/EmergencySupport/EmergencySupportMobile/EmergencySupport";

const EmergencySupportPage = () => {
  return (
    <React.Fragment>
      <EmergencySupport />
    </React.Fragment>
  );
};

export default EmergencySupportPage;
