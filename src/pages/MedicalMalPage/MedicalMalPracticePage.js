import React from "react";
import { MedicalMalpractice } from "component/MedicalMalpractice/MedicalMalpractice";
import MedicalMalpracticeMobile from "component/MedicalMalpractice/MedicalMalpracticeMobile";

export default function MedicalMalPracticePage() {
  return (
    <React.Fragment>
      {window.innerWidth < 600 ? (
        <MedicalMalpracticeMobile />
      ) : (
        <MedicalMalpractice />
      )}
    </React.Fragment>
  );
}
