import CookiePolicyMobile from "component/CookiePolicy/mobile";
import React from "react";

const CookiePolicyPage = () => {
  return (
    <React.Fragment>
      {window.innerWidth < 600 ? (
        <CookiePolicyMobile />
      ) : (
        <div>Cookie policy</div>
      )}
    </React.Fragment>
  );
};

export default CookiePolicyPage;
