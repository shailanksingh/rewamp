import PrivacyPolicyMobile from "component/PrivacyPolicy/Mobile";
import React from "react";

const PrivacyPolicyPage = () => {
  return (
    <React.Fragment>
      {window.innerWidth < 600 ? (
        <PrivacyPolicyMobile />
      ) : (
        <div>Privacy policy</div>
      )}
    </React.Fragment>
  );
};

export default PrivacyPolicyPage;
