import TawuniyaAuctionMobile from "component/TawuniyaAuction/mobile";
import React from "react";

const TawuniyaAuctionPage = () => {
  return (
    <React.Fragment>
      {window.innerWidth < 600 ? (
        <TawuniyaAuctionMobile />
      ) : (
        <div>Tawuniya Auction</div>
      )}
    </React.Fragment>
  );
};

export default TawuniyaAuctionPage;
