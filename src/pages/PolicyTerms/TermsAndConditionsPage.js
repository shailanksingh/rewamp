import React from "react";
import TermsAndConditionsMobile from "component/TermsAndConditions/mobile";

const TermsAndConditionsPage = () => {
  return (
    <React.Fragment>
      {window.innerWidth < 600 ? (
        <TermsAndConditionsMobile />
      ) : (
        <div>Terms and conditions</div>
      )}
    </React.Fragment>
  );
};

export default TermsAndConditionsPage;
