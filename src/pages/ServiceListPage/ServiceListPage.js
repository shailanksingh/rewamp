import ServiceList from "component/ServiceList";
import React from "react";

const ServiceListPage = () => {
  return <div>{<ServiceList />}</div>;
};

export default ServiceListPage;
