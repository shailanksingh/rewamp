import React from "react";
import { FinancialHighlights } from "component/AboutUs";
import AboutUsLandingPageMobile from "component/AboutUs/AboutUsMobile/AboutUsLandingPageMobile";

const FinancialHighlightsPage = () => {
  return (
    <div>
      {window.innerWidth > 600 ? (
        <FinancialHighlights />
      ) : (
        <AboutUsLandingPageMobile />
      )}
    </div>
  );
};

export default FinancialHighlightsPage;
