import React from "react";
import { BoardOfDirectors } from "component/AboutUs/BoardOfDirectors/BoardOfDirectors";
import AboutUsLandingPageMobile from "component/AboutUs/AboutUsMobile/AboutUsLandingPageMobile";
const AboutUsDirectorsPage = () => {
	return (
		<div>
			{window.innerWidth > 600 ? (
				<BoardOfDirectors />
			) : (
				<AboutUsLandingPageMobile />
			)}
		</div>
	);
};

export default AboutUsDirectorsPage;
