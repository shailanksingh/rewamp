import React from "react";
import { AboutUsMain } from "component/AboutUs/LandingPage/LandingPage";
import AboutUsLandingPageMobile from "component/AboutUs/AboutUsMobile/AboutUsLandingPageMobile";

const AboutUsLandingPage = () => {
  return (
    <div>
      {window.innerWidth > 600 ? <AboutUsMain /> : <AboutUsLandingPageMobile />}
    </div>
  );
};

export default AboutUsLandingPage;
