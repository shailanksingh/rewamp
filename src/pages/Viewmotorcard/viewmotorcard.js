import React from 'react'
import viewmotorcard from '../../assets/viewmotorcard/viewmotorcard.png'
import file from '../../assets/viewmotorcard/file.png'
import calender from '../../assets/viewmotorcard/calender.png'

function Viewmotorcard() {
  return (
    <div>
        <div class="motor">
  <div className='numberplate'>
  <div class="">
    <div class='viewmotor d-flex'>

    <img src={viewmotorcard}></img>
    <div className='platenumber'>
    <h5 class="plate">Plate Number</h5>
    <h5 className='tnd'> 3576 TND</h5>
    </div>
    </div>
    <div className='kutty d-flex'>
      <div className=''>
      <img src={file}></img>
    <h5 className='casenumber'>Case Number</h5>
    <h5 className='caseno'>1234</h5>
    </div>
    <div className=''>
    <img src={calender}></img>
    <h5 className='accident'>Accident Date</h5>
    <h5 className='caseno'>1/1/2022</h5>
    </div>
    </div>
    </div>
    <a  class="createclaim"> Create Claim</a>
  </div>
</div>
    </div>
  )
}

export default Viewmotorcard