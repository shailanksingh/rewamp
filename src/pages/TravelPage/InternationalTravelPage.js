import { InternationalTravel } from "component/Travel/InternationalTravel/index";
import React from "react";

const InternationalTravelPage = () => {
  return (
    <React.Fragment>
      <InternationalTravel />
    </React.Fragment>
  );
};

export default InternationalTravelPage;
