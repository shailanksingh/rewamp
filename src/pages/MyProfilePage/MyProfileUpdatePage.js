import React from "react";
import MyProfileUpdateMobile from "component/MyProfile/Mobile/MyProfileUpdate";

const MyProfileUpdatePage = () => {
  return (
    <React.Fragment>
      {window.innerWidth < 600 ? (
        <MyProfileUpdateMobile />
      ) : (
        <div>My Profile</div>
      )}
    </React.Fragment>
  );
};

export default MyProfileUpdatePage;
