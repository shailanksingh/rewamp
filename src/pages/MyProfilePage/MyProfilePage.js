import MyProfileMobile from "component/MyProfile/Mobile";
import React from "react";

const MyProfilePage = () => {
  return (
    <React.Fragment>
      {window.innerWidth < 600 ? <MyProfileMobile /> : <div>My Profile</div>}
    </React.Fragment>
  );
};

export default MyProfilePage;
