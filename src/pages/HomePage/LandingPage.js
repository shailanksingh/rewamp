import React from "react";
import { Landingpage } from "component/HomePage/Landingpage/Landingpage";
import { createTheme, ThemeProvider } from "@material-ui/core";

const theme = createTheme({
	palette: {
		common: {},
		primary: {
			main: "#EE7500",
		},
		secondary: {
			main: "#C4314B",
		},
		background: {
			default: "#F2F3F5",
			paper: "#F2F3F5",
		},
		// redColor: palette.augmentColor({ color: red }),
	},
});

const LandingPage = () => {
	return (
		<div>
			<ThemeProvider theme={theme}>
				<Landingpage />
			</ThemeProvider>
		</div>
	);
};

export default LandingPage;
