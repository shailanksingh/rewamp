import React from "react";
import CareersMobile from "component/Careers/CareersMobile";
import CareersDesktop from "component/Careers/CareersDesktop";

const CareersPage = () => {
  return (
    <div>
      {window.innerWidth > 600 ? <CareersDesktop /> : <CareersMobile />}
    </div>
  );
};

export default CareersPage;
