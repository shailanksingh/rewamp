import React from "react";
import { ServicePage } from "component/DashBoard/ServicesPage";

const ServicesPage = () => {
	return (
		<div>
			<ServicePage />
		</div>
	);
};

export default ServicesPage;
