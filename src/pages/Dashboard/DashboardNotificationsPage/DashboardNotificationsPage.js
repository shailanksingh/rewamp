import React from "react";
import { NotificationPage } from "component/DashBoard";

const DashboardNotificationsPage = () => {
	return (
		<div>
			<NotificationPage />
		</div>
	);
};

export default DashboardNotificationsPage;
