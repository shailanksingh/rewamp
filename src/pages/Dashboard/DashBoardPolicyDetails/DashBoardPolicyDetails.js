import React from "react";
import { DashBoardPolicyDetails } from "component/DashBoard/DashBoardPolicyDetails/DashBoardPolicyDetails";

const DashBoardPolicyDetailsPage = () => {
	return (
		<div>
			<DashBoardPolicyDetails />
		</div>
	);
};

export default DashBoardPolicyDetailsPage;
