import React from "react";
import { HomeTeleMedicinePage } from "component/DashBoard/TeleMedicinePage";

const HomeTeleMedicinesPage = () => {
    return (
      <React.Fragment>
         <HomeTeleMedicinePage />
      </React.Fragment>
    );
  };
  
export default HomeTeleMedicinesPage;