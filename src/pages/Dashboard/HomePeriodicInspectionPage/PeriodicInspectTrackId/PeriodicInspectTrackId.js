import React from "react";
import { PeriodicInspectionTrackId } from "component/DashBoard/PeriodicInspectionPage/PeriodicInspectionTrackId/PeriodicInspectionTrackId";

const PeriodicInspectTrackId = () => {
	return (
		<div>
			<PeriodicInspectionTrackId />
		</div>
	);
};

export default PeriodicInspectTrackId;
