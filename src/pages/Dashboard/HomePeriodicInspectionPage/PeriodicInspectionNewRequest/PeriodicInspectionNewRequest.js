import React from "react";
import { PeriodicInspectRequestPage } from "component/DashBoard";

const PeriodicInspectionNewRequest = () => {
	return (
		<div>
			<PeriodicInspectRequestPage />
		</div>
	);
};

export default PeriodicInspectionNewRequest;
