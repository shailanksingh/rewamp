import AllProductsMobile from "component/AllProducts/AllProductsMobile";
import React from "react";

const AllProductsMobilePage = () => {
  return (
    <React.Fragment>
      <AllProductsMobile />
    </React.Fragment>
  );
};

export default AllProductsMobilePage;
