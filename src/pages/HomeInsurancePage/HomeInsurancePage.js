import React from "react";
import { HomeInsurancePages } from "component/HomeInsurancePage";

const HomeInsurancePage = () => {
  return (
    <React.Fragment>
      <HomeInsurancePages />
    </React.Fragment>
  );
};

export default HomeInsurancePage;
