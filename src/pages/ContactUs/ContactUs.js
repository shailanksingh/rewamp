import React from "react";
import { ContactusPage } from "component/ContactUs";
import ContactUsMobile from "component/ContactUs/ContactusPage/ContactUsMobile";

const ContactUs = () => {
  return (
    <React.Fragment>
      {window.innerWidth > 600 ? <ContactusPage /> : <ContactUsMobile />}
    </React.Fragment>
  );
};

export default ContactUs;
