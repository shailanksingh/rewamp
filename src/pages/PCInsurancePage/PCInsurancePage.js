import PCInsuranceMobile from "component/PCInsurance/mobile";
import React from "react";

const PCInsurancePage = () => {
  return (
    <div className="">
      {window.innerWidth < 600 ? (
        <PCInsuranceMobile />
      ) : (
        "Web page PC Insurance"
      )}
    </div>
  );
};

export default PCInsurancePage;
