import React from "react";
import { HomeTeleMedicinePage } from "component/TeleMedicinePage";

const HomeTeleMedicinesPage = () => {
    return (
      <React.Fragment>
         <HomeTeleMedicinePage />
      </React.Fragment>
    );
  };
  
export default HomeTeleMedicinesPage;