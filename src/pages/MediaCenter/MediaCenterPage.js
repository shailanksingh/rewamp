import { MediaCenterNews } from "component/MediaCenter";
import MediaCenterNewsMobile from "component/MediaCenter/MobilePages/MediaCenterNewsMobile";
import React from "react";

const MediaCenterPage = () => {
  return (
    <React.Fragment>
      {window.innerWidth > 600 ? (
        <MediaCenterNews />
      ) : (
        <MediaCenterNewsMobile />
      )}
    </React.Fragment>
  );
};

export default MediaCenterPage;
