import React from "react";
import { MediaCenterNewsDetails } from "component/MediaCenter/MediaCenterNewsDetails/index";
import MediaCenterNewsDetailsMobile from "component/MediaCenter/MobilePages/MediaCenterNewsDetails";

function MediaCenterNewsDetailsPage() {
  return (
    <React.Fragment>
      {window.innerWidth > 600 ? (
        <MediaCenterNewsDetails />
      ) : (
        <MediaCenterNewsDetailsMobile />
      )}
    </React.Fragment>
  );
}

export default MediaCenterNewsDetailsPage;
