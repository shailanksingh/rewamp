import { MediaNewsCategory } from "component/MediaCenter";
import MediaCenterNewsCategoryMobile from "component/MediaCenter/MobilePages/MediaCenterNewsCategoryMobile";
import React from "react";

const MediaNewsCategoryPage = () => {
  return (
    <React.Fragment>
      {window.innerWidth > 600 ? (
        <MediaNewsCategory />
      ) : (
        <MediaCenterNewsCategoryMobile />
      )}
    </React.Fragment>
  );
};

export default MediaNewsCategoryPage;
