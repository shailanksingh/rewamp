import React from "react";
import { InvestorRelationsMain } from "component/InvestorRelations";
import InvestorRelationsMobile from "component/InvestorRelations/mobile/InvestorRelationsMobile";

const InvestorRelationsPage = () => {
  return (
    <div>
      {window.innerWidth > 600 ? (
        <InvestorRelationsMain />
      ) : (
        <InvestorRelationsMobile />
      )}
    </div>
  );
};

export default InvestorRelationsPage;
