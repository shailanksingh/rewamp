import React from "react";
import AllServiceMobile from "component/CustomerService/AllServiceMobile";

const AllServiceMobilePage = () => {
  return (
    <div>
      <AllServiceMobile />
    </div>
  );
};

export default AllServiceMobilePage;
