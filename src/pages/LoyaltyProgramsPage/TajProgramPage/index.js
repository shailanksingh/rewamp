import React from "react";
import TajProgram from "component/LoyaltyPrograms/mobile/TajProgram";

const TajProgramPage = () => {
  return (
    <div>
      {window.innerWidth < 600 ? <TajProgram /> : <div>TawuniyaDrive</div>}
    </div>
  );
};

export default TajProgramPage;
