import React from "react";
import TawuniyaVitality from "component/LoyaltyPrograms/mobile/TawuniyaVitality";

const TawuniyaVitalityPage = () => {
  return (
    <div>
      {window.innerWidth < 600 ? (
        <TawuniyaVitality />
      ) : (
        <div>TawuniyaIthra</div>
      )}
    </div>
  );
};

export default TawuniyaVitalityPage;
