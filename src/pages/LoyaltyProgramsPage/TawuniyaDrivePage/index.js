import React from "react";
import TawuniyaDrive from "component/LoyaltyPrograms/mobile/TawuniyaDrive";

const TawuniyaDrivePage = () => {
  return (
    <div>
      {window.innerWidth < 600 ? <TawuniyaDrive /> : <div>TawuniyaDrive</div>}
    </div>
  );
};

export default TawuniyaDrivePage;
