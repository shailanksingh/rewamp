import React from "react";
import TawuniyaIthra from "component/LoyaltyPrograms/mobile/TawuniyaIthra";

const TawuniyaIthrapage = () => {
  return (
    <div>
      {window.innerWidth < 600 ? <TawuniyaIthra /> : <div>TawuniyaIthra</div>}
    </div>
  );
};

export default TawuniyaIthrapage;
