import { RoadAssistanceMobile } from "component/RoadAssistanceMobile";
import React from "react";

const RoadAssistanceMobilePage = (services) => {
  return <RoadAssistanceMobile viewservice={services} />;
};

export default RoadAssistanceMobilePage;
