import TravelInsuranceMobile from "component/TravelInsurance/TravelInsuranceMobile";
import React from "react";

const TravelInsurancePage = () => {
  return (
    <React.Fragment>
      {window.innerWidth < 600 ? <TravelInsuranceMobile /> : ""}
    </React.Fragment>
  );
};

export default TravelInsurancePage;
