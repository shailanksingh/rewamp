import React from "react";
import InsuranceMobile from "component/InsuranceMobile/InsuranceMobile";

const MobileInsurance = () => {
  return (
    <React.Fragment>
      <InsuranceMobile />
    </React.Fragment>
  );
};

export default MobileInsurance;
