import SupportCenter from "component/SupportCenter";
import React from "react";

const SupportCenterPage = () => {
  return (
    <React.Fragment>
      <SupportCenter />
    </React.Fragment>
  );
};

export default SupportCenterPage;
