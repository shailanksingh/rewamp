import { MotorPageMobile } from "component/MotorPageMobile/MotorPageMobile";
import { CommonFaq } from "component/common/CommonFaq";
import { customerServiceFaqList } from "component/common/MockData";
import { InsuranceCardMobile } from "../../component/common/MobileReuseable/InsuranceCardMobile";
import MobileMotorTable from "component/common/MobileMotorTable";
import bail_bond from "assets/images/mobile/bail_bond.png";
import motor_road from "assets/news/motor_road.png";
import car_maintenance from "assets/news/car_maintenance.png";
import car_accident from "assets/news/car_accident.png";
import tele_med from "assets/images/mobile/tele_med.png";
import web_assist from "assets/images/mobile/web_assist.png";
import blue_flight_delay from "assets/images/mobile/blue_flight_delay.png";
import React from "react";
import right_arrow from "assets/images/mobile/right_arrow.png";
import MYP from "assets/images/mobile/MYP.png";
import Addvehicle from "assets/images/mobile/Addvehicle.png";
import Buycoverage from "assets/images/mobile/Buycoverage.png";
import Reniewpolicy from "assets/images/mobile/Reniewpolicy.png";
import search from "assets/images/mobile/search.png";
import Geographic from "assets/images/mobile/Geographic.png";
import Hire from "assets/images/mobile/Hire.png";
import Extension from "assets/images/mobile/Extension.png";
import Accidentcar from "assets/images/mobile/Accidentcar.svg";
import Availableproducts from "assets/images/mobile/Availableproducts.png";
import VehicleDash from "assets/images/mobile/VehicleDash.png";
import Covid from "assets/images/mobile/Covid-19.png";
import Travel from "assets/images/mobile/Travelinsurance.png";
import "./Style.scss";
import { useState } from "react";
const MotorMobile = () => {
  const [services, setServices] = useState([
    {
      id: 1,
      image: Covid,
      title: "Covid-19 Insurance",
      description:
        "Covid-19 Travel insurance - for Saudis Program was designed to provide protection for Saudi citizens and supports.",
      Data: "Benefits:",
      list: "Provides you with the necessary health care ",
      list1: "Medical expenses incurred around the world.",
    },
    {
      id: 2,
      image: Travel,
      title: "International Travel Insurance",
      description:
        "The company has identified the amount of compensation so that it starts from SR 920 and up to SR 3,000,000 . ",
      Data: "Benefits:",
      list: "Provides you with the necessary health care ",
      list1: "Medical expenses incurred around the world.",
    },
    {
      id: 3,
      image: Travel,
      title: "International Travel Insurance",
      description:
        "The company has identified the amount of compensation so that it starts from SR 920 and up to SR 3,000,000 . ",
      Data: "Benefits:",
      list: "Provides you with the necessary health care ",
      list1: "Medical expenses incurred around the world.",
    },
    {
      id: 4,
      image: Travel,
      title: "International Travel Insurance",
      description:
        "The company has identified the amount of compensation so that it starts from SR 920 and up to SR 3,000,000 . ",
      Data: "Benefits:",
      list: "Provides you with the necessary health care ",
      list1: "Medical expenses incurred around the world.",
    },
    {
      id: 5,
      image: Travel,
      title: "International Travel Insurance",
      description:
        "The company has identified the amount of compensation so that it starts from SR 920 and up to SR 3,000,000 . ",
      Data: "Benefits:",
      list: "Provides you with the necessary health care ",
      list1: "Medical expenses incurred around the world.",
    },
  ]);
  const insuranceCardData = [
    {
      id: 1,
      content: "Periodic Inspection ",
      cardIcon: bail_bond,
    },
    {
      id: 2,
      content: "Road Assistance",
      cardIcon: motor_road,
    },
    {
      id: 3,
      content: "Car Maintenance",
      cardIcon: car_maintenance,
    },
    {
      id: 4,
      content: "Car Accident",
      cardIcon: car_accident,
    },
    {
      id: 5,
      content: "Car Wash",
      cardIcon: tele_med,
    },
    {
      id: 6,
      content: "Refill Medication",
      cardIcon: tele_med,
    },
    {
      id: 7,
      content: "Medical Reimbursement",
      cardIcon: tele_med,
    },
    {
      id: 8,
      content: "Telemedicine",
      cardIcon: tele_med,
    },
    {
      id: 9,
      content: "Eligibility letter",
      cardIcon: tele_med,
    },
    {
      id: 10,
      content: "Pregnancy Program",
      cardIcon: tele_med,
    },
    {
      id: 11,
      content: "Chronic Disease Management",
      cardIcon: tele_med,
    },
    {
      id: 12,
      content: "Home Child Vaccination",
      cardIcon: tele_med,
    },
    {
      id: 13,
      content: "Assist America",
      cardIcon: web_assist,
    },
    {
      id: 14,
      content: "Flight Delay Assistance",
      cardIcon: blue_flight_delay,
    },
    {
      id: 15,
      content: "Request Bailbond",
      cardIcon: bail_bond,
    },
  ];
  return (
    <div className="Motorpage_container">
      <React.Fragment>
        <MotorPageMobile />
      </React.Fragment>
      <div className="motor_policy ">
        <div class="card border-0">
          <div>
            <div className="d-flex motor_policy_color ml-0">
              <div>
                <img src={MYP} />
              </div>
              <div className="mobile_container_coverage mx-2">
                <h4>Manage your Policy</h4>
                <p>Add more vehicles and coverage instantly</p>
              </div>
            </div>
          </div>
          <div>
            <div className="d-flex class mobile_container_images ">
              <div className="border-right mx-3 ">
                <img src={Addvehicle} className="mobile_image_position" />
                <p>Add Vehicles</p>
              </div>
              <div className="border-right mx-3 ">
                <img src={Buycoverage} className="mobile_image_position" />
                <p>Buy Coverages</p>
              </div>
              <div className="mx-3">
                <img src={Reniewpolicy} className="mobile_image_position" />
                <p>Renew Policy</p>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="mobile_comprehensive_container">
        <div className="mobile_comprehensive_data">
          <div class=" d-flex ">
            <div class="my-3 ">
              <div class="card mobile_container_accident">
                <div class="card-body">
                  <div className="d-flex justify-content-between">
                    <div className="mobile_comprehensive_text">
                      <h5>Comprehensive</h5>
                    </div>
                    <div>
                      <img src={Accidentcar} />
                    </div>
                  </div>
                  <div className="mobile_responsive_data">
                    <h4>Fully Covered</h4>
                    <p>
                      Covers both third-party liabilities and damages to your
                      own car as well.
                    </p>
                    <div className="mt-5 mobile_available_products">
                      <h4>Available Products</h4>
                      <div className="d-flex justify-content-between">
                        <div className="d-flex">
                          <div>
                            <img src={Availableproducts} />
                          </div>
                          <div className="my-2 mx-2 mobile_sanad">
                            <h4>Sanad </h4>
                          </div>
                        </div>
                        <div className="">
                          <a
                            className="btn"
                            style={{
                              backgroundColor: "#F47B20",
                              color: "white",
                            }}
                          >
                            Buy Now
                          </a>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="my-3 mx-3">
              <div class="card mobile_third_accident">
                <div class="card-body">
                  <div className="d-flex justify-content-between">
                    <div className="mobile_comprehensive_text">
                      <h5>Third Party</h5>
                    </div>
                    <div>
                      <img src={Accidentcar} />
                    </div>
                  </div>
                  <div className="mobile_responsive_data ">
                    <h4>Only Third-Party Covered</h4>
                    <p>Damages caused to a third-party vehicle covered.</p>
                    <div className="mobile_available_products">
                      <div className="mt-5 ">
                        <h4>Available Products</h4>
                        <div className="d-flex justify-content-between">
                          <div className="d-flex ">
                            <div>
                              <img src={VehicleDash} />
                            </div>
                            <div className="my-1 mx-2">
                              <h4>Sanad</h4>
                            </div>
                          </div>
                          <div>
                            <a
                              href="#"
                              class="btn "
                              style={{
                                backgroundColor: "#F47B20",
                                color: "white",
                              }}
                            >
                              Insure Now
                            </a>
                          </div>
                        </div>
                      </div>
                      <div>
                        <div className="d-flex justify-content-between pt-2">
                          <div className="d-flex">
                            <div>
                              <img src={VehicleDash} />
                            </div>
                            <div className="my-1 mx-2">
                              <h4>Al Shamel</h4>
                            </div>
                          </div>
                          <div>
                            <a
                              href="#"
                              class="btn "
                              style={{
                                backgroundColor: "#F47B20",
                                color: "white",
                              }}
                            >
                              Insure Now
                            </a>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="mobile_perks">
        <h4>Check out all the perks you get</h4>
      </div>
      <MobileMotorTable type={"1"} />
      <div className="mobile_additional">
        <h4>Additional Extensions of the Program</h4>
        <div>
          <div className="mobile_maintenance">
            <div className="ms-2 mt-2 ">
              <div className="mobile_desc_text ms-3 mx-2">
                <img src={Geographic} className="mobile_image_position" />
                <p className=" mt-2 mb-0 fw-bold mobile_maintenance_display">
                  Waiver of Depreciation Clause
                </p>
              </div>
            </div>
            <div className="ms-2 mt-2">
              <div className=""></div>
              <div className="mobile_desc_text ms-3 mx-2">
                <img src={Hire} className="mobile_image_position" />
                <p className=" mt-2 mb-0  fw-bold mobile_maintenance_display">
                  Hire Care <br />
                  Facility
                </p>
              </div>
            </div>
            <div className="ms-2 mt-2">
              <div className=""></div>
              <div className="mobile_desc_text ms-3 mx-2">
                <img src={Extension} className="mobile_image_position" />
                <p className=" mt-2 mb-0   fw-bold mobile_maintenance_display">
                  Geographical Extension
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="mobile_breakdown">
        <h4>Mechanical Breakdown Insurance</h4>
        <MobileMotorTable type={"2"} />
        <div className="search_box_style my-3">
          <p>What you're looking for?</p>
          <div>
            <img src={search} />
          </div>
        </div>
        <InsuranceCardMobile
          heathInsureCardData={insuranceCardData}
          isArrow={true}
        />
        <div className="view_all_products justify-content-start">
          <label>View All Products & Services</label>
          <img src={right_arrow} alt="Arrow" />
        </div>
      </div>
      <div className="mobile_container_questions">
        <h4>You’ve got questions, we’ve got answers</h4>
        <p>
          Review answers to commonly asked questions at Tawuniya, which enable
          you to be directly involved in improving our support experience.
        </p>
        <CommonFaq faqList={customerServiceFaqList} />
      </div>
      <div className="view_all_products justify-content-start">
        <label>View All Questions</label>
        <img src={right_arrow} alt="Arrow" />
      </div>
      <div className="mobile_container_products">
        <h4>You may also like our Other Products</h4>
        <p>We provide the best and trusted service for our customers</p>
      </div>
      <div className="d-flex mobile_services_slider ">
        {services.map((services, index) => (
          <div key="id" className="mobile_services_card">
            <img className="" src={services.image} />
            <h5>{services.title}</h5>
            <p>{services.description}</p>
            <h4>{services.Data}</h4>
            <ul>
              <li>{services.list}</li>
              <li>{services.list1}</li>
            </ul>
          </div>
        ))}
        <div></div>
      </div>
    </div>
  );
};

export default MotorMobile;
