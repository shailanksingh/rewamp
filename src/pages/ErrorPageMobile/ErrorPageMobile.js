import React from "react";
import ErrorPage from "component/ErrorPage";

const ErrorPageMobile = () => {
  return (
    <div>
      {window.innerWidth > 600 ? <div>ErrorPageMobile </div> : <ErrorPage />}
    </div>
  );
};

export default ErrorPageMobile;
