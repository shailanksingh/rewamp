import React from "react";
import ApprovalRequestList from "component/ApprovalRequestList";

const ApprovalRequestListPage = () => {
  return <ApprovalRequestList />;
};

export default ApprovalRequestListPage;
