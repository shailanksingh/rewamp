import React from 'react';

const PillButton = ({item, active, setActive}) => {
  return (
    <div className={`${item.id === active ? "pr-4" : " "} pillBtnContainer`}>
      {item.hidePill &&
          <div className={`${item.id === active ? "highlightPill" : "normalPill"} d-flex flex-row`} onClick={() => setActive(item.id)}>
            <div>
              <img
                src={
                  item.id === active
                    ? item.pillIconHighlight
                    : item.pillIcon
                }
                className={`${
                  item.id === active
                } img-fluid pillIcon`}
                alt="pillicon"
                style={{
                  width: "fit-content",
                  height: "fit-content",
                }}
              />
            </div>
            <div>
              <p
                className={`${
                  item.id === active
                    ? "pillHighlightText fs-16 fw-800"
                    : "pillNormalText fs-16 fw-400"
                } text-uppercase`}
              >
                {item.pillName}
              </p>
            </div>
          </div>}
    </div>
  )
}

export default PillButton;
