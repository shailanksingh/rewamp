import instance from "./instance";

export const callHelloApi = async () => {
	return instance.get("/hello");
};
