export const languageTranslate = {
	languageArab: "LANGUAGE_ARAB",
	layoutPosition: "LAYOUT_POSITION",
	requestedService: "REQUESTED_SERVICE",
	language: "LANGUAGE",
	updateLayout: "UPDATE_LAYOUT"
};

export const policyInfo = {
	policyInfo: "SET_POLICY_INFO",
};

export const medicalPolicyDetails = {
	medicalPolicyDetails: "SET_MEDICAL_POLICY_DETAILS",
};

export const travelPolicyDetails = {
	travelPolicyDetails: "SET_TRAVEL_POLICY_DETAILS",
};

export const motorPolicyDetails = {
	motorPolicyDetails: "SET_MOTOR_POLICY_DETAILS",
};

export const serviceDetails = {
	serviceDetails: "SET_SERVICE_DETAILS",
};

export const medApprovalDetails = {
	medApprovalDetails: "SET_MED_APPROVAL_DETAILS",
};

export const accidentDetail = {
	accidentDetail: "SET_MED_ACCIDENT_DETAILS",
};

export const renewalPolicyDetail = {
	renewalPolicyDetail: "SET_MED_RENEWAL_POLICY_DETAILS",
};

export const loginResponse = {
	loginResponse: "SET_LOGIN_RESPONSE",
};

export const newsUpdatesDetails = {
	newsUpdatesDetails: "NEWS_UPDATES_DETAILS",
}