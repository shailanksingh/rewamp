import instance from "service/instance";
import * as utils from "./dashboardUtils";
import axios from "axios";

export const getPolicyInformation = async () => {
	try {
		const response = await instance.post(
			"http://localhost:8080/api/postLogin/getPolicyInformation",
			utils.getPolicyInformationRequest()
		);

		return await utils.getPolicyInformationResponse(response);
	} catch (err) {
		return Promise.reject("error");
	}
};

export const getMotorPolicyDetails = async (element) => {
  try {
    const response = await instance.post(
      "http://localhost:8080/api/postLogin/getMotorPolicyDetails",
      utils.getMotorPolicyDetailsRequest(element)
    );

    return await utils.getMotorPolicyResponse(response);
  } catch (err) {
    return Promise.reject("error");
  }
};

export const getMedicalPolicyDetails = async () => {
	try {
		const response = await instance.post(
			"http://localhost:8080/api/postLogin/getMedicalPolicyDetails",
			utils.getMedicalPolicyDetailsRequest()
		);

		return await utils.getMedicalPolicyDetailsResponse(response);
	} catch (err) {
		return Promise.reject("error");
	}
};

export const getTravelPolicyDetails = async () => {
	try {
		const response = await instance.post(
			"http://localhost:8080/api/postLogin/getTravelPolicyDetails",
			utils.getTravelPolicyDetailsRequest()
		);

		return await utils.getTravelPolicyDetailsResponse(response);
	} catch (err) {
		return Promise.reject("error");
	}
};

export const getServicesDetails = async () => {
	try {
		const response = await instance.post(
			"http://localhost:8080/api/vasMotor/servicesDetails",
			utils.servicesDetailsRequest()
		);

		return await utils.servicesDetailsResponse(response);
	} catch (err) {
		return Promise.reject("error");
	}
};

export const getMedApprovalDetails = async (element) => {
  try {
    const response = await instance.post(
      "http://localhost:8080/api/postLogin/getMedApprovalDetails",
      utils.getMedApprovalDetailsRequest(element)
    );

    return await utils.getMedApprovalDetailsResponse(response);
  } catch (err) {
    return Promise.reject("error");
  }
};

export const getRenewalPolicyDetail = async () => {
  try {
    const response = await instance.post(
      "http://localhost:8080/api/motorRetail/getRenewalList",
      utils.getRenewalPolicyDetailRequest()
    );

    return await utils.getRenewalPolicyDetailResponse(response);
  } catch (err) {
    return Promise.reject("error");
  }
};

export const getAccidentDetail = async (element) => {
  try {
    const response = await instance.post(
      "http://localhost:8080/api/motorCOClaimSubmission/accidentList",
      utils.getAccidentDetailRequest(element)
    );

    return await utils.getAccidentDetailResponse(response);
  } catch (err) {
    return Promise.reject("error");
  }
};

// gets data from liferay server
export const getNewsUpdatesDetails = async () => {
  // 
  const sampleData = [{
    "contentFields": [
        {
            "contentFieldValue": {
                "data": "News"
            },
            "name": "Category"
        },
        {
            "contentFieldValue": {
                "data": "Yes"
            },
            "name": "Featured"
        },
        {
            "contentFieldValue": {
                "image": {
                    "contentUrl": "/documents/20121/0/icon.png/d68e4723-17f9-354e-897f-39542741f922?t=1655896178518"
                }
            },
            "name": "Image"
        },
        {
            "contentFieldValue": {
                "data": "Lorem Ipsum has been the industry's standard dummy text ever since the 1500s 1"
            },
            "name": "Title"
        },
        {
            "contentFieldValue": {
                "data": "There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc."
            },
            "name": "Description"
        }
    ],
    "dateCreated": "2022-07-26T09:53:07Z"
},
{
    "contentFields": [
        {
            "contentFieldValue": {
                "data": "Events"
            },
            "name": "Category"
        },
        {
            "contentFieldValue": {
                "data": "Yes"
            },
            "name": "Featured"
        },
        {
            "contentFieldValue": {
                "image": {
                    "contentUrl": "/documents/20121/0/add.png/59af3787-c102-2476-3a4c-4452cc3c11c4?t=1655965125444"
                }
            },
            "name": "Image"
        },
        {
            "contentFieldValue": {
                "data": "Contrary to popular belief, Lorem Ipsum is not simply random text 1"
            },
            "name": "Title"
        },
        {
            "contentFieldValue": {
                "data": "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum 2"
            },
            "name": "Description"
        }
    ],
    "dateCreated": "2022-07-26T09:56:12Z"
},
{
  "contentFields": [
      {
          "contentFieldValue": {
              "data": "News"
          },
          "name": "Category"
      },
      {
          "contentFieldValue": {
              "data": "No"
          },
          "name": "Featured"
      },
      {
          "contentFieldValue": {
              "image": {
                  "contentUrl": "/documents/20121/0/icon.png/d68e4723-17f9-354e-897f-39542741f922?t=1655896178518"
              }
          },
          "name": "Image"
      },
      {
          "contentFieldValue": {
              "data": "Lorem Ipsum has been the industry's standard dummy text ever since the 1500s 3"
          },
          "name": "Title"
      },
      {
          "contentFieldValue": {
              "data": "There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc."
          },
          "name": "Description"
      }
  ],
  "dateCreated": "2021-08-24T09:53:07Z"
},
{
  "contentFields": [
      {
          "contentFieldValue": {
              "data": "Promotions"
          },
          "name": "Category"
      },
      {
          "contentFieldValue": {
              "data": "No"
          },
          "name": "Featured"
      },
      {
          "contentFieldValue": {
              "image": {
                  "contentUrl": "/documents/20121/0/add.png/59af3787-c102-2476-3a4c-4452cc3c11c4?t=1655965125444"
              }
          },
          "name": "Image"
      },
      {
          "contentFieldValue": {
              "data": "Contrary to popular belief, Lorem Ipsum is not simply random text 2"
          },
          "name": "Title"
      },
      {
          "contentFieldValue": {
              "data": "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum"
          },
          "name": "Description"
      }
  ],
  "dateCreated": "2022-07-20T09:56:12Z"
},
{
  "contentFields": [
      {
          "contentFieldValue": {
              "data": "Promotions"
          },
          "name": "Category"
      },
      {
          "contentFieldValue": {
              "data": "Yes"
          },
          "name": "Featured"
      },
      {
          "contentFieldValue": {
              "image": {
                  "contentUrl": "/documents/20121/0/icon.png/d68e4723-17f9-354e-897f-39542741f922?t=1655896178518"
              }
          },
          "name": "Image"
      },
      {
          "contentFieldValue": {
              "data": "Lorem Ipsum has been the industry's standard dummy text ever since the 1500s 4"
          },
          "name": "Title"
      },
      {
          "contentFieldValue": {
              "data": "There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc."
          },
          "name": "Description"
      }
  ],
  "dateCreated": "2022-07-16T09:53:07Z"
},
{
  "contentFields": [
      {
          "contentFieldValue": {
              "data": "Promotions"
          },
          "name": "Category"
      },
      {
          "contentFieldValue": {
              "data": "No"
          },
          "name": "Featured"
      },
      {
          "contentFieldValue": {
              "image": {
                  "contentUrl": "/documents/20121/0/add.png/59af3787-c102-2476-3a4c-4452cc3c11c4?t=1655965125444"
              }
          },
          "name": "Image"
      },
      {
          "contentFieldValue": {
              "data": "Contrary to popular belief, Lorem Ipsum is not simply random text 3"
          },
          "name": "Title"
      },
      {
          "contentFieldValue": {
              "data": "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum"
          },
          "name": "Description"
      }
  ],
  "dateCreated": "2022-07-06T09:56:12Z"
},
{
  "contentFields": [
      {
          "contentFieldValue": {
              "data": "News"
          },
          "name": "Category"
      },
      {
          "contentFieldValue": {
              "data": "No"
          },
          "name": "Featured"
      },
      {
          "contentFieldValue": {
              "image": {
                  "contentUrl": "/documents/20121/0/icon.png/d68e4723-17f9-354e-897f-39542741f922?t=1655896178518"
              }
          },
          "name": "Image"
      },
      {
          "contentFieldValue": {
              "data": "Lorem Ipsum has been the industry's standard dummy text ever since the 1500s 5"
          },
          "name": "Title"
      },
      {
          "contentFieldValue": {
              "data": "There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc."
          },
          "name": "Description"
      }
  ],
  "dateCreated": "2022-06-26T09:53:07Z"
},
{
  "contentFields": [
      {
          "contentFieldValue": {
              "data": "Events"
          },
          "name": "Category"
      },
      {
          "contentFieldValue": {
              "data": "No"
          },
          "name": "Featured"
      },
      {
          "contentFieldValue": {
              "image": {
                  "contentUrl": "/documents/20121/0/add.png/59af3787-c102-2476-3a4c-4452cc3c11c4?t=1655965125444"
              }
          },
          "name": "Image"
      },
      {
          "contentFieldValue": {
              "data": "Contrary to popular belief, Lorem Ipsum is not simply random text 4"
          },
          "name": "Title"
      },
      {
          "contentFieldValue": {
              "data": "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum"
          },
          "name": "Description"
      }
  ],
  "dateCreated": "2022-05-26T09:56:12Z"
},
{
  "contentFields": [
      {
          "contentFieldValue": {
              "data": "News"
          },
          "name": "Category"
      },
      {
          "contentFieldValue": {
              "data": "Yes"
          },
          "name": "Featured"
      },
      {
          "contentFieldValue": {
              "image": {
                  "contentUrl": "/documents/20121/0/icon.png/d68e4723-17f9-354e-897f-39542741f922?t=1655896178518"
              }
          },
          "name": "Image"
      },
      {
          "contentFieldValue": {
              "data": "Lorem Ipsum has been the industry's standard dummy text ever since the 1500s 6"
          },
          "name": "Title"
      },
      {
          "contentFieldValue": {
              "data": "There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc."
          },
          "name": "Description"
      }
  ],
  "dateCreated": "2022-04-26T09:53:07Z"
},
{
  "contentFields": [
      {
          "contentFieldValue": {
              "data": "Events"
          },
          "name": "Category"
      },
      {
          "contentFieldValue": {
              "data": "Yes"
          },
          "name": "Featured"
      },
      {
          "contentFieldValue": {
              "image": {
                  "contentUrl": "/documents/20121/0/add.png/59af3787-c102-2476-3a4c-4452cc3c11c4?t=1655965125444"
              }
          },
          "name": "Image"
      },
      {
          "contentFieldValue": {
              "data": "Contrary to popular belief, Lorem Ipsum is not simply random text 5"
          },
          "name": "Title"
      },
      {
          "contentFieldValue": {
              "data": "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum"
          },
          "name": "Description"
      }
  ],
  "dateCreated": "2022-06-06T09:56:12Z"
},
{
  "contentFields": [
      {
          "contentFieldValue": {
              "data": "News"
          },
          "name": "Category"
      },
      {
          "contentFieldValue": {
              "data": "No"
          },
          "name": "Featured"
      },
      {
          "contentFieldValue": {
              "image": {
                  "contentUrl": "/documents/20121/0/icon.png/d68e4723-17f9-354e-897f-39542741f922?t=1655896178518"
              }
          },
          "name": "Image"
      },
      {
          "contentFieldValue": {
              "data": "Lorem Ipsum has been the industry's standard dummy text ever since the 1500s 7"
          },
          "name": "Title"
      },
      {
          "contentFieldValue": {
              "data": "There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc."
          },
          "name": "Description"
      }
  ],
  "dateCreated": "2022-06-26T09:53:07Z"
},
{
  "contentFields": [
      {
          "contentFieldValue": {
              "data": "Events"
          },
          "name": "Category"
      },
      {
          "contentFieldValue": {
              "data": "Yes"
          },
          "name": "Featured"
      },
      {
          "contentFieldValue": {
              "image": {
                  "contentUrl": "/documents/20121/0/add.png/59af3787-c102-2476-3a4c-4452cc3c11c4?t=1655965125444"
              }
          },
          "name": "Image"
      },
      {
          "contentFieldValue": {
              "data": "Contrary to popular belief, Lorem Ipsum is not simply random text 6"
          },
          "name": "Title"
      },
      {
          "contentFieldValue": {
              "data": "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum"
          },
          "name": "Description"
      }
  ],
  "dateCreated": "2022-05-16T09:56:12Z"
},
{
  "contentFields": [
      {
          "contentFieldValue": {
              "data": "News"
          },
          "name": "Category"
      },
      {
          "contentFieldValue": {
              "data": "Yes"
          },
          "name": "Featured"
      },
      {
          "contentFieldValue": {
              "image": {
                  "contentUrl": "/documents/20121/0/icon.png/d68e4723-17f9-354e-897f-39542741f922?t=1655896178518"
              }
          },
          "name": "Image"
      },
      {
          "contentFieldValue": {
              "data": "Lorem Ipsum has been the industry's standard dummy text ever since the 1500s 8"
          },
          "name": "Title"
      },
      {
          "contentFieldValue": {
              "data": "There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc."
          },
          "name": "Description"
      }
  ],
  "dateCreated": "2022-02-26T09:53:07Z"
},
{
  "contentFields": [
      {
          "contentFieldValue": {
              "data": "Events"
          },
          "name": "Category"
      },
      {
          "contentFieldValue": {
              "data": "Yes"
          },
          "name": "Featured"
      },
      {
          "contentFieldValue": {
              "image": {
                  "contentUrl": "/documents/20121/0/add.png/59af3787-c102-2476-3a4c-4452cc3c11c4?t=1655965125444"
              }
          },
          "name": "Image"
      },
      {
          "contentFieldValue": {
              "data": "Contrary to popular belief, Lorem Ipsum is not simply random text 7"
          },
          "name": "Title"
      },
      {
          "contentFieldValue": {
              "data": "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum"
          },
          "name": "Description"
      }
  ],
  "dateCreated": "2022-03-23T09:56:12Z"
}]
  // 
  try {
    // const { data } = await axios.get("http://localhost:9090/o/headless-delivery/v1.0/content-structures/43120/structured-contents?fields=dateCreated,contentFields.contentFieldValue.data,contentFields.name,contentFields.contentFieldValue.image.contentUrl",
    // {
    //   auth: {
    //     username: 'test@liferay.com',
    //     password: 'test',
    //   },
    //   headers: {
    //     "Access-Control-Allow-Origin": "*",
    //     // 'Access-Control-Allow-Methods':'GET,PUT,POST,DELETE,PATCH,OPTIONS',
    //     // Authorization: `Basic ${{
    //     //   Username: "test@liferay.com",
    //     //   Password: "test"
    //     // }}`,
    //     "Content-Type": 'application/x-www-form-urlencoded; charset=UTF-8;application/json',
    //   },
    //   // mode: 'no-cors',
    // })

    // fetch('http://localhost:9090/o/headless-delivery/v1.0/content-structures/43120/structured-contents?fields=dateCreated,contentFields.contentFieldValue.data,contentFields.name,contentFields.contentFieldValue.image.contentUrl',
    // {
    //   method: 'GET',
    //   headers: {
    //     "Access-Control-Allow-Origin": "*",
    //     Authorization: `Basic ${{
    //       Username: "test@liferay.com",
    //       Password: "test"
    //     }}`,
    //     "Content-Type": "application/x-www-form-urlencoded; charset=UTF-8",
    //   }
    // }).then(res => res.json()).then(data => console.log('HEMI >>> DATA', data)).catch(err => console.log('HEMI ERROR >>>', err))
    
    const resData = utils.getFormatedDataFromLiferayRes(sampleData)
    return resData;
  } catch (error) {
    return Promise.reject("error");
  }
}