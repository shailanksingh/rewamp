export const getPolicyInformationRequest = () => {
	return {
		langCode: "E",
	};
};

export const getPolicyInformationResponse = (response) => {
	return response.data;
};

export const getMotorPolicyDetailsRequest = (element) => {
	// 	let cleanedJson = JSON.parse(JSON.stringify(data));
	//   delete cleanedJson.accidentSource;
	//   delete cleanedJson.acceptTerms;
	return {
		langCode: "E",
		policyNumber: element.SPolicyNo,
		customerId: "6592985",
	};
};

export const getMotorPolicyResponse = (response) => {
	return response.data;
};

export const getMedicalPolicyDetailsRequest = () => {
	return {
		langCode: "E",
		policyNumber: "5569",
		customerId: "6592985",
		medicalCustomerId: "2249",
		idNumber: "2065946879",
	};
};

export const getMedicalPolicyDetailsResponse = (response) => {
	return {
		resultCode: "0",
		medicalMemberList: [
			{
				TDOB: "1990-07-01T12:00:00+0300",
				SGender: "F",
				SMemberCode: "002065946879001",
				SEndtNo: "0",
				SRelation: "Employee",
				SEmpId: "2212791",
				SRiskNo: "1",
				SCCHIStatus: "Accepted",
				SType: "1",
				TInceptionDate: "2022-01-01T12:00:00+0300",
				SSponsorID: "7000911508",
				SMemberName: "VICTORIA JACKSON",
				SClassNo: "1",
				DPremium: "0.0",
			},
			{
				TDOB: "2004-06-28T12:00:00+0300",
				SGender: "F",
				SMemberCode: "002065946879201",
				SEndtNo: "0",
				SRelation: "Child",
				SEmpId: "2212791",
				SRiskNo: "1",
				SCCHIStatus: "Accepted",
				SType: "3",
				TInceptionDate: "2022-01-01T12:00:00+0300",
				SSponsorID: "7000911508",
				SMemberName: "VALERIE CLARK",
				SClassNo: "1",
				DPremium: "0.0",
			},
			{
				TDOB: "2012-08-16T12:00:00+0300",
				SGender: "M",
				SMemberCode: "002065946879211",
				SEndtNo: "0",
				SRelation: "Child",
				SEmpId: "2212791",
				SRiskNo: "1",
				SCCHIStatus: "Accepted",
				SType: "3",
				TInceptionDate: "2022-01-01T12:00:00+0300",
				SSponsorID: "7000911508",
				SMemberName: "MISS JENNIFER BRYAN",
				SClassNo: "1",
				DPremium: "0.0",
			},
		],
		resultDescription: "Successfully Login to Tawuniya Portal",
	};
};

export const getTravelPolicyDetailsRequest = () => {
	return {
		customerId: "6592985",
		langCode: "E",
		lob: "TR",
		policyNumber: "18342440",
	};
};

export const getTravelPolicyDetailsResponse = (response) => {
	return response.data;
};

export const servicesDetailsRequest = () => {
	return {
		serviceCode: "1",
		lang: "E",
		channel: "Mobile",
		policyNo: "2345676545",
	};
};

export const servicesDetailsResponse = (response) => {
	return response.data;
};

export const getMedApprovalDetailsRequest = (element) => {
	return {
		cardCode: "001052962055001",
		customerId: "6592985",
		dateFrom: "2021-12-10",
		dateTo: "2022-06-10",
		idNumber: "2065946879",
		medicalCustomerId: "",
		medicalPolicyNumber: element.SPolicyNo,
		langCode: "E",
	};
};

export const getMedApprovalDetailsResponse = (response) => {
	return {
		medApprovalList: [
			{
				TRequestDate: "2022-04-03T12:00:00+0300",
				SStatus: "Conditional Approved",
				SCardNo: "002065946879001",
				SPolicyNo: "5569",
				SProviderName: "XXXX",
				TProcessedDate: "2022-04-03T12:00:00+0300",
				DAmount: "340",
				SApprovalNo: "50016829",
				SMemberName: "Victoria Jackson",
			},
			{
				TRequestDate: "2022-04-03T12:00:00+0300",
				SStatus: "Conditional Approved",
				SCardNo: "002065946879201",
				SPolicyNo: "5569",
				SProviderName: "XXXX",
				TProcessedDate: "2022-04-03T12:00:00+0300",
				DAmount: "300",
				SApprovalNo: "50016618",
				SMemberName: "Valerie Clark",
			},
		],
		resultCode: "0",
		resultDescription: "Successfully Login to Tawuniya Portal",
	};
};

export const getRenewalPolicyDetailRequest = () => {
	return {
		languageCode: "EN",
		idNumber: "2048519835",
		yob: "2000",
	};
};

export const getRenewalPolicyDetailResponse = (response) => {
	return response.data;
};

export const getAccidentDetailRequest = (element) => {
	return {
		policynumber: element.SPolicyNo,
		language: "A",
	};
};

export const getAccidentDetailResponse = (response) => {
	return {
		accidentList: [
			{
				policynumber: "14913837",

				referencenumber: "RD040118760",

				source: null,

				accidentdate: "2018-01-04 14:05:00.0",

				plateno: "ح ص ر 6836",

				responsibility: "100",

				idnumber: null,

				make: null,

				model: "1",
			},
		],

		resultCode: "S",

		resultMessage: "Sucess",

		status: "S",
	};
};

export const getFormatedDataFromLiferayRes = (data = []) => {
	const formatedData = [];
	data.forEach(item => {
		const dataObj = {};
		if(item.dateCreated) {
			dataObj['dateCreated'] = item.dateCreated;
		}
		if (item.contentFields?.length > 0) {
			item.contentFields.forEach(itm => {
				if(itm.name === 'Image') {
					dataObj[itm.name.toLowerCase()] = itm.contentFieldValue.image.contentUrl;
				}else {
					dataObj[itm.name.toLowerCase()] = itm.contentFieldValue.data;
				}
			})
		}

		formatedData.push({ ...dataObj });
	})
	return formatedData
}