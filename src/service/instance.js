import axios from "axios";
import { config } from "../config";

const instance = axios.create({
	baseURL: config.apiUrl,
	// timeout: 1000,
	// headers: {'X-Custom-Header': 'foobar'}
});

export default instance;
