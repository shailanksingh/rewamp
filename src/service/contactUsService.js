import instance from "./instance"

export const getBranchDetails = async () => {
  try {
    const { data } = await instance.post('/preLogin/getBranchInformation', {"date": "01/01/2016 01:01:54"})
    if (data && data.data && data.data.brList) return data.data.brList
    else return []
  } catch (error) {
    return Promise.reject(error)
  }
}