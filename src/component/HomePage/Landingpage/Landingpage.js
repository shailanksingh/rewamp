import React from "react";
import {
	FooterAbout,
	HomeProducts,
	HomeServices,
	HomeServiceImgCard,
} from "../LandingComponent";
import { useSelector } from "react-redux";
import { HomeServicesData } from "../LandingComponent/Schema/HomeServicesData";
import { ThemeProvider, createTheme } from "@material-ui/core";
import "./HomeLanding-MediaQuery.scss";
import { LandingPageMobile } from "./LandingPageMobile";

const theme = createTheme({
	palette: {
		common: {},
		primary: {
			main: "#EE7500",
		},
		secondary: {
			main: "#FFFFFF",
		},
		// redColor: palette.augmentColor({ color: red }),
	},
});

export const Landingpage = () => {
	const changeLayout = useSelector((state) => state.languageReducer.language);

	return (
		<ThemeProvider theme={theme}>
			<React.Fragment>
				{window.innerWidth > 600 ? (
					<>
						<HomeProducts isPillLayout={!changeLayout && true} />
						<HomeServices
							HomeServicesData={HomeServicesData}
							isPillNeeded={true}
						/>
						<HomeServiceImgCard />
						<FooterAbout needFooterIcon={false} />
					</>
				) : (
					<LandingPageMobile />
				)}
			</React.Fragment>
		</ThemeProvider>
	);
};
