import React from "react";
import Vitality from "assets/images/mobile/Vitality.svg";
import Subtract from "assets/images/mobile/Subtract.svg";
import TawuniyaLogo from "assets/images/mobile/Tawuniya-Logo.svg";
import { history } from "service/helpers";

const LandingProductsOfferings = () => {
  return (
    <div className="productsOfferings">
      <div className="te-style">Rewards & More</div>
      <div>
        <section>
          <div class="row product-row">
            <div class="swipercards ">
              <div
                className="card overflow-hiddentext-wrap"
                onClick={() => history.push("/home/tawuniya-vitality")}
              >
                <div className="d-flex align-item-center ">
                  <div className="top">
                    <img src={Vitality} alt="Vitality"></img>
                  </div>
                  <div className="bottom-slide">
                    <h4>
                      Get 30% Off annual <br></br>fitness time gym <br></br>
                      membership
                    </h4>
                  </div>
                </div>
              </div>
              <div
                className="card overflow-hiddentext-wrap one"
                onClick={() => history.push("/home/tawuniya-ithra")}
              >
                <div className="d-flex align-item-center ">
                  <div className="top">
                    <p>Tawuniya</p>
                    <img src={Subtract} alt="Subtract"></img>
                  </div>
                  <div className="bottom-slide">
                    <h4>
                      Get 30% Off annual <br></br>fitness time gym <br></br>
                      membership
                    </h4>
                  </div>
                </div>
              </div>
              <div
                className="card overflow-hiddentext-wrap two"
                onClick={() => history.push("/home/tawuniya-drive")}
              >
                <div className="d-flex align-item-center ">
                  <div className="top">
                    <span>
                      15 <sub>%</sub>
                    </span>
                    <p>Discount</p>
                  </div>
                  <div className="bottom-slide">
                    <h4>
                      Get 30% Off annual <br></br>fitness time gym <br></br>
                      membership
                    </h4>
                  </div>
                </div>
              </div>
              <div
                className="card overflow-hiddentext-wrap three"
                onClick={() => history.push("/home/taj-program")}
              >
                <div className="d-flex align-item-center ">
                  <div className="top">
                    <img src={TawuniyaLogo} alt="TawuniyaLogo"></img>
                  </div>
                  <div className="bottom-slide">
                    <h4>
                      Get 30% Off annual <br></br>fitness time gym <br></br>
                      membership
                    </h4>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>
    </div>
  );
};

export default LandingProductsOfferings;
