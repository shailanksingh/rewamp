import React, { useState } from "react";
import { useHistory } from "react-router-dom";
import FlightDelay from "assets/images/mobile/flightdelay.png";
import Accident from "assets/images/mobile/covid.png";
import Vector from "assets/images/mobile/car-bg.png";
import Shoping from "assets/images/mobile/road.png";
import "./style.scss";

function CardSliderContent() {
  const [datas] = useState([
    {
      id: 1,
      image: Shoping,
      description: "Road Side Assistance",
      checked: false,
    },
    {
      id: 2,
      image: Accident,
      description: "Covid-19 Insurance",
      checked: false,
    },
    {
      id: 3,
      image: Shoping,
      description: "Flight Delay",
      checked: false,
    },
    {
      id: 3,
      image: Accident,
      description: "Emergency",
      checked: false,
    },
  ]);
  const history = useHistory();

  return (
    <section>
      <div class="container-fluid">
        <p className="title-to">What are you up to?</p>
        <div class="row m-0">
          <div class="scrollcards home">
            {datas.map((data, index) => (
              <div key={data.id} className="card overflow-hidden text-wrap ">
                <div className="d-flex align-items-center ">
                  <span className="icon">
                    <img
                      src={data.image}
                      height="10"
                      className="iconn"
                      alt="icon"
                    />
                  </span>
                  <span
                    className="card-desc card_title"
                    onClick={
                      index === 0 ? () => history("/assisstance") : () => {}
                    }
                  >
                    {data.description}
                  </span>
                </div>
              </div>
            ))}
          </div>
        </div>
        <div className="get-insured">
          <p>Insure yourself within minutes!</p>
        </div>
      </div>
    </section>
  );
}

export default CardSliderContent;
