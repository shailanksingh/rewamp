import React, { useState } from "react";

import heartbeaticon from "assets/images/mobile/heartbeaticon.png";
import flighticon from "assets/images/mobile/flighticon.png";
import homewhitecar from "assets/images/mobile/homewhitecar.png";
import Camera from "assets/images/mobile/Camera.png";
import Unionbg from "assets/images/mobile/Unionbg.png";
import "./style.scss";
import PhoneNumberInput from "component/common/PhoneNumberInput";
import { dialingCodes } from "component/common/MockData";
import { NormalSearch } from "component/common/NormalSearch";
import iquamaIcon from "assets/svg/iquamaIcon.svg";
import coronovirus from "assets/images/mobile/coronovirus.png";
import Birthday from "assets/images/mobile/Birthday.png";
import { Tabs, Tab } from "react-bootstrap";


const LandingSectionSelect = () => {
  const [formType, setformType] = useState({
    individual: true,
    sme: false,
    healthindividual: true,
    healthsme: false,
  });
  const [helperFormValues, setHelperFormValues] = useState({ phoneNumber: "" });

  let [selectedCode, setSelectedCode] = useState(dialingCodes[0]);
  const selectForm = (form) => {
    setformType({
      individual: false,
      sme: false,
      healthindividual: false,
      healthsme: false,
      [form]: true,
    });
  };
  const handleInputChange = (e) => {
    const { name, value } = e.target || e || {};
    setHelperFormValues({ ...helperFormValues, [name]: value });
  };
  const { phoneNumber, userId } = helperFormValues;
  return (
    <div className="mx-2 SStyle landingSectionSelect ">
      <img src={coronovirus} alt="MenuDot" className="menu-dot"></img>

      <Tabs
        defaultActiveKey="Motor"
        id="uncontrolled-tab-example"
        className="landing-tab-mobile position-relative"
      >
        <Tab
          eventKey="Motor"
          title={
            <>
              <img src={homewhitecar} height="25" alt="car" /> <span>Motor</span>
            </>
          }
        >
          <div class="tab-content" id="myTabContent">
            <div
              class="tab-pane fade show active"
              id="profile"
              role="tabpanel"
              aria-labelledby="profile-tab"
            >
              <div className="Silver bg-white border-3 p-3  pt-4">
                <div className="card bg-light">
                  <div class="">
                    <div className="fontSize-16-400 flex-space">
                      <button
                        className={
                          "btn btn-light button-size " +
                          (formType.healthindividual
                            ? `active-color`
                            : `button-color`)
                        }
                        onClick={() => selectForm("healthindividual")}
                      >
                        Individuals
                      </button>
                      <button
                        className={
                          "btn btn-light button-sme " +
                          (formType.healthsme ? `active-color` : `button-color`)
                        }
                        onClick={() => selectForm("healthsme")}
                      >
                        SMEs
                      </button>
                    </div>
                  </div>

                  <div>
                    <div className="landing-input">
                      <NormalSearch
                        className="loginInputFieldOne"
                        name="userId"
                        value={userId}
                        placeholder="Saudi ID or Iqama Number"
                        onChange={handleInputChange}
                        needLeftIcon={true}
                        leftIcon={iquamaIcon}
                      />
                      <img
                        src={Camera}
                        width="20px"
                        height="15px"
                        className="imgPos2"
                        alt="icon"
                      />
                    </div>
                    <div className="landing-input">
                      <NormalSearch
                        className="loginInputFieldOne"
                        name="userId"
                        value={userId}
                        placeholder="Year Of Birth"
                        onChange={handleInputChange}
                        needLeftIcon={true}
                        leftIcon={Birthday}
                      />
                    </div>

                    <div class="landing-input">
                      <PhoneNumberInput
                        className="complaintPhoneInput"
                        selectInputClass="complaintSelectInputWidth"
                        selectInputFlexType="complaintSelectFlexType"
                        dialingCodes={dialingCodes}
                        selectedCode={selectedCode}
                        setSelectedCode={setSelectedCode}
                        value={phoneNumber}
                        name="phoneNumber"
                        onChange={handleInputChange}
                        isCodeFalse={true}
                        placeholder="Mobile Number"
                      />
                    </div>

                    <div className="terms">
                      <p>
                        By continuing you give Tawuniya advance consent to
                        obtain my and/or my dependents' information from the
                        National Information Center.
                      </p>
                      <div>
                        <button className="buy-button fs-12 w-100">
                          Buy Now
                        </button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </Tab>
        <Tab
          eventKey="Health"
          title={
            <>
              <img src={heartbeaticon} height="25px" alt="Heart" className="health_hearticon" />
              Health
            </>
          }
        >
          <div class="tab-content" id="myTabContent">
            <div
              class="tab-pane fade show active"
              id="profile"
              role="tabpanel"
              aria-labelledby="profile-tab"
            >
              <div className="Silver bg-white border-3 p-3  pt-4">
                <div className="card bg-light">
                  <div>
                    <div className="landing-input">
                      <NormalSearch
                        className="loginInputFieldOne"
                        name="userId"
                        value={userId}
                        placeholder="Saudi ID or Iqama Number"
                        onChange={handleInputChange}
                        needLeftIcon={true}
                        leftIcon={iquamaIcon}
                      />
                      <img
                        src={Camera}
                        width="20px"
                        height="15px"
                        className="imgPos2"
                        alt="icon"
                      />
                    </div>
                    <div className="landing-input">
                      <NormalSearch
                        className="loginInputFieldOne"
                        name="userId"
                        value={userId}
                        placeholder="Year Of Birth"
                        onChange={handleInputChange}
                        needLeftIcon={true}
                        leftIcon={iquamaIcon}
                      />
                    </div>

                    <div class="landing-input">
                      <PhoneNumberInput
                        className="complaintPhoneInput"
                        selectInputClass="complaintSelectInputWidth"
                        selectInputFlexType="complaintSelectFlexType"
                        dialingCodes={dialingCodes}
                        selectedCode={selectedCode}
                        setSelectedCode={setSelectedCode}
                        value={phoneNumber}
                        name="phoneNumber"
                        onChange={handleInputChange}
                        isCodeFalse={true}
                        placeholder="Mobile Number"
                      />
                    </div>

                    <div className="terms">
                      <p>
                        By continuing you give Tawuniya advance consent to
                        obtain my and/or my dependents' information from the
                        National Information Center.
                      </p>
                      <div>
                        <button className="buy-button fs-12 w-100">
                          Buy Now
                        </button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </Tab>
        <Tab
          eventKey="Travel"
          title={
            <>
              <img src={flighticon} height="25px" alt="Flight" />
              Travel
            </>
          }
        >
          <div className="Silver bg-white border-3 p-3  pt-4">
            <div className="card bg-light">
              <div>
                <div className="landing-input">
                  <NormalSearch
                    className="loginInputFieldOne"
                    name="userId"
                    value={userId}
                    placeholder="Saudi ID or Iqama Number"
                    onChange={handleInputChange}
                    needLeftIcon={true}
                    leftIcon={iquamaIcon}
                  />
                  <img
                    src={Camera}
                    width="20px"
                    height="15px"
                    className="imgPos2"
                    alt="icon"
                  />
                </div>
                <div className="landing-input">
                  <NormalSearch
                    className="loginInputFieldOne"
                    name="userId"
                    value={userId}
                    placeholder="Year Of Birth"
                    onChange={handleInputChange}
                    needLeftIcon={true}
                    leftIcon={iquamaIcon}
                  />
                </div>
                <div class="landing-input">
                  <PhoneNumberInput
                    className="complaintPhoneInput"
                    selectInputClass="complaintSelectInputWidth"
                    selectInputFlexType="complaintSelectFlexType"
                    dialingCodes={dialingCodes}
                    selectedCode={selectedCode}
                    setSelectedCode={setSelectedCode}
                    value={phoneNumber}
                    name="phoneNumber"
                    onChange={handleInputChange}
                    isCodeFalse={true}
                    placeholder="Mobile Number"
                  />
                </div>
                <div className="terms">
                  <p>
                    By continuing you give Tawuniya advance consent to obtain my
                    and/or my dependents' information from the National
                    Information Center.
                  </p>
                  <div>
                    <button className="buy-button fs-12 w-100">Buy Now</button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </Tab>
      </Tabs>
      <img src={Unionbg} className="helper"></img>
    </div>
  );
};

export default LandingSectionSelect;
