import React from "react";
import { ThemeProvider, createTheme } from "@material-ui/core";
import "../HomeLanding-MediaQuery.scss";
import "./searchbox.scss";
import CardSliderContent from "./cardSliderContent";
import LandingHelperSection from "./landingHelperSection";
import LandingProductsOfferings from "./landingProductsOfferings";
import HeaderStickyMenu from "component/common/MobileReuseable/HeaderStickyMenu";
import FooterMobile from "component/common/MobileReuseable/FooterMobile";
import InsureCard from "component/common/InsureCard";
import "../HomeLanding-MediaQuery.scss";
import "./style.scss";
import backTop from "assets/images/mobile/right-arrow.png";
import Vitality from "assets/images/mobile/vitality-logo.png";
import { NormalButton } from "component/common/NormalButton";
const theme = createTheme({
  palette: {
    common: {},
    primary: {
      main: "#EE7500",
    },
    secondary: {
      main: "#FFFFFF",
    },
  },
});

export const LandingPageMobile = () => {
  return (
    <ThemeProvider theme={theme}>
      <div className="position-relative landing-page-mobile">
        <HeaderStickyMenu customClass={true} />
        <div className="BannerContainer_mobile">
          <CardSliderContent />
          <InsureCard />
          <LandingProductsOfferings />
          <div className="do-more-get">
            <h6>Do More, Get More..</h6>
            <p>with Tawuniya unique wellness program</p>
            <img src={Vitality} alt="Vitality"></img>
            <NormalButton
              label="Get Started"
              className="backTopButton mx-auto d-block"
              needBtnPic={true}
              src={backTop}
            />
          </div>
          <LandingHelperSection />
        </div>
      </div>
      <FooterMobile />
    </ThemeProvider>
  );
};
