import React from 'react'
import ArrowRight from "assets/images/mobile/right_arrow.png";

const SearchBoxMobile = () => {
  return (
    <div className='container search_box_mob'>
        <h5>Results</h5>
        <span>Found 18 results for “</span><span className='results_color'>Al shamel</span><span>”</span>
      <div className='mt-4 d-flex justify-content-between'>
        <div>Al Shamel</div>
        <div className='results_color'>Motor Insurance</div>
        </div>
        <hr/> 
        <div className='d-flex justify-content-between'>
        <div>Al Shamel</div>
        <div className='results_color'>360 Motor Insurance</div>
        </div>
        <hr/> <div className='d-flex justify-content-between'>
        <div>Al Shamel</div>
        <div className='results_color'>Products</div>
        </div>
        <hr/> <div className='d-flex justify-content-between'>
        <div>Al Shamel</div>
        <div className='results_color'>Motor Claim</div>
        </div>
        <hr/> <div className='d-flex justify-content-between'>
        <div>Al Shamel</div>
        <div className='results_color'>Sanad Plus</div>
        </div>
        <div className='text-right mt-3'>
          <span className='full_results'>Full Results &nbsp; </span>{""}
          <span className='ms-4'><img  src={ArrowRight} alt="ArrowRight"/></span>
        </div>
        <div className='mb-3'>
          <h6>Popular</h6>
        </div>
        <div className='d-flex'>
        <div className='popular_scroll'>
          Motor Insurance
        </div>
        <div className='popular_scroll'>
          Health Insurance
        </div>
        </div>
    </div>
  )
}

export default SearchBoxMobile;