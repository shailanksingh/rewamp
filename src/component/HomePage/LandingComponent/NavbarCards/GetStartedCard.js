import React from "react";
import { history } from "service/helpers";
import { NormalButton } from "component/common/NormalButton";
import orangeArrow from "assets/svg/orangeArrow.svg";
import arrowLight from "assets/svg/Arrows/ArrowForward.svg";
import flight from "assets/svg/orangeFlight.svg";
import roadCar from "assets/svg/roadCar.svg";
import medikit from "assets/svg/medicalkit.svg";
import fileclaim from "assets/svg/files.svg";
import getstartedpic from "assets/images/getstartedBanner.png";
import "./style.scss";

export const GetStartedCard = ({ isAlign = false }) => {
  const openInNewTab = (url1) => {
    const newWindow = window.open(url1, "_blank", "noopener,noreferrer");
    if (newWindow) newWindow.opener = null;
  };
  const getStartedData = [
    {
      id: 0,
      title: "Time to insure yourself",
      subTitle:
        "Real time rates, organised claims and incraesed road safteyare all yours.Just a few simple qusetion to get started",
      titleClass: "getStartedLiningOne",
      needLinkOne: false,
      needLinkTwo: false,
    },
    {
      id: 1,
      title: "I'm an individual",
      subTitle:
        "Trusted advice and insurance solutions to protect what matters most",
      titleClass: "getStartedLiningOne pt-2",
      linkTitleOne: "See Individual Insurance Options",
      arrowIcon: orangeArrow,
      needLinkOne: true,
      needLinkTwo: false,
    },
    {
      id: 2,
      title: "I'm a Corporate or SME",
      subTitle:
        "Coverage and risk management solutions for comapanies of all sizes",
      titleClass: "getStartedNoLining pt-2",
      linkTitleOne: "See Enterprise Insurance",
      linkTitleTwo: "See SME Insurance",
      arrowIcon: orangeArrow,
      needLinkOne: true,
      needLinkTwo: true,
    },
  ];

  const serviceStartedData = [
    {
      id: 0,
      icon: roadCar,
      serviceTitle: "Road Side Assisstance",
    },
    {
      id: 1,
      icon: medikit,
      serviceTitle: "Request Telemedicine",
    },
    {
      id: 2,
      icon: flight,
      serviceTitle: "Flight Delay",
    },
    {
      id: 3,
      icon: fileclaim,
      serviceTitle: "Track A Claim",
      urlTrue: true,
      url: "https://beta.tawuniya.com.sa/web/#/assistamerica",
    },
  ];

  return (
    // getstarted card container starts here
    <div
      className={`${
        isAlign
          ? "card-GetStarted-Layout-forInsurance"
          : "card-GetStarted-Layout"
      } getStartedContainer`}
    >
      <div className="row">
        <div className="getStartedLayerOne col-lg-7 col-12 p-4">
          {getStartedData.map((item, index) => {
            return (
              <div className={item.titleClass} key={index}>
                <p className="fs-28 fw-800 m-0 pb-2 getStartedTitle">
                  {item.title}
                </p>
                <p className="fs-16 fw-400 m-0 pb-2 getStartedPara">
                  {item.subTitle}
                </p>
                {item.needLinkOne && (
                  <p className="fs-16 fw-800 linkTextOne m-0 pb-2">
                    {item.linkTitleOne}
                    <img
                      src={item.arrowIcon}
                      className="img-fluid arrowSize pl-2"
                      alt={item.arrowIcon}
                    />
                  </p>
                )}
                {item.needLinkTwo && (
                  <p className="fs-16 fw-800 linkTextTwo">
                    {item.linkTitleTwo}
                    <img
                      src={item.arrowIcon}
                      className="img-fluid arrowSize pl-2"
                      alt={item.arrowIcon}
                    />{" "}
                  </p>
                )}
              </div>
            );
          })}
        </div>
        <div className="getStartedLayerTwo col-lg-5 col-12 p-4">
          <div className="emergencyLining pb-3">
            <img src={getstartedpic} className="img-fluid" alt="bannerPic" />
          </div>
          <div className="pt-3 pb-4">
            <NormalButton
              label="My Account"
              className="myAccountBtn p-2 px-3"
              needBtnPic={true}
              adjustIcon="pl-2"
              src={arrowLight}
              onClick={() => history.push("/login")}
            />
          </div>
          {serviceStartedData.map((item, index) => {
            let { icon, serviceTitle } = item;
            return (
              <p
                className="fs-14 fw-400 m-0 pb-1 getStartedPara"
                key={index}
                onClick={() => (item.urlTrue ? openInNewTab(item.url) : "")}
              >
                <img
                  src={icon}
                  className="img-fluid serviceIconSize pr-2"
                  alt="icon"
                />{" "}
                {serviceTitle}
              </p>
            );
          })}
        </div>
      </div>
    </div>
    // getstarted card container ends here
  );
};
