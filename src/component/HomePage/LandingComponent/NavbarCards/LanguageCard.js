import React, { useState } from "react";
import { CommonCard } from "component/common/CommonCard";
import flag1 from "assets/svg/arab.svg";
import flag2 from "assets/svg/english.svg";
import flag3 from "assets/svg/turkey.svg";
import flag4 from "assets/svg/french.svg";
import flag5 from "assets/svg/urdu.svg";
import flag6 from "assets/svg/indo.svg";
import "./style.scss";
import { useDispatch, useSelector } from "react-redux";
import { updateLanguage } from "action/LanguageAct";

export const LanguageCard = ({
  arabToggle,
  translateHandler,
  isAlign = false,
}) => {
  const cardData = [
    {
      id: 0,
      cardTitle: "Arabic",
      cardPara: "عدّةو",
      cardFlag: flag1,
      needTitle: true,
      needTranslate: arabToggle,
      needToggle: translateHandler,
      higlightPill: "suggested-highlight-language-cards",
      normalPill: "suggested-language-cards",
    },
    {
      id: 1,
      cardTitle: "English",
      cardPara: "Worldwide",
      cardFlag: flag2,
      needTitle: false,
      needTranslate: null,
      higlightPill: "suggested-highlight-language-cards",
      normalPill: "suggested-language-cards",
    },
    {
      id: 2,
      cardTitle: "Turkish",
      cardPara: "عدّةو",
      cardFlag: flag3,
      needTitle: false,
      needTranslate: null,
      higlightPill: "suggested-highlight-language-cards",
      normalPill: "suggested-language-cards",
    },
    {
      id: 3,
      cardTitle: "French",
      cardPara: "عدّةو",
      cardFlag: flag4,
      needTitle: false,
      needTranslate: null,
      higlightPill: "suggested-highlight-language-cards",
      normalPill: "suggested-language-cards",
    },
    {
      id: 4,
      cardTitle: "Urdu",
      cardPara: "عدّةو",
      cardFlag: flag5,
      needTitle: false,
      needTranslate: null,
      higlightPill: "suggested-highlight-language-cards",
      normalPill: "suggested-language-cards",
    },
    {
      id: 5,
      cardTitle: "Indonesian",
      cardPara: "عدّةو",
      cardFlag: flag6,
      needTitle: false,
      needTranslate: null,
      higlightPill: "suggested-highlight-language-cards",
      normalPill: "suggested-language-cards",
    },
  ];
  const dispatch = useDispatch();
  const [pillIndex, setPillIndex] = useState(0);
  const selectedLanguage = useSelector((data) => data.languageReducer.language);

  const changeLanguage = (language) => {
    if (selectedLanguage !== language) {
      dispatch(updateLanguage(language));
    }
  };

  return (
    // language container starts here
    <div className="language-card-container">
      <CommonCard
        width="400px"
        cardPosition={
          isAlign ? "card-Language-Layout-forInsurance" : "card-Language-Layout"
        }
      >
        <p className="fs-16 fw-800 language-card-container-heading">
          Suggested language
        </p>
        <div className="row">
          {cardData.map((item, index) => {
            return (
              <div className="col-6" key={index}>
                <div
                  className={`${
                    item.id === pillIndex ? item.higlightPill : item.normalPill
                  } d-flex justify-content-between mb-3`}
                  onClick={() => {
                    changeLanguage(item.cardTitle.toLowerCase());
                    setPillIndex(item.id);
                  }}
                >
                  <div>
                    <p
                      className="fs-14 fw-700 p-0 m-0 language-Name"
                      onClick={item.id === pillIndex && item.needToggle}
                    >
                      {item.cardTitle}
                    </p>
                    <p className="fs-12 fw-400 m-0 p-0">{item.cardPara}</p>
                  </div>
                  <div>
                    <img
                      src={item.cardFlag}
                      className="img-fluid"
                      alt="flags"
                    />
                  </div>
                </div>
                {item.needTitle && (
                  <p className="fs-16 fw-800">Choose a language</p>
                )}
              </div>
            );
          })}
        </div>
      </CommonCard>
    </div>
    // language conatiner ends here
  );
};
