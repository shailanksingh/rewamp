import React from "react";
import { CommonCard } from "component/common/CommonCard";
import needHelp from "assets/svg/Need Help Icon.svg";
import needrequest from "assets/svg/Need Help Icon2.svg";
import whatsapp from "assets/svg/Need Help Icon3.svg";
import needCare from "assets/svg/Need Help Icon4.svg";
import "./style.scss";

export const SupportCard = ({ isAlign = false }) => {
  const supportData = [
    {
      id: 0,
      supportIcon: needHelp,
      supportTitle: "Call Center",
      supportSubTitle: "800 124 9990",
      needTitle: false,
      needBorder: false,
      needPadding: false,
    },
    {
      id: 1,
      supportIcon: needrequest,
      supportTitle: "Open a support ticket",
      supportSubTitle: "Submit a request",
      addedTitle: "Anytime 24/7",
      needTitle: true,
      needBorder: false,
      needPadding: false,
    },
    {
      id: 2,
      supportIcon: whatsapp,
      supportTitle: "Whatsapp",
      supportSubTitle: "9200 19990",
      addedTitle: "This is a chat only number",
      needTitle: true,
      needBorder: false,
      needPadding: false,
    },
    {
      id: 3,
      supportIcon: needCare,
      supportTitle: "Chat with our executives",
      supportSubTitle: "Start Live Chat",
      needTitle: false,
      needBorder: true,
      needPadding: false,
    },
    {
      id: 4,
      supportIcon: needCare,
      supportTitle: "Email",
      supportSubTitle: "Care@tawuniya.com.sa",
      needTitle: false,
      needBorder: false,
      needPadding: true,
    },
  ];

  return (
    <div className="support-card-container">
      <CommonCard
        width="340px"
        cardPosition={
          isAlign ? "card-Support-Layout-forInsurance" : "card-Support-Layout"
        }
      >
        <div className="p-3">
          <p className="support-title fs-28 fw-800 m-0 line-height-30">
            How can we help you?
          </p>
          <p className="support-para fs-16 fw-400 pt-2 pb-3">
            Contact our support team now!
          </p>
          <div className="row">
            {supportData.map((item, index) => {
              return (
                <div className="col-lg-12 col-12" key={index}>
                  <div
                    className={`${item.needPadding && "pt-3"} d-flex flex-row`}
                    id={item.needBorder && "borderTitle"}
                  >
                    <div>
                      <img
                        src={item.supportIcon}
                        className={`${item.id === 3 && "invisible"} img-fluid`}
                        alt={item.supportIcon}
                      />
                    </div>
                    <div className="pl-3">
                      <p className="supportCard-para fs-14 fw-400 m-0">
                        {item.supportTitle}
                      </p>
                      <p className="supportCard-para-highlight fs-20 fw-800">
                        {item.supportSubTitle}
                        <span className="added-subTitle fs-12 fw-400 m-0">
                          {item.needTitle && item.addedTitle}
                        </span>
                      </p>
                    </div>
                  </div>
                </div>
              );
            })}
          </div>
        </div>
      </CommonCard>
    </div>
  );
};
