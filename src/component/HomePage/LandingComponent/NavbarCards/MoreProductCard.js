import React from "react";
import { history } from "service/helpers";
import { CommonCard } from "component/common/CommonCard";
import { moreProductData } from "component/common/MockData";
import orangeArrow from "assets/svg/orangeArrow.svg";

export const MoreProductCard = ({ isPillLayout = false }) => {
  const paraOne = ["Al Shameel", "Sanad"];

  const paraTwo = [
    "Engineering & Contractors Insurance",
    "Services Insurance",
    "Industrial & Energy Insurance",
    "Telecommunication & IT Insurance",
    "Financial & Investment Insurance",
    "Transportation Insurance",
    "Retail & Trading Insurance",
  ];

  const paraThree = [
    "360 Motor Insurance",
    "360 Heath Insurance",
    "360 Propery & Casuality Program",
  ];

  return (
    // more product card container starts here
    <div className="More-ProductCard-Container">
      {isPillLayout ? (
        <CommonCard width="700px" cardPosition="MoreProduct-Card-Layout">
          <div className="p-2 pb-4">
            <p className="fs-18 fw-700 corporate-moreProduct-title pb-2">
              More Products
            </p>
            <div className="row">
              <div className="col-8">
                <div className="moreProduct-liner">
                  <p className="corporate-enterpriseProductTitle fs-14 fw-800">
                    Enterprise Products
                  </p>
                  <div className="row">
                    <div className="col-5">
                      <p className="corporate-moreproduct-contentTitle fs-12 fw-800">
                        Motor
                      </p>
                      {paraOne.map((items) => {
                        return (
                          <p className="corporate-moreproduct-contentPara fs-12 fw-400 m-0 pb-2">
                            {items}
                          </p>
                        );
                      })}
                    </div>
                    <div className="col-7">
                      <p className="corporate-moreproduct-contentTitle fs-12 fw-800">
                        Propery & Casualty
                      </p>
                      {paraTwo.map((items) => {
                        return (
                          <p className="corporate-moreproduct-contentPara fs-12 fw-400 m-0 pb-2">
                            {items}
                          </p>
                        );
                      })}
                    </div>
                  </div>
                  <div className="pt-4">
                    <span className="corporate-moreproduct-link fs-12 fw-800 pr-2 text-uppercase cursor-pointer">
                      view all enterprise products
                    </span>
                    <img
                      src={orangeArrow}
                      className="img-fluid corporate-orangeArrow cursor-pointer"
                      alt="arrow"
                    />
                  </div>
                </div>
              </div>
              <div className="col-4">
                <p className="corporate-smeProductTitle fs-14 fw-800">
                  SME Products
                </p>
                {paraThree.map((items) => {
                  return (
                    <p className="corporate-moreproduct-360-contentPara fs-12 fw-400 m-0 pb-2">
                      {items}
                    </p>
                  );
                })}
                <div className="pt-4">
                  <span className="corporate-moreproduct-360Link fs-12 fw-800 pr-2 text-uppercase cursor-pointer">
                    view all sme products
                  </span>
                  <img
                    src={orangeArrow}
                    className="img-fluid corporate-360-orangeArrow cursor-pointer"
                    alt="arrow"
                  />
                </div>
              </div>
            </div>
          </div>
        </CommonCard>
      ) : (
        <CommonCard width="700px" cardPosition="MoreProduct-Card-Layout">
          <div className="p-2">
            <p className="fs-18 fw-700 moreProduct-title pb-2">More Products</p>
            <div className="d-flex flex-row pb-2">
              {moreProductData.map((item, index) => {
                return (
                  <div key={index} className="pr-5">
                    <p className="fs-16 fw-700">{item.title}</p>
                    {item?.content?.map((items) => {
                      return <p className="fs-14 fw-400 m-0 pb-2">{items}</p>;
                    })}
                  </div>
                );
              })}
            </div>
          </div>
        </CommonCard>
      )}
    </div>
    // more product card container ends here
  );
};
