import React from "react";
import { CommonCard } from "component/common/CommonCard";
import emergencyBanner from "assets/images/emergencyBanner.png";
import road from "assets/svg/emergencyIcons/roadsideemergency.svg";
import accident from "assets/svg/emergencyIcons/accidentEmergency.svg";
import telemed from "assets/svg/emergencyIcons/telemedEmergency.svg";
import america from "assets/svg/emergencyIcons/assistAmericaEmergency.svg";
import flightDelay from "assets/svg/emergencyIcons/flighdelayEmergency.svg";
import railBond from "assets/svg/emergencyIcons/railBondEmergency.svg";
import "./style.scss";

export const EmergencyCard = ({ isAlign = false }) => {
  const emergencyData = [
    {
      id: 0,
      titleOne: "Motor",
      subTitleOne: "Road Side Assistance",
      subTitleTwo: "Accident Assistance",
      cardLines: "sub-emergency-conatiner",
      iconOne: road,
      iconTwo: accident,
    },
    {
      id: 1,
      titleOne: "Medical",
      subTitleOne: "Telemedicine Doctor Consultation",
      subTitleTwo: "Assist America",
      cardLines: "sub-emergency-conatiner",
      urlTrue: true,
      url: "https://beta.tawuniya.com.sa/web/#/assistamerica",
      iconOne: telemed,
      iconTwo: america,
    },
    {
      id: 2,
      titleOne: "Travel",
      titleTwo: null,
      subTitleOne: "Flight delay Assistance",
      subTitleTwo: null,
      iconOne: flightDelay,
      iconTwo: null,
    },
    {
      id: 3,
      titleOne: "Medical Malpractice",
      subTitleOne: "Request BailBond",
      subTitleTwo: null,
      iconOne: railBond,
      iconTwo: null,
    },
  ];
  const openInNewTab = (url1) => {
    const newWindow = window.open(url1, "_blank", "noopener,noreferrer");
    if (newWindow) newWindow.opener = null;
  };

  return (
    // emergency card container starts here
    <div className="emergency-card-Container">
      <CommonCard
        width="480px"
        cardPosition={
          isAlign ? "card-Support-Layout-forInsurance" : "card-Support-Layout"
        }
      >
        <div className="pt-1 pb-2 m-2 emergenyContainer">
          <div className="emergencyContentImage">
            <img src={emergencyBanner} className="img-fluid" alt="bannerPic" />
          </div>
        </div>
        <div className="pt-1 pb-2 px-3">
          <div className="row">
            {emergencyData.map((item, index) => {
              return (
                <div
                  key={index}
                  className={`${item.cardLines} col-6 pt-2 pb-2 `}
                >
                  <p className="m-0 fs-20 fw-800 py-1 emergencyTitle">
                    {item.titleOne}
                  </p>

                  <div className="d-flex flex-row">
                    <div className="pr-2">
                      <img src={item.iconOne} className="" alt="icon" />
                    </div>
                    <div>
                      <span className="m-0 fs-14 fw-400 pb-1 emergencyPara ">
                        {item.subTitleOne}
                      </span>
                    </div>
                  </div>

                  <div className="d-flex flex-row pt-1">
                    <div className="pr-2">
                      {item.subTitleTwo && (
                        <img src={item.iconTwo} className="" alt="icon" />
                      )}
                    </div>
                    <div>
                      <span className="m-0 fs-14 fw-400 pb-1 emergencyPara">
                        {item.subTitleTwo}
                      </span>
                    </div>
                  </div>

                  {/* <img src={item.iconTwo} className="img-fluid pr-2" alt="icon" /> */}

                  {/* <span
                    className="m-0 fs-16 fw-400 pb-1 emergencyPara cursor-pointer d-block"
                    onClick={() => (item.urlTrue ? openInNewTab(item.url) : "")}
                  >
                    
                    {item.subTitleOne}
                  </span> */}
                  {/* <span className="m-0 fs-16 fw-400 pb-1 emergencyPara d-block">
                    {item.subTitleTwo}
                  </span> */}
                </div>
              );
            })}
          </div>
        </div>
      </CommonCard>
    </div>
    // emergency card container ends here
  );
};
