import React from "react";
import "./style.scss";
import { Button } from "@material-ui/core";
import { history } from "service/helpers";

export const HomeServiceImgCard = () => {
	const openInNewTab = (url1) => {
		const newWindow = window.open(url1, "_blank", "noopener,noreferrer");
		if (newWindow) newWindow.opener = null;
	};
	return (
		<div className="serviceBlogsImgContainer">
			<div className="serviceBlogsImga">
				<div className="serviceBlogsImgText1">
					<h5>International Travel Insurance..</h5>
					<span>
						Comprehensive protection from risks related to travel outside the
						<br />
						Kingdom.
					</span>
					<div className="serviceBlogsBtn">
						<Button
							style={{ textTransform: "none" }}
							size="medium"
							color="primary"
							variant="contained"
							disableElevation
							disableRipple
							onClick={() =>
								openInNewTab("https://beta.tawuniya.com.sa/web/#/motor")
							}
						>
							Buy Now
						</Button>
						<Button
							style={{ textTransform: "none" }}
							size="medium"
							color="secondary"
							variant="outlined"
							onClick={() => history.push("/home/internationaltravelpage")}
							disableElevation
							disableRipple
						>
							Learn More
						</Button>
					</div>
				</div>
			</div>
			<div className="serviceBlogsImgb">
				<div className="serviceBlogsImgText2">
					<h5>Insuring your car become easier!</h5>
					<span>
						Innovative products and value added services for your car.
					</span>
					<div className="serviceBlogsBtn">
						<Button
							style={{ textTransform: "none" }}
							size="medium"
							color="primary"
							variant="contained"
							disableElevation
							disableRipple
							onClick={() =>
								openInNewTab("https://beta.tawuniya.com.sa/web/#/motor")
							}
						>
							Buy More
						</Button>
						<Button
							style={{ textTransform: "none" }}
							size="medium"
							color="secondary"
							variant="outlined"
							onClick={() => history.push("/home/health-insurance")}
							disableElevation
							disableRipple
						>
							Learn More
						</Button>
					</div>
				</div>
			</div>
		</div>
	);
};
