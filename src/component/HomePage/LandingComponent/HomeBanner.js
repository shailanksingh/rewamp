import React, { useState } from "react";
import { NormalButton } from "component/common/NormalButton";
import expandIcon from "assets/svg/expandIcon.svg";
import closeIcon from "assets/images/toggleCloseIcon.png";
import deriveicon from "assets/svg/driveicon.svg";
import previewbannera from "assets/svg/previewbannera.svg";
import previewbannerb from "assets/svg/previewbannerb.svg";
import {useTranslation} from "react-i18next";

export const HomeBanner = () => {
	// initialize state to toggle card
	const [toggle, setToggle] = useState(true);

	const {t} = useTranslation()

	return (
		// banner container starts here
		<div className="row BannerContainer position-relative">
			<div className="col-lg-12 col-md-12 col-12 BannerBackground p-2">
				{toggle ? (
					<React.Fragment>
						<div className="d-flex justify-content-between px-4 mx-2">
							<div>
								<div className="d-flex pt-1">
									<div>
										<p className="toggleOpenHeading text-light fw-800 m-0">
											{t('topBar.messageLarge')}
										</p>
									</div>
									<div className="pl-3">
										<p className="toggleOpenPara text-light m-0">
											{t('topBar.messageMedium')}</p>
									</div>
								</div>
							</div>
							<div>
								<div className="d-flex">
									<div className="pr-5">
										<NormalButton
											label={t('topBar.downloadBtnText')}
											className="download-tawuniya-open-drive-btn"
										/>
									</div>
									<div>
										<img
											src={expandIcon}
											className="img-fluid closeIconToggler pr-4 pt-2"
											onClick={() => setToggle(false)}
											alt="closeicon"
										/>
									</div>
								</div>
							</div>
						</div>
					</React.Fragment>
				) : (
					<div className="row">
						<img
							className="previewbannerleft"
							src={previewbannera}
							alt="preview banner bg"
						/>
						<img
							className="previewbannerrighta"
							src={previewbannerb}
							alt="preview banner baground inage"
						/>
						\
						<img
							className="previewbannerrightb"
							src={deriveicon}
							alt="Tawuniya Drive Icon"
						/>
						<div className="col-11 offset-1">
							<div className="d-flex justify-content-between">
								<div>
									<p className="toggleHeading fw-800 m-0">
										{t('topBar.messageLarge')}
									</p>
									<p className="togglePara fs-22 fw-400 m-0">
										{t('topBar.messageInside')}
									</p>
									<div className="py-2">
										<NormalButton
											label={t('topBar.downloadBtnText')}
											className="download-tawuniya-close-drive-btn p-3"
										/>
									</div>
								</div>
								<div className="pt-3">
									<img
										src={closeIcon}
										className="img-fluid closeIconToggler pr-2"
										onClick={() => setToggle(true)}
										alt="closeicon"
									/>
								</div>
							</div>
						</div>
					</div>
				)}
			</div>
		</div>
	);
};
