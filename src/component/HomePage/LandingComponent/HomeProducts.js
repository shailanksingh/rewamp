import React, { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { layoutHandler } from "action/LanguageAct";
import homeproducts1 from "../../../assets/images/homeproducts1.png";
import Vector3 from "../../../assets/svg/travel.svg";
import Vector1 from "../../../assets/svg/Vector2.svg";
import Vector2 from "../../../assets/svg/Vector3.svg";
import uniontopright from "../../../assets/svg/uniontopright.svg";
import unioinbottomleft from "../../../assets/svg/unioinbottomleft.svg";
import productsicon1 from "../../../assets/svg/productsicon1.svg";
import productsicon2 from "../../../assets/svg/productsicon2.svg";
import productsicon3 from "../../../assets/svg/productsicon3.svg";
import ArrowBack from "../../../assets/svg/Arrows/ArrowBack.svg";
import ArrowForward from "../../../assets/svg/Arrows/ArrowForward.svg";
import { ProductImageCard } from "component/common/ProductImageCard";
import ArrowForwardIcon from "@material-ui/icons/ArrowForward";
import MouseScrolldown from "../../../assets/svg/Mouse Scrolldown.svg";
import orangeArrow from "../../../assets/svg/orangeArrow.svg";
import motorLandingHighlight from "assets/svg/motorLandingHighlight.svg";
import travelLandingHighlight from "assets/svg/travelLandingHighlight.svg";
import healthLandingHighlight from "assets/svg/healthLandingHighlight.svg";
import mouseScroll from "assets/svg/Mouse Scrolldown.svg";
import enterprise from "assets/svg/homeEnterprise.svg";

import "./style.scss";
import {
	Box,
	Typography,
	ThemeProvider,
	createTheme,
	Button,
	Grid,
	Icon,
	Card,
	CardContent,
	CardActions,
	Divider,
} from "@material-ui/core";
import "./style.scss";

import { ProductCard } from "component/common/ProductCard";
import {
	ProductsTabMotorData,
	ProductsTabData,
	corporateSmeProductsTabData,
	corporateEnterpriseProductsTabData,
} from "./Schema/ProductsTabData";
import normalSme from "assets/svg/smeNormal.svg";
import styles from "./style.module.scss";

const theme = createTheme({
	button: {
		color: "#FFFFFF",
	},
});

export const HomeProducts = ({ isPillLayout = false }) => {
	const [cardIndex, setCardIndex] = useState(0);

	const dispatch = useDispatch();
	const [homeProductsHeight, setHomeProductsHeight] = useState(null);

	useEffect(() => {
		setHomeProductsHeight(
			parseInt(
				window
					.getComputedStyle(document.querySelector(".hm-textContainer"), null)
					.paddingTop.split("", 2)
					.join(""),
				10
			) +
				document.querySelector(".hm-text-container").clientHeight +
				document.querySelector(".hm-productCards").clientHeight +
				150
		);
		console.log("HomeProductsHeight", homeProductsHeight);
	}, [cardIndex]);

	const nextSlideHandler = () => {
		if (cardIndex + 1 >= 3) {
			setCardIndex(0);
		} else {
			setCardIndex(cardIndex + 1);
		}
		dispatch(layoutHandler(cardIndex + 1));
	};

	const previousSlideHandler = () => {
		if (cardIndex - 1 < 0) {
			setCardIndex(2);
		} else {
			setCardIndex(cardIndex - 1);
		}
		dispatch(layoutHandler(cardIndex - 1));
	};

	const [pillIndex, setPillIndex] = useState(0);

	const corporateToggle =
		pillIndex === 0
			? corporateEnterpriseProductsTabData
			: corporateSmeProductsTabData;

	const handleScroll = () => {
		window.scrollTo(0, 1000);
	};

	return (
		<ThemeProvider theme={theme}>
			{isPillLayout && (
				<div className="mb-3">
					<img
						src={mouseScroll}
						className="img-fluid mx-auto d-block pb-4 mb-2"
						alt="scrollIcon"
					/>
					<div className="d-flex justify-content-center">
						<div>
							<div className="d-flex flex-row corporate-navtab-container-root">
								<div className="pr-3 ">
									<div
										className={
											pillIndex === 0
												? "corporate-highlight-navtab-container"
												: "corporate-normal-navtab-container"
										}
										onClick={() => setPillIndex(0)}
									>
										<div className="d-flex flex-row">
											<div className="pr-2">
												{pillIndex === 0 ? (
													<img
														src={enterprise}
														className="img-fluid"
														alt="icon"
													/>
												) : (
													<img
														src={normalSme}
														className="img-fluid normalCorporateIcon"
														alt="icon"
													/>
												)}
											</div>
											<div>
												<p className="corporate-highlight-navtab fs-16 fw-800 m-0 text-uppercase pt-1 pr-2">
													enterprise
												</p>
											</div>
										</div>
									</div>
								</div>
								<div>
									<div
										className={
											pillIndex === 1
												? "corporate-highlight-navtab-container"
												: "corporate-normal-navtab-container"
										}
										onClick={() => setPillIndex(1)}
									>
										<div className="d-flex flex-row">
											<div className="pr-2">
												{pillIndex === 1 ? (
													<img
														src={enterprise}
														className="img-fluid"
														alt="icon"
													/>
												) : (
													<img
														src={normalSme}
														className="img-fluid normalCorporateIcon"
														alt="icon"
													/>
												)}
											</div>
											<div>
												<p className="corporate-highlight-navtab fs-16 fw-800 m-0 text-uppercase pt-1 pr-2">
													sme
												</p>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					{pillIndex === 0 ? (
						<p className="text-center fs-16 fw-800 pt-2">
							For Cooperate Businesses with MORE than 250 employees
						</p>
					) : (
						<p className="text-center fs-16 fw-800 pt-2">
							For Cooperate Businesses with LESS than 250 employees
						</p>
					)}
				</div>
			)}

			<div>
				<div
					className="homeProductsContainer"
					style={{ height: homeProductsHeight }}
				>
					<div
						className="hm-textContainer"
						style={{
							background: `${ProductsTabData[cardIndex].productBackground}`,
						}}
					>
						<div className="hm-text-container">
							{isPillLayout ? (
								<React.Fragment>
									{cardIndex === 0 && (
										<>
											<h4>Whatever your needs are, right there!</h4>
											<p>Innovative products and value added services</p>
										</>
									)}
									{cardIndex === 1 && (
										<>
											<h4>Your health is important to us</h4>
											<p>
												We provide the best and trusted heathcare solutions for
												you
											</p>
										</>
									)}
									{cardIndex === 2 && (
										<>
											<h4>You are safe</h4>
											<p>Great value products that cover your needs</p>
										</>
									)}
								</React.Fragment>
							) : (
								<React.Fragment>
									<h4>What would you like to protect today?</h4>
									<p>We provide the best and trusted products for you.</p>
								</React.Fragment>
							)}
							<Grid container style={{ marginTop: "30px" }}>
								<Grid item>
									<Button
										className={
											"homeProBtn-style " +
											(cardIndex === 0 ? styles.homeProBtn1 : styles.homeProBtn)
										}
										onClick={() => {
											setCardIndex(0);
										}}
										style={{
											borderRadius: "100px",
											zIndex: "2",
											backgroundColor:
												cardIndex === 0 ? "#ffffff" : "transparent",
											boxShadow:
												cardIndex === 0
													? "0px 7px 10px rgba(9, 30, 66, 0.05)"
													: "",
											color: cardIndex === 0 ? "#455560" : "#ffffff",
										}}
										startIcon={
											<Icon>
												<img
													className={
														cardIndex === 0
															? styles.homeProImg1
															: styles.homeProImg
													}
													src={Vector1}
													alt="Motor"
													style={{
														width: "20px",
														height: "20px",
														marginBottom: "20px",
														padding: "3px",
														borderRadius: "100px",
														fontSize: "16px",
													}}
												/>
											</Icon>
										}
									>
										Motor
									</Button>
								</Grid>
								<Grid item style={{ marginLeft: "15px" }}>
									<Button
										className={
											"homeProBtn-style " +
											(cardIndex === 1 ? styles.homeProBtn2 : styles.homeProBtn)
										}
										onClick={() => {
											setCardIndex(1);
										}}
										style={{
											borderRadius: "100px",
											zIndex: "2",
											backgroundColor:
												cardIndex === 1 ? "#ffffff" : "transparent",
											boxShadow:
												cardIndex === 1
													? "0px 7px 10px rgba(9, 30, 66, 0.05)"
													: "",
											color: cardIndex === 1 ? "#455560" : "#ffffff",
										}}
										startIcon={
											<Icon>
												<img
													className={
														cardIndex === 1
															? styles.homeProImg2
															: styles.homeProImg
													}
													src={Vector2}
													alt="Property & Casualty"
													style={{
														width: "20px",
														height: "20px",
														// width: "100%",
														marginBottom: "20px",
														padding: "3px",
														borderRadius: "100px",
														fontSize: "16px",
													}}
												/>
											</Icon>
										}
									>
										Health
									</Button>
								</Grid>
								<Grid item style={{ marginLeft: "15px" }}>
									<Button
										className={
											"homeProBtn-style " +
											(cardIndex === 2 ? styles.homeProBtn3 : styles.homeProBtn)
										}
										onClick={() => {
											setCardIndex(2);
										}}
										style={{
											borderRadius: "100px",
											zIndex: "2",
											backgroundColor:
												cardIndex === 2 ? "#ffffff" : "transparent",
											boxShadow:
												cardIndex === 2
													? "0px 7px 10px rgba(9, 30, 66, 0.05)"
													: "",
											color: cardIndex === 2 ? "#455560" : "#ffffff",
										}}
										startIcon={
											<Icon>
												<img
													className={
														cardIndex === 2
															? styles.homeProImg3
															: styles.homeProImg
													}
													src={Vector3}
													alt="Medical & Takaful"
													style={{
														width: "20px",
														height: "20px",
														// width: "100%",
														marginBottom: "20px",
														padding: "3px",
														borderRadius: "100px",
														fontSize: "16px",
													}}
												/>
											</Icon>
										}
									>
										property & casualty
									</Button>
								</Grid>
							</Grid>
						</div>
						<img
							className="uniontopright"
							src={uniontopright}
							alt="uniontopright"
						/>
						<img
							className="unioinbottomleft"
							src={unioinbottomleft}
							alt="unioinbottomleft"
						/>
					</div>
					<div className="hm-mainImage">
						<img
							style={{ width: "100%" }}
							src={ProductsTabData[cardIndex].productImage}
							alt=""
						/>
						<div className="hm-buttons">
							<img
								className="arrowback"
								src={ArrowBack}
								alt="arrow back"
								onClick={previousSlideHandler}
								style={{
									backgroundColor: `${ProductsTabData[cardIndex].productBackground}`,
								}}
							/>
							<img
								className="arrowforward"
								src={ArrowForward}
								onClick={nextSlideHandler}
								alt="arrow forward"
								style={{
									backgroundColor: `${ProductsTabData[cardIndex].productBackground}`,
								}}
							/>
						</div>
					</div>
					<div
						className="hm-productCards"
						sx={{
							position: "absolute",
							top: "22%",
							left: "10%",
							maxWidth: "100%",
							display: "flex",
							flexWrap: "wrap",
						}}
					>
						{isPillLayout ? (
							<React.Fragment>
								{corporateToggle[cardIndex].productCard.map((data, index) => (
									<ProductCard
										key={data.productName + index}
										Display={data.display}
										productIcon={data.productIcon}
										category={data.category}
										productName={data.productName}
										productDetail={data.productDetail}
										productDescription={data.productDescription}
										productImg={data.productImg}
										cardNo={data.cardNo}
										driveAr={data.driveAr}
										driveEn={data.driveEn}
										redirectURL={ProductsTabData[cardIndex]?.redirectURL}
										urlTrue={data.urlTrue}
										url={data.url}
									/>
								))}
							</React.Fragment>
						) : (
							<React.Fragment>
								{ProductsTabData[cardIndex].productCard.map((data, index) => (
									<ProductCard
										key={data.productName + index}
										Display={data.display}
										productIcon={data.productIcon}
										category={data.category}
										productName={data.productName}
										productDetail={data.productDetail}
										productDescription={data.productDescription}
										productImg={data.productImg}
										cardNo={data.cardNo}
										driveAr={data.driveAr}
										driveEn={data.driveEn}
										redirectURL={ProductsTabData[cardIndex]?.redirectURL}
										urlTrue={data.urlTrue}
										url={data.url}
									/>
								))}
							</React.Fragment>
						)}
						<ProductImageCard />
					</div>
					<div className="ProductBtmCta">
						<Button
							size="small"
							color="primary"
							endIcon={
								<Icon style={{ height: "fit-content", width: "fit-content" }}>
									<img
										className="ProductBtmCtaArrow"
										src={orangeArrow}
										alt="View all Products"
									/>
								</Icon>
							}
							style={{
								fontWeight: "800",
								fontSize: "14px",
								lineHeight: "16px",
								textTransform: "uppercase",
								color: "#4C565C",
							}}
						>
							View All Products
						</Button>
					</div>
					{!isPillLayout && (
						<img
							src={MouseScrolldown}
							className="MouseScrolldown"
							alt="Mouse Scroll button"
							onClick={handleScroll}
						/>
					)}
				</div>
				{/* ))} */}
			</div>
		</ThemeProvider>
	);
};
