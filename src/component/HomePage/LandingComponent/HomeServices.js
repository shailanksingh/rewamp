import React, {useEffect, useRef, useState} from "react";
import {useSelector} from "react-redux";
import "./style.scss";
import CardComp from "../../common/HomeServiceCard/index";
import ServiceCard from "../../common/ServiceImgeCard";
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import {Icon} from "@material-ui/core";
import {Theme, makeStyles} from "@material-ui/core";
import headerSearchIcon from "../../../assets/svg/headerSearchIcon.svg";
import ArrowForward from "../../../assets/svg/HomeServiceFroward1Arrow.svg";
import ArrowBack from "../../../assets/svg/HomeServiceBackArrow.svg";
import {HomeServicesData} from "./Schema/HomeServicesData";

const useStyles = makeStyles((theme) => ({
  dots: {
    bottom: -63,
    "& li.slick-active button::before": {
      color: "#EE7500",
    },
    "& li": {
      width: "12px",
      "& button::before": {
        fontSize: theme.typography.pxToRem(9),
        color: "#4C565C",
      },
    },
  },
}));

export const HomeServices = (props) => {
  const [navPill, setNavPill] = useState(0);

  const classes = useStyles();

  const sliderRef = useRef(null);

  const settings = {
    slidesToShow: 6,
    slidesToScroll: 2,
    arrows: false,
    dots: true,
    // variableWidth: true,
    dotsClass: `slick-dots ${classes.dots}`,
    responsive: [
      {
        breakpoint: 1199,
        settings: {
          slidesToShow: 6,
          slidesToScroll: 2,
        },
      },
      {
        breakpoint: 800,
        settings: {
          slidesToShow: 6,
          slidesToScroll: 1,
          infinite: true,
          dots: true,
        },
      },
    ],
  };

  useEffect(() => {
    console.log(props.HomeServicesData, "data");
  }, []);

  const getPillIndex = useSelector((val) => val.languageReducer.layoutPosition);

  return (
    <>
      <div className="serviceContainer">
        {props.HomeServicesData[0].Search ? (
          <div className="serviceContainerFlex">
            <div className="serviceContainerText">
              <h1 className="HomeServiceHeading1">
                {props.HomeServicesData[0].serviceTitle}
              </h1>

              <p className="homeservicePar1">
                {props.HomeServicesData[0].serviceSubtitle}
              </p>
            </div>

            <div className="otherServiceNavCont">
              <form className="navSearchContainer">
                <input
                  className="inputEl"
                  type="search"
                  placeholder="What are you looking for?"
                />
                <img
                  className="homeserviceSearchIcon"
                  src={headerSearchIcon}
                  alt=""
                />
              </form>
            </div>
          </div>
        ) : (
          <>
            <h1 className="HomeServiceHeading1 text-center">
              {props.HomeServicesData[0].serviceTitle}
            </h1>

            <p className="homeservicePar1 text-center">
              {props.HomeServicesData[0].serviceSubtitle}
            </p>
          </>
        )}

        {props.isPillNeeded && (
          <React.Fragment>
            {props.HomeServicesData[0].Services ? (
              <div className="ServiceNavCont">
                <div className="ServiceNavEl">
                  {props.HomeServicesData[0].Services
                    ? props.HomeServicesData[0].Services.map((item, i) =>
                      <p key={item.id + i}
                         className={navPill === item.id ? "navEl-highlight" : "navEl"}
                         onClick={() => setNavPill(item.id)}>
                        {item.pillName}</p>
                    )
                    : <></>}
                </div>
              </div>
            ) : (
              <></>
            )}
          </React.Fragment>
        )}

        {props.HomeServicesData[0].hrLine ? (
          <hr className="hrLine"/>
        ) : (
          <hr className="invisiblehrLine"/>
        )}

        <React.Fragment>
          {HomeServicesData.map((item, i) => {
            return (
              <div key={'services_' + i}>
                {item?.Services?.map((items, index) => {
                  return (
                    <React.Fragment key={'nestedServices_' + index}>
                      {props.isPillNeeded ? (
                        navPill === items.id &&
                        <Slider ref={sliderRef} {...settings}>
                          {items?.serviceCardData?.map((val, i) => {
                            return (
                              <div className="slider-card-root">
                                {!val.hideCard && (
                                  <CardComp item={val}/>
                                )}
                              </div>
                            );
                          })}
                        </Slider>
                      ) : (
                        items.id === 2 &&
                        <Slider ref={sliderRef} {...settings} key={i}>
                          {items?.serviceCardData?.map((val, i) => {
                            return (
                              <div className="slider-card-root" key={val + i}>
                                {!val.hideCard && (
                                  <CardComp item={val}/>
                                )}
                              </div>
                            );
                          })}
                        </Slider>
                      )}
                    </React.Fragment>
                  );
                })}
              </div>
            );
          })}
        </React.Fragment>

        <div className="arrowDotContainer">
          <div
            className="arrowContainer"
            onClick={() => sliderRef.current.slickPrev()}
          >
            <img src={ArrowBack}/>
          </div>
          <div className="dotContainer"></div>

          <div
            className="arrowContainer"
            onClick={() => sliderRef.current.slickNext()}
          >
            <img src={ArrowForward}/>
          </div>
        </div>
      </div>
    </>
  );
};
