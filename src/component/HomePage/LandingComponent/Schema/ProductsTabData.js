import productIconMotor from "../../../../assets/svg/productsicon1.svg";
import productIconPropertyCasuality from "../../../../assets/svg/productsicon2.svg";
import productIconMedicalTakaful from "../../../../assets/svg/productsicon3.svg";
import homeproducts1 from "../../../../assets/images/homeproducts1.png";
import homeproducts2 from "../../../../assets/images/homeproducts2.png";
import homeproducts3 from "../../../../assets/images/homeproducts3.png";
import homesubproducts1 from "../../../../assets/images/homesubproducts1.png";
import DriveAr from "../../../../assets/svg/Drive Ar.svg";
import DriveEn from "../../../../assets/svg/Drive En.svg";

export const ProductsTabData = [
	{
		productBackground: "#679AB4",
		productImage: `${homeproducts1}`,
		redirectURL: "/home-insurance/motor",
		productCard: [
			{
				display: true,
				cardNo: 1,
				productIcon: `${productIconMotor}`,
				category: "MOTOR",
				productName: "Comprehensive Insurance",
				productDetail: "AL-SHAMEL Motor Insurance Program",
				productDescription:
					"The maximum cover of Al-Shamel insurance is determined by the value of the insured vehicle at the time of accident",
				urlTrue: true,
				url: "https://beta.tawuniya.com.sa/web/#/motor",
			},
			{
				display: true,
				cardNo: 2,
				productIcon: `${productIconMotor}`,
				category: "MOTOR",
				productName: "Third Party Liability ",
				productDetail: "SANAD Private Motor Vehicle Third Party Liability.",
				productDescription:
					"In the event of an accident occurring during the policy effective period, involving indemnity for third party liability under the terms of this Policy",
				urlTrue: true,
				url: "https://beta.tawuniya.com.sa/web/#/motor",
			},
			{
				display: false,
				cardNo: 3,
				productIcon: `${productIconMotor}`,
				category: "MOTOR",
				productName: "Limited Comprehensive",
				productDetail: "SANAD Plus Insurance Program",
				productDescription:
					"Provides limited insurance cover for the damage of the insured car as well as the liability to third parties.",
			},
			{
				display: true,
				cardNo: 4,
				productIcon: `${productIconMotor}`,
				category: "MOTOR",
				productName: "Limited Comprehensive",
				productDetail: "SANAD Plus Insurance Program",
				productDescription:
					"Provides limited insurance cover for the damage of the insured car as well as the liability to third parties.",
				urlTrue: true,
				url: "https://beta.tawuniya.com.sa/web/#/motor",
			},
			{
				display: true,
				cardNo: 5,
				productIcon: `${productIconMotor}`,
				category: "MOTOR",
				productName: "Mechanical Breakdown ",
				productDetail: "Cover for Mechanical or Electrical Failures",
				productDescription:
					"This benefit compensates you for damages to the insured vehicle that result from fire or lightning.",
				urlTrue: true,
				url: "https://beta.tawuniya.com.sa/web/#/motor",
			},
			{
				display: true,
				driveAr: `${DriveAr}`,
				driveEn: `${DriveEn}`,
				cardNo: 6,
				productImg: `${homesubproducts1}`,
				productDetail: "Tawuniya Drive",
				productDescription: "behavior program that gives you the opportunity",
				urlTrue: true,
				url: "https://beta.tawuniya.com.sa/web/#/motor",
			},
		],
	},
	{
		productBackground: "#679AB4",
		productImage: `${homeproducts3}`,
		redirectURL: "/home/medicalmalpracticepage",
		productCard: [
			{
				display: true,
				productIcon: `${productIconMedicalTakaful}`,
				category: "Medical & Takaful",
				productName: "Comprehensive Insurance",
				productDetail: "AL-SHAMEL Motor Insurance Program",
				productDescription:
					"This is an insurance program that provides adequate healthcare for all family members, in compliant with the provisions of Islamic Sharia..",
			},
			{
				display: true,
				productIcon: `${productIconMedicalTakaful}`,
				category: "Medical & Takaful",
				productName: "Third Party Liability ",
				productDetail: "Medical Insurance for Saudi Arabia Visitors",
				productDescription:
					"By purchasing the Visitor Insurance Program from Tawuniya Insurance Company, Visitors to the Kingdom of Saudi Arabia..",
			},
			{
				display: true,
				productIcon: `${productIconMedicalTakaful}`,
				category: "Medical & Takaful",
				productName: "Umrah Medical Insurance",
				productDetail: "Umrah Insurance Program – For Foreign Pilgrims",
				productDescription:
					"In alignment with the Saudi Vision 2030 and in pursuit of achieving it through Doyof Al Rahman national Program..",
			},
		],
	},
	{
		productBackground: "#679AB4",
		productImage: `${homeproducts2}`,
		redirectURL: "/home/internationaltravelpage",
		productCard: [
			{
				display: true,
				productIcon: `${productIconPropertyCasuality}`,
				category: "Property & Casualty",
				productName: "Home Insurance",
				productDetail: "Insurance against theft, fire and additional risks",
				productDescription:
					"The policy covers, according to the client choices, either the buildings or contents..",
				urlTrue: false,
			},
			{
				display: true,
				productIcon: `${productIconPropertyCasuality}`,
				category: "Property & Casualty",
				productName: "International Travel Insurance",
				productDetail: "Travel Insurance for Europe and rest of the world",
				productDescription:
					"A program duly approved by the Shraiah Committee for the provision of insurance cover for travelers on international flights..",
				urlTrue: true,
				url: "https://beta.tawuniya.com.sa/web/#/travel",
			},
			{
				display: false,
			},
			{
				display: true,
				productIcon: `${productIconPropertyCasuality}`,
				category: "Property & Casualty",
				productName: "COVID-19 Travel Insurance",
				productDetail: "COVID-19 Travel Insurance",
				productDescription:
					"The government of the Kingdom of Saudi Arabia is keen to provide safety and peace of mind for Saudi citizens while travelling abroad.",
				urlTrue: false,
			},
			{
				display: true,
				productIcon: `${productIconPropertyCasuality}`,
				category: "Property & Casualty",
				productName: "Medical Malpractice Insurance",
				productDetail: "Medical Malpractice Insurance",
				productDescription:
					"A program protects those persons practicing medical professions from the risks associated with their work..",
				urlTrue: true,
				url: "https://beta.tawuniya.com.sa/web/#/medmal",
			},
			{
				display: true,
				productIcon: `${productIconPropertyCasuality}`,
				category: "Property & Casualty",
				productName: "Shop Owners Insurance",
				productDetail: "Shop Owners Insurance",
				productDescription:
					"The cost of program cover or sum insured is determined on the basis of actual value of insured shop and its contents..",
				urlTrue: false,
			},
		],
	},
];


export const corporateEnterpriseProductsTabData = [
	{
		productBackground: "#679AB4",
		productImage: `${homeproducts1}`,
		redirectURL: "/home-insurance/motor",
		productCard: [
			{
				display: true,
				cardNo: 1,
				productIcon: `${productIconMotor}`,
				category: "MOTOR",
				productName: "Al Shamel",
				productDetail: "Comprehensive Commercial Motor Insurance (AL-SHAMEL)",
				productDescription:
					"Indemnify the policyholder against the accidental loss of or damage to the insured commercial motor vehicles and the third party liabilities",
				urlTrue: true,
				url: "https://beta.tawuniya.com.sa/web/#/motor",
			},
			{
				display: true,
				cardNo: 2,
				productIcon: `${productIconMotor}`,
				category: "MOTOR",
				productName: "Sanad",
				productDetail: "Sanad for Commercial Motor Vehicle Liability Insurance",
				productDescription:
					"Covers the liability of commercial vehicles to third parties resulting from accidents up to a maximum of 10 million riyals",
				urlTrue: true,
				url: "https://beta.tawuniya.com.sa/web/#/motor",
			},
			
			
			
			
		],
	},
	{
		productBackground: "#679AB4",
		productImage: `${homeproducts3}`,
		redirectURL: "/home/medicalmalpracticepage",
		productCard: [
			{
				display: true,
				productIcon: `${productIconMedicalTakaful}`,
				category: "Medical & Takaful",
				productName: "Comprehensive Insurance",
				productDetail: "AL-SHAMEL Motor Insurance Program",
				productDescription:
					"This is an insurance program that provides adequate healthcare for all family members, in compliant with the provisions of Islamic Sharia..",
			},
			{
				display: true,
				productIcon: `${productIconMedicalTakaful}`,
				category: "Medical & Takaful",
				productName: "Third Party Liability ",
				productDetail: "Medical Insurance for Saudi Arabia Visitors",
				productDescription:
					"By purchasing the Visitor Insurance Program from Tawuniya Insurance Company, Visitors to the Kingdom of Saudi Arabia..",
			},
			{
				display: true,
				productIcon: `${productIconMedicalTakaful}`,
				category: "Medical & Takaful",
				productName: "Umrah Medical Insurance",
				productDetail: "Umrah Insurance Program – For Foreign Pilgrims",
				productDescription:
					"In alignment with the Saudi Vision 2030 and in pursuit of achieving it through Doyof Al Rahman national Program..",
			},
		],
	},
	{
		productBackground: "#679AB4",
		productImage: `${homeproducts2}`,
		redirectURL: "/home/internationaltravelpage",
		productCard: [
			{
				display: true,
				productIcon: `${productIconPropertyCasuality}`,
				category: "Property & Casualty",
				productName: "Home Insurance",
				productDetail: "Insurance against theft, fire and additional risks",
				productDescription:
					"The policy covers, according to the client choices, either the buildings or contents..",
				urlTrue: false,
			},
			{
				display: true,
				productIcon: `${productIconPropertyCasuality}`,
				category: "Property & Casualty",
				productName: "International Travel Insurance",
				productDetail: "Travel Insurance for Europe and rest of the world",
				productDescription:
					"A program duly approved by the Shraiah Committee for the provision of insurance cover for travelers on international flights..",
				urlTrue: true,
				url: "https://beta.tawuniya.com.sa/web/#/travel",
			},
			{
				display: false,
			},
			{
				display: true,
				productIcon: `${productIconPropertyCasuality}`,
				category: "Property & Casualty",
				productName: "COVID-19 Travel Insurance",
				productDetail: "COVID-19 Travel Insurance",
				productDescription:
					"The government of the Kingdom of Saudi Arabia is keen to provide safety and peace of mind for Saudi citizens while travelling abroad.",
				urlTrue: false,
			},
			{
				display: true,
				productIcon: `${productIconPropertyCasuality}`,
				category: "Property & Casualty",
				productName: "Medical Malpractice Insurance",
				productDetail: "Medical Malpractice Insurance",
				productDescription:
					"A program protects those persons practicing medical professions from the risks associated with their work..",
				urlTrue: true,
				url: "https://beta.tawuniya.com.sa/web/#/medmal",
			},
			{
				display: true,
				productIcon: `${productIconPropertyCasuality}`,
				category: "Property & Casualty",
				productName: "Shop Owners Insurance",
				productDetail: "Shop Owners Insurance",
				productDescription:
					"The cost of program cover or sum insured is determined on the basis of actual value of insured shop and its contents..",
				urlTrue: false,
			},
			{
				display: true,
				productIcon: `${productIconPropertyCasuality}`,
				category: "Property & Casualty",
				productName: "Shop Owners Insurance",
				productDetail: "Shop Owners Insurance",
				productDescription:
					"The cost of program cover or sum insured is determined on the basis of actual value of insured shop and its contents..",
				urlTrue: false,
			},
			{
				display: true,
				productIcon: `${productIconPropertyCasuality}`,
				category: "Property & Casualty",
				productName: "Shop Owners Insurance",
				productDetail: "Shop Owners Insurance",
				productDescription:
					"The cost of program cover or sum insured is determined on the basis of actual value of insured shop and its contents..",
				urlTrue: false,
			},
		],
	},
];


export const corporateSmeProductsTabData = [
	{
		productBackground: "#679AB4",
		productImage: `${homeproducts1}`,
		redirectURL: "/home-insurance/motor",
		productCard: [
			{
				display: true,
				cardNo: 1,
				productIcon: `${productIconMotor}`,
				category: "MOTOR",
				productName: "Al Shamel",
				productDetail: "Comprehensive Commercial Motor Insurance (AL-SHAMEL)",
				productDescription:
					"Indemnify the policyholder against the accidental loss of or damage to the insured commercial motor vehicles and the third party liabilities",
				urlTrue: true,
				url: "https://beta.tawuniya.com.sa/web/#/motor",
			},		
		],
	},
	{
		productBackground: "#679AB4",
		productImage: `${homeproducts3}`,
		redirectURL: "/home/medicalmalpracticepage",
		productCard: [
			{
				display: true,
				productIcon: `${productIconMedicalTakaful}`,
				category: "Medical & Takaful",
				productName: "Comprehensive Insurance",
				productDetail: "AL-SHAMEL Motor Insurance Program",
				productDescription:
					"This is an insurance program that provides adequate healthcare for all family members, in compliant with the provisions of Islamic Sharia..",
			},
		],
	},
	{
		productBackground: "#679AB4",
		productImage: `${homeproducts2}`,
		redirectURL: "/home/internationaltravelpage",
		productCard: [
			{
				display: true,
				productIcon: `${productIconPropertyCasuality}`,
				category: "Property & Casualty",
				productName: "Home Insurance",
				productDetail: "Insurance against theft, fire and additional risks",
				productDescription:
					"The policy covers, according to the client choices, either the buildings or contents..",
				urlTrue: false,
			},
		],
	},
];
