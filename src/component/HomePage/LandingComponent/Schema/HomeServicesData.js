import serviceIcons1 from "../../../../assets/svg/TawuniyaServicesIcons/serviceIcons1.svg";
import serviceIcons2 from "../../../../assets/svg/TawuniyaServicesIcons/serviceIcons2.svg";
import serviceIcons3 from "../../../../assets/svg/TawuniyaServicesIcons/serviceIcons3.svg";
import serviceIcons4 from "../../../../assets/svg/TawuniyaServicesIcons/serviceIcons4.svg";
import serviceIcons5 from "../../../../assets/svg/TawuniyaServicesIcons/serviceIcons5.svg";
import serviceIcons6 from "../../../../assets/svg/TawuniyaServicesIcons/serviceIcons6.svg";
import tawuniyaDrive from "assets/svg/tawuniyaDrivePrimary.svg";
import chronic from "assets/svg/chronicPrimary.svg";
import eligibility from "assets/svg/eligibilityPrimary.svg";
import roadSide from "assets/svg/roadSidePrimary.svg";
import ithra from "assets/svg/ithraPrimary.svg";
import motorInspect from "assets/svg/motorInspectPrimary.svg";
import freeCarMaintainence from "assets/svg/freeCarMaintainence.svg";
import endtoendClaim from "assets/svg/endtoendClaim.svg";
import covid from "assets/svg/TawuniyaServicesIcons/covidIcon.svg";
import vaccinate from "assets/svg/dashboardIcons/childVaccination.svg";
import chronicDisease from "assets/svg/dashboardIcons/chronic.svg";
import pregnancy from "assets/svg/PregnancyProgram.svg";

export const HomeServicesData = [
  {
    Search: true,
    serviceTitle: "Tawuniya support is here to help",
    hrLine: true,
    serviceSubtitle: "Always available to answer your questions",
    Services: [
      {
        id: 0,
        pillName: "ALL SERVICES",
        serviceCardData: [
          {
            headingEl: "Tawuniya Drive",
            discrptionEl:
              "It mesures your driving behavior and rewards you weekly,",
            iconE1: `${tawuniyaDrive}`,
          },
          {
            headingEl: "Chronic Disease Management",
            discrptionEl: "A program that serves people with Chronic Disease",
            iconE1: `${chronic}`,
          },
          {
            headingEl: "Request for Eligibility Letter",
            discrptionEl:
              "A letter issues from Tawuniya explaining your entitlement to treatment in case of any accidental problem",
            iconE1: `${eligibility}`,
          },
          {
            headingEl: "Road Side Assistance",
            discrptionEl: "Our services can be trustable, honest and worthy",
            iconE1: `${roadSide}`,
          },
          {
            headingEl: "Ithra’a",
            discrptionEl:
              "Special discounts on a wide range of services and products that are supplied by distinguished partners and famous brands in various sectors",
            iconE1: `${ithra}`,
          },
          {
            headingEl: "Motor Vehicle Periodic Inspection",
            discrptionEl: "Motor Vehicle Periodic Inspection",
            iconE1: `${motorInspect}`,
          },
          {
            headingEl: "Tawuniya Drive",
            discrptionEl:
              "It mesures your driving behavior and rewards you weekly,",
            iconE1: `${tawuniyaDrive}`,
          },
          {
            headingEl: "Chronic Disease Management",
            discrptionEl: "A program that serves people with Chronic Disease",
            iconE1: `${chronic}`,
          },
          {
            headingEl: "Request for Eligibility Letter",
            discrptionEl:
              "A letter issues from Tawuniya explaining your entitlement to treatment in case of any accidental problem",
            iconE1: `${eligibility}`,
          },
          {
            headingEl: "Road Side Assistance",
            discrptionEl: "Our services can be trustable, honest and worthy",
            iconE1: `${roadSide}`,
          },
          {
            headingEl: "Ithra’a",
            discrptionEl:
              "Special discounts on a wide range of services and products that are supplied by distinguished partners and famous brands in various sectors",
            iconE1: `${ithra}`,
          },
          {
            headingEl: "Motor Vehicle Periodic Inspection",
            discrptionEl: "Motor Vehicle Periodic Inspection",
            iconE1: `${motorInspect}`,
          },
        ],
      },
      {
        id: 1,
        pillName: "MOTOR",
        serviceCardData: [
          {
            headingEl: "Tawuniya Drive",
            discrptionEl:
              "It mesures your driving behavior and rewards you weekly,",
            iconE1: `${tawuniyaDrive}`,
          },
          {
            headingEl: "Road Side Assistance",
            discrptionEl: "Our services can be trustable, honest and worthy",
            iconE1: `${roadSide}`,
          },
          {
            headingEl: "Motor Vehicle Periodic Inspection",
            discrptionEl: "Motor Vehicle Periodic Inspection",
            iconE1: `${motorInspect}`,
          },
          {
            headingEl: "Free Car Maintenance trips to Agency",
            discrptionEl:
              "Towing vehicle from a place you choose to your car’s dealer and returning your vehicle back for Free",
            iconE1: `${freeCarMaintainence}`,
          },
          {
            headingEl: "End to end Claim service",
            discrptionEl: "---------------------------------------------------",
            iconE1: `${endtoendClaim}`,
          },
          {
            headingEl: "Ithra’a",
            discrptionEl:
              "Special discounts on a wide range of services and products that are supplied by distinguished partners and famous brands in various sectors",
            iconE1: `${ithra}`,
          },
          {
            headingEl: "Tawuniya Drive",
            discrptionEl:
              "It mesures your driving behavior and rewards you weekly,",
            iconE1: `${tawuniyaDrive}`,
          },
          {
            headingEl: "Road Side Assistance",
            discrptionEl: "Our services can be trustable, honest and worthy",
            iconE1: `${roadSide}`,
          },
          {
            headingEl: "Motor Vehicle Periodic Inspection",
            discrptionEl: "Motor Vehicle Periodic Inspection",
            iconE1: `${motorInspect}`,
          },
          {
            headingEl: "Free Car Maintenance trips to Agency",
            discrptionEl:
              "Towing vehicle from a place you choose to your car’s dealer and returning your vehicle back for Free",
            iconE1: `${freeCarMaintainence}`,
          },
          {
            headingEl: "End to end Claim service",
            discrptionEl: "---------------------------------------------------",
            iconE1: `${endtoendClaim}`,
          },
          {
            headingEl: "Ithra’a",
            discrptionEl:
              "Special discounts on a wide range of services and products that are supplied by distinguished partners and famous brands in various sectors",
            iconE1: `${ithra}`,
          },
        ],
      },
      {
        id: 2,
        pillName: "MEDICAL",
        serviceCardData: [
          {
            headingEl: "Visit Visa Extension Cancellation",
            discrptionEl:
              "A service that enables the visitor to cancel visit visa extension",
            iconE1: `${tawuniyaDrive}`,
          },
          {
            headingEl: "Visit Visa Extension",
            discrptionEl:
              "A service that enables the visitor to extend his policy",
            iconE1: `${chronic}`,
          },
          {
            headingEl: "Chronic Disease Management",
            discrptionEl: "A program that serves people with Chronic Disease",
            iconE1: `${eligibility}`,
          },
          {
            headingEl: "Pregnanacy Follow-up Program",
            discrptionEl: "A program that serves Pregnant woman",
            iconE1: `${pregnancy}`,
          },
          {
            headingEl: "Request For Eligibility Letter",
            discrptionEl:
              "A letter issues from Tawuniya explainiing your entitlement to treatment in case of any accidental problem",
            iconE1: `${ithra}`,
          },
          {
            headingEl: "Home Children Vaccination",
            discrptionEl:
              "A service that enables a member to vaccinehis child from home",
            iconE1: `${vaccinate}`,
          },
          {
            headingEl: "Tawuniya Drive",
            discrptionEl:
              "It mesures your driving behavior and rewards you weekly,",
            iconE1: `${tawuniyaDrive}`,
          },
          {
            headingEl: "Chronic Disease Management",
            discrptionEl: "A program that serves people with Chronic Disease",
            iconE1: `${chronicDisease}`,
          },
          {
            headingEl: "Request for Eligibility Letter",
            discrptionEl:
              "A letter issues from Tawuniya explaining your entitlement to treatment in case of any accidental problem",
            iconE1: `${eligibility}`,
          },
          {
            headingEl: "Road Side Assistance",
            discrptionEl: "Our services can be trustable, honest and worthy",
            iconE1: `${roadSide}`,
          },
          {
            headingEl: "Ithra’a",
            discrptionEl:
              "Special discounts on a wide range of services and products that are supplied by distinguished partners and famous brands in various sectors",
            iconE1: `${ithra}`,
          },
          {
            headingEl: "Motor Vehicle Periodic Inspection",
            discrptionEl: "Motor Vehicle Periodic Inspection",
            iconE1: `${motorInspect}`,
          },
        ],
      },
      {
        id: 3,
        pillName: "TRAVEL",
        serviceCardData: [
          {
            headingEl: "Travel insurance including Covid-19 cover",
            discrptionEl:
              "It is a new insurance product for Scitizens under the age of of 18",
            iconE1: `${covid}`,
          },
          {
            headingEl: "Motor Vehicle Periodic Inspection",
            discrptionEl: "Motor Vehicle Periodic Inspection",
            iconE1: `${motorInspect}`,
            hideCard: true,
          },
          {
            headingEl: "Motor Vehicle Periodic Inspection",
            discrptionEl: "Motor Vehicle Periodic Inspection",
            iconE1: `${motorInspect}`,
            hideCard: true,
          },
          {
            headingEl: "Motor Vehicle Periodic Inspection",
            discrptionEl: "Motor Vehicle Periodic Inspection",
            iconE1: `${motorInspect}`,
            hideCard: true,
          },
          {
            headingEl: "Motor Vehicle Periodic Inspection",
            discrptionEl: "Motor Vehicle Periodic Inspection",
            iconE1: `${motorInspect}`,
            hideCard: true,
          },
          {
            headingEl: "Motor Vehicle Periodic Inspection",
            discrptionEl: "Motor Vehicle Periodic Inspection",
            iconE1: `${motorInspect}`,
            hideCard: true,
          },
          {
            headingEl: "Motor Vehicle Periodic Inspection",
            discrptionEl: "Motor Vehicle Periodic Inspection",
            iconE1: `${motorInspect}`,
            hideCard: true,
          },
          {
            headingEl: "Motor Vehicle Periodic Inspection",
            discrptionEl: "Motor Vehicle Periodic Inspection",
            iconE1: `${motorInspect}`,
            hideCard: true,
          },
        ],
      },
    ],
  },
];
