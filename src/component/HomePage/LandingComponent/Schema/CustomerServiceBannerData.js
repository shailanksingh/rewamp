import serviceIcons1 from "../../../../assets/svg/TawuniyaServicesIcons/serviceIcons1.svg";
import serviceIcons2 from "../../../../assets/svg/TawuniyaServicesIcons/serviceIcons2.svg";
import serviceIcons3 from "../../../../assets/svg/TawuniyaServicesIcons/serviceIcons3.svg";
import serviceIcons4 from "../../../../assets/svg/TawuniyaServicesIcons/serviceIcons4.svg";
import serviceIcons5 from "../../../../assets/svg/TawuniyaServicesIcons/serviceIcons5.svg";
import serviceIcons6 from "../../../../assets/svg/TawuniyaServicesIcons/serviceIcons6.svg";

export const CustomerServiceBannerData = [
  {
    serviceTitle: "We are always here for you!",
    serviceSubtitle: "We provide the best and trusted service for our customer",
    Services: [
      "ALL SERVICE",
      "MOTOR",
      "PROPERTY & CASUALTY",
      "MEDICAL & TAKAFUL",
    ],
    serviceCardData: [
      {
        headingEl: "Road Side Assistance",
        discrptionEl: "Our services can be trustable, honest and worthy",
        iconE1: `${serviceIcons1}`,
      },
      {
        headingEl: "Request Telemedicine",
        discrptionEl: "Our services can be trustable, honest. ",
        iconE1: `${serviceIcons2}`,
      },
      {
        headingEl: "Report a Theft",
        discrptionEl: "Our services can be trustable, honest.",
        iconE1: `${serviceIcons3}`,
      },
      {
        headingEl: "Flight Delay Claim",
        discrptionEl: "Our services can be trustable, honest and worthy",
        iconE1: `${serviceIcons4}`,
      },
      {
        headingEl: "Assist in Accident",
        discrptionEl: "Our services can be trustable, honest and worthy",
        iconE1: `${serviceIcons5}`,
      },
      {
        headingEl: "Assist in medical",
        discrptionEl: "Our services can be trustable, honest and worthy",
        iconE1: `${serviceIcons6}`,
      },
      {
        headingEl: "Report a Theft",
        discrptionEl: "Our services can be trustable, honest and worthy",
        iconE1: `${serviceIcons3}`,
      },
      {
        headingEl: "Assist in Accident",
        discrptionEl: "Our services can be trustable, honest and worthy",
        iconE1: `${serviceIcons5}`,
      },
      {
        headingEl: "Assist in medical",
        discrptionEl: "Our services can be trustable, honest and worthy",
        iconE1: `${serviceIcons6}`,
      },
      {
        headingEl: "Request Telemedicine",
        discrptionEl: "Our services can be trustable, honest.",
        iconE1: `${serviceIcons2}`,
      },
      {
        headingEl: "Road Side Assistance",
        discrptionEl: "Our services can be trustable, honest and worthy",
        iconE1: `${serviceIcons1}`,
      },
      {
        headingEl: "Flight Delay Claim",
        discrptionEl: "Our services can be trustable, honest and worthy",
        iconE1: `${serviceIcons4}`,
      },
    ],
  },
];
