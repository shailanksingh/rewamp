import React, { useState } from "react";

import HeaderStickyMenu from "component/common/MobileReuseable/HeaderStickyMenu";
import HeaderStepsSticky from "component/common/MobileReuseable/HeaderStepsSticky";
import Motormobilecard from "component/common/motormobilecard/motormobilecard";
import MobileMotorTable from "component/common/MobileMotorTable";





import "./style.scss";

export const MotorPageMobile = () => {


    return(
        <div className="main_motor">
            <HeaderStickyMenu/>
            <HeaderStepsSticky title="Motor Insurance" buttonTitle="Buy Now" />

            <div className="container">
            <h4 className="fw-800  pt-4">Get insured within minutes!</h4>

            <Motormobilecard/>
             {/* <MobileMotorTable/> */}
            </div>
           


            
        </div>
    )


}
