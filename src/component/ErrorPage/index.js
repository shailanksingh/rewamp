import HeaderStickyMenu from 'component/common/MobileReuseable/HeaderStickyMenu';
import errorimg from "assets/images/mobile/errorimg.png";
import React from 'react';
import FooterMobile from 'component/common/MobileReuseable/FooterMobile';


const PageError = () => {
  return (
    <div>
        <HeaderStickyMenu/>
        <img src={errorimg} className="w-100" alt="..."/>
        <FooterMobile/>
    </div>
  )
}

export default PageError;
