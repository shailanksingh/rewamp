import React, { useState, useEffect } from "react";
import "./motormobile.scss";

import { CommonFaq } from "component/common/CommonFaq";
import { customerServiceFaqList } from "component/common/MockData";
import { InsuranceCardMobile } from "component/common/MobileReuseable/InsuranceCardMobile";
import BottomPopup from "component/common/MobileReuseable/BottomPopup";
import HeaderBackNav from "component/common/MobileReuseable/HeaderBackNav";
import MedicalFraudForm from "component/common/MedicalFraudForm";
import ReportViolationMobileForm from "component/common/ReportViloationMobile";

import right_arrow from "assets/images/mobile/right_arrow.png";
import tele_medicine from "assets/images/mobile/tele_medicine.png";
import bail_bond from "assets/images/mobile/bail_bond.png";
import motor_road from "assets/news/motor_road.png";
import car_maintenance from "assets/news/car_maintenance.png";
import car_accident from "assets/news/car_accident.png";

export const MotorFraudMobile = ({ arrayList, id }) => {
	const [openModal, setOpenModal] = useState(false);

	var data;

	if (id === 3) {
		data = {
			title: "Report Motor Fraud",
			content:
				"Any intentional act or accident by the owner of the insured vehicle to the vehicle or distortion or concealment of the claim documents, or/and deliberately providing false information from the vehicle insurance card holder, a third party, or a repair service provider, to obtain compensation or benefits, not due to them or others",
		};
	}
	if (id === 4) {
		data = {
			title: "Report Medical Fraud",
			content:
				"Medical insurance fraud is an intentional act by the cardholder, insured, and medical services provider to obtain not owed compensation or benefits to them or others through the deceiving, concealing, and/or misrepresenting information.",
		};
	}
	if (id === 5) {
		data = {
			title: "Property and Casualty Fraud",
			content:
				"Any intentional act committed by one of the parties in the insurance contract to obtain undue indemnities or benefits to them or others through deception and/or concealment of required documents and/or misrepresentation of information",
		};
	}
	if (id === 6) {
		data = {
			title: "Report Violation",
			content:
				"Please fill out the form below to submit your request and we will start our review on it immediately.",
		};
	}

	const Showmodal = () => {
		setOpenModal(true);
	};
	return (
		<div className="container pt-4 motor-fraud">
			<div className="card container motor_total">
				{arrayList?.map((ele) => (
					<>
						<img src={ele.head_image} alt="innercar" />
						<h4 className="mt-3 report_font">{ele.label}</h4>
						<p>{ele.content}</p>
					</>
				))}
			</div>
			{arrayList[0].list.map((ele) => (
				<>
					<div className="list_para">
						<h5 className="negative_header mt-4">{ele.title}</h5>
						{ele?.para && <p>{ele.para}</p>}
						{ele?.listPoints?.map((item) => (
							<li>{item}</li>
						))}
						{ele?.isCard === true && (
							<InsuranceCardMobile
								heathInsureCardData={insuranceCardData}
								isArrow={true}
							/>
						)}
					</div>
				</>
			))}
			<h5 className="negative_header mt-4">Frequently Asked Questions</h5>
			<p className="text-muted">Review commonly asked questions</p>
			<div className="faq-list">
				<CommonFaq faqList={customerServiceFaqList} />
			</div>
			<div className="card container-fluid report_button align_sticky">
				<button onClick={Showmodal}>Report Motor fraud</button>
			</div>

			<BottomPopup bg={"gray"} open={openModal} setOpen={setOpenModal}>
				<HeaderBackNav pageName="Customer Service" title={data.title} />
				{id === 6 ? (
					<>
						<ReportViolationMobileForm />
					</>
				) : (
					<>
						<MedicalFraudForm data={data} />
					</>
				)}
			</BottomPopup>
		</div>
	);
};

const insuranceCardData = [
	{
		id: 1,
		content: "Periodic Inspection",
		cardIcon: bail_bond,
	},
	{
		id: 2,
		content: "Road Assistance",
		cardIcon: motor_road,
	},
	{
		id: 3,
		content: "Car Maintenance",
		cardIcon: car_maintenance,
	},
	{
		id: 4,
		content: "Car Accident",
		cardIcon: car_accident,
	},
	{
		id: 5,
		content: "Car Wash",
		cardIcon: tele_medicine,
	},
	{
		id: 6,
		content: "Refill Medication",
		cardIcon: tele_medicine,
	},
	{
		id: 7,
		content: "Medical Reimbursement",
		cardIcon: tele_medicine,
	},
	{
		id: 9,
		content: "Eligibility letter",
		cardIcon: tele_medicine,
	},
	{
		id: 10,
		content: "Pregnancy Program",
		cardIcon: tele_medicine,
	},
	{
		id: 11,
		content: "Chronic Disease Management",
		cardIcon: tele_medicine,
	},
	{
		id: 12,
		content: "Home Child Vaccination",
		cardIcon: tele_medicine,
	},
	{
		id: 15,
		content: "Request Bailbond",
		cardIcon: bail_bond,
	},
];
