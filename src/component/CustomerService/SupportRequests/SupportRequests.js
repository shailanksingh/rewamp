import React from "react";
import {
  FraudBanner,
  FraudAssociates,
  Faq,
  LegendarySupport,
} from "component/common/FraudDetails";
import {
  bannerMedicalData,
  legendaryMedicSupportData,
  faqMedicData,
  faqContainerMedicData,
  associateMedicData,
} from "component/common/MockData/index";
import "./style.scss";

export const SupportRequests = () => {
  return (
    // medical fraud container starts here
    <React.Fragment>
      <FraudBanner bannerData={bannerMedicalData} />
      <FraudAssociates associateData={associateMedicData} />
      <Faq faqData={faqMedicData} faqContainerData={faqContainerMedicData} />
      <LegendarySupport
        legendarySupportData={legendaryMedicSupportData}
        needHeader={true}
      />
    </React.Fragment>
    // medical fraud container ends here
  );
};
