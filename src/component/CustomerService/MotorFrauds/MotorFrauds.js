import React from "react";
import {
	FraudBanner,
	FraudAssociates,
	Faq,
	
	LegendarySupport,
} from "component/common/FraudDetails";
import {
	bannerMotorData,
	legendaryMotorSupportData,
	faqMotorData,
	faqContainerMotorData,
	associateMotorData,
} from "component/common/MockData/index";
import "./style.scss";




export const MotorFrauds = () => {
	return (
		// motor fraud conatiner starts here
		<React.Fragment>
			<FraudBanner bannerData={bannerMotorData} />
			<FraudAssociates associateData={associateMotorData} />
			<Faq faqData={faqMotorData} faqContainerData={faqContainerMotorData} />
			<LegendarySupport
				legendarySupportData={legendaryMotorSupportData}
				needHeader={true}
				
			/>	
		</React.Fragment>
		// motor fraud conatiner ends here
	);
};
