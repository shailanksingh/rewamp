import React from "react";
import { Typography, Button } from "@material-ui/core";
import "./index.scss";

import ArrowForward from "../../../assets/svg/customerBannerForwardArrow.svg";
import { contains } from "validate.js";

import InsuranceBackShiled from "../../../assets/svg/InsuranceSvg/InsuranceBackShiled.svg";
import InsuranceFrontShiled from "../../../assets/svg/InsuranceSvg/InsuranceFrontShiled.svg";
import InsurancesmallFrontShiled from "../../../assets/svg/InsuranceSvg/InsurancesmallFrontShiled.svg";

function InsuranceFraud() {
  return (
    <div className="InsuranceFraudContainer">
      <div>
        <Typography
          component={"h1"}
          style={{ fontWeight: "800", fontSize: "40px" }}
        >
          Insurance Fraud
        </Typography>
        <Typography
          component={"p"}
          style={{
            width: "633px",
            fontSize: "18px",
            fontWeight: "400",
            lineHeight: "25.2px",
            marginBottom: "35px",
          }}
        >
          The insurance business operates based on a set of basic principles,
          most notably the principle of utmost good faith from all insurance
          process-related parties. However, the fraud by any party may occur and
          affect the other parties.
        </Typography>
        <Typography
          component={"p"}
          style={{
            fontSize: "18px",
            fontWeight: "400",
          }}
        >
          Learn More about:
        </Typography>

        <Typography
          component={"div"}
          style={{ display: "flex", marginBottom: "20px" }}
        >
          <Typography
            component={"div"}
            style={{ display: "flex", marginRight: "20px" }}
          >
            <Typography
              component={"p"}
              style={{
                color: "#EE7500",
                fontWeight: "800",
                fontSize: "16px",
                marginRight: "10px",
              }}
            >
              medical fraud
            </Typography>
            <img src={ArrowForward} alt="ArrowForward" />
          </Typography>
          <Typography
            component={"div"}
            style={{ display: "flex", marginRight: "20px" }}
          >
            <Typography
              component={"p"}
              style={{
                color: "#EE7500",
                fontWeight: "800",
                fontSize: "16px",
                marginRight: "10px",
              }}
            >
              Motor fraud
            </Typography>
            <img src={ArrowForward} alt="ArrowForward" />
          </Typography>
          <Typography
            component={"div"}
            style={{ display: "flex", marginRight: "20px" }}
          >
            <Typography
              component={"p"}
              style={{
                color: "#EE7500",
                fontWeight: "800",
                fontSize: "16px",
                marginRight: "10px",
              }}
            >
              P&C Insurance Fraud
            </Typography>
            <img src={ArrowForward} alt="ArrowForward" />
          </Typography>
        </Typography>
        <Button
          variant="contained"
          style={{ color: "#ffff", backgroundColor: "#EE7500" }}
        >
          Report a Fraud
        </Button>
      </div>
      <div>
        <div className="shieldContainer">
          <img className="backShield" src={InsuranceBackShiled} />
          <img className="backShield" src={InsuranceFrontShiled} />
          <img className="backShield" src={InsurancesmallFrontShiled} />
        </div>
      </div>
    </div>
  );
}

export default InsuranceFraud;
