import React, { useEffect, useState, useRef } from "react";
import { history } from "service/helpers";
import { Card } from "react-bootstrap";
import { NormalButton } from "component/common/NormalButton";
import OpenComplaintForm from "component/common/OpenComplaintForm";
import Phone from "assets/svg/phone.svg";
import Chat from "assets/svg/chaticon.svg";
import Communication from "assets/svg/communication.svg";
import "./style.scss";
import { SubReportViolation } from "./SubReportViolation";

const ReportViolation = ({
  isLayout,
  reportViolation,
  navToReportViolationForm,
  cardWidth,
  isMotor = false,
}) => {
  const [hideForm, setHideForm] = useState(false);
  const mailto = "mailto:Care@tawuniya.com.sa";
  useEffect(() => {
    const routeLink = history.location.pathname;
    if (routeLink === "/home/customerservice/violationspage") {
      setHideForm(true);
    }
  }, [hideForm]);

  return (
    <div className="container-fluid" style={{ marginBottom: "30px" }}>
      {hideForm && (
        <div className="reporting_channels">
          <h5>Reporting Channels</h5>
          <span>Contact the legendary support team right now</span>
        </div>
      )}

      {isLayout ? (
        <Card className="medical-card" id={cardWidth}>
          {isMotor ? (
            <div className="container-fluid">
              <div className="d-flex justify-content-center report-support-box">
                <div className="d-flex">
                  <div className="mt-3 pr-4">
                    <img src={Phone} alt="Phone" />
                  </div>
                  <div>
                    <p className="mainBannerPara text-left medical-ptag m-0 pt-3 fs-14 fw-400">
                      {" "}
                      Call Center
                    </p>
                    <h6 className="mainBannerHeading fs-18 fw-700">
                      800 124 9990
                    </h6>
                  </div>
                </div>
                <div className="px-3">
                  <div class="vertical-line "></div>
                </div>
                <div className="d-flex">
                  <div className="mt-3 pr-4">
                    <img src={Chat} alt="Chat" />
                  </div>
                  <div>
                    <p className="mainBannerPara text-left medical-ptag m-0 pt-3 fs-14 fw-400">
                      Whatsapp
                    </p>
                    <h6 className="mainBannerHeading text-left fs-18 fw-700 mb-0">
                      9200 19990
                    </h6>
                    <p className="mainBannerPara fs-14 fw-400">
                      This is a chat only number
                    </p>
                  </div>
                  <div className="ml-3">
                    <p className="mainBannerPara text-left medical-ptag pt-3 m-0 fs-14 fw-400">
                      {" "}
                      Chat with our executives
                    </p>
                    <h6 className="mainBannerHeading fs-18 fw-700 mb-0">
                      Start Live Chat
                      <label className="online_badge">Online</label>
                    </h6>
                  </div>
                </div>
                <div className="px-3">
                  <div class="vertical-lines"></div>
                </div>
                <div className="d-flex">
                  <div className="mt-3 pr-4">
                    <img
                      src={Communication}
                      className="cursor-pointer"
                      alt="email"
                      onClick={(e) => {
                        window.location = mailto;
                        e.preventDefault();
                      }}
                    />
                  </div>
                  <div>
                    <p className="mainBannerPara text-left medical-ptag pt-3 m-0 fs-14 fw-400">
                      Email
                    </p>
                    <h6
                      className="mainBannerHeading fs-18 fw-600 cursor-pointer"
                      onClick={(e) => {
                        window.location.href = mailto;
                        e.preventDefault();
                      }}
                    >
                      Care@tawuniya.com.sa
                    </h6>
                  </div>
                </div>
              </div>
            </div>
          ) : (
            <div className="container-fluid">
              <div className="d-flex justify-content-center report-support-box">
                <div className="d-flex">
                  <div className="mt-3 pr-4">
                    <img src={Phone} alt="Phone" />
                  </div>
                  <div>
                    <p className="mainBannerPara text-left medical-ptag m-0 pt-3 fs-14 fw-400">
                      {" "}
                      Call Center
                    </p>
                    <h6 className="mainBannerHeading fs-18 fw-700">
                      800 124 9990
                    </h6>
                  </div>
                </div>
                <div className="px-3">
                  <div class="vertical-line "></div>
                </div>
                <div className="d-flex">
                  <div className="mt-3 pr-4">
                    <img src={Chat} alt="Chat" />
                  </div>
                  <div>
                    <p className="mainBannerPara text-left medical-ptag m-0 pt-3 fs-14 fw-400">
                      Whatsapp
                    </p>
                    <h6 className="mainBannerHeading text-left fs-18 fw-700 mb-0">
                      9200 19990
                    </h6>
                    <p className="mainBannerPara fs-12 fw-400">
                      This is a chat only number
                    </p>
                  </div>
                  <div className="ml-3">
                    <p className="mainBannerPara text-left medical-ptag pt-3 m-0 fs-14 fw-400">
                      {" "}
                      Chat with our executives
                    </p>
                    <h6 className="mainBannerHeading fs-18 fw-700 mb-0">
                      Start Live Chat
                      <label className="online_badge">Online</label>
                    </h6>
                  </div>
                </div>
                <div className="px-3">
                  <div class="vertical-lines"></div>
                </div>
                <div className="d-flex">
                  <div className="mt-3 pr-4">
                    <img
                      src={Communication}
                      className="cursor-pointer"
                      alt="email"
                      onClick={(e) => {
                        window.location = mailto;
                        e.preventDefault();
                      }}
                    />
                  </div>
                  <div>
                    <p className="mainBannerPara text-left medical-ptag pt-3 m-0 fs-14 fw-400">
                      Email
                    </p>
                    <h6
                      className="mainBannerHeading fs-18 fw-600 cursor-pointer"
                      onClick={(e) => {
                        window.location.href = mailto;
                        e.preventDefault();
                      }}
                    >
                      Care@tawuniya.com.sa
                    </h6>
                  </div>
                </div>
              </div>
            </div>
          )}
        </Card>
      ) : (
        <SubReportViolation />
      )}

      {hideForm && (
        <React.Fragment>
          <div className="row">
            <div className="col-12 pt-4" id={navToReportViolationForm}>
              <p className="reportViolationTitle fw-800 text-center m-0">
                Report Now
              </p>
              <p className="reportViolationPara fs-16 fw-400 text-center m-0">
                Please fill out the form below to submit your request and we
                will start our review on it immediately.
              </p>
            </div>
          </div>
          <div className="row" ref={reportViolation}>
            <div className="col-12">
              <OpenComplaintForm needForm={true} />
            </div>
          </div>
        </React.Fragment>
      )}
    </div>
  );
};
export default ReportViolation;
