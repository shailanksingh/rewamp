import React from "react";
import { history } from "service/helpers";
import { Card } from "react-bootstrap";
import { NormalButton } from "component/common/NormalButton";
import Phone from "assets/svg/phone.svg";
import Chat from "assets/svg/chaticon.svg";
import Communication from "assets/svg/communication.svg";
import "./style.scss";

export const SubReportViolation = () => {
	return (
		<Card className="violation-medical-card pb-2">
			<div className="container-fluid">
				<div className="d-flex justify-content-between violation-report-support-box">
					<div className="d-flex alignViolationContent">
						<div className="mt-3 pr-4">
							<img src={Phone} alt="Phone" />
						</div>
						<div>
							<p className="violation-mainBannerPara violation-medical-ptag pt-3 mb-0 fs-14 fw-400">
								{" "}
								Call Center
							</p>
							<h6 className="violation-mainBannerHeading fs-18 fw-700">
								800 124 9990
							</h6>
						</div>
					</div>

					<div className="col-lg-0">
						<div class="vertical-line "></div>
					</div>

					<div className="d-flex alignViolationContent">
						<div className="mt-3 pr-4">
							<img src={Chat} alt="Chat" />
						</div>
						<div>
							<p className=" violation-medical-ptag pt-3 mb-0 fs-14 fw-400">
								Whatsapp
							</p>
							<h6 className="violation-mainBannerHeading text-left fs-18 fw-700 mb-0">
								9200 19990
							</h6>
							<p className="violation-mainBannerPara violation-mainBanner-Para fs-12 fw-400">
								This is a chat only number
							</p>
						</div>
						<div className="ml-3">
							<p className="violation-mainBannerPara violation-medical-ptag m-0 pt-3 fs-14 fw-400">
								{" "}
								Chat with our executives
							</p>
							<h6 className="violation-mainBannerHeading fs-18 fw-700 mb-0">
								Start Live Chat
								<label className="online_badge">Online</label>
							</h6>
						</div>
					</div>

					<div>
						<div class="vertical-line pt-5"></div>
					</div>

					<div className="d-flex alignViolationContent">
						<div className="mt-3 pr-4">
							<img src={Communication} alt="email" />
						</div>
						<div>
							<p className="violation-mainBannerPara violation-medical-ptag pt-3 mb-0 fs-14 fw-400">
								Email
							</p>
							<h6 className="violation-mainBannerHeading fs-18 fw-600">
								Care@tawuniya.com.sa
							</h6>
						</div>
					</div>

					<div className="border_or_content">
						<label>
							<span>OR</span>
						</label>
					</div>

					<div className="pt-4">
						<NormalButton
							label="Open a Support Request"
							className="violation-landingSupportBtn"
							onClick={() =>
								history.push("/home/customerservice/opencomplaint")
							}
						/>
					</div>
				</div>
			</div>
		</Card>
	);
};
