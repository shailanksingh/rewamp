import React, { useEffect, useState, useRef } from "react";
import { Link } from "react-scroll";
import { NormalButton } from "component/common/NormalButton";
import customerHome from "assets/svg/customerHome.svg";
import customerBreadCrumb from "assets/svg/customerBreadCrumb.svg";
import "./style.scss";
import "./ViolationPage-MediaQuery.scss";
import { violationsBannerData } from "component/common/MockData/ViolationsBannerData";
import FitContentIctxtCard from "component/common/FitContentTextCard";
import {
	violationsObCardTop,
	violationsObCardBtm,
} from "../../../common/MockData/index";
import add from "assets/svg/add.svg";
import Phone from "assets/svg/phone.svg";
import Chat from "assets/svg/chaticon.svg";
import Communication from "assets/svg/communication.svg";
import { history } from "service/helpers";

import { Card, Button, Accordion } from "react-bootstrap";
import OpenComplaintForm from "component/common/OpenComplaintForm";
import ReportViolation from "./ReportViolation";

export const ViolationsPage = () => {
	const reportViolation = useRef(null);

	const scrollToDown = (ref) => {
		window.scrollTo({
			top: ref.current.offsetTop,
			behavior: "smooth",
		});
	};

	return (
		<>
			<div className="row ViolationsContainer">
				{violationsBannerData.map((item, index) => {
					return (
						<React.Fragment>
							<div className="col-lg-12 col-12 violationsBanner mt-2">
								<div className="violationsBannerLeft">
									<p className="fw-800 violationsTitle">
										{item.violationsTitle}
									</p>
									<p className="violationsParaOne fs-18 fw-400">
										{item.violationsPara1}
									</p>
									<p className="violationsParaOne fs-18 fw-400">
										{item.violationsPara2}
									</p>
									<ul className="p-0 pl-4">
										{item?.violationsList?.map((items, index) => {
											return (
												<li
													key={index}
													className="violationsParaTwo fs-18 fw-400"
												>
													{items.para}
												</li>
											);
										})}
									</ul>
									<div className="d-flex flex-row">
										<div>
											<NormalButton
												label={item.btnLabel}
												className="reportviolationsBtn p-4"
												onClick={() => scrollToDown(reportViolation)}
											/>
										</div>
										<div className="pl-3">
											<Link
												to="navToReportViolationForm"
												spy={true}
												smooth={true}
												duration={5000}
											>
												<NormalButton
													label="Learn More"
													className="learnMoreBtn p-4"
												/>
											</Link>
										</div>
									</div>
								</div>
								<div className="violationsBannerRight">
									<img
										src={item.bannerPic}
										className="img-fluid violationsBannerViolationsImg"
										alt="banner"
									/>
								</div>
							</div>
						</React.Fragment>
					);
				})}
			</div>
			<div className="violationsObligationsContainer">
				<div className="violationsObligations">
					<div className="violationsObText">
						<h5>Obligations of the whistleblower</h5>
						<p>
							The person reporting any violating case should comply with the
							following
						</p>
					</div>
					<div className="violationsObCardTop">
						{violationsObCardTop.map((item) => {
							const { cardIcon, cardTitle, cardPara, cardClass } = item;
							return (
								<FitContentIctxtCard
									alignfitCard={cardClass}
									cardIcon={cardIcon}
									cardTitle={cardTitle}
									cardPara={cardPara}
								/>
							);
						})}
					</div>
					<div className="violationsObText " id="violationsObText">
						<h5>
							General obligations to protect the person reporting the violation
						</h5>
						<p>
							The Company represented by the Violation Handing Unit, shall
							comply with the following in the event it receives any violation
							reporting
						</p>
					</div>
					<div className="violationsObCardBtm">
						{violationsObCardBtm.map((item) => {
							const { cardIcon, cardTitle, cardPara, cardClass } = item;
							return (
								<FitContentIctxtCard
									alignfitCard={cardClass}
									cardIcon={cardIcon}
									cardTitle={cardTitle}
									cardPara={cardPara}
								/>
							);
						})}
					</div>
				</div>
			</div>
			<div>
				<ReportViolation
					isLayout={true}
					reportViolation={reportViolation}
					navToReportViolationForm="navToReportViolationForm"
				/>
			</div>
		</>
	);
};
