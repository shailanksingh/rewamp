import React from "react";
import {
  FraudBanner,
  FraudAssociates,
  Faq,
  LegendarySupport,
} from "component/common/FraudDetails";
import {
  bannerTravelData,
  legendaryTravelSupportData,
  faqTravelData,
  faqContainerTravelData,
  associateTravelData,
  associateTravelNewData,
} from "component/common/MockData/index";
import "./style.scss";

export const TravelFrauds = () => {
  return (
    // travel fraud conatiner starts here
    <React.Fragment>
      <FraudBanner bannerData={bannerTravelData} />
      <FraudAssociates
        associateData={associateTravelData}
        needData={true}
        associateAddedData={associateTravelNewData}
      />
      <Faq faqData={faqTravelData} faqContainerData={faqContainerTravelData} />
      <LegendarySupport legendarySupportData={legendaryTravelSupportData} needHeader={true} />
    </React.Fragment>
    // travel fraud conatiner ends here
  );
};
