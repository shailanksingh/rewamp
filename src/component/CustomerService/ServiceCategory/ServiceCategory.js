import React, { useState } from "react";
import ReportViolation from "../Violations/ViolationsPage/ReportViolation";
import { NormalSearch } from "component/common/NormalSearch";
import { ServiceCard } from "component/common/ServiceCard";
import { Faq } from "component/common/FraudDetails";
import { serviceCategoryData } from "component/common/MockData/index";
import { faqCategoryData } from "component/common/MockData/index";
import "./style.scss";

export const ServiceCategory = () => {
  const [search, setSearch] = useState("");

  const handleInputSearch = (e) => {
    setSearch(e.target.value);
  };

  return (
    <div className="row serviceCategoryContainer">
      <div className="col-lg-12 col-12 pt-5">
        <p className="fw-800 category-assisst-header text-center m-0">
          How Can We Assist You?
        </p>
        <div className="d-flex justify-content-center">
          <div>
            <p className="fs-16 fw-400 category-assisst-para text-center m-0">
              Here you will find the answers to many of the questions you may
              have,
            </p>
            <p className="fs-16 fw-400 category-assisst-para text-center">
              including information about our comprehensive range of your Motor
              insurance.
            </p>
          </div>
        </div>
      </div>
      <div className="col-lg-12 col-12">
        <div className="d-flex justify-content-center pt-3">
          <div>
            <NormalSearch
              className="headerCategorySearch"
              name="search"
              value={search}
              placeholder="Search for topics"
              onChange={handleInputSearch}
              needRightIcon={true}
            />
          </div>
        </div>
      </div>
      <div className="col-lg-12 col-12 pt-4 mt-2">
        <ServiceCard serviceData={serviceCategoryData} />
      </div>
      <div className="col-lg-12 col-12 pt-4">
        <p className="fw-800 category-trendingFaq-title text-center m-0">
          Trending Topics
        </p>
        <p className="fs-16 fw-400 category-trendingFaq-para text-center pb-3">
          Review answers to commonly asked questions at Tawuniya, which enable
          you to be directly involved in improving our support experience.
        </p>
        <Faq faqData={faqCategoryData} />
      </div>
      <div className="col-lg-12 col-12 pt-5">
        <p className="fw-800 category-cantFind-title text-center m-0">
          Can't find what you're looking for?
        </p>
        <p className="fs-16 fw-400 category-cantFind-para text-center pb-3">
          Contact the legendary support team right now.
        </p>
        <ReportViolation />
      </div>
    </div>
  );
};
