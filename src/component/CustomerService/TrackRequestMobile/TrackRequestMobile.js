import React, { useState } from "react";
import HeaderStickyMenu from "component/common/MobileReuseable/HeaderStickyMenu";
import HeaderStepsSticky from "component/common/MobileReuseable/HeaderStepsSticky";
import { NormalButton } from "component/common/NormalButton";
import ArrowRight from "assets/images/mobile/arrow-right.png";
import PhoneNumberInput from "component/common/PhoneNumberInput";
import { dialingCodes } from "component/common/MockData";
import { NormalSearch } from "component/common/NormalSearch";
import { InsuranceCardMobile } from "component/common/MobileReuseable/InsuranceCardMobile";
import right_arrow from "assets/images/mobile/right_arrow.png";
import truck_black from "assets/images/mobile/truck_black.png";
import tele_medicine from "assets/images/mobile/tele_medicine.png";
import flightdelay from "assets/images/mobile/flightdelay.png";
import "../SupportCenterRequest/style.scss";
import "./trackRequestMobile.scss";
import FooterMobile from "component/common/MobileReuseable/FooterMobile";
import Accordion from 'react-bootstrap/Accordion';

const TrackRequestMobile = () => {
  const [helperFormValues, setHelperFormValues] = useState({ phoneNumber: "" });

  let [selectedCode, setSelectedCode] = useState(dialingCodes[0]);
  const handleInputChange = (e) => {
    const { name, value } = e.target || e || {};
    setHelperFormValues({ ...helperFormValues, [name]: value });
  };
  const { phoneNumber, userId } = helperFormValues;
  const insuranceCardData = [
    {
      id: 1,
      content: "Road Side Assistance",
      cardIcon: truck_black,
    },
    {
      id: 2,
      content: "Request Telemedicine",
      cardIcon: tele_medicine,
    },
    {
      id: 3,
      content: "Flight Delay Claim",
      cardIcon: flightdelay,
    },
    {
      id: 4,
      content: "Road Side Assistance",
      cardIcon: truck_black,
    },
    {
      id: 5,
      content: "Request Telemedicine",
      cardIcon: tele_medicine,
    },
  ];
  return (
    <div>
      <HeaderStickyMenu />
      <HeaderStepsSticky
        title={"Track Requests"}
        isSelect={true}
        selectTitle={"Support Request"}
      />
   
      <div className="support-overall">
        <div className="track-request">
          <h6>SHOWING 1–30 OF 58 RESULTS</h6>
          
      <Accordion defaultActiveKey="0" flush>
      <Accordion.Item eventKey="0">
      <Accordion.Header> 
        <div className="track-card">
            <div>
              <h5>#COM-0927431</h5>
              <p>12 Dec 2021</p>
            </div>
            <div className="position-relative">
            <a>Under Processing</a> 
            </div>
          </div>
          </Accordion.Header>
        <Accordion.Body alwaysOpen="false">
        <div className="track-tab-body">
            
            <h3>Complain Details</h3>
            <ul>
              <li>
                <div>Complaint number</div>
                <div>#COM-0927431</div>
              </li>
              <li className="active">
                <div>Complaint Submission Date</div>
                <div>12 Dec 2021</div>
              </li>
              <li>
                <div>Complaint Closure Date </div>
                <div>14 Dec 2021</div>
              </li>
              <li className="active">
                <div>Complaint Category</div>
                <div>Motor</div>
              </li>
              <li>
                <div>Care Owner Details</div>
                <div>Care Owner Details</div>
              </li> 
            </ul>
          </div>
        </Accordion.Body>
      </Accordion.Item>
    </Accordion>


    <Accordion defaultActiveKey="0">
      <Accordion.Item eventKey="0">
      <Accordion.Header>
        <div className="track-card">
            <div>
              <h5>#COM-0927431</h5>
              <p>12 Dec 2021</p>
            </div>
            <div className="position-relative">
             <a className="success text-decoration-none">Resolved</a> 
            </div>
          </div>
          </Accordion.Header>
        <Accordion.Body>
        <div className="track-tab-body">
            
            <h3>Complain Details</h3>
            <ul>
              <li>
                <div>Complaint number</div>
                <div>#COM-0927431</div>
              </li>
              <li className="active">
                <div>Complaint Submission Date</div>
                <div>12 Dec 2021</div>
              </li>
              <li>
                <div>Complaint Closure Date </div>
                <div>14 Dec 2021</div>
              </li>
              <li className="active">
                <div>Complaint Category</div>
                <div>Motor</div>
              </li>
              <li>
                <div>Care Owner Details</div>
                <div>Care Owner Details</div>
              </li> 
            </ul>
          </div>
        </Accordion.Body>
      </Accordion.Item>
    </Accordion>
    
    <Accordion defaultActiveKey="0" className="clickcard">
      <Accordion.Item eventKey="0">
      <Accordion.Header>
        <div className="track-card">
            <div>
              <h5>#COM-0927431</h5>
              <p>12 Dec 2021</p>
            </div>
            <div className="position-relative">
            <a className="success text-decoration-none">Resolved</a> 
            </div>
          </div>
          </Accordion.Header>
        <Accordion.Body>
        <div className="track-tab-body">
            
            <h3>Complain Details</h3>
            <ul>
              <li>
                <div>Complaint number</div>
                <div>#COM-0927431</div>
              </li>
              <li className="active">
                <div>Complaint Submission Date</div>
                <div>12 Dec 2021</div>
              </li>
              <li>
                <div>Complaint Closure Date </div>
                <div>14 Dec 2021</div>
              </li>
              <li className="active">
                <div>Complaint Category</div>
                <div>Motor</div>
              </li>
              <li>
                <div>Care Owner Details</div>
                <div>Care Owner Details</div>
              </li> 
            </ul>
          </div>
        </Accordion.Body>
      </Accordion.Item>
    </Accordion>

        </div>

        <div className="support-input-card pt-4 mt-4">
          <div className="support-card">
            <h5>Find your Support Request</h5>
            <p>
              It looks like you are searching for a previous conversation with
              Tawuniya Support.
            </p>
            <div className="landing-input">
              <NormalSearch
                className="loginInputFieldOne"
                name="userId"
                value={userId}
                placeholder="Saudi ID / IQAMA"
                onChange={handleInputChange}
                needLeftIcon={false}
              />
            </div>

            <div class="landing-input">
              <PhoneNumberInput
                className="complaintPhoneInput"
                selectInputClass="complaintSelectInputWidth"
                selectInputFlexType="complaintSelectFlexType"
                dialingCodes={dialingCodes}
                selectedCode={selectedCode}
                setSelectedCode={setSelectedCode}
                value={phoneNumber}
                name="phoneNumber"
                onChange={handleInputChange}
                isCodeFalse={true}
                placeholder="ex: 5xxxxxxxx"
              />
            </div>

            <div className="terms">
              <div>
                <button className="buy-button fs-12 w-100">Buy Now</button>
              </div>
            </div>
          </div>
        </div>

        <h5 className="find">Can't find what you're looking for?</h5>
        <div className="support-team">
          <h6>Contact Our Support Team</h6>
          <p>We're here to help, 24 hours a day, 7 days a week.</p>
          <div className="border_support" />
          <div className="position-relative">
            <NormalButton label="Open a Complaint" className="insureNowBtn" />
            <img src={ArrowRight} className="arrow"></img>
          </div>
        </div>
        <div className="support-team live">
          <div className="d-flex justify-content-between title">
            <h6>Less Phone, More Support</h6>
            <a>Online</a>
          </div>
          <p>
            We don’t want to keep you hanging, waiting for someone to answer the
            phone.
          </p>
          <div className="border_support" />
          <div className="position-relative">
            <NormalButton label="Start Live Chat" className="insureNowBtn" />
            <img src={ArrowRight} className="arrow"></img>
          </div>
        </div>
        <div className="service_list request">
          <h5>Products & Services Support</h5>
          <InsuranceCardMobile heathInsureCardData={insuranceCardData} />
          <div className="view_all_question justify-content-start">
            <label>View All Products & Services</label>
            <img src={right_arrow} alt="Arrow" />
          </div>
        </div>
      </div>
      <FooterMobile />
    </div>
  );
};

export default TrackRequestMobile;
