import React from "react";
import { RequestSupportComponent } from "component/common/RequestSupportComponent";
import "./style.scss";

export const SupportRequest = () => {
  return (
    // opencomplaint container starts here
    <div className="row">
      <div className="col-12 pt-5">
        <RequestSupportComponent />
      </div>
    </div>
    // open complaint container ends here
  );
};
