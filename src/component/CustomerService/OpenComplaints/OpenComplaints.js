import React from "react";
import { SupportRequest } from "./OpenComplaintsComponent";

export const OpenComplaints = () => {
  return (
    // open complaint container starts here
    <React.Fragment>
      <SupportRequest />
    </React.Fragment>
    // open complaint container ends here
  );
};
