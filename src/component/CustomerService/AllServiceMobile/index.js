import BottomPopup from "component/common/MobileReuseable/BottomPopup";
import React, { useEffect, useState } from "react";
import ServicePopup from "./ServicePopup";
import HeaderStickyMenu from "component/common/MobileReuseable/HeaderStickyMenu";
import HeaderStepsSticky from "component/common/MobileReuseable/HeaderStepsSticky";
import "./style.scss";
import SupportService from "./supportService";
import SupportCenterRequest from "../SupportCenterRequest/SupportCenterRequest";
import { MotorFraudMobile } from "../MotorFraudMobile/MotorFraudMobile";
import innercar from "assets/images/mobile/innercar.png";
import Medical_fraud from "assets/images/mobile/medical_fraud.png";
import Violation from "assets/images/mobile/violation.png";
import Surplus from "assets/images/mobile/surplus.png";

const AllServiceMobile = () => {
  const [isOpenServiceDetails, setIsOpenServiceDetails] = useState(false);
  const [supportContentLabel, setSupportContentLabel] = useState({});

  const [activeMenuOption, setActiveMenuOption] = useState({});

  const selectedMenuOption = (data) => {
    setActiveMenuOption(data);
  };

  const openServiceSupport = (data) => {
    setIsOpenServiceDetails(true);
    setSupportContentLabel(data);
  };

  useEffect(() => {
    setActiveMenuOption(supportMenuList[0]);
  }, []);
  const { id } = activeMenuOption;

  return (
    <div
      className={`${id !== 1 && "rewards_image_none"} all_service_container`}
    >
      <HeaderStickyMenu />
      <HeaderStepsSticky
        title={"Support center"}
        isSelect={true}
        selectTitle={activeMenuOption}
        menuList={supportMenuList}
        selectedMenuOption={(data) => selectedMenuOption(data)}
      />
      {id === 1 && <SupportService openServiceSupport={openServiceSupport} />}
      {id === 2 && <SupportCenterRequest />}
      {id === 3 && <MotorFraudMobile arrayList={motorFraudList} id={3} />}
      {id === 4 && <MotorFraudMobile arrayList={medicalFraudList} id={4} />}
      {id === 5 && <MotorFraudMobile arrayList={casualityFraudList} id={5} />}
      {id === 6 && <MotorFraudMobile arrayList={violationFraudList} id={6} />}
      {id === 7 && <MotorFraudMobile arrayList={surplusFraudList} id={6} />}

      <BottomPopup
        open={isOpenServiceDetails}
        setOpen={setIsOpenServiceDetails}
        bg="gray"
      >
        <ServicePopup supportData={supportContentLabel} />
      </BottomPopup>
    </div>
  );
};
export default AllServiceMobile;

const supportMenuList = [
  {
    id: 1,
    label: "Main Page",
  },
  {
    id: 2,
    label: "Support Requests",
  },
  {
    id: 3,
    label: "Motor Fraud",
  },
  {
    id: 4,
    label: "Medical Fraud",
  },
  {
    id: 5,
    label: "Property and Casualty Fraud",
  },
  {
    id: 6,
    label: "Violation",
  },
  {
    id: 7,
    label: "Surplus Insurance",
  },
];
const motorFraudList = [
  {
    id: 1,
    label: "Report Motor Fraud",
    head_image: innercar,
    content:
      "Motor Insurance Fraud is any intentional act committed by the insurance cardholder to obtain undue indemnities or benefits to him or to others through fraud and/ or concealment of required documents and / or misrepresentation of information.",
    list: [
      {
        title: "The negative impact of motor fraud",
        listPoints: [
          "High premiums.",
          "Cancellation of insurance policy.",
          "Cancellation of retirement with the service provider (agency or repair shop) if it is the source of fraud.",
          "Not receiving compensation.",
          "Taking legal actions as opposed to the violator.",
          "Take action to impose legal sanctions from the competent authorities.",
        ],
      },
      {
        title: "Fraud detection mechanisms in motor insurance",
        listPoints: [
          "The presence of a unit specialized in fraud in the insurance of motors.",
          "Implement an information system for fraud detection and control.",
          "Cooperating with the competent authorities to manage and supervise the insurance of motors such as Al Muroor, Najm Company and SAMA.",
          "Assign a fraud reporting email managed by the team of the competent anti-fraud unit.",
        ],
      },
    ],
  },
];
const medicalFraudList = [
  {
    id: 1,
    label: "Report Medical Fraud",
    head_image: Medical_fraud,
    content:
      "Medical insurance fraud is an intentional act by the cardholder, insured, and medical services provider to obtain not owed compensation or benefits to them or others through the deceiving, concealing, and/or misrepresenting information.",
    list: [
      {
        title: "The Negative Impacts of Medical Fraud",
        listPoints: [
          "Higher medical insurance premiums to meet the increase in the loss ratio.",
          "Stop issuing medical insurance policies to reduce fraud claims and loss of access to insurance coverage.",
          "Cancellation of the insurance policy.",
          "Cease of dealing with medical service providers and thus deprive patients of distinctive treatment services.          ",
          "Business financial losses resulting from fraudulent claims.",
          "Claim rejection.",
          "Lawful actions.",
          "Health harm to the insured due to tampering with his medical history if the medical card is used by others",
        ],
      },
      {
        title: "Your responsibilities to prevent fraud",
        listPoints: [
          " Be aware of attempts by medical providers and others to convince you that everybody else is profiting so you may as well try to reap the benefits of insurance fraud.",
          "To avoid unwanted medical procedures, always ask the doctor about the provided services and why you need them.",
          "Do not allow anyone to use your medical card.",
          "Carefully review your claims & bills before signing them, and question charges for procedures that were not provided.",
          "Inform and call Tawuniya when you detect or suspect fraud.",
        ],
      },
    ],
  },
];
const casualityFraudList = [
  {
    id: 1,
    label: "Property and Casualty Fraud",
    head_image: innercar,
    content:
      "The Company for Cooperative Insurance (Tawuniya) is committed to provide the highest quality services through ethical and fair practices for both its employees and business partners.",
    list: [
      {
        title: "Definition of Property & Casualty Insurance Fraud",
        para: "Property & Casualty insurance fraud is any intentional act committed by one of the parties in the insurance contract to obtain undue indemnities or benefits to them or to others through deception and/or concealment of required documents and/or misrepresentation of information.",
        listPoints: [
          "Misrepresentation of material facts might occur if an insurance proposer makes a false statement with the intention to deceive the insurer in order to obtain an unlawful gain, e.g. a discount on premium.          ",
          "Inflated Damage/Loss of property. This may occur after a fire incident and an insurance claim gets filed.          ",
          "Arson for Profit, dwellings or commercial properties are destroyed by fire for the sole purpose of financial gain.          ",
          "A claim is filed for a boat that sank, but the boat never actually existed. It is not difficult to register a boat based on a bill of sale.",
        ],
      },
      {
        title: "Negative Effects of Property & Casualty Insurance Fraud",
        listPoints: [
          "High premiums.",
          "Claim process duration increased due to investigations.",
          " Increase the deductible.",
        ],
      },
      {
        title: "Mechanisms of Detecting Property & Casualty Insurance Fraud",
        listPoints: [
          "High premiums.",
          "Claim process duration increased due to investigations.",
          " Increase the deductible.",
        ],
      },
      {
        title: "Preventing Property and Casualty Insurance Fraud",
        listPoints: [
          "High premiums.",
          "Claim process duration increased due to investigations.",
          " Increase the deductible.",
        ],
      },
      {
        title: "Rewards for Reporting Property and Casualty Insurance Fraud",
        listPoints: [
          "High premiums.",
          "Claim process duration increased due to investigations.",
          " Increase the deductible.",
        ],
      },
    ],
  },
];

const violationFraudList = [
  {
    id: 1,
    label: "Violation",
    head_image: Violation,
    content:
      "Any fraudulent acts, corruption, collusion, coercion, illegal behavior, misconduct, financial mismanagement, accounting abuses, the existence of a conflict of interest, any wrong behavior, illegal or unethical practices, or other violations of the applicable laws, regulations and instructions or cover up any of the above acts.The Company urges its employees and stakeholders not to hesitate to report any violations because they are not sure of the accuracy of the report and whether this allegation can be proven or not, and that all employees of the Company and stakeholders are expected to refrain from rumors, irresponsible behavior and false allegations, and if this allegation is made in good faith, but not confirmed in the investigation, no action will be taken against the whistleblower.",
    list: [
      {
        title: "Obligations Of The Whistleblower",
        para: "The person reporting any violating case should comply with the following",
        isCard: true,
      },
      {
        title:
          "General obligations to protect the person reporting the violation",
        para: "The Company represented by the Violation Handing Unit, shall comply with the following in the event it receives any violation reporting",
        isCard: true,
      },
    ],
  },
];
const surplusFraudList = [
  {
    id: 1,
    label: "Surplus Insurance",
    head_image: Surplus,
    content:
      "Dear Customers, According to Article (70) of the Implementing Regulations for Insurance Companies and the Surplus Distribution Policy, issued by the Saudi Central Bank (SAMA), we would like to inform you that the insurance operations surplus is now available for eligible policyholders in compliance with the terms and conditions required by law.",
    list: [
      {
        title: "Surplus Distribution Conditions",
        para: "The distribution of the surplus will happen after accounting for the claims paid to the policyholder and any other amounts owed by the customer in compliance with the terms and conditions required by law.",
      },
      {
        title: "Surplus Distribution Years",
        para: "Tawuniya will distribute the surplus from insurance operations to the eligible policyholders for the following years 2020، 2019، 2016، 2015، 2014",
      },
      {
        title: " How to get your share of the surplus",
        para: "To obtain your share of the surplus via direct transfer to your bank account, please follow the following steps:",
      },
    ],
  },
];
