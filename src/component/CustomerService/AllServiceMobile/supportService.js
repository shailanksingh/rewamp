import FooterMobile from "component/common/MobileReuseable/FooterMobile";
import SupportRequestHelper from "component/common/MobileReuseable/SupportRequest";
import React, { useState } from "react";
import search from "../../../assets/images/mobile/search.png";
import "./style.scss";
import { InsuranceCardMobile } from "component/common/MobileReuseable/InsuranceCardMobile";
import bail_bond from "assets/images/mobile/bail_bond.png";
import motor_road from "assets/news/motor_road.png";
import car_maintenance from "assets/news/car_maintenance.png";
import car_accident from "assets/news/car_accident.png";
import tele_med from "assets/images/mobile/tele_med.png";
import web_assist from "assets/images/mobile/web_assist.png";
import blue_flight_delay from "assets/images/mobile/blue_flight_delay.png";
import leftarrow from "assets/about/leftarrow.svg";
import { CommonFaq } from "component/common/CommonFaq";
import Right_Arrow from "assets/about/Right_Arrow.svg";
import Violations from "assets/images/mobile/Violations.svg";
import Track_claim from "assets/images/mobile/Track_claim.svg";
import Insurance_fraud from "assets/images/mobile/Insurance_fraud.svg";
import { history } from "service/helpers";

const SupportService = ({ openServiceSupport }) => {
  const [data, setData] = useState([
    {
      id: 1,
      content: "Travel Insurance",
    },
    {
      id: 2,
      content: "COVID-19",
    },
    {
      id: 3,
      content: "Malpractice",
    },
    {
      id: 4,
      content: "Visa Insurance",
    },
  ]);
  const [content, setContent] = useState([
    {
      id: 1,
      content: "Motor Support",
    },
    {
      id: 2,
      content: "Medical Support",
    },
    {
      id: 3,
      content: "Property & Casualty Support",
    },
  ]);
  const insuranceCardData = [
    {
      id: 1,
      content: "Periodic Inspection",
      cardIcon: bail_bond,
    },
    {
      id: 2,
      content: "Road Assistance",
      cardIcon: motor_road,
    },
    {
      id: 3,
      content: "Car Maintenance",
      cardIcon: car_maintenance,
    },
    {
      id: 4,
      content: "Car Accident",
      cardIcon: car_accident,
    },
    {
      id: 5,
      content: "Car Wash",
      cardIcon: tele_med,
    },
    {
      id: 6,
      content: "Refill Medication",
      cardIcon: tele_med,
    },
    {
      id: 7,
      content: "Medical Reimbursement",
      cardIcon: tele_med,
    },
    {
      id: 8,
      content: "Telemedicine",
      cardIcon: tele_med,
    },
    {
      id: 9,
      content: "Eligibility letter",
      cardIcon: tele_med,
    },
    {
      id: 10,
      content: "Pregnancy Program",
      cardIcon: tele_med,
    },
    {
      id: 11,
      content: "Chronic Disease Management",
      cardIcon: tele_med,
    },
    {
      id: 12,
      content: "Home Child Vaccination",
      cardIcon: tele_med,
    },
    {
      id: 13,
      content: "Assist America",
      cardIcon: web_assist,
    },
    {
      id: 14,
      content: "Flight Delay Assistance",
      cardIcon: blue_flight_delay,
    },
    {
      id: 15,
      content: "Request Bailbond",
      cardIcon: bail_bond,
    },
  ];
  const customerServiceFaqList = [
    {
      id: 1,
      question: "What is medical insurance?",
      answer:
        "The insured is provided with an Insurance Card and Bail Bond approved by the Traffic Police together with SANAD Insurance Policy to prevent the arrest or detainment of the driver causing the accident?",
    },

    {
      id: 2,
      question: "How can I obtain medical insurance?",
      answer:
        "The insured is provided with an Insurance Card and Bail Bond approved by the Traffic Police together with SANAD Insurance Policy to prevent the arrest or detainment of the driver causing the accident?",
    },

    {
      id: 3,
      question: "What are the required documents to acquire medical insurance?",
      answer:
        "The insured is provided with an Insurance Card and Bail Bond approved by the Traffic Police together with SANAD Insurance Policy to prevent the arrest or detainment of the driver causing the accident?",
    },
    {
      id: 4,
      question:
        "What How much does medical insurance cost?is medical fraud unit?",
      answer:
        "The insured is provided with an Insurance Card and Bail Bond approved by the Traffic Police together with SANAD Insurance Policy to prevent the arrest or detainment of the driver causing the accident?",
    },
    {
      id: 5,
      question: "What is medical insurance?",
      answer:
        "The insured is provided with an Insurance Card and Bail Bond approved by the Traffic Police together with SANAD Insurance Policy to prevent the arrest or detainment of the driver causing the accident?",
    },
    {
      id: 6,
      question: "How can I obtain medical insurance?",
      answer:
        "The insured is provided with an Insurance Card and Bail Bond approved by the Traffic Police together with SANAD Insurance Policy to prevent the arrest or detainment of the driver causing the accident?",
    },
    {
      id: 7,
      question: "What are the required documents to acquire medical insurance?",
      answer:
        "The insured is provided with an Insurance Card and Bail Bond approved by the Traffic Police together with SANAD Insurance Policy to prevent the arrest or detainment of the driver causing the accident?",
    },
    {
      id: 8,
      question:
        "What How much does medical insurance cost?is medical fraud unit?",
      answer:
        "The insured is provided with an Insurance Card and Bail Bond approved by the Traffic Police together with SANAD Insurance Policy to prevent the arrest or detainment of the driver causing the accident?",
    },
  ];
  const [insurance, setInsurance] = useState([
    {
      id: 1,
      content: "Medical Fraud ",
      image: Right_Arrow,
    },
    {
      id: 2,
      content: "Motor Fraud ",
      image: Right_Arrow,
    },
    {
      id: 3,
      content: "P&C Insurance Fraud ",
      image: Right_Arrow,
    },
    {
      id: 4,
      content: "Report a Fraud",
      image: Right_Arrow,
    },
  ]);
  const [claim, setClaim] = useState([
    {
      id: 1,
      content: "Motor Claims",
      image: Right_Arrow,
    },
    {
      id: 2,
      content: "Medical Claims",
      image: Right_Arrow,
    },
    {
      id: 3,
      content: "P&C Claims",
      image: Right_Arrow,
    },
    {
      id: 4,
      content: "Medical Malpractice Claims",
      image: Right_Arrow,
    },
    {
      id: 5,
      content: "Travel Insurance Claims",
      image: Right_Arrow,
    },
    {
      id: 6,
      content: "Track Claims",
      image: Right_Arrow,
    },
  ]);
  return (
    <div>
      <div className="all_title">How can we help you?</div>
      <div className="search_box_style">
        <p>Search for topics</p>
        <div>
          <img src={search} />
        </div>
      </div>
      <div className="all_popular">
        <label className="all_search">Popular Searches</label>
        <div className="all_data">
          {data.map((datas) => (
            <div className="all_content">{datas.content}</div>
          ))}
        </div>
      </div>
      <div className="all_blue">
        {content.map((contents) => (
          <div
            className="all_service_blue"
            onClick={() => openServiceSupport(contents)}
          >
            {contents.content}
          </div>
        ))}
      </div>
      <div>
        <label className="my_product">Products & Services Support</label>
        <InsuranceCardMobile
          heathInsureCardData={insuranceCardData}
          isArrow={true}
        />
      </div>
      <div className="my_service">
        <label
          className="my_view"
          onClick={() => history.push("/home/servicelist")}
        >
          View All Services
        </label>
        <div className="my_image">
          <img src={leftarrow} />
        </div>
      </div>
      <div className="my_trend">
        <label className="my_topic">Trending Topics</label>
        <p className="my_review">Review commonly asked questions</p>
      </div>
      <div className="my_faq">
        <CommonFaq faqList={customerServiceFaqList} />
      </div>
      <div>
        <div class="Insurance_fraud">
          <img src={Insurance_fraud} className="w-100" alt="Insurance fraud" />
          <div className="Insurance_topic">Insurance Fraud</div>
          <div>
            {insurance.map((insure) => (
              <div className="Insurance_content">
                <p>{insure.content}</p>
                <img
                  className="Insurance_image"
                  src={insure.image}
                  alt="arrow"
                />
              </div>
            ))}
          </div>
        </div>
        <div class="claim_services">
          <div className="claim_topic">How do I make a claim?</div>
          <p className="claim_content">
            Tawuniya believes in simplification and automation wherever
            possible. Our approach to the claims journey for Tawuniya is no
            different.
          </p>
          <div className="claim_head_services">Our Claims Services</div>
          <div>
            {claim.map((claims) => (
              <div className="Claim_data">
                <p>{claims.content}</p>
                <img className="Claim_image" src={claims.image} alt="Arrow" />
              </div>
            ))}
          </div>
          <img src={Track_claim} className="w-100" alt="Track claim" />
        </div>
        <div class="Violations">
          <img src={Violations} className="w-100" alt="Violations" />
          <div className="Violations_Topic">Violations</div>
          <p className="Violations_content">
            Any fraudulent acts, corruption, collusion, coercion, illegal
            behavior, misconduct, financial mismanagement, accounting abuses,
            the existence of a conflict of interest,.
          </p>
          <button type="button" className="Report_button">
            Report Violations
          </button>
        </div>
        <label className="Cant_find_violations">
          Can’t find what you’re looking for?
        </label>
      </div>
      <SupportRequestHelper />
      <FooterMobile />
    </div>
  );
};

export default SupportService;
