import React from "react";
import "./style.scss";
import { CommonFaq } from "component/common/CommonFaq";
import { customerServiceFaqList } from "component/common/MockData";
import HeaderBackNav from "component/common/MobileReuseable/HeaderBackNav";
import SupportRequestHelper from "component/common/MobileReuseable/SupportRequest";
import { InsuranceCardMobile } from "component/common/MobileReuseable/InsuranceCardMobile";
import bail_bond from "assets/images/mobile/bail_bond.png";
import motor_road from "assets/news/motor_road.png";
import car_maintenance from "assets/news/car_maintenance.png";
import car_accident from "assets/news/car_accident.png";

import tele_med from "assets/images/mobile/tele_med.png";
import web_assist from "assets/images/mobile/web_assist.png";
import blue_flight_delay from "assets/images/mobile/blue_flight_delay.png";
import white_arrow from "../../../../assets/news/white_arrow.png";
import { history } from "service/helpers";

const ServicePopup = ({ supportData }) => {
  const insuranceCardData = [
    {
      id: 1,
      content: "Periodic Inspection",
      cardIcon: bail_bond,
    },
    {
      id: 2,
      content: "Road Assistance",
      cardIcon: motor_road,
    },
    {
      id: 3,
      content: "Car Maintenance",
      cardIcon: car_maintenance,
    },
    {
      id: 4,
      content: "Car Accident",
      cardIcon: car_accident,
    },
    {
      id: 5,
      content: "Car Wash",
      cardIcon: tele_med,
    },
    {
      id: 6,
      content: "Refill Medication",
      cardIcon: tele_med,
    },
    {
      id: 7,
      content: "Medical Reimbursement",
      cardIcon: tele_med,
    },
    {
      id: 8,
      content: "Telemedicine",
      cardIcon: tele_med,
    },
    {
      id: 9,
      content: "Eligibility letter",
      cardIcon: tele_med,
    },
    {
      id: 10,
      content: "Pregnancy Program",
      cardIcon: tele_med,
    },
    {
      id: 11,
      content: "Chronic Disease Management",
      cardIcon: tele_med,
    },
    {
      id: 12,
      content: "Home Child Vaccination",
      cardIcon: tele_med,
    },
    {
      id: 13,
      content: "Assist America",
      cardIcon: web_assist,
    },
    {
      id: 14,
      content: "Flight Delay Assistance",
      cardIcon: blue_flight_delay,
    },
    {
      id: 15,
      content: "Request Bailbond",
      cardIcon: bail_bond,
    },
  ];
  return (
    <div>
      <HeaderBackNav pageName="Customer Service" title={supportData.content} />
      <div className="service_popup_container">
        <h5>{supportData.content}</h5>
        <p>
          Medical insurance fraud is an intentional act by the cardholder,
          insured, and medical services provider to obtain not owed compensation
          or benefits to them or others through the deceiving, concealing,
          and/or misrepresenting information.
        </p>
        <div className="service_popup_products">
          <CommonFaq faqList={customerServiceFaqList} />
          <div className="service_popup_bgimage">
            <h4>Products & Services Support</h4>
            <InsuranceCardMobile
              heathInsureCardData={insuranceCardData}
              isArrow={true}
            />
            <div
              className="d-flex align-items-center"
              onClick={() => history.push("/home/servicelist")}
            >
              <h6>View All Services</h6>
              <img
                className="mb-1"
                src={white_arrow}
                width="15"
                height="15"
                onClick={() => history.push("/home/servicelist")}
               alt="..."/>
            </div>
            <div className="looking_for">
              <p>Can’t find what you’re looking for?</p>
              <SupportRequestHelper />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default ServicePopup;
