import React, { useState } from "react";
import { ServiceCard } from "component/common/ServiceCard";
import { serviceData } from "component/common/MockData/index";

import "./style.scss";
import VectorGlobal from "../../../assets/svg/VectorGlobal.svg";
import Vector1 from "../../../assets/svg/Vector1.svg";
import Vector3 from "../../../assets/svg/Vector3.svg";
import Vector2 from "../../../assets/svg/Vector2.svg";
import customerServiceFrowardBannerArrow from "../../../assets/svg/customerBannerForwardArrow.svg";
// import CardComp from "../../common/HomeServiceCard/index";
import confirmImg from "../../../assets/svg/Confirmation-Girl.svg";
import { Box, Typography, Card, Icon, Divider } from "@material-ui/core";
import { FooterCard } from "component/HomePage/LandingComponent/NavbarCards";
import serviceIconOne from "assets/svg/TawuniyaServicesIcons/serviceIcons1.svg";
import serviceArrow from "assets/svg/serviceArrow.svg";
import orangeArrow from "assets/svg/orangeArrow.svg";
import telemed from "assets/svg/telemedicine.svg";
import theft from "assets/svg/theft.svg";
import flight from "assets/svg/flightdelayclaim.svg";
import accident from "assets/svg/accident.svg";
import medAssisst from "assets/svg/medassisst.svg";
import { ComplaintTabs } from "component/common/ComplaintTabs";
import { history } from "service/helpers";

// import ServiceCard from "component/common/ServiceImgeCard/index";

export const SupportRequestConfirmation = () => {
  const [pill, setPill] = useState(0);

  return (
    <>
      <div>
        <div className="main-div mt-3 mb-4">
          <div className="imgWrapper pb-4">
            <img
              src={confirmImg}
              className="img-fluid mx-auto d-block servicewomen"
              alt=""
            />
          </div>

          <h4 className="mainHeading text-center">
            Your support request was successfully submitted
          </h4>

          <div className="d-flex justify-content-center">
            <p className="text-para text-center p-lg-0 p-2">
              One of our support specialists will be in contact with you
              shortly.For your reference ,your complaint number is{" "}
              <span className="fw-800">#4407187</span>. You can track the status
              of the request in the{" "}
              <span className="fw-500">Search Support Request </span>
              page.
            </p>
          </div>
          <div className="d-flex justify-content-center flex-direction-column">
            <div>
              <div className="btnReq mt-3">View Request Details</div>
              <div className="backBtn mx-auto d-block mt-4">
                Back to Support
              </div>
            </div>
          </div>
        </div>
        <div className="serviceRequestLining"></div>
        <p className="product-Support-Title fw-800 text-center m-0 pt-4">
          Products & Services Support
        </p>
        <div className="d-flex justify-content-center pb-2">
          <div>
            <p className="product-Support-InfoText fs-18 fw-400 text-center line-height-25 m-0">
              Here you will find the answers to many of the questions you may
              have,
            </p>
            <p className="product-Support-InfoText fs-18 fw-400 text-center line-height-25">
              including information about our comprehensive range of insurance
              products and services.
            </p>
          </div>
        </div>
        <ComplaintTabs
          compOne={
            <ServiceCard
              serviceData={serviceData}
              columnClass="col-lg-2 col-12"
              routeURL="/home/customerservice/medicalfraud"
            />
          }
        />
        <p
          className="viewProducts-ServiceLink text-center fs-16 fw-800 "
          onClick={() => history.push("/home/all-products")}
        >
          View All Products & Services
          <img
            src={orangeArrow}
            className="img-fluid viewArrowIcon pl-3"
            alt="arrow"
          />
        </p>
      </div>
    </>
  );
};
