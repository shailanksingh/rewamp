import React, { useState } from "react";
import { NormalButton } from "component/common/NormalButton";
import ArrowRight from "assets/images/mobile/arrow-right.png";
import "./style.scss";
import PhoneNumberInput from "component/common/PhoneNumberInput";
import { dialingCodes } from "component/common/MockData";
import { NormalSearch } from "component/common/NormalSearch";
import { InsuranceCardMobile } from "component/common/MobileReuseable/InsuranceCardMobile";
import BottomPopup from "component/common/MobileReuseable/BottomPopup";
import OpenComplaintMobile from "component/common/OpenComplaintMobile";
import HeaderBackNav from "component/common/MobileReuseable/HeaderBackNav";


import right_arrow from "assets/images/mobile/right_arrow.png";
import tele_medicine from "assets/images/mobile/tele_medicine.png";
import bail_bond from "assets/images/mobile/bail_bond.png";
import motor_road from "assets/news/motor_road.png";
import car_maintenance from "assets/news/car_maintenance.png";
import car_accident from "assets/news/car_accident.png";

import tele_med from "assets/images/mobile/tele_med.png";
import web_assist from "assets/images/mobile/web_assist.png";
import blue_flight_delay from "assets/images/mobile/blue_flight_delay.png";

const SupportCenterRequest = () => {
  const [helperFormValues, setHelperFormValues] = useState({ phoneNumber: "" });

  let [selectedCode, setSelectedCode] = useState(dialingCodes[0]);
  const [openModal, setOpenModal] = useState(false);
  const handleInputChange = (e) => {
    const { name, value } = e.target || e || {};
    setHelperFormValues({ ...helperFormValues, [name]: value });
  };
  const { phoneNumber, userId } = helperFormValues;
  const insuranceCardData = [
    {
      id: 1,
      content: "Periodic Inspection",
      cardIcon: bail_bond,
    },
    {
      id: 2,
      content: "Road Assistance",
      cardIcon: motor_road,
    },
    {
      id: 3,
      content: "Car Maintenance",
      cardIcon: car_maintenance,
    },
    {
      id: 4,
      content: "Car Accident",
      cardIcon: car_accident,
    },
    {
      id: 5,
      content: "Car Wash",
      cardIcon: tele_medicine,
    },
    {
      id: 6,
      content: "Refill Medication",
      cardIcon: tele_medicine,
    },
    {
      id: 7,
      content: "Medical Reimbursement",
      cardIcon: tele_medicine,
    },
    {
      id: 8,
      content: "Telemedicine",
      cardIcon: tele_med,
    },
    {
      id: 9,
      content: "Eligibility letter",
      cardIcon: tele_medicine,
    },
    {
      id: 10,
      content: "Pregnancy Program",
      cardIcon: tele_medicine,
    },
    {
      id: 11,
      content: "Chronic Disease Management",
      cardIcon: tele_medicine,
    },
    {
      id: 12,
      content: "Home Child Vaccination",
      cardIcon: tele_medicine,
    },
    {
      id: 13,
      content: "Assist America",
      cardIcon: web_assist,
    },
    {
      id: 14,
      content: "Flight Delay Assistance",
      cardIcon: blue_flight_delay,
    },
    {
      id: 15,
      content: "Request Bailbond",
      cardIcon: bail_bond,
    },
  ];

  const Showmodal=()=>{
    setOpenModal(true)
  }
  return (
    <div>
      <div className="support-overall">
        <h5>Can't find what you're looking for?</h5>
        <div className="support-team">
          <h6>Contact Our Support Team</h6>
          <p>We're here to help, 24 hours a day, 7 days a week.</p>
          <div className="border_support" />
          <div className="position-relative">
            <NormalButton label="Open a Complaint" onClick={Showmodal}  className="insureNowBtn" />
            <img alt="..." src={ArrowRight} className="arrow"></img>
          </div>
        </div>
        <div className="support-team live">
          <div className="d-flex justify-content-between title">
            <h6>Less Phone, More Support</h6>
            <a>Online</a>
          </div>
          <p>
            We don’t want to keep you hanging, waiting for someone to answer the
            phone.
          </p>
          <div className="border_support" />
          <div className="position-relative">
            <NormalButton label="Start Live Chat" className="insureNowBtn" />
            <img alt="..." src={ArrowRight} className="arrow"></img>
          </div>
        </div>
        <div className="support-input-card">
          <div className="support-card">
            <h5>Find your Support Request</h5>
            <p>
              It looks like you are searching for a previous conversation with
              Tawuniya Support.
            </p>
            <div className="landing-input">
              <NormalSearch
                className="loginInputFieldOne"
                name="userId"
                value={userId}
                placeholder="Saudi ID / IQAMA"
                onChange={handleInputChange}
                needLeftIcon={false}
              />
            </div>

            <div className="landing-input">
              <PhoneNumberInput
                className="complaintPhoneInput"
                selectInputClass="complaintSelectInputWidth"
                selectInputFlexType="complaintSelectFlexType"
                dialingCodes={dialingCodes}
                selectedCode={selectedCode}
                setSelectedCode={setSelectedCode}
                value={phoneNumber}
                name="phoneNumber"
                onChange={handleInputChange}
                isCodeFalse={true}
                placeholder="ex: 5xxxxxxxx"
              />
            </div>

            <div className="terms">
              <div>
                <button className="buy-button fs-12 w-100">Search</button>
              </div>
            </div>
          </div>
        </div>
        <div className="service_list">
          <h5>Products & Services Support</h5>
          <InsuranceCardMobile
            heathInsureCardData={insuranceCardData}
            isArrow={true}
          />
          <div className="view_all_question justify-content-start">
            <label>View All Services</label>
            <img src={right_arrow} alt="Arrow" />
          </div>
        </div>
        <div className="height"></div>
      </div>
   
      <BottomPopup bg={"gray"} open={openModal} setOpen={setOpenModal}>
      <HeaderBackNav pageName="Customer Service" title="Open a Support Request" />
   
       <OpenComplaintMobile/>
      </BottomPopup>
    </div>
  );
};

export default SupportCenterRequest;
