import React, { useState } from "react";
import { ServiceCard } from "component/common/ServiceCard";
import { Faq } from "component/common/FraudDetails";
import { faqCategoryData } from "component/common/MockData/index";
import { customerLandingData } from "component/common/MockData";
import { Navbar } from "component/common/Navbar";
import customerServiceBannerHome from "../../../../assets/svg/customerServiceBannerHome.svg";
import customerServiceBannerArrow from "../../../../assets/svg/customerServiceBannerFrowardArrow.svg";
import customerServiceFrowardBannerArrow from "../../../../assets/svg/customerBannerForwardArrow.svg";
import customerServiceSearchIcon from "../../../../assets/svg/customerServiceSearchIcon.svg";
import { Typography } from "@material-ui/core";
import { CustomerServiceBannerData } from "../../../HomePage/LandingComponent/Schema/CustomerServiceBannerData";
import CardComp from "../../../common/HomeServiceCard/index";
import add from "../../../../assets/svg/add.svg";
import Phone from "../../../../assets/svg/phone.svg";
import Chat from "../../../../assets/svg/chaticon.svg";
import Vector2 from "../../../../assets/svg/Vector2.svg";
import Vector1 from "../../../../assets/svg/Vector1.svg";
import Violations from "../../../../assets/svg/Violations.svg";
import Vector3 from "../../../../assets/svg/Vector3.svg";
import Communication from "../../../../assets/svg/communication.svg";
import { MediaCenterNews } from "../../../MediaCenter/MediaCenterNews/MediaCenterNews";
import { Card, Button, Accordion, Row, Col } from "react-bootstrap";
import InsuranceFraud from "../../../../assets/svg/InsuranceFraud.svg";
import InsuranceFraud2 from "../../../../assets/svg/InsuranceFraud2.svg";
import { Icon } from "@material-ui/core";

import "./style.scss";
import { FooterCard } from "component/HomePage/LandingComponent/NavbarCards";
import { history } from "service/helpers";
import { NormalButton } from "component/common/NormalButton";
import ReportViolation from "component/CustomerService/Violations/ViolationsPage/ReportViolation";

export const CustomerServiceLanding = () => {
	const [pill, setPill] = useState(0);
	return (
		<>
			<div className="bannerMainContainer">
				{/* <div className="customerBannerStartEl">
					<img
						src={customerServiceBannerHome}
						alt="customerServiceBannerHome"
					/>
					<Typography
						component={"p"}
						style={{ fontWeight: "400px", fontSize: "14px", color: " #34383E" }}
					>
						Customer Service
					</Typography>
					<img
						src={customerServiceBannerArrow}
						alt="customerServiceBannerFrowardArrow"
					/>
					<Typography
						component={"p"}
						style={{ fontWeight: "400px", fontSize: "14px", color: " #34383E" }}
					>
						Landing Page
					</Typography>
				</div> */}

				<div className="customerBannerMainCard mt-2">
					<div style={{ marginTop: "130px", padding: "0px" }}>
						<h1 className="customerBannerMainHeading">We’re here for you</h1>
						<h1 className="customerBannerSecondHeading">
							How Can We Assist You?
						</h1>
					</div>
					<div className="customerBannerSearchingContainer">
						<input
							type="search"
							placeholder="Search for topic"
							className="inputEl"
						/>
						<img
							src={customerServiceSearchIcon}
							alt="customerServiceSearchIcon"
						/>
					</div>
					<div className="popularAndContainer">
						<p className="popularHeading">Popular Searches</p>
						<div style={{ display: "flex" }}>
							<button className="buttonEl">Travel Insurance</button>
							<button className="buttonEl">COVID-19</button>
							<button className="buttonEl">Malpractice</button>
							<button className="buttonEl">Visa Insurance</button>
						</div>
					</div>
				</div>

				<div className="CustomerServiceDataContainer">
					<h5
						style={{
							height: "48px",
							fontWeight: "800px",
							fontSize: "40px",
							lineHeight: "48px",
							color: "#FFFFFF",
						}}
					>
						Products & Services Support
					</h5>
					<p
						style={{
							textAlign: "center",
							fontWeight: "800px",
							fontSize: "18px",
							color: "#FFFFFF",
							marginBottom: "25px",
						}}
					>
						Here you will find the answers to many of the questions you may
						have,
						<br />
						including information about our comprehensive range of insurance
						products and services.
					</p>
					<div
						style={{
							display: "flex",
							flexDirection: "row",
							justifyContent: "center",
							alignItems: "center",
							marginBottom: "25px",
						}}
					>
						<div
							className={`${
								pill === 0 ? "itemContainerActive " : "itemContainerInActive"
							} mr-3`}
							onClick={() => setPill(0)}
						>
							<Icon>
								<img
									src={Vector2}
									alt="Motor"
									className={pill === 0 ? "ActiveImg" : "inActive"}
								/>
							</Icon>
							<span className={pill === 0 ? "serviceBtnEl" : "iconDivEl"}>
								ALL PRODUCTS
							</span>
						</div>

						<div
							className={`${
								pill === 1 ? "itemContainerActive " : "itemContainerInActive"
							} mr-3`}
							onClick={() => setPill(1)}
						>
							<Icon>
								<img
									src={Vector2}
									alt="Motor"
									className={pill === 1 ? "ActiveImg" : "inActive"}
								/>
							</Icon>
							<span className={pill === 1 ? "serviceBtnEl" : "iconDivEl"}>
								{" "}
								MOTOR
							</span>
						</div>
						<div
							className={`${
								pill === 2 ? "itemContainerActive " : "itemContainerInActive"
							} mr-3`}
							onClick={() => setPill(2)}
						>
							<Icon>
								<img
									src={Vector3}
									alt="Medical"
									className={pill === 2 ? "ActiveImg" : "inActive"}
								/>
							</Icon>

							<span className={pill === 2 ? "serviceBtnEl" : "iconDivEl"}>
								MEDICAL
							</span>
						</div>

						<div
							className={
								pill === 3 ? "itemContainerActive " : "itemContainerInActive"
							}
							onClick={() => setPill(3)}
						>
							<Icon>
								<img
									src={Vector1}
									alt="Travel"
									className={pill === 3 ? "ActiveImg" : "inActive"}
								/>
							</Icon>
							<span
								className={`${
									pill === 3 ? "serviceBtnEl" : "iconDivEl"
								} text-uppercase`}
							>
								property & casualty
							</span>
						</div>
					</div>
					<div className="customerBannerCardContainer">
						{/* {CustomerServiceBannerData[0].serviceCardData.map((item, i) => {
              return (
                <div className="bannerCard">
                  <CardComp item={item} key={i} />
                </div>
              );
            })} */}
						<ServiceCard
							serviceData={customerLandingData}
							cardLayerOneHeight="landing-CustomerCard"
						/>
						<Typography component={"div"} style={{ display: "flex" }}>
							<p
								className="viewMore"
								onClick={() => history.push("/home/all-products")}
							>
								View All Products & Services
							</p>
							<img
								src={customerServiceFrowardBannerArrow}
								alt="customerServiceFrowardBannerArrow"
							/>
						</Typography>
					</div>
				</div>

				<div className="TrendingContainer">
					<div className="pb-3">
						<h3 className="surplush4 fs-36 text-center pt-4">
							Trending Topics
						</h3>
						<p className="mainBannerPara fs-16 text-center">
							Review answers to commonly asked questions at Tawuniya, which
							enable you to be directly involved in improving our support
							experience.
						</p>
					</div>
					<Faq faqData={faqCategoryData} />
				</div>

				<div className="mainDIV">
					<div>
						<div className="mainContainer">
							<div>
								<h2 className="textSection">Protect Yourself Against Fraud!</h2>
								<p className="paraText">
									The insurance business operates based on a set of basic
									principles, most notably the principle of utmost good faith
									from all insurance process-related parties. However, the fraud
									by any party may occur and affect the other parties.
								</p>
								<p className="learn-more">Learn More About</p>
								<div className="fraudItems">
									<div
										className="frauds"
										onClick={() =>
											history.push("/home/customerservice/medicalfraud")
										}
									>
										MEDICAL FRAUD
										<img src={customerServiceFrowardBannerArrow} alt=">" />
									</div>
									<div
										className="frauds"
										onClick={() =>
											history.push("/home/customerservice/motorfraud")
										}
									>
										motor FRAUD
										<img src={customerServiceFrowardBannerArrow} alt=">" />
									</div>
									<div
										className="frauds"
										onClick={() =>
											history.push("/home/customerservice/travelfraud")
										}
									>
										P&C Insurance FRAUD
										<img src={customerServiceFrowardBannerArrow} alt=">" />
									</div>
								</div>
								<div
									className="buttonFraud cursor-pointer"
									onClick={() =>
										history.push("/home/customerservice/reportpage")
									}
								>
									Report a fraud
								</div>
							</div>
							<div className="fraudImg">
								<img src={InsuranceFraud} alt="InsuranceFraud" />
							</div>
						</div>
						<div className="violationsDiv">
							<div className="violationsDiv-violations">
								<h2 className="violationsHeading">Violations</h2>
								<p className="violationsText">
									Any fraudulent acts, corruption, collusion, coercion, illegal
									behavior, misconduct, financial mismanagement, accounting
									abuses, the existence of a conflict of interest,.
								</p>
								<div style={{ display: "flex" }}>
									<div
										className="reportVBtn"
										onClick={() =>
											history.push("/home/customerservice/violationspage")
										}
									>
										Report Violations
									</div>
									<div className="learnMBtn">Learn More</div>
								</div>
							</div>
							<div className="violationImg">
								<img src={Violations} alt="violations" />
							</div>
						</div>
					</div>
					<div style={{ position: "relative", top: "60px" }}>
						<div className="mainContainer">
							<div>
								<h2 className="textSection1">How do I make a claim?</h2>
								<p className="paraText2">
									Tawuniya believes in simplification and automation wherever
									possible. Our approach to the claims journey for Tawuniya is
									no different. We want you to notify us of claims as soon as
									possible and we want to get your claim moving as soon as
									possible. Our simple steps approach helps us to keep that
									focus throughout the claim process.
								</p>
								<p className="paraText3">Our Claims Services</p>
								<div className="fraudItems2">
									<div style={{ textAlign: "left" }}>
										<ul>
											<li>
												<a className="frauds" href="/">
													MOTOR CLAIMS
													<img
														src={customerServiceFrowardBannerArrow}
														alt=">"
													/>
												</a>
											</li>
											<li>
												<a className="frauds" href="/">
													MEDICAL CLAIMS
													<img
														src={customerServiceFrowardBannerArrow}
														alt=">"
													/>
												</a>
											</li>
											<li>
												<a className="frauds" href="/">
													Track Your Claim Claims
													<img
														src={customerServiceFrowardBannerArrow}
														alt=">"
													/>
												</a>
											</li>
											<li>
												<a className="frauds" href="/">
													Surplus Insurance
													<img
														src={customerServiceFrowardBannerArrow}
														alt=">"
													/>
												</a>
											</li>
										</ul>
									</div>
									<div>
										<ul>
											<li>
												<a
													className="frauds"
													onClick={() =>
														history.push(
															"/home-insurance/medicalmalpracticepage"
														)
													}
													href="/"
												>
													MALPRACTICE CLAIM
													<img
														src={customerServiceFrowardBannerArrow}
														alt=">"
													/>
												</a>
											</li>
											<li>
												<a className="frauds" href="/">
													TRAVEL CLAIM
													<img
														src={customerServiceFrowardBannerArrow}
														alt=">"
													/>
												</a>
											</li>
											<li>
												<a className="frauds" href="/">
													Track Your Claim
													<img
														src={customerServiceFrowardBannerArrow}
														alt=">"
													/>
												</a>
											</li>
										</ul>
									</div>
								</div>

								<div className="fraudImg2">
									<img src={InsuranceFraud2} alt="InsuranceFraud" />
								</div>
							</div>
						</div>
					</div>
				</div>
				<div className="pb-5">
					<div className="canNotFindContainer">
						<div className="">
							<h2 className="findContainerMainHeading fw-800 text-center">
								Can't find what you're looking for?
							</h2>
							<p className=" findContainerMainPar pt-2 medical-ptag text-center">
								Contact the legendary support team right now.
							</p>
						</div>

						<ReportViolation />
					</div>
				</div>
			</div>
		</>
	);
};
