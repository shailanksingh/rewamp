import React from "react";
import BottomPopup from "component/common/MobileReuseable/BottomPopup";
import TravelPolicyCard from "component/common/MobileReuseable/PolicyCard/TravelPolicyCard/TravelPolicyCard";
import MotorPolicyCard from "component/common/MobileReuseable/PolicyCard/MotorPolicyCard/MotorPolicyCard";
import PolicyCard from "component/common/MobileReuseable/PolicyCard";
import SupportRequestHelper from "component/common/MobileReuseable/SupportRequest";

const ViewAllPolicy = ({
  viewPolicyModel,
  setViewPolicyModel,
  openManagePolicy,
}) => {
  return (
    <BottomPopup
      open={viewPolicyModel}
      setOpen={setViewPolicyModel}
      bg={"paddingBox"}
    >
      <div className="view_policy">
        <h6 className="title">
          Your Policies <span>3</span>
        </h6>
        <TravelPolicyCard viewDetails={(data) => openManagePolicy(data)} />
        <MotorPolicyCard viewDetails={(data) => openManagePolicy(data)} />
        <PolicyCard viewDetails={(data) => openManagePolicy(data)} />
        <SupportRequestHelper />
      </div>
    </BottomPopup>
  );
};

export default ViewAllPolicy;
