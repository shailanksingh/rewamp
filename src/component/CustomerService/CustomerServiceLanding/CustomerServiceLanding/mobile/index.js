import React, { useState } from "react";
import { ThemeProvider, createTheme } from "@material-ui/core";
import "./style.scss";
import CardSliderContent from "./cardSliderContent";
import LandingHelperSection from "./landingHelperSection";
import LandingProductsOfferings from "./landingProductsOfferings";
import HeaderStickyMenu from "component/common/MobileReuseable/HeaderStickyMenu";
import FooterMobile from "component/common/MobileReuseable/FooterMobile";
import PolicyCard from "component/common/MobileReuseable/PolicyCard";
import MotorPolicyCard from "component/common/MobileReuseable/PolicyCard/MotorPolicyCard/MotorPolicyCard";
import TravelPolicyCard from "component/common/MobileReuseable/PolicyCard/TravelPolicyCard/TravelPolicyCard";
import ViewAllPolicy from "./ViewAllPolicy";
import ManageYourPolicy from "./ManageYourPolicy";
import NewPolicyCard from "component/common/MobileReuseable/PolicyCard/NewPolicyCard/NewPolicyCard";
import right_arrow from "assets/images/mobile/right_arrow.png";
import Coverage from "./Coverage";
import PolicyDetails from "./PolicyDetails";
import MotorPolicyCardDetails from "./policyDetails/MotorPolicyCardDetails";
import InsureCard from "component/common/InsureCard";

const theme = createTheme({
  palette: {
    common: {},
    primary: {
      main: "#EE7500",
    },
    secondary: {
      main: "#FFFFFF",
    },
  },
});

export const CustomerServiceLandingMobile = () => {
  const [viewPolicyModel, setViewPolicyModel] = useState(false);
  const [isCoverage, setCoverage] = useState(false);
  const [isPolicy, setPolicy] = useState(false);
  const [isMedical, setIsMedical] = useState(false);
  const [managePolicyModel, setManagePolicyModel] = useState(false);
  const [policyListView, setPolicyListView] = useState(true);
  const [isFailUpload, setIsFailUpload] = useState(false);
  const [activePolicy, setActivePolicy] = useState("Health");

  const viewDetails = () => {
    setViewPolicyModel(true);
  };
  const managePolicyData = (data) => {
    setManagePolicyModel(true);
    setActivePolicy(data);
  };

  return (
    <ThemeProvider theme={theme}>
      <div className="position-relative landing-page-mobile">
        <HeaderStickyMenu customClass={true} />

        <div className="BannerContainer_mobile pt-2">
          <div className="mx-3 quick">
            <p className="mb-1 Sec">
              Welcome <strong>Ahmed</strong>, Here are quick actions:
            </p>
            <CardSliderContent />
          </div>

          <div className="policy_count">
            <label>Your Policies</label>
            <span>2</span>
          </div>
          <div className={policyListView ? "policy_bg" : ""}>
            <div
              className={` ${
                policyListView ? "card_swap" : "policy_list_view"
              }`}
            >
              {policyListView && <NewPolicyCard />}

              <TravelPolicyCard
                isShrink={policyListView}
                viewDetails={viewDetails}
                setIsMedical={setIsMedical}
              />

              <MotorPolicyCard
                isShrink={policyListView}
                setPolicyListView={setPolicyListView}
              />
              <PolicyCard
                viewDetails={viewDetails}
                setIsMedical={setIsMedical}
              />
            </div>
            {policyListView && (
              <label
                className="view_all_cards"
                onClick={() => setPolicyListView(false)}
              >
                View all cards <img src={right_arrow} alt="Arrow" />
              </label>
            )}
          </div>
        </div>
        <div className="customer-insurance">
          <div className="get-insured minute">
            <p>Get insured within minutes!</p>
          </div>
          <InsureCard />
        </div>
        <LandingProductsOfferings />
        <div className="help-sec">
          <LandingHelperSection />
        </div>
      </div>

      <MotorPolicyCardDetails
        isMedical={isMedical}
        setIsMedical={setIsMedical}
        isFailUpload={isFailUpload}
      />

      <Coverage isCoverage={isCoverage} setCoverage={setCoverage} />
      <PolicyDetails isPolicy={isPolicy} setPolicy={setPolicy} />

      <ViewAllPolicy
        viewPolicyModel={viewPolicyModel}
        setViewPolicyModel={setViewPolicyModel}
        openManagePolicy={(data) => managePolicyData(data)}
      />
      <ManageYourPolicy
        managePolicyModel={managePolicyModel}
        setManagePolicyModel={setManagePolicyModel}
        activePolicy={activePolicy}
      />
      {/* <div className="testing-demo d-flex mx-3">
        <p
          onClick={() => {
            setIsMedical(true);
            setIsFailUpload(true);
          }}
          className="mr-2"
        >
          upload fail
        </p>
        <p
          onClick={() => {
            setCoverage(true);
          }}
          className="mr-2"
        >
          coverage
        </p>
        <p
          onClick={() => {
            setPolicy(true);
          }}
          className="mr-2"
        >
          policy
        </p>
      </div> */}
      <FooterMobile />
    </ThemeProvider>
  );
};
