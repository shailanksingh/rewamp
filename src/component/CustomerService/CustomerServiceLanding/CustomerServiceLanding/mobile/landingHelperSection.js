import React from "react";
import Chat from "assets/images/mobile/messenger_blue.svg";
import Complaints from "assets/images/mobile/edit_file_blue.svg";
import All from "assets/images/mobile/grid_blue.svg";
import Search from "assets/images/mobile/search.png";
import Fader from "assets/images/mobile/Fader.png";
import ArrowRight from "assets/images/mobile/right_arrow.png";
import map_car from "assets/images/mobile/map_car.svg";
import map_H from "assets/images/mobile/map_H.svg";
import tawuniya_logo from "assets/images/mobile/tawuniya_logo_blue.svg";
import { history } from "service/helpers";

import "./style.scss";

const LandingHelperSection = () => {
  return (
    <div className="container landingHelperSection">
      <p className="tx-style">How Can We Help You?</p>
      <form class="row align-items-center input-width pt-0">
        <input
          type="text"
          class="form-control"
          id="autoSizingInput"
          placeholder="What you're looking for you?"
        />
        <img alt="Search" src={Search} className="search"></img>
      </form>
      <div class="row g-2 pt-3 bc-style">
        <div class="col-4">
          <div class="p-2  bg-white">
            <div className="d-flex justify-content-center">
              <img src={Chat} height="20" alt="Chat" />
            </div>
            <div className="pt-1 t-style d-flex justify-content-center">
              <p>Chat Now</p>
            </div>
          </div>
        </div>
        <div class="col-4">
          <div class="p-2  bg-white">
            <div className="d-flex justify-content-center">
              <img src={Complaints} alt="Complaints" />
            </div>
            <div className="pt-1 t-style d-flex justify-content-center">
              <p>Complaints</p>
            </div>
          </div>
        </div>
        <div class="col-4">
          <div class="p-2  bg-white">
            <div
              className="d-flex justify-content-center"
              onClick={() => history.push("/home/all-service")}
            >
              <img src={All} height="20" alt="Toggle" />
            </div>
            <div className="pt-1 t-style d-flex justify-content-center">
              <p>All Services</p>
            </div>
          </div>
        </div>
      </div>
      <div className="map-landing">
        <p className="">Network Of Providers</p>
        <div className="iframe-map position-relative">
          <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3966.521260322283!2d106.8195613507864!3d-6.194741395493371!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e69f5390917b759%3A0x6b45e67356080477!2sPT%20Kulkul%20Teknologi%20Internasional!5e0!3m2!1sen!2sid!4v1601138221085!5m2!1sen!2sid" />

          <form class="row align-items-center input-width pt-0 map-search">
            <input
              type="text"
              class="form-control"
              id="autoSizingInput"
              placeholder="What you're looking for you?"
            />
            <img alt="Search" src={Search} className="search"></img>
            <ul>
              <li>
                <img src={map_H} height="20" alt="Chat" />
              </li>
              <li>
                <img src={map_car} height="20" alt="Chat" />
              </li>
              <li>
                <img src={tawuniya_logo} height="20" alt="Chat" />
              </li>
            </ul>
          </form>
          <div className="d-flex pt-2">
            <div className="txt-size my-1">View All Providers</div>
            <div className="mx-2 image-size" height="20">
              <img src={ArrowRight} alt="Arrow" className="ml-2" />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default LandingHelperSection;
