import React from "react";
import Vitality from "assets/images/mobile/Vitality.svg";
import Subtract from "assets/images/mobile/Subtract.svg";
import TawuniyaLogo from "assets/images/mobile/Tawuniya-Logo.svg";
import Arrow from "assets/images/mobile/arrow.png";
import ArrowRight from "assets/images/mobile/right_arrow.png";
import { history } from "service/helpers";

const LandingProductsOfferings = () => {
  return (
    <div className="productsOfferings">
      <div className="te-style">Rewards & More</div>
      <div>
        <section>
          <div class="row product-row">
            <div class="swipercards ">
              <div
                className="card overflow-hiddentext-wrap"
                onClick={() => history.push("/home/tawuniya-vitality")}
              >
                <div className="d-flex align-item-center ">
                  <div className="top">
                    <p>Tawuniya</p>
                    <img src={Vitality} alt="Vitality"></img>
                  </div>
                  <div className="bottom-slide">
                    <h4>
                      Get 30% Off annual fitness <br></br> time gym membership!
                    </h4>
                    <h6>
                      <span>
                        <img src={Arrow} alt="Arrow" />
                      </span>
                      Learn More
                    </h6>
                  </div>
                </div>
              </div>
              <div
                className="card overflow-hiddentext-wrap one"
                onClick={() => history.push("/home/tawuniya-ithra")}
              >
                <div className="d-flex align-item-center ">
                  <div className="top">
                    <p>Tawuniya</p>
                    <img src={Subtract} alt="Subtract"></img>
                  </div>
                  <div className="bottom-slide">
                    <h4>
                      Access to hundreds of<br></br> exclusive offers, discounts
                      and <br></br>more
                    </h4>
                    <h6>
                      <span>
                        <img src={Arrow} alt="Arrow" />
                      </span>
                      Learn More
                    </h6>
                  </div>
                </div>
              </div>
              <div
                className="card overflow-hiddentext-wrap two"
                onClick={() => history.push("/home/tawuniya-drive")}
              >
                <div className="d-flex align-item-center ">
                  <div className="top">
                    <span>
                      15 <sub>%</sub>
                    </span>
                    <p>Discount</p>
                  </div>
                  <div className="bottom-slide">
                    <h4>
                      Get 15% Off on <br></br> comprehensive insurance
                    </h4>
                    <h6>
                      <span>
                        <img src={Arrow} alt="Arrow" />
                      </span>
                      Learn More
                    </h6>
                  </div>
                </div>
              </div>
              <div
                className="card overflow-hiddentext-wrap three"
                onClick={() => history.push("/home/taj-program")}
              >
                <div className="d-flex align-item-center ">
                  <div className="top">
                    <img src={TawuniyaLogo} alt="TawuniyaLogo"></img>
                  </div>
                  <div className="bottom-slide">
                    <h4>
                      Special services and <br></br> programmes to cover your{" "}
                      <br></br> needs
                    </h4>
                    <h6>
                      <span>
                        <img src={Arrow} alt="Arrow" />
                      </span>
                      Learn More
                    </h6>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>
      <div className="d-flex mx-3 rewards-pro">
        <div className="txt-size my-1">View all rewards</div>
        <div className="mx-2 image-size" height="20">
          <img src={ArrowRight} alt="Arrow" className="ml-2" />
        </div>
      </div>
    </div>
  );
};

export default LandingProductsOfferings;
