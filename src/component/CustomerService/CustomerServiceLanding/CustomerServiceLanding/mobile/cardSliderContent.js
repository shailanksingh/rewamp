import React, { useState } from "react";
import { useHistory } from "react-router-dom";
import car_tree_blue from "assets/images/mobile/car_tree_blue.svg";
import FlightDelay from "assets/images/mobile/flight_blue.svg";
import grid_blue from "assets/images/mobile/grid_blue.svg";
import truck_blue from "assets/images/mobile/truck_blue.svg";
import paid_orange from "assets/images/mobile/paid_orange.svg";
import "./style.scss";

function CardSliderContent() {
  const [datas] = useState([
    {
      id: 1,
      image: paid_orange,
      description: "Submit & Track Claims",
      checked: false,
    },
    {
      id: 2,
      image: truck_blue,
      description: "Manage Your Policy",
      checked: false,
    },
    {
      id: 3,
      image: grid_blue,
      description: "Explore Services",
      checked: false,
    },
    {
      id: 4,
      image: car_tree_blue,
      description: "Road Side Assistance",
      checked: false,
    },
    {
      id: 5,
      image: FlightDelay,
      description: "Flight Delay",
      checked: false,
    },
  ]);
  const history = useHistory();

  return (
    <section>
      <div class="container-fluid">
        <div class="row m-0">
          <div class="scrollcards ">
            {datas.map((data, index) => (
              <div key={data.id} className="card overflow-hidden text-wrap ">
                <div className="d-flex align-items-center ">
                  <span className="icon">
                    <img
                      src={data.image}
                      height="10"
                      className="iconn"
                      alt="icon"
                    />
                  </span>
                  <span
                    className="card-desc card_title"
                    onClick={
                      index === 0 ? () => history("/assisstance") : () => {}
                    }
                  >
                    {data.description}
                  </span>
                </div>
              </div>
            ))}
          </div>
        </div>
      </div>
    </section>
  );
}

export default CardSliderContent;
