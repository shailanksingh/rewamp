import React from "react";
import BottomPopup from "component/common/MobileReuseable/BottomPopup";
import Badge from "assets/images/mobile/Badge.svg";
import Beneficiary from "assets/images/mobile/Beneficiary.png";
import contact from "assets/images/mobile/contact.png";
import calc from "assets/images/mobile/calc.png";
import badge from "assets/images/mobile/badge.png";
import policynum from "assets/images/mobile/policynum.png";
import gender from "assets/images/mobile/gender.png";
import uploaddate from "assets/images/mobile/uploaddate.png";
import expirydate from "assets/images/mobile/expirydate.png";
import close3 from "assets/images/mobile/close3.png";
import phone from "assets/images/mobile/phone.png";
import whatsapp from "assets/images/mobile/whatsapp.png";
import mail from "assets/images/mobile/mail.png";
import "./style.scss";

const PolicyDetails = ({ isPolicy, setPolicy }) => {
  return (
    <BottomPopup open={isPolicy} setOpen={() => setPolicy(false)} bg={"gray"}>
      <div className="coverage-section">
        <div className="coverage-head">
          <p>Health Services</p>
          <h6>Your Policy Details</h6>
        </div>
        <div className="coverage-body">
          <div className="d-flex justify-content-between">
          <h5>Policy Details</h5>
            <div className="closeicon">
              <img src={close3}></img>
            </div>
            </div>
          <div className="policy">
            <div>
              <img src={Badge}></img>
            </div>
            <div className="policy-view">
              <p className="m-0">Medical Network</p>
              <p className="active">11</p>
            </div>
          </div>
          <div className="policy">
            <div>
              <img src={contact}></img>
            </div>
            <div className="policy-view">
              <p className="m-0">Identity Number</p>
              <p className="active">22222222</p>
            </div>
          </div>
          <div className="policy">
            <div>
              <img src={policynum}></img>
            </div>
            <div className="policy-view">
              <p className="m-0">Policy Number</p>
              <p className="active">5569</p>
            </div>
          </div>
          <div className="policy">
            <div>
              <img src={badge}></img>
            </div>
            <div className="policy-view">
              <p className="m-0">Class</p>
              <p className="active">1</p>
            </div>
          </div>
          <div className="policy">
            <div>
              <img src={gender}></img>
            </div>
            <div className="policy-view">
              <p className="m-0">Gender</p>
              <p className="active">male</p>
            </div>
          </div>
          <div className="policy">
            <div>
              <img src={calc}></img>
            </div>
            <div className="policy-view">
              <p className="m-0">Deductible Rate</p>
              <p className="active">100</p>
            </div>
          </div>
          <div className="policy">
            <div>
              <img src={calc}></img>
            </div>
            <div className="policy-view">
              <p className="m-0">Max. Limit</p>
              <p className="active">100</p>
            </div>
          </div>
          <div className="policy">
            <div>
              <img src={uploaddate}></img>
            </div>
            <div className="policy-view">
              <p className="m-0">Uploaded Date</p>
              <p className="active">23-05-2022</p>
            </div>
          </div>
          <div className="policy">
            <div>
              <img src={expirydate}></img>
            </div>
            <div className="policy-view">
              <p className="m-0">Expiry Date</p>
              <p className="active">31-12-2022</p>
            </div>
          </div>
          <div className="policy">
            <div>
              <img src={Beneficiary}></img>
            </div>
            <div className="policy-view">
              <p className="m-0">Beneficiary Number</p>
              <p className="active">222222222</p>
            </div>
          </div>
          <div className="d-flex justify-content-between">
          <h5>Contact Daman (CHI)</h5>
            <div className="closeicon">
              <img src={close3}></img>
            </div>
            </div>
            <div className="support-request-helper">
      <div className="support-request-callcenter">
        <img className="phone-img" src={phone} alt="phone" />
        <span className="call-center">Call Center</span>
        <div className="call-number">800 124 9990</div>
      </div>
      <div className="support-request-whatsapp">
        <img className="whatsapp-img" src={whatsapp} alt="whatsapp" />
        <span className="whatsapp">Whatsapp</span>
        <div className="whatsapp-number">9200 19990</div>
        <div className="whatsapp-chat">This is a chat only number</div>
      </div>
      
      <div className="support-request-mail">
        <img className="mail-img" src={mail} alt="mail" />
        <span className="support-email">Email</span>
        <div className="support-email-care">Care@tawuniya.com.sa</div>
      </div>
      <h2 className="hr-lines">OR</h2>
      <div className="support-button">
        <button className="support-request-button">
          Open a Support Request
        </button>
      </div>
    </div>
        </div>
      </div>
    </BottomPopup>
  );
};

export default PolicyDetails;
