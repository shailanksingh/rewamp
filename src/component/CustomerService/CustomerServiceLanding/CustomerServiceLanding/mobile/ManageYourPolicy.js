import React, { useEffect, useState } from "react";
import BottomPopup from "component/common/MobileReuseable/BottomPopup";
import vehicle_orange from "assets/images/mobile/vehicle_orange.svg";
import shield_orange from "assets/images/mobile/shield_orange.svg";
import solid_H from "assets/images/mobile/solid_H.svg";
import approval_history from "assets/images/mobile/approval_history.svg";
import solid_close_icon from "assets/images/mobile/solid_close_icon.svg";
import solid_loop from "assets/images/mobile/solid_loop.svg";
import clipboard_orange from "assets/images/mobile/clipboard_orange.svg";
import manage_policy_bottom from "assets/images/mobile/manage_policy_bottom.svg";
import solid_right_arrow from "assets/images/mobile/solid_right_arrow.svg";
import Vitality from "assets/images/mobile/vitality_policy.svg";
import person_icon from "assets/images/mobile/person_icon.svg";
const HealthPolicyData = [
  {
    id: 1,
    title: "Your covrage & Benifiets",
    labelImage: vehicle_orange,
  },
  {
    id: 2,
    title: "Your Usage",
    labelImage: shield_orange,
  },
  {
    id: 3,
    title: "Medical Providers",
    labelImage: solid_H,
  },
  {
    id: 4,
    title: "Approvals History",
    labelImage: approval_history,
  },
  {
    id: 5,
    title: "Medical Reimbursement",
    labelImage: solid_close_icon,
  },
  {
    id: 6,
    title: "Taj Services",
    labelImage: solid_loop,
  },
  {
    id: 7,
    title: "Ithra Program",
    labelImage: solid_close_icon,
  },
  {
    id: 8,
    title: "Submit a Support Request",
    labelImage: clipboard_orange,
  },
];

const MotorPolicyData = [
  {
    id: 1,
    title: "Manage Your Vehicles",
    labelImage: vehicle_orange,
  },
  {
    id: 2,
    title: "Add Additional Coverages",
    labelImage: shield_orange,
  },
  {
    id: 3,
    title: "Add Drivers",
    labelImage: person_icon,
  },
  {
    id: 4,
    title: "Submit a Claim",
    labelImage: clipboard_orange,
  },

  {
    id: 5,
    title: "Renew your policy",
    labelImage: solid_loop,
  },
  {
    id: 6,
    title: "Cancel Your Policy",
    labelImage: solid_close_icon,
  },
];

const ManageYourPolicy = ({
  managePolicyModel,
  setManagePolicyModel,
  activePolicy,
}) => {
  const [policyListData, setPolicyListData] = useState(HealthPolicyData);
  useEffect(() => {
    setPolicyListData(
      activePolicy === "Motor" ? MotorPolicyData : HealthPolicyData
    );
  }, [activePolicy]);
  return (
    <BottomPopup open={managePolicyModel} setOpen={setManagePolicyModel}>
      <div className="manage_your_policy">
        <p>Policy # 22222222</p>
        <h6>Manage Your Policy</h6>
        <div className="manage_policy_body">
          {policyListData?.map(({ title, labelImage, id }) => {
            return (
              <div>
                <img src={labelImage} alt="Icon" />
                <label className={id === 8 && "mb-0 border-0"}>{title}</label>
              </div>
            );
          })}
        </div>
        <div className="policy_footer">
          <img src={manage_policy_bottom} alt="Claim" className="banner" />
          <div className="content_section">
            <img src={Vitality} alt="Vitality" />
            <p>Get 30% Off annual fitness time gym membership!</p>
            <div className="d-flex align-items-center">
              <img src={solid_right_arrow} alt="Arrow" />
              <label>Claim Your Reword</label>
            </div>
          </div>
        </div>
      </div>
    </BottomPopup>
  );
};

export default ManageYourPolicy;
