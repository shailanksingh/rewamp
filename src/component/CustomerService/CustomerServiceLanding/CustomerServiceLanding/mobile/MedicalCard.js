import React from "react";
import BottomPopup from "component/common/MobileReuseable/BottomPopup";
import PolicyCard from "component/common/MobileReuseable/PolicyCard";
import Share from "assets/images/mobile/share.png";
import Wallet from "assets/images/mobile/wallet.png";
import UploadError from "assets/images/mobile/upload_error.png";
import Clock from "assets/images/mobile/clock.png";
import Hospital from "assets/images/mobile/hospital.png";
import ArrowBlue from "assets/images/mobile/arrow-right-blue.png";
import GetHelp from "assets/images/mobile/get-help.png";
import { CommonFaq } from "component/common/CommonFaq";
import { customerServiceFaqList } from "component/common/MockData";
import ArrowRight from "assets/images/mobile/right_arrow.png";
import Document from "assets/images/mobile/document.png";
import truck_black from "assets/images/mobile/refill.png";
import tele_medicine from "assets/images/mobile/medical.png";
import flightdelay from "assets/images/mobile/tele.png";
import { InsuranceCardMobile } from "component/common/MobileReuseable/InsuranceCardMobile";
import SupportRequestHelper from "component/common/MobileReuseable/SupportRequest";
import "./style.scss";
import MedicalCaroselCard from "component/common/MedicalCaroselCard";
import { NormalButton } from "component/common/NormalButton";

const MedicalCard = ({ isMedical, setIsMedical = () => {}, isFailUpload }) => {
  const insuranceCardData = [
    {
      id: 1,
      content: "Refill Medication",
      cardIcon: truck_black,
    },
    {
      id: 2,
      content: "Medical Reimbursement",
      cardIcon: tele_medicine,
    },
    {
      id: 3,
      content: "Telemedicine",
      cardIcon: flightdelay,
    },
    {
      id: 4,
      content: "Road Side Assistance",
      cardIcon: truck_black,
    },
    {
      id: 5,
      content: "Request Telemedicine",
      cardIcon: tele_medicine,
    },
  ];

  return (
    <BottomPopup
      open={isMedical}
      setOpen={() => setIsMedical(false)}
      bg={"gray"}
    >
      <div className="medical-section">
        {isFailUpload && (
          <div className="upload-fail">
            <div className="file-error">
              <div className="pr-3">
                <img src={UploadError}></img>
              </div>
              <div>
                <h6>CCHI Upload Faild</h6>
                <p>
                  The addition of Youssef, Your son has been rejected by CCHI
                  “Daman”. You can check the issue and re-add him. a gentel
                  action is required by your company HR department.{" "}
                </p>
              </div>
            </div>
          </div>
        )}
        {/* POLICY CARD SECTION */}
        <div className={`${isFailUpload && "pt-0"} medical-card`}>
          <PolicyCard />
          <div className="card-head">
            <div className="share">
              <img alt="Share" src={Share}></img>
              <p>Save or Share</p>
            </div>
            <div className="wallet">
              <img alt="Wallet" src={Wallet}></img>
              <h6>
                <span>Add to</span>Apple Wallet
              </h6>
            </div>
          </div>
        </div>
        {/* CAROSEL MEDICAL CARD */}
        <MedicalCaroselCard />
        <div className="recent-inter">
          <h6>Recent Interactions</h6>
          {!isFailUpload ? (
            <>
              <div className="recent-card">
                <div className="d-flex justify-content-between">
                  <p>
                    <img src={Clock}/>30 Minutes Ago
                  </p>
                  <div className="d-flex">
                    <p className="pr-2">
                      <img src={Document}/> 17364427
                    </p>
                    <span>In progress</span>
                  </div>
                </div>
                <div className="hospital d-flex align-items-center">
                  <img src={Hospital} className="pr-2"></img>
                  <div>
                    <p>Car Wash</p>
                    <h5>Merc. Benz - 3576 TND</h5>
                  </div>
                </div>
                <div className="help-section">
                  <div className="get-help">
                    <p>
                      <img src={GetHelp} alt="GetHelp"></img>Get Help
                    </p>
                  </div>
                  <div className="detail">
                    <h6>View Details</h6>
                    <img src={ArrowBlue} alt="ArrowBlue"></img>
                  </div>
                </div>
              </div>
              <div className="d-flex pt-2">
                <div className="txt-size my-1">View All Interactions</div>
                <div className="mx-2 pt-1 image-size" height="20">
                  <img src={ArrowRight} alt="Arrow" className="ml-2" />
                </div>
              </div>
            </>
          ) : (
            <div className="pending-inter">
              <p>You Do not have any pending interactions.</p>
              <NormalButton label="Get Support" className="verifyLoginBtn" />
            </div>
          )}
        </div>
        <div className="service_list">
          <h5>Services we offer</h5>
          <InsuranceCardMobile
            heathInsureCardData={insuranceCardData}
            isArrow={false}
          />
          <div className="d-flex pt-2">
            <div className="txt-size my-1">View All Services</div>
            <div className="mx-2 pt-1 image-size" height="20">
              <img src={ArrowRight} alt="Arrow" className="ml-2" />
            </div>
          </div>
        </div>
        <div className="question_section">
          <h6>Frequently Asked Questions</h6>
          <CommonFaq faqList={customerServiceFaqList} />
          <div className="d-flex pt-2">
            <div className="txt-size my-1">View All FAQs</div>
            <div className="mx-2 pt-1 image-size" height="20">
              <img src={ArrowRight} alt="Arrow" className="ml-2" />
            </div>
          </div>
        </div>
        <SupportRequestHelper />
      </div>
    </BottomPopup>
  );
};

export default MedicalCard;
