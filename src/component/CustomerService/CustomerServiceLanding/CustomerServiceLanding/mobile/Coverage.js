import React from "react";
import BottomPopup from "component/common/MobileReuseable/BottomPopup";
import ProgressBar from "react-bootstrap/ProgressBar";
import close3 from "assets/images/mobile/close3.png";
import "./style.scss";

const Coverage = ({ isCoverage, setCoverage }) => {
  return (
    <BottomPopup
      open={isCoverage}
      setOpen={() => setCoverage(false)}
      bg={"gray"}
    >
      <div className="coverage-section">
        <div className="coverage-head">
          <p>Health Services</p>
          <h6>Your Policy Coverage & Usage</h6>
        </div>
        <div className="coverage-body">
        <div className="d-flex justify-content-between">
          <h5>Basic Medical Coverage</h5>
            <div className="closeicon">
              <img src={close3}></img>
            </div>
            </div>
          <div className="basic">
            <div className="card-view">
              <p>Maximum policy benefit limit per person every year</p>
              <p className="active">SR 30,000 </p>
            </div>
            <div className="progress-block">
              <ProgressBar now={40} />
            </div>
          </div>
          <div className="basic">
            <div className="card-view">
              <p>Room & board limit:
At preferred provider network
At non preferred provider network (emergency only)</p>
              <p className="active">Shared Room
SR 350 </p>
            </div>
          </div>
          <div className="basic">
            <div className="card-view">
              <p>Out-patient doctors fees at preferred provider network</p>
              <p className="active">SR 75 </p>
            </div>
          </div>
          <div className="basic">
            <div className="card-view">
              <p>Maximum pre-existing benefit sub-limit per person every year (no waiting period)</p>
              <p className="active">SR 30,000 </p>
            </div>
          </div>
          <div className="basic">
            <div className="card-view">
              <p>Local ambulance services</p>
              <p className="active">Covered </p>
            </div>
          </div>
          <div className="basic">
            <div className="card-view">
              <p>Circumcision for newborn male babies</p>
              <p className="active">Covered </p>
            </div>
          </div>
          <div className="basic">
            <div className="card-view">
              <p> Ear piercing for newborn female babies</p>
              <p className="active">Covered </p>
            </div>
          </div>
          <div className="basic">
            <div className="card-view">
              <p>Vaccinations for children as per MOH specification)</p>
              <p className="active">Covered </p>
            </div>
          </div>
          <div className="basic">
            <div className="card-view">
              <p>Vaccinations for children as per MOH specification</p>
              <p className="active">Covered </p>
            </div>
          </div>
          <div className="basic">
            <div className="card-view">
              <p>Physiotherapy treatment</p>
              <p className="active">Covered (12 Sessions)</p>
            </div>
          </div>
          <div className="basic">
            <div className="card-view">
              <p>Hearing aids / other aids/ equipments</p>
              <p className="active">SR 1,000</p>
            </div>
            <div className="progress-block">
              <ProgressBar now={40} />
            </div>
          </div>
          <div className="basic">
            <div className="card-view">
              <p>Geographical limits :
Within KSA
Outside KSA</p>
              <p className="active">SR 1,000</p>
            </div>
            <div className="progress-block">
              <ProgressBar now={40} />
            </div>
          </div>
          <div className="d-flex justify-content-between Deductible">
          <h5>Optical Cover Benefits Limit</h5>
            <div className="closeicon">
              <img src={close3}></img>
            </div>
            </div>
            <div className="basic">
            <div className="card-view">
              <p>Maximum optical benefit 
limit Per Person Per Year</p>
              <p className="active">SR 200</p>
            </div>
            <div className="progress-block">
              <ProgressBar now={40} />
            </div>
          </div>
          <div className="basic">
            <div className="card-view">
              <p>Optical examination</p>
              <p className="active">Full Cover</p>
            </div>
            <div className="progress-block">
              <ProgressBar now={40} />
            </div>
          </div>
          <div className="basic">
            <div className="card-view">
              <p>lenses once per 
person per year</p>
              <p className="active">Full Cover</p>
            </div>
            <div className="progress-block">
              <ProgressBar now={40} />
            </div>
          </div>
          <div className="d-flex justify-content-between Deductible">
          <h5>Matirnity/Obstetrical Cover</h5>
            <div className="closeicon">
              <img src={close3}></img>
            </div>
            </div>
            <div className="basic">
            <div className="card-view">
              <p>Maximum maternity benefit 
limit per spouse per year (PSPY)</p>
              <p className="active">SR 5,000</p>
            </div>
            <div className="progress-block">
              <ProgressBar now={40} />
            </div>
          </div>
          <div className="basic">
            <div className="card-view">
              <p>Normal delivery Caesarean 
section/complications</p>
              <p className="active">SR 5,000</p>
            </div>
            <div className="progress-block">
              <ProgressBar now={40} />
            </div>
          </div>
          <div className="basic">
            <div className="card-view">
              <p>Legal Abortion/Miscarriage
(Pre and post natal care 
covered under basic limit)</p>
              <p className="active">SR 5,000</p>
            </div>
            <div className="progress-block">
              <ProgressBar now={40} />
            </div>
          </div>
          <div className="d-flex justify-content-between Deductible">
          <h5>Dental cover benefits limit</h5>
            <div className="closeicon">
              <img src={close3}></img>
            </div>
            </div>
            <div className="basic">
            <div className="card-view">
              <p>Maximum dental benefit 
limit PPPY (basic cover only)</p>
              <p className="active">SR 5,000</p>
            </div>
            <div className="progress-block">
              <ProgressBar now={40} />
            </div>
          </div>
          <div className="basic">
            <div className="card-view">
              <p>Dental consultation</p>
              <p className="active">Covered</p>
            </div>
            <div className="progress-block">
              <ProgressBar now={40} />
            </div>
          </div>
          <div className="basic">
            <div className="card-view">
              <p>Amalgam/composite
(non-cosmetic) filling</p>
              <p className="active">Covered</p>
            </div>
            <div className="progress-block">
              <ProgressBar now={40} />
            </div>
          </div>
          <div className="basic">
            <div className="card-view">
              <p>Root canal treatment	</p>
              <p className="active">Covered</p>
            </div>
            <div className="progress-block">
              <ProgressBar now={40} />
            </div>
          </div>
          <div className="basic">
            <div className="card-view">
              <p>Gum treatment</p>
              <p className="active">Covered</p>
            </div>
            <div className="progress-block">
              <ProgressBar now={40} />
            </div>
          </div>
          <div className="basic">
            <div className="card-view">
              <p>X-rays & prescribed 
medication in respect 
of above covers</p>
              <p className="active">Covered</p>
            </div>
            <div className="progress-block">
              <ProgressBar now={40} />
            </div>
          </div>
          <div className="basic">
            <div className="card-view">
              <p>Normal delivery Caesarean 
section/complications</p>
              <p className="active">Covered</p>
            </div>
            <div className="progress-block">
              <ProgressBar now={40} />
            </div>
          </div>
          <div className="d-flex justify-content-between Deductible ">
          <h5>Deductible coinsurance</h5>
            <div className="closeicon">
              <img src={close3}></img>
            </div>
            </div>
            <div className="basic">
            <div className="card-view">
              <p>Each and every 
out-patient claim</p>
              <p className="active">SR 2000</p>
            </div>
            <div className="progress-block">
              <ProgressBar now={40} />
            </div>
          </div>
          <div className="basic">
            <div className="card-view">
              <p>Each and every
in-patient claim</p>
              <p className="active">SR 2000</p>
            </div>
            <div className="progress-block">
              <ProgressBar now={40} />
            </div>
          </div>
        </div>
      </div>
    </BottomPopup>
  );
};

export default Coverage;
