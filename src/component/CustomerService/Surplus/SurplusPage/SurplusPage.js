import React, { useState } from "react";
import { Card, Button, Accordion } from "react-bootstrap";
import { Faq } from "component/common/FraudDetails";
import {
  faqTravelData,
  faqContainerTravelData,
} from "component/common/MockData/index";
import layerOne from "assets/svg/Layer1.svg";
import add from "../../../../assets/svg/add.svg";
import RightArrow from "../../../../assets/svg/Right-arrow.svg";
import customerHome from "assets/svg/customerHome.svg";
import orangeCircleOne from "assets/svg/TawuniyaOrangeCircleOne.svg";
import orangeCircleTwo from "assets/svg/surpulusOrangeCircle.svg";
import customerBreadCrumb from "assets/svg/customerBreadCrumb.svg";

import "./style.scss";
import { NormalButton } from "component/common/NormalButton";

export const SurplusPage = () => {
  return (
    <React.Fragment>
      <div className="row pt-3">
        <div className="col-lg-12 col-md-12 col-12">
          {/* card start here */}
          <Card className="surpluscard">
            <img
              src={layerOne}
              className="img-fluid layerOneBanner"
              alt="banner"
            />
            <Card.Body>
              <Card.Text>
                <div className="container row">
                  <div className="col-lg-12 pt-4">
                    <h4 className="surplush fs-35 fw-800">Surplus Insurance</h4>
                    <p className="surplusptag  pt-2">
                      Dear Customers, According to Article (70) of the
                      Implementing Regulations for Insurance Companies and the
                      Surplus Distribution Policy, issued by the Saudi Central
                      Bank (SAMA), we would like to inform you that the
                      insurance operations surplus is now available for eligible
                      policyholders in compliance with the terms and conditions
                      required by law.
                    </p>
                    <div className="d-flex flex-row">
                      <div>
                        <NormalButton
                          label="Apply to Surplus"
                          className="applySurplusBtn p-4"
                        />
                      </div>
                      <div className="pl-2">
                        <NormalButton
                          label="Learn more"
                          className="surplusLearnmoreBtn p-4"
                        />
                      </div>
                    </div>

                    {/* <Button className="surplus-btn mt-3 mr-2" variant="primary">
                      Apply to Surplus
                    </Button>{" "}
                    <Button className="surplus-btn2 mt-3" variant="primary">
                      Learn more
                    </Button>{" "} */}
                  </div>
                </div>
              </Card.Text>
            </Card.Body>
          </Card>

          {/* card end here */}
          <div className="row pt-5 px-5 mx-3">
            <div className="col-lg-4">
              <div className="verticalLine">
                <h5 className="surplush4 fw-800 fs-16">
                  Surplus Distribution Conditions
                </h5>
                <p className="fs-13 fw-400 mainBannerParas pt-3">
                  The distribution of the surplus will happen after accounting
                  for the claims paid to the policyholder and any other amounts
                  owed by the customer in compliance with the terms and
                  conditions required by law.
                </p>
              </div>
            </div>

            {/* <div className="col-lg-0 space">
                <div class="vertical-line"></div>
              </div> */}

            <div className="col-lg-4">
              <div className="verticalLine">
                <h5 className="surplush4 fw-800 fs-16">
                  Surplus Distribution Years
                </h5>
                <p className="fs-13 fw-400 mainBannerParas pt-3 pb-3">
                  Tawuniya will distribute the surplus from insurance operations
                  to the eligible policyholders for the following years 2020،
                  2019، 2016، 2015، 2014
                </p>
              </div>
            </div>

            {/* <div className="col-lg-0 space">
                <div class="vertical-line"></div>
              </div> */}

            <div className="col-lg-4">
              <h5 className="surplush4 fw-800 fs-16 surpluswidth">
                How to get your share of the surplus
              </h5>
              <p className="fs-13 fw-400 mainBannerParas pt-3">
                To obtain your share of the surplus via direct transfer to your
                bank account, please follow the following steps:
              </p>
            </div>
          </div>
          {/* --------- */}

          {/* 2card start here */}
          <Card className="surpluscard2 px-5 mt-5">
            <Card.Body>
              <Card.Text>
                <div className="row ">
                  <img
                    src={orangeCircleOne}
                    className="img-fluid surplusCircleOne"
                    alt="circle"
                  />

                  <div className="col-lg-7 col-md-8 pt-2">
                    <h4 className="surplush4 fw-800 fs-30 card2-h4 pb-2">
                      Check Your Eligibility
                    </h4>
                    <p className="fs-13 surplusptag2">
                      Dear Customers, According to Article (70) of the
                      Implementing Regulations for Insurance Companies and the
                      Surplus Distribution Policy, issued by the Saudi Central
                      Bank (SAMA).
                    </p>
                  </div>

                  <div className="col-lg-5 col-md-8 pt-4">
                    <Button className="surplus-btn-c2 " variant="primary">
                      Apply to Surplus{" "}
                      <img className="pl-2" src={RightArrow} alt="" />{" "}
                    </Button>{" "}
                  </div>
                  <img
                    src={orangeCircleTwo}
                    className="img-fluid surplusCircleTwo"
                    alt="circle"
                  />
                </div>
              </Card.Text>
            </Card.Body>
          </Card>

          {/* 2card ends here */}

          {/* section 4 */}

          {/* section 4 ends here */}
          <Faq
            faqData={faqTravelData}
            faqContainerData={faqContainerTravelData}
          />
        </div>
      </div>
    </React.Fragment>
  );
};
