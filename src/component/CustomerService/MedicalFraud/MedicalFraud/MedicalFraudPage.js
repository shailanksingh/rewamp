import React, { useState } from "react";
import {
	Card,
	Button,
	Accordion,
	AccordionContext,
	useAccordionButton,
} from "react-bootstrap";
import orangeArrow from "assets/svg/orangeArrow.svg";
import add from "assets/svg/add.svg";
import { ServiceCard } from "component/common/ServiceCard";
import { serviceData } from "component/common/MockData/index";
import Phone from "assets/svg/phone.svg";
import Chat from "assets/svg/chaticon.svg";
import Communication from "assets/svg/communication.svg";

import Add from "assets/svg/ReportFraud/open.svg";
import Minus from "assets/svg/ReportFraud/close.svg";

import "./style.scss";
import "./MedicalFraud-MediaQuery.css";
import { history } from "service/helpers";
import { ComplaintTabs } from "component/common/ComplaintTabs";

export const MedicalFraudPage = () => {
	const settings = {
		dots: true,
		infinite: false,
		speed: 500,
		slidesToShow: 5,
		slidesToScroll: 1,
	};

	const accdata = [
		{
			id: 1,
			title: "What is insurance fraud?",
			images: Add,
			data: "Tawuniya provides its customers with more than 60 types of insurance including medical, motor, fire,property, engineering, casualty, marine, aviation,Takaful, liability insurance and many other types of insurance",
		},
		{
			id: 2,
			title: " What kind of people/Organizations commits insurance fraud?",
			images: Add,
			data: "Tawuniya provides its customers with more than 60 types of insurance including medical, motor, fire, property, engineering, casualty, marine, aviation,Takaful, liability insurance and many other types of insurance",
		},
		{
			id: 3,
			title: "What is fraud vs. Mistake?",
			images: Add,
			data: "Tawuniya provides its customers with more than 60 types of insurance including medical, motor, fire, property, engineering, casualty, marine, aviation,Takaful, liability insurance and many other types of insurance",
		},
		{
			id: 4,
			title: " What are some examples of insurance fraud/Abuse?",
			images: Add,
			data: "Tawuniya provides its customers with more than 60 types of insurance including medical, motor, fire, property, engineering, casualty, marine, aviation,Takaful, liability insurance and many other types of insurance",
		},
		{
			id: 5,
			title: "How does fraud impact you?",
			images: Add,
			data: "Tawuniya provides its customers with more than 60 types of insurance including medical, motor, fire, property, engineering, casualty, marine, aviation,Takaful, liability insurance and many other types of insurance",
		},

		{
			id: 6,
			title: "What is medical fraud unit?",
			images: Add,
			data: "Tawuniya provides its customers with more than 60 types of insurance including medical, motor, fire, property, engineering, casualty, marine, aviation,Takaful, liability insurance and many other types of insurance",
		},
		{
			id: 7,
			title: "Shall I get a reward for reporting medical insurance fraud?",
			images: Add,
			data: "Tawuniya provides its customers with more than 60 types of insurance including medical, motor, fire, property, engineering, casualty, marine, aviation,Takaful, liability insurance and many other types of insurance",
		},
	];

	return (
		<React.Fragment>
			<div className="medicalFraudMainContainer">
				<div className="medicalFaqSubContainer">
					<h2 className="mainBannerHeading  reportheader ">Medical Fraud</h2>
					<div className="container">
						<p className="mainBannerPara text-center">
							Medical insurance fraud is an intentional act by the cardholder,
							insured, and medical services provider to obtain not owed
							compensation or benefits to them or others through the deceiving,
							concealing, and/or misrepresenting information.
						</p>
					</div>

					{/* accordian start here */}

					<div className="container">
						<Accordion>
							<div className="row">
								{accdata.map((item, index) => {
									return (
										<div className="col-lg-12 pt-4">
											<Card className="accordiancard">
												<Accordion.Item eventKey={item.id}>
													<Accordion.Header className="accord-hdg">
														<img src={item.images} />{" "}
														<span className="accord-ptag">{item.title}</span>
													</Accordion.Header>
													<Accordion.Body>
														<div className="container accordianbg">
															<p className="faqParEl pb-3 pt-3">{item.data}</p>
														</div>
													</Accordion.Body>
												</Accordion.Item>
											</Card>
										</div>
									);
								})}
							</div>
						</Accordion>
					</div>
					{/* ----accordian ends here */}
				</div>

				<div>
					<div className="canNotFindContainer pt-5">
						<h2 className="canNotFindMainHeading ">
							Can't find what you're looking for?
						</h2>
						<p className="pt-2 pb-1 medical-ptag text-center">
							Contact the legendary support team right now.
						</p>
						{/* <div className="canNotFindCard">
              <div className="cardNotSubCardContainer">
                <div className="canNotFindSubCard">
                  <div className="imgCard">
                    <img src={Phone} />
                  </div>
                  <div>
                    <p className="findContainerSubPar">Call Center</p>
                    <h6 className="findContainerSubHeading">800 124 9990</h6>
                  </div>
                </div>

                <div>
                  <div class="verticalLine"></div>
                </div>

                <div className="canNotFindSubCard">
                  <div className="imgCard">
                    <img src={Chat} />
                  </div>
                  <div>
                    <p className="findContainerSubPar">Whatsapp</p>
                    <h6 className="findContainerSubHeading">9200 19990</h6>
                    <p className="findContainerSmallPar">
                      This is a chat only number
                    </p>
                  </div>
                </div>

                <div className="chatContainerEl">
                  <p className="findContainerSubPar">
                    Chat with our executives
                  </p>
                  <h6 className="findContainerSubHeading">
                    Start Live Chat <span className="onlineEl">Online</span>
                  </h6>
                </div>
                <div>
                  <div class="verticalLine"></div>
                </div>

                <div className="canNotFindSubCard">
                  <div className="imgCard">
                    <img src={Communication} />
                  </div>
                  <div>
                    <p className="findContainerSubPar">Email</p>
                    <h6 className="findContainerSubHeading">
                      Care@tawuniya.com.sa
                    </h6>
                  </div>
                </div>
              </div>

              <div className="cardNotSubLeftContainer">
                <div className="hrLineContainer">
                  <hr className="hrLine" />
                  <span className="orName">OR</span>
                  <hr className="hrLine" />
                </div>
                <button className="buttonEl">Open a Support Request</button>
              </div>
            </div> */}
						<Card className="medical-card">
							<div className="container-fluid">
								<div className="d-flex justify-content-between report-support-box">
									<div className="d-flex">
										<div className="mt-3 pr-4">
											<img src={Phone} alt="Phone" />
										</div>
										<div>
											<p className="mainBannerPara medical-ptag pt-3 pb-3 mb-0 fs-14 fw-400">
												{" "}
												Call Center
											</p>
											<h6 className="mainBannerHeading fs-18 fw-700">
												800 124 9990
											</h6>
										</div>
									</div>

									<div className="col-lg-0">
										<div class="vertical-line "></div>
									</div>

									<div className="d-flex">
										<div className="mt-3 pr-4">
											<img src={Chat} alt="Chat" />
										</div>
										<div>
											<p className="mainBannerPara medical-ptag pb-2 pt-3 mb-0 fs-14 fw-400">
												Whatsapp
											</p>
											<h6 className="mainBannerHeading text-left pb-2 fs-18 fw-700 mb-0">
												9200 19990
											</h6>
											<p className="mainBannerPara fs-12 fw-400">
												This is a chat only number
											</p>
										</div>
										<div className="ml-3">
											<p className="mainBannerPara pb-2 medical-ptag pt-3 mb-0 fs-14 fw-400">
												{" "}
												Chat with our executives
											</p>
											<h6 className="mainBannerHeading fs-18 fw-700 mb-0">
												Start Live Chat
												<label className="online_badge">Online</label>
											</h6>
										</div>
									</div>

									<div>
										<div class="vertical-line pt-5"></div>
									</div>

									<div className="d-flex">
										<div className="mt-3 pr-4">
											<img src={Communication} alt="email" />
										</div>
										<div>
											<p className="mainBannerPara medical-ptag pb-2 pt-3 mb-0 fs-14 fw-400">
												Email
											</p>
											<h6 className="mainBannerHeading fs-18 fw-600">
												Care@tawuniya.com.sa
											</h6>
										</div>
									</div>

									<div className="border_or_content">
										<label>
											<span>OR</span>
										</label>
									</div>

									<div className="pt-4">
										<Button
											className="medical-btn2 fs-13"
											onClick={() =>
												history.push("/home/customerservice/opencomplaint")
											}
										>
											Open a Support Request
										</Button>
									</div>
								</div>
							</div>
						</Card>
					</div>

					<p className="medicFraud-Support-Title fw-800 text-center m-0 pt-5">
						Products & Services Support
					</p>
					<div className="d-flex justify-content-center pb-2">
						<div>
							<p className="medicFraud-Support-InfoText fs-16 fw-400 text-center line-height-25 m-0">
								Here you will find the answers to many of the questions you may
								have,
							</p>
							<p className="medicFraud-Support-InfoText fs-16 fw-400 text-center line-height-25">
								including information about our comprehensive range of insurance
								products and services.
							</p>
						</div>
					</div>
					<ComplaintTabs
						compOne={
							<ServiceCard
								serviceData={serviceData}
								columnClass="col-lg-2 col-12"
								routeURL="/home/customerservice/medicalfraud"
							/>
						}
					/>
					<p
						className="medicFraud-viewProducts-ServiceLink text-center fs-16 fw-800 "
						onClick={() => history.push("/home/all-products")}
					>
						View All Products & Services
						<img
							src={orangeArrow}
							className="img-fluid medicFraud-viewArrowIcon pl-3"
							alt="arrow"
						/>
					</p>

					{/* 3 card */}
				</div>
				{/* ----------- */}
			</div>
		</React.Fragment>
	);
};
