import React, { useState } from "react";
import "./style.scss";
import HeaderStepsSticky from "component/common/MobileReuseable/HeaderStepsSticky";
import HeaderStickyMenu from "component/common/MobileReuseable/HeaderStickyMenu";
import PolicyCard from "component/common/PolicyCard";
import { PolicyList } from "component/common/MockData";
import Insurance_Trip from "assets/images/mobile/Insurance_Trip.svg";
import Peace_Mind from "assets/images/mobile/Peace_Mind.svg";
import SAR_days from "assets/images/mobile/SAR_days.svg";
import Tawuniya_Drive from "assets/images/mobile/Tawuniya_Drive.svg";
import Chronic_Disease from "assets/images/mobile/Chronic_Disease.svg";
import { CommonFaq } from "component/common/CommonFaq";
import { customerServiceFaqList } from "component/common/MockData";
import SupportRequestHelper from "component/common/MobileReuseable/SupportRequest";
import TawuniyaAppQuick from "component/common/MobileReuseable/TawuniyaAppQuick/Ithra";
import arrow from "assets/images/mobile/right_arrow.png";
import MakeClaimCard from "component/common/MobileReuseable/MakeClaimCard";
import MyFamilyMobilePage from "component/MyFamilyMobilePage/Insurancecard";
import PlansForFutureCard from "component/common/MobileReuseable/PlansForFutureCard";

const TravelInsuranceMobile = () => {
  const [services, setServices] = useState([
    {
      id: 1,
      image: Peace_Mind,
      title: "Peace Of Mind",
      description:
        "The program provides the passenger with reassurance and peace of mind to enjoy the trip and offers comprehensive protection.",
    },
    {
      id: 2,
      image: SAR_days,
      title: "Discounted Prices",
      description:
        "Insurance cover for young people is available at discounted prices.",
    },
    {
      id: 3,
      image: SAR_days,
      title: "Discounted Prices",
      description:
        "Insurance cover for young people is available at discounted prices.",
    },
    {
      id: 4,
      image: SAR_days,
      title: "Discounted Prices",
      description:
        "Insurance cover for young people is available at discounted prices.",
    },
    {
      id: 5,
      image: SAR_days,
      title: "Discounted Prices",
      description:
        "Insurance cover for young people is available at discounted prices.",
    },
  ]);

  const [drives, setdrives] = useState([
    {
      id: 1,
      image: Tawuniya_Drive,
      title: "Tawuniya Drive",
      description: "It mesures your driving behavior and rewards you weekly,",
    },
    {
      id: 2,
      image: Chronic_Disease,
      title: "Chronic Disease Management",
      description: "A program that serves people with Chronic Disease",
    },
    {
      id: 3,
      image: Chronic_Disease,
      title: "Chronic Disease Management",
      description: "A program that serves people with Chronic Disease",
    },
    {
      id: 4,
      image: Chronic_Disease,
      title: "Chronic Disease Management",
      description: "A program that serves people with Chronic Disease",
    },
    {
      id: 5,
      image: Chronic_Disease,
      title: "Chronic Disease Management",
      description: "A program that serves people with Chronic Disease",
    },
  ]);

  return (
    <div className="mobile_travel_insurace_container">
      <HeaderStickyMenu />
      <HeaderStepsSticky title="International Travel" buttonTitle="Buy Now" />
      <div className="Insure_Trip">
        <h4>Insure your trip within minutes!</h4>
        <p>
          The Saudi insurance pioneer, will help you to identify, analyze and
          manage such risks and suggest appropriate insurance solutions.
        </p>
      </div>

      {/* <div className="passport_numbers">
        <label>Passport Number</label>
        </div> */}
      <div className="Manage_policy">Mange Your Policy</div>
      <div className="Policycard_Travel">
        <PolicyCard policyList={PolicyList} />
      </div>
      <div className="Plans_Future">
        <h4>Plans For Your Future</h4>
        <p>Simple prices, No hidden fees, Adanced features for your business</p>
      </div>
      <div className="contents-row">
        <PlansForFutureCard isAnnual={false} />
        <PlansForFutureCard
          isAnnual={false}
          title="KSA"
          subTitle="Kingdom of Saudi Arabia"
        />
        <PlansForFutureCard
          isAnnual={false}
          title="GCC"
          subTitle="Gulf Cooperation Council"
        />
        <PlansForFutureCard
          isAnnual={false}
          title="Worldwide"
          subTitle="Including USA & Canada"
        />
      </div>
      <div className="Advantages_Program">
        <h4>Advantages Of The Program</h4>
        <p>
          The Saudi insurance pioneer, will help you to identify, analyze and
          manage such risks and suggest appropriate insurance solutions.
        </p>
      </div>

      <div className="d-flex peacemind_slider ">
        {services.map((services, index) => (
          <div key="id" className="peacemind_card">
            <img className="" src={services.image} />
            <h5>{services.title}</h5>
            <p>{services.description}</p>
          </div>
        ))}
      </div>

      <div className="Our_Exclusive">
        <h4>Our Exclusive and Added-Value Services</h4>
        <label>Tawuniya support is here to help.</label>
      </div>

      <div className="d-flex taj_services_slider ">
        {drives.map((drives, index) => (
          <div key="id" className="taj_services_card">
            <img className="" src={drives.image} />
            <h5>{drives.title}</h5>
            <p>{drives.description}</p>
          </div>
        ))}
      </div>
      <div></div>
      <div className="medical_claim">
        <MakeClaimCard />
      </div>
      <div className="questions_section">
        <h4>You’ve got questions, we’ve got answers</h4>
        <p>
          Review answers to commonly asked questions at Tawuniya, which enable
          you to be directly involved in improving our support experience.
        </p>
        <CommonFaq faqList={customerServiceFaqList} />
      </div>

      <div className="d-flex">
        <div className="veiw_questions_style">
          <p>View All Questions</p>
        </div>
        <div className="pt-1 mx-2">
          {" "}
          <img src={arrow} />
        </div>
      </div>
      <TawuniyaAppQuick />
      <SupportRequestHelper isContact={false} />
      <MyFamilyMobilePage />
    </div>
  );
};

export default TravelInsuranceMobile;
