import React, { useState } from "react";
import { history } from "service/helpers";
import Truck from "assets/images/mobile/Truck.jpg";
import Accident from "assets/images/mobile/Accident.png";
import rightmove from "assets/images/mobile/rightmove.png";
import Close from "assets/images/mobile/Close.png";
import backImage from "assets/images/mobile/backImage.png";
import doctor from "assets/images/mobile/Doctorr.png";
import globe from "assets/images/mobile/globe.png";
import flightdelay from "assets/images/mobile/flightdelay.png";
import bail from "assets/images/mobile/bail.png";
import motor_road from "assets/news/motor_road.png";
import car_accident from "assets/news/car_accident.png";
import tele_med from "assets/images/mobile/tele_med.png";
import web_assist from "assets/images/mobile/web_assist.png";
import blue_flight_delay from "assets/images/mobile/blue_flight_delay.png";
import bail_bond from "assets/images/mobile/bail_bond.png";
import { NormalButton } from "component/common/NormalButton";
import "./style.scss";
import SupportRequestHelper from "component/common/MobileReuseable/SupportRequest";

// import rightmove from "../../../assets/pageassets/rightmove.png";

const EmergencySupport = () => {
  const [datas, setDatas] = useState([
    {
      id: 1,
      label: "Motor",
      dataList: [
        {
          id: 1,
          image: motor_road,
          description: "Road Assistance",
          checked: false,
        },
        {
          id: 2,
          image: car_accident,
          description: "Car Accident",
          checked: false,
        },
      ],
    },
    {
      id: 2,
      label: "Health",
      dataList: [
        {
          id: 1,
          image: tele_med,
          description: "Telemedicine",
          checked: false,
        },
        {
          id: 2,
          image: web_assist,
          description: "Assist America",
          checked: false,
        },
      ],
    },
    {
      id: 2,
      label: "Travel",
      dataList: [
        {
          id: 1,
          image: blue_flight_delay,
          description: "Flight Delay",
          description2: "Assistance",
        },
      ],
    },
    {
      id: 4,
      label: "Medical Malpractice",
      dataList: [
        {
          id: 1,
          image: bail_bond,
          description: "Request Bailbond",
        },
      ],
    },
  ]);
  return (
    <div className="emergency_parent">
      <div className="emergency_banner">
        <img
          src={backImage}
          width="100%"
          className="position-relative"
          alt="Background"
          onClick={() => history.goback()}
        />
        <p>We hope all is good!</p>
        <p className="sub_Text">We are here for you</p>
        <img
          src={Close}
          onClick={() => history.push("/home")}
          className="close_icon"
          height="32px"
          width="32px"
          alt="Close"
        />
      </div>
      <div className="container  mt-4 emergency_container">
        {datas.map((val) => {
          return (
            <div className={`row   ${val.id === 4 && "bottom_space"}`}>
              <div className="support_label">{val.label}</div>
              <div className="motor scrollcards px-0">
                {val.dataList.map((data) => (
                  <div
                    key={data.id}
                    className="motorcard-design "
                    onClick={() => history.push("/dashboard/road-assisstance")}
                  >
                    <div className=" ">
                      <img
                        src={data.image}
                        className="img-map  mt-3 mx-2 "
                        alt="Icon"
                      />

                      <h6 className="color-default font-lato700 mx-2">
                        {data.description}
                        <br />
                        {data.description2}
                      </h6>
                    </div>
                  </div>
                ))}
              </div>
            </div>
          );
        })}
      </div>
      <div className="emergency_still_need">Still Needs Help?</div>
      <div className="emergency_support_request">
        <SupportRequestHelper />
      </div>
      {/* <div className="bottom-fix bg-white d-flex justify-content-center p-3">
        <NormalButton
          label="Still Need Support ?"
          onClick={() => history.push("/home/customerservice/opencomplaint")}
          className="need_support_button"
        />
      </div> */}
    </div>
  );
};

export default EmergencySupport;
