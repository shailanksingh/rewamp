import React, { useState } from "react";
import "./style.scss";
import Close from "assets/images/mobile/Close.png";
import { history } from "service/helpers";
import { InsuranceCardMobile } from "component/common/MobileReuseable/InsuranceCardMobile";
import truck_black from "assets/images/mobile/refill.png";
import tele_medicine from "assets/images/mobile/medical.png";
import flightdelay from "assets/images/mobile/tele.png";

const insuranceCardData = [
  {
    id: 1,
    content: "Periodic Inspection",
    cardIcon: truck_black,
    service: "motor",
  },
  {
    id: 2,
    content: "Road Side Assistance",
    cardIcon: tele_medicine,
    service: "motor",
  },
  {
    id: 3,
    content: "Car Maintenance",
    cardIcon: flightdelay,
    service: "motor",
  },
  {
    id: 4,
    content: "Car Accident",
    cardIcon: truck_black,
    service: "motor",
  },
  {
    id: 5,
    content: "Car Wash",
    cardIcon: tele_medicine,
    service: "motor",
  },
  {
    id: 6,
    content: "Refill Medication",
    cardIcon: tele_medicine,
    service: "health",
  },
  {
    id: 7,
    content: "Medical Reimbursement",
    cardIcon: tele_medicine,
    service: "health",
  },
  {
    id: 8,
    content: "Eligibility letter",
    cardIcon: tele_medicine,
    service: "health",
  },
  {
    id: 9,
    content: "Pregnancy Program",
    cardIcon: tele_medicine,
  },
  {
    id: 10,
    content: "Chronic Disease Management",
    cardIcon: tele_medicine,
  },
  {
    id: 11,
    content: "Home Child Vaccination",
    cardIcon: tele_medicine,
  },
  {
    id: 12,
    content: "Assist America",
    cardIcon: tele_medicine,
  },
  {
    id: 13,
    content: "Flight Delay Assistance",
    cardIcon: tele_medicine,
    service: "pc",
  },
  {
    id: 14,
    content: "Flight Delay Assistance",
    cardIcon: tele_medicine,
    service: "pc",
  },
];

const buttonListData = [
  {
    id: 1,
    label: "All Services",
  },
  {
    id: 2,
    label: "Motor",
    service: "motor",
  },
  {
    id: 3,
    label: "Health",
    service: "health",
  },
  {
    id: 4,
    label: "Property and Casualty",
    service: "pc",
  },
];

const ServiceList = () => {
  const [activeProduct, setActiveProduct] = useState(0);
  const [serviceListData, setServiceListData] = useState(insuranceCardData);
  const activeProductSource = (index, service) => {
    setActiveProduct(index);
    setServiceListData(
      service
        ? insuranceCardData.filter((val) => val.service === service)
        : insuranceCardData
    );
  };
  return (
    <div className="service_list_container">
      <img
        src={Close}
        alt="Close"
        className="close_service_list"
        onClick={() => history.goBack()}
      />
      <div className="service_list_header">
        <p>Product & Services</p>
        <p className="fw-800 fs-16">Our Services</p>
        <div className="service_buttons">
          {buttonListData.map((val, index) => {
            return (
              <button
                className={activeProduct === index && "active"}
                onClick={() => activeProductSource(index, val.service)}
              >
                {val.label}
              </button>
            );
          })}
        </div>
      </div>
      <div className="service_list_data">
        <InsuranceCardMobile
          heathInsureCardData={serviceListData}
          isArrow={true}
          rowWiseList={true}
        />
      </div>
    </div>
  );
};

export default ServiceList;
