import BottomPopup from "component/common/MobileReuseable/BottomPopup";
import HeaderStepsSticky from "component/common/MobileReuseable/HeaderStepsSticky";
import HeaderStickyMenu from "component/common/MobileReuseable/HeaderStickyMenu";
import React, { useState } from "react";
import GetQuotePCInsurance from "./GetQuotePCInsurance";
import "./style.scss";
import engineer from "assets/about/engineer.png";
import {
  engineeringContractorsInsurance,
  engineeringKeyBenefits,
  engineeringHealthInsurancepolicy,
  engineeringQuesAndAnswer,
} from "component/common/MockData";
import FooterMobile from "component/common/MobileReuseable/FooterMobile";
import { CommonFaq } from "component/common/CommonFaq";

const PCInsuranceMobile = () => {
  const [openQuote, setOpenQuote] = useState(true);
  return (
    <div className="pc_insurance_container">
      <HeaderStickyMenu />
      <HeaderStepsSticky
        title={"Engineering & Contractors Insurance"}
        buttonTitle="Get a Quote"
      />
      <div className="pc_engineer">
        <img src={engineer} />
        <label className="pc_contract">
          Engineering & Contractors Insurance
        </label>
      </div>
      <div className="line"></div>
      <div className="pa_para">
        Tawuniya has designed this collection of insurance products especially
        for the Engineering & Contractors business to provide the appropriate
        protection to transact its activities safely.
      </div>
      <div className="pc_white">
        {engineeringContractorsInsurance.map((data) => (
          <div className="pc_box">
            <img src={data.bannerImage} />
            <div className="pc_data">{data.label}</div>
          </div>
        ))}
      </div>
      <div className="pc_key">
        <label>Key Benefits</label>
        <p>
          No need to coordinate with multiple points of contact and third
          parties. With Tawuniya’s Health Insurance, you only need to stay in
          touch with us and no one else.
        </p>
      </div>
      {engineeringKeyBenefits.map((content) => (
        <div className="pc_keybenefits">
          <img src={content.image} />
          <label>{content.label}</label>
        </div>
      ))}
      <div className="pc_protect">
        <label>
          Why protect your employees’ health with a Health Insurance policy?
        </label>
        <p>
          The Saudi insurance pioneer, will help you to identify, analyze and
          manage such risks and suggest appropriate insurance solutions.
        </p>
      </div>
      <div className="pc_insurance">
        {engineeringHealthInsurancepolicy.map((data) => (
          <div className="pc_health">
            <img src={data.image} />
            <div>
              {" "}
              <label>{data.title}</label>
              <p>{data.content}</p>
            </div>
          </div>
        ))}
      </div>
      <div className="pc_ques">
        <label>You’ve got questions, we’ve got answers</label>
        <p>
          Review answers to commonly asked questions at Tawuniya, which enable
          you to be directly involved in improving our support experience.
        </p>
      </div>
      <div className="my_faq">
        <CommonFaq faqList={engineeringQuesAndAnswer} />
      </div>
      <FooterMobile />
      <div className="pc_button_white">
      <button className="pc_button">Get Quote</button>
      </div>
      <BottomPopup open={openQuote} setOpen={setOpenQuote} bg="gray">
        <GetQuotePCInsurance />
      </BottomPopup>
    </div>
  );
};

export default PCInsuranceMobile;
