import React, { useState } from "react";
import { dialingCodes,engineeringContractorsInsuranceQueue } from "component/common/MockData";
import { NormalCheckbox } from "component/common/NormalCheckBox";
import { NormalSearch } from "component/common/NormalSearch";
import PhoneNumberInput from "component/common/PhoneNumberInput";
import "./style.scss";
import Pc_Name from "assets/images/mobile/Pc_Name.svg";
import Pc_Company from "assets/images/mobile/Pc_Company.svg";
import Pc_Email from "assets/images/mobile/Pc_Email.svg";
import HeaderBackNav from "component/common/MobileReuseable/HeaderBackNav";

const GetQuotePCInsurance = () => {
  let [selectedCode, setSelectedCode] = useState(dialingCodes[0]);

  return (
    <div className="pc_quote_container">
      <HeaderBackNav pageName="Enterprise - Property & Casualty" title="Engineering & Contractors Insurance"/>
      <h4>Get Quote</h4>
      <div className="quote_form row">
        <div className="col-6">
          <NormalSearch
            className="loginInputFieldOne"
            name="userId"
            placeholder={"First name"}
            needLeftIcon={true}
            leftIcon={Pc_Name}
          />
        </div>
        <div className="col-6">
          <NormalSearch
            className="loginInputFieldOne"
            name="userId"
            placeholder={"Last Name"}
            needLeftIcon={true}
            leftIcon={Pc_Name}
          />
        </div>
        <div className="col-12">
          <NormalSearch
            className="loginInputFieldOne"
            name="userId"
            placeholder={"Company name"}
            needLeftIcon={true}
            leftIcon={Pc_Company}
          />
        </div>
        <div className="col-12">
          <NormalSearch
            className="loginInputFieldOne"
            name="userId"
            placeholder={"Enter email"}
            needLeftIcon={true}
            leftIcon={Pc_Email}
          />
        </div>
        <div className="col-12">
          <PhoneNumberInput
            className="loginPhoneInput"
            selectInputClass="loginSelectInputWidth"
            selectInputFlexType="loginSelectFlexType"
            dialingCodes={dialingCodes}
            selectedCode={selectedCode}
            setSelectedCode={setSelectedCode}
            name="phn"
          />
        </div>
        <div className="Engineering_content">
          {/* <NormalCheckbox label="Contractors' All Risks Insurance" /> */}
          <h4>Select product that you intersted in</h4>
         {engineeringContractorsInsuranceQueue.map((content)=>
        <label>
          <NormalCheckbox/> {content.label}
          </label>
          )
          }
          <hr></hr>
        </div>
      </div>
      <div className="Pc_Bottom_content">
       <label>Message, Notes..</label><br/>
       <textarea type='text' placeholder='Type your message here..'></textarea>
       <button>Get Quote</button>
      </div>
    </div>
  );
};

export default GetQuotePCInsurance;
