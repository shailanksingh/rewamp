import React from "react";
import SupportCenterMobile from "./SupportCenterMobile";

const SupportCenter = () => {
  return <div>{window.innerWidth < 600 && <SupportCenterMobile />}</div>;
};

export default SupportCenter;
