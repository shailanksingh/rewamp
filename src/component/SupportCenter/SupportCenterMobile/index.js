import React from "react";
import { history } from "service/helpers";
import "./style.scss";
import support_bg from "assets/images/mobile/support_bg.png";
import Close from "assets/images/mobile/Close.png";
import plus_icon from "assets/images/mobile/plus_icon.png";
import { NormalSearch } from "component/common/NormalSearch";
import right_arrow from "assets/images/mobile/right_arrow.png";
import Tasks from "assets/images/mobile/Tasks.png";
import tele_medicine from "assets/images/mobile/Road_service.png";
import flightdelay from "assets/images/mobile/Car_service.png";

import { InsuranceCardMobile } from "component/common/MobileReuseable/InsuranceCardMobile";

import mobile_icon from "assets/images/mobile/mobile_icon.png";
import email_icon from "assets/images/mobile/email_icon.png";
import whatsapp_icon from "assets/images/mobile/whatsapp_icon.png";
import support_ticket from "assets/images/mobile/support_ticket.png";

import { SupportCards } from "component/common/MobileReuseable/supportCards";

import SupportRequestHelper from "component/common/MobileReuseable/SupportRequest";

const insuranceCardData = [
  {
    id: 1,
    content: "Periodic Inspection",
    cardIcon: Tasks,
  },
  {
    id: 2,
    content: "Road Assistance",
    cardIcon: tele_medicine,
  },
  {
    id: 3,
    content: "Car Maintenance",
    cardIcon: flightdelay,
  },
  {
    id: 4,
    content: "Periodic Inspection",
    cardIcon: Tasks,
  },
  {
    id: 5,
    content: "Road Assistance",
    cardIcon: tele_medicine,
  },
];

const SupportCenterMobile = () => {
  return (
    <div className="suppport_center_container">
      <div className="support_center_header">
        <img src={support_bg} alt="Support" className="support_bg" />
        <p>We are here for you</p>
        <p className="sub_Text">Support center</p>
        <img
          src={Close}
          onClick={() => history.push("/home")}
          className="close_icon"
          height="32px"
          width="32px"
          alt="Close"
        />
      </div>
      <div className="header_search_part">
        <NormalSearch
          className="headerSearch"
          name="search"
          placeholder="What you're looking for?"
          needRightIcon={true}
        />
      </div>
      <div className="questions_section">
        <h>Most Asked Questions</h>
        <div className="question_body">
          <div className="icon_placement">
            <img src={plus_icon} alt="Plus" />
          </div>
          <p>
            The insured is provided with an Insurance Card and Bail Bond
            approved by the Traffic Police together with SANAD Insurance Policy
            to prevent the arrest or detainment of the driver causing the
            accident?
          </p>
        </div>
        <div className="question_body">
          <div className="icon_placement">
            <img src={plus_icon} alt="Plus" />
          </div>
          <p>
            The insurance covers all types of cars without specifying their type
            or model?
          </p>
        </div>
        <div className="view_font_modify">
          <label className="mx-2 view_font_modify">View all Questions</label>
          <img src={right_arrow} alt="Arrow" className="mx-1"/>
        </div>
      </div>
      <div className="service_list">
        <h5>Services we offer</h5>
        <InsuranceCardMobile heathInsureCardData={insuranceCardData} />
        <div className="view_all_question justify-content-start">
          <label className="mx-2">View all services</label>
          <img src={right_arrow} alt="Arrow" className="mb-1 mx-2"/>
        </div>
      </div>
      <div>
        
        <div className="card-body contact_support_team">
         <div>
          <p className="mb-1">Can’t find an answer?</p>
        <p className="mb-3">Contact our support team now.</p>
        </div>
      <SupportRequestHelper/>
      </div>
      </div>
      
    </div>
  );
};

export default SupportCenterMobile;
