import React from "react";
import { history } from "service/helpers";
import Close from "assets/images/mobile/close_black.png";
import "./style.scss";

export const TitleBannerMobile = ({ title, subTitle }) => {
  return (
    <div className="road_assistance_header_banner">
      <div className="road_assistance_header">
        <img
          src={Close}
          alt="Close"
          className="close_icon"
          onClick={() => history.goBack()}
        />
        <div className="title_banner_mobile">
          <h6>{title}</h6>
          <h5>{subTitle}</h5>
        </div>
      </div>
    </div>
  );
};
