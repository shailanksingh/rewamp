import React from "react";
import { history } from "service/helpers";
import { crumbLinks } from "component/common/MockData/NewMockData";
import customerHome from "assets/svg/customerHome.svg";
import customerBreadCrumb from "assets/svg/customerBreadCrumb.svg";
import "./style.scss";

export const CommonBreadCrumb = () => {
	return (
		<React.Fragment>
			{crumbLinks.map((item, index) => {
				return (
					<React.Fragment key={item.link + index}>
						{history.location.pathname === item.link && (
							<div className="row supportRequestContainer w-100" key={index}>
								<div style={{ zIndex: "999" }} className="col-lg-12 col-12">
									<div
										className={`${item.crumbClass} d-flex flex-row breadCrumbLayout`}
									>
										<div>
											<img
												src={customerHome}
												className="img-fluid cursor-pointer homeCrumbIcon pr-2"
												onClick={() => history.push(item.baseLink)}
												alt="icon"
											/>
											<span
												className="breadcrumbTitle fs-14 fw-400"
												onClick={() => history.push(item.navigateLink)}
											>
												{" "}
												{item.crumbHeading}
											</span>
										</div>
										<div>
											{item.needCrumbIcon && (
												<img
													src={customerBreadCrumb}
													className="img-fluid pl-3 pr-3"
													alt="icon"
												/>
											)}
											<span
												className={`${
													item.needHighlight
														? "breadcrumbSubTitleDark"
														: "breadcrumbSubTitle"
												} fs-14 fw-400`}
												onClick={() => history.push(item.subNavigateLink)}
											>
												{" "}
												{item.crumbContent}
											</span>
										</div>
										{item.needHighlight && (
											<div>
												<img
													src={customerBreadCrumb}
													className="img-fluid pl-3 pr-3"
													alt="icon"
												/>
												<span className="breadcrumbSubTitle fs-14 fw-400">
													{" "}
													{item.subCrumbContent}
												</span>
											</div>
										)}
									</div>
								</div>
							</div>
						)}
					</React.Fragment>
				);
			})}
		</React.Fragment>
	);
};
