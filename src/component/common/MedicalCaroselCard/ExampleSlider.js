import Carousel from 'react-bootstrap/Carousel';
import React from "react";
import motor_car from "assets/news/motor_car.png";
import blue_shield from "assets/news/blue_shield.png";
import Pie from "assets/images/mobile/pie.png";


function UncontrolledExample() {
  return (
    <Carousel show={2}>
      <Carousel.Item>
        <div className="slider-card">
     <div className="d-flex align-items-center">
       <img src={motor_car} alt="Find"></img>
       <p>Locate Agency/ Workshops</p>
     </div>
   </div>
      </Carousel.Item>
      <Carousel.Item>
      <div className="slider-card">
        <div className="d-flex align-items-center">
          <img src={blue_shield} alt="Shied"></img>
          <p>Your covrage & Benifiets</p>
        </div>
      </div>

      </Carousel.Item>
      <Carousel.Item>
      <div className="slider-card">
        <div className="d-flex align-items-center">
          <img src={Pie} alt="Pie"></img>
          <p>Your Detailed Usage</p>
        </div>
        </div>
      </Carousel.Item>
    </Carousel>
  );
}

export default UncontrolledExample;