import React ,{useEffect, useRef} from "react";
import motor_car from "assets/news/motor_car.png";
import blue_shield from "assets/news/blue_shield.png";
import Pie from "assets/images/mobile/pie.png";
import ExampleSlider from "component/common/MedicalCaroselCard/ExampleSlider";
import "./style.scss";

export default function MedicalCaroselCard() {
  const sliderRef = useRef(null)
  const moveRight = ()=>{
    // console.log(sliderRef.current.scrollLeft , sliderRef.current.clientWidth/4)
    if(sliderRef.current.scrollLeft < sliderRef.current.clientWidth/4) {
      sliderRef.current.scrollLeft += 50
    }
    
  }
  const moveFirst = ()=>{
    // console.log(sliderRef.current.scrollLeft , sliderRef.current.clientWidth/4)
   
      sliderRef.current.scrollLeft = 0
    
    
  }
  
  const moveLast = ()=>{
    // console.log(sliderRef.current.scrollLeft , sliderRef.current.clientWidth/4)
 
      sliderRef.current.scrollLeft = sliderRef.current.clientWidth
   
    
  }
  
  return (
    <>
    <div ref={sliderRef} className="medical-slider">
      <div className="slider-card">
     
        <div className="d-flex align-items-center">
          <img src={motor_car} alt="Find"></img>
          <p>Locate Agency/ Workshops</p>
        </div>
      </div>
      
      <div className="slider-card">
        <div className="d-flex align-items-center">
          <img src={blue_shield} alt="Shied"></img>
          <p>Your covrage & Benifiets</p>
        </div>
      </div>
      <div className="slider-card">
        <div className="d-flex align-items-center">
          <img src={Pie} alt="Pie"></img>
          <p>Your Detailed Usage</p>
        </div>
        </div>
       
    </div>

     <div className="orange d-flex justify-content-center ">
     <div className="orange_circle_one " onClick={moveFirst}></div>
     <div className="orange_circle_two " onClick={moveRight}></div>
     <div className="orange_circle_three" onClick={moveLast}></div>
   </div>
   </>
  );
}
