import React, { useState } from "react";
import {
	Box,
	Typography,
	ThemeProvider,
	createTheme,
	Button,
	Grid,
	Icon,
	Card,
	CardContent,
	CardActions,
	Divider,
	CardMedia,
} from "@material-ui/core";
import productsicon1 from "../../../assets/svg/productsicon1.svg";
import homesubproducts1 from "../../../assets/images/homesubproducts1.png";

import ChevronRightIcon from "@material-ui/icons/ChevronRight";

const theme = createTheme({
	palette: {
		common: {},
		primary: {
			main: "#EE7500",
		},
		// redColor: palette.augmentColor({ color: red }),
	},
	typography: {
		h5: {
			fontWeight: 800,
			fontSize: 24,
		},
		subtitle2: {
			fontSize: 12,
			fontWeight: 800,
		},
		body1: {
			fontWeight: 700,
			fontSize: 19,
			color: "#4C565C",
		},
		body2: {
			fontWeight: 400,
			fontSize: 14,
			color: "#4C565C",
		},
	},
	button: {
		color: "#FFFFFF",
	},
});

export const ProductImageCard = ({
	category,
	productName,
	productDetail,
	productDescription,
	productIcon,
}) => {
	return (
		<ThemeProvider theme={theme}>
			<CardMedia image={homesubproducts1} title="Contemplative Reptile" />
		</ThemeProvider>
	);
};
