import React, { useState, useCallback } from "react";
import { history } from "service/helpers";
import Dropzone from "react-dropzone";
import { Data } from "@react-google-maps/api";
import { NormalCheckbox } from "../NormalCheckBox";
import { NormalSearch } from "../NormalSearch";
import PhoneNumberInput from "component/common/PhoneNumberInput/index";
import { NormalRadioButton } from "../NormalRadioButton";
import { NormalButton } from "../NormalButton";
import { NormalSelect } from "../NormalSelect";
import addMail from "assets/svg/Add Mail.svg";
import KSAFlagImage from "assets/images/ksaFlag.png";
import IndiaFlagImage from "assets/images/indiaFlag.png";
import officeBag from "assets/svg/Office Bag.svg";
import exclamation from "assets/svg/Exclamation Mark.svg";
import upload from "assets/svg/uploadFileIcon.svg";
import profile from "assets/svg/userProfileIcon.svg";
import selectDropDown from "assets/svg/complaintSelectDropdown.svg";
import closeIcon from "assets/svg/canvasClose.svg";
import uploader from "assets/svg/browseFileUploader.svg";
import "./style.scss";
import { FileUploader } from "../DashBoardSubComponents";

const OpenComplaintForm = ({ needForm = false }) => {
	const [files, setFiles] = useState([]);

	const [check, setCheck] = useState([]);

	const [showPreview, setShowPreview] = useState(false);

	const [isChecked, setIsChecked] = useState(false);

	const handleCheck = (e) => {
		setIsChecked(e.target.checked);
		console.log(isChecked, "jsj");
	};

	const onDrop = useCallback((acceptedFiles) => {
		setFiles(
			acceptedFiles.map((file) =>
				Object.assign(file, {
					preview: URL.createObjectURL(file),
				})
			)
		);
		if (acceptedFiles && acceptedFiles[0]) {
			const formdata = new FormData();
			formdata.append("image", acceptedFiles[0]);
		}
		setShowPreview(true);
	});

	const images = files.map((file) => (
		<div
			className="dnd-container"
			sx={{
				display: "flex",
				justifyContent: "center",
				alignItems: "center",
				width: "100%",
				height: "100%",
			}}
			key={file.name}
		>
			<img
				style={{ width: "100px" }}
				className="dnd-img mx-auto d-block"
				src={file.preview}
				alt="Screen"
			/>
		</div>
	));

	const removeImageHandler = (e) => {
		// const arr = files;
		// arr.splice(item, 1);
		// setFiles([...arr]);
		e.stopPropagation();
		setFiles([]);
		setShowPreview(false);
	};

	const [formInput, setFormInput] = useState({
		userId: "",
		policyId: "",
		message: "",
	});

	let dialingCodes = [
		{
			code: "+966",
			image: KSAFlagImage,
		},
		{
			code: "+91",
			image: IndiaFlagImage,
		},
	];

	let [selectedCode, setSelectedCode] = useState(dialingCodes[0]);

	let [phoneNumber, setPhoneNumber] = useState("");

	//initialiize state for radio buttons
	const [radioValue, setRadioValue] = useState("Motor");

	const handleFormInputChange = (e) => {
		const { name, value } = e.target || e || {};
		setFormInput({ ...formInput, [name]: value });
	};

	const handleChange = (event) => {
		let isChecked = event.target.checked;
		let value = event.target.value;
		let array = [];
		if (isChecked) {
			array = [...check];
			array.push(+value);
		} else {
			array = check.filter((id) => +id !== +value);
		}
		setCheck([...array]);
		console.log(check, "bol");
	};

	return (
		<div className="row">
			<div className="col-12">
				{needForm ? (
					<div className="d-flex justify-content-center pt-4">
						<div className="openFormContainer">
							<div>
								<NormalSearch
									className="formInputFieldOne"
									name="userId"
									value={formInput.userId}
									placeholder="Enter your  name"
									onChange={handleFormInputChange}
									needLeftIcon={true}
									leftIcon={profile}
								/>
							</div>
							<div className="pt-4">
								<NormalSearch
									className="formInputFieldOne"
									name="userId"
									value={formInput.userId}
									placeholder="ex: email@tawuniya.com.sa"
									onChange={handleFormInputChange}
									needLeftIcon={true}
									leftIcon={addMail}
								/>
							</div>
							<div className="py-4">
								<PhoneNumberInput
									className="formNewPhoneInput"
									selectInputClass="formNewSelectInputWidth"
									selectInputFlexType="formNewSelectFlexType"
									dialingCodes={dialingCodes}
									selectedCode={selectedCode}
									setSelectedCode={setSelectedCode}
									value={phoneNumber}
									name="phoneNumber"
									onChange={({ target: { value } }) => setPhoneNumber(value)}
								/>
							</div>
							<div className="borderLiningTwo"></div>
							<div className="pt-4">
								<NormalSearch
									className="formInputFieldTwo"
									name="userId"
									value={formInput.userId}
									placeholder="Subject"
									onChange={handleFormInputChange}
									needLeftIcon={true}
									leftIcon={null}
									hideLeftIcon="removeLeftIcon"
								/>
							</div>

							<div className="pt-3 pb-2">
								<p className="messageTitle fs-14 fw-700 m-0 pb-2">
									Report Details
								</p>
								<textarea className="complaintForm-textArea">
									Type here...
								</textarea>
							</div>
							<div className="d-flex flex-row">
								<div>
									<img
										src={exclamation}
										className="img-fluid exclamatorIcon pr-2"
										alt="icon"
									/>
								</div>
								<div>
									<p className="fs-12 fw-400 formConditions m-0">
										Be detailed as possible when submitting a request in order
										for us to help you more effectively. The more detailed the
										information you provide, the faster we will be able to
										resolve your issue.
									</p>
									<p className="fs-12 fw-400 formConditions m-0">
										Below are some tips:
									</p>
								</div>
							</div>
							<ul className="fs-12 fw-400 formConditionsList pl-5 mb-3">
								<li>
									Explain step-by-step how to reproduce the scenario or the
									problem that you are describing.
								</li>
								<li>If you think a document would help, please include one.</li>
								<li>
									State when the problem started and what changes were made
									immediately beforehand.
								</li>
							</ul>
							<div className="borderLiningThree mb-3"></div>
							<p className="fs-14 fw-700 doc-new-Title">Supporting Documents</p>
							<FileUploader onDrop={onDrop} files={files} />
							<div className="pt-2 pb-3 violationLiner">
								<img src={exclamation} className="img-fluid pr-2" alt="icon" />
								<span className="fs-12 fw-400 allowedFileText">
									Allowed files: JPEG, PNG, DOC, DOCX, PDF Maximum Size: 10MB
								</span>
							</div>
							<div className="pt-3">
								<div className="form-violation-group">
									<input
										type="checkbox"
										onChange={handleCheck}
										name="isChecked"
										checked={isChecked}
									/>
									<span className="terms-condition-text fs-14 fw-400">
										I hereby declare that all the above information is true and
										accurate.
									</span>
								</div>
							</div>

							<div className="borderLiningFour my-4"></div>

							<NormalButton
								label="Submit"
								className="complaintFormSubmitBtn p-4"
								onClick={() =>
									history.push(
										"/home/customerservice/SupportRequestConfirmation"
									)
								}
							/>
						</div>
					</div>
				) : (
					<div className="d-flex justify-content-center pt-4">
						<div className="openFormContainer">
							<p className="fs-30 fw-800 text-center complaint-formTitle m-0">
								Open a Complaint Request
							</p>
							<p className="fs-16 fw-400 text-center complaint-formPara">
								Please fill out the form below to submit your request and we
								will start our review on it immediately.
							</p>
							<p className="fs-20 fw-800 personalTitle">Personal Details</p>
							<div>
								<NormalSearch
									className="form-Input-Field-One"
									name="userId"
									value={formInput.userId}
									placeholder="Saudi ID / IQAMA No."
									onChange={handleFormInputChange}
									needLeftIcon={true}
									leftIcon={addMail}
								/>
							</div>
							<div className="py-4">
								<PhoneNumberInput
									className="form-Phone-InputComplaint"
									selectInputClass="form-Select-Input-Width"
									selectInputFlexType="form-Select-Flex-TypeComplaint"
									dialingCodes={dialingCodes}
									selectedCode={selectedCode}
									setSelectedCode={setSelectedCode}
									value={phoneNumber}
									name="phoneNumber"
									onChange={({ target: { value } }) => setPhoneNumber(value)}
								/>
							</div>
							<div className="borderLiningTwo mb-3"></div>
							<p className="fs-20 fw-800 m-0 pb-3 requestTitle">
								Request Details
							</p>
							<p className="fs-14 fw-700 requestTypePara">Request Type</p>
							<div className="d-flex flex-row">
								<div className="col-6 p-0">
									<NormalRadioButton
										type="radio"
										name="radioOne"
										value="Motor"
										onChange={(e) => setRadioValue(e.target.value)}
										radioValue="Motor"
										checked={radioValue === "Motor"}
										isStyledRadio={true}
										radioContainer={
											radioValue === "Motor"
												? "higlightRadioContainer"
												: "normalRadioContainer"
										}
									/>
								</div>
								<div className="col-6 pr-0">
									<NormalRadioButton
										type="radio"
										name="radioOne"
										value="Medical"
										onChange={(e) => setRadioValue(e.target.value)}
										radioValue="Medical"
										checked={radioValue === "Medical"}
										isStyledRadio={true}
										radioContainer={
											radioValue === "Medical"
												? "higlightRadioContainer"
												: "normalRadioContainer"
										}
									/>
								</div>
							</div>
							<div className="d-flex flex-row pt-3">
								<div className="col-6 p-0">
									<NormalRadioButton
										type="radio"
										name="radioOne"
										value="P&C (Property & Casualty)"
										onChange={(e) => setRadioValue(e.target.value)}
										radioValue="P&C (Property & Casualty)"
										checked={radioValue === "P&C (Property & Casualty)"}
										isStyledRadio={true}
										radioContainer={
											radioValue === "P&C (Property & Casualty)"
												? "higlightRadioContainer"
												: "normalRadioContainer"
										}
									/>
								</div>
								<div className="col-6 pr-0">
									<NormalRadioButton
										type="radio"
										name="radioOne"
										value="E-Services Support"
										onChange={(e) => setRadioValue(e.target.value)}
										radioValue="E-Services Support"
										checked={radioValue === "E-Services Support"}
										isStyledRadio={true}
										radioContainer={
											radioValue === "E-Services Support"
												? "higlightRadioContainer"
												: "normalRadioContainer"
										}
									/>
								</div>
							</div>
							<div className="pt-3">
								<div className="Complaint-SelectContainer">
									<p className="fs-12 fw-400 service-Type-ComplaintLabel m-0">Service Type</p>
									<NormalSelect									
										className="Complaint-FormSelectInput"
										arrowVerticalAlign="Complaint-ArrowAlign"
										placeholder="Recovery"
										selectArrow={selectDropDown}
										selectControlHeight="22px"
										selectFontWeight="700"
									/>
								</div>
							</div>
							<div className="pt-3">
								<NormalSearch
									className="form-Input-Field-One"
									name="policyId"
									value={formInput.policyId}
									placeholder="Policy Number"
									onChange={handleFormInputChange}
									needLeftIcon={true}
									leftIcon={officeBag}
								/>
							</div>
							<div className="pt-3 pb-2">
								<p className="messageTitle fs-14 fw-700 m-0 pb-2">Message</p>
								<textarea className="complaintForm-textArea">
									Type here...
								</textarea>
							</div>
							<div className="d-flex flex-row">
								<div>
									<img
										src={exclamation}
										className="img-fluid exclamatorIcon pr-2"
										alt="icon"
									/>
								</div>
								<div>
									<p className="fs-12 fw-400 formConditions m-0">
										Be detailed as possible when submitting a request in order
										for us to help you more effectively. The more detailed the
										information you provide, the faster we will be able to
										resolve your issue.
									</p>
									<p className="fs-12 fw-400 formConditions m-0">
										Below are some tips:
									</p>
								</div>
							</div>
							<ul className="fs-12 fw-400 formConditionsList pl-5 mb-3">
								<li>
									Explain step-by-step how to reproduce the scenario or the
									problem that you are describing.
								</li>
								<li>If you think a document would help, please include one.</li>
								<li>
									State when the problem started and what changes were made
									immediately beforehand.
								</li>
							</ul>
							<div className="borderLiningThree mb-3"></div>
							<p className="fs-20 fw-800 doc-Title">Supporting Documents</p>

							<Dropzone onDrop={onDrop}>
								{({ getRootProps, getInputProps }) => (
									<div
										{...getRootProps({
											className: "dropzone",
											onDrop: (event) => event.preventDefault(),
										})}
									>
										<div
											style={{
												display: "flex",
												justifyContent: "center",
												alignItems: "center",
												flexDirection: "column",
											}}
											className="image-upload-wrap"
										>
											<input {...getInputProps()} />
											{!showPreview ? (
												<div className="drag-text">
													<div>
														<img
															src={upload}
															className="img-fluid mx-auto d-block pb-3"
															alt="uploadicon"
														/>
														<div className="d-flex justify-content-center">
															<div>
																<p className="uploadTitle fs-14 fw-400 m-0 pb-3">
																	Upload files by drag and drop or{" "}
																	<span className="uploadLink">
																		click to upload
																	</span>
																	.
																</p>
															</div>
														</div>
														<p className="fs-12 fw-400 allowedFormats text-center">
															Allowed files: JPEG, PNG, DOC, DOCX, PDF Maximum
															Size: 10MB
														</p>
													</div>
												</div>
											) : (
												<>
													<div className="drag-text">
														<div>
															<div>{images}</div>
															{showPreview ? (
																<img
																	src={closeIcon}
																	className="img-fluid removeImgBtn"
																	onClick={(e) => removeImageHandler(e)}
																	alt="icon"
																/>
															) : (
																""
															)}
															<div className="d-flex justify-content-center">
																<div>
																	<p className="uploadTitle fs-14 fw-400 m-0 pb-3">
																		Upload files by drag and drop or{" "}
																		<span className="uploadLink">
																			click to upload
																		</span>
																		.
																	</p>
																</div>
															</div>
															<p className="fs-12 fw-400 allowedFormats text-center">
																Allowed files: JPEG, PNG, DOC, DOCX, PDF Maximum
																Size: 10MB
															</p>
														</div>
													</div>
												</>
											)}
										</div>
									</div>
								)}
							</Dropzone>

							<div className="borderLiningFour my-4"></div>

							<NormalButton
								label="Submit"
								className="complaintFormSubmitBtn p-4"
								onClick={() =>
									history.push(
										"/home/customerservice/SupportRequestConfirmation"
									)
								}
							/>
						</div>
					</div>
				)}
			</div>
		</div>
	);
};

export default OpenComplaintForm;
