import React, { useState, useCallback, useEffect } from "react";

import {Table } from "react-bootstrap"
import Tick from "assets/svg/MobileAssets/tick.svg"
import Cancel from "assets/svg/MobileAssets/cancel.svg"
import Plus from "assets/svg/MobileAssets/plus.svg"



import "./style.scss"






const MobileMotorTable=(type)=>{

  const [tabledatas , setTabledatas]=useState([])
  const [header , setHeader]=useState([])



  useEffect(()=>{
    console.log(type.type)
    if(type.type==="1"){
      setTabledatas(tabledata)
      setHeader(title)
    }
    if(type.type==="2"){
      setTabledatas(breakdowndata)
      setHeader(title2)
    }
  })

  const title=['Covers & Benefits',' Al Shamel','Sanad','Sanad Plus']
  const title2=['Component Covered','Gold','Silver','Bronze']

  const breakdowndata=[

    {
      item1:"Engine",
      item2:"20,000",
      item3:"20,000",
      item4:"20,000"
    },
    {
      item1:"Gearbox",
      item2:"10,000",
      item3:"10,000",
      item4:"10,000"
    },
    {
      item1:"Differential",
      item2:"10,000",
      item3:"10,000",
      item4:"10,000"
    },
    {
      item1:"Turbo Assembly",
      item2:"5,000",
      item3:"5,000",
      item4:Cancel
    },
    {
      item1:"Prop Shaft & CV Joints",
      item2:"4,000",
      item3:"4,000",
      item4:Cancel
    },
    {
      item1:"Steering Mechanism",
      item2:"4,000",
      item3:"4,000",
      item4:Cancel
    },
    {
      item1:"Wheel Bearings",
      item2:"4,000",
      item3:"4,000",
      item4:Cancel
    },
    {
      item1:"Breaking System",
      item2:"4,000",
      item3:"4,000",
      item4:Cancel
    },
    {
      item1:"Breaking System",
      item2:"4,000",
      item3:"4,000",
      item4:Cancel
    },
    {
      item1:"Fuel System",
      item2:"4,000",
      item3:"4,000",
      item4:Cancel
    },
    {
      item1:"Electrical Components",
      item2:"4,000",
      item3:"4,000",
      item4:Cancel
    },
    {
      item1:"Suspension",
      item2:"4,000",
      item3:"4,000",
      item4:Cancel
    },
    {
      item1:"Malicious damage",
      item2:"4,000",
      item3:"4,000",
      item4:Cancel
    },
    {
      item1:"Cylinder Head Gasket",
      item2:"4,000",
      item3:"4,000",
      item4:Cancel
    },
  ]

  const tabledata=[
    {
      item1:"Cover against Loss/Damage to own vehicle",
      item2:Tick,
      item3:Cancel,
      item4:"Upto 10K"
  },
      {
        item1:"Third party liability upto 10m SR",
        item2:Tick,
        item3:Tick,
        item4:Tick
    },
    {
      item1:"Emergency Medical expenses",
      item2:Tick,
      item3:Cancel,
      item4:Cancel
    },
    {
      item1:"End to end claim services",
      item2:Tick,
      item3:Cancel,
      item4:Cancel
    },
    {
      item1:"No claims discount",
      item2:Tick,
      item3:Tick,
      item4:Tick
    },
    {
      item1:"Loyalty discount",
      item2:Tick,
      item3:Tick,
      item4:Tick
    },
    {
      item1:"Personal Accident (passengers)",
      item2:Tick,
      item3:Tick,
      item4:Tick
    },
    {
      item1:"Personal Accident (Driver)",
      item2:Tick,
      item3:Plus,
      item4:Plus
    },
    {
      item1:"Roadside Assistance",
      item2:Tick,
      item3:Plus,
      item4:Plus
    },
    {
      item1:"Natural Perils (Flood, Hail)",
      item2:Tick,
      item3:Plus,
      item4:Plus
    },
    {
      item1:"Glass Cover",
      item2:Tick,
      item3:Plus,
      item4:Plus
    },
    {
      item1:"Theft",
      item2:Tick,
      item3:Plus,
      item4:Plus
    },
    {
      item1:"Malicious damage",
      item2:Tick,
      item3:Plus,
      item4:Plus
    },
    {
      item1:"Tawuniya Drive",
      item2:Tick,
      item3:Plus,
      item4:Plus
    },
    ]


    return(
        <>
        <div className="container maintable_motor">
        <table className="mt-5">

        <tr>
       <td className="fw-800"></td>
       <td className="fw-800 bestseller ">
           <p className="text-center fs-11">Best Seller</p>
       </td>
       <td className="fw-800 mt-2"></td>
       <td className="fw-800 text-center"></td>
   </tr>
  
       
        <tr>
       
            <td className="fw-800">{header[0]}</td>
            <td className="fw-800 topborder ">
              {header[1]}
            <p className="mt-3 fw-400">Comprehensive</p>
            </td>
            <td className="fw-800 mt-2">{header[2]}</td>
            <td className="fw-800 text-center">{header[3]}</td>
        </tr>
       
          {tabledatas.map((item,index)=>{
            return(
              <tr>
              <td>{item.item1}</td>
              {item.item2.split(".").pop()==="svg" ? <>
              {index===13 ?<>
                <td className="lastrow"><img className="ml-4 " src={item.item2} alt="tick"/></td>
              </>:<>
              <td><img className="ml-4" src={item.item2} alt="tick"/></td>
              </>}
             
              </>:<>
              {index===13 ?<>
                <td className="tdfont lastrow pl-4">{item.item2}</td>
              </>:<>
              <td className="tdfont pl-4">{item.item2}</td>
              </>}
             
              </>}
              {item.item3.split(".").pop()==="svg" ? <>
              <td><img src={item.item3} alt="tick"/></td>
              </>:<>
              <td className="tdfont">{item.item3}</td>
              </>}
              {item.item4.split(".").pop()==="svg" ? <>
              <td><img src={item.item4} alt="tick"/></td>
              </>:<>
              <td className="tdfont">{item.item4}</td>
              </>}
              </tr>
            )
          })}
           
        
       
    </table>
        </div>
     
        </>
    )


}

export default MobileMotorTable