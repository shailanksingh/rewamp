import React from "react";
import { NormalButton } from "../NormalButton";
import drive from "assets/svg/driveImg.svg";
import driveBanner from "assets/images/Drive Banner (small).png"
import "./style.scss";

export const RewardCard = ({
  cardSubTitle,
  subtitleClass,
  label,
  className,
}) => {
  return (
    <div className="rewardConatiner">
      <img src={driveBanner} className="img-fluid driveBanner" alt="banner" />
      <div className="rewardSubContainer">
        <img src={drive} className="img-fluid appdevice" alt="apppic"/>
        <div className="rewardContent">
        <p className={`${subtitleClass} fs-18 fw-800`}>{cardSubTitle}</p>
        <NormalButton label={label} className={className} />
        </div>
      </div>
    </div>
  );
};
