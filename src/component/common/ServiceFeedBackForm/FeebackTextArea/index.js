import React from "react";
import "../style.scss";

export const FeedbackTextArea = () => {
  return (
    <div className="feebackForm-textArea-liner pb-2">
      <textarea className="feedback-Form-textArea mt-4">Type here...</textarea>
    </div>
  );
};
