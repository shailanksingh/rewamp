import React, { useState } from "react";
import Number1 from "assets/tele-request/number1.svg";
import Number2 from "assets/tele-request/number2.svg";
import Number3 from "assets/tele-request/number3.svg";
import Number4 from "assets/tele-request/number4.svg";
import Number5 from "assets/tele-request/number5.svg";
import Number6 from "assets/tele-request/number6.svg";
import Number7 from "assets/tele-request/number7.svg";
import Number8 from "assets/tele-request/number8.svg";
import Number9 from "assets/tele-request/number9.svg";
import Number10 from "assets/tele-request/number10.svg";
import "../style.scss";

export const NumberRating = () => {
  const [rate, setRate] = useState(0);

  const ratingData = [
    {
      id: 0,
      icon: Number1,
    },
    {
      id: 1,
      icon: Number2,
    },
    {
      id: 2,
      icon: Number3,
    },
    {
      id: 3,
      icon: Number4,
    },
    {
      id: 4,
      icon: Number5,
    },
    {
      id: 5,
      icon: Number6,
    },
    {
      id: 6,
      icon: Number7,
    },
    {
      id: 7,
      icon: Number8,
    },
    {
      id: 8,
      icon: Number9,
    },
    {
      id: 9,
      icon: Number10,
    },
  ];

  return (
    <React.Fragment>
        <div className="d-flex justify-content-between pt-4">
      {ratingData.map((item, i) => {
        return (
          <div key={i} onClick={() => setRate(item.id)}>
            <img
              src={item.icon}
              className="img-fluid number-rating-icon cursor-pointer"
              alt="icon"
            />
          </div>
        );
      })}
    </div>
    <div className="d-flex justify-content-between pt-2">
    <div>
      <p className="poorLike fs-11 fw-700">Not at all Likely</p>
    </div>
    <div>
      <p className="goodLike fs-11 fw-700">Extremely Likely</p>
    </div>
  </div>
    </React.Fragment>
  );
};
