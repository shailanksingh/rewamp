import React, {useState} from "react";
import Emoji1 from "assets/tele-request/emoji1.svg";
import Emoji2 from "assets/tele-request/emoji2.svg";
import Emoji3 from "assets/tele-request/emoji3.svg";
import Emoji4 from "assets/tele-request/emoji4.svg";
import Emoji5 from "assets/tele-request/emoji5.svg";
import "../style.scss";

export const EmojiRating = () =>{
    const [emoji, setEmoji] = useState(0);

    const emojiData = [
        {
          id: 0,
          icon: Emoji1,
        },
        {
          id: 1,
          icon: Emoji2,
        },
        {
          id: 2,
          icon: Emoji3,
        },
        {
          id: 3,
          icon: Emoji4,
        },
        {
          id: 4,
          icon: Emoji5,
        },
      ];

    return(
        <div className="d-flex justify-content-between pt-4 emojiBoxLining pb-2">
              {emojiData.map((item, i) => {
                return (
                  <div key={i}>
                    <div
                      className={
                        emoji === item.id
                          ? "highlight-emojiBlockContainer cursor-pointer"
                          : "normal-emojiBlockContainer cursor-pointer"
                      }
                      onClick={() => setEmoji(item.id)}
                    >
                      <img
                        src={item.icon}
                        className="img-fluid emogi-express-icon"
                        alt="icon"
                      />
                    </div>
                  </div>
                );
              })}
            </div>
    )
}