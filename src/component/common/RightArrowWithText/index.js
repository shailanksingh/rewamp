import React from "react";
import "./style.scss";
import right_arrow from "assets/images/mobile/right_arrow.png";
import white_arrow from "assets/images/mobile/right-arrow-white.png";

const RightArrowWithText = ({ label, isWhiteArrow = false }) => {
  return (
    <div className="right_arrow_container">
      <label>{label}</label>
      <img src={isWhiteArrow ? white_arrow : right_arrow} alt="arrow" />
    </div>
  );
};

export default RightArrowWithText;
