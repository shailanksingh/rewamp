import React, { useState } from "react";
import { useParams } from "react-router-dom";
import { Card, Modal, Button, Form } from "react-bootstrap";
import Tick from "assets/tele-request/tick.svg";
import User from "assets/tele-request/user.svg";
import Approval from "assets/tele-request/Horizontal.svg";
import Phone from "assets/tele-request/phone.svg";
import Contact from "assets/tele-request/contact.svg";
import Emoji1 from "assets/tele-request/emoji1.svg";
import NumberPointCircle from "assets/svg/dashboardIcons/NumberPointCircle.svg";
import PointCircle from "assets/svg/dashboardIcons/PointCircle.svg";
import Emoji2 from "assets/tele-request/emoji2.svg";
import Emoji3 from "assets/tele-request/emoji3.svg";
import Emoji4 from "assets/tele-request/emoji4.svg";
import Emoji5 from "assets/tele-request/emoji5.svg";
import Number1 from "assets/tele-request/number1.svg";
import Number2 from "assets/tele-request/number2.svg";
import Number3 from "assets/tele-request/number3.svg";
import Number4 from "assets/tele-request/number4.svg";
import Number5 from "assets/tele-request/number5.svg";
import Number6 from "assets/tele-request/number6.svg";
import Number7 from "assets/tele-request/number7.svg";
import Number8 from "assets/tele-request/number8.svg";
import Number9 from "assets/tele-request/number9.svg";
import Number10 from "assets/tele-request/number10.svg";
import Close from "assets/tele-request/close.svg";
import requestCar from "assets/svg/dashboardIcons/requsetVehicle.svg";
import requestLoaction from "assets/svg/dashboardIcons/requestLocation.svg";
import women from "assets/svg/dashboardIcons/Confirmation Woman Dashboard.svg";
import "./style.scss";

export const RequestIdPage = ({ requestIdData }) => {
  const { id } = useParams();
  const [show, setShow] = useState(false);

  const [alert, setAlert] = useState(false);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  return (
    <div className="requestMaincontent pt-4">
      <Card className={alert ? "requestmaincard" : "alert-requestmaincard"}>
        {alert && (
          <div className="pt-2">
            <div className="container alert  mt-4">
              <div className="d-flex">
                <img src={Tick} className="img-fluid pr-3" alt="tick" />
                <p className="alertptag m-0">We have received your request</p>
              </div>
            </div>
          </div>
        )}

        <div className="container-fluid ml-2">
          <p
            className="alertptag-heading m-0 pb-2"
            onClick={() => setAlert(!alert)}
          >
            {requestIdData.requestIdTitle}
          </p>
          <div class="d-flex justify-content-between">
            <div>
              <h4 className={`${alert ? "fs-24" : "fs-20"} fw-800 requestid`}>
                {requestIdData.requestId}
              </h4>
            </div>
            <div className="col-5">
              <table class="progressbar">
                <tr class="counter">
                  <td class="active">
                    <img className="progress-icons" src={PointCircle} alt="" />
                  </td>
                  <td class="active">
                    <img className="progress-icons" src={PointCircle} alt="" />
                    <span></span>
                  </td>
                  <td class="active">
                    <img
                      className="progress-icons"
                      src={!alert ? NumberPointCircle : PointCircle}
                      alt=""
                    />
                    <span></span>
                  </td>
                </tr>
                <tr class="descriptions">
                  <td class="active">Approved</td>
                  <td class="active">In Progress</td>
                  <td class="active">Completed</td>
                </tr>
              </table>
            </div>
          </div>
          <hr className="hrtag mr-5 pb-2" />
          <p className="fw-800 alertptag-heading pb-2">Request Details</p>
          <hr className="mb-3 hrtag mr-5 pb-2" />

          <div className="detailbg p-2">
            <div className="container">
              <div className="row">
                {/* <div className="col-lg-0 pt-3">
                  <div className="container">
                    <img className="img-fluid" src={requestCar} alt="contact" />
                  </div>
                </div> */}

                <div className="col-lg-4  ">
                  {/* <p className="pt-3 alertptag-heading fs-16 fw-400">Member</p>
                  <p className="fs-16 fw-800 reducespace">Prashant Dixit </p> */}

                  <div className="d-flex flex-row">
                    <div className="pr-2">
                      <img
                        className="img-fluid pt-3 mt-1"
                        src={requestIdData.detailIconOne}
                        alt="contact"
                      />
                    </div>
                    <div>
                      <p className="fs-13 fw-400 alertptag-heading pt-3">
                        {requestIdData.detailTitleOne}
                      </p>
                      <p className="fs-13 fw-800 reducespace">
                        {requestIdData.detailParaOne}
                      </p>
                    </div>
                  </div>
                </div>

                <div className="col-lg-0">
                  <div class="vertical-line "></div>
                </div>

                {/* <div className="col-lg-0 pt-3">
                  <div className="container">
                    <img className="img-fluid" src={requestLoaction} alt="contact" />
                  </div>
                </div> */}

                <div className="col-lg-4">
                  <div className="d-flex flex-row">
                    <div className="pr-2">
                      <img
                        className="img-fluid pt-3 mt-1"
                        src={requestIdData.detailIconTwo}
                        alt="contact"
                      />
                    </div>
                    <div>
                      <p className="fs-13 fw-400 alertptag-heading pt-3">
                        {requestIdData.detailTitleTwo}
                      </p>
                      <p className="fs-13 fw-800 reducespace">
                        {requestIdData.detailParaTwo}
                      </p>
                    </div>
                  </div>
                </div>

                {requestIdData.detailIconThree ? (
                  <div className="col-lg-0">
                    <div class="vertical-line "></div>
                  </div>
                ) : null}

                <div className="col-lg-3">
                  <div className="d-flex flex-row">
                    <div className="pr-2">
                      {requestIdData.detailIconThree ? (
                        <img
                          className="img-fluid pt-3 mt-1"
                          src={requestIdData.detailIconThree}
                          alt="contact"
                        />
                      ) : null}
                    </div>
                    <div>
                      <p className="fs-13 fw-400 alertptag-heading pt-3">
                        {requestIdData.detailTitleThree}
                      </p>
                      <p className="fs-13 fw-800 reducespace">
                        {requestIdData.detailParaThree}
                      </p>
                    </div>
                  </div>
                </div>

                {requestIdData.detailIconFour && (
                  <div className="col-12">
                    <div className="d-flex flex-row">
                      <div className="pr-2">
                        <img src={requestIdData.detailIconFour} alt="icon" />
                      </div>
                      <div>
                        <p className="fs-13 fw-400 alertptag-heading pt-1 m-0">
                          {requestIdData.detailTitleFour}
                        </p>
                        <p className="fs-13 fw-800 reducespace-consult">
                          {requestIdData.detailParaFour}
                        </p>
                      </div>
                    </div>
                  </div>
                )}
              </div>
              {/* ------------ */}
            </div>
          </div>
        </div>

        {!alert && (
          <div>
            <img
              src={women}
              className="img-fluid mx-auto d-block pt-4"
              alt="women"
            />
            <p className="fs-14 fw-400 requestConfirmedText text-center pt-4 mb-0">
              Your request has been completed
            </p>
          </div>
        )}
      </Card>

      {/* <Button variant="primary" onClick={handleShow}>
        Launch demo modal
      </Button>
      <Modal className="modal" show={show} onHide={handleClose}>
        <Modal.Body>
        <img className="closeicon pr-2" onClick={handleClose} src={Close} alt ="close"/>
          <div className="container pt-4">
            <p className="fw-800 fs-18">We would really appreciate it if you would just take a moment to let us know about your experience.</p>
            <hr/>
            <p className="fw-800">How satisfied were you with the effort it took to complete your request?</p>
            <div className="container row">
              <div className="col-lg-2">
              <img src={Emoji1} alt="emoji"/>
              </div>
              <div className="col-lg-2">
              <img className="pl-2" src={Emoji2} alt="emoji"/>
              </div>
              <div className="col-lg-2">
              <img className="pl-3"  src={Emoji3} alt="emoji"/>       
              </div>
              <div className="col-lg-2">
              <img className="pl-3"  src={Emoji5} alt="emoji"/>
              </div>
              <div className="col-lg-2">
              <img className="pl-4"  src={Emoji5} alt="emoji"/>
              </div>         
            </div>
            <hr/>
           
            <p className="fw-800">How likely are you would recommend Tawuniya to a friend, relative, or colleague?</p>
           
           
            <div className="d-flex">
              <img src={Number1} alt="number"/>
              <img src={Number2} alt="number"/>  
              <img src={Number3} alt="number"/>
              <img src={Number4} alt="number"/>
              <img src={Number5} alt="number"/>  
              <img src={Number6} alt="number"/>
              <img src={Number7} alt="number"/>
              <img src={Number8} alt="number"/>
              <img src={Number9} alt="number"/>  
              <img src={Number10} alt="number"/>

            

            </div>
            <div className="d-flex">
            <p className="fw-800 pt-2">Not at all Likely</p>
            <p className="textalign-right fw-800 pt-2">Extremely Likely</p>
            </div>
            <hr/>
           
              <p className="fw-800">How may we make your experience better in the future?</p>
              <Form.Control
              as="textarea"
              placeholder="Type here.."
              style={{ height: "100px" }}
            />
            <hr/>
              <Button className="sendbtn ">Send My Feedback</Button>

          </div>
        </Modal.Body>
       
      </Modal> */}
      <div className="requestIdLiner mt-4 mb-3"></div>
    </div>
  );
};
