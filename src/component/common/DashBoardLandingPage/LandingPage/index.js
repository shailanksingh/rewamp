import React, { useContext } from "react";
import Accordion from "react-bootstrap/Accordion";
import { useAccordionButton, AccordionContext } from "react-bootstrap";
import { NormalButton } from "component/common/NormalButton";
import { RoadAssistanceMobile } from "component/RoadAssistanceMobile";
import addIcon from "assets/svg/addIcon.svg";
import hideCollapse from "assets/svg/hideCollapse.svg";
import "./style.scss";
import { history } from "service/helpers";

function CustomToggle({ children, eventKey, callback }) {
  const { activeEventKey } = useContext(AccordionContext);

  const decoratedOnClick = useAccordionButton(
    eventKey,
    () => callback && callback(eventKey)
  );

  const isCurrentEventKey = activeEventKey === eventKey;

  return (
    <button
      type="button"
      className="dashboard-addButton"
      onClick={decoratedOnClick}
    >
      <div className="d-flex flex-row">
        <div>
          <img
            src={isCurrentEventKey ? hideCollapse : addIcon}
            className="img-fluid dashboard-accordionCollapseIcon pr-3"
            alt="icon"
          />
        </div>
        <div>{children}</div>
      </div>
    </button>
  );
}

export const DashBoardLandingPage = ({
  dashboardProgressData,
  faqDashboardData,
  dashboardHeaderData,
  dashBoardContentData,
  isLayout = false,
}) => {
  return (
    <div>
      {window.innerWidth < 600 ? (
        <RoadAssistanceMobile />
      ) : (
        <React.Fragment>
          <div className="road-home-container">
            <div className="row px-3">
              <div className="col-12 dashboard-header-content-container p-3 mt-4">
                <div className="d-flex justify-content-between">
                  <div>
                    <p className="dashboard-header-content-title fs-14 fw-400 m-0">
                      {dashboardHeaderData.headerBannerTitle}
                    </p>
                    <p className="dashboard-header-content-para fs-18 fw-800 m-0">
                      {dashboardHeaderData.headerBannerPara}
                    </p>
                  </div>
                  <div>
                    <NormalButton
                      label="Request The Service"
                      className="dashboard-requestService-Button"
                      onClick={() => history.push(dashboardHeaderData.url)}
                    />
                  </div>
                </div>
              </div>

              <div
                className="col-12 road-sub-home-container mt-4 "
                id={dashBoardContentData.bannerClass}
              >
                <p className="landing-container-para fs-11 fw-400 line-height-22 m-0">
                  {dashBoardContentData.bannerPara}
                </p>
                <img
                  src={dashBoardContentData.bannerImg}
                  className={`${dashBoardContentData.bannerImgClass} img-fluid`}
                  alt="carBanner"
                />
              </div>

              <div className="col-lg-12 col-12 benifitServiceContainer pt-5 pb-2">
                <p className="fs-22 fw-800 benifitServiceTitle m-0 pb-3">
                  {dashBoardContentData.benifitTitle}
                </p>
                {isLayout ? (
                  <div className="row">
                    {dashboardProgressData.map((item, i) => {
                      return (
                        <div
                          className={`${item.class} col-lg-4 col-md-4 col-12 pb-3 howItWorksLayoutBox`}
                          key={i}
                        >
                          <div className="how-It-Works-Container">
                            {i < dashboardProgressData.length - 1 ? (
                              <img
                                src={item.arrowIcon}
                                className={`img-fluid progressArrow`}
                                alt="icon"
                              />
                            ) : (
                              ""
                            )}

                            <div className="d-flex justify-content-center">
                              <div>
                                <div className="number-progresss-circle mx-auto d-block mb-1">
                                  <p className="number-progress fs-10 fw-800 m-0">
                                    {item.progressNo}
                                  </p>
                                </div>
                                <p className="how-progressSubTitle fs-16 fw-800 m-0 text-center pb-2">
                                  {item.progresssTitle}
                                </p>
                                <p className="how-progressSubPara fs-12 fw-400 m-0 text-center">
                                  {item.progressPara}
                                </p>
                              </div>
                            </div>
                          </div>
                        </div>
                      );
                    })}
                  </div>
                ) : (
                  <div className="row">
                    {dashboardProgressData.map((item, i) => {
                      return (
                        <div
                          className={`${item.class} col-lg-6 col-md-6 col-12 pb-3 howItWorksLayoutBox px-0`}
                          key={i}
                        >
                          <div className="how-It-Works-Container">
                            <img
                              src={item.arrowIcon}
                              className={`${
                                item.id % 2 === 0 ? "" : "d-none"
                              } img-fluid progressArrow`}
                              alt="icon"
                            />
                            <div className="d-flex justify-content-center">
                              <div>
                                <div className="number-progresss-circle mx-auto d-block mb-1">
                                  <p className="number-progress fs-10 fw-800 m-0">
                                    {item.progressNo}
                                  </p>
                                </div>
                                <p className="how-progressSubTitle fs-16 fw-800 m-0 text-center pb-2">
                                  {item.progresssTitle}
                                </p>
                                <p className="how-progressSubPara fs-12 fw-400 m-0 text-center">
                                  {item.progressPara}
                                </p>
                              </div>
                            </div>
                          </div>
                        </div>
                      );
                    })}
                  </div>
                )}
              </div>
              <div className="col-lg-12 col-12 roadAssisst-faqConatiner pt-5">
                <p className="fs-22 fw-800 dashboard-faqTitle text-center m-0 pt-3 pb-1">
                  {dashBoardContentData.faqTitle}
                </p>
                <p className="fs-14 fw-400 dashboard-faqPara text-center">
                  {dashBoardContentData.faqPara}
                </p>

                <div className="row">
                  {faqDashboardData.map((item, index) => {
                    return (
                      <div className="col-lg-12 col-12 pb-2 p-0" key={index}>
                        <div className="dashboard-reportAccordionQuestion">
                          <Accordion defaultActiveKey={item.evenKey}>
                            <CustomToggle eventKey="0">
                              <p className="fs-14 fw-400 pt-3 mt-1 dashboard-accordionQuestion">
                                {item.question}
                              </p>
                            </CustomToggle>
                            <div>
                              <Accordion.Collapse eventKey="0">
                                <div className="pt-1 pb-1">
                                  <div className="dashboard-accordionAnswer-container">
                                    <p className="fs-14 fw-400 p-3 dashboard-accordionAnswer">
                                      {item.answer}
                                    </p>
                                  </div>
                                </div>
                              </Accordion.Collapse>
                            </div>
                          </Accordion>
                        </div>
                      </div>
                    );
                  })}
                </div>
              </div>
              <div className="col-lg-12 col-12 p-0">
                <div className="dashboard-request-BtnConatiner mb-3">
                  {/* <NormalButton
                    label="Request this Service"
                    className="dashboard-requestBtn p-4"
                  /> */}
                </div>
              </div>
            </div>
          </div>
        </React.Fragment>
      )}
    </div>
  );
};
