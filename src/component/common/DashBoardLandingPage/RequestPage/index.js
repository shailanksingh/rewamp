import React from "react";
import { history } from "service/helpers";
import { NormalButton } from "component/common/NormalButton";
import "./style.scss";

export const RequestPage = ({ children, requestHeaderData }) => {
  return (
    <div className="row requestPage-container mt-4">
      <div className="col-lg-12 col-12">
        <div className="request-header-container p-3">
          <div className="d-flex justify-content-between">
            <div>
              <p className="request-header-content-title fs-14 fw-400 m-0">
                {requestHeaderData.requestTitle}
              </p>
              <p className="request-header-content-para fs-18 fw-800 m-0">
                {requestHeaderData.requestPara}
              </p>
            </div>
            <div>
              <NormalButton label="Cancel" className="requestCancelBtn px-3" onClick={() => history.push(requestHeaderData.url)} />
            </div>
          </div>
        </div>
      </div>
      <div className="col-lg-12 col-12 pt-4">
      <div className="request-body-container">{children}</div>
        <div className="request-bottom-liner mt-4 mb-2" />
      </div>
    </div>
  );
};
