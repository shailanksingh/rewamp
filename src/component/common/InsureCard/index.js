import React, { useState } from "react";

import motor from "assets/svg/recentFeedMotor.svg";
import whiteCarMob from "assets/svg/whitecarmob.png";
import whiteHeartMob from "assets/svg/whiteheartmob.png";
import blueHeartMob from "assets/svg/blueheartmob.png";
import blueTravelMob from "assets/svg/bluetravelmob.png";
import blueCoronaMob from "assets/svg/bluecoronamob.png";

import normalTravel from "assets/svg/recentFeedNormalTravel.svg";
import covidNormal from "assets/svg/covidNormalPill.svg";
import {MotorForm} from "component/common/RecentFeeds/RecentForms";
import HomeBg from "assets/images/mobile/home-bg.png";
import "../../HomePage/Landingpage/LandingPageMobile/style.scss";
import "./style.scss";

export default function InsureCard() {
  const [pillIndex, setPillIndex] = useState(0);

  const pillData = [
    {
      id: 0,
      normalPillIcon: whiteCarMob,
      highlightPillIcon: motor,
      pillLabel: "Motor",
      normalPillClass: "",
      higlightPillClass: "",
      content: <MotorForm title={"Individuals"} />,
    },
    {
      id: 1,
      normalPillIcon: whiteHeartMob,
      highlightPillIcon: blueHeartMob,
      pillLabel: "Health",
      normalPillClass: "",
      higlightPillClass: "",
      content: <MotorForm title={"My Family"} />,
    },
    {
      id: 2,
      normalPillIcon: normalTravel,
      highlightPillIcon: blueTravelMob,
      pillLabel: "Travel",
      normalPillClass: "",
      higlightPillClass: "",
      content: <MotorForm isBtn={true} />,
    },
    {
      id: 3,
      normalPillIcon: covidNormal,
      highlightPillIcon: blueCoronaMob,
      pillLabel: "Covid-19",
      normalPillClass: "",
      higlightPillClass: "",
      content: <MotorForm isBtn={true} />,
    },
  ];
  return (
    <div className="position-relative">
      <div className="row landing-insurance">
        <div className="col-12 recentFeedContainer p-0">
          <div className="col-lg-12 col-12 recent-main-container">
            <div className="recentContainer">
              <div className="recentFeed-navpillFlow">
                {pillData.map((item, index) => {
                  return (
                    <div className="pillBox" key={index}>
                      <div
                        className={
                          item.id === pillIndex
                            ? "recentFeedHighlightPill"
                            : "recentFeedNormalPill"
                        }
                        onClick={() => setPillIndex(item.id)}
                      >
                        <img
                          src={
                            item.id === pillIndex
                              ? item.highlightPillIcon
                              : item.normalPillIcon
                          }
                          className="img-fluid pillIcon pr-2"
                          alt={item.pillLabel}
                        />{" "}
                        <span className="fs-16 fw-400 recentFeed-normalPillLabel">
                          {item.pillLabel}
                        </span>
                      </div>
                    </div>
                  );
                })}
              </div>
              <div className="row">
                <div className="col-12">
                  {pillData.map((item, index) => {
                    return (
                      <div key={index}>
                        {item.id === pillIndex && item.content}
                      </div>
                    );
                  })}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <img src={HomeBg} className="insurance-bg" alt="..." />
    </div>
  );
}
