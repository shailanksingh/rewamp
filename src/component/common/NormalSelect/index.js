import React, { Component } from "react";
import Select, { components } from "react-select";
import ErrorComponent from "component/common/ErrorComponent";
import "./select.scss";

/**
 * Dropdown: The Common Re-usable Select-Dropdown across website.
 * @return {JSX.Element} The JSX Code for Select-Dropdown
 */

export const NormalSelect = (props) =>{
	// change select
	// handleChange = (newValue) => {
	// 	let { isMulti } = this.props;
	// 	if (!!isMulti) {
	// 		let body = {
	// 			target: {
	// 				name: this.props.name,
	// 				value: [],
	// 			},
	// 		};
	// 		if (!!newValue && newValue.length) {
	// 			newValue.forEach((array) => {
	// 				let obj = {
	// 					value: array.value,
	// 					label: array.label,
	// 				};
	// 				body.target.value.push(obj);
	// 			});
	// 		}
	// 		this.props.handleChange(body);
	// 	} else {
	// 		let body = {
	// 			target: {
	// 				name: this.props.name,
	// 				value: newValue ? newValue.value : "",
	// 				label: newValue ? newValue.label : "",
	// 			},
	// 		};

	// 		this.props.handleChange(body);
	// 	}
	// };

	
		let {
			className = "select-form-control w-100",
			onChange,
			options = [],
			value = "",
			name = "",
			placeholder = "Select",
			disabled = false,
			label = "",
			isMulti = false,
			isClearable = false,
			isSearchable = true,
			isBoxShadow = false,
			errorMessage,
			selectArrow,
			selectControlHeight,
			selectFontWeight,
			arrowVerticalAlign,
			phColor,
			fontSize,
			paddingLeft
		} = props;

		const DropdownIndicator = (props) => {
			return (
				components.DropdownIndicator && (
					<components.DropdownIndicator {...props}>
						<img
							src={selectArrow}
							className="img-fluid selectArrowIcon"
							id={arrowVerticalAlign}
							alt="icon"
						/>
					</components.DropdownIndicator>
				)
			);
		};
		const customStyles = {
			placeholder: (base) => ({
				...base,
				fontSize: fontSize,
				color: phColor,
				fontWeight: selectFontWeight,
				paddingLeft: paddingLeft
			}),
			indicatorSeparator: (base) => ({
				...base,
				display: "none",
			}),
			dropdownIndicator: (base) => ({
				...base,
				padding: 0,
				height: "24px",
				width: "24px",
				color: "#EE7500",
				display: "flex",
				justifyContent: "center",
				alignItems: "center",
			}),
			singleValue: (base) => ({
				...base,
				color: "#4C565C",
			}),
			control: (base) => ({
				...base,
				borderRadius: 4,
				border: "none",
				height: selectControlHeight,
				outline: "0 ",
				background: "#FFFFFF",
			}),
		};
		console.log(options,'aak')
		return (
			<>
				<div className={"select-section w-100"}>
					{label !== "" ? (
						<div>
							<label className="font-weight-normal mb-1">{label}</label>
						</div>
					) : null}
					{isMulti ? (
						<Select
							className={className}
							classNamePrefix="Select"
							isDisabled={disabled}
							isClearable={isClearable}
							isSearchable={isSearchable}
							name={name}
							options={options}
							onChange={onChange}
							isMulti={true}
							placeholder={placeholder}
							styles={customStyles}
							value={value}
							components={{ DropdownIndicator }}
						/>
					) : (
						<Select
							className={className}
							classNamePrefix="Select"
							isDisabled={disabled}
							isClearable={isClearable}
							isSearchable={isSearchable}
							name={name}
							options={options}
							onChange={onChange}
							isMulti={isMulti}
							placeholder={placeholder}
							styles={customStyles}
							value={
								!!options && options.length > 0
									? options.find((data) => data.value === value)
										? options.find((data) => data.value === value)
										: null
									: null
							}
							components={{ DropdownIndicator }}
						/>
					)}
				</div>
				{errorMessage && errorMessage !== "" && (
					<ErrorComponent message={errorMessage} />
				)}
			</>
		);
}