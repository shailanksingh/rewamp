import React from "react";
import "./style.scss";

export const BottomButtonMobile = ({ title, onClick }) => {
  return (
    <div className="bottom_button_submit">
      <button onClick={onClick}>{title}</button>
    </div>
  );
};
