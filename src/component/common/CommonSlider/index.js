import React, { useRef, useEffect } from "react";
import "./style.scss";
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import ArrowForward from "../../../assets/svg/HomeServiceFroward1Arrow.svg";
import ArrowBack from "../../../assets/svg/HomeServiceBackArrow.svg";
import Find from "assets/images/mobile/find.png";
import Docter from "assets/images/mobile/doctor.png";
import Shied from "assets/images/mobile/shied.png";
import Pie from "assets/images/mobile/pie.png";

export const CommonSlider = () => {
  useEffect(() => {
    window.scrollTo(0, 0);
  });

  const sliderRef = useRef(null);

  const settings = {
    slidesToShow: 3,
    slidesToScroll: 1,
    arrows: false,
    infinite: true,
    dots: true,
    infinite: true,
    speed: 500,
  };

  return (
    <div className="medical-slider">
      <Slider {...settings}>
        <div className="slider-card">
          <div className="d-flex align-items-center">
            <img src={Find} alt="Find"></img>
            <p>Find healthcare provider</p>
          </div>
        </div>
        <div className="slider-card">
          <div className="d-flex align-items-center">
            <img src={Docter} alt="Docter"></img>
            <p>Consult Live doctor</p>
          </div>
        </div>
        <div className="slider-card">
          <div className="d-flex align-items-center">
            <img src={Shied} alt="Shied"></img>
            <p>Your covrage & Benifiets</p>
          </div>
        </div>
        <div className="slider-card">
          <div className="d-flex align-items-center">
            <img src={Pie} alt="Pie"></img>
            <p>Your Detailed Usage</p>
          </div>
        </div>
      </Slider>
    </div>
  );
};
