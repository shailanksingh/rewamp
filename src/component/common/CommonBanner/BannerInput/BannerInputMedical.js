import React from "react";
import {NormalButton} from "component/common/NormalButton";
import {NormalInput} from "component/common/NormalInput";
import {NormalSelect} from "component/common/NormalSelect";
import selectArrow from "assets/svg/selectArrowIconHome.svg";
import corporate from "../../../../assets/svg/corporate.svg";
import "./style.scss";
import {useTranslation} from "react-i18next";
import * as Yup from "yup";
import {useFormik} from "formik";

const BannerInputMedical = () => {
  const {t} = useTranslation();

  const ValidationSchema = Yup.object().shape({});

  const formik = useFormik({
    initialValues: {
      corporate: '',
      idNumber: '',
      yearOfBirth: '',
      mobileNumber: ''
    },
    validationSchema: ValidationSchema,
    onSubmit: (values) => console.log(values)
  })

  const handleSubmit = () => formik.submitForm();
  return (
    <>
      <div className="inputContainer mt-2">
        <div className="d-flex flex-row pl-3">
          <div className="inputTwo">
            <span className="inputTitleTwo fs-16 fw-800">{t('banner.forInput')}</span>
            <div className="d-flex align-items-end w-100">
              <img src={corporate} alt=""/>
              <NormalSelect
                name="corporate"
                id="corporate"
                value={formik.values.corporate}
                error={formik.errors.corporate}
                onChange={formik.handleChange}
                variant="outlined"
                placeholder={t('banner.corporate')}
                className="bannerSelectInput"
                selectArrow={selectArrow}
                selectControlHeight="22px"
              />
            </div>
          </div>
          <div className="inputOne">
            <span className="inputTitleOne fs-16 fw-800">{t('banner.iqamaNumber')}</span>
            <NormalInput
              name="idNumber"
              id="idNumber"
              value={formik.values.idNumber}
              error={formik.errors.idNumber}
              onChange={formik.handleChange}
              variant="outlined"
              placeholder={t('banner.iqamaNumberPlaceholder')}
              className="inpOneField"
            />
          </div>
          <div className="inputThree2 inputTwo pl-md-2">
            <span className="inputTitleTwo fs-16 fw-800">{t('banner.yearOfBirth')}</span>
            <NormalSelect
              className="bannerSelectInput pl-lg-1 pl-md-1"
              arrowVerticalAlign="bannerArrowAlign"
              selectArrow={selectArrow}
              selectControlHeight="22px"
              selectFontWeight="400"
              placeholder={t('banner.yearOfBirthPlaceholder')}
              name="yearOfBirth"
              id="yearOfBirth"
              value={formik.values.yearOfBirth}
              error={formik.errors.yearOfBirth}
              onChange={formik.handleChange}
            />
          </div>
          <div className="inputThree">
            <span className="inputTitleThree fs-16 fw-800">{t('banner.mobileNumber')}</span>
            <NormalInput
              className="inpThreeField"
              variant="outlined"
              placeholder={t('banner.mobileNumberPlaceholder')}
              name="mobileNumber"
              id="mobileNumber"
              value={formik.values.mobileNumber}
              error={formik.errors.mobileNumber}
              onChange={formik.handleChange}/>
          </div>
          <div className="inpBtnField">
            <div>
              <NormalButton className="buyButton p-3"
                            label={t('banner.buyNow')}
                            onClick={handleSubmit}/>
            </div>
          </div>
        </div>
      </div>
      <div className="pt-3">
        <p className="inputTerms text-center fs-12 fw-400">{t('banner.paragraph')}</p>
      </div>
    </>
  );
};

export default BannerInputMedical;
