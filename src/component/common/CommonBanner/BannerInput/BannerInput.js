import React from "react";
import {NormalButton} from "component/common/NormalButton";
import {NormalInput} from "component/common/NormalInput";
import {NormalSelect} from "component/common/NormalSelect";
import individuals from "../../../../assets/svg/individuals.svg";
import selectArrow from "assets/svg/selectArrowIconHome.svg";
import "./style.scss";
import {useFormik} from "formik";
import * as Yup from 'yup';
import {useTranslation} from "react-i18next";

const BannerInput = ({isPillLayout = false}) => {

  const {t} = useTranslation();

  const ValidationSchema = Yup.object().shape({});

  const formik = useFormik({
    initialValues: {
      forInput: '',
      crNumber: '',
      crExpiryDate: '',
      mobileNumber: '',
      idNumber: '',
      yearOfBirth: ''
    },
    validationSchema: ValidationSchema,
    onSubmit: (values) => console.log(values)
  })

  const handleSubmit = () => formik.submitForm();

  return (
    <>
      {isPillLayout ? (
        <div className="inputContainer mt-2">
          <div className="d-flex flex-row pl-md-2">
            <div className="inputOne-Corporate">
              <span className="inputTitleOne fs-16 fw-800">{t('banner.crNumber')}</span>
              <NormalInput
                className="inpOneField"
                name="crNumber"
                id="crNumber"
                value={formik.values.crNumber}
                error={formik.errors.crNumber}
                onChange={formik.handleChange}
                variant="outlined"
                placeholder={t('banner.crNumberPlaceholder')}
              />
            </div>
            <div className="inputTwo-Corporate">
              <span className="inputTitleTwo fs-16 fw-800 pl-3">{t('banner.crExpiryDate')}</span>
              <div className="d-flex align-items-end w-100 pl-3">
                <NormalSelect
                  className="bannerSelectInput"
                  arrowVerticalAlign="bannerArrowAlign"
                  placeholder={t('banner.crExpiryDatePlaceholder')}
                  selectArrow={selectArrow}
                  selectControlHeight="22px"
                  selectFontWeight="400"
                  phColor="#455560"
                  fontSize="16px"
                  name="crExpiryDate"
                  id="crExpiryDate"
                  value={formik.values.crExpiryDate}
                  error={formik.errors.crExpiryDate}
                  onChange={formik.handleChange}
                />
              </div>
            </div>
            <div className="inputThree-Corporate">
              <span className="inputTitleThree fs-16 fw-800">{t('banner.mobileNumber')}</span>
              <NormalInput
                className="inpThreeField"
                variant="outlined"
                placeholder={t('banner.mobileNumberPlaceholder')}
                name="mobileNumber"
                id="mobileNumber"
                value={formik.values.mobileNumber}
                error={formik.errors.mobileNumber}
                onChange={formik.handleChange}
              />
            </div>
            <div className="inpBtnField">
              <div>
                <NormalButton
                  className="buyButton p-3"
                  label={t('banner.buyNow')}
                  onClick={handleSubmit}
                />
              </div>
            </div>
          </div>
        </div>
      ) : (
        <div className="inputContainer mt-2">
          <div className="d-flex flex-row px-3">
            <div className="inputTwo">
              <span className="inputTitleTwo fs-16 fw-800">{t('banner.forInput')}</span>
              <div className="d-flex align-items-end w-100">
                <img src={individuals} alt="For Individuals Icon"/>
                <NormalSelect
                  className="bannerSelectInput"
                  arrowVerticalAlign="bannerArrowAlign"
                  placeholder={t('banner.individuals')}
                  selectArrow={selectArrow}
                  selectControlHeight="22px"
                  selectFontWeight="400"
                  name="forInput"
                  id="forInput"
                  value={formik.values.forInput}
                  error={formik.errors.forInput}
                  onChange={formik.handleChange}
                />
              </div>
            </div>
            <div className="inputOne">
              <span className="inputTitleOne fs-16 fw-800">{t('banner.iqamaNumber')}</span>
              <NormalInput
                className="inpOneField"
                variant="outlined"
                placeholder={t('banner.iqamaNumberPlaceholder')}
                name="idNumber"
                id="idNumber"
                value={formik.values.idNumber}
                error={formik.errors.idNumber}
                onChange={formik.handleChange}
              />
            </div>
            <div className="inputTwo2 inputTwo">
              <span className="inputTitleTwo fs-16 fw-800">{t('banner.yearOfBirth')}</span>
              <NormalSelect
                className="bannerSelectInput pl-lg-1 pl-md-1"
                arrowVerticalAlign="bannerArrowAlign"
                placeholder={t('banner.yearOfBirthPlaceholder')}
                selectArrow={selectArrow}
                selectControlHeight="22px"
                selectFontWeight="400"
                name="yearOfBirth"
                id="yearOfBirth"
                value={formik.values.yearOfBirth}
                error={formik.errors.yearOfBirth}
                onChange={formik.handleChange}
              />
            </div>
            <div className="inputThree">
              <span className="inputTitleThree fs-16 fw-800">{t('banner.mobileNumber')}</span>
              <NormalInput
                className="inpThreeField"
                variant="outlined"
                placeholder={t('banner.mobileNumberPlaceholder')}
                name="mobileNumber"
                id="mobileNumber"
                value={formik.values.mobileNumber}
                error={formik.errors.mobileNumber}
                onChange={formik.handleChange}/>
            </div>
            <div className="inpBtnField">
              <div>
                <NormalButton className="buyButton p-3"
                              label={t('banner.buyNow')}
                              onClick={handleSubmit}/>
              </div>
            </div>
          </div>
        </div>
      )}
      <div className="pt-3">
        <p className="inputTerms text-center fs-12 fw-400">{t('banner.paragraph')}</p>
      </div>
    </>
  );
};

export default BannerInput;
