import React from "react";
import { NormalButton } from "component/common/NormalButton";
import { NormalInput } from "component/common/NormalInput";
import { NormalSelect } from "component/common/NormalSelect";
import corporate from "../../../../assets/svg/corporate.svg";
import selectArrow from "assets/svg/selectArrowIconHome.svg";
import "./style.scss";
import {useTranslation} from "react-i18next";
import * as Yup from "yup";
import {useFormik} from "formik";

const BannerInputTravel = () => {
	const {t} = useTranslation();

	const ValidationSchema = Yup.object().shape({});

	const formik = useFormik({
		initialValues: {
			corporate: '',
			mobileNumber: '',
			passportNumber: '',
			yearOfBirth: ''
		},
		validationSchema: ValidationSchema,
		onSubmit: (values) => console.log(values)
	})

	const handleSubmit = () => formik.submitForm();
	return (
		<>
			<div className="inputContainer mt-2">
				<div className="d-flex flex-row pl-3">
					<div className="inputTwo">
						<span className="inputTitleTwo fs-16 fw-800">{t('banner.forInput')}</span>
						<div className="d-flex align-items-end w-100">
							<img src={corporate} alt="" />
							<NormalSelect
								className="bannerSelectInput"
								arrowVerticalAlign="bannerArrowAlign"
								selectArrow={selectArrow}
								selectControlHeight="22px"
								selectFontWeight="400"
								name="corporate"
								id="corporate"
								value={formik.values.corporate}
								error={formik.errors.corporate}
								onChange={formik.handleChange}
								variant="outlined"
								placeholder={t('banner.corporate')}
							/>
						</div>
					</div>
					<div className="inputOne">
						<span className="inputTitleOne fs-16 fw-800">{t('banner.passportNumber')}</span>
						<NormalInput
							className="inpOneField"
							variant="outlined"
							placeholder={t('banner.passportNumberPlaceholder')}
							name="passportNumber"
							id="passportNumber"
							value={formik.values.passportNumber}
							error={formik.errors.passportNumber}
							onChange={formik.handleChange}
						/>
					</div>
					<div className="inputThree">
						<span className="inputTitleThree fs-16 fw-800">{t('banner.mobileNumber')}</span>
						<NormalInput
							className="inpThreeField"
							variant="outlined"
							placeholder={t('banner.mobileNumberPlaceholder')}
							name="mobileNumber"
							id="mobileNumber"
							value={formik.values.mobileNumber}
							error={formik.errors.mobileNumber}
							onChange={formik.handleChange}
						/>
					</div>
					<div className="inpBtnField">
						<div>
							<NormalButton
								className="buyButton p-3"
								label={t('banner.buyNow')}
								onClick={handleSubmit}
							/>
						</div>
					</div>
				</div>
			</div>

			<div className="pt-3">
				<p className="inputTerms text-center fs-12 fw-400">{t('banner.paragraph')}</p>
			</div>
		</>
	);
};

export default BannerInputTravel;
