import React, {useState, useEffect} from "react";
import {useDispatch, useSelector} from "react-redux";
import {updateLayout} from "action/LanguageAct";
import BannerInput from "./BannerInput/BannerInput";
import BannerInputMedical from "./BannerInput/BannerInputMedical";
import BannerInputTravel from "./BannerInput/BannerInputTravel";
import medicalIcon from "assets/svg/medicalIcon.svg";
import travelIcon from "assets/svg/travelIcon.svg";
import threeDots from "assets/svg/threeDots.svg";
import greyMotor from "assets/svg/greyMotor.svg";
import motorLandingHighlight from "assets/svg/motorLandingHighlight.svg";
import healthLandingHighlight from "assets/svg/healthLandingHighlight.svg";
import travelLandingHighlight from "assets/svg/travelLandingHighlight.svg";

import "./style.scss";
import {PillButton} from "../../../shared";
import {useTranslation} from "react-i18next";
import InputWrapper from "./InputWrapper/InputWrapper";

export const CommonBanner = ({
                               header,
                               para,
                               toggleProductCard,
                               isPillLayout = false,
                             }) => {
  const [pill, setPill] = useState(0);
  const {t} = useTranslation();

  const changeLayout = useSelector((state) => state.languageReducer.language);

  const compOne = <BannerInput isPillLayout={!changeLayout && true}/>;
  const compTwo = isPillLayout ? (
    <BannerInput isPillLayout={!changeLayout && true}/>
  ) : (
    <BannerInputMedical/>
  );
  const compThree = <BannerInputTravel/>;

  useEffect(() => {
    if (changeLayout === true) {
      document.querySelector(
        ".OnlyDesktopUseMainBannerContainerGuest"
      ).style.marginBottom = "140px";
    } else if (changeLayout === false) {
      document.querySelector(
        ".OnlyDesktopUseMainBannerContainerGuest"
      ).style.marginBottom = "90px";
    }
  }, [changeLayout]);

  const myPills = [
    {
      id: 0,
      pillName: t('headerContent.motor'),
      content: "helo",
      hidePill: true,
      pillIcon: greyMotor,
      pillIconHighlight: motorLandingHighlight,
      comp: compOne,
    },
    {
      id: 1,
      pillName: t('headerContent.health'),
      content: "ho",
      hidePill: true,
      pillIcon: medicalIcon,
      pillIconHighlight: healthLandingHighlight,
      comp: compTwo,
    },
    {
      id: 2,
      pillName: t('headerContent.travel'),
      content: "elo",
      hidePill: true,
      pillIcon: travelIcon,
      pillIconHighlight: travelLandingHighlight,
      comp: compThree,
    },
    {
      id: 3,
      pillName: t('headerContent.moreProducts'),
      content: null,
      hidePill: false,
      pillIcon: threeDots,
    },
  ];

  const corporatePills = [
    {
      id: 0,
      pillName: t('headerContent.motor'),
      content: "helo",
      hidePill: true,
      pillIcon: greyMotor,
      pillIconHighlight: motorLandingHighlight,
      comp: compOne,
    },
    {
      id: 1,
      pillName: t('headerContent.health'),
      content: "ho",
      hidePill: true,
      pillIcon: medicalIcon,
      pillIconHighlight: healthLandingHighlight,
      comp: compTwo,
    },
    {
      id: 2,
      pillName: t('headerContent.moreProducts'),
      content: null,
      hidePill: false,
      pillIcon: threeDots,
    },
  ];

  return (
    <div className="mainBannerContainer OnlyDesktopUseMainBannerContainerGuest">
      <div className="row mainBannerTop pt-5">
        <div className="col-lg-12 col-md-12 col-12">
          <h1 className="mainBannerHeading fw-800 text-center pt-5">
            {header}
          </h1>
          <p className="mainBannerPara text-center fw-400 line-height-25">
            {para}
          </p>
        </div>
        <div className="col-12">
          <div className="d-flex justify-content-center">
            <div>
              <div className="pillContainer">
                {!isPillLayout ? (
                  <div className="d-flex flex-row pt-4 pb-3">
                    {corporatePills.map((item, index) =>
                      <PillButton key={item.id + index} {...{item}} active={pill} setActive={setPill}/>
                    )}
                  </div>
                ) : (
                  <div className="d-flex flex-row pt-4 pb-3">
                    {myPills.map((item, index) =>
                      <div
                        key={item.id + index}
                        className={`${item.id === pill ? "pr-4" : " "} pillBtnContainer`}>
                        {item.hidePill &&
                          <PillButton key={item.id + index} {...{item}} active={pill} setActive={setPill}/>}
                        {item.id === 3 && <div className="d-flex flex-row p-0" onClick={toggleProductCard}>
                          <div>
                            <p className="text-uppercase moreProductPill fs-16 fw-700">
                              {item.pillName}
                            </p>
                          </div>
                          <div>
                            <img
                              src={item.pillIcon}
                              className="img-fluid threeDotImg"
                              alt="pillicon"
                            />
                          </div>
                        </div>}
                      </div>
                    )}
                  </div>
                )}
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="row">
        <div className="col-12">
          {myPills.map((item, index) => <InputWrapper key={item.id + index} {...{item, pill}} />)}
        </div>
      </div>
    </div>
  );
};
