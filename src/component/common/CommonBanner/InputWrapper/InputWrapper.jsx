import React from "react";

const InputWrapper = ({item, pill}) => {
  return item.id === pill &&
    <div className="row">
      {item.hidePill && (
        <div className="col-lg-9 col-xl-9 col-12 mx-auto d-block">
          {item.comp}
        </div>
      )}
    </div>
}
export default InputWrapper;
