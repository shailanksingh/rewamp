import React, { useState, useCallback, useEffect } from "react";
import Dropzone from "react-dropzone";
import { NormalSearch } from "../NormalSearch";
import PhoneNumberInput from "component/common/PhoneNumberInput/index";
import { NormalRadioButton } from "../NormalRadioButton";
import { NormalButton } from "../NormalButton";
import { NormalSelect } from "../NormalSelect";
import addMail from "assets/svg/Add Mail.svg";
import Profile from "assets/svg/Forms/profilenew.svg";
import File from "assets/svg/Forms/file.svg";
import Calendar from "assets/svg/Forms/calendar2new.svg";
import Calendar2 from "assets/svg/Forms/calendarnew.svg";

import KSAFlagImage from "assets/images/ksaFlag.png";
import IndiaFlagImage from "assets/images/indiaFlag.png";
import officeBag from "assets/svg/Office Bag.svg";
import exclamation from "assets/svg/Exclamation Mark.svg";
import upload from "assets/svg/uploadFileIcon.svg";
import selectDropDown from "assets/svg/complaintSelectDropdown.svg";
import closeIcon from "assets/svg/canvasClose.svg";
import { InputGroup, Form, Button } from "react-bootstrap";
import "./style.scss";
import { history } from "service/helpers";
import FormValidation from "service/helpers/FormValidation";
import ErrorComponent from "component/common/ErrorComponent";

const errorMessages = {
  userName: { required: "Reporter Name field is required" },
  region: { required: "Region field is required" },
  city: { required: "City field is required" },
  phoneNumber: { 
    required: "Phone Number field is required",
    phone: "Phone Number you entered is not valid", 
  },
  emailId: {  
      required: "Email field is required",
      emailError: "The Email you entered is not valid",
  },
  fraudDate: { required: "Fraud Incident date field is required" },
  providerName: { required: "Medical Provoder field Name is required" },
  responsibilityName: { required: "Fraud Incident Responsible Name field is required" },
  fraudIncidentDetails: { required: "Fraud Incident Details field is required" },
  files: { required: "Supporting Document field is required" },
  certify: { required: "Checkbox field is required" }
};

const MedicalFraudForm = (props) => {
  const [files, setFiles] = useState([]);

  const [showPreview, setShowPreview] = useState(false);
  const [btnclassname, setBtnclassname] = useState("complaintFileSubmitBtn");
  const [title, setTitle] = useState("");
  const [content, setContent] = useState("");
  const [formErrors, setFormErrors] = useState(null);

  useEffect(() => {
    var checkscreen = window.innerWidth < 600;
    if (checkscreen) {
      try {
        setTitle(props.data.title);
        setContent(props.data.content);
      } catch (e) {
        console.log(e);
      }

      setBtnclassname("complaintFileSubmitBtnmobile");
    } else {
      setBtnclassname("complaintFileSubmitBtn");
    }
  });

  const [formInput, setFormInput] = useState({
    userName: "",
    region: "",
    city: "",
    phoneNumber: "",
    emailId: "",
    fraudDate: "",
    providerName: "",
    responsibilityName: "",
    fraudIncidentDetails: "",
    files: "",
    certify: ""
  });

  const images = !formInput.files ? "" : formInput.files.map((file) => (
    <div
      className="dnd-container"
      sx={{
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        width: "100%",
        height: "100%",
      }}
      key={file.name}
    >
      <img
        style={{ width: "100px" }}
        className="dnd-img mx-auto d-block"
        src={file.preview}
        alt="Screen"
      />
    </div>
  ));

  const removeImageHandler = (e) => {
    // const arr = files;
    // arr.splice(item, 1);
    // setFiles([...arr]);
    e.stopPropagation();
    setFormInput({ ...formInput, files: "" });
    setShowPreview(false);
  };

  let dialingCodes = [
    {
      code: "+966",
      image: KSAFlagImage,
    },
    {
      code: "+91",
      image: IndiaFlagImage,
    },
  ];

  let [selectedCode, setSelectedCode] = useState(dialingCodes[0]);

  let [phoneNumber, setPhoneNumber] = useState("");

  //initialiize state for radio buttons
  const [radioValue, setRadioValue] = useState("Motor");
  let [filename, setFilename] = useState("File-name.pdf");

  const handleFormInputChange = (e) => {
    const { name, value } = e.target || e || {};
    setFormInput({ ...formInput, [name]: value });
  };

  const uploadfile = (e) => {
    console.log(e.target.value);
  };

  const formSubmit = () => {
    let errors = FormValidation(errorMessages, formInput, formErrors);
    setFormErrors(null);
    if (!errors) {
        history.push("/home/customerservice");
    } else {
      setFormErrors({ ...errors });
    }
  }
  const onDrop = useCallback((acceptedFiles) => {
    setFormInput({ ...formInput, 
      files: acceptedFiles.map((file) =>
        Object.assign(file, {
          preview: URL.createObjectURL(file),
        })
      ) 
    });
    if (acceptedFiles && acceptedFiles[0]) {
      const formdata = new FormData();
      formdata.append("image", acceptedFiles[0]);
    }
    setShowPreview(true);
  });
  return (
    <div className="row">
      <div className="col-12">
        <div className="d-flex justify-content-center pt-4">
          <div className="openFormContainer">
            {window.innerWidth < 600 ? (
              <>
                <p className="fs-25 fw-800 complaint-formTitle m-0">{title}</p>
                <p className="fs-16 fw-400 pt-1 complaint-formPara">
                  {content}
                </p>
              </>
            ) : (
              <>
                <p className="fs-30 fw-800 text-center complaint-formTitle m-0">
                  Report Medical Fraud
                </p>
                <p className="fs-16 fw-400 text-center complaint-formPara">
                  An intentional act by the cardholder, insured, and medical
                  services provider to obtain not owed compensation or benefits
                  to them or others through deceiving, concealing, and
                  misrepresenting information
                </p>
              </>
            )}

            <p className="fs-20 fw-800 personalTitle">Reporter Details</p>
            <div>
              <NormalSearch
                className="formInputFieldOne"
                name="userName"
                value={formInput.userName}
                placeholder="Reporter Name"
                onChange={handleFormInputChange}
                needLeftIcon={true}
                leftIcon={Profile}
                errorMessage={formErrors?.userName ? formErrors?.userName?.required : null}
              />
            </div>
            <div className="d-flex flex-row">
              <div className="col-6 pt-3 p-0">
			  <div className="complaintSelectContainer-medic complaintFormSelectInput2">
                  <NormalSelect
                    className="complaintFormSelectInput"
                    arrowVerticalAlign="complaintArrowAlign-medic"
                    placeholder="Region"
                    selectArrow={selectDropDown}
                    selectFontWeight="400"
                    phColor="#455560"
                    fontSize="16px"
					          paddingLeft="10px"
                    name="region"
                    handleChange={handleFormInputChange}
                    value={formInput.region}
                    options={[{
                      value: "region1",
                      label: "Region 1"
                    }]}
                  />
                </div>
                {formErrors?.region && <ErrorComponent message={formErrors?.region?.required} />}
              </div>

              <div className="col-6 pt-3 pr-0">
                <div className="complaintSelectContainer-medic complaintFormSelectInput2">
                  <NormalSelect
                    className="complaintFormSelectInput"
                    arrowVerticalAlign="complaintArrowAlign-medic"
                    placeholder="City"
                    selectArrow={selectDropDown}
                    selectFontWeight="400"
                    phColor="#455560"
                    fontSize="16px"
					          paddingLeft="10px"
                    name="city"
                    value={formInput.city}
                    handleChange={handleFormInputChange}
                    options={[{
                      value: "city1",
                      label: "City 1"
                    }]}
                  />
                </div>
                {formErrors?.city && <ErrorComponent message={formErrors?.city?.required} />}
              </div>
            </div>

            {/* --- */}

            <div className="py-4">
              <PhoneNumberInput
                className="formPhoneInput"
                selectInputClass="formSelectInputWidth"
                selectInputFlexType="formSelectFlexType-medical"
                dialingCodes={dialingCodes}
                selectedCode={selectedCode}
                setSelectedCode={setSelectedCode}
                value={formInput.phoneNumber}
                name="phoneNumber"
                onChange={handleFormInputChange}
                errorMessage={formErrors?.phoneNumber ? (formErrors?.phoneNumber?.phone || formErrors?.phoneNumber?.required) : null}
              />
            </div>

            <div>
              <NormalSearch
                className="formInputFieldOne"
                name="emailId"
                value={formInput.emailId}
                placeholder="ex: email@tawuniya.com.sa"
                onChange={handleFormInputChange}
                needLeftIcon={true}
                leftIcon={addMail}
                errorMessage={formErrors?.emailId ? (formErrors?.emailId?.emailError || formErrors?.emailId?.required) : null}
              />
            </div>

            <div className="borderLiningTwo mt-3"></div>
            <p className="fs-20 fw-800 m-0 pt-2 pb-3 requestTitle">
              Incident Details
            </p>

            <div className="pt-3">
              <NormalSearch
                type="email"
                className="formInputFieldOne"
                name="fraudDate"
                value={formInput.fraudDate}
                placeholder="Fraud Incident Date"
                onChange={handleFormInputChange}
                needLeftIcon={true}
                leftIcon={Calendar2}
                errorMessage={formErrors?.fraudDate ? formErrors?.fraudDate?.required : null}
              />
            </div>

            <div className="pt-3">
              <NormalSearch
                className="formInputFieldOne"
                name="providerName"
                value={formInput.providerName}
                placeholder="Medical Provider Name"
                onChange={handleFormInputChange}
                needLeftIcon={true}
                leftIcon={Calendar}
                errorMessage={formErrors?.providerName ? formErrors?.providerName?.required : null}
              />
            </div>
            <div className="pt-3">
              <NormalSearch
                className="formInputFieldOne"
                name="responsibilityName"
                value={formInput.responsibilityName}
                placeholder="Fraud Incident Responsible Name"
                onChange={handleFormInputChange}
                needLeftIcon={true}
                leftIcon={Profile}
                errorMessage={formErrors?.responsibilityName ? formErrors?.responsibilityName?.required : null}
              />
            </div>

            <div className="pt-3 pb-2">
              <p className="messageTitle fs-14 fw-700 m-0 pb-2">
                Fraud Incident Details
              </p>
              <textarea className="complaintForm-textArea"
                name="fraudIncidentDetails"
                value={formInput?.fraudIncidentDetails}
                onChange={handleFormInputChange}
              >
                Type here...
              </textarea>
              {formErrors?.fraudIncidentDetails && <ErrorComponent message={formErrors?.fraudIncidentDetails?.required} />}
            </div>
            <div className="d-flex flex-row">
              <div>
                <img src={exclamation} className="img-fluid pr-2" alt="icon" />
              </div>
              <div>
                <p className="fs-12 fw-400 formConditions m-0">
                  Be detailed as possible when submitting a request in order for
                  us to help you more effectively. The more detailed the
                  information you provide, the faster we will be able to resolve
                  your issue.
                </p>
                <p className="fs-12 fw-400 formConditions m-0">
                  Below are some tips:
                </p>
              </div>
            </div>
            <ul className="fs-12 fw-400 formConditionsList pl-5 mb-3">
              <li>
                Explain step-by-step how to reproduce the scenario or the
                problem that you are describing.
              </li>
              <li>If you think a document would help, please include one.</li>
              <li>
                State when the problem started and what changes were made
                immediately beforehand.
              </li>
            </ul>
            <div className="borderLiningThree mb-3"></div>
            <p className="messageTitle fs-14 fw-700 m-0 pb-2">
              Supporting Documents
            </p>

            <Dropzone onDrop={onDrop}>
              {({ getRootProps, getInputProps }) => (
                <div
                  {...getRootProps({
                    className: "dropzone",
                    onDrop: (event) => event.preventDefault(),
                  })}
                >
                  <div
                    style={{
                      display: "flex",
                      justifyContent: "center",
                      alignItems: "center",
                      flexDirection: "column",
                    }}
                    className="image-upload-wrap"
                  >
                    <input {...getInputProps()} />
                    {!showPreview ? (
                      <div className="drag-text">
                        <div>
                          <img
                            src={upload}
                            className="img-fluid mx-auto d-block pb-3"
                            alt="uploadicon"
                          />
                          <div className="d-flex justify-content-center">
                            <div>
                              <p className="uploadTitle fs-14 fw-400 m-0 pb-3">
                                Upload files by drag and drop or{" "}
                                <span className="uploadLink">
                                  click to upload
                                </span>
                                .
                              </p>
                            </div>
                          </div>
                          <p className="fs-12 fw-400 allowedFormats text-center">
                            Allowed files: JPEG, PNG, DOC, DOCX, PDF Maximum
                            Size: 10MB
                          </p>
                        </div>
                      </div>
                    ) : (
                      <>
                        <div className="drag-text">
                          <div>
                            <div>{images}</div>
                            {showPreview ? (
                              <img
                                src={closeIcon}
                                className="img-fluid removeImgBtn"
                                onClick={(e) => removeImageHandler(e)}
                                alt="icon"
                              />
                            ) : (
                              ""
                            )}
                            <div className="d-flex justify-content-center">
                              <div>
                                <p className="uploadTitle fs-14 fw-400 m-0 pb-3">
                                  Upload files by drag and drop or{" "}
                                  <span className="uploadLink">
                                    click to upload
                                  </span>
                                  .
                                </p>
                              </div>
                            </div>
                            <p className="fs-12 fw-400 allowedFormats text-center">
                              Allowed files: JPEG, PNG, DOC, DOCX, PDF Maximum
                              Size: 10MB
                            </p>
                          </div>
                        </div>
                      </>
                    )}
                  </div>
                </div>
              )}
            </Dropzone>
            {formErrors?.files && <ErrorComponent message={formErrors?.files?.required} />}

            {/* <div className="complaintSelectContainer complaintFormSelectInput2 pb-2">
              <div className="d-flex">
                <p className="fw-800 fs-14 pt-3 pr-2">{filename}</p>

                <button label="Browse" className={btnclassname}>
                  <input
                    className="fileuploadInput"
                    onChange={uploadfile}
                    type="file"
                    name="file1"
                  />
                  <img className="pr-2 " alt="file" src={File} />
                  Browse
                </button>
              </div>
            </div>
            <div className="d-flex flex-row">
              <div>
                <img
                  src={exclamation}
                  className="img-fluid pr-2 pt-2"
                  alt="icon"
                />
              </div>
              <div>
                <p className="fs-12 pt-2 fw-400 formConditions m-0">
                  Allowed files: JPEG, PNG, DOC, DOCX, PDF Maximum Size: 10MB
                </p>
              </div>
            </div> */}

            {/* <hr /> */}

            <div className="d-flex flex-row pt-4">
              <div>
                <input type="checkbox" className="pr-2 pt-2" 
                  name="certify"
                  checked={formInput.certify}
                  onChange={({ target: { name, checked } }) => {
                    handleFormInputChange({ target: { name, value: checked } });
                  }}
                />
              </div>
              <div>
                <p className="fs-12 pt-1 fw-400 formConditions pl-2 m-0">
                  I hereby certify that all information mentioned above about
                  the fraud / misuse of property and casualty insurance is
                  correct to the best of my knowledge and belief.
                </p>
              </div>
            </div>
            {formErrors?.certify && <ErrorComponent message={formErrors?.certify?.required} />}

            <div className="borderLiningFour my-4"></div>
            <NormalButton
              label="Submit Report"
              className="complaintFormSubmitBtn p-4"
              onClick={() => formSubmit()}
            />
            
          </div>
        </div>
      </div>
    </div>
  );
};

export default MedicalFraudForm;
