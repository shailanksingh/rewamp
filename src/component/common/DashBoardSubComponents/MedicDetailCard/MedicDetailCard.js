import React from "react";
import glucoseMeter from "assets/svg/glucose-meter (Traced).svg";
import datePicker from "assets/svg/primaryDatePicker.svg";
import cardEdit from "assets/svg/cardEditIcon.svg";
import cardDelete from "assets/svg/cardDeleteIcon.svg";
import "./style.scss";

export const MedicDetailCard = ({ medicDetailData, onClick }) => {
  return (
    <div className="row medicDetail-mainContainer m-0 py-4">
      {medicDetailData.map((item, i) => {
        return (
          <div className="col-12 pb-3" key={i}>
            <div className="medicDetail-container p-3">
              <div className="d-flex justify-content-between">
                <div>
                  <div className="d-flex flex-row">
                    <div className="pr-3">
                      <img
                        src={glucoseMeter}
                        className="img-fluid"
                        alt="icon"
                      />
                    </div>
                    <div>
                      <p className="m-0 disease-type fs-14 fw-800 pb-1">
                        {item.disease}
                      </p>
                      <p className="m-0 medicine-name fs-12 fw-400">
                        {item.medicine}
                      </p>
                      <div className="d-flex flex-row pt-2">
                        <div className="pr-3">
                          <p className="m-0 quantity-level fs-11 fw-400 pt-1">
                            {item.quantity}
                          </p>
                        </div>
                        <div>
                          <div className="date-time-container">
                            <img
                              src={datePicker}
                              className="img-fluid"
                              alt="icon"
                            />
                            <span className="pl-2 timeline-text fs-11 fw-400">
                              {item.timeLine}
                            </span>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div>
                  <div className="d-flex flex-row">
                    <div className="pr-2">
                      <img src={cardEdit} className="img-fluid" alt="icon" />
                    </div>
                    <div>
                      <img src={cardDelete} className="img-fluid" onClick={onClick} alt="icon" />
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        );
      })}
    </div>
  );
};
