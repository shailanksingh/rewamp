import { NormalButton } from "component/common/NormalButton";
import React from "react";
import "./style.scss";

export const DashBoardNavTabs = ({
  tab,
  setTab,
  children,
  labelOne,
  labelTwo,
}) => {
  return (
    <React.Fragment>
      <div className="d-flex flex-row dashBoardNavtabContainer">
        <div className="col-6 px-1">
          <NormalButton
            label={labelOne}
            className={tab === 0 ? "highlight-pillTabOne" : "normal-pillTabOne"}
            onClick={() => setTab(0)}
          />
        </div>
        <div className="col-6 px-1">
          <NormalButton
            label={labelTwo}
            className={tab === 1 ? "highlight-pillTabTwo" : "normal-pillTabTwo"}
            onClick={() => setTab(1)}
          />
        </div>
      </div>
      <div className="row">
        <div className="col-12">{children}</div>
      </div>
    </React.Fragment>
  );
};
