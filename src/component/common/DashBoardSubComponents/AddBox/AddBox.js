import React from "react";
import minusCounter from "assets/svg/minusCounter.svg";
import plusCounter from "assets/svg/plusCounter.svg";
import "./style.scss";

export const AddBox = ({ add, minus, boxValue }) => {
  return (
    <div className="addBoxContainer">
      <div className="d-flex justify-content-between">
        <div>
          <p className="m-0 fs-12 fw-400 boxNoTitle">Number of Boxes</p>
          <span className="fs-16 fw-700 boxNoItems">{boxValue}</span>
        </div>
        <div>
          <div className="d-flex flex-row">
            <div>
              <img
                src={minusCounter}
                className="img-fluid pr-2"
                alt="icon"
                onClick={minus}
              />
            </div>
            <div>
              <img
                src={plusCounter}
                className="img-fluid"
                alt="icon"
                onClick={add}
              />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};
