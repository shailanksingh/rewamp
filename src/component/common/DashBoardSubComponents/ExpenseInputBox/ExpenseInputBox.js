import React from "react";
import { NormalSearch } from "component/common/NormalSearch";
import "./style.scss";

export const ExpenseInputBox = ({
  name,
  value,
  placeholder,
  onChange,
  expenseLabel,
  isExpenseInputLayout = false,
}) => {
  return (
    <div className="row expenseInputContainer">
      {isExpenseInputLayout ? (
        <React.Fragment>
          <div className="col-11 pr-0">
            <NormalSearch
              className="expenseInputTwo"
              name={name}
              value={value}
              placeholder={placeholder}
              onChange={onChange}
            />
          </div>
          <div className="col-1 pl-0">
            <div className="subExpenseInputLayerTwo">
              <p className="fs-16 fw-400 text-uppercase subExpenseBoxTwoText m-0">
                {expenseLabel}
              </p>
            </div>
          </div>
        </React.Fragment>
      ) : (
        <React.Fragment>
          <div className="col-1 pr-0">
            <div className="subExpenseInputLayerOne">
              <p className="fs-16 fw-400 text-uppercase subExpenseBoxOneText m-0">
                {expenseLabel}
              </p>
            </div>
          </div>
          <div className="col-11 pl-0">
            <NormalSearch
              className="expenseInputOne"
              name={name}
              value={value}
              placeholder={placeholder}
              onChange={onChange}
            />
          </div>
        </React.Fragment>
      )}
    </div>
  );
};
