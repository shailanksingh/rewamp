import React, { useState } from "react";

import "./style.scss";

export const DashboardSelector = ({ toggle, setToggle, division }) => {
  // const [toggle, setToggle] = useState([
  //   {
  //     id: 0,
  //     name: "Diabetes",
  //     status: false,
  //     highlightIcon: highlightGlucose,
  //     normalIcon: normalGlucose,
  //   },
  //   {
  //     id: 1,
  //     name: "Hypertension",
  //     status: false,
  //     highlightIcon: highlightGlucose,
  //     normalIcon: normalGlucose,
  //   },
  // ]);

  const handleState = (i) => {
    let state = [...toggle];
    if (state[i].status === true) {
      state[i].status = false;
    } else state[i].status = true;
    setToggle([...state]);
  };

  return (
    <div className="row">
      {toggle.map((item, i) => {
        return (
          <div className={`${division} pb-2`} key={i}>
            <div
              className={`${
                item.status
                  ? "highlight-selector-card cursor-pointer"
                  : "selector-card cursor-pointer"
              }`}
              onClick={() => handleState(i)}
            >
              <img
                src={item.status ? item.highlightIcon : item.normalIcon}
                className="img-fluid mx-auto d-block"
                alt="icon"
              />
              <p
                className={`${
                  item.status ? "highLight-diseaseName" : "normal-diseaseName"
                } fs-18 fw-800 text-center pt-2 m-0`}
              >
                {item.name}
              </p>
              <p
                className={`${
                  item.status ? "highLight-diseasePara" : "normal-diseasePara"
                } fs-10 fw-400 text-center pt-2 m-0`}
              >
                {item.discription}
              </p>
            </div>
          </div>
        );
      })}
    </div>
  );
};
