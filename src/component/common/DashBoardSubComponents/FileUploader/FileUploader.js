import React from "react";
import Dropzone from "react-dropzone";
import { NormalButton } from "component/common/NormalButton";
import uploader from "assets/svg/browseFileUploader.svg";
import "./style.scss";

export const FileUploader = ({ onDrop, files }) => {
  return (
    <React.Fragment>
      <Dropzone onDrop={onDrop}>
        {({ getRootProps, getInputProps }) => (
          <div
            {...getRootProps({
              className: "dropzone",
              onDrop: (event) => event.preventDefault(),
            })}
          >
            <div className="image-new-upload-wrap">
              <div className="d-flex justify-content-between">
                <div>
                  {files.length === 0 && (
                    <p className="fs-16 fw-700 file-Titlle m-0">
                      file-name.pdf
                    </p>
                  )}
                  {files.map((item, i) => {
                    return (
                      <p className="fs-16 fw-700 file-Titlle m-0" key={i}>
                        {item.path}
                      </p>
                    );
                  })}
                </div>
                <div className="browseUploadContainer">
                  <NormalButton
                    label="Browse"
                    className="browseFileUploaderBtn"
                    newSrc={uploader}
                    adjustNewIcon="pr-2"
                    needNewBtnIcon={true}
                  />
                </div>
              </div>
            </div>
          </div>
        )}
      </Dropzone>
    </React.Fragment>
  );
};
