import React from "react";
import appStore from "assets/images/appleStore.png";
import playStore from "assets/images/playStore.png";
import appGal from "assets/images/appGallery.png";
import "./style.scss";

export const AuthFooter = () => {
  return (
    <div className="row">
      <div className="col-lg-12 col-12 authFooterContainer px-5">
        <div className="d-flex justify-content-between">
          <div className="leftAuthContent">
            <p className="fs-14 fw-400 authCopyrightText m-0">
              Copyright © Tawuniya 2022, all rights reserved
            </p>
            <div className="d-flex flex-row">
              <div>
                <p className="fs-12 fw-400 authTerms">Terms and Conditions</p>
              </div>
              <div className="pl-lg-2 pl-2">
                <p className="fs-12 fw-400 authTerms">Privacy Policy</p>
              </div>
              <div className="pl-lg-2 pl-2">
                <p className="fs-12 fw-400 authTerms">Cookie Policy</p>
              </div>
            </div>
          </div>
          <div>
            <div className="d-flex flex-row rightAuthContent">
              <div>
                <img src={appStore} className="img-fluid authFooterBtnOne" alt="iconBtns" />
              </div>
              <div>
                <img src={playStore} className="img-fluid authFooterBtnTwo" alt="iconBtns" />
              </div>
              <div>
                <img src={appGal} className="img-fluid authFooterBtnThree" alt="iconBtns" />
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};
