import React, { useState, useCallback, useEffect } from "react";
import Dropzone from "react-dropzone";
import { NormalSearch } from "../NormalSearch";
import PhoneNumberInput from "component/common/PhoneNumberInput/index";
import { NormalRadioButton } from "../NormalRadioButton";
import { NormalButton } from "../NormalButton";
import { NormalSelect } from "../NormalSelect";
import addMail from "assets/svg/Add Mail.svg";
import Profile from "assets/svg/Forms/profilenew.svg";
import File from "assets/svg/Forms/file.svg";
import Calendar from "assets/svg/Forms/calendar2new.svg";
import Calendar2 from "assets/svg/Forms/calendarnew.svg";

import KSAFlagImage from "assets/images/ksaFlag.png";
import IndiaFlagImage from "assets/images/indiaFlag.png";
import officeBag from "assets/svg/Office Bag.svg";
import exclamation from "assets/svg/Exclamation Mark.svg";
import upload from "assets/svg/uploadFileIcon.svg";
import selectDropDown from "assets/svg/complaintSelectDropdown.svg";
import closeIcon from "assets/svg/canvasClose.svg";
import { InputGroup, Form, Button } from "react-bootstrap";
import "./style.scss";
import { history } from "service/helpers";

const ReportViolationMobileForm = (props) => {
	const [files, setFiles] = useState([]);

	const [showPreview, setShowPreview] = useState(false);
	const [btnclassname, setBtnclassname] = useState("complaintFileSubmitBtn");

	useEffect(() => {
		var checkscreen = window.innerWidth < 600;
		if (checkscreen) {
			setBtnclassname("complaintFileSubmitBtnmobile");
		} else {
			setBtnclassname("complaintFileSubmitBtn");
		}
	});

	const onDrop = useCallback((acceptedFiles) => {
		setFiles(
			acceptedFiles.map((file) =>
				Object.assign(file, {
					preview: URL.createObjectURL(file),
				})
			)
		);
		if (acceptedFiles && acceptedFiles[0]) {
			const formdata = new FormData();
			formdata.append("image", acceptedFiles[0]);
		}
		setShowPreview(true);
	});

	const images = files.map((file) => (
		<div
			className="dnd-container"
			sx={{
				display: "flex",
				justifyContent: "center",
				alignItems: "center",
				width: "100%",
				height: "100%",
			}}
			key={file.name}
		>
			<img
				style={{ width: "100px" }}
				className="dnd-img mx-auto d-block"
				src={file.preview}
				alt="Screen"
			/>
		</div>
	));

	const removeImageHandler = (e) => {
		// const arr = files;
		// arr.splice(item, 1);
		// setFiles([...arr]);
		e.stopPropagation();
		setFiles([]);
		setShowPreview(false);
	};

	const [formInput, setFormInput] = useState({
		userId: "",
		policyId: "",
		message: "",
	});

	let dialingCodes = [
		{
			code: "+91",
			image: IndiaFlagImage,
		},
		{
			code: "+966",
			image: KSAFlagImage,
		},
	];

	let [selectedCode, setSelectedCode] = useState(dialingCodes[0]);

	let [phoneNumber, setPhoneNumber] = useState("");

	//initialiize state for radio buttons
	const [radioValue, setRadioValue] = useState("Motor");
	let [filename, setFilename] = useState("File-name.pdf");

	const handleFormInputChange = (e) => {
		const { name, value } = e.target || e || {};
		setFormInput({ ...formInput, [name]: value });
	};

	const uploadfile = (e) => {
		console.log(e.target.value);
	};

	return (
		<div className="row">
			<div className="col-12">
				<div className="d-flex justify-content-center pt-4">
					<div className="openFormContainer">
						<p className="fs-25 fw-800 complaint-formTitle m-0">
							Report Violation
						</p>
						<p className="fs-16 fw-400 pt-1 complaint-formPara">
							Please fill out the form below to submit your request and we will
							start our review on it immediately.
						</p>

						<div>
							<NormalSearch
								className="formInputFieldOne"
								name="name"
								value={formInput.userId}
								placeholder="Enter your  name"
								onChange={handleFormInputChange}
								needLeftIcon={true}
								leftIcon={Profile}
							/>
						</div>

						{/* --- */}

						<div className="pt-3">
							<NormalSearch
								className="formInputFieldOne"
								name="userId"
								value={formInput.userId}
								placeholder="ex: email@tawuniya.com.sa"
								onChange={handleFormInputChange}
								needLeftIcon={true}
								leftIcon={addMail}
							/>
						</div>

						<div className="py-4">
							<PhoneNumberInput
								className="formPhoneInput"
								selectInputClass="formSelectInputWidth"
								selectInputFlexType="formSelectFlexType"
								dialingCodes={dialingCodes}
								selectedCode={selectedCode}
								setSelectedCode={setSelectedCode}
								value={phoneNumber}
								name="phoneNumber"
								onChange={({ target: { value } }) => setPhoneNumber(value)}
							/>
						</div>

						<div className="borderLiningTwo mb-3"></div>

						<div>
							<NormalSearch
								className="formInputFieldOne_new"
								name="name"
								value={formInput.userId}
								placeholder="Subject"
							/>
						</div>

						<p className="fs-16 pl-2 fw-800 m-0 pt-2 pb-1 requestTitle">
							Report Details
						</p>

						<div className="pt-3 pb-2">
							<textarea className="complaintForm-textArea">
								Type here...
							</textarea>
						</div>
						<div className="d-flex flex-row">
							<div>
								<img src={exclamation} className="img-fluid pr-2" alt="icon" />
							</div>
							<div>
								<p className="fs-12 fw-400 formConditions m-0">
									Be detailed as possible when submitting a request in order for
									us to help you more effectively. The more detailed the
									information you provide, the faster we will be able to resolve
									your issue.
								</p>
								<p className="fs-12 fw-400 formConditions m-0">
									Below are some tips:
								</p>
							</div>
						</div>
						<ul className="fs-12 fw-400 formConditionsList pl-5 mb-3">
							<li>
								Explain step-by-step how to reproduce the scenario or the
								problem that you are describing.
							</li>
							<li>If you think a document would help, please include one.</li>
							<li>
								State when the problem started and what changes were made
								immediately beforehand.
							</li>
						</ul>
						<div className="borderLiningThree mb-3"></div>
						<p className="messageTitle fs-14 fw-700 m-0 pb-2">
							Supporting Documents
						</p>

						<Dropzone onDrop={onDrop}>
							{({ getRootProps, getInputProps }) => (
								<div
									{...getRootProps({
										className: "dropzone",
										onDrop: (event) => event.preventDefault(),
									})}
								>
									<div
										style={{
											display: "flex",
											justifyContent: "center",
											alignItems: "center",
											flexDirection: "column",
										}}
										className="image-upload-wrap"
									>
										<input {...getInputProps()} />
										{!showPreview ? (
											<div className="drag-text">
												<div>
													<img
														src={upload}
														className="img-fluid mx-auto d-block pb-3"
														alt="uploadicon"
													/>
													<div className="d-flex justify-content-center">
														<div>
															<p className="uploadTitle fs-14 fw-400 m-0 pb-3">
																Upload files by drag and drop or{" "}
																<span className="uploadLink">
																	click to upload
																</span>
																.
															</p>
														</div>
													</div>
													<p className="fs-12 fw-400 allowedFormats text-center">
														Allowed files: JPEG, PNG, DOC, DOCX, PDF Maximum
														Size: 10MB
													</p>
												</div>
											</div>
										) : (
											<>
												<div className="drag-text">
													<div>
														<div>{images}</div>
														{showPreview ? (
															<img
																src={closeIcon}
																className="img-fluid removeImgBtn"
																onClick={(e) => removeImageHandler(e)}
																alt="icon"
															/>
														) : (
															""
														)}
														<div className="d-flex justify-content-center">
															<div>
																<p className="uploadTitle fs-14 fw-400 m-0 pb-3">
																	Upload files by drag and drop or{" "}
																	<span className="uploadLink">
																		click to upload
																	</span>
																	.
																</p>
															</div>
														</div>
														<p className="fs-12 fw-400 allowedFormats text-center">
															Allowed files: JPEG, PNG, DOC, DOCX, PDF Maximum
															Size: 10MB
														</p>
													</div>
												</div>
											</>
										)}
									</div>
								</div>
							)}
						</Dropzone>

						{/* <div className="complaintSelectContainer complaintFormSelectInput2 pb-2">
              <div className="d-flex">
                <p className="fw-800 fs-14 pt-3 pr-2">{filename}</p>

                <button label="Browse" className={btnclassname}>
                  <input
                    className="fileuploadInput"
                    onChange={uploadfile}
                    type="file"
                    name="file1"
                  />
                  <img className="pr-2 " alt="file" src={File} />
                  Browse
                </button>
              </div>
            </div>
            <div className="d-flex flex-row">
              <div>
                <img
                  src={exclamation}
                  className="img-fluid pr-2 pt-2"
                  alt="icon"
                />
              </div>
              <div>
                <p className="fs-12 pt-2 fw-400 formConditions m-0">
                  Allowed files: JPEG, PNG, DOC, DOCX, PDF Maximum Size: 10MB
                </p>
              </div>
            </div> */}

						<hr />

						<div className="d-flex flex-row">
							<div>
								<input type="checkbox" className="pr-2 pt-2" />
							</div>
							<div>
								<p className="fs-12 pt-1 fw-400 formConditions pl-2 m-0">
									I hereby declare that all the above information is true and
									accurate.
								</p>
							</div>
						</div>

						<div className="borderLiningFour my-4"></div>
						<NormalButton
							label="Submit Report"
							className="complaintFormSubmitBtn p-4"
							onClick={() => history.push("/home/customerservice")}
						/>
						<p className="fs-16 fw-400 pt-4 complaint-formPara">
							Tawuniya undertakes to maintain the confidentiality of the
							information of the person who is reporting the fraud in the
							medical insurance.
						</p>
					</div>
				</div>
			</div>
		</div>
	);
};

export default ReportViolationMobileForm;
