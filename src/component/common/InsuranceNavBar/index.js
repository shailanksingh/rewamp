import React, { useEffect, useState } from "react";
import { history } from "service/helpers";
import { UseHover } from "./UseHover";
import { InsuranceSubNavBar } from "./InsuranceSubNavBar";
import { NormalButton } from "../NormalButton";
import { NormalSearch } from "../NormalSearch";
import logo from "assets/svg/tawuniyaLogo.svg";
import hamburgerIcon from "assets/svg/hamburgerIcon.svg";
import hamburgerMobile from "assets/svg/hamburgerIconMobile.svg";
import notifyIcon from "assets/svg/notifyIcon.svg";
import modeIcon from "assets/svg/modeIcon.svg";
import searchIcon from "assets/svg/headerSearchLight.svg";
import languageIcon from "assets/svg/languageIcon.svg";
import phoneIcon from "assets/svg/phoneCircleIcon.svg";
import mailIcon from "assets/svg/mail icon.svg";
import chatIcon from "assets/svg/chat icon.svg";
import { NavLink } from "react-router-dom";
import "./style.scss";

export const InsuranceNavBar = ({
	handleToggler,
	toggleLanguageCard,
	toggleSupportCard,
	toggleEmergencyCard,
	toggleGetStartedCard,
	navContent,
	isSubNav = false,
}) => {
	const [press, setPress] = useState(false);

	const [search, setSearch] = useState("");

	const [hover, isHover] = UseHover();

	const [swap, setSwap] = useState(null);

	const changeImg = [notifyIcon, phoneIcon, mailIcon, chatIcon];

	const [transition, setTransition] = useState(0);

	useEffect(() => {
		setSwap(changeImg[transition]);
		console.log("prabhat");
	}, [transition]);

	useEffect(() => {
		const timer = setTimeout(() => {
			if (transition + 1 === changeImg.length) {
				setTransition(0);
			} else {
				setTransition((item) => item + 1);
			}
		}, 1500);
		return () => clearTimeout(timer);
	}, []);

	const handleInputSearch = () => {
		setPress(false);
	};

	return (
		<React.Fragment>
			{isSubNav && (
				<div className="navbarInsurance" id="navbarDesktop">
					<div className="d-flex justify-content-between w-100 h-100">
						<div className="navInsuranceTextContent">
							<NavLink to="/home">
								<img
									src={logo}
									className="img-fluid cursor-pointer tawuniyaLogo"
									alt="tawuniyaLogo"
								/>
							</NavLink>
						</div>
						<div className="navInsuranceTextContent pl-4">
							<img
								src={hamburgerIcon}
								className="img-fluid hamburgerIcon"
								onClick={handleToggler}
								alt="homechefLogo"
							/>
						</div>
						{navContent.map((item, index) => {
							return (
								<React.Fragment>
									<div className="navInsuranceTextContent pl-4" key={index}>
										<p className="navLink-Text fs-16 fw-400">{item.navText}</p>
									</div>
								</React.Fragment>
							);
						})}
						<div className="d-flex justify-content-end w-100 align-items-center">
							<div
								className="d-flex justify-content-center align-items-center"
								id="alignRightNavContents"
							>
								<div className="pr-1">
									<img
										src={languageIcon}
										className="img-fluid languageBtnIcon"
										onClick={toggleLanguageCard}
										alt="languageicon"
									/>
								</div>
								<div className="pr-lg-1" ref={hover}>
									{isHover ? (
										<NormalSearch
											className="headerSearch"
											name="search"
											value={search}
											placeholder="What you're looking for?"
											onChange={(e) => setSearch(e.target.value)}
											handleInputSearch={handleInputSearch}
											needRightIcon={true}
										/>
									) : (
										<img
											src={searchIcon}
											className="img-fluid searchBtnIcon"
											alt="searchicon"
										/>
									)}
								</div>
								<div className="pr-lg-1">
									<img
										src={modeIcon}
										className="img-fluid notifyBtnIcon"
										onClick={toggleSupportCard}
										alt="notifyicon"
									/>
								</div>
								<div className="pr-lg-2">
									<img
										src={swap}
										className="img-fluid notifyBtnIcon"
										onClick={toggleSupportCard}
										alt="notifyicon"
									/>
								</div>
								<div className="pr-lg-1">
									<NormalButton
										label="Emergency?"
										className="getStartBtn"
										onClick={toggleEmergencyCard}
									/>
								</div>
								<React.Fragment>
									<div className="pl-lg-3">
										<NormalButton
											label="Get Started"
											className="emergencyBtn"
											onClick={toggleGetStartedCard}
										/>
									</div>
								</React.Fragment>
							</div>
						</div>
					</div>
				</div>
			)}
			<InsuranceSubNavBar />
		</React.Fragment>
	);
};
