import React, { useEffect, useState } from "react";
import { history } from "service/helpers";
import PANDCBANNER1 from "assets/svg/TawuniyaServicesIcons/PANDCBANNER1.svg";
import PANDCBANNER2 from "assets/svg/TawuniyaServicesIcons/PANDCBANNER2.svg";
import PANDCBANNER3 from "assets/svg/TawuniyaServicesIcons/PANDCBANNER3.svg";
import car from "assets/svg/motorInsuranceCar.svg";
import { NormalButton } from "../NormalButton";
import "./style.scss";
import { Data } from "@react-google-maps/api";

const homeInsuranceSubnavBarData = [
	{
		id: 0,
		icon: PANDCBANNER1,
		title: "property & casualty",
		subTitile: "Insurance",
		rightNavSubContents: [
			{
				id: 1,
				content: "Overview",
			},
			{
				id: 2,
				content: "Coverage",
			},
			{
				id: 3,
				content: "Advantages",
			},
			{
				id: 4,
				content: "FAQs",
			},
		],
	},
];

const internationalTravelSubnavBarData = [
	{
		id: 0,
		icon: PANDCBANNER2,
		title: "property & casualty",
		subTitile: "Insurance",
		rightNavSubContents: [
			{
				id: 1,
				content: "Overview",
			},
			{
				id: 2,
				content: "Plans",
			},
			{
				id: 3,
				content: "Advantages",
			},
			{
				id: 4,
				content: "Terms",
			},
			{
				id: 5,
				content: "FAQs",
			},
		],
	},
];

const medicalMalpracticeSubnavBarData = [
	{
		id: 0,
		icon: PANDCBANNER3,
		title: "property & casualty",
		subTitile: "Insurance",
		rightNavSubContents: [
			{
				id: 1,
				content: "Overview",
			},
			{
				id: 2,
				content: "Eligible Persons",
			},
			{
				id: 3,
				content: "Advantages",
			},
			{
				id: 4,
				content: "Benefits",
			},
			{
				id: 5,
				content: "FAQs",
			},
		],
	},
];

export const InsuranceSubNavBar = () => {
	const [subNavbarData, setSubNavbarData] = useState(
		homeInsuranceSubnavBarData
	);

	useEffect(() => {
		if (history.location.pathname === "/products/individuals/homeinsurance") {
			setSubNavbarData(homeInsuranceSubnavBarData);
		} else if (
			history.location.pathname === "/products/individuals/internationaltravel"
		) {
			setSubNavbarData(internationalTravelSubnavBarData);
		} else if (
			history.location.pathname === "/products/individuals/medicalmalpractice"
		) {
			setSubNavbarData(medicalMalpracticeSubnavBarData);
		}
	});
	return (
		<React.Fragment>
			<div className="navLinerContainer pt-2">
				{/* <div className="navLiner"></div> */}
			</div>
			{subNavbarData.map((item, id) => {
				return (
					<div
						key={id}
						className="d-flex justify-content-between navbarSubInsurance"
					>
						<div>
							<img
								src={car}
								className="img-fluid pr-1 subInsuranceLogo"
								alt="icon"
							/>{" "}
							<span className="fs-16 fw-700 insuranceNavTitle text-uppercase">
								{item.title} <span className="fw-400">{item.subTitile}</span>{" "}
							</span>
						</div>
						<div>
							<div className="d-flex flex-row">
								{item.rightNavSubContents.map((data, id) => {
									return (
										<div key={id} className="pr-3">
											<p className="fs-16 fw-400 rightNavSubContents">
												{data.content}
											</p>
										</div>
									);
								})}

								<div className="pl-3">
									<NormalButton label="Buy Now" className="navBuyBtn px-4" />
								</div>
							</div>
						</div>
					</div>
				);
			})}
		</React.Fragment>
	);
};
