import React from "react";
import "./style.scss";

function AbtUsBannerCard({ cardIcon, cardTitle, cardPara, cardTitleColor }) {
	return (
		<div className="AbtUsBannerCard">
			{cardIcon && <img src={cardIcon} alt="" />}
			<h5 style={{ color: cardTitleColor }}>{cardTitle}</h5>
			<p>{cardPara}</p>
		</div>
	);
}

export default AbtUsBannerCard;
