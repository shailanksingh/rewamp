import React, { useRef } from "react";
import Slider from "react-slick";
import TawuniyaApp from "./Components/TawuniyaApp";
import TawuniyaDrive from "./Components/TawuniyaDrive";
import TawuniyaDriveFeatures from "./Components/TawuniyaDriveFeatures";
import ArrowForward from "../../../assets/svg/HomeServiceFroward1Arrow.svg";
import ArrowBack from "../../../assets/svg/HomeServiceBackArrow.svg";
import "../../HomePage/LandingComponent/style.scss";
import "./style.scss";
import { makeStyles } from "@material-ui/core";
const useStyles = makeStyles((theme) => ({
  dots: {
    bottom: -45,
    "& li.slick-active button::before": {
      color: "#EE7500",
    },
    "& li": {
      width: "12px",
      "& button::before": {
        fontSize: theme.typography.pxToRem(9),
        color: "#4C565C",
      },
    },
  },
}));

export const TawuniyaAppSlider = () => {
  const classes = useStyles();
  const sliderRef = useRef(null);

  const settings = {
    dots: true,
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
    dotsClass: `slick-dots ${classes.dots}`,
  };

  return (
    <div className="TawuniyaAppSliderContainer" style={{ height: "100vh" }}>
      <Slider style={{ height: "600px" }} ref={sliderRef} {...settings}>
        <div style={{ height: "600px" }}>
          <TawuniyaApp />
        </div>
        <div style={{ height: "600px" }}>
          <TawuniyaDrive />
        </div>
        <div style={{ height: "600px" }}>
          <TawuniyaDriveFeatures />
        </div>
      </Slider>

      <div className="arrowDotContainer">
        <div
          className="arrowContainer arrowContainerPrev"
          onClick={() => sliderRef.current.slickPrev()}
        >
          <img src={ArrowBack} alt="..." />
        </div>
        <div className="dotContainer">
        </div>

        <div
          className="arrowContainer arrowContainerNext"
          onClick={() => sliderRef.current.slickNext()}
        >
          <img src={ArrowForward} alt="..." />
        </div>
      </div>
    </div>
  );
};
