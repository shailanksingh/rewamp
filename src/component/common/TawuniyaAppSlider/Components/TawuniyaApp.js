import React from "react";
import tawuniyaApp from "../../../../assets/images/tawuniyaApp.png";
import tawuniyaWaterMark from "../../../../assets/svg/tawuniyaWaterMark.svg";
import appleButton from "../../../../assets/svg/appleButton.svg";
import playstoreButton from "../../../../assets/svg/playstoreButton.svg";
import huwaweiButton from "../../../../assets/svg/huwaweiButton.svg";
import { AppButton } from "component/common/AppButton";
import "../Components/style.scss";
export default function TawuniyaApp() {
	return (
		<div className="tawuniyaAppMain">
			<div className="tawuniyaAppPaper"></div>
			<div className="tawuniyaAppContainer">
				<div className="tawuniyaAppLeft">
					<img src={tawuniyaApp} alt="" />
				</div>
				<div className="tawuniyaAppRight">
					<div className="tawuniyaArWm">
						<img src={tawuniyaWaterMark} alt="" />
					</div>
					<div className="tawuniyaArTextbox">
						<h5>Tawuniya App</h5>
						<p>Get the most out of your Insurance</p>
						<span>
							Save up to 20% on your car insurance renewal. Download Tawuniya
							Drive powered by Vitality, stay safe and earn your rewards.
						</span>
					</div>
					<div className="tawuniyaArBtnCont">
						<p>Download Now</p>
						<div className="tawuniyaArBtnGrp">
							<img src={appleButton} alt="" />
							<img src={playstoreButton} alt="" />
							<img src={huwaweiButton} alt="" />
						</div>
					</div>
				</div>
			</div>
		</div>
	);
}
