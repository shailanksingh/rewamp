import React from "react";
import tawuniyaDrive from "../../../../assets/images/tawuniyaDrive.png";
import ic_logIn from "../../../../assets/svg/ic_log-in.svg";
import ic_drive from "../../../../assets/svg/ic_drive.svg";
import ic_medal from "../../../../assets/svg/ic_medal.svg";
import curveBtm from "../../../../assets/svg/curveBtm.svg";
import curveTop from "../../../../assets/svg/curveTop.svg";
import appleButton from "../../../../assets/svg/appleButton.svg";
import playstoreButton from "../../../../assets/svg/playstoreButton.svg";
import driveBarcode from "../../../../assets/svg/driveBarcode.svg";

import "../Components/style.scss";
export default function TawuniyaDrive() {
	return (
		<div className="tawuniyaDriveMain">
			<div className="tawuniyaDrivePaper"></div>
			<div className="tawuniyaDriveContainer">
				<div className="tawuniyaDriveLeft">
					<img src={tawuniyaDrive} alt="" />
				</div>
				<div className="tawuniyaDriveRight">
					<div className="tawuniyaDrHead">
						<h5>Tawuniya Drive</h5>
						<p>
							You earn Tawuniya Drive points every week based on how well you
							drive. Download the Tawuniya drive app, drive safe and get
							rewarded.
						</p>
					</div>
					<div className="tawuniyaDrFeatures">
						<p>How it works:</p>
						<div className="tawuniyaDrfetBox">
							<div className="tawuniyarDrFetBoxItem">
								<img src={ic_logIn} alt="login icon" />
								<span>Install Tawuniya Drive</span>
							</div>
							<img
								className="tawuniyaDrCurvLineA"
								src={curveBtm}
								alt="curved line icon"
							/>
							<div className="tawuniyarDrFetBoxItem">
								<img src={ic_drive} alt="drive icon" />
								<span>Drive safe</span>
							</div>
							<img
								className="tawuniyaDrCurvLineB"
								src={curveTop}
								alt="curved line icon"
							/>
							<div className="tawuniyarDrFetBoxItem">
								<img src={ic_medal} alt="medal icon" />
								<span>Get rewarded</span>
							</div>
						</div>
					</div>
					<div className="tawuniyaDrDnldBox">
						<h5>Download Now</h5>
						<div className="tawuniyaDrDnldIcCon">
							<div className="tawuniyaDrBarcode">
								<img src={driveBarcode} alt="scan barcode to download app" />
								<span>Scan QR</span>
							</div>
							<div className="tawuniyaDrRestIcon">
								<span>or</span>
								<img src={appleButton} alt="download on the app store" />
								<img src={playstoreButton} alt="get it on playstore" />
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	);
}
