import React from "react";
import "../Components/style.scss";
import tawuniyaDriveFeatures from "../../../../assets/svg/tawuniyaDriveFeatures.svg";
import ic_gift from "../../../../assets/svg/ic_gift.svg";
import ic_percent from "../../../../assets/svg/ic_percent.svg";
import ic_wheel from "../../../../assets/svg/ic_wheel.svg";
import appleButton from "../../../../assets/svg/appleButton.svg";
import playstoreButton from "../../../../assets/svg/playstoreButton.svg";

export default function TawuniyaDriveFeatures() {
	return (
		<div className="tawuniyaFeaturesMain">
			<div className="tawuniyaFeaturesPaper"></div>
			<div className="tawuniyaFeaturesContainer">
				<div className="tawuniyaFeaturesLeft">
					<img src={tawuniyaDriveFeatures} alt="" />
				</div>
				<div className="tawuniyaFeaturesRight">
					<h5>Tawuniya Drive Features</h5>
					<div className="tawuniyaFrBox">
						<div className="tawuniyaFrBoxItem">
							<img src={ic_percent} alt="" />
							<span>Up to 20% discount on car insurance</span>
						</div>
						<div className="tawuniyaFrBoxItem">
							<img src={ic_gift} alt="" />
							<span>Weekly active rewards</span>
						</div>
						<div className="tawuniyaFrBoxItem">
							<img src={ic_wheel} alt="" />
							<span>Safer driving habits</span>
						</div>
					</div>
					<div className="tawuniyaFrDnld">
						<span>Download Now</span>
						<div className="tawuniyaFrDnldBtn">
							<img src={appleButton} alt="download on the app store" />
							<img src={playstoreButton} alt="get it on playstore" />
						</div>
					</div>
				</div>
			</div>
		</div>
	);
}
