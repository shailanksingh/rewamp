import React, { useState } from "react";
import { NormalButton } from "component/common/NormalButton";
import expandIcon from "assets/svg/expandIcon.svg";
import closeIcon from "assets/images/toggleCloseIcon.png";
import carBanner from "assets/svg/carBannerImage.svg";
import twenty from "assets/svg/20%.svg";
import leftArrow from "assets/svg/navLeftArrow.svg";
import rightArrow from "assets/svg/navRightArrow.svg";
import "./style.scss";

export const ToggleBanner = () => {
  // initialize state to toggle card
  const [toggle, setToggle] = useState(true);

  return (
    // banner container starts here
    <div className="row BannerContainer position-relative">
      <div className="col-lg-12 col-md-12 col-12 BannerBackground p-2">
        {toggle ? (
          <React.Fragment>
            <div className="d-flex justify-content-center">
              <div>
                <img src={leftArrow} className="img-fluid pr-5" alt="arrow" />
                <span>
                  <span className="fs-16 fw-700 navToggleConatinerContentOne">
                    15%
                  </span>{" "}
                  <span className="fs-16 fw-400 navToggleConatinerContentOne">
                    off for all new customers. Use code
                  </span>{" "}
                  <span className="fs-16 fw-700 navToggleConatinerContentOne">
                    SAVE15
                  </span>{" "}
                </span>
                <img src={rightArrow} className="img-fluid pl-5" alt="arrow" />
              </div>
            </div>
            <img
              src={expandIcon}
              className="img-fluid expandIconToggler"
              onClick={() => setToggle(false)}
              alt="closeicon"
            />
          </React.Fragment>
        ) : (
          <div className="row">
            <div className="col-12">
              <div className="row toggleContainerTwo">
                <div className="col-6">
                <div className="d-flex justify-content-end">
                <img
                  src={twenty}
                  className="img-fluid"
                  alt="bannerTwenty"
                />
                <img
                  src={carBanner}
                  className="img-fluid carBanner"
                  alt="bannerCar"
                />
                </div>
                </div>
                <div className="rightToggleContent col-6">
                  <p className="toggleHeading fw-800 m-0">
                    Get the most out of your car insurance
                  </p>
                  <p className="togglePara fs-22 fw-400 m-0">
                    Save up to 20% on your car insurance renewal.
                  </p>
                  <div className="pt-2 pb-3">
                    <div className="d-flex flex-row">
                      <div>
                        <NormalButton
                          label="Dowload Tawuniya Drive"
                          className="download-tawuniya-close-drive-btn p-lg-3"
                        />
                      </div>
                      <div className="pl-3">
                        <span className="fs-14 fw-400 poweredText">
                          Powered by Vitality
                        </span>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <img
              src={closeIcon}
              className="img-fluid closeIconToggler"
              onClick={() => setToggle(true)}
              alt="closeicon"
            />
          </div>
        )}
      </div>
    </div>
    // banner container ends here
  );
};
