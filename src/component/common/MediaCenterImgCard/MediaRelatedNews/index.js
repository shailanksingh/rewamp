import React from "react";
import "./style.scss";

function RelatedNewsCard(props) {
  let { Item, isMobile } = props;
  return (
    <div className={isMobile ? "newsRelated_mobile" : "newsRelated"}>
      <div className="DateContainer">
        <button className="buttonEl">{Item.buttonData}</button>
        <p className="dateEl">{Item.date}</p>
      </div>
      <p className="RelatedHeading">{Item.description}</p>
      <div className="hrLine"></div>
    </div>
  );
}

export default RelatedNewsCard;
