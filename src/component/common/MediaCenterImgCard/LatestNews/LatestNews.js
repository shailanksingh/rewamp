import React from "react";
import "./style.scss";
import "./LatestNewsCard-MediaQuery.css";
import { history } from "service/helpers";
import { formatToDMY } from "service/utilities";

function LatestNewsCard({ news }) {
  return (
    <div
      className="latestNewsCard"
      onClick={() => history.push("/home/mediacenter/medianewsdetails", { title: news.title })}
    >
      <div className="DateContainer">
        <button className="buttonEl">{news.category}</button>
        <p className="dateEl">{formatToDMY(news.dateCreated)}</p>
      </div>
      <p className="latestNewsCardPar">{news.title}</p>
    </div>
  );
}

export default LatestNewsCard;
