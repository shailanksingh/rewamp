import React from "react";
import { Typography, Button } from "@material-ui/core";
import MediaCenterOrangeArrow from "assets/svg/MediaCenterIcon/MediaCenterOrangeArrow.svg";
import "./styles.scss";
// import { style } from "@mui/system";
import { history } from "service/helpers";
import { formatToDMY } from "service/utilities";

function MediaImgCard({ news }) {
  const sampleImg = 'https://www.picamon.com/wp-content/uploads/2020/10/Picamon-northern-lights-0-5f8b42955e1ad'
  return (
    <>
      <div
        className="FeatureContainer"
        style={{
          background: `linear-gradient(0deg, rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0.5)), url(${sampleImg})`,
          backgroundSize: "cover",
        }}
      >
        <button className="buttonEl">{news.category}</button>
        <div>
          <h2 className="Heading">{news.title}</h2>
          <div className="DateContainer">
            <p className="mediaMainImgDate">{formatToDMY(news.dateCreated)}</p>
            <p className="VerticalLine"></p>
            <div
              style={{
                display: "flex",
                cursor: 'pointer',
              }}
              onClick={() => history.push("/home/mediacenter/medianewsdetails", { title: news.title })}
            >
              <p className="ArrowText">Read the full story</p>
              <img
                src={MediaCenterOrangeArrow}
                alt="MediaCenterOrangeArrow"
                style={{ marginBottom: "10px", marginLeft: "20px" }}
              />
            </div>
          </div>
        </div>
      </div>
    </>
  );
}

export default MediaImgCard;
