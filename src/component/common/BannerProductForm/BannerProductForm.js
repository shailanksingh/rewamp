import React, { useState } from "react";
import { InsuranceCard } from "component/common/InsuranceCard";
import { NormalSearch } from "../NormalSearch";
import PhoneNumberInput from "../PhoneNumberInput";
import { NormalInput } from "component/common/NormalInput";
import { NormalRadioButton } from "component/common/NormalRadioButton";
import { NormalButton } from "component/common/NormalButton";
import { RewardCard } from "component/common/RewardCard";
import policyCar from "assets/svg/policyCar.svg";
import coverage from "assets/svg/coverage.svg";
import renew from "assets/svg/renew.svg";
import mmp1 from "../../../assets/svg/mmp1.svg";
import mmp2 from "../../../assets/svg/mmp2.svg";
import mmp3 from "../../../assets/svg/mmp3.svg";
import KSAFlagImage from "../../../assets/images/ksaFlag.png";
import IndiaFlagImage from "../../../assets/images/indiaFlag.png";
import iquamaIcon from "assets/svg/iquamaIcon.svg";
import "./style.scss";
import "../../MotorPage/MotorInsuranceComponent/style.scss";
export const BannerProductForm = () => {
	const insuranceCardData = [
		{
			id: 0,
			cardIcon: `${mmp1}`,
			content: "Submit / Track a Claim",
		},
		{
			id: 1,
			cardIcon: `${mmp2}`,
			content: "Road Side and Accident Assistance",
		},
		{
			id: 2,
			cardIcon: `${mmp2}`,
			content: "Get free MVPI Services at your Doorstep",
		},
		{
			id: 3,
			cardIcon: `${mmp3}`,
			content: "Get free Trip to you Car Agency for Maintenance",
		},
	];

	let dialingCodes = [
		{
			code: "+91",
			image: IndiaFlagImage,
		},
		{
			code: "+966",
			image: KSAFlagImage,
		},
	];

	//initialize state for input field
	const [valueOne, setValueOne] = useState("");

	//initialize state for input field
	const [valueTwo, setValueTwo] = useState("");

	//initialiize state for radio buttons
	const [radioOne, setRadioOne] = useState("Yes");

	//initialize state for input field
	const [inpOne, setInpOne] = useState({ userId: "", phone: "" });

	let [selectedCode, setSelectedCode] = useState(dialingCodes[0]);

	let [phoneNumber, setPhoneNumber] = useState("");

	const handleInputChange = (e) => {
		const { name, value } = e.target || e || {};
		setInpOne({ ...inpOne, [name]: value });
	};

	return (
		// motor insurance banner starts here
		<div className="row motorInsuranceContainer pb-5">
			<div className="col-lg-7 col-12 pt-5">
				<p className="motorTitle fw-800 m-0 pt-5">
					Medical Malpractice Insurance Program
				</p>
				<p className="motorPara fs-16 fw-400 pb-3">
					A program protects those persons practicing medical professions from
					the risks associated with their work, and the legal third party
					liability that may arise out of any error, negligence or omission
					incurred during the performance of their work accrding to the terms,
					conditions and exclusions set forth in the insurance policy.
				</p>
				<div className="motorSubTitleBox">
					<h5>Download Documents</h5>
					<p>We provide the best and trusted service for our customers</p>
				</div>
				<div className="row pt-3">
					<InsuranceCard
						heathInsureCardData={insuranceCardData}
						// insuranceCardPadding="pb-3"
						// insuranceTextClass="fs-16 fw-700 insuranceText pt-2"
					/>
				</div>
				{/* <div className="row pt-4">
					<div className="col-lg-6 col-md-12 col-12 pb-lg-0 pb-md-5 pb-5">
						<div className="managePolicy">
							<div className="policyContainer p-3">
								<p className="manageText fs-20 fw-800 m-0">
									Manage your Policy
								</p>
								<span className="fs-14 fw-600 addText">
									Add more vehicles and coverage instantly
								</span>
							</div>
							<div className="policyTypeContainer p-3">
								<div className="row py-2">
									<div className="col-lg-4 col-12 vehilcleBox pr-3">
										<img
											src={policyCar}
											className="img-fluid mx-auto d-block"
											alt="icon"
										/>
										<p className="policyBoxPara fs-14 fw-400 text-center">
											Add Vehicles
										</p>
									</div>
									<div className="col-lg-4 col-12 pl-2 pr-3 coverageBox">
										<img
											src={coverage}
											className="img-fluid mx-auto d-block"
											alt="icon"
										/>
										<p className="policyBoxPara fs-14 fw-400 text-center">
											Buy Coverages
										</p>
									</div>
									<div className="col-lg-4 col-12 renewBox pl-2">
										<img
											src={renew}
											className="img-fluid mx-auto d-block"
											alt="icon"
										/>
										<p className="renewBoxPara fs-14 fw-400 text-center">
											Renew Policy
										</p>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div className="col-lg-6 col-md-12 col-12">
						<RewardCard
							cardTitle="Drive"
							cardSubTitle="Save Money By Driving Better"
							titleClass="rewardTitle"
							subtitleClass="rewardSubTitle"
							label="Claim your Rewards"
							className="rewardBtnClass px-3"
						/>
					</div>
				</div> */}
			</div>
			<div className="col-lg-5 col-md-12 col-12 pt-5">
				<div className="insureMotorCard h-100">
					<div className="insureLayerOne">
						<p className="insureCardTitle fw-800 m-0">
							Insure Your Health Now!
						</p>
					</div>
					<div className="insureLayerTwo">
						<div className="subInsureLayer p-4">
							<div className="row">
								<div className="col-12">
									<NormalSearch
										className="motorInputfieldOne"
										name="userId"
										value={inpOne.userId}
										placeholder="CR Number"
										onChange={handleInputChange}
										needLeftIcon={true}
										leftIcon={iquamaIcon}
									/>
								</div>
								{/* <div className="col-12">
									<NormalSearch
										className="motorInputfieldOne"
										name="userId"
										value={inpOne.userId}
										placeholder="CR Expiration Date"
										onChange={handleInputChange}
										needLeftIcon={true}
										leftIcon={iquamaIcon}
									/>
								</div> */}
								<div className="col-12 pt-4">
									<PhoneNumberInput
										className="motorPhoneInput"
										selectInputClass="motorSelectInputWidth"
										selectInputFlexType="motorFlexType"
										dialingCodes={dialingCodes}
										selectedCode={selectedCode}
										setSelectedCode={setSelectedCode}
										value={phoneNumber}
										name="phoneNumber"
										onChange={({ target: { value } }) => setPhoneNumber(value)}
									/>
								</div>
							</div>
							<p className="fs-16 fw-400 pt-3">
								Do you want to insure vehicles for your company?
							</p>
							<div className="d-flex flex-row">
								<div>
									<NormalRadioButton
										type="radio"
										name="radioOne"
										value="Yes"
										onChange={(e) => setRadioOne(e.target.value)}
										radioValue="Yes"
										checked={radioOne === "Yes"}
									/>
								</div>
								<div className="pl-3">
									<NormalRadioButton
										type="radio"
										name="radioOne"
										value="No"
										onChange={(e) => setRadioOne(e.target.value)}
										radioValue="No"
										checked={radioOne === "No"}
									/>
								</div>
							</div>
						</div>
						<div className="px-5">
							<p className="fs-16 fw-400 insureTerms pb-3">
								By continuing you give Tawuniya advance consent to obtain my
								and/or my dependents' information from the National Information
								Center.
							</p>
							<div className="pb-5 mb-4">
								<NormalButton label="Buy Now" className="insureNowBtn" />
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		// motor insurance banner ends here
	);
};
