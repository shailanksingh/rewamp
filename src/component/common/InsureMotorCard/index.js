import React, { useState } from "react";
import { NormalButton } from "../NormalButton";
import { NormalInput } from "../NormalInput";
import { NormalRadioButton } from "../NormalRadioButton";
import "./style.scss";

export const InsureMotorCard = () => {
	const [valueOne, setValueOne] = useState("");

	const [valueTwo, setValueTwo] = useState("");

	const [radioOne, setRadioOne] = useState("Yes");

	const [radioTwo, setRadioTwo] = useState("No");

	const handleRadioOne = (e) => {
		setRadioOne(e.target.value);
		console.log(radioOne, "jjj");
	};

	const handleRadioTwo = (e) => {
		setRadioTwo(e.target.value);
		console.log(radioTwo, "jjj");
	};

	return (
		<div className="row insureMotorContainer pt-3">
			<div className="col-12 ml-lg-5">
				<div className="insureLayerOne">
					<p className=" insureCardTitle fw-800 m-0">Insure Your Motor Now!</p>
				</div>
				<div className="insureLayerTwo">
					<div className="row insureSubLayerTwo">
						<div className="col-12 p-4">
							<div>
								<NormalInput
									textField="motorInputfieldOne"
									variant="outlined"
									value={valueOne}
									placeholder="Natioal ID, Iqama"
									onChange={(e) => setValueOne(e.target.value)}
								/>
							</div>
							<div className="pt-4">
								<NormalInput
									variant="outlined"
									textField="motorInputfieldOne"
									value={valueTwo}
									placeholder="+966 ex: 5xxxxxxxx"
									onChange={(e) => setValueTwo(e.target.value)}
								/>
							</div>
							<p className="fs-16 fw-400 pt-3">
								Do you want to insure vehicles for your company?
							</p>
							<div className="d-flex flex-row">
								<div>
									<NormalRadioButton
										type="radio"
										value={radioOne}
										onChange={handleRadioOne}
										radioValue="Yes"
									/>
								</div>
								<div className="pl-3">
									<NormalRadioButton
										type="radio"
										value={radioTwo}
										onChange={handleRadioTwo}
										radioValue="No"
									/>
								</div>
							</div>
						</div>
					</div>
					<div className="row">
						<div className="col-12 insureNewSubLayer px-5 mx-1">
							<p className="fs-16 fw-400 insureTerms">
								By continuing you give Tawuniya advance consent to obtain my
								and/or my dependents' information from the National Information
								Center.
							</p>
							<NormalButton label="Insure Now" className="insureNowBtn" />
						</div>
					</div>
				</div>
			</div>
		</div>
	);
};
