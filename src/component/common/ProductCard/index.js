import React, { useState } from "react";
import { Link } from "react-router-dom";
import { history } from "service/helpers";
import {
  Box,
  Typography,
  ThemeProvider,
  createTheme,
  Button,
  Grid,
  Icon,
  Card,
  CardContent,
  CardActions,
  Divider,
} from "@material-ui/core";
import productsicon1 from "../../../assets/svg/productsicon1.svg";
import orangeArrow from "../../../assets/svg/orangeArrow.svg";
import styles from "./style.module.scss";

import ChevronRightIcon from "@material-ui/icons/ChevronRight";

const theme = createTheme({
  palette: {
    common: {},
    primary: {
      main: "#EE7500",
    },
    // redColor: palette.augmentColor({ color: red }),
  },
  typography: {
    // h5: {
    // 	fontWeight: 800,
    // 	fontSize: 20,
    // },
    // subtitle2: {
    // 	fontSize: 12,
    // 	fontWeight: 800,
    // },
    // body1: {
    // 	fontWeight: 700,
    // 	fontSize: 19,
    // 	color: "#4C565C",
    // },
    // body2: {
    // 	fontWeight: 400,
    // 	fontSize: 14,
    // 	color: "#4C565C",
    // },
  },
  button: {
    color: "#FFFFFF",
  },
});

export const ProductCard = ({
  cardNo,
  Display,
  category,
  productName,
  productDetail,
  productDescription,
  productIcon,
  productImg,
  driveAr,
  driveEn,
  redirectURL,
  urlTrue,
  url,
}) => {
  const openInNewTab = (url1) => {
    const newWindow = window.open(url1, "_blank", "noopener,noreferrer");
    if (newWindow) newWindow.opener = null;
  };
  return (
    <Box className={styles.pcCardContainer}>
      <ThemeProvider theme={theme}>
        <Card
          className={styles.pcCard}
          style={{
            visibility: Display ? "visible" : "hidden",
            display: cardNo === 6 ? "none" : "block",
          }}
        >
          <Box className={styles.pcMain}>
            <Grid
              className={styles.pcHead}
              container
              spacing={1}
              direction="row"
              alignItems="center"
            >
              <Grid className={styles.pcProductIcon} item>
                <Icon>
                  <img src={productIcon} alt="" />
                </Icon>
              </Grid>
              <Grid item ml={2}>
                <Typography className={styles.pcSubtitle} variant="subtitle2">
                  {category}
                </Typography>
              </Grid>
            </Grid>
            <CardContent className={styles.pcContent}>
              <Typography
                className={styles.pcCardTitle}
                gutterBottom
                variant="h5"
                component="h2"
              >
                {productName}
              </Typography>
              <Typography
                className={styles.pcCardDetails}
                variant="body1"
                color="textSecondary"
                component="p"
              >
                {productDetail}
              </Typography>
              <Typography
                className={styles.pcCardDescription}
                variant="body2"
                color="textSecondary"
                component="p"
              >
                {productDescription}
              </Typography>
            </CardContent>
          </Box>
          <Divider />
          <CardActions className={styles.pcBtm}>
            {/* <Typography className={styles.pcBtmTitle} variant="body2">
							EXPLORE MORE
						</Typography> */}
            <Link>
              <Button
                className={styles.pcBtmTitle}
                size="small"
                color="primary"
                variant="text"
                onClick={() =>
                  redirectURL ? history.push(redirectURL) : console.log("")
                }
                endIcon={
                  <Icon style={{ height: "fit-content", width: "fit-content" }}>
                    <img
                      style={{ height: "0.8rem", marginBottom: "2px" }}
                      className="ProductBtmCtaArrow"
                      src={orangeArrow}
                      alt="View all Products"
                    />
                  </Icon>
                }
              >
                EXPLORE MORE
              </Button>
            </Link>
            <Button
              size="small"
              color="primary"
              variant="outlined"
              onClick={() => (urlTrue ? openInNewTab(url) : "")}
              // endIcon={<ChevronRightIcon />}
            >
              Buy Now
            </Button>
          </CardActions>
        </Card>
        <Card
          className={styles.pcAntCard}
          style={{
            visibility: Display ? "visible" : "hidden",
            display: cardNo === 6 ? "block" : "none",
            position: "relative",
          }}
        >
          {/* <img src={productImg} style={{ width: "100%", height: "100%" }} /> */}
          <Box className={styles.pcAntCardBox} component={"div"}>
            <img src={driveAr} className={styles.pcAntImg1} />
            <img src={driveEn} className={styles.pcAntImg2} />
            <Box className={styles.pcAntTextBox} component={"div"}>
              <Typography className={styles.pcAntDetail} component={"p"}>
                {productDetail}
              </Typography>
              <Typography className={styles.pcAntDesc}>
                {productDescription}
              </Typography>
            </Box>
            <Button
              className={styles.pcAntBtn}
              size="small"
              color="primary"
              variant="contained"
              endIcon={<ChevronRightIcon />}
            >
              Claim Your Rewards
            </Button>
          </Box>
        </Card>
      </ThemeProvider>
    </Box>
  );
};
