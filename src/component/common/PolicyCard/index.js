import React from "react";
import "./style.scss";

export default function PolicyCard({ policyList }) {
  return (
    <>
      {policyList?.map((ele, index) => (
        <div className="policy-cards" key={index}>
          <img src={ele.image} alt="Health"></img>
          <p>{ele.title}</p>
        </div>
      ))}
    </>
  );
}
