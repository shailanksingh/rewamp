import orangeArrow from "assets/svg/orangeArrow.svg";
import greyArrow from "assets/svg/greyArrow.svg";
import downArrow from "assets/svg/downArrow.svg";
import footerArrow from "assets/svg/footerArrow.svg";
import carDamage from "assets/svg/damageCar.svg";
import xCircle from "assets/svg/xCircle.svg";
import xOrangeCircle from "assets/svg/orangeCloseIcon.svg";
import tickMark from "assets/svg/tickMark.svg";
import plus from "assets/svg/pluscircle.svg";
import holdCar from "assets/svg/holdCar.svg";
import orangePerkTick from "assets/svg/orangePerkTick.svg";
import perkIcon1 from "assets/svg/perkIcons/perksIcon1.svg";
import perkIcon2 from "assets/svg/perkIcons/perksIcon2.svg";
import perkIcon3 from "assets/svg/perkIcons/perksIcon3.svg";
import perkIcon4 from "assets/svg/perkIcons/perksIcon4.svg";
import perkIcon5 from "assets/svg/perkIcons/perksIcon5.svg";
import perkIcon6 from "assets/svg/perkIcons/perksIcon6.svg";
import perkIcon7 from "assets/svg/perkIcons/perksIcon7.svg";
import perkIcon8 from "assets/svg/perkIcons/perksIcon8.svg";
import perkIcon9 from "assets/svg/perkIcons/perksIcon9.svg";
import perkIcon10 from "assets/svg/perkIcons/perksIcon10.svg";
import perkIcon11 from "assets/svg/perkIcons/perksIcon11.svg";
import perkIcon12 from "assets/svg/perkIcons/perksIcon12.svg";
import perkIcon13 from "assets/svg/perkIcons/perksIcon13.svg";
import perkIcon14 from "assets/svg/perkIcons/perksIcon14.svg";
import perkDrive from "assets/svg/perkIcons/perkDrive.svg";
import engine from "assets/svg/breakDownIcons/engine.svg";
import gear from "assets/svg/breakDownIcons/gearbox.svg";
import differential from "assets/svg/breakDownIcons/differential.svg";
import turbo from "assets/svg/breakDownIcons/turbo.svg";
import prop from "assets/svg/breakDownIcons/propShaft.svg";
import steer from "assets/svg/breakDownIcons/steer.svg";
import wheelBearing from "assets/svg/breakDownIcons/wheelBearing.svg";
import breakSystem from "assets/svg/breakDownIcons/break.svg";
import fuel from "assets/svg/breakDownIcons/fuelSystem.svg";
import electricComponents from "assets/svg/breakDownIcons/elcetricComponents.svg";
import suspension from "assets/svg/breakDownIcons/suspension.svg";
import cylinder from "assets/svg/breakDownIcons/cylinder.svg";
import fraudBanner from "assets/svg/Layer 1.svg";
import violationsCardIcon from "assets/svg/motorViolations.svg";
import motorFraud from "assets/svg/Motorfaudbanner.svg";
import travelFraud from "assets/svg/travelFraudBanner.svg";
import KSAFlagImage from "assets/images/ksaFlag.png";
import IndiaFlagImage from "assets/images/indiaFlag.png";
import serviceIconOne from "assets/svg/TawuniyaServicesIcons/serviceIcons1.svg";
import serviceArrow from "assets/svg/serviceArrow.svg";
import telemed from "assets/svg/telemedicine.svg";
import theft from "assets/svg/theft.svg";
import flight from "assets/svg/flightdelayclaim.svg";
import accident from "assets/svg/accident.svg";
import medAssisst from "assets/svg/medassisst.svg";
import motot_insurance_image from "assets/images/mobile/motot_insurance_image.svg";
import medical_tint from "assets/images/mobile/medical_tint.svg";
import property_tint from "assets/images/mobile/property_tint.svg";
import careers_image_one from "assets/images/mobile/careers_image_one.svg";
import careers_image_two from "assets/images/mobile/careers_image_two.svg";
import careers_image_three from "assets/images/mobile/careers_image_three.svg";
import carBanner from "assets/svg/roadCarBanner.svg";
import teleMedBanner from "assets/svg/teleMedDasboardBanner.svg";
import progressArrow from "assets/svg/progressGreyArrow.svg";

import al_shamel_motor from "assets/images/mobile/al_shamel_motor.svg";
import sanad_motor from "assets/images/mobile/al_shamel_motor.svg";
import sanad_plus_motor from "assets/images/mobile/sanad_plus_motor.svg";
import mech_breakdown_motor from "assets/images/mobile/mech_breakdown_motor.svg";
import family_insurance_medical from "assets/images/mobile/family_insurance_medical.svg";
import Visit_insurance from "assets/images/mobile/Visit_insurance.svg";
import Umrah_insurance from "assets/images/mobile/Umrah_insurance.svg";
import Hajj_insurance from "assets/images/mobile/Hajj_insurance.svg";
import Home_insurance from "assets/images/mobile/Home_insurance.svg";
import International_Travel_insurance from "assets/images/mobile/International_Travel_insurance.svg";
import Covid19_Insurance from "assets/images/mobile/Covid19_Insurance.svg";
import Malpractice_Insurance from "assets/images/Malpractice_Insurance.svg";
import Shop_owner_Insurance from "assets/images/Shop_owner_Insurance.svg";
import Engineering_Insurance from "assets/images/mobile/Engineering_Insurance.svg";
import Industrial_Insurance from "assets/images/mobile/Industrial_Insurance.svg";
import Inverstment_Insurance from "assets/images/mobile/Inverstment_Insurance.svg";
import Retail_Insurance from "assets/images/mobile/Retail_Insurance.svg";
import Services_Insurance from "assets/images/mobile/Services_Insurance.svg";
import Telecommunication_Insurance from "assets/images/mobile/Telecommunication_Insurance.svg";
import Transportion_Insurance from "assets/images/mobile/Transportion_Insurance.svg";
import SanadNew from "assets/images/mobile/SanadNew.svg";
import Vitality_insurance from "assets/images/mobile/Vitality_insurance.svg";
import liability_insurance from "assets/images/mobile/liability_insurance.svg";
import Damage_insurance from "assets/images/mobile/Damage_insurance.svg";
import gift from "assets/about/gift.png";
import boost from "assets/about/boost.png";
import strength from "assets/about/strength.png";
import HealthPloicy from "assets/about/health.png";
import engineerIcon1 from "assets/news/engineerIcon1.png";
import engineerIcon2 from "assets/news/engineerIcon2.png";
import engineerIcon3 from "assets/news/engineerIcon3.png";
import engineerIcon4 from "assets/news/engineerIcon4.png";
import engineerIcon5 from "assets/news/engineerIcon5.png";
import engineerIcon6 from "assets/news/engineerIcon6.png";
import engineerIcon7 from "assets/news/engineerIcon7.png";
import engineerIcon8 from "assets/news/engineerIcon8.png";
import engineerIcon9 from "assets/news/engineerIcon9.png";
import engineerIcon10 from "assets/news/engineerIcon10.png";
import engineerIcon11 from "assets/news/engineerIcon11.png";
import engineerIcon12 from "assets/news/engineerIcon12.png";
import engineerIcon13 from "assets/news/engineerIcon13.png";
import engineerIcon14 from "assets/news/engineerIcon14.png";
import engineerIcon15 from "assets/news/engineerIcon15.png";
import engineerIcon16 from "assets/news/engineerIcon16.png";
import industrialIcon4 from "assets/news/industrialIcon4.png";
import industrialIcon7 from "assets/news/industrialIcon7.png";
import industrialIcon10 from "assets/news/industrialIcon10.png";
import industrialIcon16 from "assets/news/industrialIcon16.png";
import financialIcon7 from "assets/news/financialIcon7.png";
import financialIcon8 from "assets/news/financialIcon8.png";
import financialIcon9 from "assets/news/financialIcon9.png";
import retailIcon7 from "assets/news/retailIcon7.png";
import retailIcon8 from "assets/news/retailIcon8.png";
import retailIcon10 from "assets/news/retailIcon10.png";
import retailIcon12 from "assets/news/retailIcon12.png";
import retailIcon13 from "assets/news/retailIcon13.png";
import serviceIcon10 from "assets/news/serviceIcon10.png";
import serviceIcon12 from "assets/news/serviceIcon12.png";
import serviceIcon13 from "assets/news/serviceIcon13.png";
import transportIcon5 from "assets/news/transportIcon5.png";
import transportIcon7 from "assets/news/transportIcon7.png";
import transportIcon8 from "assets/news/transportIcon8.png";
import transportIcon9 from "assets/news/transportIcon9.png";
import transportIcon10 from "assets/news/transportIcon10.png";
import HealthPolicy from "assets/images/mobile/health.png";
import Add from "assets/images/mobile/add.png";
import Upgrade from "assets/images/mobile/upgrade.png";
import Update from "assets/images/mobile/update.png";

export const FHTimeLineData = [
  {
    fhyear: "2021",
    fhTimeLineList: [
      {
        lineTrue: true,
        fhTitile: "Excellence in Customer Service- KSA ",
        fhReference: "By Global Brands Magazine",
      },
      {
        lineTrue: true,
        fhTitile: "Best Health Insurance Brand in Saudi Arabia",
        fhReference: "By Global Brands Magazine",
      },
      {
        lineTrue: true,
        fhTitile: "Best Auto Insurance Company-KSA",
        fhReference: "By Global Banking and Finance Review",
      },
      {
        lineTrue: true,
        fhTitile:
          "Best Shared Value Insurance Concept – Tawuniya Vitality – Saudi Arabia 2021",
        fhReference: "By International Finance Awards",
      },
      {
        lineTrue: true,
        fhTitile: "Outstanding Contribution to Social Impact in Saudi Arabia",
        fhReference: "By CFI British Magazine",
      },
      {
        lineTrue: true,
        fhTitile: "Best Innovative Insurance Company ",
        fhReference: "International Business Magazine",
      },
      {
        lineTrue: true,
        fhTitile: "The best Takaful insurance company in the world",
        fhReference: "Global Islamic Finance Awards",
      },
      {
        lineTrue: true,
        fhTitile: "Top 10 Most Valuable Insurance Brands in the Middle East",
        fhReference: "By Brand Finance",
      },
      {
        lineTrue: true,
        fhTitile: "Top 100 Companies in the Middle East",
        fhReference: "By Forbes Magazine",
      },
      {
        lineTrue: false,
        fhTitile:
          "Selection of Tawuniya CEO among Top 50 Influential Leaders- KSA",
        fhReference: "By Arabia Business Magazine",
      },
    ],
  },
  {
    fhyear: "2020",
    fhTimeLineList: [
      {
        lineTrue: true,
        fhTitile: "Excellence in Customer Service- KSA ",
        fhReference: "By Global Brands Magazine",
      },
      {
        lineTrue: true,
        fhTitile:
          "Best Shared Value Insurance Concept – Tawuniya Vitality – Saudi Arabia 2021",
        fhReference: "By International Finance Awards",
      },
      {
        lineTrue: false,
        fhTitile:
          "Selection of Tawuniya CEO among Top 50 Influential Leaders- KSA",
        fhReference: "By Arabia Business Magazine",
      },
    ],
  },
];

export const FHCardData = [
  {
    fhCardTitle: "Total Assets",
    fhCardValue: "14.6 Billion",
  },
  {
    fhCardTitle: "Total Equity",
    fhCardValue: "3 Billion",
  },
  {
    fhCardTitle: "Gross Written Premiums (GWP)",
    fhCardValue: "10.2 Billion",
  },
  {
    fhCardTitle: "Net Earned Premiums",
    fhCardValue: "7.9 Billion",
  },
  {
    fhCardTitle: "Net Incurred Claims",
    fhCardValue: "6.6 Billion",
  },
  {
    fhCardTitle: "Net Profit before Zakat",
    fhCardValue: "350 Million",
  },
  {
    fhCardTitle:
      "Surplus of Insurance Operations Minus Policyholders Investments Revenues",
    fhCardValue: "77.5 Million",
  },
  {
    fhCardTitle: "Net income of Policyholders Investments",
    fhCardValue: "145.7 Million",
  },
  {
    fhCardTitle: "Net income of Shareholders Capital Investments",
    fhCardValue: "157 Million",
  },
  {
    fhCardTitle: "Earnings per Share",
    fhCardValue: "2.13",
  },
  {
    fhCardTitle: "Issued and Paid-up Capital",
    fhCardValue: "1,250 Million",
  },
];

export const AboutUsFixedHeightCard = [
  {
    cardIcon: orangeArrow,
    cardTitle: "Our Competitive Advantages",
    cardPara1: "Financial Highlights of Tawuniya in 2021",
    cardUrl: "/home/aboutus/financialhighlights",
    cardPara2: "Competitive Advantages",
    cardPara3: "Tawuniya Awards & Ranking- 2021",
    cardTitleColor: "#EE7500",
    cardSubtitleColor: "#FFFFFF",
    cardBackgroundColor: "#1A1C1F",
  },
  {
    cardTitle: "Board Of Directors & Senior Executives",
    cardPara1: "Board Of Directors (BOD)",
    cardPara2: "Senior Executives",
    cardTitleColor: "#FFFFFF",
    cardUrl: "/home/aboutus/directors",
    cardSubtitleColor: "#FFFFFF",
    cardBackgroundColor: "#679AB4",
  },
  {
    cardIcon: orangeArrow,
    cardTitle: "Social Responsibility",
    cardPara1: "Adopting the CSR Strategy",
    cardPara2: "Social Contributions in 2021",
    cardUrl: "/home/social",
    cardTitleColor: "#EE7500",
    cardSubtitleColor: "#4C565C",
    cardBackgroundColor: "#FFFFFF",
  },
];

export const AboutUsBannerCards = [
  {
    cardTitle: "Our Mission",
    cardPara:
      "Exceed expectations through superior customer experience and service excellence",
  },
  {
    cardTitle: "Our Purpose",
    cardPara: "Together for a safer life and bigger dreams",
  },
  {
    cardTitle: "Our Vision",
    cardPara: "Largest insurer in the MENA region",
  },
];

export const violationsObCardTop = [
  {
    cardIcon: violationsCardIcon,
    cardTitle: "Ascertaining credibility In reporting",
    cardPara:
      "by avoiding any rumors and allegations that are not based on real facts.",
    cardClass: "",
  },
  {
    cardIcon: violationsCardIcon,
    cardTitle: "Avoiding Malicious Reports",
    cardPara:
      "for the purpose of defaming the reputation of others, inflicting revenge on them, revenge or undermining confidence in the financial institution, its employees or stakeholders.",
    cardClass: "alignfitCard",
  },
  {
    cardIcon: violationsCardIcon,
    cardTitle: "The complete Confidentiality",
    cardPara:
      "for the reported violation in order to achieve the public interest.",
    cardClass: "alignfitCard",
  },
];

export const violationsObCardBtm = [
  {
    cardIcon: violationsCardIcon,
    cardTitle: "Non-Disclosure",
    cardPara: "Not to disclose any information about the whistleblower.",
    cardClass: "",
  },
  {
    cardIcon: violationsCardIcon,
    cardTitle: "Anonymous Reporter is not covered",
    cardPara:
      "The informant, whose name and identity has not been disclosed, is not entitled to claim the protection covered under Subsection (10-4-1) of the Whistleblowing Policy.",
    cardClass: "alignfitCard",
  },
  {
    cardIcon: violationsCardIcon,
    cardTitle: "Protection of non-malicious reporting persons",
    cardPara: "From any revengeful action taken against the whistleblower.",
    cardClass: "alignfitCard",
  },
];

export const productContents = [
  {
    id: 0,
    productTitle: "Products",
    arrowIcon: orangeArrow,
    highlightItem: true,
  },
  {
    id: 1,
    productTitle: "Claim Assistance",
    arrowIcon: greyArrow,
    highlightItem: false,
  },
  {
    id: 2,
    productTitle: "Tawuniya Vitality",
    arrowIcon: greyArrow,
    highlightItem: false,
  },
  {
    id: 3,
    productTitle: "Help Center",
    arrowIcon: greyArrow,
    highlightItem: false,
  },
  {
    id: 4,
    productTitle: "About",
    arrowIcon: greyArrow,
    highlightItem: false,
  },
  {
    id: 5,
    productTitle: "Investor Relations",
    arrowIcon: null,
    highlightItem: false,
  },
  {
    id: 6,
    productTitle: "Contact Us",
    arrowIcon: null,
    highlightItem: false,
  },
];

export const otherProducts = [
  {
    id: 0,
    productTitle: "Motor Insurance",
    productContentIsArray: true,
    productContent: [
      "Al Shamel",
      "Sanad",
      "Sanad Plus",
      "Motor Flex",
      "Tawuniya Drive",
    ],
    subIcon: downArrow,
  },
  {
    id: 0,
    productTitle: "Property & Casualty",
    productContent: [
      "Home Insurance",
      "International Travel Insurance",
      "COVID-19 Travel Insurance",
      "Medical Malpractice Insurance",
      "Shop Owners Insurance",
    ],
    subIcon: downArrow,
  },
  {
    id: 0,
    productTitle: "Medical & Takaful",
    productContent: [
      "My Family Medical Insurance",
      "Visit Visa Medical Insurance",
      "Umrah Medical Insurance",
    ],
    subIcon: downArrow,
  },
];

export const moreProductData = [
  {
    id: 0,
    title: "Motor",
    content: [
      "Tawuniya Drive",
      "Motor Flex",
      "Sanad Plus",
      "Sanad",
      "Al Shameel",
    ],
  },
  {
    id: 1,
    title: "Property & Casualty",
    content: [
      "Shop Owners Insurance",
      "Medical Malpractice Insurance",
      "COVID-19 Travel Insurance",
      "International Travel Insurance",
      "Home Insurance",
    ],
  },
  {
    id: 2,
    title: "Medical & Takaful",
    content: [
      "Umrah Medical Insurance",
      "Visit Visa Medical Insurance",
      "My Family Medical Insurance",
    ],
  },
];

export const footerData = [
  {
    id: 0,
    title: "Motor",
    titleAr: "المحرك",
    footerIcon: footerArrow,
    content: [
      {
        id: 1,
        label: "Al Shamel",
        labelAr: "الشامل",
        routeURL: "/home-insurance/motor",
      },
      {
        id: 2,
        label: "Sanad",
        labelAr: "سند",
        routeURL: "/home-insurance/motor",
      },
      {
        id: 3,
        label: "Sanad Plus",
        labelAr: "سند بلس",
        routeURL: "/home-insurance/motor",
      },
      {
        id: 4,
        label: "Mechanical Breakdown",
        labelAr: "الانهيار الميكانيكي",
        routeURL: "/home-insurance/motor",
      },
      {
        id: 5,
        label: "Tawuniya Drive",
        labelAr: "محرك التعاونية",
        routeURL: "/home-insurance/motor",
      },
    ],
  },
  {
    id: 1,
    title: "Property & Casualty",
    titleAr: "الممتلكات والإصابات",
    footerIcon: footerArrow,
    content: [
      {
        id: 1,
        label: "Home Insurance",
        labelAr: "تامين المنازل",
        routeURL: "/home/health-insurance",
      },
      {
        id: 2,
        label: "International Travel Insurance",
        labelAr: "تامين السفر الدولي",
        routeURL: "/home/internationaltravelpage",
      },
      {
        id: 3,
        label: "COVID-19 Travel Insurance",
        labelAr: "تامين فيروس كورونا للسفر",
      },
      {
        id: 4,
        label: "Medical Malpractice Insurance",
        labelAr: "تأمين الأخطاء الطبية",
        routeURL: "/home/medicalmalpracticepage",
      },
      {
        id: 5,
        label: "Shop Owners Insurance",
        labelAr: "تامين اصحاب المحلات"
      },
    ],
  },
  {
    id: 2,
    title: "Medical & Takaful",
    titleAr: "طبي وتكافل",
    footerIcon: footerArrow,
    content: [
      {
        id: 1,
        label: "My Family Medical Insurance",
        labelAr: "تامين عائلتي الصحي",
      },
      {
        id: 2,
        label: "Visit Visa Medical Insurance",
        labelAr: "تامين تاشيرة الزيارة الطبي",
      },
      {
        id: 3,
        label: "Hajj Insurance Program",
        labelAr: "برنامج تامين الحج",
        routeURL: "/home/programs/hajjinsuranceprogram",
      },
      {
        id: 4,
        label: "Umrah Medical Insurance",
        labelAr: "برنامج تامين العمرة",
        routeURL: "/home/programs/UmrahInsuranceProgram",
      },
    ],
  },
  {
    id: 3,
    title: "Customer Service",
    titleAr: "خدمة العملاء",
    footerIcon: footerArrow,
    content: [
      {
        id: 1,
        label: "Help Center",
        labelAr: "مركز المساعدة",
      },
      {
        id: 2,
        label: "Motor FAQs",
        labelAr: "الاسئلة الشائعة - المحرك",
        routeURL: "/home/customerservice/motorfraud",
      },
      {
        id: 3,
        label: "Medical FAQs",
        labelAr: "الاسئلة الشائعة - الطبي",
        routeURL: "/home/customerservice/medicalfraud",
      },
      {
        id: 4,
        label: "Property & Casuality FAQs",
        labelAr: "الاسئلة الشائعة - الممتلكات",
        routeURL: "/home/customerservice/travelfraud",
      },
      {
        id: 5,
        label: "Open a Support Request",
        labelAr: "فتح تذكرة جديدة",
        routeURL: "/home/customerservice/opencomplaint",
      },
      {
        id: 6,
        label: "Surplus Insurance",
        labelAr: "تامين بلس",
        routeURL: "/home/customerservice/surplus",
      },
    ],
  },
];

export const footerNextData = [
  {
    id: 0,
    title: "About Us",
    titleAr: "عننا",
    footerIcon: footerArrow,

    content: [
      {
        id: 1,
        label: "About Tawuniya",
        labelAr: "عن االتعاونية",
        routeURL: "/home/aboutus",
      },
      {
        id: 2,
        label: "Financial Highlights",
        labelAr: "الملامح المالية",
        routeURL: "/home/aboutus/financialhighlights",
      },
      {
        id: 3,
        label: "Competitive Advantages",
        labelAr: "المميزات التنافسية",
      },
      {
        id: 4,
        label: "Board Of Directors",
        labelAr: "مجلس الادارة",
        routeURL: "/home/aboutus/directors",
      },
      {
        id: 5,
        label: "Social Responsibility",
        labelAr: "المسؤوليات الاجتماعية",
        routeURL: "/home/social",
      },
      {
        id: 6,
        label: "Media Center",
        labelAr: "المركز الطبي",
        routeURL: "/home/mediacenter",
      },
      {
        id: 7,
        label: "Contact Us",
        labelAr: "اتصل بنا",
        routeURL: "/home/contactus",
      },
    ],
  },
  {
    id: 1,
    title: "Fraud & Violcations",
    titleAr: "الاحتيال والانتهاكات",
    footerIcon: footerArrow,
    content: [
      {
        id: 1,
        label: "Motor Fraud",
        labelAr: "احتيال محركات",
        routeURL: "/home/customerservice/motorfraud",
      },
      {
        id: 2,
        label: "Medical Fraud",
        labelAr: "احتيال طبي",
        routeURL: "/home/customerservice/medicalfraud",
      },
      {
        id: 3,
        label: "Property & Casuality Fraud",
        labelAr: "احتيال ممتلكات",
        routeURL: "/home/customerservice/travelfraud",
      },
      {
        id: 4,
        label: "Report Violations",
        labelAr: "ابلاغ عن تعدي",
        routeURL: "/home/customerservice/violationspage",
      },
    ],
  },
  {
    id: 2,
    title: "Claim Assistance",
    titleAr: "مستردات التامين",
    footerIcon: footerArrow,
    content: [
      {
        id: 1,
        label: "Motor Insurance Claims",
        labelAr: "تتبع مستردات المحركات",
      },
      {
        id: 2,
        label: "Property & Casualty Claims",
        labelAr: "تتبع مستردات الممتلكات",
      },
      {
        id: 3,
        label: "Medical & Takaful Claims",
        labelAr: "تتبع المستردات الطبية",
      },
      {
        id: 4,
        label: "Track Your Claim",
        labelAr: "تتبع المستردات",
      },
    ],
  },
];

export const bannerMedicalData = [
  {
    fraudTitle: "Medical Fraud",
    fraudPara:
      "Medical insurance fraud is an intentional act by the cardholder,insured, and medical services provider to obtain not owed compensation or benefits to them or others through the deceiving, concealing, and/or misrepresenting information.",
    fraudList: [
      {
        id: 0,
        para: "Using the medical insurance card by someone else.",
      },
      {
        id: 1,
        para: "Mislead to accurately report medical history & pre-existing conditions.",
      },
      {
        id: 2,
        para: "Asking for services, medicines, and/or procedures that are not provided, not covered, or for more expensive treatments or medicines than those prescribed.",
      },
      {
        id: 3,
        para: "Visiting same or multiple hospitals to receive unnecessary or duplicate medicines.",
      },
    ],
    btnLabel: "Report Medical Fraud",
    url: "reportpage",
    bannerPic: fraudBanner,
    fraudBannerImgClass: "medicFraudBanner",
    bannerLayout: "fraudBannerOne",
    serviceRequested: "medicalFraud",
  },
];

export const bannerMotorData = [
  {
    fraudTitle: "Motor Fraud",
    fraudPara:
      "Motor Insurance Fraud is any intentional act committed by the insurance cardholder to obtain undue indemnities or benefits to him or to others through fraud and/ or concealment of required documents and / or misrepresentation of information.",
    fraudList: [
      {
        id: 0,
        para: "Using the motor for an incorrect and false motor accident.",
      },
      {
        id: 1,
        para: "Non-disclosing material facts that changed the accident course of verification.",
      },
      {
        id: 2,
        para: "Causing intentional damages to the motor such as burning or colliding a fixed object.",
      },
      {
        id: 3,
        para: "Replacing the driver with someone who meets the policy requirements.",
      },
      {
        id: 4,
        para: "Notifying the case as motor theft while it was not stolen.",
      },
      {
        id: 5,
        para: "Falsification of official documents to submit the claim.",
      },
    ],
    btnLabel: "Report Motor Fraud",
    url: "reportpage",
    bannerPic: motorFraud,
    fraudBannerImgClass: "motorFraudBanner",
    bannerLayout: "fraudBannerTwo",
    serviceRequested: "motorFraud",
  },
];

export const bannerTravelData = [
  {
    fraudTitle: "Travel Fraud",
    fraudPara:
      "The Company for Cooperative Insurance (Tawuniya) is committed to provide the highest quality services through ethical and fair practices for both its employees and business partners. One of the principal objectives of the company is to protect all parties involved in Property & Casualty insurance from fraud or abuse, which results in negative effects on insurance rates and thus increase the premiums payable by insureds to cover the losses resulting from such fraud. In fact, Property & Casualty insurance fraud operations affect everyone including you. Therefore, you find here all you need to know about Property & Casualty insurance fraud, its definition, FAQ, the difference between fraud and errors, examples of fraud and its consequences, and ways to prevent fraud or abuse of Property & Casualty insurance, and how to report these suspicious operations to Tawuniya.",
    btnLabel: "Report Travel Fraud",
    bannerPic: travelFraud,
    fraudBannerImgClass: "travelFraudBanner",
    bannerLayout: "fraudBannerThree",
    url: "reportpage",
    serviceRequested: "travelFraud",
  },
];

export const legendaryMedicSupportData = [
  {
    heading: "Can't find what you're looking for?",
    para: "Contact the legendary support team right now.",
    legendParaOne:
      "Motor Insurance Fraud is any intentional act committed by the insurance cardholder to obtain undue indemnities or benefits to him or to others through fraud and/ or concealment of required documents and / or misrepresentation of information.",
    legendParaTwo:
      "The Company for Cooperative Insurance (Tawuniya) is committed to provide the highest quality services through ethical and fair practices for both its employees and business partners. One of the principal objectives of the company is to protect all parties involved in Property & Casualty insurance from fraud or abuse, which results in negative effects on insurance rates and thus increase the premiums payable by insureds to cover the losses resulting from such fraud.",
    btnLabelOne: "Report a Motor Fraud",
    btnLabelTwo: "Report a Travel Fraud",
    fraudTitleOne: "Motor Fraud",
    fraudTitleTwo: "Travel Fraud",
    positionLayerTwo: "positionMedic",
  },
];

export const legendaryMotorSupportData = [
  {
    heading: "Can't find what you're looking for?",
    para: "Contact the legendary support team right now.",
    legendParaOne:
      "Motor Insurance Fraud is any intentional act committed by the insurance cardholder to obtain undue indemnities or benefits to him or to others through fraud and/ or concealment of required documents and / or misrepresentation of information.",
    legendParaTwo:
      "The Company for Cooperative Insurance (Tawuniya) is committed to provide the highest quality services through ethical and fair practices for both its employees and business partners. One of the principal objectives of the company is to protect all parties involved in Property & Casualty insurance from fraud or abuse, which results in negative effects on insurance rates and thus increase the premiums payable by insureds to cover the losses resulting from such fraud.",
    btnLabelOne: "Report Motor Fraud",
    btnLabelTwo: "Report a Travel Fraud",
    fraudTitleOne: "Motor Fraud",
    fraudTitleTwo: "Travel Fraud",
    positionLayerTwo: "positionMotor",
  },
];

export const legendaryTravelSupportData = [
  {
    heading: "Can't find what you're looking for?",
    para: "Contact the legendary support team right now.",
    legendParaOne:
      "Motor Insurance Fraud is any intentional act committed by the insurance cardholder to obtain undue indemnities or benefits to him or to others through fraud and/ or concealment of required documents and / or misrepresentation of information.",
    legendParaTwo:
      "The Company for Cooperative Insurance (Tawuniya) is committed to provide the highest quality services through ethical and fair practices for both its employees and business partners. One of the principal objectives of the company is to protect all parties involved in Property & Casualty insurance from fraud or abuse, which results in negative effects on insurance rates and thus increase the premiums payable by insureds to cover the losses resulting from such fraud.",
    btnLabelOne: "Report a Motor Fraud",
    btnLabelTwo: "Report a Travel Fraud",
    fraudTitleOne: "Motor Fraud",
    fraudTitleTwo: "Travel Fraud",
    positionLayerTwo: "positionTravel",
  },
];

export const heathInsureCardData = [
  {
    heading: "Can't find what you're looking for?",
    para: "Contact the legendary support team right now.",
    legendParaOne:
      "Comprehensive protection from risks related to travel outside the Kingdom.",
    legendParaTwo:
      "Get quality car insurance at an affordable price. with Tawuniya, find a car insurance that suits your needs.",
    btnLabelOne: "Buy Now",
    btnLabelTwo: "Buy Now",
    fraudTitleOne: "Health Messaage No.1",
    fraudTitleTwo: "Health Messaage No.2",
    positionLayerTwo: "positionHealth",
  },
];

export const faqMedicData = [
  {
    id: 0,
    question: "What is insurance fraud?",
    answer:
      "It's important to distinguish between Medical insurance fraud/abuse and mistakes. Medical insurance fraud is an act of deception by misrepresentation of material facts, or silence when good faith requires expression, resulting in material damage to one who relies on it and the right to rely on it. Simply, it is obtaining something of value from someone else through deceit. By contrast, mistakes are made unknowingly by anyone who lacks knowledge, in other words, it’s a misstatement or unintentional deviation from the truth. Therefore, Tawuniya always recommend reading the Medical Insurance Guidelines and visiting Tawuniya social media accounts for more information about the ideal use of your Medical Card.",
  },
  {
    id: 1,
    question: "What kind of people/Organizations commits insurance fraud?",
    answer:
      "It's important to distinguish between Medical insurance fraud/abuse and mistakes. Medical insurance fraud is an act of deception by misrepresentation of material facts, or silence when good faith requires expression, resulting in material damage to one who relies on it and the right to rely on it. Simply, it is obtaining something of value from someone else through deceit. By contrast, mistakes are made unknowingly by anyone who lacks knowledge, in other words, it’s a misstatement or unintentional deviation from the truth. Therefore, Tawuniya always recommend reading the Medical Insurance Guidelines and visiting Tawuniya social media accounts for more information about the ideal use of your Medical Card.",
  },
  {
    id: 2,
    question: "What is fraud vs. Mistake?",
    answer:
      "It's important to distinguish between Medical insurance fraud/abuse and mistakes. Medical insurance fraud is an act of deception by misrepresentation of material facts, or silence when good faith requires expression, resulting in material damage to one who relies on it and the right to rely on it. Simply, it is obtaining something of value from someone else through deceit. By contrast, mistakes are made unknowingly by anyone who lacks knowledge, in other words, it’s a misstatement or unintentional deviation from the truth. Therefore, Tawuniya always recommend reading the Medical Insurance Guidelines and visiting Tawuniya social media accounts for more information about the ideal use of your Medical Card.",
  },
  {
    id: 3,
    question: "What are some examples of insurance fraud/Abuse?",
    answer:
      "It's important to distinguish between Medical insurance fraud/abuse and mistakes. Medical insurance fraud is an act of deception by misrepresentation of material facts, or silence when good faith requires expression, resulting in material damage to one who relies on it and the right to rely on it. Simply, it is obtaining something of value from someone else through deceit. By contrast, mistakes are made unknowingly by anyone who lacks knowledge, in other words, it’s a misstatement or unintentional deviation from the truth. Therefore, Tawuniya always recommend reading the Medical Insurance Guidelines and visiting Tawuniya social media accounts for more information about the ideal use of your Medical Card.",
  },
  {
    id: 4,
    question: "How does fraud impact you?",
    answer:
      "It's important to distinguish between Medical insurance fraud/abuse and mistakes. Medical insurance fraud is an act of deception by misrepresentation of material facts, or silence when good faith requires expression, resulting in material damage to one who relies on it and the right to rely on it. Simply, it is obtaining something of value from someone else through deceit. By contrast, mistakes are made unknowingly by anyone who lacks knowledge, in other words, it’s a misstatement or unintentional deviation from the truth. Therefore, Tawuniya always recommend reading the Medical Insurance Guidelines and visiting Tawuniya social media accounts for more information about the ideal use of your Medical Card.",
  },
  {
    id: 5,
    question: "What are we doing to prevent fraud?",
    answer:
      "It's important to distinguish between Medical insurance fraud/abuse and mistakes. Medical insurance fraud is an act of deception by misrepresentation of material facts, or silence when good faith requires expression, resulting in material damage to one who relies on it and the right to rely on it. Simply, it is obtaining something of value from someone else through deceit. By contrast, mistakes are made unknowingly by anyone who lacks knowledge, in other words, it’s a misstatement or unintentional deviation from the truth. Therefore, Tawuniya always recommend reading the Medical Insurance Guidelines and visiting Tawuniya social media accounts for more information about the ideal use of your Medical Card.",
  },
  {
    id: 6,
    question: "What is medical fraud unit?",
    answer:
      "It's important to distinguish between Medical insurance fraud/abuse and mistakes. Medical insurance fraud is an act of deception by misrepresentation of material facts, or silence when good faith requires expression, resulting in material damage to one who relies on it and the right to rely on it. Simply, it is obtaining something of value from someone else through deceit. By contrast, mistakes are made unknowingly by anyone who lacks knowledge, in other words, it’s a misstatement or unintentional deviation from the truth. Therefore, Tawuniya always recommend reading the Medical Insurance Guidelines and visiting Tawuniya social media accounts for more information about the ideal use of your Medical Card.",
  },
  {
    id: 7,
    question: "Shall I get a reward for reporting medical insurance fraud?",
    answer:
      "It's important to distinguish between Medical insurance fraud/abuse and mistakes. Medical insurance fraud is an act of deception by misrepresentation of material facts, or silence when good faith requires expression, resulting in material damage to one who relies on it and the right to rely on it. Simply, it is obtaining something of value from someone else through deceit. By contrast, mistakes are made unknowingly by anyone who lacks knowledge, in other words, it’s a misstatement or unintentional deviation from the truth. Therefore, Tawuniya always recommend reading the Medical Insurance Guidelines and visiting Tawuniya social media accounts for more information about the ideal use of your Medical Card.",
  },
];

export const faqMotorData = [
  {
    id: 0,
    question: "What is insurance fraud?",
    answer:
      "It's important to distinguish between Medical insurance fraud/abuse and mistakes. Medical insurance fraud is an act of deception by misrepresentation of material facts, or silence when good faith requires expression, resulting in material damage to one who relies on it and the right to rely on it. Simply, it is obtaining something of value from someone else through deceit. By contrast, mistakes are made unknowingly by anyone who lacks knowledge, in other words, it’s a misstatement or unintentional deviation from the truth. Therefore, Tawuniya always recommend reading the Medical Insurance Guidelines and visiting Tawuniya social media accounts for more information about the ideal use of your Medical Card.",
  },
  {
    id: 1,
    question: "What kind of people/Organizations commits insurance fraud?",
    answer:
      "It's important to distinguish between Medical insurance fraud/abuse and mistakes. Medical insurance fraud is an act of deception by misrepresentation of material facts, or silence when good faith requires expression, resulting in material damage to one who relies on it and the right to rely on it. Simply, it is obtaining something of value from someone else through deceit. By contrast, mistakes are made unknowingly by anyone who lacks knowledge, in other words, it’s a misstatement or unintentional deviation from the truth. Therefore, Tawuniya always recommend reading the Medical Insurance Guidelines and visiting Tawuniya social media accounts for more information about the ideal use of your Medical Card.",
  },
  {
    id: 2,
    question: "What is fraud vs. Mistake?",
    answer:
      "It's important to distinguish between Medical insurance fraud/abuse and mistakes. Medical insurance fraud is an act of deception by misrepresentation of material facts, or silence when good faith requires expression, resulting in material damage to one who relies on it and the right to rely on it. Simply, it is obtaining something of value from someone else through deceit. By contrast, mistakes are made unknowingly by anyone who lacks knowledge, in other words, it’s a misstatement or unintentional deviation from the truth. Therefore, Tawuniya always recommend reading the Medical Insurance Guidelines and visiting Tawuniya social media accounts for more information about the ideal use of your Medical Card.",
  },
  {
    id: 3,
    question: "What are some examples of insurance fraud/Abuse?",
    answer:
      "It's important to distinguish between Medical insurance fraud/abuse and mistakes. Medical insurance fraud is an act of deception by misrepresentation of material facts, or silence when good faith requires expression, resulting in material damage to one who relies on it and the right to rely on it. Simply, it is obtaining something of value from someone else through deceit. By contrast, mistakes are made unknowingly by anyone who lacks knowledge, in other words, it’s a misstatement or unintentional deviation from the truth. Therefore, Tawuniya always recommend reading the Medical Insurance Guidelines and visiting Tawuniya social media accounts for more information about the ideal use of your Medical Card.",
  },
  {
    id: 4,
    question: "How does fraud impact you?",
    answer:
      "It's important to distinguish between Medical insurance fraud/abuse and mistakes. Medical insurance fraud is an act of deception by misrepresentation of material facts, or silence when good faith requires expression, resulting in material damage to one who relies on it and the right to rely on it. Simply, it is obtaining something of value from someone else through deceit. By contrast, mistakes are made unknowingly by anyone who lacks knowledge, in other words, it’s a misstatement or unintentional deviation from the truth. Therefore, Tawuniya always recommend reading the Medical Insurance Guidelines and visiting Tawuniya social media accounts for more information about the ideal use of your Medical Card.",
  },
  {
    id: 5,
    question: "What are we doing to prevent fraud?",
    answer:
      "It's important to distinguish between Medical insurance fraud/abuse and mistakes. Medical insurance fraud is an act of deception by misrepresentation of material facts, or silence when good faith requires expression, resulting in material damage to one who relies on it and the right to rely on it. Simply, it is obtaining something of value from someone else through deceit. By contrast, mistakes are made unknowingly by anyone who lacks knowledge, in other words, it’s a misstatement or unintentional deviation from the truth. Therefore, Tawuniya always recommend reading the Medical Insurance Guidelines and visiting Tawuniya social media accounts for more information about the ideal use of your Medical Card.",
  },
  {
    id: 6,
    question: "What is medical fraud unit?",
    answer:
      "It's important to distinguish between Medical insurance fraud/abuse and mistakes. Medical insurance fraud is an act of deception by misrepresentation of material facts, or silence when good faith requires expression, resulting in material damage to one who relies on it and the right to rely on it. Simply, it is obtaining something of value from someone else through deceit. By contrast, mistakes are made unknowingly by anyone who lacks knowledge, in other words, it’s a misstatement or unintentional deviation from the truth. Therefore, Tawuniya always recommend reading the Medical Insurance Guidelines and visiting Tawuniya social media accounts for more information about the ideal use of your Medical Card.",
  },
  {
    id: 7,
    question: "Shall I get a reward for reporting medical insurance fraud?",
    answer:
      "It's important to distinguish between Medical insurance fraud/abuse and mistakes. Medical insurance fraud is an act of deception by misrepresentation of material facts, or silence when good faith requires expression, resulting in material damage to one who relies on it and the right to rely on it. Simply, it is obtaining something of value from someone else through deceit. By contrast, mistakes are made unknowingly by anyone who lacks knowledge, in other words, it’s a misstatement or unintentional deviation from the truth. Therefore, Tawuniya always recommend reading the Medical Insurance Guidelines and visiting Tawuniya social media accounts for more information about the ideal use of your Medical Card.",
  },
];

export const faqTravelData = [
  {
    id: 0,
    question: "What is insurance fraud?",
    answer:
      "It's important to distinguish between Medical insurance fraud/abuse and mistakes. Medical insurance fraud is an act of deception by misrepresentation of material facts, or silence when good faith requires expression, resulting in material damage to one who relies on it and the right to rely on it. Simply, it is obtaining something of value from someone else through deceit. By contrast, mistakes are made unknowingly by anyone who lacks knowledge, in other words, it’s a misstatement or unintentional deviation from the truth. Therefore, Tawuniya always recommend reading the Medical Insurance Guidelines and visiting Tawuniya social media accounts for more information about the ideal use of your Medical Card.",
  },
  {
    id: 1,
    question: "What kind of people/Organizations commits insurance fraud?",
    answer:
      "It's important to distinguish between Medical insurance fraud/abuse and mistakes. Medical insurance fraud is an act of deception by misrepresentation of material facts, or silence when good faith requires expression, resulting in material damage to one who relies on it and the right to rely on it. Simply, it is obtaining something of value from someone else through deceit. By contrast, mistakes are made unknowingly by anyone who lacks knowledge, in other words, it’s a misstatement or unintentional deviation from the truth. Therefore, Tawuniya always recommend reading the Medical Insurance Guidelines and visiting Tawuniya social media accounts for more information about the ideal use of your Medical Card.",
  },
  {
    id: 2,
    question: "What is fraud vs. Mistake?",
    answer:
      "It's important to distinguish between Medical insurance fraud/abuse and mistakes. Medical insurance fraud is an act of deception by misrepresentation of material facts, or silence when good faith requires expression, resulting in material damage to one who relies on it and the right to rely on it. Simply, it is obtaining something of value from someone else through deceit. By contrast, mistakes are made unknowingly by anyone who lacks knowledge, in other words, it’s a misstatement or unintentional deviation from the truth. Therefore, Tawuniya always recommend reading the Medical Insurance Guidelines and visiting Tawuniya social media accounts for more information about the ideal use of your Medical Card.",
  },
  {
    id: 3,
    question: "What are some examples of insurance fraud/Abuse?",
    answer:
      "It's important to distinguish between Medical insurance fraud/abuse and mistakes. Medical insurance fraud is an act of deception by misrepresentation of material facts, or silence when good faith requires expression, resulting in material damage to one who relies on it and the right to rely on it. Simply, it is obtaining something of value from someone else through deceit. By contrast, mistakes are made unknowingly by anyone who lacks knowledge, in other words, it’s a misstatement or unintentional deviation from the truth. Therefore, Tawuniya always recommend reading the Medical Insurance Guidelines and visiting Tawuniya social media accounts for more information about the ideal use of your Medical Card.",
  },
  {
    id: 4,
    question: "How does fraud impact you?",
    answer:
      "It's important to distinguish between Medical insurance fraud/abuse and mistakes. Medical insurance fraud is an act of deception by misrepresentation of material facts, or silence when good faith requires expression, resulting in material damage to one who relies on it and the right to rely on it. Simply, it is obtaining something of value from someone else through deceit. By contrast, mistakes are made unknowingly by anyone who lacks knowledge, in other words, it’s a misstatement or unintentional deviation from the truth. Therefore, Tawuniya always recommend reading the Medical Insurance Guidelines and visiting Tawuniya social media accounts for more information about the ideal use of your Medical Card.",
  },
  {
    id: 5,
    question: "What are we doing to prevent fraud?",
    answer:
      "It's important to distinguish between Medical insurance fraud/abuse and mistakes. Medical insurance fraud is an act of deception by misrepresentation of material facts, or silence when good faith requires expression, resulting in material damage to one who relies on it and the right to rely on it. Simply, it is obtaining something of value from someone else through deceit. By contrast, mistakes are made unknowingly by anyone who lacks knowledge, in other words, it’s a misstatement or unintentional deviation from the truth. Therefore, Tawuniya always recommend reading the Medical Insurance Guidelines and visiting Tawuniya social media accounts for more information about the ideal use of your Medical Card.",
  },
  {
    id: 6,
    question: "What is medical fraud unit?",
    answer:
      "It's important to distinguish between Medical insurance fraud/abuse and mistakes. Medical insurance fraud is an act of deception by misrepresentation of material facts, or silence when good faith requires expression, resulting in material damage to one who relies on it and the right to rely on it. Simply, it is obtaining something of value from someone else through deceit. By contrast, mistakes are made unknowingly by anyone who lacks knowledge, in other words, it’s a misstatement or unintentional deviation from the truth. Therefore, Tawuniya always recommend reading the Medical Insurance Guidelines and visiting Tawuniya social media accounts for more information about the ideal use of your Medical Card.",
  },
  {
    id: 7,
    question: "Shall I get a reward for reporting medical insurance fraud?",
    answer:
      "It's important to distinguish between Medical insurance fraud/abuse and mistakes. Medical insurance fraud is an act of deception by misrepresentation of material facts, or silence when good faith requires expression, resulting in material damage to one who relies on it and the right to rely on it. Simply, it is obtaining something of value from someone else through deceit. By contrast, mistakes are made unknowingly by anyone who lacks knowledge, in other words, it’s a misstatement or unintentional deviation from the truth. Therefore, Tawuniya always recommend reading the Medical Insurance Guidelines and visiting Tawuniya social media accounts for more information about the ideal use of your Medical Card.",
  },
];

export const faqContainerMedicData = [
  {
    serviceTitle: "Report Medical Fraud",
    servicePara:
      "An e-service provided by Tawuniya enables the beneficiary to submit complaints related to fraud. The team going to research, study, and analysis of the report pertaining to fraud, technically and legally.",
  },
];

export const faqCategoryData = [
  {
    id: 0,
    question: "What is medical insurance?",
    answer:
      "It's important to distinguish between Medical insurance fraud/abuse and mistakes. Medical insurance fraud is an act of deception by misrepresentation of material facts, or silence when good faith requires expression, resulting in material damage to one who relies on it and the right to rely on it. Simply, it is obtaining something of value from someone else through deceit. By contrast, mistakes are made unknowingly by anyone who lacks knowledge, in other words, it’s a misstatement or unintentional deviation from the truth. Therefore, Tawuniya always recommend reading the Medical Insurance Guidelines and visiting Tawuniya social media accounts for more information about the ideal use of your Medical Card.",
  },
  {
    id: 1,
    question: "How much does medical insurance cost?",
    answer:
      "It's important to distinguish between Medical insurance fraud/abuse and mistakes. Medical insurance fraud is an act of deception by misrepresentation of material facts, or silence when good faith requires expression, resulting in material damage to one who relies on it and the right to rely on it. Simply, it is obtaining something of value from someone else through deceit. By contrast, mistakes are made unknowingly by anyone who lacks knowledge, in other words, it’s a misstatement or unintentional deviation from the truth. Therefore, Tawuniya always recommend reading the Medical Insurance Guidelines and visiting Tawuniya social media accounts for more information about the ideal use of your Medical Card.",
  },
  {
    id: 2,
    question: "How can I obtain medical insurance?",
    answer:
      "It's important to distinguish between Medical insurance fraud/abuse and mistakes. Medical insurance fraud is an act of deception by misrepresentation of material facts, or silence when good faith requires expression, resulting in material damage to one who relies on it and the right to rely on it. Simply, it is obtaining something of value from someone else through deceit. By contrast, mistakes are made unknowingly by anyone who lacks knowledge, in other words, it’s a misstatement or unintentional deviation from the truth. Therefore, Tawuniya always recommend reading the Medical Insurance Guidelines and visiting Tawuniya social media accounts for more information about the ideal use of your Medical Card.",
  },
  {
    id: 3,
    question: "What are the required documents to acquire medical insurance?",
    answer:
      "It's important to distinguish between Medical insurance fraud/abuse and mistakes. Medical insurance fraud is an act of deception by misrepresentation of material facts, or silence when good faith requires expression, resulting in material damage to one who relies on it and the right to rely on it. Simply, it is obtaining something of value from someone else through deceit. By contrast, mistakes are made unknowingly by anyone who lacks knowledge, in other words, it’s a misstatement or unintentional deviation from the truth. Therefore, Tawuniya always recommend reading the Medical Insurance Guidelines and visiting Tawuniya social media accounts for more information about the ideal use of your Medical Card.",
  },
  {
    id: 4,
    question: "What are the required documents to acquire medical insurance?",
    answer:
      "It's important to distinguish between Medical insurance fraud/abuse and mistakes. Medical insurance fraud is an act of deception by misrepresentation of material facts, or silence when good faith requires expression, resulting in material damage to one who relies on it and the right to rely on it. Simply, it is obtaining something of value from someone else through deceit. By contrast, mistakes are made unknowingly by anyone who lacks knowledge, in other words, it’s a misstatement or unintentional deviation from the truth. Therefore, Tawuniya always recommend reading the Medical Insurance Guidelines and visiting Tawuniya social media accounts for more information about the ideal use of your Medical Card.",
  },
  {
    id: 5,
    question: "How can I obtain medical insurance?",
    answer:
      "It's important to distinguish between Medical insurance fraud/abuse and mistakes. Medical insurance fraud is an act of deception by misrepresentation of material facts, or silence when good faith requires expression, resulting in material damage to one who relies on it and the right to rely on it. Simply, it is obtaining something of value from someone else through deceit. By contrast, mistakes are made unknowingly by anyone who lacks knowledge, in other words, it’s a misstatement or unintentional deviation from the truth. Therefore, Tawuniya always recommend reading the Medical Insurance Guidelines and visiting Tawuniya social media accounts for more information about the ideal use of your Medical Card.",
  },
  {
    id: 6,
    question: "How much does medical insurance cost?",
    answer:
      "It's important to distinguish between Medical insurance fraud/abuse and mistakes. Medical insurance fraud is an act of deception by misrepresentation of material facts, or silence when good faith requires expression, resulting in material damage to one who relies on it and the right to rely on it. Simply, it is obtaining something of value from someone else through deceit. By contrast, mistakes are made unknowingly by anyone who lacks knowledge, in other words, it’s a misstatement or unintentional deviation from the truth. Therefore, Tawuniya always recommend reading the Medical Insurance Guidelines and visiting Tawuniya social media accounts for more information about the ideal use of your Medical Card.",
  },
  {
    id: 7,
    question: "What is medical insurance?",
    answer:
      "It's important to distinguish between Medical insurance fraud/abuse and mistakes. Medical insurance fraud is an act of deception by misrepresentation of material facts, or silence when good faith requires expression, resulting in material damage to one who relies on it and the right to rely on it. Simply, it is obtaining something of value from someone else through deceit. By contrast, mistakes are made unknowingly by anyone who lacks knowledge, in other words, it’s a misstatement or unintentional deviation from the truth. Therefore, Tawuniya always recommend reading the Medical Insurance Guidelines and visiting Tawuniya social media accounts for more information about the ideal use of your Medical Card.",
  },
];

export const faqCategoryDataAgentsPage = [
  {
    id: 0,
    question: "What is insurance fraud?",
    answer:
      "It's important to distinguish between Medical insurance fraud/abuse and mistakes. Medical insurance fraud is an act of deception by misrepresentation of material facts, or silence when good faith requires expression, resulting in material damage to one who relies on it and the right to rely on it. Simply, it is obtaining something of value from someone else through deceit. By contrast, mistakes are made unknowingly by anyone who lacks knowledge, in other words, it’s a misstatement or unintentional deviation from the truth. Therefore, Tawuniya always recommend reading the Medical Insurance Guidelines and visiting Tawuniya social media accounts for more information about the ideal use of your Medical Card.",
  },
  {
    id: 1,
    question: "What kind of people/Organizations commits insurance fraud?",
    answer:
      "It's important to distinguish between Medical insurance fraud/abuse and mistakes. Medical insurance fraud is an act of deception by misrepresentation of material facts, or silence when good faith requires expression, resulting in material damage to one who relies on it and the right to rely on it. Simply, it is obtaining something of value from someone else through deceit. By contrast, mistakes are made unknowingly by anyone who lacks knowledge, in other words, it’s a misstatement or unintentional deviation from the truth. Therefore, Tawuniya always recommend reading the Medical Insurance Guidelines and visiting Tawuniya social media accounts for more information about the ideal use of your Medical Card.",
  },
  {
    id: 2,
    question: "What is fraud vs. Mistake?",
    answer:
      "It's important to distinguish between Medical insurance fraud/abuse and mistakes. Medical insurance fraud is an act of deception by misrepresentation of material facts, or silence when good faith requires expression, resulting in material damage to one who relies on it and the right to rely on it. Simply, it is obtaining something of value from someone else through deceit. By contrast, mistakes are made unknowingly by anyone who lacks knowledge, in other words, it’s a misstatement or unintentional deviation from the truth. Therefore, Tawuniya always recommend reading the Medical Insurance Guidelines and visiting Tawuniya social media accounts for more information about the ideal use of your Medical Card.",
  },
  {
    id: 3,
    question: "What are some examples of insurance fraud/Abuse?",
    answer:
      "It's important to distinguish between Medical insurance fraud/abuse and mistakes. Medical insurance fraud is an act of deception by misrepresentation of material facts, or silence when good faith requires expression, resulting in material damage to one who relies on it and the right to rely on it. Simply, it is obtaining something of value from someone else through deceit. By contrast, mistakes are made unknowingly by anyone who lacks knowledge, in other words, it’s a misstatement or unintentional deviation from the truth. Therefore, Tawuniya always recommend reading the Medical Insurance Guidelines and visiting Tawuniya social media accounts for more information about the ideal use of your Medical Card.",
  },
  {
    id: 4,
    question: "How does fraud impact you?",
    answer:
      "It's important to distinguish between Medical insurance fraud/abuse and mistakes. Medical insurance fraud is an act of deception by misrepresentation of material facts, or silence when good faith requires expression, resulting in material damage to one who relies on it and the right to rely on it. Simply, it is obtaining something of value from someone else through deceit. By contrast, mistakes are made unknowingly by anyone who lacks knowledge, in other words, it’s a misstatement or unintentional deviation from the truth. Therefore, Tawuniya always recommend reading the Medical Insurance Guidelines and visiting Tawuniya social media accounts for more information about the ideal use of your Medical Card.",
  },
  {
    id: 5,
    question: "What are we doing to prevent fraud?",
    answer:
      "It's important to distinguish between Medical insurance fraud/abuse and mistakes. Medical insurance fraud is an act of deception by misrepresentation of material facts, or silence when good faith requires expression, resulting in material damage to one who relies on it and the right to rely on it. Simply, it is obtaining something of value from someone else through deceit. By contrast, mistakes are made unknowingly by anyone who lacks knowledge, in other words, it’s a misstatement or unintentional deviation from the truth. Therefore, Tawuniya always recommend reading the Medical Insurance Guidelines and visiting Tawuniya social media accounts for more information about the ideal use of your Medical Card.",
  },
  {
    id: 6,
    question: "What is medical fraud unit?",
    answer:
      "It's important to distinguish between Medical insurance fraud/abuse and mistakes. Medical insurance fraud is an act of deception by misrepresentation of material facts, or silence when good faith requires expression, resulting in material damage to one who relies on it and the right to rely on it. Simply, it is obtaining something of value from someone else through deceit. By contrast, mistakes are made unknowingly by anyone who lacks knowledge, in other words, it’s a misstatement or unintentional deviation from the truth. Therefore, Tawuniya always recommend reading the Medical Insurance Guidelines and visiting Tawuniya social media accounts for more information about the ideal use of your Medical Card.",
  },
  {
    id: 7,
    question: "Shall I get a reward for reporting medical insurance fraud?",
    answer:
      "It's important to distinguish between Medical insurance fraud/abuse and mistakes. Medical insurance fraud is an act of deception by misrepresentation of material facts, or silence when good faith requires expression, resulting in material damage to one who relies on it and the right to rely on it. Simply, it is obtaining something of value from someone else through deceit. By contrast, mistakes are made unknowingly by anyone who lacks knowledge, in other words, it’s a misstatement or unintentional deviation from the truth. Therefore, Tawuniya always recommend reading the Medical Insurance Guidelines and visiting Tawuniya social media accounts for more information about the ideal use of your Medical Card.",
  },
];

export const faqContainerMotorData = [
  {
    serviceTitle: "Report Motor Fraud",
    servicePara:
      "An e-service provided by Tawuniya enables the beneficiary to submit complaints related to fraud. The team going to research, study, and analysis of the report pertaining to fraud, technically and legally.",
  },
];

export const faqContainerTravelData = [
  {
    serviceTitle: "Report Travel Fraud",
    servicePara:
      "An e-service provided by Tawuniya enables the beneficiary to submit complaints related to fraud. The team going to research, study, and analysis of the report pertaining to fraud, technically and legally.",
  },
];

export const associateMedicData = [
  {
    id: 0,
    liningClass: "associateLining pr-lg-3",
    associateTitleClass: "associateTitle",
    associateTitle: "The Negative Impacts of Medical Fraud",
    associatePara: [
      {
        id: 0,
        para: "Higher medical insurance premiums to meet the increase in the loss ratio.",
        associateParaClass: "associateParaOne",
      },
      {
        id: 1,
        para: "Stop issuing medical insurance policies to reduce fraud claims and loss of access to insurance coverage.",
        associateParaClass: "associateParaOne",
      },
      {
        id: 2,
        para: "Cancellation of the insurance policy.",
        associateParaClass: "associateParaOne",
      },
      {
        id: 3,
        para: "Cease of dealing with medical service providers and thus deprive patients of distinctive treatment services.",
        associateParaClass: "associateParaOne",
      },
      {
        id: 4,
        para: "Business financial losses resulting from fraudulent claims.",
        associateParaClass: "associateParaOne",
      },
      {
        id: 5,
        para: "Claim rejection.",
        associateParaClass: "associateParaOne",
      },
      {
        id: 6,
        para: "Lawful actions.",
        associateParaClass: "associateParaOne",
      },
      {
        id: 7,
        para: "Health harm to the insured due to tampering with his medical history if the medical card is used by others",
        associateParaClass: "associateParaOne",
      },
    ],
  },
  {
    id: 1,
    liningClass: "associateLining pl-lg-4 pr-lg-3",
    associateTitleClass: "associateTitle",
    associateTitle: "Medical Fraud Detection Methods",
    associatePara: [
      {
        id: 0,
        para: "A dedicated Medical Fraud Unit.",
        associateParaClass: "associateParaOne",
      },
      {
        id: 1,
        para: "Applying fraud control systems",
        associateParaClass: "associateParaOne",
      },
      {
        id: 2,
        para: "Cooperation with regulators such as CCHI and/or SAMA.",
        associateParaClass: "associateParaOne",
      },
      {
        id: 3,
        para: "A toll-free hotline which you can call to report suspected fraud.",
        associateParaClass: "associateParaOne",
      },
      {
        id: 4,
        para: "A special Fraud reporting Email monitored by Fraud Unit Team",
        associateParaClass: "associateParaOne",
      },
    ],
  },
  {
    id: 2,
    liningClass: "associateNoLining pl-lg-4",
    associateTitleClass: "associateTitle",
    associateTitle: "Your responsibilities to prevent fraud",
    associatePara: [
      {
        id: 0,
        para: "Be aware of attempts by medical providers and others to convince you that everybody else is profiting so you may as well try to reap the benefits of insurance fraud.",
        associateParaClass: "associateParaNoLining",
      },
      {
        id: 1,
        para: "To avoid unwanted medical procedures, always ask the doctor about the provided services and why you need them.",
        associateParaClass: "associateParaNoLining",
      },
      {
        id: 2,
        para: "Do not allow anyone to use your medical card.",
        associateParaClass: "associateParaNoLining",
      },
      {
        id: 3,
        para: "Carefully review your claims & bills before signing them, and question charges for procedures that were not provided.",
        associateParaClass: "associateParaNoLining",
      },
      {
        id: 4,
        para: "Inform and call Tawuniya when you detect or suspect fraud.",
        associateParaClass: "associateParaNoLining",
      },
    ],
  },
];

export const associateMotorData = [
  {
    id: 0,
    liningClass: "associateLining pr-lg-0",
    associateTitleClass: "associateTitle",
    associateTitle: "The Negative Impacts of motor Fraud",
    associatePara: [
      {
        id: 0,
        para: "High premiums.",
        associateParaClass: "associateParaOne",
      },
      {
        id: 1,
        para: "Cancellation of insurance policy.",
        associateParaClass: "associateParaOne",
      },
      {
        id: 2,
        para: "Cancellation of retirement with the service provider (agency or repair shop) if it is the source of fraud.",
        associateParaClass: "associateParaOne",
      },
      {
        id: 3,
        para: "Not receiving compensation.",
        associateParaClass: "associateParaOne",
      },
      {
        id: 4,
        para: "Taking legal actions as opposed to the violator.",
        associateParaClass: "associateParaOne",
      },
      {
        id: 5,
        para: "Take action to impose legal sanctions from the competent authorities.",
        associateParaClass: "associateParaOne",
      },
    ],
  },
  {
    id: 1,
    liningClass: "associateLining pl-lg-4 pr-lg-3",
    associateTitleClass: "associateTitle",
    associateTitle: "Fraud detection mechanisms in motor insurance",
    associatePara: [
      {
        id: 0,
        para: "The presence of a unit specialized in fraud in the insurance of motors.",
        associateParaClass: "associateParaOne",
      },
      {
        id: 1,
        para: "Implement an information system for fraud detection and control.",
        associateParaClass: "associateParaOne",
      },
      {
        id: 2,
        para: "Cooperating with the competent authorities to manage and supervise the insurance of motors such as Al Muroor, Najm Company and SAMA.",
        associateParaClass: "associateParaOne",
      },
      {
        id: 3,
        para: "Assign a fraud reporting email managed by the team of the competent anti-fraud unit.",
        associateParaClass: "associateParaOne",
      },
    ],
  },
  {
    id: 2,
    liningClass: "associateNoLining pl-lg-4",
    associateTitleClass: "associateTitle",
    associateTitle: "Your role in preventing motor insurance fraud",
    associatePara: [
      {
        id: 0,
        para: "Inform the Competent Authority (Najm Company) of any manipulation in the description of the accident, or the replacement of the driver or any means of fraud.",
        associateParaClass: "associateParaNoLining",
      },
      {
        id: 1,
        para: "Not colluding with the driver causing the accident to transfer responsibility to the person holding the insurance policy.",
        associateParaClass: "associateParaNoLining",
      },
      {
        id: 2,
        para: "Immediately notifying the company when fraud has been discovered or suspected by any party.",
        associateParaClass: "associateParaNoLining",
      },
    ],
  },
];

export const associateTravelData = [
  {
    id: 0,
    liningClass: "associateLining pr-lg-3",
    associateTitleClass: "associateTitle-travel",
    associateTitle: "Definition of Property & Casualty Insurance Fraud",
    associatePara: [
      {
        id: 0,
        para: "Property & Casualty insurance fraud is any intentional act committed by one of the parties in the insurance contract to obtain undue indemnities or benefits to them or to others through deception and/or concealment of required documents and/or misrepresentation of information.",
        associateParaClass: "associateParaTwo",
        needList: true,
      },
    ],
  },
  {
    id: 1,
    liningClass: "associateLining pl-lg-4 pr-lg-3",
    associateTitleClass: "associateTitle-travel",
    associateTitle: "Examples of Property & Casualty Insurance Fraud",
    associatePara: [
      {
        id: 0,
        para: "Misrepresentation of material facts might occur if an insurance proposer makes a false statement with the intention to deceive the insurer in order to obtain an unlawful gain, e.g. a discount on premium.",
        associateParaClass: "associateParaTwo",
      },
      {
        id: 1,
        para: "Inflated Damage/Loss of property. This may occur after a fire incident and an insurance claim gets filed.",
        associateParaClass: "associateParaTwo",
      },
      {
        id: 2,
        para: "Arson for Profit, dwellings or commercial properties are destroyed by fire for the sole purpose of financial gain.",
        associateParaClass: "associateParaTwo",
      },
      {
        id: 3,
        para: "A claim is filed for a boat that sank, but the boat never actually existed. It is not difficult to register a boat based on a bill of sale.",
        associateParaClass: "associateParaTwo",
      },
    ],
  },
  {
    id: 2,
    liningClass: "associateNoLining pl-lg-4",
    associateTitleClass: "associateTitle-travel",
    associateTitle: "Negative Effects of Property & Casualty Insurance Fraud",
    associatePara: [
      {
        id: 0,
        para: "High premiums.",
        associateParaClass: "associateParaNoLining",
      },
      {
        id: 1,
        para: "Claim process duration increased due to investigations.",
        associateParaClass: "associateParaNoLining",
      },
      {
        id: 2,
        para: "Increase the deductible.",
        associateParaClass: "associateParaNoLining",
      },
    ],
  },
];

export const associateTravelNewData = [
  {
    id: 0,
    liningClass: "associateLining pr-lg-3",
    associateTitleClass: "associateTitle-travel",
    associateTitle:
      "Mechanisms of Detecting Property & Casualty Insurance Fraud",
    associatePara: [
      {
        id: 0,
        para: "Application of a sophisticated fraud control information system.",
        associateParaClass: "associateParaOne",
      },
      {
        id: 1,
        para: "Cooperation with regulatory bodies such the Saudi Arabian Monetary Authority (SAMA).",
        associateParaClass: "associateParaOne",
      },
      {
        id: 2,
        para: "Designation of a special email to report fraud managed by the team of the anti-fraud unit.",
        associateParaClass: "associateParaOne",
      },
    ],
  },
  {
    id: 1,
    liningClass: "associateLining pl-lg-4 pr-lg-3",
    associateTitleClass: "associateTitle-travel",
    associateTitle: "Preventing Property and Casualty Insurance Fraud",
    associatePara: [
      {
        id: 0,
        para: "As a valuable member and a key source to help us deal with fraud in Property &Casualty insurance, you can participate in preventing and combating these fraudulent operations by reporting any suspicious acts through the below available channels.",
        associateParaClass: "associateParaThree",
        needList: true,
      },
    ],
  },
  {
    id: 2,
    liningClass: "associateNoLining pl-lg-4",
    associateTitleClass: "associateTitle-travel",
    associateTitle:
      "Rewards for Reporting Property and Casualty Insurance Fraud",
    associatePara: [
      {
        id: 0,
        para: "Tawuniya pays a financial reward to the individuals who report proved and confirmed frauds in accordance with the Company's approved policies and procedures.",
        associateParaClass: "associateParaNoLiningNew",
        needList: true,
      },
    ],
  },
  {
    id: 0,
    liningClass: "associateLining pr-lg-3",
    associateTitleClass: "associateTitle-travel",
    associateTitle:
      "Mechanisms of Detecting Property & Casualty Insurance Fraud",
    associatePara: [
      {
        id: 0,
        para: "Application of a sophisticated fraud control information system.",
        associateParaClass: "associateParaOne",
      },
      {
        id: 1,
        para: "Cooperation with regulatory bodies such the Saudi Arabian Monetary Authority (SAMA).",
        associateParaClass: "associateParaOne",
      },
      {
        id: 2,
        para: "Designation of a special email to report fraud managed by the team of the anti-fraud unit.",
        associateParaClass: "associateParaOne",
      },
    ],
  },
  {
    id: 1,
    liningClass: "associateLining pl-lg-4 pr-lg-3",
    associateTitleClass: "associateTitle-travel",
    associateTitle: "Preventing Property and Casualty Insurance Fraud",
    associatePara: [
      {
        id: 0,
        para: "As a valuable member and a key source to help us deal with fraud in Property &Casualty insurance, you can participate in preventing and combating these fraudulent operations by reporting any suspicious acts through the below available channels.",
        associateParaClass: "associateParaThree",
        needList: true,
      },
    ],
  },
  {
    id: 2,
    liningClass: "associateNoLining pl-lg-4",
    associateTitleClass: "associateTitle-travel",
    associateTitle:
      "Rewards for Reporting Property and Casualty Insurance Fraud",
    associatePara: [
      {
        id: 0,
        para: "Tawuniya pays a financial reward to the individuals who report proved and confirmed frauds in accordance with the Company's approved policies and procedures.",
        associateParaClass: "associateParaNoLiningNew",
        needList: true,
      },
    ],
  },
];

export const serviceData = [
  {
    id: 0,
    title: "Road Side Assistance",
    content: "Our services can be trustable, honest and worthy",
    contentAlignClass: "pb-5",
    imgOne: serviceIconOne,
    imgTwo: serviceArrow,
  },
  {
    id: 1,
    title: "Request Telemedicine",
    content: "Our services can be trustable, honest.",
    contentAlignClass: "pb-5",
    imgOne: telemed,
    imgTwo: serviceArrow,
  },
  {
    id: 2,
    title: "Report a Theft",
    content: "Our services can be trustable, honest and worthy",
    contentAlignClass: "pb-5",
    imgOne: theft,
    imgTwo: serviceArrow,
  },
  {
    id: 3,
    title: "Flight Delay Claim",
    content: "Our services can be trustable, honest and worthy",
    contentAlignClass: "pb-5",
    imgOne: flight,
    imgTwo: serviceArrow,
  },
  {
    id: 4,
    title: "Assist in Accident",
    content: "Our services can be trustable, honest and worthy",
    contentAlignClass: "pb-5",
    imgOne: accident,
    imgTwo: serviceArrow,
  },
  {
    id: 5,
    title: "Assist in medical",
    content: "Our services can be trustable, honest and worthy",
    contentAlignClass: "pb-5",
    imgOne: medAssisst,
    imgTwo: serviceArrow,
  },
  {
    id: 6,
    title: "Report a Theft",
    content: "Our services can be trustable, honest and worthy",
    contentAlignClass: "pb-5",
    imgOne: theft,
    imgTwo: serviceArrow,
  },
  {
    id: 7,
    title: "Assist in Accident",
    content: "Our services can be trustable, honest and worthy",
    contentAlignClass: "pb-5",
    imgOne: accident,
    imgTwo: serviceArrow,
  },
  {
    id: 8,
    title: "Assist in medical",
    content: "Our services can be trustable, honest and worthy",
    contentAlignClass: "pb-5",
    imgOne: medAssisst,
    imgTwo: serviceArrow,
  },
  {
    id: 9,
    title: "Request Telemedicine",
    content: "Our services can be trustable, honest.",
    contentAlignClass: "pb-5",
    imgOne: telemed,
    imgTwo: serviceArrow,
  },
  {
    id: 10,
    title: "Road Side Assistance",
    content: "Our services can be trustable, honest and worthy",
    contentAlignClass: "pb-5",
    imgOne: serviceIconOne,
    imgTwo: serviceArrow,
  },
  {
    id: 11,
    title: "Flight Delay Claim",
    content: "Our services can be trustable, honest and worthy",
    contentAlignClass: "pb-5",
    imgOne: flight,
    imgTwo: serviceArrow,
  },
];

export const serviceSupportData = [
  {
    id: 0,
    title: "Road Side Assistance",
    content: "Our services can be trustable, honest and worthy",
    contentAlignClass: "pb-5",
    imgOne: serviceIconOne,
    imgTwo: serviceArrow,
  },
  {
    id: 1,
    title: "Request Telemedicine",
    content: "Our services can be trustable, honest.",
    contentAlignClass: "pb-5",
    imgOne: telemed,
    imgTwo: serviceArrow,
  },
  {
    id: 2,
    title: "Report a Theft",
    content: "Our services can be trustable, honest and worthy",
    contentAlignClass: "pb-5",
    imgOne: theft,
    imgTwo: serviceArrow,
  },
  {
    id: 3,
    title: "Flight Delay Claim",
    content: "Our services can be trustable, honest and worthy",
    contentAlignClass: "pb-5",
    imgOne: flight,
    imgTwo: serviceArrow,
  },
  {
    id: 4,
    title: "Report a Theft",
    content: "Our services can be trustable, honest and worthy",
    contentAlignClass: "pb-5",
    imgOne: theft,
    imgTwo: serviceArrow,
  },
  {
    id: 5,
    title: "Assist in Accident",
    content: "Our services can be trustable, honest and worthy",
    contentAlignClass: "pb-5",
    imgOne: accident,
    imgTwo: serviceArrow,
  },
  {
    id: 6,
    title: "Road Side Assistance",
    content: "Our services can be trustable, honest and worthy",
    contentAlignClass: "pb-5",
    imgOne: serviceIconOne,
    imgTwo: serviceArrow,
  },
  {
    id: 7,
    title: "Flight Delay Claim",
    content: "Our services can be trustable, honest and worthy",
    contentAlignClass: "pb-5",
    imgOne: flight,
    imgTwo: serviceArrow,
  },
];

export const customerLandingData = [
  {
    id: 0,
    title: "Road Side Assistance",
    content: "Our services can be trustable, honest and worthy",
    contentAlignClass: "",
    imgOne: serviceIconOne,
    imgTwo: serviceArrow,
  },
  {
    id: 1,
    title: "Request Telemedicine",
    content: "Our services can be trustable, honest.",
    contentAlignClass: "",
    imgOne: telemed,
    imgTwo: serviceArrow,
  },
  {
    id: 2,
    title: "Report a Theft",
    content: "Our services can be trustable, honest and worthy",
    contentAlignClass: "",
    imgOne: theft,
    imgTwo: serviceArrow,
  },
  {
    id: 3,
    title: "Flight Delay Claim",
    content: "Our services can be trustable, honest and worthy",
    contentAlignClass: "",
    imgOne: flight,
    imgTwo: serviceArrow,
  },
  {
    id: 4,
    title: "Assist in Accident",
    content: "Our services can be trustable, honest and worthy",
    contentAlignClass: "",
    imgOne: accident,
    imgTwo: serviceArrow,
  },
  {
    id: 5,
    title: "Assist in medical",
    content: "Our services can be trustable, honest and worthy",
    contentAlignClass: "",
    imgOne: medAssisst,
    imgTwo: serviceArrow,
  },
  {
    id: 6,
    title: "Report a Theft",
    content: "Our services can be trustable, honest and worthy",
    contentAlignClass: "",
    imgOne: theft,
    imgTwo: serviceArrow,
  },
  {
    id: 7,
    title: "Assist in Accident",
    content: "Our services can be trustable, honest and worthy",
    contentAlignClass: "",
    imgOne: accident,
    imgTwo: serviceArrow,
  },
  {
    id: 8,
    title: "Assist in medical",
    content: "Our services can be trustable, honest and worthy",
    contentAlignClass: "",
    imgOne: medAssisst,
    imgTwo: serviceArrow,
  },
  {
    id: 9,
    title: "Request Telemedicine",
    content: "Our services can be trustable, honest.",
    contentAlignClass: "",
    imgOne: telemed,
    imgTwo: serviceArrow,
  },
  {
    id: 10,
    title: "Road Side Assistance",
    content: "Our services can be trustable, honest and worthy",
    contentAlignClass: "",
    imgOne: serviceIconOne,
    imgTwo: serviceArrow,
  },
  {
    id: 11,
    title: "Flight Delay Claim",
    content: "Our services can be trustable, honest and worthy",
    contentAlignClass: "",
    imgOne: flight,
    imgTwo: serviceArrow,
  },
];

export const serviceCategoryData = [
  {
    id: 0,
    title: "Road Side Assistance",
    content: "Our services can be trustable, honest and worthy",
    contentAlignClass: "pb-5",
    imgOne: serviceIconOne,
    imgTwo: serviceArrow,
  },
  {
    id: 1,
    title: "Request Telemedicine",
    content: "Our services can be trustable, honest.",
    contentAlignClass: "pb-5",
    imgOne: telemed,
    imgTwo: serviceArrow,
  },
  {
    id: 2,
    title: "Report a Theft",
    content: "Our services can be trustable, honest and worthy",
    contentAlignClass: "pb-5",
    imgOne: theft,
    imgTwo: serviceArrow,
  },
  {
    id: 3,
    title: "Flight Delay Claim",
    content: "Our services can be trustable, honest and worthy",
    contentAlignClass: "pb-5",
    imgOne: flight,
    imgTwo: serviceArrow,
  },
  {
    id: 4,
    title: "Assist in Accident",
    content: "Our services can be trustable, honest and worthy",
    contentAlignClass: "pb-5",
    imgOne: accident,
    imgTwo: serviceArrow,
  },
  {
    id: 5,
    title: "Assist in medical",
    content: "Our services can be trustable, honest and worthy",
    contentAlignClass: "pb-5",
    imgOne: medAssisst,
    imgTwo: serviceArrow,
  },
  {
    id: 6,
    title: "Report a Theft",
    content: "Our services can be trustable, honest and worthy",
    contentAlignClass: "pb-5",
    imgOne: theft,
    imgTwo: serviceArrow,
  },
  {
    id: 7,
    title: "Assist in Accident",
    content: "Our services can be trustable, honest and worthy",
    contentAlignClass: "pb-5",
    imgOne: accident,
    imgTwo: serviceArrow,
  },
  {
    id: 8,
    title: "Assist in medical",
    content: "Our services can be trustable, honest and worthy",
    contentAlignClass: "pb-5",
    imgOne: medAssisst,
    imgTwo: serviceArrow,
  },
  {
    id: 9,
    title: "Request Telemedicine",
    content: "Our services can be trustable, honest.",
    contentAlignClass: "pb-5",
    imgOne: telemed,
    imgTwo: serviceArrow,
  },
  {
    id: 10,
    title: "Road Side Assistance",
    content: "Our services can be trustable, honest and worthy",
    contentAlignClass: "pb-5",
    imgOne: serviceIconOne,
    imgTwo: serviceArrow,
  },
  {
    id: 11,
    title: "Flight Delay Claim",
    content: "Our services can be trustable, honest and worthy",
    contentAlignClass: "pb-5",
    imgOne: flight,
    imgTwo: serviceArrow,
  },
  {
    id: 12,
    title: "Report a Theft",
    content: "Our services can be trustable, honest and worthy",
    contentAlignClass: "pb-5",
    imgOne: theft,
    imgTwo: serviceArrow,
  },
  {
    id: 13,
    title: "Assist in Accident",
    content: "Our services can be trustable, honest and worthy",
    contentAlignClass: "pb-5",
    imgOne: accident,
    imgTwo: serviceArrow,
  },
  {
    id: 14,
    title: "Assist in medical",
    content: "Our services can be trustable, honest and worthy",
    contentAlignClass: "pb-5",
    imgOne: medAssisst,
    imgTwo: serviceArrow,
  },
  {
    id: 15,
    title: "Request Telemedicine",
    content: "Our services can be trustable, honest.",
    contentAlignClass: "pb-5",
    imgOne: telemed,
    imgTwo: serviceArrow,
  },
];

export const resultData = [
  {
    id: 0,
    pillNo: 0,
    code: "#COM-0927431",
    date: "12 Dec 2021",
    statusClass: "searchStatus",
    status: "Under Processing",
    detailsData: [
      {
        id: 0,
        complaintTitle: "Complaint number",
        complaintText: "#COM-0927431",
      },
      {
        id: 1,
        complaintTitle: "Complaint Submission Date",
        complaintText: "12 Dec 2021",
      },
      {
        id: 2,
        complaintTitle: "Complaint Closure Date",
        complaintText: "14 Dec 2021",
      },
      {
        id: 3,
        complaintTitle: "Complaint Category",
        complaintText: "Motor",
      },
      {
        id: 4,
        complaintTitle: "Care Owner Details",
        complaintText: "#COM-0927431",
      },
      {
        id: 5,
        complaintTitle: "Care Owner Details",
        complaintText: "Care Owner Name",
      },
      {
        id: 6,
        complaintTitle: "Care Owner Details",
        complaintText: "email@tawuniya.com.sa",
      },
      {
        id: 7,
        complaintTitle: "Care Owner Direct Extension Number",
        complaintText: "Direct Extension Number",
      },
      {
        id: 8,
        complaintTitle: "",
        complaintText: "",
      },
    ],
  },
  {
    id: 1,
    pillNo: 1,
    code: "#COM-0927431",
    date: "12 Dec 2021",
    statusClass: "searchSuccessStatus",
    status: "Resolved",
    detailsData: [
      {
        id: 0,
        complaintTitle: "Complaint number",
        complaintText: "#COM-0927431",
      },
      {
        id: 1,
        complaintTitle: "Complaint Submission Date",
        complaintText: "12 Dec 2021",
      },
      {
        id: 2,
        complaintTitle: "Complaint Closure Date",
        complaintText: "14 Dec 2021",
      },
      {
        id: 3,
        complaintTitle: "Complaint Category",
        complaintText: "Motor",
      },
      {
        id: 4,
        complaintTitle: "Care Owner Details",
        complaintText: "#COM-0927431",
      },
      {
        id: 5,
        complaintTitle: "Care Owner Details",
        complaintText: "Care Owner Name",
      },
      {
        id: 6,
        complaintTitle: "Care Owner Details",
        complaintText: "email@tawuniya.com.sa",
      },
      {
        id: 7,
        complaintTitle: "Care Owner Direct Extension Number",
        complaintText: "Direct Extension Number",
      },
      {
        id: 8,
        complaintTitle: "",
        complaintText: "",
      },
    ],
  },
  {
    id: 2,
    pillNo: 2,
    code: "#COM-0927431",
    date: "12 Dec 2021",
    statusClass: "searchSuccessStatus",
    status: "Resolved",
    detailsData: [
      {
        id: 0,
        complaintTitle: "Complaint number",
        complaintText: "#COM-0927431",
      },
      {
        id: 1,
        complaintTitle: "Complaint Submission Date",
        complaintText: "12 Dec 2021",
      },
      {
        id: 2,
        complaintTitle: "Complaint Closure Date",
        complaintText: "14 Dec 2021",
      },
      {
        id: 3,
        complaintTitle: "Complaint Category",
        complaintText: "Motor",
      },
      {
        id: 4,
        complaintTitle: "Care Owner Details",
        complaintText: "#COM-0927431",
      },
      {
        id: 5,
        complaintTitle: "Care Owner Details",
        complaintText: "Care Owner Name",
      },
      {
        id: 6,
        complaintTitle: "Care Owner Details",
        complaintText: "email@tawuniya.com.sa",
      },
      {
        id: 7,
        complaintTitle: "Care Owner Direct Extension Number",
        complaintText: "Direct Extension Number",
      },
      {
        id: 8,
        complaintTitle: "",
        complaintText: "",
      },
    ],
  },
  {
    id: 3,
    pillNo: 3,
    code: "#COM-0927431",
    date: "12 Dec 2021",
    statusClass: "searchSuccessStatus",
    status: "Resolved",
    detailsData: [
      {
        id: 0,
        complaintTitle: "Complaint number",
        complaintText: "#COM-0927431",
      },
      {
        id: 1,
        complaintTitle: "Complaint Submission Date",
        complaintText: "12 Dec 2021",
      },
      {
        id: 2,
        complaintTitle: "Complaint Closure Date",
        complaintText: "14 Dec 2021",
      },
      {
        id: 3,
        complaintTitle: "Complaint Category",
        complaintText: "Motor",
      },
      {
        id: 4,
        complaintTitle: "Care Owner Details",
        complaintText: "#COM-0927431",
      },
      {
        id: 5,
        complaintTitle: "Care Owner Details",
        complaintText: "Care Owner Name",
      },
      {
        id: 6,
        complaintTitle: "Care Owner Details",
        complaintText: "email@tawuniya.com.sa",
      },
      {
        id: 7,
        complaintTitle: "Care Owner Direct Extension Number",
        complaintText: "Direct Extension Number",
      },
      {
        id: 8,
        complaintTitle: "",
        complaintText: "",
      },
    ],
  },
];

export const motorInsuraceData = [
  {
    imageData: {
      imgFile: motot_insurance_image,
      title: "Motor Insurance",
      description: "Access to hundreds of exclusive offers, discounts and more",
      label: "Individuals",
    },
    cardList: [
      {
        title: "Al Shamel",
        subTitle:
          "The maximum cover of Al-Shamel insurance is determined by the value of the insured vehicle at the time of accident, in addition to the public liability towards third party affected by the incident. ",
        bannerImage: al_shamel_motor,
      },
      {
        title: "Sanad",
        subTitle:
          "Cover up to SR 10 million in respect of the third party bodily injury and property damage",
        bannerImage: SanadNew,
      },
      {
        title: "Sanad Plus",
        subTitle:
          "Provides limited insurance cover for the damage of the insured private car as well as the liability to third parties",
        bannerImage: sanad_plus_motor,
      },
      {
        title: "Mechanical Breakdown",
        subTitle:
          "Cover the repair of mechanical and electrical failures of the car and the wages of the necessary human resources",
        bannerImage: mech_breakdown_motor,
      },
      {
        title: "Al Shamel",
        subTitle:
          "The maximum cover of Al-Shamel insurance is determined by the value of the insured vehicle at the time of accident, in addition to the public liability towards third party affected by the incident. ",
        bannerImage: al_shamel_motor,
      },
    ],
  },
  {
    imageData: {
      imgFile: medical_tint,
      title: "Medical & Takaful",
      description: "Access to hundreds of exclusive offers, discounts and more",
    },
    cardList: [
      {
        title: "My Family Insurance",
        subTitle:
          "Provides adequate healthcare for all family members with four categories of benefits",
        bannerImage: family_insurance_medical,
      },
      {
        title: "Visit Visa Medical Insurance",
        subTitle:
          "The visitors to the Kingdom of Saudi Arabia will obtain the healthcare for medical emergencies or accidents",
        bannerImage: Visit_insurance,
      },
      {
        title: "Umrah Insurance Program ",
        subTitle:
          "Provides insurance coverage for medical emergency cases and general accidents for foreign Ummrah performers",
        bannerImage: Umrah_insurance,
      },
      {
        title: "Hajj Insurance Program ",
        subTitle:
          "Provides insurance coverage for medical emergency cases and general accidents for foreign Hajj performers",
        bannerImage: Hajj_insurance,
      },
    ],
  },
  {
    imageData: {
      imgFile: property_tint,
      title: "Property & Casualty",
      description: "Access to hundreds of exclusive offers, discounts and more",
    },
    cardList: [
      {
        title: "Home Insurance",
        subTitle:
          "Provides protection to residential buildings and their contents against natural disasters, and covers losses resulting from any incidents leading to the deterioration or damage to the house",
        bannerImage: Home_insurance,
      },
      {
        title: "International Travel Insurance",
        subTitle:
          "Provides comprehensive coverage for travelers regarding the risks related to travel outside Saudi Arabia",
        bannerImage: International_Travel_insurance,
      },
      {
        title: "COVID-19 Travel Insurance",
        subTitle:
          "Protect from the Covide-19 risk of infection and the associated problems resulting from the government's preventive measures to limit the spread of the virus",
        bannerImage: Covid19_Insurance,
      },
      {
        title: "Medical Malpractice Insurance",
        subTitle:
          "Provide coverage for those practicing medical professions from the risks associated with their work and the legal third party liability that may arise out of any error, negligence, or omission incurred during the performance of their work",
        bannerImage: Malpractice_Insurance,
      },
      {
        title: "Shop Owners Insurance",
        subTitle:
          "Provides comprehensive coverage for all types of shops except for certain activities specified in the insurance policy",
        bannerImage: Shop_owner_Insurance,
      },
    ],
  },
];

export const corporateProductList = [
  {
    imageData: {
      imgFile: motot_insurance_image,
      title: "Motor Insurance",
      description: "Access to hundreds of exclusive offers, discounts and more",
    },
    cardList: [
      {
        title: "Alshamel",
        subTitle:
          "Indemnify the policyholder against the accidental loss of or damage to the insured commercial motor vehicles and the third party liabilities",
        bannerImage: al_shamel_motor,
      },
      {
        title: "Sanad",
        subTitle:
          "Covers the liability of commercial vehicles to third parties resulting from accidents up to a maximum of 10 million riyals",
        bannerImage: SanadNew,
      },
    ],
  },
  {
    imageData: {
      imgFile: medical_tint,
      title: "Medical & Takaful",
      description: "Access to hundreds of exclusive offers, discounts and more",
    },
    cardList: [
      {
        title: "Balsam",
        subTitle:
          "Integrated healthcare for employees and their family members through a wide network of accredited hospitals and medical centers",
        bannerImage: family_insurance_medical,
      },
      {
        title: "Takaful",
        subTitle:
          "Provides insurance coverage for employees against the death due to any cause",
        bannerImage: Malpractice_Insurance,
      },
    ],
  },
  {
    imageData: {
      imgFile: property_tint,
      title: "Property & Casualty",
      description: "Access to hundreds of exclusive offers, discounts and more",
    },
    cardList: [
      {
        title: "Engineering & Contractors Insurance",
        subTitle:
          "Tawuniya has designed this collection of insurance products especially for the Engineering & Contractors business to provide the appropriate protection to transact its activities safely",
        bannerImage: Engineering_Insurance,
      },
      {
        title: "Industrial & Energy Insurance",
        subTitle:
          "We designed this collection of insurance products especially for the Industrial and Energy business to provide the appropriate protection to transact its activities safely.",
        bannerImage: Industrial_Insurance,
      },
      {
        title: "Financial & Investment Insurance",
        subTitle:
          "We designed this collection of insurance products especially for the Financial Institution & Investment business to provide the appropriate protection to transact its activities safely.",
        bannerImage: Inverstment_Insurance,
      },
      {
        title: "Retail & Trading Insurance",
        subTitle:
          "We designed this collection of insurance products especially for the Retail & Trading business to provide the appropriate protection to transact its activities safely.",
        bannerImage: Retail_Insurance,
      },
      {
        title: "Services Insurance",
        subTitle:
          "We designed this collection of insurance products especially for the Services business to provide the appropriate protection to transact its activities safely.",
        bannerImage: Services_Insurance,
      },
      {
        title: "Telecommunication & IT Insurance",
        subTitle: "Health Insurance for Small & Medium Business",
        bannerImage: Telecommunication_Insurance,
      },

      {
        title: "Transportation Insurance",
        subTitle: "Health Insurance for Small & Medium Business",
        bannerImage: Transportion_Insurance,
      },
    ],
  },
];

export const SMEProductList = [
  {
    imageData: {
      imgFile: motot_insurance_image,
      title: "Motor Insurance",
      description: "Access to hundreds of exclusive offers, discounts and more",
    },
    cardList: [
      {
        title: "360° Motor Insurance Program",
        subTitle:
          "A program that combines the benefits of the medical insurance policy, Taj program for integrated health care, free membership of Vitality Tawuniya and many other features",
        bannerImage: Vitality_insurance,
      },
    ],
  },
  {
    imageData: {
      imgFile: medical_tint,
      title: "Medical & Takaful",
      description: "Access to hundreds of exclusive offers, discounts and more",
    },
    cardList: [
      {
        title: "360° Health Insurance Program",
        subTitle:
          "A program that combines the basic and extensions benefits of the comprehensive insurance (Alshamel) and third party liability insurance (Sanad) to protect the SME motor vehicles",
        bannerImage: liability_insurance,
      },
    ],
  },
  {
    imageData: {
      imgFile: property_tint,
      title: "Property & Casualty",
      description: "Access to hundreds of exclusive offers, discounts and more",
    },
    cardList: [
      {
        title: "360° Property & Casuality Program",
        subTitle:
          "Provides protection to residential buildings and their contents against natural disasters, and covers losses resulting from any incidents leading to the deterioration or damage to the house",
        bannerImage: Damage_insurance,
      },
    ],
  },
];

export const careerContentData = [
  {
    id: 1,
    banner: careers_image_one,
    label: "Make a positive impact",
    description:
      "What makes working at Tawuniya meaningful is knowing that you are playing an important role in bringing new technologies to the world that improve people’s lives. Our innovations enable people in new ways, and we employ our technology to contribute to growth and development in the communities in which we operate around the world. Here at Tawuniya, you have the opportunity to do truly meaningful work that has a positive impact on the world.",
  },
  {
    id: 2,
    banner: careers_image_two,
    label: "Shape the future of our world",
    description:
      "It is the diversity, creativity, and passion of the people who work at Tawuniya that have made us one of the world’s most innovative companies, and our people continue to drive our innovation forward. We work together in an open and collaborative environment that promotes sharing of the unique knowledge and expertise that each individual brings. This is a place where you can work with great people and your ideas can be brought to life in new products and solutions that are shaping the future of how we live.",
  },
  {
    id: 3,
    banner: careers_image_three,
    label: "Take on big challenges",
    description:
      "Throughout our history, we’ve taken on big challenges and we strive to be the very best at what we do. This spirit is the driving force that has made us an insurance leader and sustains our ambition to develop cutting-edge technologies that push the boundaries of what’s possible. Come join us as we take on the next big challenges of the future.",
  },
];

export const relatedNewsData = [
  {
    id: 1,
    tagTitle: "News",
    date: "10 May 2022",
    description: "Tawuniya appoints Dr. Ammr Kurdi as Chief Financial Officer",
  },
  {
    id: 2,
    tagTitle: "News",
    date: "10 May 2022",
    description: "Tawuniya appoints Dr. Ammr Kurdi as Chief Financial Officer",
  },
  {
    id: 3,
    tagTitle: "News",
    date: "10 May 2022",
    description: "Tawuniya appoints Dr. Ammr Kurdi as Chief Financial Officer",
  },
  {
    id: 4,
    tagTitle: "News",
    date: "10 May 2022",
    description: "Tawuniya appoints Dr. Ammr Kurdi as Chief Financial Officer",
  },
];

export const dashboardRoadHeaderData = {
  headerBannerTitle: "Motor Services",
  headerBannerPara: "Road Assistance",
  url: "/dashboard/service/road-assisstance/new-request",
};

export const dashboardTeleMedHeaderData = {
  headerBannerTitle: "Health Services",
  headerBannerPara: "Telemedicine",
  url: "/dashboard/service/tele-medicine/new-request",
};

export const dashboardPeriodicInspectHeaderData = {
  headerBannerTitle: "Motor Services",
  headerBannerPara: "Periodic Inspection",
  url: "/dashboard/service/periodic-inspection/new-request",
};

export const dashboardCarWashHeaderData = {
  headerBannerTitle: "Motor Services",
  headerBannerPara: "Car Wash",
  url: "",
};

export const dashboardRoadProgressData = [
  {
    id: 0,
    progressNo: "1",
    progresssTitle: "Submit Application",
    progressPara:
      "Complete your request details and attached the needed documents",
    class: "pr-3",
    arrowIcon: progressArrow,
  },
  {
    id: 1,
    progressNo: "2",
    progresssTitle: "Get Service",
    progressPara: "Receive the needed service.",
    class: "pl-3",
    arrowIcon: progressArrow,
  },
];

export const tableBreakDownData = [
  {
    id: 0,
    tableDivider: "customContainerOne p-0",
    tableClass: "benifitContainer pl-lg-5 pl-3 pt-3",
    tableNoClass: "p-3",
    needBorder: true,
    txt: null,
    newText: [
      {
        id: 0,
        tableCellContent: "Engine",
        tableSubClass: "benifitContainer-top-borderThin-BreakDown pl-lg-5 pl-3",
        icon: engine,
        iconClass: "img-fluid xCircleIcon pr-3 pr-3",
        needOnlyIcon: true,
      },
      {
        id: 1,
        tableCellContent: "Gearbox",
        tableSubClass: "benifitContainer pl-lg-5 pl-3",
        icon: gear,
        iconClass: "img-fluid xCircleIcon pr-3",
        needOnlyIcon: true,
      },
      {
        id: 2,
        tableCellContent: "Differential",
        tableSubClass: "benifitContainer pl-lg-5 pl-3",
        icon: differential,
        iconClass: "img-fluid xCircleIcon pr-3",
        needOnlyIcon: true,
      },
      {
        id: 3,
        tableCellContent: "Turbo Assembly",
        tableSubClass: "benifitContainer pl-lg-5 pl-3",
        icon: turbo,
        iconClass: "img-fluid xCircleIcon pr-3",
        needOnlyIcon: true,
      },
      {
        id: 4,
        tableCellContent: "Prop Shaft & CV Joints",
        tableSubClass: "benifitContainer pl-lg-5 pl-3",
        icon: prop,
        iconClass: "img-fluid xCircleIcon pr-3",
        needOnlyIcon: true,
      },
      {
        id: 5,
        tableCellContent: "Steering Mechanism",
        tableSubClass: "benifitContainer pl-lg-5 pl-3",
        icon: steer,
        iconClass: "img-fluid xCircleIcon pr-3",
        needOnlyIcon: true,
      },
      {
        id: 6,
        tableCellContent: "Wheel Bearings",
        tableSubClass: "benifitContainer pl-lg-5 pl-3",
        icon: wheelBearing,
        iconClass: "img-fluid xCircleIcon pr-3",
        needOnlyIcon: true,
      },
      {
        id: 7,
        tableCellContent: "Breaking System",
        tableSubClass: "benifitContainer pl-lg-5 pl-3",
        icon: breakSystem,
        iconClass: "img-fluid xCircleIcon pr-3",
        needOnlyIcon: true,
      },
      {
        id: 8,
        tableCellContent: "Fuel System",
        tableSubClass: "benifitContainer pl-lg-5 pl-3",
        icon: fuel,
        iconClass: "img-fluid xCircleIcon pr-3",
        needOnlyIcon: true,
      },
      {
        id: 9,
        tableCellContent: "Electrical Components",
        tableSubClass: "benifitContainer pl-lg-5 pl-3",
        icon: electricComponents,
        iconClass: "img-fluid xCircleIcon pr-3",
        needOnlyIcon: true,
      },
      {
        id: 10,
        tableCellContent: "Suspension",
        tableSubClass: "benifitContainer pl-lg-5 pl-3",
        icon: suspension,
        iconClass: "img-fluid xCircleIcon pr-3",
        needOnlyIcon: true,
      },
      {
        id: 11,
        tableCellContent: "Wheel Bearings",
        tableSubClass: "benifitContainer pl-lg-5 pl-3",
        icon: wheelBearing,
        iconClass: "img-fluid xCircleIcon pr-3",
        needOnlyIcon: true,
      },
      {
        id: 12,
        tableCellContent: "Cylinder Head Gasket",
        tableSubClass: "benifitContainer-bottom-borderThin pl-lg-5 pl-3",
        icon: cylinder,
        iconClass: "img-fluid xCircleIcon pr-3",
        needOnlyIcon: true,
      },
    ],
  },
  {
    id: 1,
    tableDivider: "customContainerTwo p-0",
    tableClass: "bestSellerContainer text-center pt-3",
    tableNoClass: "p-3",
    txt: null,
    newText: [
      {
        id: 0,
        tableCellContent: "20,000",
        tableSubClass: "bestSellerContainer text-center",
        needOnlyIcon: true,
      },
      {
        id: 1,
        tableCellContent: "10,000",
        tableSubClass: "bestSellerContainer text-center",
        needOnlyIcon: true,
      },
      {
        id: 2,
        tableCellContent: "10,000",
        tableSubClass: "bestSellerContainer text-center",
        needOnlyIcon: true,
      },
      {
        id: 3,
        tableCellContent: "5,000",
        tableSubClass: "bestSellerContainer text-center",
        needOnlyIcon: true,
      },
      {
        id: 4,
        tableCellContent: "4,000",
        tableSubClass: "bestSellerContainer text-center",
        needOnlyIcon: true,
      },
      {
        id: 5,
        tableCellContent: "4,000",
        tableSubClass: "bestSellerContainer text-center",
        needOnlyIcon: true,
      },
      {
        id: 6,
        tableCellContent: "4,000",
        tableSubClass: "bestSellerContainer text-center",
        needOnlyIcon: true,
      },
      {
        id: 7,
        tableCellContent: "4,000",
        tableSubClass: "bestSellerContainer text-center",
        needOnlyIcon: true,
      },
      {
        id: 8,
        tableCellContent: "4,000",
        tableSubClass: "bestSellerContainer text-center",
        needOnlyIcon: true,
      },
      {
        id: 9,
        tableCellContent: "4,000",
        tableSubClass: "bestSellerContainer text-center",
        needOnlyIcon: true,
      },
      {
        id: 10,
        tableCellContent: "4,000",
        tableSubClass: "bestSellerContainer text-center",
        needOnlyIcon: true,
      },
      {
        id: 11,
        tableCellContent: "4,000",
        tableSubClass: "bestSellerContainer text-center",
        needOnlyIcon: true,
      },
      {
        id: 12,
        tableCellContent: "4,000",
        tableSubClass: "bestSeller-bottom-Container text-center",
        needOnlyIcon: true,
      },
    ],
  },
  {
    id: 2,
    tableDivider: "customContainerThree p-0",
    tableClass: "sanadContainer text-center pt-3",
    tableNoClass: "p-3",
    txt: null,
    newText: [
      {
        id: 0,
        tableCellContent: "20,000",
        tableSubClass: "sanadContainer text-center",
        needOnlyIcon: true,
      },
      {
        id: 1,
        tableCellContent: "10,000",
        tableSubClass: "sanadContainer text-center",
        needOnlyIcon: true,
      },
      {
        id: 2,
        tableCellContent: "10,000",
        tableSubClass: "sanadContainer text-center",
        needOnlyIcon: true,
      },
      {
        id: 3,
        tableCellContent: "5,000",
        tableSubClass: "sanadContainer text-center",
        needOnlyIcon: true,
      },
      {
        id: 4,
        tableCellContent: "4,000",
        tableSubClass: "sanadContainer text-center",
        needOnlyIcon: true,
      },
      {
        id: 5,
        tableCellContent: "4,000",
        tableSubClass: "sanadContainer text-center",
        needOnlyIcon: true,
      },
      {
        id: 6,
        tableCellContent: "4,000",
        tableSubClass: "sanadContainer text-center",
        needOnlyIcon: true,
      },
      {
        id: 7,
        tableCellContent: "4,000",
        tableSubClass: "sanadContainer text-center",
        needOnlyIcon: true,
      },
      {
        id: 8,
        tableCellContent: "4,000",
        tableSubClass: "sanadContainer text-center",
        needOnlyIcon: true,
      },
      {
        id: 9,
        tableCellContent: "4,000",
        tableSubClass: "sanadContainer text-center",
        needOnlyIcon: true,
      },
      {
        id: 10,
        tableCellContent: "4,000",
        tableSubClass: "sanadContainer text-center",
        needOnlyIcon: true,
      },
      {
        id: 11,
        tableCellContent: "4,000",
        tableSubClass: "sanadContainer text-center",
        needOnlyIcon: true,
      },
      {
        id: 12,
        tableCellContent: null,
        tableSubClass: "sanad-bottom-Container text-center",
        needOnlyIcon: true,
        iconClass: "img-fluid xCircleIcon",
        icon: xOrangeCircle,
      },
    ],
  },
  {
    id: 3,
    tableDivider: "customContainerFour p-0",
    tableClass: "sanadPlusContainer text-center pt-3",
    tableNoClass: "p-3",
    txt: null,
    newText: [
      {
        id: 0,
        tableCellContent: "20,000",
        tableSubClass: "sanadPlusContainer text-center",
        needOnlyIcon: true,
      },
      {
        id: 1,
        tableCellContent: "10,000",
        tableSubClass: "sanadPlusContainer text-center",
        needOnlyIcon: true,
      },
      {
        id: 2,
        tableCellContent: "10,000",
        tableSubClass: "sanadPlusContainer text-center",
        needOnlyIcon: true,
      },
      {
        id: 3,
        tableCellContent: null,
        tableSubClass: "sanadPlusContainer text-center",
        icon: xOrangeCircle,
        iconClass: "img-fluid xCircleIcon",
        needOnlyIcon: true,
      },
      {
        id: 4,
        tableCellContent: null,
        tableSubClass: "sanadPlusContainer text-center",
        icon: xOrangeCircle,
        iconClass: "img-fluid xCircleIcon",
        needOnlyIcon: true,
      },
      {
        id: 5,
        tableCellContent: null,
        tableSubClass: "sanadPlusContainer text-center",
        icon: xOrangeCircle,
        iconClass: "img-fluid xCircleIcon",
        needOnlyIcon: true,
      },
      {
        id: 6,
        tableCellContent: null,
        tableSubClass: "sanadPlusContainer text-center",
        icon: xOrangeCircle,
        iconClass: "img-fluid xCircleIcon",
        needOnlyIcon: true,
      },
      {
        id: 7,
        tableCellContent: null,
        tableSubClass: "sanadPlusContainer text-center",
        icon: xOrangeCircle,
        iconClass: "img-fluid xCircleIcon",
        needOnlyIcon: true,
      },
      {
        id: 8,
        tableCellContent: null,
        tableSubClass: "sanadPlusContainer text-center",
        icon: xOrangeCircle,
        iconClass: "img-fluid xCircleIcon",
        needOnlyIcon: true,
      },
      {
        id: 9,
        tableCellContent: null,
        tableSubClass: "sanadPlusContainer text-center",
        icon: xOrangeCircle,
        iconClass: "img-fluid xCircleIcon",
        needOnlyIcon: true,
      },
      {
        id: 10,
        tableCellContent: null,
        tableSubClass: "sanadPlusContainer text-center",
        icon: xOrangeCircle,
        iconClass: "img-fluid xCircleIcon",
        needOnlyIcon: true,
      },
      {
        id: 11,
        tableCellContent: null,
        tableSubClass: "sanadPlusContainer text-center",
        icon: xOrangeCircle,
        iconClass: "img-fluid xCircleIcon",
        needOnlyIcon: true,
      },
      {
        id: 12,
        tableCellContent: null,
        tableSubClass: "sanadPlus-bottom-Container text-center",
        icon: xOrangeCircle,
        iconClass: "img-fluid xCircleIcon",
        needOnlyIcon: true,
      },
    ],
  },
];

export const headerBreakDownData = [
  {
    id: 0,
    mainClass: "customHeaderOne",
    baseClassOne: "ContainerComponentOne",
    baseClassTwo: "ContainerComponentTwo",
    paraClass: "fs-24 fw-800 pl-lg-5 pl-3 headerTitleOne",
    paraTitle: "Components covered",
    hideCellOne: true,
    showTip: "hideToolTip",
  },
  {
    id: 1,
    mainClass: "customHeaderTwo",
    baseClassOne: "ContainerGoldOne",
    baseClassTwo: null,
    paraClass: "fs-24 fw-800 headerTitleTwo",
    paraTitle: "Gold",
    btnLabel: "Order Now",
    hideCellOne: false,
    showTip: "hideToolTip",
  },
  {
    id: 2,
    mainClass: "customHeaderThree",
    baseClassOne: "ContainerSilverOne",
    baseClassTwo: null,
    paraClass: "fs-24 fw-800 headerTitleThree",
    paraTitle: "Silver",
    hideCellOne: false,
    showTip: "hideToolTip",
  },
  {
    id: 3,
    mainClass: "customHeaderFour",
    baseClassOne: "ContainerBronzeOne",
    baseClassTwo: null,
    paraClass: "fs-24 fw-800 headerTitleFour",
    paraTitle: "Bronze",
    hideCellOne: false,
    showTip: "hideToolTip",
  },
];

export const headerPerkData = [
  {
    id: 0,
    mainClass: "customHeaderOne",
    baseClassOne: "ContainerComponentOne",
    baseClassTwo: "ContainerComponentTwo",
    paraClass: "fs-24 fw-800 pl-lg-5 pl-3 headerTitleOne",
    paraTitle: "Covers & Benefits",
    hideCellOne: true,
    showTip: "hideToolTip",
  },
  {
    id: 1,
    mainClass: "customHeaderTwo",
    baseClassOne: "ContainerGoldOne",
    baseClassTwo: null,
    paraClass: "fs-24 fw-800 headerTitleTwo m-0 pb-2",
    paraTitle: "Al SHAMEL",
    paraSubtitle: null,
    paraSubtitleNew: "Comprehensive",
    paraSubtitleNewClass: "fs-14 fw-400 componentTwoSubTitleNew",
    btnLabel: "Buy Now",
    hideSubClass: "hideSubClass",
    hideCellOne: false,
    needExtraContent: true,
  },
  {
    id: 2,
    mainClass: "customHeaderThree",
    baseClassOne: "ContainerSilverOne",
    baseClassTwo: null,
    paraClass: "fs-24 fw-800 headerTitleThree m-0",
    paraTitle: "SANAD",
    paraSubtitleNew: "Third Party Liability",
    motorTxt: "Motor Flex",
    holdIcon: holdCar,
    motorClass: "fs-14 fw-400 motorText",
    paraSubtitleNewClass: "fs-14 fw-400 componentThreeSubTitleNew",
    hideCellOne: false,
    needExtraContent: true,
    showTip: "hideToolTip",
  },
  {
    id: 3,
    mainClass: "customHeaderFour",
    baseClassOne: "ContainerBronzeOne",
    baseClassTwo: null,
    paraClass: "fs-24 fw-800 headerTitleFour m-0",
    paraTitle: "SANAD PLUS",
    paraSubtitleNew: "Third Party Liability",
    motorTxt: "Motor Flex",
    holdIcon: holdCar,
    motorClass: "fs-14 fw-400 motorText",
    paraSubtitleNewClass: "fs-14 fw-400 componentFourSubTitleNew",
    hideCellOne: false,
    needExtraContent: true,
    showTip: "hideToolTip",
  },
];

export const tablePerkData = [
  {
    id: 0,
    tableDivider: "customContainerOne p-0",
    tableClass: "benifitContainer pl-lg-5 pl-3 pt-3",
    tableNoClass: "p-3",
    needBorder: true,
    txt: null,
    newText: [
      {
        id: 0,
        tableCellContent:
          "Cover against Loss/ Damage to own vehicle (Agency or Non- agency)",
        tableCellContentNew: "",
        tableSubClass: "benifitContainer-top-borderThin pl-lg-5 pl-3",
        icon: perkIcon1,
        iconClass: "img-fluid xCircleIcon pr-3",
        needOnlyIcon: true,
      },
      {
        id: 1,
        tableCellContent: "Third party liability upto 10m SR",
        tableSubClass: "benifitContainer pl-lg-5 pl-3",
        icon: perkIcon12,
        iconClass: "img-fluid xCircleIcon pr-3",
        needOnlyIcon: true,
      },
      {
        id: 2,
        tableCellContent: "Emergency Medical expenses",
        tableSubClass: "benifitContainer pl-lg-5 pl-3",
        icon: perkIcon5,
        iconClass: "img-fluid xCircleIcon pr-3",
        needOnlyIcon: true,
      },
      {
        id: 3,
        tableCellContent: "End to end claim services",
        tableSubClass: "benifitContainer pl-lg-5 pl-3",
        icon: perkIcon13,
        iconClass: "img-fluid xCircleIcon pr-3",
        needOnlyIcon: true,
      },
      {
        id: 4,
        tableCellContent: "No claims discount",
        tableSubClass: "benifitContainer pl-lg-5 pl-3",
        icon: perkIcon1,
        iconClass: "img-fluid xCircleIcon pr-3",
        needOnlyIcon: true,
      },
      {
        id: 5,
        tableCellContent: "Loyalty discount",
        tableSubClass: "benifitContainer pl-lg-5 pl-3",
        icon: perkIcon6,
        iconClass: "img-fluid xCircleIcon pr-3",
        needOnlyIcon: true,
      },
      {
        id: 6,
        tableCellContent: "Personal Accident (passengers)",
        tableSubClass: "benifitContainer pl-lg-5 pl-3",
        icon: perkIcon2,
        iconClass: "img-fluid xCircleIcon pr-3",
        needOnlyIcon: true,
      },
      {
        id: 7,
        tableCellContent: "Personal Accident (Driver)",
        tableSubClass: "benifitContainer pl-lg-5 pl-3",
        icon: perkIcon7,
        iconClass: "img-fluid xCircleIcon pr-3",
        needOnlyIcon: true,
      },
      {
        id: 8,
        tableCellContent: "Roadside Assistance",
        tableSubClass: "benifitContainer pl-lg-5 pl-3",
        icon: perkIcon8,
        iconClass: "img-fluid xCircleIcon pr-3",
        needOnlyIcon: true,
      },
      {
        id: 9,
        tableCellContent: "Natural Perils (Flood, Hail)",
        tableSubClass: "benifitContainer pl-lg-5 pl-3",
        icon: perkIcon9,
        iconClass: "img-fluid xCircleIcon pr-3",
        needOnlyIcon: true,
      },
      {
        id: 10,
        tableCellContent: "Glass Cover",
        tableSubClass: "benifitContainer pl-lg-5 pl-3",
        icon: perkIcon10,
        iconClass: "img-fluid xCircleIcon pr-3",
        needOnlyIcon: true,
      },
      {
        id: 11,
        tableCellContent: "Theft",
        tableSubClass: "benifitContainer pl-lg-5 pl-3",
        icon: perkIcon11,
        iconClass: "img-fluid xCircleIcon pr-3",
        needOnlyIcon: true,
      },
      {
        id: 12,
        tableCellContent: "Malicious damage",
        tableSubClass: "benifitContainer pl-lg-5 pl-3",
        icon: perkIcon3,
        iconClass: "img-fluid xCircleIcon pr-3",
        needOnlyIcon: true,
      },
      {
        id: 13,
        tableCellContent: "Tawuniya Drive",
        tableSubClass: "benifitContainer-bottom-borderThin pl-lg-5 pl-3",
        icon: perkIcon14,
        iconClass: "img-fluid xCircleIcon pr-3",
        needOnlyIcon: true,
      },
    ],
  },
  {
    id: 1,
    tableDivider: "customContainerTwo p-0",
    tableClass: "bestSellerContainer text-center pt-3",
    tableNoClass: "p-3",
    txt: null,
    newText: [
      {
        id: 0,
        tableCellContent: null,
        tableSubClass: "bestSellerContainer-top-borderThin text-center",
        needOnlyIcon: true,
        icon: orangePerkTick,
        iconClass: "img-fluid xCircleIcon",
      },
      {
        id: 1,
        tableCellContent: null,
        tableSubClass: "bestSellerContainer text-center",
        needOnlyIcon: true,
        icon: orangePerkTick,
        iconClass: "img-fluid xCircleIcon",
      },
      {
        id: 2,
        tableCellContent: null,
        tableSubClass: "bestSellerContainer text-center",
        needOnlyIcon: true,
        icon: orangePerkTick,
        iconClass: "img-fluid xCircleIcon",
      },
      {
        id: 3,
        tableCellContent: null,
        tableSubClass: "bestSellerContainer text-center",
        needOnlyIcon: true,
        icon: orangePerkTick,
        iconClass: "img-fluid xCircleIcon",
      },
      {
        id: 4,
        tableCellContent: null,
        tableSubClass: "bestSellerContainer text-center",
        needOnlyIcon: true,
        icon: orangePerkTick,
        iconClass: "img-fluid xCircleIcon",
      },
      {
        id: 5,
        tableCellContent: null,
        tableSubClass: "bestSellerContainer text-center",
        needOnlyIcon: true,
        icon: orangePerkTick,
        iconClass: "img-fluid xCircleIcon",
      },
      {
        id: 6,
        tableCellContent: null,
        tableSubClass: "bestSellerContainer text-center",
        needOnlyIcon: true,
        icon: orangePerkTick,
        iconClass: "img-fluid xCircleIcon",
      },
      {
        id: 7,
        tableCellContent: null,
        tableSubClass: "bestSellerContainer text-center",
        needOnlyIcon: true,
        icon: orangePerkTick,
        iconClass: "img-fluid xCircleIcon",
      },
      {
        id: 8,
        tableCellContent: null,
        tableSubClass: "bestSellerContainer text-center",
        needOnlyIcon: true,
        icon: orangePerkTick,
        iconClass: "img-fluid xCircleIcon",
      },
      {
        id: 9,
        tableCellContent: null,
        tableSubClass: "bestSellerContainer text-center",
        needOnlyIcon: true,
        icon: orangePerkTick,
        iconClass: "img-fluid xCircleIcon",
      },
      {
        id: 10,
        tableCellContent: null,
        tableSubClass: "bestSellerContainer text-center",
        needOnlyIcon: true,
        icon: orangePerkTick,
        iconClass: "img-fluid xCircleIcon",
      },
      {
        id: 11,
        tableCellContent: null,
        tableSubClass: "bestSellerContainer text-center",
        needOnlyIcon: true,
        icon: orangePerkTick,
        iconClass: "img-fluid xCircleIcon",
      },
      {
        id: 12,
        tableCellContent: null,
        tableSubClass: "bestSellerContainer text-center",
        needOnlyIcon: true,
        icon: orangePerkTick,
        iconClass: "img-fluid xCircleIcon",
      },
      {
        id: 13,
        tableCellContent: null,
        tableSubClass: "bestSeller-bottom-Container text-center",
        needOnlyIcon: true,
        icon: orangePerkTick,
        iconClass: "img-fluid xCircleIcon",
      },
    ],
  },
  {
    id: 2,
    tableDivider: "customContainerThree p-0",
    tableClass: "sanadContainer text-center pt-3",
    tableNoClass: "p-3",
    txt: null,
    newText: [
      {
        id: 0,
        tableCellContent: null,
        tableSubClass: "sanadContainer-top-borderThin text-center",
        needOnlyIcon: true,
        icon: xCircle,
        iconClass: "img-fluid xCircleIcon",
      },
      {
        id: 1,
        tableCellContent: null,
        tableSubClass: "sanadContainer text-center",
        needOnlyIcon: true,
        icon: tickMark,
        iconClass: "img-fluid xCircleIcon",
      },
      {
        id: 2,
        tableCellContent: null,
        tableSubClass: "sanadContainer text-center",
        needOnlyIcon: true,
        icon: xCircle,
        iconClass: "img-fluid xCircleIcon",
      },
      {
        id: 3,
        tableCellContent: null,
        tableSubClass: "sanadContainer text-center",
        needOnlyIcon: true,
        icon: xCircle,
        iconClass: "img-fluid xCircleIcon",
      },
      {
        id: 4,
        tableCellContent: null,
        tableSubClass: "sanadContainer text-center",
        needOnlyIcon: true,
        icon: tickMark,
        iconClass: "img-fluid xCircleIcon",
      },
      {
        id: 5,
        tableCellContent: null,
        tableSubClass: "sanadContainer text-center",
        needOnlyIcon: true,
        icon: tickMark,
        iconClass: "img-fluid xCircleIcon",
      },
      {
        id: 6,
        tableCellContent: null,
        tableSubClass: "sanadContainer text-center",
        needOnlyIcon: true,
        icon: tickMark,
        iconClass: "img-fluid xCircleIcon",
      },
      {
        id: 7,
        tableCellContent: null,
        tableSubClass: "sanadContainer text-center",
        needOnlyIcon: true,
        icon: plus,
        iconClass: "img-fluid xCircleIcon",
      },
      {
        id: 8,
        tableCellContent: null,
        tableSubClass: "sanadContainer text-center",
        needOnlyIcon: true,
        icon: plus,
        iconClass: "img-fluid xCircleIcon",
      },
      {
        id: 9,
        tableCellContent: null,
        tableSubClass: "sanadContainer text-center",
        needOnlyIcon: true,
        icon: plus,
        iconClass: "img-fluid xCircleIcon",
      },
      {
        id: 10,
        tableCellContent: null,
        tableSubClass: "sanadContainer text-center",
        needOnlyIcon: true,
        icon: plus,
        iconClass: "img-fluid xCircleIcon",
      },
      {
        id: 11,
        tableCellContent: null,
        tableSubClass: "sanadContainer text-center",
        needOnlyIcon: true,
        icon: plus,
        iconClass: "img-fluid xCircleIcon",
      },
      {
        id: 12,
        tableCellContent: null,
        tableSubClass: "sanadContainer text-center",
        needOnlyIcon: true,
        icon: plus,
        iconClass: "img-fluid xCircleIcon",
      },
      {
        id: 13,
        tableCellContent: null,
        tableSubClass: "sanad-bottom-Container text-center",
        needOnlyIcon: true,
        icon: plus,
        iconClass: "img-fluid xCircleIcon",
      },
    ],
  },
  {
    id: 3,
    tableDivider: "customContainerFour p-0",
    tableClass: "sanadPlusContainer text-center",
    tableNoClass: "p-3",
    txt: null,
    newText: [
      {
        id: 0,
        tableCellContent: "Upto 10 K",
        tableSubClass: "sanadPlusContainer-top-borderThin text-center",
        needOnlyIcon: true,
      },
      {
        id: 1,
        tableCellContent: null,
        tableSubClass: "sanadPlusContainer text-center",
        icon: tickMark,
        needOnlyIcon: true,
        iconClass: "img-fluid xCircleIcon",
      },
      {
        id: 2,
        tableCellContent: null,
        tableSubClass: "sanadPlusContainer text-center",
        icon: xCircle,
        needOnlyIcon: true,
        iconClass: "img-fluid xCircleIcon",
      },
      {
        id: 3,
        tableCellContent: null,
        tableSubClass: "sanadPlusContainer text-center",
        icon: xCircle,
        needOnlyIcon: true,
        iconClass: "img-fluid xCircleIcon",
      },
      {
        id: 4,
        tableCellContent: null,
        tableSubClass: "sanadPlusContainer text-center",
        icon: tickMark,
        needOnlyIcon: true,
        iconClass: "img-fluid xCircleIcon",
      },
      {
        id: 5,
        tableCellContent: null,
        tableSubClass: "sanadPlusContainer text-center",
        icon: tickMark,
        needOnlyIcon: true,
        iconClass: "img-fluid xCircleIcon",
      },
      {
        id: 6,
        tableCellContent: null,
        tableSubClass: "sanadPlusContainer text-center",
        icon: tickMark,
        needOnlyIcon: true,
        iconClass: "img-fluid xCircleIcon",
      },
      {
        id: 7,
        tableCellContent: null,
        tableSubClass: "sanadPlusContainer text-center",
        icon: plus,
        needOnlyIcon: true,
        iconClass: "img-fluid xCircleIcon",
      },
      {
        id: 8,
        tableCellContent: null,
        tableSubClass: "sanadPlusContainer text-center",
        icon: plus,
        needOnlyIcon: true,
        iconClass: "img-fluid xCircleIcon",
      },
      {
        id: 9,
        tableCellContent: null,
        tableSubClass: "sanadPlusContainer text-center",
        icon: plus,
        needOnlyIcon: true,
        iconClass: "img-fluid xCircleIcon",
      },
      {
        id: 10,
        tableCellContent: null,
        tableSubClass: "sanadPlusContainer text-center",
        icon: plus,
        needOnlyIcon: true,
        iconClass: "img-fluid xCircleIcon",
      },
      {
        id: 11,
        tableCellContent: null,
        tableSubClass: "sanadPlusContainer text-center",
        icon: plus,
        needOnlyIcon: true,
        iconClass: "img-fluid xCircleIcon",
      },
      {
        id: 12,
        tableCellContent: null,
        tableSubClass: "sanadPlusContainer text-center",
        icon: plus,
        needOnlyIcon: true,
        iconClass: "img-fluid xCircleIcon",
      },
      {
        id: 13,
        tableCellContent: null,
        tableSubClass: "sanadPlus-bottom-Container text-center",
        icon: plus,
        needOnlyIcon: true,
        iconClass: "img-fluid xCircleIcon",
      },
    ],
  },
];

export const dialingCodes = [
  {
    code: "+966",
    image: KSAFlagImage,
  },
  {
    code: "+91",
    image: IndiaFlagImage,
  },
];
export const customerServiceFaqList = [
  {
    id: 1,
    question: "What is medical insurance?",
    answer:
      "It's important to distinguish between Medical insurance fraud/abuse and mistakes. Medical insurance fraud is an act of deception by misrepresentation of material facts, or silence when good faith requires expression, resulting in material damage to one who relies on it and the right to rely on it. Simply, it is obtaining something of value from someone else through deceit. By contrast, mistakes are made unknowingly by anyone who lacks knowledge, in other words, it’s a misstatement or unintentional deviation from the truth. Therefore, Tawuniya always recommend reading the Medical Insurance Guidelines and visiting Tawuniya social media accounts for more information about the ideal use of your Medical Card.",
  },

  {
    id: 2,
    question: "How can I obtain medical insurance?",
    answer:
      "It's important to distinguish between Medical insurance fraud/abuse and mistakes. Medical insurance fraud is an act of deception by misrepresentation of material facts, or silence when good faith requires expression, resulting in material damage to one who relies on it and the right to rely on it. Simply, it is obtaining something of value from someone else through deceit. By contrast, mistakes are made unknowingly by anyone who lacks knowledge, in other words, it’s a misstatement or unintentional deviation from the truth. Therefore, Tawuniya always recommend reading the Medical Insurance Guidelines and visiting Tawuniya social media accounts for more information about the ideal use of your Medical Card.",
  },

  {
    id: 3,
    question: "How does fraud impact you?",
    answer:
      "It's important to distinguish between Medical insurance fraud/abuse and mistakes. Medical insurance fraud is an act of deception by misrepresentation of material facts, or silence when good faith requires expression, resulting in material damage to one who relies on it and the right to rely on it. Simply, it is obtaining something of value from someone else through deceit. By contrast, mistakes are made unknowingly by anyone who lacks knowledge, in other words, it’s a misstatement or unintentional deviation from the truth. Therefore, Tawuniya always recommend reading the Medical Insurance Guidelines and visiting Tawuniya social media accounts for more information about the ideal use of your Medical Card.",
  },
  {
    id: 4,
    question: "What is medical fraud unit?",
    answer:
      "It's important to distinguish between Medical insurance fraud/abuse and mistakes. Medical insurance fraud is an act of deception by misrepresentation of material facts, or silence when good faith requires expression, resulting in material damage to one who relies on it and the right to rely on it. Simply, it is obtaining something of value from someone else through deceit. By contrast, mistakes are made unknowingly by anyone who lacks knowledge, in other words, it’s a misstatement or unintentional deviation from the truth. Therefore, Tawuniya always recommend reading the Medical Insurance Guidelines and visiting Tawuniya social media accounts for more information about the ideal use of your Medical Card.",
  },
];
export const myFamilyExclusive = [
  {
    id: 1,
    question: "What is insurance fraud?",
    answer:
      "The insured is provided with an Insurance Card and Bail Bond approved by the Traffic Police together with SANAD Insurance Policy to prevent the arrest or detainment of the driver causing the accident?",
  },
  {
    id: 2,
    question: "What is fraud vs. Mistake?",
    answer:
      "The insured is provided with an Insurance Card and Bail Bond approved by the Traffic Police together with SANAD Insurance Policy to prevent the arrest or detainment of the driver causing the accident?",
  },

  {
    id: 3,
    question: "How does fraud impact you?",
    answer:
      "The insured is provided with an Insurance Card and Bail Bond approved by the Traffic Police together with SANAD Insurance Policy to prevent the arrest or detainment of the driver causing the accident?",
  },
  {
    id: 4,
    question: "What is medical fraud unit?",
    answer:
      "The insured is provided with an Insurance Card and Bail Bond approved by the Traffic Police together with SANAD Insurance Policy to prevent the arrest or detainment of the driver causing the accident?",
  },
  {
    id: 5,
    question: "What kind of people/Organizations commits insurance fraud?",
    answer:
      "The insured is provided with an Insurance Card and Bail Bond approved by the Traffic Police together with SANAD Insurance Policy to prevent the arrest or detainment of the driver causing the accident?",
  },
  {
    id: 6,
    question: "What are some examples of insurance fraud/Abuse?",
    answer:
      "The insured is provided with an Insurance Card and Bail Bond approved by the Traffic Police together with SANAD Insurance Policy to prevent the arrest or detainment of the driver causing the accident?",
  },
  {
    id: 7,
    question: "What are we doing to prevent fraud?",
    answer:
      "The insured is provided with an Insurance Card and Bail Bond approved by the Traffic Police together with SANAD Insurance Policy to prevent the arrest or detainment of the driver causing the accident?",
  },
  {
    id: 8,
    question: "Shall I get a reward for reporting medical insurance fraud?",
    answer:
      "The insured is provided with an Insurance Card and Bail Bond approved by the Traffic Police together with SANAD Insurance Policy to prevent the arrest or detainment of the driver causing the accident?",
  },
];
export const HomePolicyList = [
  {
    id: 1,
    question: "What is insurance fraud?",
    answer:
      "The insured is provided with an Insurance Card and Bail Bond approved by the Traffic Police together with SANAD Insurance Policy to prevent the arrest or detainment of the driver causing the accident?",
  },
  {
    id: 2,
    question: "What is fraud vs. Mistake?",
    answer:
      "The insured is provided with an Insurance Card and Bail Bond approved by the Traffic Police together with SANAD Insurance Policy to prevent the arrest or detainment of the driver causing the accident?",
  },

  {
    id: 3,
    question: "How does fraud impact you?",
    answer:
      "The insured is provided with an Insurance Card and Bail Bond approved by the Traffic Police together with SANAD Insurance Policy to prevent the arrest or detainment of the driver causing the accident?",
  },
  {
    id: 4,
    question: "What is medical fraud unit?",
    answer:
      "The insured is provided with an Insurance Card and Bail Bond approved by the Traffic Police together with SANAD Insurance Policy to prevent the arrest or detainment of the driver causing the accident?",
  },
  {
    id: 5,
    question: "What kind of people/Organizations commits insurance fraud?",
    answer:
      "The insured is provided with an Insurance Card and Bail Bond approved by the Traffic Police together with SANAD Insurance Policy to prevent the arrest or detainment of the driver causing the accident?",
  },
  {
    id: 6,
    question: "What are some examples of insurance fraud/Abuse?",
    answer:
      "The insured is provided with an Insurance Card and Bail Bond approved by the Traffic Police together with SANAD Insurance Policy to prevent the arrest or detainment of the driver causing the accident?",
  },
  {
    id: 7,
    question: "What are we doing to prevent fraud?",
    answer:
      "The insured is provided with an Insurance Card and Bail Bond approved by the Traffic Police together with SANAD Insurance Policy to prevent the arrest or detainment of the driver causing the accident?",
  },
  {
    id: 8,
    question: "Shall I get a reward for reporting medical insurance fraud?",
    answer:
      "The insured is provided with an Insurance Card and Bail Bond approved by the Traffic Police together with SANAD Insurance Policy to prevent the arrest or detainment of the driver causing the accident?",
  },
];
export const PolicyList = [
  {
    image: HealthPolicy,
    title: "Check Your policy details",
  },
  {
    image: HealthPolicy,
    title: "Add/Delete members",
  },
  {
    image: HealthPolicy,
    title: "Upgrade existing members ",
  },
  {
    image: HealthPolicy,
    title: "Update members information",
  },
];

export const dashboardEligibilityHeaderData = {
  headerBannerTitle: "Health Services",
  headerBannerPara: "Eligibility letter",
  url: "/dashboard/service/eligibility-letter/new-request",
};
export const motorFraudQuestions = [
  {
    id: 1,
    question: "What is insurance fraud?",
    answer:
      "It's important to distinguish between Medical insurance fraud/abuse and mistakes. Medical insurance fraud is an act of deception by misrepresentation of material facts, or silence when good faith requires expression, resulting in material damage to one who relies on it and the right to rely on it. Simply, it is obtaining something of value from someone else through deceit. By contrast, mistakes are made unknowingly by anyone who lacks knowledge, in other words, it’s a misstatement or unintentional deviation from the truth. Therefore, Tawuniya always recommend reading the Medical Insurance Guidelines and visiting Tawuniya social media accounts for more information about the ideal use of your Medical Card.",
  },

  {
    id: 2,
    question: "What is fraud vs. Mistake?",
    answer:
      "It's important to distinguish between Medical insurance fraud/abuse and mistakes. Medical insurance fraud is an act of deception by misrepresentation of material facts, or silence when good faith requires expression, resulting in material damage to one who relies on it and the right to rely on it. Simply, it is obtaining something of value from someone else through deceit. By contrast, mistakes are made unknowingly by anyone who lacks knowledge, in other words, it’s a misstatement or unintentional deviation from the truth. Therefore, Tawuniya always recommend reading the Medical Insurance Guidelines and visiting Tawuniya social media accounts for more information about the ideal use of your Medical Card.",
  },

  {
    id: 3,
    question: "How does fraud impact you?",
    answer:
      "It's important to distinguish between Medical insurance fraud/abuse and mistakes. Medical insurance fraud is an act of deception by misrepresentation of material facts, or silence when good faith requires expression, resulting in material damage to one who relies on it and the right to rely on it. Simply, it is obtaining something of value from someone else through deceit. By contrast, mistakes are made unknowingly by anyone who lacks knowledge, in other words, it’s a misstatement or unintentional deviation from the truth. Therefore, Tawuniya always recommend reading the Medical Insurance Guidelines and visiting Tawuniya social media accounts for more information about the ideal use of your Medical Card.",
  },
  {
    id: 4,
    question: "What is medical fraud unit?",
    answer:
      "It's important to distinguish between Medical insurance fraud/abuse and mistakes. Medical insurance fraud is an act of deception by misrepresentation of material facts, or silence when good faith requires expression, resulting in material damage to one who relies on it and the right to rely on it. Simply, it is obtaining something of value from someone else through deceit. By contrast, mistakes are made unknowingly by anyone who lacks knowledge, in other words, it’s a misstatement or unintentional deviation from the truth. Therefore, Tawuniya always recommend reading the Medical Insurance Guidelines and visiting Tawuniya social media accounts for more information about the ideal use of your Medical Card.",
  },
];

export const SocialResponsibilityDetailsData = [
  {
    id: 1,
    tagTitle: "Productivity",
    date: "10 May 2022",
    description:
      "Tawuniya is the first company in the Kingdom to install vehicle insurance policies",
  },
  {
    id: 2,
    tagTitle: "Environmental",
    date: "10 May 2022",
    description:
      "Tawuniya is the first company in the Kingdom to install vehicle insurance policies",
  },
  {
    id: 3,
    tagTitle: "Productivity",
    date: "10 May 2022",
    description:
      "Tawuniya is the first company in the Kingdom to install vehicle insurance policies",
  },
  {
    id: 4,
    tagTitle: "Environmental",
    date: "10 May 2022",
    description:
      "Tawuniya is the first company in the Kingdom to install vehicle insurance policies",
  },
];
export const MediaCenterLatestNews = [
  {
    id: 1,
    tagTitle: "News",
    date: "Week ago",
    description: "Tawuniya launches Covid-19 Travel Insurance program",
  },
  {
    id: 2,
    tagTitle: "News",
    date: "Week ago",
    description: "Tawuniya launches Covid-19 Travel Insurance program",
  },
  {
    id: 3,
    tagTitle: "News",
    date: "Week ago",
    description: "Tawuniya launches Covid-19 Travel Insurance program",
  },
  {
    id: 4,
    tagTitle: "News",
    date: "Week ago",
    description: "Tawuniya launches Covid-19 Travel Insurance program",
  },
];

export const progressRoadData = [
  {
    id: 0,
    number: 1,
    title: "Submit Application",
    para: "Complete your request details and attached the needed documents",
  },
  {
    id: 1,
    number: 2,
    title: "Review",
    para: "The application will be reviewed in 3 days by our team",
    paddClass: "pl-3 pr-3",
  },
  {
    id: 2,
    number: 3,
    title: "Get Reimburse",
    para: "Reimbursement will be deposited to your bank account within 3 working days after approval",
  },
];

export const progressTeleMedData = [
  {
    id: 0,
    number: 1,
    title: "Submit Application",
    para: "Complete your consultation details",
  },
  {
    id: 1,
    number: 2,
    title: "Review the Call",
    para: "Receive reliable medical consultation from accredited doctors in all health specialties.",
    paddClass: "pl-3 pr-3",
  },
];

export const roadAssisstantContentData = {
  bannerTitle: "Road Assistance",
  bannerPara:
    "Sample text: 3 simple steps to reimburse the expenses outside the network Upon receiving the medical treatment outside the appointed network, the member will pay directly to the medical provider. He/she can apply for reimbursement of his/her medical expenses by submitting a request via Tawuniya's website or Tawuniya App and uploading all necessary support documents. Tawuniya will settle the claim and the funds will be transferred to his/her bank account.",
  bannerImg: carBanner,
  bannerClass: "road-sub-home-container",
  bannerImgClass: "road-assisstant-carBanner",
  benifitTitle: "How do I benefit from the service?",
  benifitPara: "3 simple steps to reimburse the expenses",
  faqTitle: "Frequently Asked Questions",
  faqPara:
    "Review answers to commonly asked questions at Tawuniya, which enable you to be directly involved in improving our support experience.",
  progressLineClass: "benifitPrgressLine",
};

export const teleMedicineContentData = {
  bannerTitle: "Telemedicine",
  bannerPara:
    "With the Doctor Consultation service, you will no longer need to book an appointment in the hospital. This service allows you to book a reliable online consultation appointment from accredited doctors in all health specialties via the smartphone app and website.",
  bannerImg: teleMedBanner,
  bannerClass: "teleMedicine-sub-home-container",
  bannerImgClass: "teleMedicine-carBanner",
  benifitTitle: "How it works?",
  benifitPara: "3 simple steps to reimburse the expenses",
  faqTitle: "Frequently Asked Questions",
  faqPara:
    "Review answers to commonly asked questions at Tawuniya, which enable you to be directly involved in improving our support experience.",
  progressLineClass: "benifitTeleMedProgressLine",
};

export const periodicInspectContentData = {
  bannerTitle: "Road Assistance",
  bannerPara:
    "Sample text: 3 simple steps to reimburse the expenses outside the network Upon receiving the medical treatment outside the appointed network, the member will pay directly to the medical provider. He/she can apply for reimbursement of his/her medical expenses by submitting a request via Tawuniya's website or Tawuniya App and uploading all necessary support documents. Tawuniya will settle the claim and the funds will be transferred to his/her bank account.",
  bannerImg: carBanner,
  bannerClass: "periodicInspect-sub-home-container",
  bannerImgClass: "periodicInspect-carBanner",
  benifitTitle: "How do I benefit from the service?",
  benifitPara: "3 simple steps to reimburse the expenses",
  faqTitle: "Frequently Asked Questions",
  faqPara:
    "Review answers to commonly asked questions at Tawuniya, which enable you to be directly involved in improving our support experience.",
  progressLineClass: "benifitPrgressLine",
};

export const carWashContentData = {
  bannerTitle: "Road Assistance",
  bannerPara:
    "Sample text: 3 simple steps to reimburse the expenses outside the network Upon receiving the medical treatment outside the appointed network, the member will pay directly to the medical provider. He/she can apply for reimbursement of his/her medical expenses by submitting a request via Tawuniya's website or Tawuniya App and uploading all necessary support documents. Tawuniya will settle the claim and the funds will be transferred to his/her bank account.",
  bannerImg: carBanner,
  bannerClass: "carWash-sub-home-container",
  bannerImgClass: "carWash-carBanner",
  benifitTitle: "How it works?",
  benifitPara: "3 simple steps to reimburse the expenses",
  faqTitle: "Frequently Asked Questions",
  faqPara:
    "Review answers to commonly asked questions at Tawuniya, which enable you to be directly involved in improving our support experience.",
  progressLineClass: "benifitPrgressLine",
};

export const faqRoadDashboardData = [
  {
    id: 0,
    evenKey: "0",
    question: "What kind of people/Organizations commits insurance fraud?",
    answer:
      "The insured is provided with an Insurance Card and Bail Bond approved by the Traffic Police together with SANAD Insurance Policy to prevent the arrest or detainment of the driver causing the accident?",
  },
  {
    id: 1,
    evenKey: "1",
    question: "What are some examples of insurance fraud/Abuse?",
    answer:
      "It's important to distinguish between Medical insurance fraud/abuse and mistakes. Medical insurance fraud is an act of deception by misrepresentation of material facts, or silence when good faith requires expression, resulting in material damage to one who relies on it and the right to rely on it. Simply, it is obtaining something of value from someone else through deceit. By contrast, mistakes are made unknowingly by anyone who lacks knowledge, in other words, it’s a misstatement or unintentional deviation from the truth. Therefore, Tawuniya always recommend reading the Medical Insurance Guidelines and visiting Tawuniya social media accounts for more information about the ideal use of your Medical Card.",
  },
  {
    id: 2,
    evenKey: "1",
    question: "What are we doing to prevent fraud?",
    answer:
      "It's important to distinguish between Medical insurance fraud/abuse and mistakes. Medical insurance fraud is an act of deception by misrepresentation of material facts, or silence when good faith requires expression, resulting in material damage to one who relies on it and the right to rely on it. Simply, it is obtaining something of value from someone else through deceit. By contrast, mistakes are made unknowingly by anyone who lacks knowledge, in other words, it’s a misstatement or unintentional deviation from the truth. Therefore, Tawuniya always recommend reading the Medical Insurance Guidelines and visiting Tawuniya social media accounts for more information about the ideal use of your Medical Card.",
  },
  {
    id: 3,
    evenKey: "1",
    question: "Shall I get a reward for reporting medical insurance fraud?",
    answer:
      "It's important to distinguish between Medical insurance fraud/abuse and mistakes. Medical insurance fraud is an act of deception by misrepresentation of material facts, or silence when good faith requires expression, resulting in material damage to one who relies on it and the right to rely on it. Simply, it is obtaining something of value from someone else through deceit. By contrast, mistakes are made unknowingly by anyone who lacks knowledge, in other words, it’s a misstatement or unintentional deviation from the truth. Therefore, Tawuniya always recommend reading the Medical Insurance Guidelines and visiting Tawuniya social media accounts for more information about the ideal use of your Medical Card.",
  },
];

export const faqPeriodicInspectDashboardData = [
  {
    id: 0,
    evenKey: "0",
    question: "What kind of people/Organizations commits insurance fraud?",
    answer:
      "The insured is provided with an Insurance Card and Bail Bond approved by the Traffic Police together with SANAD Insurance Policy to prevent the arrest or detainment of the driver causing the accident?",
  },
  {
    id: 1,
    evenKey: "1",
    question: "What are some examples of insurance fraud/Abuse?",
    answer:
      "It's important to distinguish between Medical insurance fraud/abuse and mistakes. Medical insurance fraud is an act of deception by misrepresentation of material facts, or silence when good faith requires expression, resulting in material damage to one who relies on it and the right to rely on it. Simply, it is obtaining something of value from someone else through deceit. By contrast, mistakes are made unknowingly by anyone who lacks knowledge, in other words, it’s a misstatement or unintentional deviation from the truth. Therefore, Tawuniya always recommend reading the Medical Insurance Guidelines and visiting Tawuniya social media accounts for more information about the ideal use of your Medical Card.",
  },
  {
    id: 2,
    evenKey: "1",
    question: "What are we doing to prevent fraud?",
    answer:
      "It's important to distinguish between Medical insurance fraud/abuse and mistakes. Medical insurance fraud is an act of deception by misrepresentation of material facts, or silence when good faith requires expression, resulting in material damage to one who relies on it and the right to rely on it. Simply, it is obtaining something of value from someone else through deceit. By contrast, mistakes are made unknowingly by anyone who lacks knowledge, in other words, it’s a misstatement or unintentional deviation from the truth. Therefore, Tawuniya always recommend reading the Medical Insurance Guidelines and visiting Tawuniya social media accounts for more information about the ideal use of your Medical Card.",
  },
  {
    id: 3,
    evenKey: "1",
    question: "Shall I get a reward for reporting medical insurance fraud?",
    answer:
      "It's important to distinguish between Medical insurance fraud/abuse and mistakes. Medical insurance fraud is an act of deception by misrepresentation of material facts, or silence when good faith requires expression, resulting in material damage to one who relies on it and the right to rely on it. Simply, it is obtaining something of value from someone else through deceit. By contrast, mistakes are made unknowingly by anyone who lacks knowledge, in other words, it’s a misstatement or unintentional deviation from the truth. Therefore, Tawuniya always recommend reading the Medical Insurance Guidelines and visiting Tawuniya social media accounts for more information about the ideal use of your Medical Card.",
  },
];

export const faqCarWashDashboardData = [
  {
    id: 0,
    evenKey: "0",
    question: "What kind of people/Organizations commits insurance fraud?",
    answer:
      "The insured is provided with an Insurance Card and Bail Bond approved by the Traffic Police together with SANAD Insurance Policy to prevent the arrest or detainment of the driver causing the accident?",
  },
  {
    id: 1,
    evenKey: "1",
    question: "What are some examples of insurance fraud/Abuse?",
    answer:
      "It's important to distinguish between Medical insurance fraud/abuse and mistakes. Medical insurance fraud is an act of deception by misrepresentation of material facts, or silence when good faith requires expression, resulting in material damage to one who relies on it and the right to rely on it. Simply, it is obtaining something of value from someone else through deceit. By contrast, mistakes are made unknowingly by anyone who lacks knowledge, in other words, it’s a misstatement or unintentional deviation from the truth. Therefore, Tawuniya always recommend reading the Medical Insurance Guidelines and visiting Tawuniya social media accounts for more information about the ideal use of your Medical Card.",
  },
  {
    id: 2,
    evenKey: "1",
    question: "What are we doing to prevent fraud?",
    answer:
      "It's important to distinguish between Medical insurance fraud/abuse and mistakes. Medical insurance fraud is an act of deception by misrepresentation of material facts, or silence when good faith requires expression, resulting in material damage to one who relies on it and the right to rely on it. Simply, it is obtaining something of value from someone else through deceit. By contrast, mistakes are made unknowingly by anyone who lacks knowledge, in other words, it’s a misstatement or unintentional deviation from the truth. Therefore, Tawuniya always recommend reading the Medical Insurance Guidelines and visiting Tawuniya social media accounts for more information about the ideal use of your Medical Card.",
  },
  {
    id: 3,
    evenKey: "1",
    question: "Shall I get a reward for reporting medical insurance fraud?",
    answer:
      "It's important to distinguish between Medical insurance fraud/abuse and mistakes. Medical insurance fraud is an act of deception by misrepresentation of material facts, or silence when good faith requires expression, resulting in material damage to one who relies on it and the right to rely on it. Simply, it is obtaining something of value from someone else through deceit. By contrast, mistakes are made unknowingly by anyone who lacks knowledge, in other words, it’s a misstatement or unintentional deviation from the truth. Therefore, Tawuniya always recommend reading the Medical Insurance Guidelines and visiting Tawuniya social media accounts for more information about the ideal use of your Medical Card.",
  },
];

export const dashboardTeleMedProgressData = [
  {
    id: 0,
    progressNo: "1",
    progresssTitle: "Submit Application",
    progressPara: "Complete your consultation details",
    class: "pr-3",
    arrowIcon: progressArrow,
  },
  {
    id: 1,
    progressNo: "2",
    progresssTitle: "Receive the Call",
    progressPara:
      "Receive reliable medical consultation from accredited doctors in all health specialties.",
    class: "pl-3",
    arrowIcon: progressArrow,
  },
];

export const dashboardPeriodicInspectionProgressData = [
  {
    id: 0,
    progressNo: "1",
    progresssTitle: "Submit Application",
    progressPara:
      "Complete your request details and attached the needed documents",
    class: "pr-3",
    arrowIcon: progressArrow,
  },
  {
    id: 1,
    progressNo: "2",
    progresssTitle: "Get Service",
    progressPara: "Receive the needed service.",
    class: "pl-3",
    arrowIcon: progressArrow,
  },
];

export const dashboardCarWashProgressData = [
  {
    id: 0,
    progressNo: "1",
    progresssTitle: "Submit Application",
    progressPara:
      "Complete your request details and attached the needed documents",
    class: "pr-3",
    arrowIcon: progressArrow,
  },
  {
    id: 1,
    progressNo: "2",
    progresssTitle: "Get Service",
    progressPara: "Receive the needed service.",
    class: "pl-3",
    arrowIcon: progressArrow,
  },
];

export const carMaintainContentData = {
  bannerTitle: "Road Assistance",
  bannerPara:
    "Sample text: 3 simple steps to reimburse the expenses outside the network Upon receiving the medical treatment outside the appointed network, the member will pay directly to the medical provider. He/she can apply for reimbursement of his/her medical expenses by submitting a request via Tawuniya's website or Tawuniya App and uploading all necessary support documents. Tawuniya will settle the claim and the funds will be transferred to his/her bank account.",
  bannerImg: carBanner,
  bannerClass: "carMaintain-sub-home-container",
  bannerImgClass: "carMaintain-carBanner",
  benifitTitle: "How it works?",
  benifitPara: "3 simple steps to reimburse the expenses",
  faqTitle: "Frequently Asked Questions",
  faqPara:
    "Review answers to commonly asked questions at Tawuniya, which enable you to be directly involved in improving our support experience.",
  progressLineClass: "benifitPrgressLine",
};

export const faqCarMaintainDashboardData = [
  {
    id: 0,
    evenKey: "0",
    question: "What kind of people/Organizations commits insurance fraud?",
    answer:
      "The insured is provided with an Insurance Card and Bail Bond approved by the Traffic Police together with SANAD Insurance Policy to prevent the arrest or detainment of the driver causing the accident?",
  },
  {
    id: 1,
    evenKey: "1",
    question: "What are some examples of insurance fraud/Abuse?",
    answer:
      "It's important to distinguish between Medical insurance fraud/abuse and mistakes. Medical insurance fraud is an act of deception by misrepresentation of material facts, or silence when good faith requires expression, resulting in material damage to one who relies on it and the right to rely on it. Simply, it is obtaining something of value from someone else through deceit. By contrast, mistakes are made unknowingly by anyone who lacks knowledge, in other words, it’s a misstatement or unintentional deviation from the truth. Therefore, Tawuniya always recommend reading the Medical Insurance Guidelines and visiting Tawuniya social media accounts for more information about the ideal use of your Medical Card.",
  },
  {
    id: 2,
    evenKey: "1",
    question: "What are we doing to prevent fraud?",
    answer:
      "It's important to distinguish between Medical insurance fraud/abuse and mistakes. Medical insurance fraud is an act of deception by misrepresentation of material facts, or silence when good faith requires expression, resulting in material damage to one who relies on it and the right to rely on it. Simply, it is obtaining something of value from someone else through deceit. By contrast, mistakes are made unknowingly by anyone who lacks knowledge, in other words, it’s a misstatement or unintentional deviation from the truth. Therefore, Tawuniya always recommend reading the Medical Insurance Guidelines and visiting Tawuniya social media accounts for more information about the ideal use of your Medical Card.",
  },
  {
    id: 3,
    evenKey: "1",
    question: "Shall I get a reward for reporting medical insurance fraud?",
    answer:
      "It's important to distinguish between Medical insurance fraud/abuse and mistakes. Medical insurance fraud is an act of deception by misrepresentation of material facts, or silence when good faith requires expression, resulting in material damage to one who relies on it and the right to rely on it. Simply, it is obtaining something of value from someone else through deceit. By contrast, mistakes are made unknowingly by anyone who lacks knowledge, in other words, it’s a misstatement or unintentional deviation from the truth. Therefore, Tawuniya always recommend reading the Medical Insurance Guidelines and visiting Tawuniya social media accounts for more information about the ideal use of your Medical Card.",
  },
];

export const dashboardCarMaintainHeaderData = {
  headerBannerTitle: "Motor Services",
  headerBannerPara: "Car Maintenance",
  url: "/dashboard/service/car-maintainance/new-request",
};

export const dashboardCarMaintainProgressData = [
  {
    id: 0,
    progressNo: "1",
    progresssTitle: "Submit Application",
    progressPara:
      "Complete your request details and attached the needed documents",
    class: "pr-3",
    arrowIcon: progressArrow,
  },
  {
    id: 1,
    progressNo: "2",
    progresssTitle: "Get Service",
    progressPara: "Receive the needed service.",
    class: "pl-3",
    arrowIcon: progressArrow,
  },
];

export const HomeChildVaccinateContentData = {
  bannerTitle: "Telemedicine",
  bannerPara:
    "Vaccinating your child at home Vaccination is essential for your child's safety and protection, which is part of our responsibility. Therefore, we provide vaccination service at home to Tawuniya members whose age between 0 – 7 years based on the basic vaccinations schedule issued by MOH and included in the Cooperative Health Insurance Uniﬁed Policy published by CCHI.",
  bannerImg: teleMedBanner,
  bannerClass: "homeVaccinate-sub-home-container",
  bannerImgClass: "homeVaccinate-carBanner",
  benifitTitle: "How it works?",
  benifitPara: "3 simple steps to reimburse the expenses",
  faqTitle: "Frequently Asked Questions",
  faqPara:
    "Review answers to commonly asked questions at Tawuniya, which enable you to be directly involved in improving our support experience.",
  progressLineClass: "benifitTeleMedProgressLine",
};

export const faqHomeChildVaccinateDashboardData = [
  {
    id: 0,
    evenKey: "0",
    question: "What kind of people/Organizations commits insurance fraud?",
    answer:
      "The insured is provided with an Insurance Card and Bail Bond approved by the Traffic Police together with SANAD Insurance Policy to prevent the arrest or detainment of the driver causing the accident?",
  },
  {
    id: 1,
    evenKey: "1",
    question: "What are some examples of insurance fraud/Abuse?",
    answer:
      "It's important to distinguish between Medical insurance fraud/abuse and mistakes. Medical insurance fraud is an act of deception by misrepresentation of material facts, or silence when good faith requires expression, resulting in material damage to one who relies on it and the right to rely on it. Simply, it is obtaining something of value from someone else through deceit. By contrast, mistakes are made unknowingly by anyone who lacks knowledge, in other words, it’s a misstatement or unintentional deviation from the truth. Therefore, Tawuniya always recommend reading the Medical Insurance Guidelines and visiting Tawuniya social media accounts for more information about the ideal use of your Medical Card.",
  },
  {
    id: 2,
    evenKey: "1",
    question: "What are we doing to prevent fraud?",
    answer:
      "It's important to distinguish between Medical insurance fraud/abuse and mistakes. Medical insurance fraud is an act of deception by misrepresentation of material facts, or silence when good faith requires expression, resulting in material damage to one who relies on it and the right to rely on it. Simply, it is obtaining something of value from someone else through deceit. By contrast, mistakes are made unknowingly by anyone who lacks knowledge, in other words, it’s a misstatement or unintentional deviation from the truth. Therefore, Tawuniya always recommend reading the Medical Insurance Guidelines and visiting Tawuniya social media accounts for more information about the ideal use of your Medical Card.",
  },
  {
    id: 3,
    evenKey: "1",
    question: "Shall I get a reward for reporting medical insurance fraud?",
    answer:
      "It's important to distinguish between Medical insurance fraud/abuse and mistakes. Medical insurance fraud is an act of deception by misrepresentation of material facts, or silence when good faith requires expression, resulting in material damage to one who relies on it and the right to rely on it. Simply, it is obtaining something of value from someone else through deceit. By contrast, mistakes are made unknowingly by anyone who lacks knowledge, in other words, it’s a misstatement or unintentional deviation from the truth. Therefore, Tawuniya always recommend reading the Medical Insurance Guidelines and visiting Tawuniya social media accounts for more information about the ideal use of your Medical Card.",
  },
];

export const dashboardHomeChildVaccinateHeaderData = {
  headerBannerTitle: "Health Services",
  headerBannerPara: "Home Child Vaccination",
};

export const dashboardHomeChildVaccinateProgressData = [
  {
    id: 0,
    progressNo: "1",
    progresssTitle: "Submit Application",
    progressPara: "Complete your consultation details",
    class: "pr-3",
    arrowIcon: progressArrow,
  },
  {
    id: 1,
    progressNo: "2",
    progresssTitle: "Receive the Call",
    progressPara:
      "Receive reliable medical consultation from accredited doctors in all health specialties.",
    class: "pl-3",
    arrowIcon: progressArrow,
  },
];

export const eligibilityContentData = {
  bannerTitle: "Telemedicine",
  bannerPara:
    "Your right to obtain treatment is reserved In case you have any problem while visiting the approved medical service provider, Tawuniya will issue an eligibility letter, conﬁrming your right to get the necessary treatment.",
  bannerImg: teleMedBanner,
  bannerClass: "eligibility-sub-home-container",
  bannerImgClass: "eligibility-carBanner",
  benifitTitle: "How it works?",
  benifitPara: "3 simple steps to reimburse the expenses",
  faqTitle: "Frequently Asked Questions",
  faqPara:
    "Review answers to commonly asked questions at Tawuniya, which enable you to be directly involved in improving our support experience.",
  progressLineClass: "benifitTeleMedProgressLine",
};

export const faqEligibilityDashboardData = [
  {
    id: 0,
    evenKey: "0",
    question: "What kind of people/Organizations commits insurance fraud?",
    answer:
      "The insured is provided with an Insurance Card and Bail Bond approved by the Traffic Police together with SANAD Insurance Policy to prevent the arrest or detainment of the driver causing the accident?",
  },
  {
    id: 1,
    evenKey: "1",
    question: "What are some examples of insurance fraud/Abuse?",
    answer:
      "It's important to distinguish between Medical insurance fraud/abuse and mistakes. Medical insurance fraud is an act of deception by misrepresentation of material facts, or silence when good faith requires expression, resulting in material damage to one who relies on it and the right to rely on it. Simply, it is obtaining something of value from someone else through deceit. By contrast, mistakes are made unknowingly by anyone who lacks knowledge, in other words, it’s a misstatement or unintentional deviation from the truth. Therefore, Tawuniya always recommend reading the Medical Insurance Guidelines and visiting Tawuniya social media accounts for more information about the ideal use of your Medical Card.",
  },
  {
    id: 2,
    evenKey: "1",
    question: "What are we doing to prevent fraud?",
    answer:
      "It's important to distinguish between Medical insurance fraud/abuse and mistakes. Medical insurance fraud is an act of deception by misrepresentation of material facts, or silence when good faith requires expression, resulting in material damage to one who relies on it and the right to rely on it. Simply, it is obtaining something of value from someone else through deceit. By contrast, mistakes are made unknowingly by anyone who lacks knowledge, in other words, it’s a misstatement or unintentional deviation from the truth. Therefore, Tawuniya always recommend reading the Medical Insurance Guidelines and visiting Tawuniya social media accounts for more information about the ideal use of your Medical Card.",
  },
  {
    id: 3,
    evenKey: "1",
    question: "Shall I get a reward for reporting medical insurance fraud?",
    answer:
      "It's important to distinguish between Medical insurance fraud/abuse and mistakes. Medical insurance fraud is an act of deception by misrepresentation of material facts, or silence when good faith requires expression, resulting in material damage to one who relies on it and the right to rely on it. Simply, it is obtaining something of value from someone else through deceit. By contrast, mistakes are made unknowingly by anyone who lacks knowledge, in other words, it’s a misstatement or unintentional deviation from the truth. Therefore, Tawuniya always recommend reading the Medical Insurance Guidelines and visiting Tawuniya social media accounts for more information about the ideal use of your Medical Card.",
  },
];

export const dashboardEligibilityProgressData = [
  {
    id: 0,
    progressNo: "1",
    progresssTitle: "Submit Request",
    progressPara:
      "In case there is any reason that prevents you from obtaining treatment from the approved service provider, contact Tawuniya on 8001249990, or submit a request through Tawuniya App. or send an SMS to 0501492222, including the member ID number.",
    class: "pr-3",
    arrowIcon: progressArrow,
  },
  {
    id: 1,
    progressNo: "2",
    progresssTitle: "Get Eligibility letter",
    progressPara:
      "Tawuniya will issue an immediate eligibility letter addressed to the service provider stating that Tawuniya has agreed to provide you the necessary treatment.",
    class: "pl-3",
    arrowIcon: progressArrow,
  },
];

export const pregnancyContentData = {
  bannerTitle: "Telemedicine",
  bannerPara:
    "An unforgettable pregnancy journey We will do our best to make your pregnancy journey full of memorable and beautiful memories through the pregnancy follow-up program that takes care of a pregnant mother and increases her awareness of all procedures and provide awarness tips during all the phases of pregnancy",
  bannerImg: teleMedBanner,
  bannerClass: "pregnancy-sub-home-container",
  bannerImgClass: "pregnancy-carBanner",
  benifitTitle: "How it works?",
  benifitPara: "3 simple steps to reimburse the expenses",
  faqTitle: "Frequently Asked Questions",
  faqPara:
    "Review answers to commonly asked questions at Tawuniya, which enable you to be directly involved in improving our support experience.",
  progressLineClass: "benifitTeleMedProgressLine",
};

export const faqPregnancyDashboardData = [
  {
    id: 0,
    evenKey: "0",
    question: "What kind of people/Organizations commits insurance fraud?",
    answer:
      "The insured is provided with an Insurance Card and Bail Bond approved by the Traffic Police together with SANAD Insurance Policy to prevent the arrest or detainment of the driver causing the accident?",
  },
  {
    id: 1,
    evenKey: "1",
    question: "What are some examples of insurance fraud/Abuse?",
    answer:
      "It's important to distinguish between Medical insurance fraud/abuse and mistakes. Medical insurance fraud is an act of deception by misrepresentation of material facts, or silence when good faith requires expression, resulting in material damage to one who relies on it and the right to rely on it. Simply, it is obtaining something of value from someone else through deceit. By contrast, mistakes are made unknowingly by anyone who lacks knowledge, in other words, it’s a misstatement or unintentional deviation from the truth. Therefore, Tawuniya always recommend reading the Medical Insurance Guidelines and visiting Tawuniya social media accounts for more information about the ideal use of your Medical Card.",
  },
  {
    id: 2,
    evenKey: "1",
    question: "What are we doing to prevent fraud?",
    answer:
      "It's important to distinguish between Medical insurance fraud/abuse and mistakes. Medical insurance fraud is an act of deception by misrepresentation of material facts, or silence when good faith requires expression, resulting in material damage to one who relies on it and the right to rely on it. Simply, it is obtaining something of value from someone else through deceit. By contrast, mistakes are made unknowingly by anyone who lacks knowledge, in other words, it’s a misstatement or unintentional deviation from the truth. Therefore, Tawuniya always recommend reading the Medical Insurance Guidelines and visiting Tawuniya social media accounts for more information about the ideal use of your Medical Card.",
  },
  {
    id: 3,
    evenKey: "1",
    question: "Shall I get a reward for reporting medical insurance fraud?",
    answer:
      "It's important to distinguish between Medical insurance fraud/abuse and mistakes. Medical insurance fraud is an act of deception by misrepresentation of material facts, or silence when good faith requires expression, resulting in material damage to one who relies on it and the right to rely on it. Simply, it is obtaining something of value from someone else through deceit. By contrast, mistakes are made unknowingly by anyone who lacks knowledge, in other words, it’s a misstatement or unintentional deviation from the truth. Therefore, Tawuniya always recommend reading the Medical Insurance Guidelines and visiting Tawuniya social media accounts for more information about the ideal use of your Medical Card.",
  },
];

export const dashboardPregnancyHeaderData = {
  headerBannerTitle: "Health Services",
  headerBannerPara: "Pregnancy Program",
};

export const dashboardPregnancyProgressData = [
  {
    id: 0,
    progressNo: "1",
    progresssTitle: "Submit Application",
    progressPara:
      "In case of acceptance, Tawuniya will develop an awareness plan for the pregnant woman and provide tips and consultation starting from joining the program until birth",
    class: "pr-3",
    arrowIcon: progressArrow,
  },
  {
    id: 1,
    progressNo: "2",
    progresssTitle: "Receive the Call",
    progressPara:
      "In case of acceptance, Tawuniya will develop an awareness plan for the pregnant woman and provide tips and consultation starting from joining the program until birth",
    class: "pl-3",
    arrowIcon: progressArrow,
  },
  {
    id: 2,
    progressNo: "3",
    progresssTitle: "Get Reimburse",
    progressPara:
      "In case of acceptance, Tawuniya will develop an awareness plan for the pregnant woman and provide tips and consultation starting from joining the program until birth",
    class: "pr-3",
    arrowIcon: progressArrow,
  },
];

export const reimBursmentContentData = {
  bannerTitle: "Telemedicine",
  bannerPara:
    "Reimburse your expenses outside the network While you are being treated outside the approved network, you will pay directly to the medical provider. In this case, you can apply for reimbursement of your medical expenses by submitting a request via Tawuniya’s website or Tawuniya App and uploading all necessary support documents. Tawuniya will settle the claim, and the funds will be transferred to your bank account",
  bannerImg: teleMedBanner,
  bannerClass: "reimBursment-sub-home-container",
  bannerImgClass: "reimBursment-carBanner",
  benifitTitle: "How it works?",
  benifitPara: "3 simple steps to reimburse the expenses",
  faqTitle: "Frequently Asked Questions",
  faqPara:
    "Review answers to commonly asked questions at Tawuniya, which enable you to be directly involved in improving our support experience.",
  progressLineClass: "benifitTeleMedProgressLine",
};

export const faqReimBursmentDashboardData = [
  {
    id: 0,
    evenKey: "0",
    question: "What kind of people/Organizations commits insurance fraud?",
    answer:
      "The insured is provided with an Insurance Card and Bail Bond approved by the Traffic Police together with SANAD Insurance Policy to prevent the arrest or detainment of the driver causing the accident?",
  },
  {
    id: 1,
    evenKey: "1",
    question: "What are some examples of insurance fraud/Abuse?",
    answer:
      "It's important to distinguish between Medical insurance fraud/abuse and mistakes. Medical insurance fraud is an act of deception by misrepresentation of material facts, or silence when good faith requires expression, resulting in material damage to one who relies on it and the right to rely on it. Simply, it is obtaining something of value from someone else through deceit. By contrast, mistakes are made unknowingly by anyone who lacks knowledge, in other words, it’s a misstatement or unintentional deviation from the truth. Therefore, Tawuniya always recommend reading the Medical Insurance Guidelines and visiting Tawuniya social media accounts for more information about the ideal use of your Medical Card.",
  },
  {
    id: 2,
    evenKey: "1",
    question: "What are we doing to prevent fraud?",
    answer:
      "It's important to distinguish between Medical insurance fraud/abuse and mistakes. Medical insurance fraud is an act of deception by misrepresentation of material facts, or silence when good faith requires expression, resulting in material damage to one who relies on it and the right to rely on it. Simply, it is obtaining something of value from someone else through deceit. By contrast, mistakes are made unknowingly by anyone who lacks knowledge, in other words, it’s a misstatement or unintentional deviation from the truth. Therefore, Tawuniya always recommend reading the Medical Insurance Guidelines and visiting Tawuniya social media accounts for more information about the ideal use of your Medical Card.",
  },
  {
    id: 3,
    evenKey: "1",
    question: "Shall I get a reward for reporting medical insurance fraud?",
    answer:
      "It's important to distinguish between Medical insurance fraud/abuse and mistakes. Medical insurance fraud is an act of deception by misrepresentation of material facts, or silence when good faith requires expression, resulting in material damage to one who relies on it and the right to rely on it. Simply, it is obtaining something of value from someone else through deceit. By contrast, mistakes are made unknowingly by anyone who lacks knowledge, in other words, it’s a misstatement or unintentional deviation from the truth. Therefore, Tawuniya always recommend reading the Medical Insurance Guidelines and visiting Tawuniya social media accounts for more information about the ideal use of your Medical Card.",
  },
];

export const dashboardReimBursmentHeaderData = {
  headerBannerTitle: "Health Services",
  headerBannerPara: "Medical Reimbursement",
};

export const dashboardReimBursmentProgressData = [
  {
    id: 0,
    progressNo: "1",
    progresssTitle: "Submit Application",
    progressPara:
      "Complete your request details and attached the needed documents",
    class: "pr-3",
    arrowIcon: progressArrow,
  },
  {
    id: 1,
    progressNo: "2",
    progresssTitle: "Review",
    progressPara: "The application will be reviewed in by our team",
    class: "pl-3",
    arrowIcon: progressArrow,
  },
  {
    id: 2,
    progressNo: "3",
    progresssTitle: "Get Reimburse",
    progressPara:
      "Reimbursement will be deposited to your bank account after approval",
    class: "pr-3",
    arrowIcon: progressArrow,
  },
];

export const loginTranslateData = {
  cardTitleEn: "Login & Join!",
  userIdPlaceHolderEn: "Saudi ID or Iqama Number",
  dobPlaceHolderEn: "Year of Birth",
  mobilePlaceHolderEn: "",
  continueBtnEn: "Continue",
  resetBtnEn: "Reset Mobile Number",
  cardTitleAr: "تسجيل الدخول والانضمام!",
  userIdPlaceHolderAr: "الهوية السعودية أو رقم الإقامة",
  dobPlaceHolderAr: " سنة الولادة",
  mobilePlaceHolderAr: "",
  continueBtnAr: "متابعة",
  termsAr:
    "من خلال الاستمرار ، فإنك توافق على شروط الخدمة وسياسة الخصوصية الخاصة بالتعاونية",
  resetBtnAr: "إعادة تعيين رقم الهاتف ",
};

export const loginVerifyTranslateData = {
  cardTitleEn: "Verification",
  cardParaEn:
    "An SMS will be sent to the following mobile number you have registered with us:",
  cardVerifyTitleEn: "Verification Code",
  verifyBtnEn: "Verify",
  resendTxtEn: "Resend code in 00:",
  cardTitleAr: "تحقق",
  cardParaAr:
    "سيتم إرسال رسالة نصية قصيرة إلى رقم الهاتف المحمول التالي الذي قمت بتسجيله لدينا:",
  cardVerifyTitleAr: "رمز التحقق",
  verifyBtnAr: "تحقق ",
  resendTxtAr: "إعادة إرسال الرمز في ",
  noResendTxtOneAr: "ل إعادة إرسال",
  noResendTxtTwoAr: "م تتلقى الرمز ؟",
};

export const navbarTranslateData = {};

export const MediaCenterLatestRelatedNews = [
  {
    buttonData: "News",
    date: "10 May 2022",
    description: "Tawuniya appoints Dr. Ammr Kurdi as Chief Financial Officer",
  },
  {
    buttonData: "News",
    date: "2 days ago",
    description:
      "Tawuniya is the first company in the Kingdom to install vehicle insurance policies",
  },
  {
    buttonData: "News",
    date: "Week ago",
    description: "Tawuniya launches Covid-19 Travel Insurance program",
  },
  {
    buttonData: "News",
    date: "2 days ago",
    description:
      "Tawuniya is the first company in the Kingdom to install vehicle insurance policies",
  },
  {
    buttonData: "News",
    date: "Week ago",
    description: "Tawuniya launches Covid-19 Travel Insurance program",
  },
];

export const industrialEnergyInsurance = [
  {
    id: 1,
    label: "Property Insurance: All Risk",
    bannerImage: engineerIcon13,
  },
  {
    id: 2,
    label: "Property Insurance: Fire And Additional Perils",
    bannerImage: engineerIcon14,
  },
  {
    id: 3,
    label: "Consequential Loss-Property Damage Insurance",
    bannerImage: engineerIcon15,
  },
  {
    id: 4,
    label: "Energy - All Large/ Major Risks Insurance",
    bannerImage: industrialIcon4,
  },
  {
    id: 5,
    label: "Machinery Breakdown Insurance",
    bannerImage: engineerIcon4,
  },
  {
    id: 6,
    label: "Loss of Profits Insurance following machinery breakdown",
    bannerImage: engineerIcon6,
  },
  {
    id: 7,
    label: "Boiler And Pressure Vessel Insurance",
    bannerImage: industrialIcon7,
  },
  {
    id: 8,
    label: "Public and Product Liability Insurance",
    bannerImage: engineerIcon7,
  },
  {
    id: 9,
    label: "Workmen's Compensation Insurance",
    bannerImage: engineerIcon9,
  },
  {
    id: 10,
    label: "Professional Indemnity Insurance",
    bannerImage: industrialIcon10,
  },
  {
    id: 11,
    label: "Fidelity Guarantee Insurance",
    bannerImage: engineerIcon12,
  },
  {
    id: 12,
    label: "Money Insurance",
    bannerImage: engineerIcon11,
  },
  {
    id: 13,
    label: "Contractors' All Risks Insurance",
    bannerImage: engineerIcon1,
  },
  {
    id: 14,
    label: "Contractors' Plant & Machinery Insurance",
    bannerImage: engineerIcon2,
  },
  {
    id: 15,
    label: "Erection All Risk Insurance",
    bannerImage: engineerIcon3,
  },
  {
    id: 16,
    label: "Sabotage & Terrorism Insurance",
    bannerImage: industrialIcon4,
  },
  {
    id: 17,
    label: "Marine Cargo Insurance",
    bannerImage: engineerIcon16,
  },
];

export const financialInvestmentInsurance = [
  {
    id: 1,
    label: "Public Liability Insurance",
    bannerImage: engineerIcon7,
  },
  {
    id: 2,
    label: "Property Insurance: All Risk",
    bannerImage: engineerIcon13,
  },
  {
    id: 3,
    label: "Property Insurance: Fire And Additional Perils",
    bannerImage: engineerIcon14,
  },
  {
    id: 4,
    label: "Professional Indemnity Insurance for Stock Brokers",
    bannerImage: engineerIcon8,
  },
  {
    id: 5,
    label: "Money Insurance",
    bannerImage: engineerIcon11,
  },
  {
    id: 6,
    label: "Fidelity Guarantee Insurance",
    bannerImage: engineerIcon12,
  },
  {
    id: 7,
    label: "Crime Bond Insurance",
    bannerImage: financialIcon7,
  },
  {
    id: 8,
    label: "Theft Insurance",
    bannerImage: financialIcon8,
  },
  {
    id: 9,
    label: "Business Travel Insurance",
    bannerImage: financialIcon9,
  },
  {
    id: 10,
    label: "Sabotage & Terrorism Insurance",
    bannerImage: industrialIcon16,
  },
  {
    id: 11,
    label: "Contractors' All Risks Insurance",
    bannerImage: engineerIcon1,
  },
  {
    id: 12,
    label: "Marine Cargo Insurance",
    bannerImage: engineerIcon16,
  },
];

export const retailTradingInsurance = [
  {
    id: 1,
    label: "Property Insurance: All Risk",
    bannerImage: engineerIcon13,
  },
  {
    id: 2,
    label: "Property Insurance: Fire And Additional Perils",
    bannerImage: engineerIcon14,
  },
  {
    id: 3,
    label: "Consequential Loss-Property Damage Insurance",
    bannerImage: engineerIcon15,
  },
  {
    id: 4,
    label: "Public and Product Liability Insurance",
    bannerImage: engineerIcon7,
  },
  {
    id: 5,
    label: "Theft Insurance",
    bannerImage: financialIcon8,
  },
  {
    id: 6,
    label: "Money Insurance",
    bannerImage: engineerIcon11,
  },
  {
    id: 7,
    label: "Credit Insurance",
    bannerImage: retailIcon7,
  },
  {
    id: 8,
    label: "Plate Glass Insurance",
    bannerImage: retailIcon8,
  },
  {
    id: 9,
    label: "Marine Cargo Insurance",
    bannerImage: engineerIcon16,
  },
  {
    id: 10,
    label: "Goods in Transit Insurance",
    bannerImage: retailIcon10,
  },
  {
    id: 11,
    label: "Workmen's Compensation Insurance",
    bannerImage: engineerIcon9,
  },
  {
    id: 12,
    label: "Deterioration of Stock in cold storage Insurance",
    bannerImage: retailIcon12,
  },
  {
    id: 13,
    label: "Airport Contractors Liability Insurance",
    bannerImage: retailIcon13,
  },
  {
    id: 14,
    label: "Fidelity Guarantee Insurance",
    bannerImage: engineerIcon12,
  },
  {
    id: 15,
    label: "Contractors' All Risks Insurance",
    bannerImage: engineerIcon1,
  },
];

export const servicesInsurance = [
  {
    id: 1,
    label: "Property Insurance: All Risk",
    bannerImage: engineerIcon13,
  },
  {
    id: 2,
    label: "Property Insurance: Fire And Additional Perils",
    bannerImage: engineerIcon14,
  },
  {
    id: 3,
    label: "Consequential Loss-Property Damage Insurance",
    bannerImage: engineerIcon15,
  },
  {
    id: 4,
    label: "Public and Product Liability Insurance",
    bannerImage: engineerIcon7,
  },
  {
    id: 5,
    label: "Theft Insurance",
    bannerImage: financialIcon8,
  },
  {
    id: 6,
    label: "Money Insurance",
    bannerImage: engineerIcon11,
  },
  {
    id: 7,
    label: "Credit Insurance",
    bannerImage: retailIcon7,
  },
  {
    id: 8,
    label: "Plate Glass Insurance",
    bannerImage: retailIcon8,
  },
  {
    id: 9,
    label: "Marine Cargo Insurance",
    bannerImage: engineerIcon16,
  },
  {
    id: 10,
    label: "Airport Contractors Liability Insurance",
    bannerImage: serviceIcon10,
  },
  {
    id: 11,
    label: "Fidelity Guarantee Insurance",
    bannerImage: engineerIcon12,
  },
  {
    id: 12,
    label: "Professional Indemnity Insurance",
    bannerImage: serviceIcon12,
  },
  {
    id: 13,
    label: "Personal Accident Insurance",
    bannerImage: serviceIcon13,
  },
  {
    id: 14,
    label: "Sabotage & Terrorism Insurance",
    bannerImage: industrialIcon16,
  },
  {
    id: 15,
    label: "Contractors' All Risks Insurance",
    bannerImage: engineerIcon1,
  },
];

export const telecommunicationITInsurance = [
  {
    id: 1,
    label: "Property Insurance: All Risk",
    bannerImage: engineerIcon13,
  },
  {
    id: 2,
    label: "Property Insurance: Fire And Additional Perils",
    bannerImage: engineerIcon14,
  },
  {
    id: 3,
    label: "Consequential Loss-Property Damage Insurance",
    bannerImage: engineerIcon15,
  },
  {
    id: 4,
    label: "Public Liability Insurance",
    bannerImage: engineerIcon7,
  },
  {
    id: 5,
    label: "Professional Indemnity Insurance",
    bannerImage: engineerIcon8,
  },
  {
    id: 6,
    label: "Electronic Equipments Insurance",
    bannerImage: engineerIcon10,
  },
  {
    id: 7,
    label: "Erection All Risks Insurance",
    bannerImage: engineerIcon3,
  },
  {
    id: 8,
    label: "Theft Insurance",
    bannerImage: financialIcon8,
  },
  {
    id: 9,
    label: "Fidelity Guarantee Insurance",
    bannerImage: engineerIcon12,
  },
  {
    id: 10,
    label: "Money Insurance",
    bannerImage: engineerIcon11,
  },
  {
    id: 11,
    label: "Plate Glass Insurance",
    bannerImage: retailIcon8,
  },
  {
    id: 12,
    label: "Personal Accident Insurance",
    bannerImage: serviceIcon13,
  },
  {
    id: 13,
    label: "Workmen's Compensation Insurance",
    bannerImage: engineerIcon9,
  },
  {
    id: 14,
    label: "Contractors' All Risks Insurance",
    bannerImage: engineerIcon1,
  },
  {
    id: 15,
    label: "Marine Cargo Insurance",
    bannerImage: engineerIcon16,
  },
];

export const transportationInsurance = [
  {
    id: 1,
    label: "Property Insurance: All Risk",
    bannerImage: engineerIcon13,
  },
  {
    id: 2,
    label: "Property Insurance: Fire And Additional Perils",
    bannerImage: engineerIcon14,
  },
  {
    id: 3,
    label: "Public Liability Insurance",
    bannerImage: engineerIcon7,
  },
  {
    id: 4,
    label: "Marine Cargo Insurance",
    bannerImage: engineerIcon16,
  },
  {
    id: 5,
    label: "Marine Hull Insurance",
    bannerImage: transportIcon5,
  },
  {
    id: 6,
    label: "Goods in Transit Insurance",
    bannerImage: retailIcon10,
  },
  {
    id: 7,
    label: "Aircraft Insurance",
    bannerImage: transportIcon7,
  },
  {
    id: 8,
    label: "Aircraft Hull & Liability Insurance",
    bannerImage: transportIcon8,
  },
  {
    id: 9,
    label: "Aircraft Hull “War and Allied Perils” Insurance",
    bannerImage: transportIcon9,
  },
  {
    id: 10,
    label: "Aircrew Loss of license Insurance",
    bannerImage: transportIcon10,
  },
  {
    id: 11,
    label: "Airport Contractors Liability Insurance",
    bannerImage: retailIcon13,
  },
  {
    id: 12,
    label: "Business Travel Insurance",
    bannerImage: financialIcon9,
  },
  {
    id: 13,
    label: "Money Insurance",
    bannerImage: engineerIcon11,
  },
  {
    id: 14,
    label: "Theft Insurance",
    bannerImage: financialIcon8,
  },
  {
    id: 15,
    label: "Professional Indemnity Insurance",
    bannerImage: engineerIcon8,
  },
  {
    id: 16,
    label: "Workmen's Compensation Insurance",
    bannerImage: engineerIcon9,
  },
  {
    id: 17,
    label: "Crime Bond Insurance",
    bannerImage: financialIcon7,
  },
  {
    id: 18,
    label: "Contractors' All Risks Insurance",
    bannerImage: engineerIcon1,
  },
];
export const engineeringContractorsInsuranceQueue = [
  {
    id: 1,
    label: "Contractors' All Risks Insurance",
  },
  {
    id: 2,
    label: "Contractors' Plant & Machinery Insurance",
  },
  {
    id: 3,
    label: "Erection All Risks Insurance",
  },
  {
    id: 4,
    label: "Machinery Breakdown Insurance",
  },
  {
    id: 5,
    label: "Civil Engineering Completed risk Insurance",
  },
  {
    id: 6,
    label: "Loss of Profits Insurance following machinery breakdown",
  },
  {
    id: 7,
    label: "Public Liability Insurance",
  },
  {
    id: 8,
    label: "Professional Indemnity Insurance - Architect’s & Civil Engineering",
  },
  {
    id: 9,
    label: "Workmen's Compensation Insurance",
  },
  {
    id: 10,
    label: "Electronic Equipments Insurance",
  },
  {
    id: 11,
    label: "Money Insurance",
  },
  {
    id: 12,
    label: "Fidelity Guarantee Insurance",
  },
  {
    id: 13,
    label: "Property Insurance: All Risk",
  },
  {
    id: 14,
    label: "Property Insurance: Fire And Additional Perils",
  },
  {
    id: 15,
    label: "Consequential Loss-Property Damage Insurance",
  },
  {
    id: 16,
    label: "Marine Cargo Insurance",
  },
];

export const IndustrialEnergyInsuranceQueue = [
  {
    id: 1,
    label: "Property Insurance: All Risk",
  },
  {
    id: 2,
    label: "Property Insurance: Fire And Additional Perils",
  },
  {
    id: 3,
    label: "Consequential Loss-Property Damage Insurance",
  },
  {
    id: 4,
    label: "Energy - All Large/ Major Risks Insurance",
  },
  {
    id: 5,
    label: "Machinery Breakdown Insurance",
  },
  {
    id: 6,
    label: "Loss Of Profits Insurance following machinery breakdown",
  },
  {
    id: 7,
    label: "Boiler And Pressure Vessel Insurance",
  },
  {
    id: 8,
    label: "Public and Product Liability Insurance",
  },
  {
    id: 9,
    label: "Workmen's Compensation Insurance",
  },
  {
    id: 10,
    label: "Professional Indemnity Insurance",
  },
  {
    id: 11,
    label: "Fidelity Guarantee Insurance",
  },
  {
    id: 12,
    label: "Money Insurance",
  },
  {
    id: 13,
    label: "Contractors' All Risks Insurance",
  },
  {
    id: 14,
    label: "Contractors' Plant & Machinery Insurance",
  },
  {
    id: 15,
    label: "Erection All Risks Insurance",
  },
  {
    id: 16,
    label: "Sabotage & Terrorism Insurance",
  },
  {
    id: 17,
    label: "Marine Cargo Insurance",
  },
];
export const engineeringKeyBenefits = [
  {
    id: 1,
    label: "Premium",
    image: gift,
  },
  {
    id: 2,
    label: "Starting at 500 SAR per employee",
    image: gift,
  },
  {
    id: 3,
    label: "Cashless Hospital",
    image: gift,
  },
  {
    id: 4,
    label: "Geographical Coverage",
    image: gift,
  },
  {
    id: 5,
    label: "6400+ Cashless Hospitals Across KSA",
    image: gift,
  },
];
export const engineeringHealthInsurancepolicy = [
  {
    id: 1,
    image: boost,
    title: "Boost Employee Retention",
    content:
      "Health insurance will not only give your employees and their families enough financial security, but an overall sense of satisfaction that their employer actually cares about them.",
  },
  {
    id: 2,
    image: strength,
    title: "Strengthen Employee Motivation",
    content:
      "Happy employees make happy workspaces and evidently successful companies! It’s no surprise that the safer and satisfied employees feel happier and motivated!",
  },
  {
    id: 3,
    image: boost,
    title: "Enhance their Mental Wellbeing",
    content:
      "Health insurance will not only safeguard the employees' savings but, also enhance their overall mental well-being with the right support",
  },
  {
    id: 4,
    image: boost,
    title: "Protect them from Severe Health Conditions",
    content:
      "Safeguard your employees from the same, amongst other diseases; the earlier these issues are diagnosed the earlier they can be treated and resolved.",
  },
  {
    id: 5,
    image: boost,
    title: "6400+ Cashless Hospitals",
    content:
      "With more than 6400+ cashless hospitals across KSA, your employees can be covered at ease no matter where they are!",
  },
];
export const engineeringQuesAndAnswer = [
  {
    id: 1,
    question: "What is insurance fraud?",
    answer:
      "The insured is provided with an Insurance Card and Bail Bond approved by the Traffic Police together with SANAD Insurance Policy to prevent the arrest or detainment of the driver causing the accident?",
  },
  {
    id: 2,
    question: "What is fraud vs. Mistake?",
    answer:
      "The insured is provided with an Insurance Card and Bail Bond approved by the Traffic Police together with SANAD Insurance Policy to prevent the arrest or detainment of the driver causing the accident?",
  },
  {
    id: 3,
    question: "How does fraud impact you?",
    answer:
      "The insured is provided with an Insurance Card and Bail Bond approved by the Traffic Police together with SANAD Insurance Policy to prevent the arrest or detainment of the driver causing the accident?",
  },
  {
    id: 4,
    question: "What is medical fraud unit?",
    answer:
      "The insured is provided with an Insurance Card and Bail Bond approved by the Traffic Police together with SANAD Insurance Policy to prevent the arrest or detainment of the driver causing the accident?",
  },
  {
    id: 5,
    question: "What kind of people/Organizations commits insurance fraud?",
    answer:
      "The insured is provided with an Insurance Card and Bail Bond approved by the Traffic Police together with SANAD Insurance Policy to prevent the arrest or detainment of the driver causing the accident?",
  },
  {
    id: 6,
    question: "What are some examples of insurance fraud/Abuse?",
    answer:
      "The insured is provided with an Insurance Card and Bail Bond approved by the Traffic Police together with SANAD Insurance Policy to prevent the arrest or detainment of the driver causing the accident?",
  },
  {
    id: 7,
    question: "What are we doing to prevent fraud?",
    answer:
      "The insured is provided with an Insurance Card and Bail Bond approved by the Traffic Police together with SANAD Insurance Policy to prevent the arrest or detainment of the driver causing the accident?",
  },
  {
    id: 8,
    question: "Shall I get a reward for reporting medical insurance fraud?",
    answer:
      "The insured is provided with an Insurance Card and Bail Bond approved by the Traffic Police together with SANAD Insurance Policy to prevent the arrest or detainment of the driver causing the accident?",
  },
];
export const engineeringQuesAndAnswerbottom = [
  {
    id: 1,
    question: "What is insurance fraud?",
    answer:
      "The insured is provided with an Insurance Card and Bail Bond approved by the Traffic Police together with SANAD Insurance Policy to prevent the arrest or detainment of the driver causing the accident?",
  },
  {
    id: 2,
    question: "What is fraud vs. Mistake?",
    answer:
      "The insured is provided with an Insurance Card and Bail Bond approved by the Traffic Police together with SANAD Insurance Policy to prevent the arrest or detainment of the driver causing the accident?",
  },
  {
    id: 3,
    question: "How does fraud impact you?",
    answer:
      "The insured is provided with an Insurance Card and Bail Bond approved by the Traffic Police together with SANAD Insurance Policy to prevent the arrest or detainment of the driver causing the accident?",
  },
  {
    id: 4,
    question: "What is medical fraud unit?",
    answer:
      "The insured is provided with an Insurance Card and Bail Bond approved by the Traffic Police together with SANAD Insurance Policy to prevent the arrest or detainment of the driver causing the accident?",
  },
];

export const engineeringContractorsInsurance = [
  {
    id: 1,
    label: "Contractors' All Risks Insurance",
    bannerImage: engineerIcon1,
  },
  {
    id: 2,
    label: "Contractors' Plant & Machinery Insurance",
    bannerImage: engineerIcon2,
  },
  {
    id: 3,
    label: "Erection All Risks Insurance",
    bannerImage: engineerIcon3,
  },
  {
    id: 4,
    label: "Machinery Breakdown Insurance",
    bannerImage: engineerIcon4,
  },
  {
    id: 5,
    label: "Civil Engineering Completed risk Insurance",
    bannerImage: engineerIcon5,
  },
  {
    id: 6,
    label: "Loss of Profits Insurance following machinery breakdown",
    bannerImage: engineerIcon6,
  },
  {
    id: 7,
    label: "Workmen's Compensation Insurance",
    bannerImage: engineerIcon7,
  },
  {
    id: 8,
    label: "Professional Indemnity Insurance",
    bannerImage: engineerIcon8,
  },
  {
    id: 9,
    label: "Workmen's Compensation Insurance",
    bannerImage: engineerIcon9,
  },
  {
    id: 10,
    label: "Electronic Equipments Insurance",
    bannerImage: engineerIcon10,
  },
  {
    id: 11,
    label: "Money Insurance",
    bannerImage: engineerIcon11,
  },
  {
    id: 12,
    label: "Fidelity Guarantee Insurance",
    bannerImage: engineerIcon12,
  },
  {
    id: 13,
    label: "Property Insurance: All Risk",
    bannerImage: engineerIcon13,
  },
  {
    id: 14,
    label: "Property Insurance: Fire And Additional Perils",
    bannerImage: engineerIcon14,
  },
  {
    id: 15,
    label: "Consequential Loss-Property Damage Insurance",
    bannerImage: engineerIcon15,
  },
  {
    id: 16,
    label: "Marine Cargo Insurance",
    bannerImage: engineerIcon16,
  },
];

export const FinancialInvestmentInsuranceQueue = [
  {
    id: 1,
    label: "Public Liability Insurance",
  },
  {
    id: 2,
    label: "Property Insurance: All Risk",
  },
  {
    id: 3,
    label: "Property Insurance: Fire and Additional Perils",
  },
  {
    id: 4,
    label: "Professional Indemnity Insurance for Stock Brokers",
  },
  {
    id: 5,
    label: "Money Insurance",
  },
  {
    id: 6,
    label: "Fidelity Guarantee Insurance",
  },
  {
    id: 7,
    label: "Crime Bond Insurance",
  },
  {
    id: 8,
    label: "Theft Insurance",
  },
  {
    id: 9,
    label: "Business Travel Insurance",
  },
  {
    id: 10,
    label: "Sabotage & Terrorism Insurance",
  },
  {
    id: 11,
    label: "Contractors' All Risks Insurance",
  },
  {
    id: 12,
    label: "Marine Cargo Insurance",
  },
];

export const RetailInsuranceQueue = [
  {
    id: 1,
    label: "Property Insurance: All Risk",
  },
  {
    id: 2,
    label: "Property Insurance: Fire And Additional Perils",
  },
  {
    id: 3,
    label: "Consequential Loss-Property Damage Insurance",
  },
  {
    id: 4,
    label: "Public and Product Liability Insurance",
  },
  {
    id: 5,
    label: "Theft Insurance",
  },
  {
    id: 6,
    label: "Money Insurance",
  },
  {
    id: 7,
    label: "Credit Insurance",
  },
  {
    id: 8,
    label: "Plate Glass Insurance",
  },
  {
    id: 9,
    label: "Marine Cargo Insurance",
  },
  {
    id: 10,
    label: "Goods in Transit Insurance",
  },
  {
    id: 11,
    label: "Workmen's Compensation Insurance",
  },
  {
    id: 12,
    label: "Deterioration of Stock in cold storage Insurance",
  },
  {
    id: 13,
    label: "Airport Contractors Liability Insurance",
  },
  {
    id: 14,
    label: "Fidelity Guarantee Insurance",
  },
  {
    id: 15,
    label: "Contractors' All Risks Insurance",
  },
];

export const ServicesInsuranceQueue = [
  {
    id: 1,
    label: "Property Insurance: All Risk",
  },
  {
    id: 2,
    label: "Property Insurance: Fire And Additional Perils",
  },
  {
    id: 3,
    label: "Consequential Loss-Property Damage Insurance",
  },
  {
    id: 4,
    label: "Public and Product Liability Insurance",
  },
  {
    id: 5,
    label: "Theft Insurance",
  },
  {
    id: 6,
    label: "Money Insurance",
  },
  {
    id: 7,
    label: "Credit Insurance",
  },
  {
    id: 8,
    label: "Plate Glass Insurance",
  },
  {
    id: 9,
    label: "Marine Cargo Insurance",
  },
  {
    id: 10,
    label: "Airport Contractors Liability Insurance",
  },
  {
    id: 11,
    label: "Fidelity Guarantee Insurance",
  },
  {
    id: 12,
    label: "Professional Indemnity Insurance",
  },
  {
    id: 13,
    label: "Personal Accident Insurance",
  },
  {
    id: 14,
    label: "Sabotage & Terrorism Insurance",
  },
  {
    id: 15,
    label: "Contractors' All Risks Insurance",
  },
];

export const TelecommunicationInsuranceQueue = [
  {
    id: 1,
    label: "Property Insurance: All Risk",
  },
  {
    id: 2,
    label: "Property Insurance: Fire And Additional Perils",
  },
  {
    id: 3,
    label: "Consequential Loss-Property Damage Insurance",
  },
  {
    id: 4,
    label: "Public Liability Insurance",
  },
  {
    id: 5,
    label: "Professional Indemnity Insurance",
  },
  {
    id: 6,
    label: "Electronic Equipments Insurance",
  },
  {
    id: 7,
    label: "Erection All Risks Insurance",
  },
  {
    id: 8,
    label: "Theft Insurance",
  },
  {
    id: 9,
    label: "Fidelity Guarantee Insurance",
  },
  {
    id: 10,
    label: "Money Insurance",
  },
  {
    id: 11,
    label: "Plate Glass Insurance",
  },
  {
    id: 12,
    label: "Personal Accident Insurance",
  },
  {
    id: 13,
    label: "Workmen's Compensation Insurance",
  },
  {
    id: 14,
    label: "Contractors' All Risks Insurance",
  },
  {
    id: 15,
    label: "Marine Cargo Insurance",
  },
];

export const TransportationInsuranceQueue = [
  {
    id: 1,
    label: "Property Insurance: All Risk",
  },
  {
    id: 2,
    label: "Property Insurance: Fire And Additional Perils",
  },
  {
    id: 3,
    label: "Public Liability Insurance",
  },
  {
    id: 4,
    label: "Marine Cargo Insurance",
  },
  {
    id: 5,
    label: "Marine Hull Insurance",
  },
  {
    id: 6,
    label: "Goods in Transit Insurance",
  },
  {
    id: 7,
    label: "Aircraft Insurance",
  },
  {
    id: 8,
    label: "Aircraft Hull & Liability Insurance",
  },
  {
    id: 9,
    label: "Aircraft Hull “War and Allied Perils” Insurance",
  },
  {
    id: 10,
    label: "Aircrew Loss of license Insurance",
  },
  {
    id: 11,
    label: "Airport Contractors Liability Insurance",
  },
  {
    id: 12,
    label: "Business Travel Insurance",
  },
  {
    id: 13,
    label: "Money Insurance",
  },
  {
    id: 14,
    label: "Theft Insurance",
  },
  {
    id: 15,
    label: "Professional Indemnity Insurance",
  },
  {
    id: 16,
    label: "Workmen's Compensation Insurance",
  },
  {
    id: 17,
    label: "Crime Bond Insurance",
  },
  {
    id: 18,
    label: "Contractors' All Risks Insurance",
  },
];
