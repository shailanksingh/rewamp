import ViolationsBanner from "../../../assets/svg/Violations.svg";

export const violationsBannerData = [
	{
		violationsTitle: "Violations",
		violationsPara1:
			"Any fraudulent acts, corruption, collusion, coercion, illegal behavior, misconduct, financial mismanagement, accounting abuses, the existence of a conflict of interest, any wrong behavior, illegal or unethical practices, or other violations of the applicable laws, regulations and instructions or cover up any of the above acts.",
		violationsPara2:
			"The Company urges its employees and stakeholders not to hesitate to report any violations because they are not sure of the accuracy of the report and whether this allegation can be proven or not, and that all employees of the Company and stakeholders are expected to refrain from rumors, irresponsible behavior and false allegations, and if this allegation is made in good faith, but not confirmed in the investigation, no action will be taken against the whistleblower.",
		btnLabel: "Report Violations",
		bannerPic: ViolationsBanner,
	},
];
