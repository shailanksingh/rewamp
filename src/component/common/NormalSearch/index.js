import React from "react";
import { useSelector } from "react-redux";
import "./NormalSearch.scss";
import ErrorComponent from "component/common/ErrorComponent";

export const NormalSearch = ({
  placeholder = "",
  onChange = {},
  value = "",
  name = "",
  handleInputSearch,
  className,
  leftIcon,
  needRightIcon = false,
  needLeftIcon = false,
  searchAligner,
  hideLeftIcon,
  errorMessage,
}) => {
  const getArabStore = useSelector((data) => data.languageReducer.languageArab);

  return (
    <>
      <div className="normal-search">
        {needLeftIcon && (
          <React.Fragment>
            {getArabStore ? (
              <div className="rightIconBox">
                <div className="rightIcon">
                  <img
                    src={leftIcon}
                    className="img-fluid Inputicon"
                    alt="leftIcon"
                  />
                </div>
              </div>
            ) : (
              <div className="leftIconBox">
                <div className="leftIcon" id={hideLeftIcon}>
                  <img
                    src={leftIcon}
                    className="img-fluid Inputicon"
                    alt="leftIcon"
                  />
                </div>
              </div>
            )}
          </React.Fragment>
        )}
        <input
          className={`${className} searchBox`}
          id={getArabStore && "userIdplaceHolderAlign"}
          name={name}
          type="text"
          value={value}
          placeholder={placeholder}
          onChange={(e) => {
            let body = {};
            let tempVal = e.target.value;

            body = {
              target: {
                name: e.target.name,
                value: tempVal,
              },
            };

            onChange(body);
          }}
        />
        {needRightIcon && (
          <span className="search-icon">
            <i
              className="icon-search fs-20"
              id={searchAligner}
              onClick={handleInputSearch}
            />
          </span>
        )}
      </div>
      {errorMessage &&
        <ErrorComponent message={errorMessage} />
      }
    </>
  );
};
