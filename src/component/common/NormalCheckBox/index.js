import React from "react";
import "./style.scss";

export const NormalCheckbox = ({
  className = "custom-checkbox",
  label = "",
  value = "",
  name = "",
  onChange,
  checked,
  disable = false,
  labelClassName = "label-txt fw-400 text-black mx-2 fs-16",
  labelLeft,
  defaultChecked,
}) => {
  return (
    <React.Fragment>
      <label className={className}>
        <span>
          {labelLeft ? <span className={labelClassName}>{labelLeft}</span> : ""}
        </span>
        <input
          className="checkbox"
          type="checkbox"
          name={name}
          value={value}
          checked={checked}
          disabled={disable}
          defaultChecked={defaultChecked}
          onChange={onChange}
        />
        <span className="label-txt">
          {label ? <span className={labelClassName}>{label}</span> : ""}
        </span>
      </label>
    </React.Fragment>
  );
};
