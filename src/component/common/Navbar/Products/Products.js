import React from "react";
import "./style.scss";
import ProductsIcon1 from "../../../../assets/images/menuicons/products-icon1.svg";
import ProductsIcon2 from "../../../../assets/images/menuicons/products-icon2.svg";
import ProductsIcon3 from "../../../../assets/images/menuicons/products-icon3.svg";
import ProductsIcon4 from "../../../../assets/images/menuicons/products-icon4.svg";
import ProductsIcon5 from "../../../../assets/images/menuicons/products-icon5.svg";
import ProductsIcon6 from "../../../../assets/images/menuicons/products-icon6.svg";
import ProductsIcon7 from "../../../../assets/images/menuicons/products-icon7.svg";
import ProductsIcon8 from "../../../../assets/images/menuicons/products-icon8.svg";
import ProductsIcon9 from "../../../../assets/images/menuicons/products-icon9.svg";
import ProductsIcon10 from "../../../../assets/images/menuicons/products-icon10.svg";
import ProductsIcon11 from "../../../../assets/images/menuicons/products-icon11.svg";
import ProductsIcon12 from "../../../../assets/images/menuicons/products-icon12.svg";
import ProductsIcon13 from "../../../../assets/images/menuicons/products-icon13.svg";
import ProductsIcon14 from "../../../../assets/images/menuicons/products-icon14.svg";
import ProductsIcon15 from "../../../../assets/images/menuicons/products-icon15.svg";
import ProductsIcon16 from "../../../../assets/images/menuicons/products-icon16.svg";
import ProductsIcon17 from "../../../../assets/images/menuicons/products-icon17.svg";
import ProductsIcon18 from "../../../../assets/images/menuicons/products-icon18.svg";
import ProductsIcon19 from "../../../../assets/images/menuicons/products-icon19.svg";
import DownArrowOrangeIcon from "../../../../assets/images/menuicons/down-arrow-orange.svg";
import RightArrowCircleIcon from "../../../../assets/images/menuicons/right-arrow-circle.svg";
import RightArrowIcon from "../../../../assets/images/menuicons/right-arrow.svg";
import { NavLink } from "react-router-dom";

let individualMenuList = [
	{
		title: "Motor",
		list: [
			{
				title: "Al Shamel",
				description: "Comprehensive cars insurance",
				image: ProductsIcon1,
				url: "/home-insurance/motor",
			},
			{
				title: "Sanad",
				description: "Motor vehicle liability insurance",
				image: ProductsIcon2,
				url: "/home-insurance/motor",
			},
			{
				title: "Sanad Plus",
				description: "Motor vehicle liability with extra features",
				image: ProductsIcon3,
				url: "/home-insurance/motor",
			},
			{
				title: "Motor Flex",
				description: "Motor third party with extra optional coverages",
				image: ProductsIcon4,
				url: "/home-insurance/motor",
			},
			{
				title: "Tawuniya Drive",
				description: "Tawuniya Drive",
				image: ProductsIcon5,
				url: "/home-insurance/motor",
			},
		],
	},
	{
		title: "Property & Casualty",
		list: [
			{
				title: "Home Insurance",
				description: "Insurance against theft, fire and additional risks",
				image: ProductsIcon6,
				url: "/home/homeinsurance",
			},
			{
				title: "International Travel Insurance",
				description: "Travel Insurance for Europe and rest of the world",
				image: ProductsIcon7,
				url: "/home/internationaltravelpage",
			},
			{
				title: "COVID-19 Travel Insurance",
				description: "COVID-19 Travel Insurance",
				image: ProductsIcon8,
				url: "#",
			},
			{
				title: "Medical Malpractice Insurance",
				description: "Medical Malpractice Insurance",
				image: ProductsIcon9,
				url: "/home/medicalmalpracticepage",
			},
			{
				title: "Shop Owners Insurance",
				description: "Shop Owners Insurance",
				image: ProductsIcon10,
				url: "#",
			},
		],
	},
	{
		title: "Medical & Takaful",
		list: [
			{
				title: "My Family",
				description: "Families medical insurance",
				image: ProductsIcon11,
				url: "/home/medicalmalpracticepage",
			},
			{
				title: "Visit Visa Medical Insurance",
				description: "Medical Insurance for Saudi Arabia Visitors",
				image: ProductsIcon12,
				url: "/home/medicalmalpracticepage",
			},
			{
				title: "Hajj Insurance Program",
				description: "For Foreign Pilgrims",
				image: ProductsIcon13,
				url: "/home/programs/hajjinsuranceprogram",
			},
			{
				title: "Umrah Insurance Program",
				description: "For Foreign Pilgrims",
				image: ProductsIcon14,
				url: "/home/programs/UmrahInsuranceProgram",
			},
		],
	},
];

let lessEmployeesMenuList = [
	{
		title: "360° Motor Insurance",
		description: "Health Insurance for Small & Medium Business",
		url: "#",
	},
	{
		title: "360° Health Insurance",
		description: "Health Insurance for Small & Medium Business",
		url: "#",
	},
	{
		title: "360° Property & Casuality Program",
		description: "Health Insurance for Small & Medium Business",
		url: "#",
	},
];

let moreEmployeesMenuList = [
	{
		title: "Motor",
		list: [
			{
				title: "Al Shamel",
				description: "Comprehensive cars insurance",
				image: ProductsIcon1,
				url: "https://beta.tawuniya.com.sa/web/#/motor",
			},
			{
				title: "Sanad",
				description: "Motor vehicle liability insurance",
				image: ProductsIcon2,
				url: "https://beta.tawuniya.com.sa/web/#/motor",
			},
		],
		cardPd: "pr-2",
	},
	{
		title: "Medical",
		list: [
			{
				title: "Balsam",
				description: "Medcial Insurance for Corporates",
				image: ProductsIcon11,
				url: "/home/medicalmalpracticepage",
			},
			{
				title: "Takaful",
				description: "Program for Employees Collaborative Insurance",
				image: ProductsIcon9,
				url: "/home/medicalmalpracticepage",
			},
		],
		cardPd: "pl-2",
	},
];

let moreEmployeesMenuLastList = [
	{
		title: "PROPERTY & CASUALTY",
		list: [
			{
				title: "Engineering & Contractors Insurance",
				description: "Health Insurance for Small & Medium Business",
				image: ProductsIcon17,
				url: "#",
			},
			{
				title: "Services Insurance",
				description: "Health Insurance for Small & Medium Business",
				image: ProductsIcon18,
				url: "#",
			},
			{
				title: "Industrial & Energy Insurance",
				description: "Health Insurance for Small & Medium Business",
				image: ProductsIcon19,
				url: "#",
			},
		],
	},
];

const Products = ({ setMenuOpen = () => "", currentMenu }) => {
	return (
		<div className="products-list-menu-root">
			{currentMenu === "Individuals" ? (
				<>
					<div className="products-list-menu">
						{individualMenuList?.map((menu, index) => {
							return (
								<div className="menu-list-section" key={index.toString()}>
									<div className="menu-list-title">
										{menu?.title} <img src={DownArrowOrangeIcon} alt="..." />
									</div>
									{menu?.list?.map((item, childIndex) => {
										return (
											<NavLink
												to={item?.url}
												className="menu-details"
												key={childIndex.toString()}
												onClick={() => setMenuOpen(null)}
											>
												<div className="image">
													<img src={item?.image} alt="..." />
												</div>
												<div className="text-content">
													<div className="title">{item?.title}</div>
													<div className="description">{item?.description}</div>
												</div>
											</NavLink>
										);
									})}
								</div>
							);
						})}
					</div>
					<div className="all-products">
						<div>
							View All Products <img src={RightArrowIcon} alt="..." />
						</div>
					</div>
				</>
			) : (
				<>
					<div className="products-list-menu-after-login">
						<div className="section-left">
							<div className="section-title">
								<img src={ProductsIcon15} alt="..." />
								<div>
									For Businesses with
									<br />
									MORE than <span>250</span> employees
								</div>
							</div>
							<div className="section-content">
								<div className="more-employees-list row">
									{moreEmployeesMenuList?.map(({ title, list }, index) => {
										return (
											<div className={`${list.cardPd} col-6`}>
												<div
													className="more-employees-box"
													key={index.toString()}
												>
													<div className="more-employees-title">{title}</div>
													{list.map((item, childIndex) => {
														return (
															<NavLink
																to={item?.url}
																className="more-employees"
																key={childIndex.toString()}
																onClick={() => setMenuOpen(null)}
															>
																<div className="image">
																	<img src={item?.image} alt="..." />
																</div>
																<div className="text-content">
																	<div className="title">{item?.title}</div>
																	<div className="description">
																		{item?.description}
																	</div>
																</div>
															</NavLink>
														);
													})}
												</div>
											</div>
										);
									})}
								</div>
								<div className="more-employees-list">
									{moreEmployeesMenuLastList?.map(({ title, list }, index) => {
										return (
											<div
												className="more-employees-box"
												key={index.toString()}
											>
												<div className="more-employees-title">{title}</div>
												{list.map((item, childIndex) => {
													return (
														<NavLink
															to={item?.url}
															className="more-employees"
															key={childIndex.toString()}
															onClick={() => setMenuOpen(null)}
														>
															<div className="image">
																<img src={item?.image} alt="..." />
															</div>
															<div className="text-content">
																<div className="title">{item?.title}</div>
																<div className="description">
																	{item?.description}
																</div>
															</div>
														</NavLink>
													);
												})}
											</div>
										);
									})}
								</div>
								<div className="all-products">
									<div>
										View All Products <img src={RightArrowIcon} alt="..." />
									</div>
								</div>
							</div>
						</div>
						<div className="section-right">
							<div className="section-title">
								<img src={ProductsIcon16} alt="..." />
								<div>
									For Businesses with
									<br />
									LESS than <span>250</span> employees
								</div>
							</div>
							<div className="section-content">
								<div className="less-employees-list">
									{lessEmployeesMenuList?.map((item, index) => {
										return (
											<NavLink
												to={item?.url}
												className="less-employees"
												key={index.toString()}
												onClick={() => setMenuOpen(null)}
											>
												<span className="title">{item?.title}</span>
												<span className="description">{item?.description}</span>
												<span className="image">
													<img src={RightArrowCircleIcon} alt="..." />
												</span>
											</NavLink>
										);
									})}
								</div>
								<div className="all-products">
									<div>
										View All Products <img src={RightArrowIcon} alt="..." />
									</div>
								</div>
							</div>
						</div>
					</div>
				</>
			)}
		</div>
	);
};

export default Products;
