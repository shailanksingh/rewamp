import React, {useEffect, useRef, useState} from "react";
import {useSelector} from "react-redux";
import {history} from "service/helpers";
import MenuPopup from "./MenuPopup";
import {NormalButton} from "../NormalButton";
import {NormalSearch} from "../NormalSearch";
import logo from "assets/svg/Tawuniya-Primary-Logo.svg";
import orangeArrow from "assets/svg/orangeArrow.svg";
import notifyIcon from "assets/svg/notifyIcon.svg";
import phoneIcon from "assets/svg/phoneCircleIcon.svg";
import mailIcon from "assets/svg/mail icon.svg";
import chatIcon from "assets/svg/chat icon.svg";
import searchIcon from "assets/svg/headerSearchLight.svg";
import WorldIcon from "assets/svg/world-icon.svg";
import "./navbar.scss";
import {useTranslation} from "react-i18next";
import {DetectOutsideClicks} from "../../../hooks";

export const Navbar = ({
                         handleToggler,
                         toggleLanguageCard,
                         toggleSupportCard,
                         toggleEmergencyCard,
                         toggleGetStartedCard,
                         navContent,
                       }) => {
  const [search, setSearch] = useState("");

  const [isSearchOpened, setIsSearchOpened] = useState(false);

  const [menuOpen, setMenuOpen] = useState(null);

  const [swap, setSwap] = useState(null);

  const changeImg = [notifyIcon, phoneIcon, mailIcon, chatIcon];

  const [transition, setTransition] = useState(0);

  const selectedLanguage = useSelector((data) => data.languageReducer.language);

  useEffect(() => {
  const body = document.getElementById('root')
    body.setAttribute('dir', `${selectedLanguage === 'arabic' ? 'rtl' : 'ltr'}`)
    body.classList.add(`${selectedLanguage === 'arabic' ? 'rtl' : 'ltr'}`)
    body.classList.remove(`${selectedLanguage !== 'arabic' ? 'rtl' : 'ltr'}`)
  }, [selectedLanguage]);

  const handleInputSearch = (e) => {
    setSearch(e.target.value);
  };

  useEffect(() => {
    setSwap(changeImg[transition]);
  }, [transition]);

  useEffect(() => {
    const timer = setTimeout(() => {
      if (changeImg.length === transition + 1) {
        setTransition(0);
      } else {
        setTransition(transition + 1);
      }
    }, 1500);
    return () => clearTimeout(timer);
  }, [transition, changeImg]);

  const homeNavLinks = [
    {
      id: 0,
      link: "/home",
    },
    {
      id: 1,
      link: "/home/customerservice",
    },
    {
      id: 2,
      link: "/home/customerservice/opencomplaint",
    },
    {
      id: 3,
      link: "/home/customerservice/medicalfraud",
    },
    {
      id: 4,
      link: "/home/customerservice/motorfraud",
    },
    {
      id: 5,
      link: "/home/customerservice/travelfraud",
    },
    {
      id: 6,
      link: "/home/customerservice/surplus",
    },
    {
      id: 7,
      link: "/home/customerservice/servicequestions",
    },
    {
      id: 8,
      link: "/home/customerservice/reportpage",
    },
    {
      id: 9,
      link: "/home/customerservice/violationspage",
    },
    {
      id: 10,
      link: "/home/customerservice/SupportRequestConfirmation",
    },
    {
      id: 11,
      link: "/home/motorpage",
    },
    {
      id: 12,
      link: "/home/mediacenter",
    },
    {
      id: 13,
      link: "/home/mediacenter/medianewscategory",
    },
    {
      id: 14,
      link: "/home/programs/hajjinsuranceprogram",
    },
    {
      id: 15,
      link: "/home/programs/UmrahInsuranceProgram",
    },
    {
      id: 16,
      link: "/home/aboutus",
    },
    {
      id: 17,
      link: "/home/aboutus/financialhighlights",
    },
    {
      id: 18,
      link: "/home/aboutus/directors",
    },
    {
      id: 19,
      link: "/home/social",
    },
    {
      id: 19,
      link: "/home/social/project",
    },
    {
      id: 20,
      link: "/home/internationaltravelpage",
    },
    {
      id: 21,
      link: "/home/medicalmalpracticepage",
    },
    {
      id: 22,
      link: "/home/home-insurance",
    },
    {
      id: 23,
      link: "/home/health-insurance",
    },
    {
      id: 24,
      link: "/home/homeinsurance",
    },
    {
      id: 25,
      link: "/home/contactus",
    },
    {
      id: 26,
      link: "/home/mediacenter/medianewsdetails",
    },
    {
      id: 27,
      link: "/home/customerservice/service-category",
    },
  ];

  const authNavLinks = [
    {
      id: 0,
      link: "/login",
    },
    {
      id: 1,
      link: "/login/verify",
    },
    {
      id: 2,
      link: "/register",
    },
    {
      id: 3,
      link: "/register/verify",
    },
    {
      id: 4,
      link: "/register/absher-authentication",
    },
  ];

  const searchResultData = [
    {
      id: 0,
      name: "Al Shamel",
      title: "Motor Insurance",
    },
    {
      id: 1,
      name: "Al Shamel",
      title: "360 Motor Insurance",
    },
    {
      id: 2,
      name: "Al Shamel",
      title: "Products",
    },
    {
      id: 3,
      name: "Al Shamel",
      title: "Motor Claim",
    },
    {
      id: 4,
      name: "Al Shamel",
      title: "Sanad Plus",
    },
  ];

  const suggestionData = [
    {
      id: 0,
      titleOne: "Motor insurance",
      titleTwo: "health insurance",
    },
    {
      id: 1,
      titleOne: "Covid-19",
      titleTwo: "travel insurance",
    },
  ];

  const {t} = useTranslation();

  const searchArea = useRef(null);
  const isClickOutside = DetectOutsideClicks(searchArea);

  useEffect(() => {
    if (isSearchOpened) {
      isClickOutside && setIsSearchOpened(false)
    }
  }, [isClickOutside])

  return (
    <div className="navbar" id="navbarDesktop">
      <div className="d-flex justify-content-between align-items-center w-100 h-100 pt-3">
        <div className="mb-2 mr-3">
          <img
            src={logo}
            className="img-fluid tawuniyaLogo cursor-pointer"
            alt="tawuniyaLogo"
            onClick={() => history.push("/home")}
          />
        </div>
        {navContent.map((item, index) => {
          return (
            <div className="pl-3" key={item.navText + index}>
              <p
                className="navLink-Text fs-16 fw-400"
                onClick={() => {
                  if (index === menuOpen) {
                    setMenuOpen(null);
                  } else {
                    setMenuOpen(index);
                  }
                }}
              >
                {item.navText}
              </p>
            </div>
          );
        })}
        <div className="d-flex justify-content-end w-100 align-items-center">
          <div className="d-flex justify-content-center align-items-center">
            <div className="pr-1">
              <div className="currently-selected-language" onClick={toggleLanguageCard}>
                {selectedLanguage === "arabic" ? "Ar" : "En"}
                <img
                  src={WorldIcon}
                  className="img-fluid languageBtnIcon"
                  alt="WorldIcon"
                />
              </div>
            </div>
            <div className="pr-lg-1" ref={searchArea}>
              {isSearchOpened ? (
                <React.Fragment>
                  <NormalSearch
                    className="headerSearch"
                    name="search"
                    value={search}
                    placeholder={t('navbar.searchPlaceholder')}
                    onChange={handleInputSearch}
                    needRightIcon={true}
                  />
                  <div className="searchResultBox mt-3">
                    <p className="fs-12 fw-800 search-ResultTitle">
                      {t('navbar.results')}
                    </p>
                    {search.length === 0 ? (
                      <p className="fs-10 fw-400 search-ResultPara">
                        {t('navbar.noResultFound')}
                      </p>
                    ) : (
                      <React.Fragment>
                        <p className="fs-10 fw-400 search-ResultPara">
                          {t('navbar.foundedResults')}
                          <span className="search-ResultName">
                                Al shamel
                              </span>
                          ”
                        </p>
                        {searchResultData.map((item, i) => {
                          return (
                            <div
                              className="d-flex justify-content-between resultContainer"
                              key={item.name + i}
                            >
                              <div className="py-2">
                                <p className="fs-10 fw-400 result_Name m-0">
                                  {item.name}
                                </p>
                              </div>
                              <div className="py-2">
                                <p className="fs-10 fw-400 result_Title m-0">
                                  {item.title}
                                </p>
                              </div>
                            </div>
                          );
                        })}
                        <div className="d-flex justify-content-end pt-2">
                          <div>
                                <span className="fs-10 fw-800 fullResultLink text-uppercase pr-1">
                                  {t('navbar.fullResults')}
                                </span>{" "}
                            <img
                              src={orangeArrow}
                              className="img-fluid fullResult-arrow"
                              alt="arrow"
                            />
                          </div>
                        </div>
                        <p className="fs-12 fw-800 mostUsed-Title pt-3">
                          Most Used
                        </p>
                        {suggestionData.map((item, i) => (
                            <div className="d-flex flex-row pb-2" key={item.titleOne + i}>
                              <div>
                                <p className="fs-10 fw-400 text-center result-suggestion m-0">
                                  {item.titleOne}
                                </p>
                              </div>
                              <div className="pl-2">
                                <p className="fs-10 fw-400 text-center result-suggestion m-0">
                                  {item.titleTwo}
                                </p>
                              </div>
                            </div>
                          )
                        )}
                      </React.Fragment>
                    )}
                  </div>
                </React.Fragment>
              ) : (
                <img
                  src={searchIcon}
                  className="img-fluid searchBtnIcon"
                  alt="searchicon"
                  onClick={() => setIsSearchOpened(!isSearchOpened)}
                />
              )}
            </div>

            <div className="pr-lg-2">
              <img
                src={swap}
                className="img-fluid notifyBtnIcon"
                onClick={toggleSupportCard}
                alt="notifyicon"
              />
            </div>
            {homeNavLinks.map((item, index) =>
                history.location.pathname === item.link && (
                  <React.Fragment key={item.link + index}>
                    <div className="pr-lg-3">
                      <NormalButton
                        label={t('navbar.emergency')}
                        className="getStartBtn"
                        onClick={toggleEmergencyCard}
                      />
                    </div>
                    <div>
                      <NormalButton
                        label={t('navbar.myServices')}
                        className="emergencyBtn"
                        onClick={toggleGetStartedCard}
                      />
                    </div>
                  </React.Fragment>
                )
            )}
            {authNavLinks.map((item, index) =>
                history.location.pathname === item.link && (
                  <div key={item.link + index}>
                    <div className="pl-lg-3">
                      <NormalButton
                        label={t('navbar.emergency')}
                        className="redEmerencyBtn"
                        onClick={toggleEmergencyCard}
                      />
                    </div>
                    <div className="pl-lg-3">
                      <NormalButton
                        label={t('navbar.myServices')}
                        className="emergencyBtn"
                        onClick={toggleGetStartedCard}
                      />
                    </div>
                  </div>
                )
            )}
          </div>
        </div>
      </div>
      {menuOpen !== null && (
        <MenuPopup
          navContent={navContent}
          menuIndex={menuOpen}
          setMenuOpen={setMenuOpen}
        />
      )}
    </div>
  );
};
