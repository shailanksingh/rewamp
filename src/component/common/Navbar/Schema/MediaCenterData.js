import MediaCenterImgCard1 from "../../../../assets/images/MediaCenterImgCard1.png";
import MediaCenterImgCard2 from "../../../../assets/images/MediaCenterImgCard2.png";
import MediaCenterImgCard3 from "../../../../assets/images/MediaCenterImgCard3.png";

export const MediaCenterData = [
	{
		MediaCenterImgCardData: [
			{
				Heading: `Standard & Poor's assigns Tawuniya an "A-" rating; outlook "stable"`,
				backGroundImg: `${MediaCenterImgCard1}`,
				ButtonEl: "News",
			},
			{
				Heading: "Tawuniya signs a health insurance contract with Al Hokair",
				backGroundImg: `${MediaCenterImgCard2}`,
				ButtonEl: "Events",
			},
			{
				Heading: "Tawuniya appoints Dr. Ammr Kurdi as Chief Financial Officer",
				backGroundImg: `${MediaCenterImgCard3}`,
				ButtonEl: "Promotions",
			},
			{
				Heading: "Tawuniya signs a health insurance contract with Al Hokair",
				backGroundImg: `${MediaCenterImgCard2}`,
				ButtonEl: "News",
			},
			{
				Heading: "Tawuniya signs a health insurance contract with Al Hokair",
				backGroundImg: `${MediaCenterImgCard2}`,
				ButtonEl: "Events",
			},
		],
	},
];
