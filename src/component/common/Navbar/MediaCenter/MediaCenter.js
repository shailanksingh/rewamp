import React, { useRef } from "react";
import RightArrowIcon from "../../../../assets/images/menuicons/right-arrow.svg";
import MediaImgCard from "../../../common/MediaCenterImgCard/MedaiCenterFeatureCard/index";
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import "./style.scss";
import { Theme, makeStyles } from "@material-ui/core";
import ArrowForward from "../../../../assets/svg/HomeServiceFroward1Arrow.svg";
import ArrowBack from "../../../../assets/svg/HomeServiceBackArrow.svg";
import { MediaCenterData } from "../Schema/MediaCenterData";

const useStyles = makeStyles((theme) => ({
  dots: {
    zIndex: 0,
    "& li.slick-active button::before": {
      color: "#EE7500",
    },
    "& li": {
      width: "12px",
      "& button::before": {
        fontSize: theme.typography.pxToRem(9),
        color: "#4C565C",
      },
    },
  },
}));

const MediaCenter = () => {
    const sliderRef = useRef(null);
    const classes = useStyles();
    const settings = {
        slidesToShow: 3,
        slidesToScroll: 1,
        arrows: false,
        dots: true,
        dotsClass: `slick-dots slickClass ${classes.dots}`,
    };
    return (
        <div className="menu-media-center-details">
            <div className="media-center-top">
                <div class="top-title">News Categories</div>
                <div className="top-section-links">
                    <a href="#">All News <img src={RightArrowIcon} alt="..." /></a>
                    <a href="#">News <img src={RightArrowIcon} alt="..." /></a>
                    <a href="#">Events <img src={RightArrowIcon} alt="..." /></a>
                    <a href="#">Promotion <img src={RightArrowIcon} alt="..." /></a>
                </div>
            </div>
            <div className="media-center-box">
                <div className="media-center-title">Featured News</div>
                <div className="mediaCenterFeatureSliderContainer">
                    <Slider
                    ref={sliderRef}
                    {...settings}
                    className="mediaCenterFeatureImgCard"
                    >
                    {MediaCenterData[0].MediaCenterImgCardData.map((item, i) => {
                        return <MediaImgCard Item={item} key={i} />;
                    })}
                    </Slider>

                    <div className="arrowDotContainer">
                        <div
                            className="arrowContainer"
                            onClick={() => sliderRef.current.slickPrev()}
                        >
                            <img src={ArrowBack} />
                        </div>
                        <div className="dotContainer">
                            {/* <p className="colorActive"></p>
                            <p className="dotEl"></p>
                            <p className="dotEl"></p>
                            <p className="dotEl"></p>
                            <p className="dotEl"></p> */}
                        </div>
                        <div
                            style={{ zIndex: 3 }}
                            className="arrowContainer"
                            onClick={() => sliderRef.current.slickNext()}
                        >
                            <img src={ArrowForward} />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default MediaCenter