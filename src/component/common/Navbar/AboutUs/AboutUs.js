import React from "react";
import "./style.scss";
import BannerImage from "../../../../assets/images/tawuniya-HQ1.png";
import RightArrowWhiteIcon from "../../../../assets/images/menuicons/right-arrow-white-sm.svg";
import RightArrowIcon from "../../../../assets/images/menuicons/right-arrow.svg";

const AboutUs = () => {
    return (
        <div className="menu-about-us-details">
            <div className="aboutus-banner-section" style={{ backgroundImage: "url(" + BannerImage + ")" }}>
                <div className="aboutus-banner-title">Our Mission & Values</div>
                <p>Exceed expectations through superior customer<br /> experience and service excellence.</p>
                <button className="orangeBtn">
                    Read The Full Story <img src={RightArrowWhiteIcon} alt="..." />
                </button>
            </div>
            <div className="aboutus-box-root">
                <div className="aboutus-box-content content-width-50">
                    <div className="aboutus-box-title">
                        Our Competitive Advantages
                    </div>
                    <div className="aboutus-box-links">
                        <div><a href="#">Financial Highlights of Tawuniya in 2021 <img src={RightArrowIcon} alt="..." /></a></div>
                        <div><a href="#">Financial Highlights of Tawuniya in 2021 <img src={RightArrowIcon} alt="..." /></a></div>
                        <div><a href="#">Tawuniya Awards & Ranking- 2021 <img src={RightArrowIcon} alt="..." /></a></div>
                    </div>
                </div>
                <div className="aboutus-box-content content-width-50">
                    <div className="aboutus-box-title">
                        Board Of Directors & Senior Executives
                    </div>
                    <div className="aboutus-box-links">
                        <div><a href="#">Board Of Directors (BOD) <img src={RightArrowIcon} alt="..." /></a></div>
                        <div><a href="#">Senior Executives <img src={RightArrowIcon} alt="..." /></a></div>
                    </div>
                </div>
                <div className="aboutus-box-content">
                    <div className="aboutus-box-title">
                        Social Responsibility
                    </div>
                    <div className="aboutus-box-links">
                        <div><a href="#">Adopting the CSR Strategy <img src={RightArrowIcon} alt="..." /></a></div>
                        <div><a href="#">Social Contributions in 2021 <img src={RightArrowIcon} alt="..." /></a></div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default AboutUs;