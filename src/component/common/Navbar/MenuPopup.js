import React, { useState } from "react";
import "./menupopup.scss";
// import IndividualsIcon from "../../../assets/images/menuicons/individuals.svg";
// import EnterpriseIcon from "../../../assets/images/menuicons/enterprise.svg";
// import SMEIcon from "../../../assets/images/menuicons/sme.svg";
// import InvestorIcon from "../../../assets/images/menuicons/investor.svg";
import RightArrowIcon from "../../../assets/images/menuicons/right-arrow.svg";
import RightArrowBlackIcon from "../../../assets/images/menuicons/right-arrow-black.svg";
import IndividualMenuIcon1 from "../../../assets/images/menuicons/individual-menu-icon-1.svg";
import EnterpriseMenuIcon1 from "../../../assets/images/menuicons/enterprise-menu-icon-1.svg";
import EnterpriseMenuIcon2 from "../../../assets/images/menuicons/enterprise-menu-icon-2.svg";
import EnterpriseMenuIcon3 from "../../../assets/images/menuicons/enterprise-menu-icon-3.svg";
import EnterpriseMenuIcon4 from "../../../assets/images/menuicons/enterprise-menu-icon-4.svg";
import EnterpriseMenuIcon5 from "../../../assets/images/menuicons/enterprise-menu-icon-5.svg";
import EnterpriseMenuIcon6 from "../../../assets/images/menuicons/enterprise-menu-icon-6.svg";
import FileIcon from "../../../assets/images/menuicons/file.svg";
import CreditCardIcon from "../../../assets/images/menuicons/credit-card.svg";
import DocumentIcon from "../../../assets/images/menuicons/document.svg";
import MenuDetailsImage1 from "../../../assets/images/menu-details-image1.png";
import MenuDetailsImage2 from "../../../assets/images/menu-details-image2.png";
import MenuDetailsImage3 from "../../../assets/images/menu-details-image3.png";
import MenuDetailsLogo1 from "../../../assets/images/menu-details-logo1.png";
import MenuDetailsLogo2 from "../../../assets/images/menu-details-logo2.png";
import MenuDetailsLogo3 from "../../../assets/images/menu-details-logo3.png";
import RightArrowWhiteIcon from "../../../assets/images/menuicons/right-arrow-white-sm.svg";
import MenuCloseIcon from "../../../assets/images/menuicons/menu-close.svg";
import { NavLink } from "react-router-dom";
import CustomerService from "./CustomerService/CustomerService";
import Products from "./Products/Products";
import LoyaltyPrograms from "./LoyaltyPrograms/LoyaltyPrograms";
import AboutUs from "./AboutUs/AboutUs";
import MediaCenter from "./MediaCenter/MediaCenter";

const rightSideMenuList = [
	{
		title: "Home",
		url: "/home",
	},
	{
		title: "Products",
	},
	{
		title: "Loyalty Programs",
	},
	{
		title: "Customer Service",
	},
	{
		title: "About Us",
	},
	{
		title: "Media Center",
	},
	{
		title: "Contact Us",
		url: "/home/contactus"
	},
];
const insiderMenuList = [
	{
		title: "Motor",
		url: "#",
	},
	{
		title: "Medical",
		url: "#",
	},
	{
		title: "Property & Casuality",
		url: "#",
	},
	{
		title: "Travel",
		url: "#",
	},
	{
		title: "Medical Malpractice",
		url: "#",
	},
];

const investorMenuList = [
	{
		title: "Products",
		url: "https://beta.tawuniya.com.sa/web/#/tpclaim",
	},
	{
		title: "Investor Relations",
		url: "#",
	},
	{
		title: "Analyst Coverage",
		url: "#",
	},
	{
		title: "Fact Sheet",
		url: "#",
	},
	{
		title: "Announcements",
		url: "#",
	},
	{
		title: "Financial Information",
		url: "#",
	},
	{
		title: "About Us",
		url: "/home/aboutus",
	},
	{
		title: "Share Information",
		url: "#",
	},
	{
		title: "Media Center",
		url: "/home/mediacenter",
	},
	{
		title: "Dividends",
		url: "#",
	},
	{
		title: "Contact Us",
		url: "/home/contactus",
	},
];

const investorStockDetails = {
	time: "14:29 30-05-2022",
	price: "61.60 ",
	lastprice: "+1.15",
	high: "62.30",
	low: "61.00",
	volume: "617,746",
	previousClose: "60.90",
	change: "+0.70",
};

const MenuPopup = (props) => {
	const isLoggedIn = true;
	const { menuIndex, setMenuOpen, navContent } = props;
	const [ currentListMenuIndex, setCurrentListMenuIndex ] = useState(null);
	const rightSideMenu = ({ isInsiderMenu = false } = {}) => {
		let menuList = isInsiderMenu ? insiderMenuList : rightSideMenuList;
		return (
			<div className="right-side-menu-list">
				{menuList?.map(({ title, url }, index) => {
					if (url) {
						return (
							<NavLink
								to={url}
								key={index.toString()}
								className="bold-menu"
								exact
								onClick={() => setMenuOpen(null)}
							>
								{title}{" "}
								<img src={RightArrowIcon} className="orange-color" alt="..." />{" "}
								<img
									className="black-color"
									src={RightArrowBlackIcon}
									alt="..."
								/>
							</NavLink>
						);
					} else {
						return (
							<div className={"bold-menu " + (currentListMenuIndex === index ? "active" : "")} key={index.toString()} onClick={() => setCurrentListMenuIndex(index)}>
								{title}{" "}
								<img src={RightArrowIcon} className="orange-color" alt="..." />{" "}
								<img
									className="black-color"
									src={RightArrowBlackIcon}
									alt="..."
								/>
							</div>
						);
					}
				})}
			</div>
		);
	};
	return (
		<div className="page-navbar-menu-desktop">
			<div className="page-navbar-title-section">
				<div className="left-section">
					<span>I'm Looking for</span>
					<div className="menu-tabs">
						{navContent.map((item, index) => {
							return (
								<div
									key={index.toString()}
									onClick={() => setMenuOpen(index)}
									className={index === menuIndex ? "active" : ""}
								>
									{item.navText}
								</div>
							);
						})}
					</div>
				</div>
				<div className="right-section">
					{isLoggedIn ? 
						<>
							<button className="greyBtn">
								SME's Login <img src={RightArrowWhiteIcon} alt="..." />
							</button>
							<button className="orangeBtn">
								Enterprise Login <img src={RightArrowWhiteIcon} alt="..." />
							</button>
						</> : 
						<button className="orangeBtn">
							My Account <img src={RightArrowWhiteIcon} alt="..." />
						</button>
					}
					<img
						src={MenuCloseIcon}
						className="close-icon"
						alt="Close"
						onClick={() => setMenuOpen(null)}
					/>
				</div>
			</div>
			{menuIndex === 0 && (
				<div className="individuals-menu">
					<div className="top-menu-section left-section-small">
						<div className="left-section">{rightSideMenu()}</div>
						<div className="right-section">
							{/* <div className="menu-title-section">
								<div className="image">
								<img src={IndividualsIcon} alt="..." />
								</div>
								<div className="title-content">
								<strong>The best and trusted products for you.</strong>
								<p>
									For Cooperate Businesses with more than <span>250</span>{" "}
									employees
								</p>
								</div>
							</div> */}
							{currentListMenuIndex === 1 && <Products 
								setMenuOpen={setMenuOpen}
								isLoggedIn={isLoggedIn}
								currentMenu="Individuals"
							/>}
							{(currentListMenuIndex === 2 || currentListMenuIndex === null) && <LoyaltyPrograms 
								currentMenu="Individuals"
							/>}
							{currentListMenuIndex === 3 && <CustomerService />}
							{currentListMenuIndex === 4 && <AboutUs />}
							{currentListMenuIndex === 5 && <MediaCenter />}
						</div>
					</div>
				</div>
			)}
			{menuIndex === 1 && (
				<div className="enterprise-menu">
					<div className="top-menu-section left-section-small">
						<div className="left-section">{rightSideMenu()}</div>
						<div className="right-section">
							{currentListMenuIndex === 1 && <Products 
								setMenuOpen={setMenuOpen}
								isLoggedIn={isLoggedIn}
								currentMenu="Corporate"
							/>}
							{(currentListMenuIndex === 2 || currentListMenuIndex === null) && <LoyaltyPrograms  
								currentMenu="Corporate"
							/>}
							{currentListMenuIndex === 3 && <CustomerService />}
							{currentListMenuIndex === 4 && <AboutUs />}
							{currentListMenuIndex === 5 && <MediaCenter />}
						</div>
					</div>
				</div>
			)}
			{menuIndex === 2 && (
				<div className="sme-menu">
					<div className="top-menu-section">
						<div className="left-section">
							<div className="investor-menu-list">
								{investorMenuList?.map((item, index) => {
									return (
										<NavLink
											to={item?.url}
											className="bold-menu"
											key={index.toString()}
											onClick={() => setMenuOpen(null)}
										>
											{item?.title} <img src={RightArrowIcon} alt="..." />
										</NavLink>
									);
								})}
							</div>
						</div>
						<div className="right-section">
							<div className="stock-price-box">
								<div className="title">Stock Price</div>
								<div className="time">{investorStockDetails?.time}</div>
								<div className="price">SAR {investorStockDetails?.price}</div>
								<div className="lastprice">
									Last Price {investorStockDetails?.lastprice} %
								</div>
								<div className="price-list">
									<div>
										<label>High</label>
										<span>{investorStockDetails?.high}</span>
									</div>
									<div>
										<label>Low</label>
										<span>{investorStockDetails?.low}</span>
									</div>
									<div>
										<label>Volume</label>
										<span>{investorStockDetails?.volume}</span>
									</div>
									<div>
										<label>Previous Close</label>
										<span>{investorStockDetails?.previousClose}</span>
									</div>
									<div>
										<label>Change</label>
										<span>{investorStockDetails?.change}</span>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			)}
			{menuIndex === 3 && (
				<div className="investor-menu">
					<div className="menu-title-section left-section-small">
						{/* <div className="image">
              <img src={InvestorIcon} alt="..." />
            </div> */}
						<div className="title-content">
							<strong>Hope you're ok?</strong>
							<p>We're available 24/7, even on National Holidays.</p>
						</div>
					</div>
					<div className="top-menu-section half-column-section">
						<div className="left-section">
							<div className="small-title">Submit a Claim</div>
							{rightSideMenu({ isInsiderMenu: true })}
						</div>
						<div className="right-section">
							<div className="small-title">Track Your Claim</div>
							<div className="menu-section-form">
								<div className="input-div">
									<img src={FileIcon} alt="..." />
									<input type="text" placeholder="Policy No	" />
								</div>
								<div className="input-div">
									<img src={CreditCardIcon} alt="..." />
									<input type="text" placeholder="National ID or Iqama" />
								</div>
								<div className="input-div">
									<img src={DocumentIcon} alt="..." />
									<input type="text" placeholder="Claim Number" />
								</div>
								<div className="form-action">
									<button>Track</button>
								</div>
							</div>
							{/* <div className="menu-title-section">
                <div className="image">
                  <img src={SMEIcon} alt="..." />
                </div>
                <div className="title-content">
                  <strong>The best and trusted products for you.</strong>
                  <p>
                    For Cooperate Businesses with more than <span>250</span>{" "}
                    employees
                  </p>
                </div>
              </div> */}
						</div>
					</div>
				</div>
			)}
		</div>
	);
};

export default MenuPopup;
