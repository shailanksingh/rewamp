import React from "react";
import "./style.scss";
import RightArrowIcon from "../../../../assets/images/menuicons/right-arrow.svg";
import CustomerServiceImage1 from "../../../../assets/images/menuicons/customer-service-image1.svg";
import CustomerServiceImage2 from "../../../../assets/images/menuicons/customer-service-image2.svg";

const CustomerService = () => {
    return (
        <div className="menu-customer-service-details">
            <div className="customer-service-top">
                <div className="top-title">How Can We Assist You?</div>
                <p>Here you will find the answers to many of the questions you may have, including information about our comprehensive range of insurance products and services.</p>
                <div className="top-section-links">
                    <div className="top-section-links-left">
                        <a href="#">Motor <img src={RightArrowIcon} alt="..." /></a>
                        <a href="#">medical <img src={RightArrowIcon} alt="..." /></a>
                        <a href="#">property & casualty <img src={RightArrowIcon} alt="..." /></a>
                    </div>
                    <div className="top-section-links-right">
                        <a href="#">All Categories <img src={RightArrowIcon} alt="..." /></a>
                    </div>
                </div>
            </div>
            <div className="customer-service-box-root">
                <div className="customer-service-box-content content-width-50">
                    <div className="customer-service-box-title">
                        Protect Yourself<br />
                        Against Fraud!
                    </div>
                    <p>The insurance business operates based on a set of basic principles.</p>
                    <div className="customer-service-box-links">
                        <div><a href="#">medical fraud <img src={RightArrowIcon} alt="..." /></a></div>
                        <div><a href="#">Motor fraud <img src={RightArrowIcon} alt="..." /></a></div>
                        <div><a href="#">Property & Casualty Fraud <img src={RightArrowIcon} alt="..." /></a></div>
                        <div><a href="#">Report a Fraud <img src={RightArrowIcon} alt="..." /></a></div>
                    </div>
                    <img src={CustomerServiceImage1} className="box-bg-image" alt="..." />
                </div>
                <div className="customer-service-box-content content-width-50">
                    <div className="customer-service-box-title">
                        Violations
                    </div>
                    <p>Any fraudulent acts, corruption, collusion, coercion, illegal behavior.</p>
                    <div className="customer-service-box-links">
                        <div><a href="#">Learn More <img src={RightArrowIcon} alt="..." /></a></div>
                        <div><a href="#">Report Violations <img src={RightArrowIcon} alt="..." /></a></div>
                    </div>
                    <img src={CustomerServiceImage2} className="box-bg-image" alt="..." />
                </div>
                <div className="customer-service-box-content">
                    <div className="network-coverage">
                        <div className="network-coverage-left">
                            <div className="customer-service-box-title">
                                Network Coverage
                            </div>
                            <p>All you need to know our service providers.</p>
                        </div>
                        <div className="network-coverage-right">
                            <div><a href="#">Medical Providers <img src={RightArrowIcon} alt="..." /></a></div>
                            <div><a href="#">Tawuniya Network <img src={RightArrowIcon} alt="..." /></a></div>
                            <div><a href="#">Tawuniya Agents <img src={RightArrowIcon} alt="..." /></a></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default CustomerService