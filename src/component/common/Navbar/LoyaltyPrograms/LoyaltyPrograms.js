import React from "react";
import MenuDetailsImage1 from "../../../../assets/images/menu-details-image1.png";
import MenuDetailsImage2 from "../../../../assets/images/menu-details-image2.png";
import MenuDetailsImage3 from "../../../../assets/images/menu-details-image3.png";
import MenuDetailsImage4 from "../../../../assets/images/menu-details-image4.png";
import MenuDetailsLogo1 from "../../../../assets/images/menu-details-logo1.png";
import MenuDetailsLogo2 from "../../../../assets/images/menu-details-logo2.png";
import MenuDetailsLogo3 from "../../../../assets/images/menu-details-logo3.png";
import MenuDetailsLogo4 from "../../../../assets/images/menu-details-logo4.png";
import RightArrowBlackIcon from "../../../../assets/images/menuicons/right-arrow-black.svg";
import "./style.scss";

const Products = ({ currentMenu }) => {
    return (
        <div className="loyalityprograms-menu-list">
            {currentMenu === "Individuals" ? 
                <div className="details-menu">
                    <div className="bg-image">
                        <img
                            src={MenuDetailsImage4}
                            style={{
                                background: "#DB1753",
                                boxShadow: "0px 4px 10px rgba(9, 30, 66, 0.06)",
                            }}
                            alt="..."
                        />
                    </div>
                    <div className="img-content">
                        <div className="content-logo">
                            <img src={MenuDetailsLogo4} alt="..." />
                        </div>
                        <div className="text-content">
                            <p>Get 30% Off annual fitness time gym membership</p>
                            <a href="#">
                                <span>
                                    <img src={RightArrowBlackIcon} alt="..." />
                                </span>
                                Learn More
                            </a>
                        </div>
                    </div>
                </div> : 
                <div className="details-menu">
                    <div className="bg-image">
                        <img
                            src={MenuDetailsImage1}
                            style={{
                                background: "#679AB4",
                                boxShadow: "0px 4px 10px rgba(9, 30, 66, 0.06)",
                            }}
                            alt="..."
                        />
                    </div>
                    <div className="img-content">
                        <div className="content-logo">
                            <img src={MenuDetailsLogo1} alt="..." />
                        </div>
                        <div className="text-content">
                            <p>Special services and programmes to cover your needs</p>
                            <a href="#">
                                <span>
                                    <img src={RightArrowBlackIcon} alt="..." />
                                </span>
                                Learn More
                            </a>
                        </div>
                    </div>
                </div>
            }
            <div className="details-menu">
                <div className="bg-image">
                    <img
                        src={MenuDetailsImage2}
                        alt="..."
                        style={{
                            background: "#455560",
                            boxShadow: "0px 4px 10px rgba(9, 30, 66, 0.06)",
                        }}
                    />
                </div>
                <div className="img-content">
                    <div className="content-logo">
                        <img src={MenuDetailsLogo2} alt="..." />
                    </div>
                    <div className="text-content">
                        <p>
                            It mesures your driving behavior and rewards you
                            weekly..
                        </p>
                        <a href="#">
                            <span>
                                <img src={RightArrowBlackIcon} alt="..." />
                            </span>
                            Learn More
                        </a>
                    </div>
                </div>
            </div>
            <div className="details-menu">
                <div className="bg-image">
                    <img
                        src={MenuDetailsImage3}
                        alt="..."
                        style={{
                            background: "#679AB4",
                            boxShadow: "0px 4px 10px rgba(9, 30, 66, 0.06)",
                        }}
                    />
                </div>
                <div className="img-content">
                    <div className="content-logo">
                        <img src={MenuDetailsLogo3} alt="..." />
                    </div>
                    <div className="text-content">
                        <p>
                            Access to hundreds of exclusive offers, discounts and
                            more
                        </p>
                        <a href="#">
                            <span>
                                <img src={RightArrowBlackIcon} alt="..." />
                            </span>
                            Learn More
                        </a>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default Products;