import React, { useState, useCallback, useEffect } from "react";
import {Card} from "react-bootstrap"
import { NormalSearch } from "../NormalSearch";
import PhoneNumberInput from "component/common/PhoneNumberInput/index";

import Profile from "assets/svg/Forms/profile.svg"
import KSAFlagImage from "assets/images/ksaFlag.png";
import IndiaFlagImage from "assets/images/indiaFlag.png";





import "./style.scss"

const Motormobilecard =()=>{

    let dialingCodes = [
        {
          code: "+91",
          image: IndiaFlagImage,
        },
        {
          code: "+966",
          image: KSAFlagImage,
        },
      ];
    
      let [selectedCode, setSelectedCode] = useState(dialingCodes[0]);
    
      let [phoneNumber, setPhoneNumber] = useState("");
    
    

    return(

        <div className="main_card_motor pt-3">
            <Card className="maincard">

                <div className="container formbg mt-3">

                    <div className="rowbg">
                    <div className="row align_motortbtn">

                        <div className="col-6 ">
                        <button className="btn_bg">Individuals</button>
                        </div>

                        <div className="col-6">
                        <button className="btn_bg btn_bg2">SMEs</button>
                        </div>

                        </div>
                    </div>

                    <div className="row">
                        <div className="col-12 pt-3">
                        <NormalSearch
                        className="formInputFieldOne"
                        name="iqamaid"
                        placeholder="ID or IQAMA"
                        needLeftIcon={true}
                        needRightIcon={true}
                        leftIcon={Profile}
                    />
                        </div>

                        <div className="col-12 pt-3">
                        <NormalSearch
                        className="formInputFieldOne"
                        name="yob"
                        placeholder="Year Of Birth"
                        needLeftIcon={true}
                        leftIcon={Profile}
                    />
                        </div>

                        <div className="col-12 pt-3">
                        <PhoneNumberInput
                        className="formPhoneInput"
                        selectInputClass="formSelectInputWidth"
                        selectInputFlexType="formSelectFlexType"
                        dialingCodes={dialingCodes}
                        selectedCode={selectedCode}
                        setSelectedCode={setSelectedCode}
                        value={phoneNumber}
                        name="phoneNumber"
                        onChange={({ target: { value } }) => setPhoneNumber(value)}
                        />
                        </div>

                        <div className="col-12 pt-3">
                        <p className="fs-12 motorptag">By continuing you give Tawuniya advance consent to obtain my and/or my dependents' information from the National Information Center.</p>
                        </div>

                

                    </div>

              
                {/* ------ */}
                </div>
              
            </Card>

        </div>
    )

}


export default Motormobilecard