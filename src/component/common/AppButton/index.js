import React from "react";
import { Button, Icon } from "@material-ui/core";

export const AppButton = (props) => {
	const { variant, color, appIcon, title, subtitle } = props;
	return (
		<Button
			startIcon={
				<Icon style={{ height: "fit-content", width: "fit-content" }}>
					<img src={appIcon} alt="app icon" />
				</Icon>
			}
			variant={variant}
			color={color}
		>
			<div>
				<span style={{ fontSize: "8px" }}>{subtitle}</span>
				<p>{title}</p>
			</div>
		</Button>
	);
};
