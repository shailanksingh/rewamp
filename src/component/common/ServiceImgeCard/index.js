import React from "react";
import "./index.scss";

function ServiceCard(props) {
  const { Item } = props;

  return (
    <>
      <div className="serviceCardContainer">
        <div className="serviceCardImgContainer">
          <img className="serviceImgEl" alt="serviceIMg" src={Item.imgSrc} />
        </div>

        <div
          className={
            Item.bottom
              ? "serviceDetailsContainer serviceImgBottomTrue"
              : "serviceDetailsContainer"
          }
        >
          <h1 className="serviceMainHeading">{Item.heading}</h1>
          <p className="serviceDescription">{Item.discrption}</p>
          <div className="serviceBtnContainer">
            <button className="serviceImgCardButton1">Buy Now</button>
            <button className="serviceImgCardButton2">Learn More</button>
          </div>
        </div>
      </div>
    </>
  );
}

export default ServiceCard;
