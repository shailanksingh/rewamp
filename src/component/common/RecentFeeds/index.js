import React, { useState } from "react";
import { history } from "service/helpers";
import { CovidForm, HealthForm, MotorForm, TravelForm } from "./RecentForms";
import { NormalSearch } from "../NormalSearch";
import { NormalButton } from "../NormalButton";
import { ServiceFeedBackForm } from "../ServiceFeedBackForm";
import { feedbackContentData } from "component/common/MockData/NewMockData";
import { emojiFiData } from "component/common/MockData/NewMockData";
import { ratingData } from "component/common/MockData/NewMockData";
import searchIcon from "assets/svg/headerSearchLight.svg";
import languageIcon from "assets/svg/languageIcon.svg";
import modeIcon from "assets/svg/modeIcon.svg";
import appStore from "assets/images/appleStore.png";
import playStore from "assets/images/playStore.png";
import appGal from "assets/images/appGallery.png";
import messenger from "assets/svg/Facebook messenger.svg";
import editor from "assets/svg/Edit File.svg";
import locatePin from "assets/svg/Location pin.svg";
import grids from "assets/svg/Grid.svg";
import phoneIcon from "assets/svg/phoneCircleIcon.svg";
import motor from "assets/svg/recentFeedMotor.svg";
import normalHealth from "assets/svg/recentFeedNormalHealth.svg";
import normalTravel from "assets/svg/recentFeedNormalTravel.svg";
import covidNormal from "assets/svg/covidNormalPill.svg";
import logoplus from "assets/svg/Tawuniya-LogoPlusVitality.svg";
import "./style.scss";

export const RecentFeeds = () => {
  const [search, setSearch] = useState("");

  const arrayIcons = [appStore, playStore, appGal];

  const arrayHelpIcons = [
    {
      id: 0,
      icon: messenger,
      label: "Chat Now",
      link: "",
    },
    {
      id: 1,
      icon: editor,
      label: "Complaints",
      link: "",
    },
    {
      id: 2,
      icon: locatePin,
      label: "Locate us",
      link: "",
    },
    {
      id: 3,
      icon: grids,
      label: "All Services",
      link: "/dashboard/service",
    },
  ];

  const [pillIndex, setPillIndex] = useState(0);

  const [update, setUpdate] = useState({
    needForm: true,
    qOne: true,
    qTwo: true,
    qThree: true,
  });

  const pillData = [
    {
      id: 0,
      normalPillIcon: motor,
      highlightPillIcon: motor,
      pillLabel: "Motor",
      normalPillClass: "",
      higlightPillClass: "",
      content: <MotorForm />,
    },
    {
      id: 1,
      normalPillIcon: normalHealth,
      highlightPillIcon: motor,
      pillLabel: "Health",
      normalPillClass: "",
      higlightPillClass: "",
      content: <HealthForm />,
    },
    {
      id: 2,
      normalPillIcon: normalTravel,
      highlightPillIcon: motor,
      pillLabel: "Travel",
      normalPillClass: "",
      higlightPillClass: "",
      content: <TravelForm />,
    },
    {
      id: 3,
      normalPillIcon: covidNormal,
      highlightPillIcon: motor,
      pillLabel: "Covid-19",
      normalPillClass: "",
      higlightPillClass: "",
      content: <CovidForm />,
    },
  ];

  const bannerSliderData = [
    {
      id: 0,
      content: "Get 30% Off annual fitness time gym membership",
    },
    {
      id: 1,
      content: "Get 30% Off annual fitness time gym membership",
    },
    {
      id: 2,
      content: "Get 30% Off annual fitness time gym membership",
    },
    {
      id: 3,
      content: "Get 30% Off annual fitness time gym membership",
    },
  ];

  return (
    <React.Fragment>
      <div className="row">
        <div className="col-12 recentFeedContainer">
          <div className="d-flex flex-row-reverse">
            <div>
              <img
                src={modeIcon}
                className="img-fluid dashboard-top-icons"
                alt="icon"
              />
            </div>
            <div>
              <NormalButton
                label="Emergency?"
                className="emergencyDashboardBtn"
              />
            </div>
            <div>
              <img
                src={phoneIcon}
                className="img-fluid dashboard-top-icons"
                alt="icon"
              />
            </div>
            <div>
              <img
                src={searchIcon}
                className="img-fluid dashboard-top-icons"
                alt="icon"
              />
            </div>
            <div>
              <img
                src={languageIcon}
                className="img-fluid dashboard-languageIcon"
                alt="icon"
              />
            </div>
          </div>
          <div className="col-lg-12 col-12 recent-main-container">
            <div className="recentContainer">
              <p className="fs-18 fw-800 recentFeed-getInsureLabel m-0 pb-2">
                Get insured within minutes!
              </p>
              <div className="recentFeed-navpillFlow">
                {pillData.map((item, index) => {
                  return (
                    <div className="pillBox" key={index}>
                      <div
                        className={
                          item.id === pillIndex
                            ? "recentFeedHighlightPill"
                            : "recentFeedNormalPill"
                        }
                        onClick={() => setPillIndex(item.id)}
                      >
                        <img
                          src={
                            item.id === pillIndex
                              ? item.highlightPillIcon
                              : item.normalPillIcon
                          }
                          className="img-fluid pillIcon pr-2"
                          alt={item.pillLabel}
                        />{" "}
                        <span className="fs-12 fw-400 recentFeed-normalPillLabel">
                          {item.pillLabel}
                        </span>
                      </div>
                    </div>
                  );
                })}
              </div>
              <div className="row">
                <div className="col-12">
                  {pillData.map((item, index) => {
                    return (
                      <div key={index}>
                        {item.id === pillIndex && item.content}
                      </div>
                    );
                  })}
                </div>
              </div>
            </div>
          </div>
          <div className="subDashboardContainer p-3">
            <div className="searchDashboardContainer">
              <p className="text-center fs-16 fw-800 helptext m-0 pb-2">
                How can we help you?
              </p>
              <NormalSearch
                className="dashBoardheaderSearch"
                name="search"
                value={search}
                placeholder="What you're looking for?"
                onChange={(e) => setSearch(e.target.value)}
                needRightIcon={true}
                searchAligner="alignSearchIcon"
              />
              <div className="row pt-3 px-3">
                {arrayHelpIcons.map((item, index) => {
                  return (
                    <div
                      className="col-3 p-0 pr-1"
                      onClick={() => history.push(item.link)}
                    >
                      <div className="dashboard-helpIcons" key={index}>
                        <img
                          src={item.icon}
                          className="img-fluid mx-auto d-block"
                          alt="icon"
                        />
                        <p className="fs-9 fw-400 helpIconLabel text-center pt-2">
                          {item.label}
                        </p>
                      </div>
                    </div>
                  );
                })}
              </div>
            </div>
            <div>
              <p className="fs-16 fw-800 reward-Title pt-2">Rewards & More</p>
              <div className="recent-BannerFlex">
                {bannerSliderData.map((item, index) => {
                  return (
                    <div className="pr-3" key={index}>
                      <div className="banner-InnerLayer p-3">
                        <img src={logoplus} className="img-fluid" alt="logo" />
                        <div className="d-flex align-items-end recentInnerDiscount">
                          <div>
                            <p className="fs-18 fw-800 discount-Para m-0">
                              {item.content}
                            </p>
                          </div>
                        </div>
                      </div>
                    </div>
                  );
                })}
              </div>
            </div>
            <div className="d-flex flex-row dashboard-socialIconBox pt-3 pb-5">
              {arrayIcons.map((items) => {
                return (
                  <div>
                    <img
                      src={items}
                      className="img-fluid socio-icons"
                      alt="buttonIcon"
                    />
                  </div>
                );
              })}
            </div>
          </div>
        </div>
        <div className="col-12 pl-5">
          <ServiceFeedBackForm
            feedbackContentData={feedbackContentData}
            emojiData={emojiFiData}
            ratingData={ratingData}
            update={update}
          />
        </div>
      </div>
    </React.Fragment>
  );
};
