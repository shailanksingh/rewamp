import React, { useState } from "react";
import { NormalButton } from "../../NormalButton";
import { NormalSelect } from "../../NormalSelect";
import cam from "assets/svg/natonCamera.svg";
import selectArrow from "assets/svg/selectArrowIconHome.svg";
import "../style.scss";

export const HealthForm = () => {
  const [btnPill, setBtnPill] = useState(0);

  return (
    <div className="health-recentFeed-SubForm mt-3">
      <div className="row">
        <div className="col-12">
          <div className="row">
            <div className="col-6 pr-0">
              <NormalButton
                label="Individuals"
                className={
                  btnPill === 0
                    ? "health-inndividualDashboardBtn-highlight"
                    : "health-inndividualDashboardBtn"
                }
                onClick={() => setBtnPill(0)}
              />
            </div>
            <div className="col-6">
              <NormalButton
                label="SMEs"
                className={
                  btnPill === 1
                    ? "health-smeDashboardBtn-highlight"
                    : "health-smeDashboardBtn"
                }
                onClick={() => setBtnPill(1)}
              />
            </div>
          </div>
        </div>
        {btnPill === 0 ? (
          <div className="col-12 pt-2">
            <p className="fs-12 fw-800 health-feed-LabelOne m-0">National ID</p>
            <img src={cam} className="img-fluid health-cameraIcon" alt="icon" />
            <input
              type="text"
              className="health-nationIdClass w-100"
              placeholder="Enter your ID number"
            />
            <p className="fs-12 fw-800 health-feed-LabelTwo m-0 pt-2">
              Year of Birth
            </p>
            <NormalSelect
              className="health-bannerSelectInput"
              arrowVerticalAlign="health-bannerArrowAlign"
              placeholder="Select a year"
              selectArrow={selectArrow}
              selectControlHeight="0px"
              selectFontWeight="400"
              phColor="#455560"
              fontSize="12px"
            />
            <p className="fs-12 fw-800 health-feed-LabelTwo m-0">
              Mobile Number
            </p>
            <input
              type="text"
              className="health-mobileNoClass w-100"
              placeholder="+966 5xxxxxxxx"
            />
            <div className="pt-2">
              <NormalButton
                label="Buy Now"
                className="health-recentFeedBuyNowBtn"
              />
            </div>
            <p className="fs-9 fw-400 health-recentFeedTerms pt-2 m-0">
              By continuing you give Tawuniya advance consent to obtain my
              and/or my dependents' information from the National Information
              Center.
            </p>
          </div>
        ) : (
          <div className="col-12 pt-2">
            <p className="fs-12 fw-800 health-feed-LabelOne m-0">CR Number</p>
            <img src={cam} className="img-fluid health-cameraIcon" alt="icon" />
            <input
              type="text"
              className="health-nationIdClass w-100"
              placeholder="Enter company CR number"
            />
            <p className="fs-12 fw-800 health-feed-LabelTwo m-0 pt-2">
              Expirey Date
            </p>
            <NormalSelect
              className="health-bannerSelectInput"
              arrowVerticalAlign="health-bannerArrowAlign"
              placeholder="Select the expirey date"
              selectArrow={selectArrow}
              selectControlHeight="0px"
              selectFontWeight="400"
              phColor="#455560"
              fontSize="12px"
            />
            <p className="fs-12 fw-800 health-feed-LabelTwo m-0">
              Mobile Number
            </p>
            <input
              type="text"
              className="health-mobileNoClass w-100"
              placeholder="+966 5xxxxxxxx"
            />
            <div className="pt-2">
              <NormalButton
                label="Buy Now"
                className="health-recentFeedBuyNowBtn"
              />
            </div>
            <p className="fs-9 fw-400 health-recentFeedTerms pt-2 m-0">
              By continuing you give Tawuniya advance consent to obtain my
              and/or my dependents' information from the National Information
              Center.
            </p>
          </div>
        )}
      </div>
    </div>
  );
};
