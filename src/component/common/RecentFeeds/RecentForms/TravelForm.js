import React, { useState } from "react";
import { NormalButton } from "../../NormalButton";
import { NormalSelect } from "../../NormalSelect";
import cam from "assets/svg/natonCamera.svg";
import selectArrow from "assets/svg/selectArrowIconHome.svg";
import "../style.scss";

export const TravelForm = () => {
  const [btnPill, setBtnPill] = useState(0);

  return (
    <div className="travel-recentFeed-SubForm mt-3">
      <div className="row">
        <div className="col-12">
          <div className="row">
            <div className="col-6 pr-0">
              <NormalButton
                label="Individuals"
                className={
                  btnPill === 0
                    ? "travel-inndividualDashboardBtn-highlight"
                    : "travel-inndividualDashboardBtn"
                }
                onClick={() => setBtnPill(0)}
              />
            </div>
            <div className="col-6">
              <NormalButton
                label="SMEs"
                className={
                  btnPill === 1
                    ? "travel-smeDashboardBtn-highlight"
                    : "travel-smeDashboardBtn"
                }
                onClick={() => setBtnPill(1)}
              />
            </div>
          </div>
        </div>
        {btnPill === 0 ? (
          <div className="col-12 pt-2">
            <p className="fs-12 fw-800 travel-feed-LabelOne m-0">
              Passport Number
            </p>
            <img src={cam} className="img-fluid travel-cameraIcon" alt="icon" />
            <input
              type="text"
              className="travel-nationIdClass w-100"
              placeholder="Enter your Passport Number"
            />
            <p className="fs-12 fw-800 travel-feed-LabelTwo m-0 pt-2">
              Year of Birth
            </p>
            <NormalSelect
              className="travel-bannerSelectInput"
              arrowVerticalAlign="travel-bannerArrowAlign"
              placeholder="Select a year"
              selectArrow={selectArrow}
              selectControlHeight="0px"
              selectFontWeight="400"
              phColor="#455560"
              fontSize="12px"
            />
            <p className="fs-12 fw-800 travel-feed-LabelTwo m-0">
              Mobile Number
            </p>
            <input
              type="text"
              className="travel-mobileNoClass w-100"
              placeholder="+966 5xxxxxxxx"
            />
            <div className="pt-2">
              <NormalButton
                label="Buy Now"
                className="travel-recentFeedBuyNowBtn"
              />
              <p className="fs-9 fw-400 travel-recentFeedTerms pt-2 m-0">
                By continuing you give Tawuniya advance consent to obtain my
                and/or my dependents' information from the National Information
                Center.
              </p>
            </div>
          </div>
        ) : (
          <div className="col-12 pt-2">
            <p className="fs-12 fw-800 travel-feed-LabelOne m-0">CR Number</p>
            <img src={cam} className="img-fluid travel-cameraIcon" alt="icon" />
            <input
              type="text"
              className="travel-nationIdClass w-100"
              placeholder="Enter company CR number"
            />
            <p className="fs-12 fw-800 travel-feed-LabelTwo m-0 pt-2">
              Expirey Date
            </p>
            <NormalSelect
              className="travel-bannerSelectInput"
              arrowVerticalAlign="travel-bannerArrowAlign"
              placeholder="Select the expirey date"
              selectArrow={selectArrow}
              selectControlHeight="0px"
              selectFontWeight="400"
              phColor="#455560"
              fontSize="12px"
            />
            <p className="fs-12 fw-800 travel-feed-LabelTwo m-0">
              Mobile Number
            </p>
            <input
              type="text"
              className="travel-mobileNoClass w-100"
              placeholder="+966 5xxxxxxxx"
            />
            <div className="pt-2">
              <NormalButton
                label="Buy Now"
                className="travel-recentFeedBuyNowBtn"
              />
              <p className="fs-9 fw-400 travel-recentFeedTerms pt-2 m-0">
                By continuing you give Tawuniya advance consent to obtain my
                and/or my dependents' information from the National Information
                Center.
              </p>
            </div>
          </div>
        )}
      </div>
    </div>
  );
};
