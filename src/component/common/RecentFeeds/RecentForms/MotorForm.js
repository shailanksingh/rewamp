import React, { useState } from "react";
import { NormalButton } from "../../NormalButton";
import { NormalSelect } from "../../NormalSelect";
import cam from "assets/images/mobile/cam.png";
import selectArrow from "assets/svg/selectArrowIconHome.svg";
import "../style.scss";

export const MotorForm = ({ title = "Individuals", isBtn = false }) => {
	const [btnPill, setBtnPill] = useState(0);

	return (
		<div className="recentFeed-SubForm mt-3">
			<div className="row">
				<div className="col-12">
					{!isBtn && (
						<div className="row m-0">
							<div className="col-6 p-0 pr-1">
								<NormalButton
									label={title}
									className={
										btnPill === 0
											? "inndividualDashboardBtn-highlight"
											: "inndividualDashboardBtn"
									}
									onClick={() => setBtnPill(0)}
								/>
							</div>
							<div className="col-6 p-0 pl-1">
								<NormalButton
									label="SMEs"
									className={
										btnPill === 1
											? "smeDashboardBtn-highlight"
											: "smeDashboardBtn"
									}
									onClick={() => setBtnPill(1)}
								/>
							</div>
						</div>
					)}
				</div>
				{btnPill === 0 ? (
					<div className="col-12 pt-2">
						<p className="fs-12 fw-800 feed-LabelOne m-0">National ID</p>
						<img src={cam} className="img-fluid cameraIcon" alt="icon" />
						<input
							type="text"
							className="nationIdClass w-100"
							placeholder="Enter your ID number"
						/>
						<p className="fs-12 fw-800 feed-LabelTwo m-0 pt-2">Year of Birth</p>
						<NormalSelect
							className="bannerSelectInput"
							arrowVerticalAlign="bannerArrowAlign"
							placeholder="Select a year"
							selectArrow={selectArrow}
							selectControlHeight="0px"
							selectFontWeight="400"
							phColor="#455560"
							fontSize="12px"
						/>
						<p className="fs-12 fw-800 feed-LabelTwo m-0">Mobile Number</p>
						<div className="d-flex">
							<input
								type="text"
								className="mobileNoClass w-100"
								placeholder="+966 5xxxxxxxx"
							/>
						</div>
					</div>
				) : (
					<div className="col-12 pt-2">
						<p className="fs-12 fw-800 feed-LabelOne m-0">CR Number</p>
						<img src={cam} className="img-fluid cameraIcon" alt="icon" />
						<input
							type="text"
							className="nationIdClass w-100"
							placeholder="Enter company CR number"
						/>
						<p className="fs-12 fw-800 feed-LabelTwo m-0 pt-2">Expirey Date</p>
						<NormalSelect
							className="bannerSelectInput"
							arrowVerticalAlign="bannerArrowAlign"
							placeholder="Select the expirey date"
							selectArrow={selectArrow}
							selectControlHeight="0px"
							selectFontWeight="400"
							phColor="#455560"
							fontSize="12px"
						/>
						<p className="fs-12 fw-800 feed-LabelTwo m-0">Mobile Number</p>
						<input
							type="text"
							className="mobileNoClass w-100"
							placeholder="+966 5xxxxxxxx"
						/>
					</div>
				)}
			</div>
			{btnPill === 0 ? (
				<React.Fragment>
					<div className="pt-2">
						<NormalButton label="Buy Now" className="recentFeedBuyNowBtn" />
					</div>
					<p className="fs-9 fw-400 recentFeedTerms pt-2 m-0">
						By continuing you give Tawuniya advance consent to obtain my and/or
						my dependents' information from the National Information Center.
					</p>
				</React.Fragment>
			) : (
				<React.Fragment>
					<div className="pt-2">
						<NormalButton label="Buy Now" className="recentFeedBuyNowBtn" />
					</div>
					<p className="fs-9 fw-400 recentFeedTerms pt-2 m-0">
						By continuing you give Tawuniya advance consent to obtain my and/or
						my dependents' information from the National Information Center.
					</p>
				</React.Fragment>
			)}
		</div>
	);
};
