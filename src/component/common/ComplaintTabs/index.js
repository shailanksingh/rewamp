import React, { useState } from "react";
import allProductsIcon from "assets/svg/allProctsIcon.svg";
import medicalIcon from "assets/svg/medicalIcon.svg";
import travelIcon from "assets/svg/travelIcon.svg";
import normalGlobe from "assets/svg/normalGlobe.svg";
import greyMotor from "assets/svg/greyMotor.svg";
import highlightMotor from "assets/svg/motorLandingHighlight.svg";
import highlightMedical from "assets/svg/healthLandingHighlight.svg";
import highlightProperty from "assets/svg/travelLandingHighlight.svg";
import "./style.scss";

export const ComplaintTabs = ({
  justifyContentTabsClass = "justify-content-center",
  compOne,
  compTwo,
  compThree,
  compFour,
}) => {
  const [pill, setPill] = useState(0);

  const myPills = [
    {
      id: 0,
      pillName: "ALL PRODUCTS",
      content: "helo",
      hidePill: true,
      pillIcon: normalGlobe,
      pillIconHighlight: allProductsIcon,
      comp: compOne,
      tableHighlightClass: "space-higlight",
      tabNormalClass: "space-normal",
    },
    {
      id: 1,
      pillName: "MOTOR",
      content: "ho",
      hidePill: true,
      pillIcon: greyMotor,
      pillIconHighlight: highlightMotor,
      comp: compTwo,
      tableHighlightClass: "space-higlight",
      tabNormalClass: "space-normal",
    },
    {
      id: 2,
      pillName: "MEDICAL",
      content: "elo",
      hidePill: true,
      pillIcon: medicalIcon,
      pillIconHighlight: highlightMedical,
      comp: compThree,
      tableHighlightClass: "space-higlight",
      tabNormalClass: "space-normal",
    },
    {
      id: 3,
      pillName: "PROPERTY & CASUALTY",
      content: "elo",
      hidePill: true,
      pillIcon: travelIcon,
      pillIconHighlight: highlightProperty,
      comp: compFour,
      tableHighlightClass: "",
      tabNormalClass: "",
    },
  ];
  return (
    <div className="Complaint-mainBannerContainer">
      <div className="row Complaint-mainBannerTop">
        <div className="col-12">
          <div className={`d-flex ${justifyContentTabsClass}`}>
            <div>
              <div className="pillContainer">
                <div className="d-flex flex-row pb-2">
                  {myPills.map((item, index) => {
                    return (
                      <div
                        key={index}
                        className={`${
                          item.id === pill
                            ? item.tableHighlightClass
                            : item.tabNormalClass
                        } pillBtnContainer`}
                      >
                        {item.hidePill && (
                          <>
                            <div
                              className={`${
                                item.id === pill
                                  ? "highlightPill"
                                  : "normalPill"
                              } d-flex flex-row`}
                              onClick={() => setPill(item.id)}
                            >
                              <div>
                                <img
                                  src={
                                    item.id === pill
                                      ? item.pillIconHighlight
                                      : item.pillIcon
                                  }
                                  className={`${
                                    item.id === pill
                                  } img-fluid pillIcon`}
                                  alt="pillicon"
                                  id={item.id === 0 && "alignWorldIcon"}
                                />
                              </div>
                              <div>
                                <p
                                  className={`${
                                    item.id === pill
                                      ? "pillHighlightText fs-14 fw-800"
                                      : "pillNormalText fs-14 fw-400"
                                  } text-uppercase`}
                                >
                                  {item.pillName}
                                </p>
                              </div>
                            </div>
                          </>
                        )}
                      </div>
                    );
                  })}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="row">
        <div className="col-12">
          {myPills.map((item, index) => {
            return (
              <React.Fragment>
                {item.id === pill && (
                  <div className="row" key={index}>
                    {item.hidePill && (
                      <div className="col-lg-12 col-12">
                        <div className="pt-3">{item.comp}</div>
                      </div>
                    )}
                  </div>
                )}
              </React.Fragment>
            );
          })}
        </div>
      </div>
    </div>
  );
};
