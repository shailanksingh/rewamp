import React from "react";
import "./style.scss";

export const SupportCards = ({
  icon,
  title,
  supportText,
  subText,
  whatsAppText,
}) => {
  return (
    <div className="support_cards_list">
      <div className="image_section">
        <img src={icon} alt="Icon" />
      </div>
      <div className="body_section">
        <label>{title}</label>
        <label className="support_medium_text">{supportText}</label>
        {subText && <label>{subText}</label>}
        {whatsAppText && (
          <div className="whatsapp_text">
            <label>Chat with our executives</label>
            <h5>Start Live Chat</h5>
          </div>
        )}
      </div>
    </div>
  );
};
