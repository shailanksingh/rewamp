import React from "react";
import "./style.scss";
import phone from "assets/images/mobile/phone.png";
import whatsapp from "assets/images/mobile/whatsapp.png";
import chatmobile from "assets/images/mobile/chatmobile.png";
import mail from "assets/images/mobile/mail.png";

const SupportRequestHelper = ({ isContact = true }) => {
  return (
    <div className={`${!isContact && "bg-white"} support-request-helper`}>
      <div className="support-request-callcenter">
        <img className="phone-img" src={phone} alt="phone" />
        <span className="call-center">Call Center</span>
        <div className="call-number">800 124 9990</div>
      </div>
      <div className="support-request-whatsapp">
        <img className="whatsapp-img" src={whatsapp} alt="whatsapp" />
        <span className="whatsapp">Whatsapp</span>
        <div className="whatsapp-number">9200 19990</div>
        <div className="whatsapp-chat">This is a chat only number</div>
      </div>
      <div className="support-request-executives">
        <img className="chatmobile-img" src={chatmobile} alt="chat" />
        <span className="executives">Chat with our executives</span>
        <button className="button-online">Online</button>
        <div className="executives-livechat">Start Live Chat</div>
      </div>
      <div className={`${!isContact && "pb-3"} support-request-mail`}>
        <img className="mail-img" src={mail} alt="mail" />
        <span className="support-email">Email</span>
        <div className="support-email-care">Care@tawuniya.com.sa</div>
      </div>
      {isContact && (
        <>
          <h2 className="hr-lines">OR</h2>
          <div className="support-button">
            <button className="support-request-button">
              Open a Support Request
            </button>
          </div>
        </>
      )}
    </div>
  );
};

export default SupportRequestHelper;
