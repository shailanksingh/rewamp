import React, { useState, useEffect } from "react";
import Menu from "assets/images/mobile/Menu.png";
import Emergency from "assets/images/mobile/emergency_header.svg";
import MobileMenuPopup from "component/common/NavBarMobile/MobileMenuPopup";
import "./style.scss";
import { history } from "service/helpers";

const HeaderStickyMenu = ({ customClass = false }) => {
  const [scroll, setScroll] = useState(false);
  let [isMenuActive, setMenuActive] = useState(false);

  useEffect(() => {
    window.addEventListener("scroll", () => {
      setScroll(window.scrollY > 50);
    });
  }, []);

  return (
    <>
      <div
        className={`${customClass && "isLanding"} ${
          scroll && customClass && "active"
        } header_mobile_sticky`}
      >
        <div>
          <img src={Menu} alt="Menu" onClick={() => setMenuActive(true)} />
          {customClass && <span className="notify">2</span>}
        </div>
        <div className="header_emergency">
          <label onClick={() => history.push("/home/emergency-support")}>
            Emergency
          </label>
          <img
            src={Emergency}
            alt="Emergency"
            className="toggle-logo"
            onClick={() => history.push("/home/emergency-support")}
          />
        </div>
      </div>
      <MobileMenuPopup
        closeMenu={() => setMenuActive(false)}
        active={isMenuActive}
      />
    </>
  );
};

export default HeaderStickyMenu;
