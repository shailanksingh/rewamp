import React, { useEffect, useState } from "react";
import "./style.scss";
import { Myfamilyinsurance } from "component/MyFamilyMobilePage/Myfamilyinsurance";
import header_down from "assets/images/mobile/header_down.png";
import header_left_menu from "assets/images/mobile/header_left_menu.svg";
import header_up_arrow from "assets/images/mobile/header_up_arrow.svg";
import BottomPopup from "../BottomPopup";

const HeaderStepsSticky = ({
  title,
  isSelect,
  selectTitle,
  leftIcon,
  menuList,
  selectedMenuOption,
  buttonTitle,
  flexPart = false,
}) => {
  const [openMenu, setOpenMenu] = useState(false);
  const [bottomPop, setBottomPop] = useState(false);
  const activeMenu = (val) => {
    selectedMenuOption(val);
    setOpenMenu(!openMenu);
  };

  return (
    <div className="header_steps_container">
      <div className="header_body_container">
        <div className="header_steps_body">
          {leftIcon && <img src={leftIcon} alt="Arrow" />}
          <h6>{title}</h6>
        </div>
        {isSelect && (
          <div className={flexPart ? "d-flex align-items-center" : ""}>
            <label>{selectTitle.label}</label>
            <img
              src={openMenu ? header_up_arrow : header_down}
              alt="Arrow"
              onClick={() => setOpenMenu(!openMenu)}
            />
          </div>
        )}
        {buttonTitle && (
          <div>
            <button
              className="header_button"
              onClick={() => setBottomPop(true)}
            >
              {buttonTitle}
            </button>
          </div>
        )}
      </div>
      {openMenu && menuList?.length > 0 && (
        <div className="mt-4">
          {menuList?.map((val) => {
            return (
              <div className="header_menu_list">
                <label
                  className={`${val.id === selectTitle.id && "menu_active"}`}
                  onClick={() => activeMenu(val)}
                >
                  main_overlay
                  <img src={header_left_menu} alt="Arrow" />
                  {val.label}
                </label>
              </div>
            );
          })}
        </div>
      )}
      <BottomPopup
        open={bottomPop}
        setOpen={setBottomPop}
        bg="gray"
        isBottom={true}
      >
        <Myfamilyinsurance />
      </BottomPopup>
    </div>
  );
};

export default HeaderStepsSticky;
