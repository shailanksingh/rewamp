import React from "react";
import "./style.scss";
import BackArrowSolid from "assets/images/mobile/BackArrowSolid.svg";
import { history } from "service/helpers";

const HeaderBackNav = ({ pageName = "", title = "" }) => {
  return (
    <div className="header_back_nav_container">
      <img src={BackArrowSolid} alt="Back" onClick={() => history.goBack()} />
      <p>{pageName}</p>
      <h6>{title}</h6>
    </div>
  );
};

export default HeaderBackNav;
