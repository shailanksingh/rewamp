import React from "react";
import "./style.scss";
import Close from "assets/images/mobile/Close.png";
import down_arrow from "assets/images/mobile/down_arrow.png";
import { history } from "service/helpers";

const WhiteHeaderSticky = ({
  title,
  SubTitle,
  isMenu = false,
  menuOption = "",
}) => {
  return (
    <div className="white_header_container">
      <img
        src={Close}
        alt="Close"
        className="close_service_list"
        onClick={() => history.goBack()}
      />
      <div className="white_header_body">
        <p>{title}</p>
        <p className="fw-800 fs-16">{SubTitle}</p>
        {isMenu && (
          <div className="menu_option">
            <label>{menuOption}</label>
            <img src={down_arrow} alt="Down" />
          </div>
        )}
      </div>
    </div>
  );
};

export default WhiteHeaderSticky;
