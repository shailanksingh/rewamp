import React from "react";
import "./style.scss";
import email from "assets/news/email.png";
import fax from "assets/news/fax.png";
import locate from "assets/news/locate.png";
import phone from "assets/news/phone.png";

const SocialResponsibilityContactUs = () => {
  return (
    <div className="contact_us_black_box">
      <div className="d-flex">
        <img
          className="black_box_image_phone"
          src={phone}
          width="20"
          height="20"
          alt="Phone"
        />
        <div className="">
          <div className="black_box_phone ">Phone</div>
          <div className="black_box_phno ">+966 11 252 5800</div>
        </div>
      </div>
      <hr className="contact_line" />
      <div className="d-flex">
        <img
          className="black_box_image_fax"
          src={fax}
          width="20"
          height="20"
          alt="Fax"
        />
        <div className="">
          <div className="black_box_fax ">Fax</div>
          <div className="black_box_faxno ">+966 11 400 0844</div>
          <div className="fax_chat">This is a chat only number</div>
        </div>
      </div>
      <hr className="contact_line" />
      <div className="d-flex">
        <img
          className="black_box_image_email"
          src={email}
          width="20"
          height="20"
          alt="Email"
        />
        <div className="">
          <div className="black_box_email ">Email</div>
          <div className="black_box_emailid">info@tawuniya.com.sa</div>
        </div>
      </div>
      <hr className="contact_line" />
      <div className="d-flex">
        <img
          className="black_box_image_box"
          src={fax}
          width="20"
          height="20"
          alt="P.O Fax"
        />
        <div className="">
          <div className="black_box_faxbox ">P.O Box</div>
          <div className="black_box_boxno">86959</div>
        </div>
      </div>
      <hr className="contact_line" />
      <div className="d-flex">
        <img
          className="black_box_image_location"
          src={locate}
          width="20"
          height="20"
          alt="Locate"
        />
        <div className="">
          <div className="black_box_location ">Address</div>
          <div className="black_box_address">
            6507 Thomamah Road (Takhassusi) - <br />
            Ar Rabi, Riyadh 11632
          </div>
        </div>
      </div>
      <div className="bottom" />
    </div>
  );
};

export default SocialResponsibilityContactUs;
