import React, { useState, useEffect, useRef } from "react";
import Dialog from "@material-ui/core/Dialog";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import Slide from "@material-ui/core/Slide";
import "./style.scss";
import {
  makeStyles,
  createTheme,
  ThemeProvider,
} from "@material-ui/core/styles";
import CloseIcon from "assets/images/mobile/Close.png";

const useStyles = makeStyles({
  title: {},
  dialog: {
    margin: 0,
    "& .MuiDialog-paper-7": {
      width: "100%",
    },
    "& .paddingBox": {
      padding: 0,
    },
    "& .gray": {
      background: "#F2F3F5",
      padding: 0,
    },
  },
});
const theme = createTheme({
  shape: {
    borderRadius: "20px 20px 0 0",
  },
  overrides: {
    MuiDialog: {
      paper: {
        margin: 0,
      },
      title: {
        position: "relative",
      },
      scrollPaper: {
        "&::after": {
          verticalAlign: "bottom",
        },
        alignItems: "flex-end",
        verticalAlign: "bottom",
      },
    },
    MuiDialogContent: {
      dividers: {
        borderTop: 0,
        minHeight: "100vh",
        width: "100vw",
        // padding: "16px",
        padding: "0px",
        position: "relative",
      },
    },
  },
});

const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});

const BottomPopup = ({
  children,
  open,
  setOpen,
  bg = "",
  isBottom = false,
}) => {
  const [scroll, setScroll] = useState("paper");

  const handleClose = () => {
    setOpen(false);
  };

  const descriptionElementRef = useRef(null);

  useEffect(() => {
    if (open) {
      const { current: descriptionElement } = descriptionElementRef;
      if (descriptionElement !== null) {
        descriptionElement.focus();
      }
    }
  }, [open]);

  useEffect(() => {}, []);

  const classes = useStyles();

  return (
    <div>
      <ThemeProvider theme={theme}>
        <Dialog
          open={open}
          TransitionComponent={Transition}
          onClose={handleClose}
          className={classes.dialog}
          scroll={"paper"}
          aria-labelledby="scroll-dialog-title"
          aria-describedby="scroll-dialog-description"
        >
          <DialogContent dividers={scroll === "paper"} className={bg}>
            <DialogContentText
              id="scroll-dialog-description"
              ref={descriptionElementRef}
              tabIndex={-1}
            >
              <div
                className={`${isBottom && "is-bottom"} bottom_popup_container`}
              >
                <img
                  src={CloseIcon}
                  alt="Close"
                  className="close_icon"
                  onClick={handleClose}
                />
                {children}
              </div>
            </DialogContentText>
          </DialogContent>
        </Dialog>
      </ThemeProvider>
    </div>
  );
};

export default BottomPopup;
