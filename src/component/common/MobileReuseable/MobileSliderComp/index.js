import React, { useRef } from "react";
import ArrowForward from "assets/svg/HomeServiceFroward1Arrow.svg";
import ArrowBack from "assets/svg/HomeServiceBackArrow.svg";
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import { makeStyles } from "@material-ui/core";
import "./style.scss";

const useStyles = makeStyles((theme) => ({
  dots: {
    // bottom: -63,
    "& li.slick-active button::before": {
      color: "#EE7500",
    },
    "& li": {
      width: "12px",
      "& button::before": {
        fontSize: theme.typography.pxToRem(9),
        color: "#4C565C",
      },
    },
  },
}));

const MobileSliderComp = ({ children, isArrow = true, slidesToShow = 2 }) => {
  const classes = useStyles();

  const sliderRef = useRef(null);

  const settings = {
    slidesToShow: 6,
    slidesToScroll: 2,
    arrows: false,
    dots: true,
    // variableWidth: true,
    dotsClass: `slick-dots ${classes.dots}`,
    responsive: [
      {
        breakpoint: 1199,
        settings: {
          slidesToShow: 6,
          slidesToScroll: 2,
        },
      },
      {
        breakpoint: 800,
        settings: {
          slidesToShow: slidesToShow,
          slidesToScroll: 1,
          infinite: true,
          dots: true,
        },
      },
    ],
  };
  return (
    <div>
      <Slider ref={sliderRef} {...settings}>
        {children}
      </Slider>
      <div className="mobile_arrowDotContainer">
        {isArrow && (
          <div
            className="mobile_arrowContainer"
            onClick={() => sliderRef.current.slickPrev()}
          >
            {isArrow && <img src={ArrowBack} alt="ArrowBack" />}
          </div>
        )}
        <div className="mobile_arrow_dotContainer" />
        {isArrow && (
          <div
            className="mobile_arrowContainer"
            onClick={() => sliderRef.current.slickNext()}
          >
            <img src={ArrowForward} alt="ArrowForward" />
          </div>
        )}
      </div>
    </div>
  );
};

export default MobileSliderComp;
