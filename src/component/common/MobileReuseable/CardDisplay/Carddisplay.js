import React from "react";
import "./Style.scss";
function Carddisplay() {
  return (
    <div className="financial_highlights_container">
      <div class="card financial_highlights container">
        <div class="my-3">
          <p className="mb-2">Total Assests</p>
          <div className="d-flex align-items-center">
            <div>
              <h3>14.6 Billion</h3>
            </div>
            <div className="mx-1">
              <h4>SR</h4>
            </div>
          </div>
        </div>
      </div>
      <div class="card financial_highlights container my-2">
        <div class="my-3">
          <p className="mb-2">Total Equity</p>
          <div className="d-flex align-items-center">
            <div>
              <h3>3 Billion</h3>
            </div>
            <div className="mx-1">
              <h4>SR</h4>
            </div>
          </div>
        </div>
      </div>
      <div class="card financial_highlights container my-2">
        <div class="my-3">
          <p className="mb-2">Gross Written Premiums (GWP)</p>
          <div className="d-flex align-items-center">
            <div>
              <h3>10.2 Billion</h3>
            </div>
            <div className="mx-1">
              <h4>SR</h4>
            </div>
          </div>
        </div>
      </div>
      <div class="card financial_highlights container my-2">
        <div class="my-3">
          <p className="mb-2">Net Earned Premiums</p>
          <div className="d-flex align-items-center">
            <div>
              <h3>7.9 Billion</h3>
            </div>
            <div className="mx-1">
              <h4>SR</h4>
            </div>
          </div>
        </div>
      </div>
      <div class="card financial_highlights container my-2">
        <div class="my-3">
          <p className="mb-2">Net Incurred Claims</p>
          <div className="d-flex align-items-center">
            <div>
              <h3>6.6 Billion</h3>
            </div>
            <div className="mx-1">
              <h4>SR</h4>
            </div>
          </div>
        </div>
      </div>
      <div class="card financial_highlights container my-2">
        <div class="my-3">
          <p className="mb-2">Net Profit before Zakat</p>
          <div className="d-flex align-items-center">
            <div>
              <h3>350 Million</h3>
            </div>
            <div className="mx-1">
              <h4>SR</h4>
            </div>
          </div>
        </div>
      </div>
      <div class="card financial_highlights container my-2">
        <div class="my-3">
          <p className="mb-2">
            Surplus of Insurance Operations Minus Policyholders Investments
            Revenues
          </p>
          <div className="d-flex align-items-center">
            <div>
              <h3>77.5 Million</h3>
            </div>
            <div className="mx-1">
              <h4>SR</h4>
            </div>
          </div>
        </div>
      </div>
      <div class="card financial_highlights container my-2">
        <div class="my-3">
          <p className="mb-2">Net income of Policyholders Investments</p>
          <div className="d-flex align-items-center">
            <div>
              <h3>145.7 Million</h3>
            </div>
            <div className="mx-1">
              <h4>SR</h4>
            </div>
          </div>
        </div>
      </div>
      <div class="card financial_highlights container my-2">
        <div class="my-3">
          <p className="mb-2">Net income of Shareholders Capital Investments</p>
          <div className="d-flex align-items-center">
            <div>
              <h3>157 Million</h3>
            </div>
            <div className="mx-1">
              <h4>SR</h4>
            </div>
          </div>
        </div>
      </div>
      <div class="card financial_highlights container my-2">
        <div class="my-3">
          <p className="mb-2">Earnings per Share</p>
          <div className="d-flex align-items-center">
            <div>
              <h3>2.13</h3>
            </div>
            <div className="mx-1">
              <h4>SR</h4>
            </div>
          </div>
        </div>
      </div>
      <div class="card financial_highlights container my-2 ">
        <div class="my-3">
          <p className="mb-2">Issued and Paid-up Capital</p>
          <div className="d-flex align-items-center">
            <div>
              <h3>1,250 Million</h3>
            </div>
            <div className="mx-1">
              <h4>SR</h4>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Carddisplay;
