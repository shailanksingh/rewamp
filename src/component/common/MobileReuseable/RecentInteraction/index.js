import React from "react";
import Document from "assets/images/mobile/document.png";
import Clock from "assets/images/mobile/clock.png";
import Hospital from "assets/images/mobile/hospital.png";
import GetHelp from "assets/images/mobile/get-help.png";
import ArrowBlue from "assets/images/mobile/arrow-right-blue.png";
import health_policy_H from "assets/images/mobile/health_policy_H.svg";
import "./style.scss";

const RecentInteraction = ({
  minute = "30 Minutes Ago",
  docNumber = "17364427",
  progress = "In progress",
  title = "Car Wash",
  number = "Merc. Benz - 3576 TND",
  isHealth = false,
}) => {
  return (
    <>
      <div className="recent-card">
        <div className="d-flex justify-content-between">
          <p>
            <img src={Clock} alt="Clock" />
            {minute}
          </p>
          <div className="d-flex">
            <p className="pr-2">
              <img src={Document} alt="Document" />
              {docNumber}
            </p>
            <span>{progress}</span>
          </div>
        </div>
        <div className="hospital d-flex align-items-center">
          <img
            src={isHealth ? health_policy_H : Hospital}
            className="pr-2"
            alt="Hospital"
          />
          <div>
            <p>{title}</p>
            <h5>{number}</h5>
          </div>
        </div>
        <div className="help-section">
          <div className="get-help">
            <p>
              <img src={GetHelp} alt="GetHelp" />
              Get Help
            </p>
          </div>
          <div className="detail">
            <h6>View Details</h6>
            <img src={ArrowBlue} alt="ArrowBlue" />
          </div>
        </div>
      </div>
    </>
  );
};

export default RecentInteraction;
