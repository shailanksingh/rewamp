import React from "react";
import handiphone from "assets/svg/handiphone.png";
import applestore from "assets/svg/applestore.png";
import "./style.scss";

const Ithra = () => {
  return (
    <div className="container">
      <div className="card container appstore_mob border-0">
        <h5>
          Tawuniya App Quick,
          <br /> Easy & Hassle free
        </h5>
        <p className="">
          Our friendly customer support
          <br /> team is your extended family.
          <br /> Speak your heart out.{" "}
        </p>
        <p className="download">Download Now</p>
        <img className="appstore_img" src={applestore} alt="applestore" />
        <img className="handphone_img" src={handiphone} alt="handiphone" />
      </div>
    </div>
  );
};

export default Ithra;
