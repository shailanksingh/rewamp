import React from "react";
import "./style.scss";

const InsuranceCoverPlans = ({
  numberOfDays = 30,
  numberOfSAR = 195,
  description = "Cost of compulsory travel insurance for citizen",
  buttonTitle = "Buy Now",
}) => {
  return (
    <div className="mobile_insurance_cover">
      <div className="cover_header">
        <label className="fs-28">
          {numberOfDays}
          <span className="fs-12">Days</span>
        </label>
        <label className="fs-24">
          {numberOfSAR}
          <span className="fs-14 text-white">SAR</span>
        </label>
      </div>
      <div className="cover_body">
        <p>{description}</p>
        <button>{buttonTitle}</button>
      </div>
    </div>
  );
};

export default InsuranceCoverPlans;
