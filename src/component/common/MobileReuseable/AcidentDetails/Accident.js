import React from "react";
import card_com from "assets/images/mobile/card_com.png";
import calendar from "assets/images/mobile/calendar.png";
import _vehicle from "assets/images/mobile/taxi.png";
import file_comm from "assets/images/mobile/file_comm.png";
import "./Style.scss";
const Accident = ({
  minute = "Case No #12345",
  progress = "Under Process",
  peguout = "Peugeot",
  title = " - 3008 Allure",
  number = "3576 TND",
  claim = "Track Claim",
  policy = "Policy Number",
  reference = "Reference Number",
  accident = "Accident Date",
  request = "Request Dare",
  date = "1/1/2022",
  isHealth = false,

}) => {
  return (
    <>
      <div className="accident_card">
        <div className="d-flex justify-content-between">
          <p>{minute}</p>
          <div className="d-flex">
            <p className="pr-2"></p>
            <span>{progress}</span>
          </div>
        </div>
        <div className="policy ">
          <div className="d-flex">
            <img src={_vehicle} className="pr-2" alt="Hospital" />
            <div className="border-left mx-3">
              <div className="mx-4">
                <div className="d-flex">
                  <div className="paguout">
                    <p>{peguout}</p>
                  </div>
                  <div>
                    <p>{title}</p>
                  </div>
                </div>
                <h5>{number}</h5>
              </div>
            </div>
          </div>
          <div className="d-flex policy_data my-4">
           <div>
              <img src={card_com} className="pr-2" alt="Hospital" />
            </div>
            
              <div>
                <p>{policy}</p>
                <h5>{date}</h5>
              </div>
            
            <div className="mx-3">
              <div className="mx-3 d-flex">
                <div>
                  <img src={file_comm} className="pr-2" />
                </div>
                <div>
                  <p>{reference}</p>
                  <h5>{date}</h5>
                </div>
              </div>
            </div>
          </div>
          <div>
            <div className="d-flex policy_data my-4">
              <div>
                <img src={calendar} className="pr-2" alt="Hospital" />
              </div>
              <div>
                <p>{accident}</p>
                <h5>{date}</h5>
              </div>
              <div className="mx-3">
                <div className="mx-3 d-flex">
                  <div>
                    <img src={calendar} className="pr-2" />
                  </div>
                  <div>
                    <p>{request}</p>
                    <h5>{date}</h5>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div className="track_claim">
          <span>{claim}</span>
        </div>
      </div>
    </>
  );
};
export default Accident;
