
import React, { useState } from "react";
import close from "assets/images/mobile/Close.png";
import "./Style.scss";

const buttonListData = [
  {
    id: 1,
    label: "All Service",
    
  },
  {
    id: 2,
    label: "Motor",
    
  },
  {
    id: 3,
    label: "Health",
   
  },
  {
    id: 4,
    label: "Property and Casualty",
   
  },
];
const ServiceSticky = ({ getActiveProduct }) => {
  const [activeProduct, setActiveProduct] = useState(0);
  const activeProductSource = (index, ) => {
    setActiveProduct(index);
    getActiveProduct();
  };
  return (
    <div className="product_sticky_header">
      <div className="product_sticky_image">
        <img src={close} />
      </div>
      <div className="product_content">
        <p className="mb-0">Product & Services</p>
        <h4>Our Services</h4>
      </div>
      <div className="service_sticky_products">
        <div className="service_product_body">
          {buttonListData.map((val, index) => {
            return (
              <button
                className={activeProduct === index && "Service_active"}
                onClick={() => activeProductSource(index, val.sou)}
              >
                {val.label}
              </button>
            );
          })}
        </div>
      </div>
      {/* {activeProduct==""&&sourceList}
        {activeProduct==""&&sourceList}
        {activeProduct==""&&sourceList} */}
    </div>
  );
};
export default ServiceSticky;
