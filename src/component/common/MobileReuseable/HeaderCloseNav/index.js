import React from "react";
import "./Style.scss";
import close from "assets/images/mobile/Close.svg";
import { history } from "service/helpers";

const HeaderCloseNav = ({ pageName = "", title = "", isBottom = false }) => {
  return (
    <div className={`${isBottom && "is-bottom"} header_close_nav_container`}>
      {/* <img
        className="header_image_position"
        src={close}
        alt="Back"
        onClick={() => history.goBack()}
      /> */}
      <p className="pt-3">{pageName}</p>
      <h6>{title}</h6>
    </div>
  );
};

export default HeaderCloseNav;
