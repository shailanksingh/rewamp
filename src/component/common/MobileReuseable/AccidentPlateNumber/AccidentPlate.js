import React from 'react';
import "./Style.scss"
import _vehicle from "assets/images/mobile/taxi.png";
import calendar from "assets/images/mobile/calendar.png";
import file_comm from "assets/images/mobile/file_comm.png";

const AccidentPlate=(
    {caseTitle="Case Number",
    caseno="1234",
    accident="Accident Date",
    date="01/01/2022"}
    ) =>{
  return (
    <div>
       <div className="car_plate_number">
        <div className="silver_theme">
          <div className="red_car d-flex ">
          <img  className="mt-3 mx-2"src={_vehicle}  width="60" height='50'/>
          <div className=" gray_vertical_line"></div>
          <div className="mt-3 plate_number mx-1">
          <p>Plate Number</p>
          
          <h6>3576 TND</h6>
          </div>
          </div>
          <div className="d-flex ">
          <div className="d-flex mt-3 mx-3 case_number">
            <img src={file_comm} width='18' height='20'/>
            <div>
            <p>
            {caseTitle}
            </p>
            <h6>{caseno}</h6>
            </div>
            
          </div>
          <div className="d-flex mt-3  accident_date" >
            <img  className=""src={calendar} width='20' height='20'/>
            <div>
            <p>
             {accident}
            </p>
            <h6>{date}</h6>
            </div>
            
          </div>
          </div>
        
        </div>
        <div className="orange_create_claim mt-4">
          <p >Create Claim</p>
        </div>

       </div>
    </div>
  );
}

export default AccidentPlate;
