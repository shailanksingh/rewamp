import React from "react";
import "./style.scss";

const PlansForFutureCard = ({
  title = "Schengen",
  subTitle = "Including 26 Countries",
  buttonText = "Buy Now",
  details = "Plan Details",
  maxLimitText = "Annual Maximum Limit",
  maxLimitAmount = "1,000,000 SR",
  isAnnual = true,
}) => {
  return (
    <div className="mobile_plan_for_future">
      <div className="plan_header">
        <label>{title}</label>
        <p>{subTitle}</p>
      </div>

      {isAnnual && (
        <div className="annual_section">
          <h6>{maxLimitText}</h6>
          <label>{maxLimitAmount}</label>
        </div>
      )}
      <hr />
      <div className="plan_button_section">
        <button>{buttonText}</button>
        <p>{details}</p>
      </div>
    </div>
  );
};

export default PlansForFutureCard;
