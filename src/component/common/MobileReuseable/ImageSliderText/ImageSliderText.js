import React, { useState } from "react";
import "./style.scss";

const ImageSliderText = ({ banner, label, description }) => {
  const [datas, setDatas] = useState([
    {
      id: 1,
      description:
        "Smartphone enabled self-inspection processes takes minutes!",
      name: "Super-Simple Claims",
      checked: false,
      class: "test1",
    },

    {
      id: 2,
      description: "We are trusted by customers in our years of operations!",
      name: "Loved by Customers",
      checked: false,
      class: "test2",
    },
    {
      id: 3,

      description:
        "No hidden clauses ,jargon free documents in simple language",
      name: "More TLC , Less T&C",
      checked: false,
      class: "test3",
    },
  ]);
  return (
    <div className="image_slider_container">
      <img src={banner} alt="Banner" />
      <div className="description">
        <h6>{label}</h6>
        <p>{description}</p>
      </div>
    </div>
  );
};

export default ImageSliderText;
