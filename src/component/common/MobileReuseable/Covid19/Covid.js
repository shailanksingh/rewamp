import React from "react";
import Covid from "assets/images/mobile/Covid-19.png";
import Travel from "assets/images/mobile/Travelinsurance.png";
import "./Style.scss";
import { useState } from "react";
const MotorMobile = () => {
  const [services, setServices] = useState([
    {
      id: 1,
      image: Covid,
      title: "Covid-19 Insurance",
      description:
        "Covid-19 Travel insurance - for Saudis Program was designed to provide protection for Saudi citizens and supports.",
      Data: "Benefits:",
      list: "Provides you with the necessary health care ",
      list1: "Medical expenses incurred around the world.",
    },
    {
      id: 2,
      image: Travel,
      title: "International Travel Insurance",
      description:
        "The company has identified the amount of compensation so that it starts from SR 920 and up to SR 3,000,000 . ",
      Data: "Benefits:",
      list: "Provides you with the necessary health care ",
      list1: "Medical expenses incurred around the world.",
    },
    {
      id: 3,
      image: Travel,
      title: "International Travel Insurance",
      description:
        "The company has identified the amount of compensation so that it starts from SR 920 and up to SR 3,000,000 . ",
      Data: "Benefits:",
      list: "Provides you with the necessary health care ",
      list1: "Medical expenses incurred around the world.",
    },
    {
      id: 4,
      image: Travel,
      title: "International Travel Insurance",
      description:
        "The company has identified the amount of compensation so that it starts from SR 920 and up to SR 3,000,000 . ",
      Data: "Benefits:",
      list: "Provides you with the necessary health care ",
      list1: "Medical expenses incurred around the world.",
    },
    {
      id: 5,
      image: Travel,
      title: "International Travel Insurance",
      description:
        "The company has identified the amount of compensation so that it starts from SR 920 and up to SR 3,000,000 . ",
      Data: "Benefits:",
      list: "Provides you with the necessary health care ",
      list1: "Medical expenses incurred around the world.",
    },
  ]);
  
  return (
    <div>
      <div className="d-flex mobile_services_slider ">
        {services.map((services, index) => (
          <div key="id" className="mobile_services_card">
            <img className="" src={services.image} />
            <h5>{services.title}</h5>
            <p>{services.description}</p>
            <h4>{services.Data}</h4>
            <ul>
              <li>{services.list}</li>
              <li>{services.list1}</li>
            </ul>
          </div>
        ))}
        
      </div>
      </div>
  );
};

export default MotorMobile;
