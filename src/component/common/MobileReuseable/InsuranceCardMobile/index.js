import React, { useState } from "react";
import BottomPopup from "component/common/MobileReuseable/BottomPopup";
import RoadAssistanceMobilePage from "pages/RoadAssistanceMobilePage/RoadAssistanceMobilePage";

import insuranceArrow from "assets/svg/insuranceArrow.svg";
import "./style.scss";

export const InsuranceCardMobile = ({
  heathInsureCardData,
  textWidth,
  isArrow = true,
  rowWiseList = false,
}) => {
  const [isOpenServiceDetails, setIsOpenServiceDetails] = useState(false);
  const [checkservices, setCheckservices] = useState({});

  const servicedata = [
    {
      id: 1,
      title: "Motor Services",
      subtitle: "Periodic Inspection",
      content:
        " Sample text: 3 simple steps to reimburse the expenses outside the network Upon receiving the medical treatment outside the appointed network, the member will pay directly to the medical provider He/she can apply for reimbursement of his/her medical expenses by submitting a request via Tawuniya's website or Tawuniya App and uploading all necessary support documents. Tawuniya will settle the claim and the funds will be transferred to his/her bank account.",
    },
    {
      id: 2,
      subtitle: "Road Assistance",
      title: "Motor Services",
      content:
        " Sample text: 3 simple steps to reimburse the expenses outside the network Upon receiving the medical treatment outside the appointed network, the member will pay directly to the medical provider He/she can apply for reimbursement of his/her medical expenses by submitting a request via Tawuniya's website or Tawuniya App and uploading all necessary support documents. Tawuniya will settle the claim and the funds will be transferred to his/her bank account.",
    },
    {
      id: 3,
      subtitle: "Car Maintenance",
      title: "Motor Services",
      content:
        " Sample text: 3 simple steps to reimburse the expenses outside the network Upon receiving the medical treatment outside the appointed network, the member will pay directly to the medical provider He/she can apply for reimbursement of his/her medical expenses by submitting a request via Tawuniya's website or Tawuniya App and uploading all necessary support documents. Tawuniya will settle the claim and the funds will be transferred to his/her bank account.",
    },
    {
      id: 4,
      subtitle: "Car Accident",
      title: "Motor Services",
      content:
        " Sample text: 3 simple steps to reimburse the expenses outside the network Upon receiving the medical treatment outside the appointed network, the member will pay directly to the medical provider He/she can apply for reimbursement of his/her medical expenses by submitting a request via Tawuniya's website or Tawuniya App and uploading all necessary support documents. Tawuniya will settle the claim and the funds will be transferred to his/her bank account.",
    },
    {
      id: 5,
      subtitle: "Car Wash",
      title: "Motor Services",
      content:
        " Sample text: 3 simple steps to reimburse the expenses outside the network Upon receiving the medical treatment outside the appointed network, the member will pay directly to the medical provider He/she can apply for reimbursement of his/her medical expenses by submitting a request via Tawuniya's website or Tawuniya App and uploading all necessary support documents. Tawuniya will settle the claim and the funds will be transferred to his/her bank account.",
    },
    {
      id: 6,
      subtitle: "Refill Medication",
      title: "Health Services",
      content:
        "Receive your medication without visiting your doctor. This service enables you to re-ﬁlling the medicines of chronic diseases that are used for long periods ranging from one to six months, such as diabetes, pressure, and other conditions. So that you can directly receive them from any of the approved pharmacy networks without the need to see your doctor.",
    },
    {
      id: 7,
      subtitle: "Medical Reimbursement",
      title: "Health Services",
      content:
        " Reimburse your expenses outside the network While you are being treated outside the approved network, you will pay directly to the medical provider. In this case, you can apply for reimbursement of your medical expenses by submitting a request via Tawuniya’s website or Tawuniya App and uploading all necessary support documents. Tawuniya will settle the claim, and the funds will be transferred to your bank account",
    },
    {
      id: 8,
      subtitle: "Telemedicine",
      title: "Health Services",
      content:
        "With the Doctor Consultation service, you will no longer need to book an appointment in the hospital. This service allows you to book a reliable online consultation appointment from accredited doctors in all health specialties via the smartphone app and website.",
    },
    {
      id: 9,
      subtitle: "Eligibility letter",
      title: "Medical Services",
      content:
        "Your right to obtain treatment is reserved In case you have any problem while visiting the approved medical service provider, Tawuniya will issue an eligibility letter, conﬁrming your right to get the necessary treatment.",
    },
    {
      id: 10,
      subtitle: "Pregnancy Program",
      title: "Medical Services",
      content:
        "An unforgettable pregnancy journey We will do our best to make your pregnancy journey full of memorable and beautiful memories through the pregnancy follow-up program that takes care of a pregnant mother and increases her awareness of all procedures and provide awarness tips during all the phases of pregnancy",
    },
    {
      id: 11,
      subtitle: "Chronic Disease Management",
      title: "Medical Services",
      content:
        "We take care of your health to the smallest detail If you suffer from any chronic disease, this service allows you to get comprehensive health care that takes care of the smallest detail. From medical consultations to receiving the necessary medication, in addition to laboratory services. In some cases, the service is provided at your residence.",
    },
    {
      id: 12,
      subtitle: "Home Child Vaccination",
      title: "Medical Services",
      content:
        "Vaccinating your child at home Vaccination is essential for your child's safety and protection, which is part of our responsibility. Therefore, we provide vaccination service at home to Tawuniya members whose age between 0 – 7 years based on the basic vaccinations schedule issued by MOH and included in the Cooperative Health Insurance Uniﬁed Policy published by CCHI.",
    },
    {
      id: 13,
      subtitle: "Assist America",
      title: "Medical Services",
      content:
        "Your enrollment through Tawuniya Company includes a unique global emergency assistance program provided by Assist America®. This program immediately connects you to qualified healthcare providers, hospitals, pharmacies and other services if you experience an emergency while traveling more than 150 kilometers away from your permanent resident or in another country, for up to 90 days. Our international partner Assist America for emergnecy servcies provides you with many services: • Medical evacuation • Second Opinion • Medical transition • Repatriation",
    },
    {
      id: 14,
      subtitle: "Flight Delay Assistance",
      title: "Medical Services",
      content:
        "If your flight is delayed by the period you have chosen , and you got our  Flight delay compensation Insurance, then you don't need to worry any further! Tawuniya will automatically compensate you. How will you receive  your reimbursement? You will receive an SMS on your mobile number that is restored in our records when you flight delayed  . Once you provide your IBAN number, you will receive the amount in your bank account within  three days . When are you entitled for a flight delay compensation ? You are entitled to  flight delay compensation if your flight was late for the period you chosen.",
    },
  ];

  const handleselect = (e) => {
    if (e.currentTarget.id === "1") {
      setIsOpenServiceDetails(true);
      setCheckservices(servicedata[0]);
    }
    if (e.currentTarget.id === "2") {
      setIsOpenServiceDetails(true);
      setCheckservices(servicedata[1]);
    }
    if (e.currentTarget.id === "3") {
      setIsOpenServiceDetails(true);
      setCheckservices(servicedata[2]);
    }
    if (e.currentTarget.id === "4") {
      setIsOpenServiceDetails(true);
      setCheckservices(servicedata[3]);
    }
    if (e.currentTarget.id === "5") {
      setIsOpenServiceDetails(true);
      setCheckservices(servicedata[4]);
    }
    if (e.currentTarget.id === "6") {
      setIsOpenServiceDetails(true);
      setCheckservices(servicedata[5]);
    }
    if (e.currentTarget.id === "7") {
      setIsOpenServiceDetails(true);
      setCheckservices(servicedata[6]);
    }
    if (e.currentTarget.id === "8") {
      setIsOpenServiceDetails(true);
      setCheckservices(servicedata[7]);
    }
    if (e.currentTarget.id === "9") {
      setIsOpenServiceDetails(true);
      setCheckservices(servicedata[8]);
    }
    if (e.currentTarget.id === "10") {
      setIsOpenServiceDetails(true);
      setCheckservices(servicedata[9]);
    }
    if (e.currentTarget.id === "11") {
      setIsOpenServiceDetails(true);
      setCheckservices(servicedata[10]);
    }
    if (e.currentTarget.id === "12") {
      setIsOpenServiceDetails(true);
      setCheckservices(servicedata[11]);
    }
    if (e.currentTarget.id === "13") {
      setIsOpenServiceDetails(true);
      setCheckservices(servicedata[12]);
    }
    if (e.currentTarget.id === "14") {
      setIsOpenServiceDetails(true);
      setCheckservices(servicedata[13]);
    }
    // if(e.currentTarget.id==="15"){
    //   setIsOpenServiceDetails(true)
    //   setCheckservices(servicedata[14])
    // }
  };
  return (
    <div
      className={`insuranceBannerCardContainerMobile ${
        rowWiseList && "row m-0 row_wice_list"
      }`}
    >
      {heathInsureCardData?.map((item, index) => {
        return (
          <div key={index} className="insuranceBannerCardMobile">
            <div id={item.id} onClick={handleselect}>
              <img
                src={item.cardIcon}
                className="img-fluid"
                id={item.contentAlign}
                alt="icon"
              />
              <p
                className={`${"InsuranceCardText"} fs-16 fw-800  pt-2`}
                id={textWidth}
              >
                {item.content}
              </p>
              {!isArrow && (
                <img
                  src={insuranceArrow}
                  className="img-fluid pb-3"
                  alt="icon"
                />
              )}
            </div>
          </div>
        );
      })}

      <BottomPopup
        open={isOpenServiceDetails}
        setOpen={setIsOpenServiceDetails}
        bg={"gray"}
      >
        <RoadAssistanceMobilePage services={checkservices} />
      </BottomPopup>
    </div>
  );
};
