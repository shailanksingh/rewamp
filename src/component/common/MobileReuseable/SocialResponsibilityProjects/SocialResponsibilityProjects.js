import React from "react";
import "./style.scss";

const SocialResponsibilityProjects = ({
  tagTitle = "News",
  days = "",
  title = "",
  hideline,
 
}) => {
  return (
    <div>
      <div className=" mt-3  competitive_advantage ">
        <div className="d-flex justify-content-between">
          <div className="news_tag">{tagTitle}</div>

          <div className="competitive_days">{days}</div>
        </div>

        <div className="competitive_title mt-3">{title}</div>
       {!hideline &&(<hr></hr>)}
      </div>
    </div>
  );
};

export default SocialResponsibilityProjects;
