import React from "react";
import "./style.scss";

import social_media_fb from "assets/images/mobile/social_media_fb.svg";
import social_media_instagram from "assets/images/mobile/social_media_instagram.svg";
import social_media_linkin from "assets/images/mobile/social_media_linkin.svg";
import social_media_twitter from "assets/images/mobile/social_media_twitter.svg";
import social_media_youtube from "assets/images/mobile/social_media_youtube.svg";
import social_media_whastsapp from "assets/images/mobile/social_media_whastsapp.svg";
import footer_tawuniya_logo from "assets/images/mobile/footer_tawuniya_logo.svg";

const FooterMobile = () => {
  return (
    <div className="footer_container">
      <div className="follow_as_section">
        <h6>FOLLOW US</h6>
        <img src={social_media_twitter} alt="Twitter" />
        <img src={social_media_fb} alt="Twitter" />
        <img src={social_media_youtube} alt="Twitter" />
        <img src={social_media_linkin} alt="Twitter" />
        <img src={social_media_instagram} alt="Twitter" />
        <img src={social_media_whastsapp} alt="Twitter" />
      </div>
      
      <div className="footer_bottom_section row">
        <div className="col-6">
          <p>Terms and Conditions</p>
          <p>Privacy Policy</p>
          <p>Cookie Policy</p>
        </div>
        <div className="col-6">
          <p>Tawuniya Auctions</p>
          <p>Consumer Protection Rules And Instructions</p>
          <p>Online Insurance Activities Regulation</p>
          <p>Committee of Insurance Disputes</p>
        </div>
        
      </div>
      <div className="copy_right_section">
        <img src={footer_tawuniya_logo} alt="Plus" />
        <label>Copyright © Tawuniya 2022, all rights reserved</label>
      </div>
    </div>
  );
};

export default FooterMobile;
