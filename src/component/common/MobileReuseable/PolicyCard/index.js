import React from "react";
import heart_beat_health from "assets/images/mobile/heart_beat_health.svg";
import green_rouded_tick from "assets/images/mobile/green_rouded_tick.svg";
import health_policy_company_logo from "assets/images/mobile/health_policy_company_logo.svg";
import weekly_points from "assets/images/mobile/weekly_points.svg";
import { history } from "service/helpers";
import badge_policy from "assets/images/mobile/badge_policy.svg";
import trophy from "assets/images/mobile/trophy.svg";
import progress_policy from "assets/images/mobile/progress_policy.svg";
import expand_blue from "assets/images/mobile/expand_blue.svg";

import "./style.scss";
import { useSelector } from "react-redux";

const PolicyCard = ({
	viewDetails,
	setIsMedical,
	viewDesktopPolicyDetails,
	pageName,
}) => {
	const fullName = useSelector(
		(state) => state?.loginDetailsReducer?.loginResponse?.user_info?.fullName
	);
	return (
		<div
			className={
				"policy_card_container " +
				(pageName === "your-policies" ? "current-page-your-policies" : "")
			}
			onClick={() => {
				setIsMedical && setIsMedical(true);
			}}
		>
			<div className="d-flex align-items-start">
				<div className="card_header_section">
					<div className="health_title">
						<img src={heart_beat_health} alt="Heart" />
						<label>HEALTH</label>
					</div>
					<div className="plolicy_number">
						<label>Ploicy # 23445</label>
						<label className="status">
							Najm <img src={green_rouded_tick} alt="Status" />
						</label>
					</div>
				</div>
				<img src={health_policy_company_logo} alt="Logo" />
			</div>

			<div className="policy_body_section">
				{pageName === "your-policies" && <p>Memeber # 22222222222</p>}
				<h6>{fullName}</h6>
				{pageName !== "your-policies" && <p>Memeber # 22222222222</p>}
				<div className="details">
					<p>VIP +</p>
					<label>
						CO Pay:
						<br /> <span>10%</span>
					</label>
					<label>
						Max Pay:
						<br /> <span>100</span> SAR
					</label>
					<label>
						AP. Limit: <br />
						<span>2000</span> SAR
					</label>
				</div>
			</div>
			<hr className="policy_border" />
			<div className="policy_footer">
				<div className="weekly_points">
					<img src={weekly_points} alt="Points" />
					<p>
						Weekly Points <span>6000</span>
					</p>
				</div>
				<div className="body_section">
					<img src={badge_policy} alt="Badge" />
					<img src={trophy} alt="Trophy" />
					<span />
				</div>
				<div className="progress_policy">
					<img src={progress_policy} alt="Progress" />
					<p>
						Expires in <span>6 Month</span>
					</p>
				</div>
				<div className="expand">
					<img
						src={expand_blue}
						alt="Expand"
            onClick={() => {
              if (viewDesktopPolicyDetails !== undefined) {
                if (viewDesktopPolicyDetails) {
                  history.push("/dashboard/policydetails/health")
                } else {
                  viewDetails("Health")
                }
              }
            }}
					/>
				</div>
			</div>
		</div>
	);
};

export default PolicyCard;
