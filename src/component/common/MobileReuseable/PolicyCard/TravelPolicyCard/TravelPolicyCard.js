import React from "react";
import policy_tawuniya_logo from "assets/images/mobile/policy_tawuniya_logo.svg";
import flight_small from "assets/images/mobile/flight_small.svg";
import progress_policy from "assets/images/mobile/progress_travel.svg";
import expand_green from "assets/images/mobile/expand_green.svg";
import flight_white from "assets/images/mobile/flight_white.svg";
import globe_white from "assets/images/mobile/globe_white.svg";
import { history } from "service/helpers";
import "./style.scss";
import { useSelector } from "react-redux";

const TravelPolicyCard = ({
	isShrink = false,
	viewDetails,
	viewDesktopPolicyDetails,
	setIsMedical,
}) => {
	const fullName = useSelector(
		(state) => state?.loginDetailsReducer?.loginResponse?.user_info?.fullName
	);
	return (
		<div className={`travel_policy_card_container ${isShrink && "shrink"}`}>
			<div className="d-flex align-items-start">
				<div className="card_header_section">
					<div className="health_title">
						<img src={flight_white} alt="Heart" />
						{!isShrink && <label>Travel</label>}
						{isShrink && (
							<label className="shrink">
								<span>Travel</span>Insurance
							</label>
						)}
					</div>
				</div>
				{isShrink && <label className="fs-12 mb-0">A12345676</label>}
				{!isShrink && <img src={policy_tawuniya_logo} alt="Logo" />}
			</div>

			<div className="policy_body_section">
				<p>Ploicy # 22222222222</p>
				<h6>{fullName}</h6>
				<div className="details">
					<img src={globe_white} alt="World" />
					<label>Worldwide</label>
					<p className="mr-2">+ Covid 19</p>
					<p>+ Flight Delay</p>
				</div>
			</div>
			<hr className="policy_border" />
			<div className="policy_footer">
				<div className="progress_policy">
					<img src={progress_policy} alt="Progress" />
					<p>
						Expires in <span>6 Month</span>
					</p>
				</div>

				<div className="body_section">
					<img src={flight_small} alt="Badge" />
					<p>
						Departure
						<br />
						<span>23 Jan 2022</span>
					</p>
					{/* <div /> */}
				</div>
				<div className="body_section">
					<img src={flight_small} alt="Badge" />
					<p>
						Arrival
						<br />
						<span>23 Jan 2022</span>
					</p>
				</div>

				<div className="expand">
					<img
						src={expand_green}
						alt="Expand"
						onClick={() => {
							if (viewDesktopPolicyDetails !== undefined) {
								if (viewDesktopPolicyDetails) {
									history.push("/dashboard/policydetails/travel");
								} else {
									viewDetails("Travel");
								}
							}
						}}
					/>
				</div>
			</div>
		</div>
	);
};

export default TravelPolicyCard;
