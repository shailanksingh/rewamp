import React from "react";
import new_policy_logo from "assets/images/mobile/new_policy_logo.svg";
import plus_rounded from "assets/images/mobile/plus_rounded.svg";

import "./style.scss";

const NewPolicyCard = ({}) => {
  return (
    <div className={"new_policy_card_container"}>
      <div className="card_header_section">
        <div className="health_title">
          <img src={new_policy_logo} alt="Heart" />
          <label>Get New Insurance Policy</label>
        </div>
        <img src={plus_rounded} alt="Heart" />
      </div>
    </div>
  );
};

export default NewPolicyCard;
