import React from "react";
import white_car from "assets/images/mobile/white_car.svg";
import green_rouded_tick from "assets/images/mobile/green_rouded_tick.svg";
import policy_tawuniya_logo from "assets/images/mobile/policy_tawuniya_logo.svg";
import motor_D from "assets/images/mobile/motor_D.svg";
import Wallet from "assets/images/mobile/Wallet.svg";
import progress_policy from "assets/images/mobile/progress_policy.svg";
import expand_red from "assets/images/mobile/expand_red.svg";
import isEmpty from "lodash/isEmpty";
import { history } from "service/helpers";

import "./style.scss";
import { useSelector } from "react-redux";

const MotorPolicyCard = ({
	isShrink = false,
	setPolicyListView,
	viewDetails,
	viewDesktopPolicyDetails,
	policy,
}) => {
	const fullName = useSelector(
		(state) => state?.loginDetailsReducer?.loginResponse?.user_info?.fullName
	);
	const numberPlate = policy?.SDescription.split("No. ")[1];
	const carName = policy?.SDescription.split(" ").slice(0, 2).join(" ");
	return (
		<div className={`motor_policy_card_container ${isShrink && "shrink"}`}>
			<div
				className="d-flex align-items-start"
				onClick={() => (isShrink ? setPolicyListView(false) : console.log(""))}
			>
				<div className="card_header_section">
					<div className="health_title">
						<img src={white_car} alt="Heart" />
						{!isShrink && <label>Motor</label>}
						{isShrink && (
							<label className="shrink">
								<span>Motor</span>
								{"-"}
								{carName}
							</label>
						)}
					</div>
					{!isShrink && (
						<div className="plolicy_number">
							<label className="status">
								Najm <img src={green_rouded_tick} alt="Status" />
							</label>
						</div>
					)}
				</div>
				{isShrink && <label className="fs-12 mb-0">{numberPlate}</label>}
				{!isShrink && <img src={policy_tawuniya_logo} alt="Logo" />}
			</div>

			<div className="policy_body_section">
				{!isEmpty(policy) && <p>Ploicy #{policy.SPolicyNo}</p>}
				{!isEmpty(fullName) && <h6>{fullName}</h6>}
				<div className="details">
					{!isEmpty(policy) && (
						<p>
							{policy.SLob} -{" "}
							{policy.SProduct.substring(0, policy.SProduct.indexOf("-"))}
						</p>
					)}
					<label>{carName}</label>
					<label>{numberPlate}</label>
				</div>
			</div>
			<hr className="policy_border" />
			<div className="policy_footer">
				<div className="weekly_points">
					<img src={motor_D} alt="Points" />
					<p>
						W.Score <span>83%</span>
					</p>
				</div>

				<div className="body_section">
					<img src={Wallet} alt="Badge" />
					<p>
						Currency
						<br />
						<span>20</span> SAR
					</p>
					<div />
				</div>

				<div className="progress_policy">
					<img src={progress_policy} alt="Progress" />
					<p>
						Expires in <span>6 Month</span>
					</p>
				</div>
				<div className="expand">
					<img
						src={expand_red}
						alt="Expand"
						onClick={() => {
							if (viewDesktopPolicyDetails !== undefined) {
								if (viewDesktopPolicyDetails) {
									history.push("/dashboard/policydetails/motor");
								} else {
									viewDetails("Motor");
								}
							}
						}}
					/>
				</div>
			</div>
		</div>
	);
};

export default MotorPolicyCard;
