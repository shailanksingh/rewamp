import React from "react";
import "./style.scss";
import community_side from "assets/images/mobile/community_side.svg";
import community_side_two from "assets/images/mobile/community_side_two.svg";

const JoinOurCommunity = () => {
  return (
    <div className="community_container">
      <h6>Join our community and help define it.</h6>
      <p>
        Explore a collaborative culture of inclusion, growth, and originality,
        supported by resources that make a difference in your life.
      </p>
      <button
        onClick={() =>
          window.open(
            "https://www.linkedin.com/company/tawuniya/jobs/",
            "_self"
          )
        }
      >
        Join Our Community
      </button>
      <img src={community_side} alt="logo" />
      <img src={community_side_two} alt="logo" />
    </div>
  );
};

export default JoinOurCommunity;
