import React from "react";
import "./style.scss";
import claimassistance from "assets/images/mobile/claimassistance.png";

const MakeClaimCard = ({
  title = "How do I make a claim?",
  description = "Tawuniya believes in simplification and automation wherever possible. Our approach to the claims journey for Tawuniya is no different. We want you to notify us of claims as soon as possible and we want to get your claim moving as soon as possible. Our simple steps approach helps us to keep that focus throughout the claim process.",
  buttonTitle = "Claim Assistance",
}) => {
  return (
    <div className="makeaclaim">
      <label>{title}</label>
      <p>{description}</p>
      <button className="claimbutton">{buttonTitle}</button>
      <img src={claimassistance} alt="claimassistance" className="w-100"></img>
    </div>
  );
};

export default MakeClaimCard;
