import React, { useState, useCallback } from "react";
import { history } from "service/helpers";
import Dropzone from "react-dropzone";
import { NormalSearch } from "../NormalSearch";
import { NormalInput } from "../NormalInput";
import PhoneNumberInput from "component/common/PhoneNumberInput/index";
import { NormalRadioButton } from "../NormalRadioButton";
import { NormalButton } from "../NormalButton";

import { NormalSelect } from "../NormalSelect";
import addMail from "assets/svg/Add Mail.svg";
import KSAFlagImage from "assets/images/ksaFlag.png";
import IndiaFlagImage from "assets/images/indiaFlag.png";
import officeBag from "assets/svg/Office Bag.svg";
import exclamation from "assets/svg/Exclamation Mark.svg";
import upload from "assets/svg/uploadFileIcon.svg";
import profile from "assets/svg/userProfileIcon.svg";
import selectDropDown from "assets/svg/complaintSelectDropdown.svg";
import closeIcon from "assets/svg/canvasClose.svg";
import uploader from "assets/svg/browseFileUploader.svg";
import "./style.scss";
import { NormalCheckbox } from "../NormalCheckBox";
import { Data } from "@react-google-maps/api";

import BottomPopup from "component/common/MobileReuseable/BottomPopup";
import UpdateVerification from "component/MyProfile/Mobile/UpdateVerification";

const MobileContactForm = ({ needForm = false }) => {
  const [files, setFiles] = useState([]);

  const [check, setCheck] = useState([]);
  const [showPreview, setShowPreview] = useState(false);
  const [isChecked, setIsChecked] = useState(false);
  const [isverficationModel, setIsverficationModel] = useState(false);
  const [open, setOpen] = useState(false);

  const handleCheck = (e) => {
    setIsChecked(e.target.checked);
    console.log(isChecked, "jsj");
  };

  const onDrop = useCallback((acceptedFiles) => {
    setFiles(
      acceptedFiles.map((file) =>
        Object.assign(file, {
          preview: URL.createObjectURL(file),
        })
      )
    );
    if (acceptedFiles && acceptedFiles[0]) {
      const formdata = new FormData();
      formdata.append("image", acceptedFiles[0]);
    }
    setShowPreview(true);
    console.log("ajj", files);
  });

  const images = files.map((file) => (
    <div
      className="dnd-container"
      sx={{
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        width: "100%",
        height: "100%",
      }}
      key={file.name}
    >
      <img
        style={{ width: "100px" }}
        className="dnd-img mx-auto d-block"
        src={file.preview}
        alt="Screen"
      />
    </div>
  ));

  const removeImageHandler = (e) => {
    // const arr = files;
    // arr.splice(item, 1);
    // setFiles([...arr]);
    e.stopPropagation();
    setFiles([]);
    setShowPreview(false);
  };

  const [formInput, setFormInput] = useState({
    userId: "",
    policyId: "",
    message: "",
  });

  let dialingCodes = [
    {
      code: "+966",
      image: KSAFlagImage,
    },
    {
      code: "+91",
      image: IndiaFlagImage,
    },
  ];

  let [selectedCode, setSelectedCode] = useState(dialingCodes[0]);

  let [phoneNumber, setPhoneNumber] = useState("");

  //initialiize state for radio buttons
  const [radioValue, setRadioValue] = useState("Motor");

  const handleFormInputChange = (e) => {
    const { name, value } = e.target || e || {};
    setFormInput({ ...formInput, [name]: value });
  };

  const handleChange = (event) => {
    let isChecked = event.target.checked;
    let value = event.target.value;
    let array = [];
    if (isChecked) {
      array = [...check];
      array.push(+value);
    } else {
      array = check.filter((id) => +id !== +value);
    }
    setCheck([...array]);
    console.log(check, "bol");
  };

  return (
    <div className="row m-0">
      <div className="col-12">
        <div className="d-flex justify-content-center pt-4">
          <div className="openFormContainer_contact">
            <p className="fs-20 fw-800 complaint-formTitle m-0">Inquery Form</p>
            <p className="fs-16 fw-400 pt-1 complaint-formPara">
              Let us know how can we reach you
            </p>
            <p className="fs-16 fw-800 personalTitle">Personal Details</p>
            <div className="row">
              <div className="col-12">
                <NormalSearch
                  className="formInputFieldOne"
                  name="firstname"
                  placeholder="First Name"
                />
              </div>
              <div className="col-12 pt-3">
                <NormalSearch
                  className="formInputFieldOne"
                  name="lasttname"
                  placeholder="Last Name"
                />
              </div>

              <div className="col-12 pt-3">
                <PhoneNumberInput
                  className="formPhoneInput"
                  selectInputClass="formSelectInputWidth"
                  selectInputFlexType="formSelectFlexType"
                  dialingCodes={dialingCodes}
                  selectedCode={selectedCode}
                  setSelectedCode={setSelectedCode}
                  value={phoneNumber}
                  name="phoneNumber"
                  onChange={({ target: { value } }) => setPhoneNumber(value)}
                />
              </div>

              <div className="col-12 pt-3">
                <NormalButton
                  label="Send OTP"
                  className="complaintFormSubmitBtn btnbg p-4"
                  onClick={() => setIsverficationModel(true)}
                />
              </div>

              <div className="col-12 pt-3">
                <NormalSearch
                  className="formInputFieldemail"
                  name="userId"
                  placeholder="ex: email@tawuniya.com.sa"
                  needLeftIcon={true}
                  leftIcon={addMail}
                />
              </div>

              <div className="col-12 pt-3">
                <p className="fs-16 fw-400 pt-1 complaint-formPara">
                  Preferred way to contact you
                </p>
              </div>

              <div className="col-12 pt-3 ">
                <NormalRadioButton
                  type="radio"
                  name="radioOne"
                  value="Email"
                  onChange={(e) => setRadioValue(e.target.value)}
                  radioValue="Email"
                  checked={radioValue === "Email"}
                  s
                  isStyledRadio={true}
                  radioContainer={
                    radioValue === "Email"
                      ? "higlightRadioContainer"
                      : "normalRadioContainer"
                  }
                />
              </div>
              <div className="col-12 pt-3">
                <NormalRadioButton
                  type="radio"
                  name="radioOne"
                  value="Phone/Mobile Number"
                  onChange={(e) => setRadioValue(e.target.value)}
                  radioValue="Phone/Mobile Number"
                  checked={radioValue === "Phone/Mobile Number"}
                  isStyledRadio={true}
                  radioContainer={
                    radioValue === "Phone/Mobile Number"
                      ? "higlightRadioContainer"
                      : "normalRadioContainer"
                  }
                />
              </div>
            </div>
            <div className="borderLiningFour my-4"></div>
            <div className="pt-3 pb-2">
              <p className="fw-800 fs-16">Reason of Contact</p>
            </div>
            <div className="row pt-3">
              <div className="col-12 ">
                <NormalRadioButton
                  type="radio"
                  name="radioOne"
                  value="Inquiry"
                  onChange={(e) => setRadioValue(e.target.value)}
                  radioValue="Inquiry"
                  checked={radioValue === "Inquiry"}
                  isStyledRadio={true}
                  radioContainer={
                    radioValue === "Inquiry"
                      ? "higlightRadioContainer"
                      : "normalRadioContainer"
                  }
                />
              </div>
              <div className="col-12 pt-3">
                <NormalRadioButton
                  type="radio"
                  name="radioOne"
                  value="Suggestion"
                  onChange={(e) => setRadioValue(e.target.value)}
                  radioValue="Suggestion"
                  checked={radioValue === "Suggestion"}
                  isStyledRadio={true}
                  radioContainer={
                    radioValue === "Suggestion"
                      ? "higlightRadioContainer"
                      : "normalRadioContainer"
                  }
                />
              </div>

              <div className="col-12 pt-3">
                <NormalRadioButton
                  type="radio"
                  name="radioOne"
                  value="Question"
                  onChange={(e) => setRadioValue(e.target.value)}
                  radioValue="Question"
                  checked={radioValue === "Question"}
                  isStyledRadio={true}
                  radioContainer={
                    radioValue === "Question"
                      ? "higlightRadioContainer"
                      : "normalRadioContainer"
                  }
                />
              </div>
            </div>

            <div className="borderLiningFour my-4"></div>

            <div className="pt-3 pb-2">
              <p className="messageTitle fs-14 fw-700 m-0 pb-2">Your Message</p>
            </div>

            <div className="pt-1">
              <NormalSearch
                className="formInputFieldOne"
                name="Subject"
                placeholder="Subject"
              />
            </div>

            <div className="pt-3 pb-2">
              <p className="messageTitle fs-14 fw-700 m-0 pb-2">Your Message</p>
              <textarea className="complaintForm-textArea">
                Type here...
              </textarea>
            </div>

            {/* <div className="borderLiningFour my-4"></div> */}
            <NormalButton
              label="Send"
              className="complaintFormSubmitBtn p-4"
              onClick={() => setOpen(true)}
            />
          </div>
        </div>
      </div>

      <BottomPopup open={isverficationModel} setOpen={setIsverficationModel}>
        <UpdateVerification />
      </BottomPopup>
    </div>
  );
};

export default MobileContactForm;
