import React from "react";
// import { Card, Button } from "react-bootstrap";
import { Box, Typography, Card, Icon, Divider } from "@material-ui/core";
import serviceArrow from "../../../assets/svg/Arrows/serviceArrow.svg";
import styles from "./style.module.scss";
import { history } from "service/helpers";

function CardComp(props) {
  const { item, routeURL } = props;

  // console.log(item);
  return (
    <Box
      className={styles.sCardContainer}
      sx={{
        mb: 4,
      }}
    >
      <Box className={styles.sCardTextBox}>
        <Typography className={styles.sCardBoxHeading} variant="p">
          {item.headingEl}
        </Typography>
        <Typography
          className={styles.sCardBoxDescription}
          component={"p"}
          variant="p"
        >
          {item.discrptionEl}
        </Typography>
      </Box>
      <Box className={styles.sCardIconBox}>
        <img src={item.iconE1} className={styles.tawuniyaSliderCardIcons} alt="Motor" />

        <img
          className={styles.sCardArrow}
          src={serviceArrow}
          alt="Arrow"
          onClick={() =>
            routeURL
              ? history.push("/home/customerservice/servicequestions")
              : console.log("")
          }
        />
      </Box>
    </Box>
  );
}

export default CardComp;
