import React from "react";
import { NormalButton } from "component/common/NormalButton";
import "./style.scss";

export const LegendarySupport = ({
  legendarySupportData,
  needHeader = false,
}) => {
  return (
    <div className="row legendarySupportContainer">
      {legendarySupportData.map((item, index) => {
        return (
          <React.Fragment>
            {needHeader && (
              <div className="col-lg-12 col-12 pt-3" key={index}>
                <p className="fw-800 cantWaitTitle m-0 text-center">
                  {item.heading}
                </p>
                <p className="contactText fs-16 fw-400 text-center">
                  {item.para}
                </p>
              </div>
            )}
            <div className="col-lg-6 col-md-12 col-12 pt-3">
              <div className="legendarySupportOne pt-2 pb-lg-0 pb-3">
                <div className="supportOneContainer">
                  <div className="layerOne">
                    <p className="fw-800 lengendaryTitle m-0 pb-2">
                      {item.fraudTitleOne}
                    </p>
                    <p className="fs-14 fw-400 legendaryPara">
                      {item.legendParaOne}
                    </p>
                    <div className="d-flex flex-row">
                      <div>
                        <NormalButton
                          label={item.btnLabelOne}
                          className="reportFraudBtn p-4"
                        />
                      </div>
                      <div className="pl-3">
                        <NormalButton
                          label="Learn More"
                          className="learnMoreBtn p-4"
                        />
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="col-lg-6 col-md-12 col-12 pt-3">
              <div className="legendarySupportTwo pt-2 pb-lg-0 pb-3">
                <div className="supportTwoContainer">
                  <div className={`${item.positionLayerTwo} layerTwo`}>
                    <p className="fw-800 lengendaryTitleNew m-0 pb-2">
                      {item.fraudTitleTwo}
                    </p>
                    <p className="fs-14 fw-400 legendaryParaNew">
                      {item.legendParaTwo}
                    </p>
                    <div className="d-flex flex-row">
                      <div>
                        <NormalButton
                          label={item.btnLabelTwo}
                          className="reportFraudBtn p-4"
                        />
                      </div>
                      <div className="pl-3">
                        <NormalButton
                          label="Learn More"
                          className="learnMoreBtn p-4"
                        />
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </React.Fragment>
        );
      })}
    </div>
  );
};
