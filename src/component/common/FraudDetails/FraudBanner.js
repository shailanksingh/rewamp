import React from "react";
import { useDispatch } from "react-redux";
import { requestedServiceSelector } from "action/LanguageAct";
import { history } from "service/helpers";
import { NormalButton } from "component/common/NormalButton";
import "./style.scss";

export const FraudBanner = ({ bannerData }) => {
	const dispatch = useDispatch();
	function fraudServiceNavigator(url, serviceRequested) {
		history.push({
			pathname: `/home/customerservice/${url}`,
		});
		dispatch(requestedServiceSelector(serviceRequested));
	}
	return (
		<div className="row SupportRequestContainer p-3">
			{bannerData.map((item, index) => {
				return (
					<React.Fragment>
						<div
							className={`${item.bannerLayout} col-lg-12 col-12`}
							key={index}
						>
							<p className="fw-800 fraudTitle">{item.fraudTitle}</p>
							<p className="fraudParaOne fs-18 fw-400">{item.fraudPara}</p>
							<ul className="p-0 pl-4">
								{item?.fraudList?.map((items, index) => {
									return (
										<li key={index} className="fraudParaTwo fs-18 fw-400">
											{items.para}
										</li>
									);
								})}
							</ul>
							<div className="d-flex flex-row">
								<div>
									<NormalButton
										label={item.btnLabel}
										className="reportFraudBtn"
										onClick={() => {
											fraudServiceNavigator(item.url, item.serviceRequested);
										}}
									/>
								</div>
								<div className="pl-3">
									<NormalButton
										label="Learn More"
										className="learnMoreBtn"
										onClick={() => {
											fraudServiceNavigator(item.url, item.serviceRequested);
										}}
									/>
								</div>
							</div>
							<img
								src={item.bannerPic}
								className={`${item.fraudBannerImgClass} img-fluid`}
								alt="banner"
							/>
						</div>
					</React.Fragment>
				);
			})}
		</div>
	);
};
