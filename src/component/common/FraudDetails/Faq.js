import React, { useContext, useState } from "react";
import Accordion from "react-bootstrap/Accordion";
import { useAccordionButton, AccordionContext } from "react-bootstrap";
import { NormalButton } from "component/common/NormalButton";
import greyArrow from "assets/svg/darkArrow.svg";
import addIcon from "assets/svg/addIcon.svg";
import hideCollapse from "assets/svg/hideCollapse.svg";
import orangeCircleOne from "assets/svg/TawuniyaOrangeCircleOne.svg";
import orangeCircleTwo from "assets/svg/TawuniyaOrangeCircleTwo.svg";
import "./style.scss";
import { history } from "service/helpers";
import { useEffect } from "react";
import RightArrowIcon from "assets/images/menuicons/right-arrow.svg";

function CustomToggle({ children, eventKey, callback }) {
  const { activeEventKey } = useContext(AccordionContext);

  const decoratedOnClick = useAccordionButton(
    eventKey,
    () => callback && callback(eventKey)
  );

  const isCurrentEventKey = activeEventKey === eventKey;

  return (
    <button type="button" className="addButton" onClick={decoratedOnClick}>
      <div className="d-flex flex-row">
        <div>
          <img
            src={isCurrentEventKey ? hideCollapse : addIcon}
            className="img-fluid accordionCollapseIcon pr-3"
            alt="icon"
          />
        </div>
        <div>{children}</div>
      </div>
    </button>
  );
}

export const Faq = ({
  faqData,
  faqContainerData,
  viewAll = false,
  title,
  description,
  faqHeaderClass,
  faqParaClass,
}) => {
  const navLinks = [
    {
      id: 0,
      linkName: "/home/customerservice/medicalfraud",
    },
    {
      id: 1,
      linkName: "/home/customerservice/motorfraud",
    },
    {
      id: 2,
      linkName: "/home/customerservice/travelfraud",
    },
  ];

  const [removeLnk, setRemoveLnk] = useState(true);

  useEffect(() => {
    const routeLnk = history.location.pathname;
    if (routeLnk === "/home/customerservice/service-category") {
      setRemoveLnk(false);
    } else if (routeLnk === "/home/customerservice") {
      setRemoveLnk(false);
    } else if (routeLnk === "/home-insurance/motor") {
      setRemoveLnk(false);
    }
  }, [removeLnk]);

  return (
    <div className="row faqContainer">
      {removeLnk && (
        <div className={`${title ? null : "pt-5"} col-lg-12 col-12 `}>
          <p className={title ? faqHeaderClass : "fw-800 text-center faqTitle m-0"}>
            {title ? title : "Frequently Asked Questions"}
          </p>
          <div className={description && "d-flex justify-content-center"}>
            <div>
              <p
                className={
                  description
                    ? faqParaClass
                    : "fs-16 fw-400 text-center faqPara pb-3"
                }
              >
                {description
                  ? description
                  : "Review answers to commonly asked questions at Tawuniya, which enable you to be directly involved in improving our support experience."}
              </p>
            </div>
          </div>
        </div>
      )}
      <div className="col-lg-12 col-12">
        <div className="row">
          {faqData.map((item, index) => {
            return (
              <div className="col-lg-6 col-md-12 col-12 pb-2" key={index}>
                <div className="reportAccordionQuestion">
                  <Accordion defaultActiveKey="1">
                    <CustomToggle eventKey="0">
                      <p className="fs-18 fw-400 pt-3 accordionQuestion">
                        {item.question}
                      </p>
                    </CustomToggle>
                    <div>
                      <Accordion.Collapse eventKey="0">
                        <div className="pb-3">
                          <p className="fs-16 fw-400 p-3 reportAccordionAnswer">
                            {item.answer}
                          </p>
                        </div>
                      </Accordion.Collapse>
                    </div>
                  </Accordion>
                </div>
              </div>
            );
          })}
        </div>
      </div>
      {navLinks.map((item, index) => {
        return (
          <React.Fragment>
            {history.location.pathname === item.linkName && (
              <div className="col-lg-12 col-12 pt-5 pb-4" key={index}>
                <div className="orangeContainerLining pb-4">
                  <img
                    src={orangeCircleOne}
                    className="img-fluid orangeCircleOne"
                    alt="circle"
                  />
                  <img
                    src={orangeCircleTwo}
                    className="img-fluid orangeCircleTwo"
                    alt="circle"
                  />
                  <div className="orangeContainer">
                    <div className="row">
                      {faqContainerData.map((item, index) => {
                        return (
                          <React.Fragment>
                            <div
                              className="col-lg-9 col-md-9 col-12"
                              key={index}
                            >
                              <p className="fraudServiceTitle m-0 pb-2">
                                {item.serviceTitle}
                              </p>
                              <p className="fraudServicePara">
                                {item.servicePara}
                              </p>
                            </div>
                            <div className="col-lg-3 col-12">
                              <div className="d-flex justify-content-lg-end pb-lg-0 pb-4">
                                <div>
                                  <NormalButton
                                    label="Start Reporting"
                                    className="reportButton p-lg-4"
                                    needBtnPic={true}
                                    adjustIcon="pl-3"
                                    src={greyArrow}
                                    onClick={() =>
                                      history.push(
                                        "/home/customerservice/reportpage"
                                      )
                                    }
                                  />
                                </div>
                              </div>
                            </div>
                          </React.Fragment>
                        );
                      })}
                    </div>
                  </div>
                </div>
              </div>
            )}
          </React.Fragment>
        );
      })}
      {viewAll && (
        <div className="col-12">
          <div className="view-all-questions">
            <div>
              View All questions <img src={RightArrowIcon} alt="..." />
            </div>
          </div>
        </div>
      )}
    </div>
  );
};
