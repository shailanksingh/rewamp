import React from "react";
import "./style.scss";

export const FraudAssociates = ({
  associateData,
  associateAddedData,
  needData = false,
}) => {
  return (
    <React.Fragment>
      <div className="row fraudAssociatesContainer pt-5 mt-3 px-lg-5">
        {associateData.map((item, index) => {
          return (
            <div key={index} className={`${item.liningClass} col-lg-4 col-12`}>
              <p className={`${item.associateTitleClass} fs-16 fw-800`}>
                {item.associateTitle}
              </p>
              {item?.associatePara?.map((items, index) => {
                  return (
                    <React.Fragment>
                      {items.needList ? (
                        <p
                          className={`${items.associateParaClass} fs-14 fw-400`}
                          key={index}
                        >
                          {items.para}
                        </p>
                      ) : (
                        <ul className="p-0 m-0 pl-3">
                          <li
                            className={`${items.associateParaClass} fs-14 fw-400`}
                          >
                            {items.para}
                          </li>
                        </ul>
                      )}
                    </React.Fragment>
                  );
                })}
            </div>
          );
        })}
      </div>
      {needData && (
        <div className="row fraudAssociatesContainer pt-5 pb-3 mt-3 px-lg-5">
          {associateAddedData.map((item, index) => {
            return (
              <div
                key={index}
                className={`${item.liningClass} col-lg-4 col-12`}
              >
                <p className={`${item.associateTitleClass} fs-16 fw-800`}>
                  {item.associateTitle}
                </p>
                {item?.associatePara?.map((items, index) => {
                  return (
                    <React.Fragment>
                      {items.needList ? (
                        <p
                          className={`${items.associateParaClass} fs-14 fw-400`}
                          key={index}
                        >
                          {items.para}
                        </p>
                      ) : (
                        <ul className="p-0 m-0 pl-3">
                          <li
                            className={`${items.associateParaClass} fs-14 fw-400`}
                          >
                            {items.para}
                          </li>
                        </ul>
                      )}
                    </React.Fragment>
                  );
                })}
              </div>
            );
          })}
        </div>
      )}
    </React.Fragment>
  );
};
