import React from "react";
import { NormalButton } from "../NormalButton";
import "./style.scss";

export const CommonTable = ({ headerData, tableData }) => {
  return (
    <React.Fragment>
      <div className="d-flex flex-row commonHeaderContainer">
        {headerData.map((item, index) => {
          return (
            <div className={item.mainClass} key={index}>
              <div className="perkToolTip" id={item.showTip}>
                <div className="arrow-down"></div>
                <span className="fs-12 fw-800">Best Seller</span></div>
              <div className={item.baseClassOne}>
                <span className="fs-20 fw-800 componentTwoSubTitle">
                  {item.paraSubtitle}
                </span>
                <p className={item.paraClass}>{item.paraTitle}</p>
                {item.needExtraContent && (
                  <div>
                    <p className={item.paraSubtitleNewClass}>
                      {item.paraSubtitleNew}
                    </p>
                    <span className={item.motorClass} id={item.hideSubClass}>
                      <img
                        src={item.holdIcon}
                        className={`${item.iconSizeClass} img-fluid holdCardImg pr-2`}
                        alt={item.holdIcon}
                      />{" "}
                      {item.motorTxt}
                    </span>
                  </div>
                )}
                {item.id === 1 && (
                  <NormalButton label={item.btnLabel} className="orderNowBtn" />
                )}
              </div>
              <div className={item.baseClassTwo}>
                {item.id === 0 && (
                  <p className={item.paraClass}>{item.paraTitle}</p>
                )}
              </div>
            </div>
          );
        })}
      </div>
      <div className="d-flex flex-row commonTableContainer pb-5">
        {tableData.map((item, index) => {
          return (
            <div className={item.tableDivider} key={index}>
              {item?.newText?.map((items, index) => {
                return (
                  <React.Fragment>
                    <div className={items.tableSubClass} key={index}>
                      {items.needOnlyIcon ? (
                        <React.Fragment>
                          <img
                            src={items.icon}
                            className={`${
                              items.icon
                                ? items.iconClass
                                : "img-fluid"
                            } `}
                            alt={item.icon}
                          />
                          <span
                            className={`${
                              item.tableClass ===
                              "bestSellerContainer text-center pt-3"
                                ? "text-light"
                                : "text-dark"
                            } fs-18 fw-400 tableCellContentText`}
                          >
                            {items.tableCellContent}{" "}
                            
                          </span>
                          {/* <div className="pr-3"></div><span style={{display:'block'}} >{items.tableCellContentNew}</span> */}
                        </React.Fragment>
                      ) : (
                        <img src={items.icon} alt={item.icon} />
                      )}
                    </div>
                  </React.Fragment>
                );
              })}
            </div>
          );
        })}
      </div>
    </React.Fragment>
  );
};
