import React, { useState } from "react";
import news from "assets/images/mobile/news.png";
import "./slidecard.scss";

function SlideCard() {
	const [datas, setDatas] = useState([
		{
			id: 1,
			image: news,
			date: "10 May 2022",
			description1:
				"Tawuniya appoints Dr. Ammr Kurdi as Chief Financial Officer",
			checked: false,
		},
		{
			id: 2,
			image: news,
			date: "10 May 2022",
			description2:
				"Tawuniya is the first company in the Kingdom to install vehicle insurance policies",
			checked: false,
		},
		{
			id: 3,
			image: news,
			date: "10 May 2022",
			description3: "Tawuniya launches Covid-19 Travel Insurance program",
			checked: false,
		},
		{
			id: 4,
			image: news,
			date: "10 May 2022",
			description4:
				"Tawuniya is the first company in the Kingdom to install vehicle insurance policies",
			checked: false,
		},
	]);
	return (
		<div>
			<div className="my-4 bg-image">
				<section>
					<div class="container-fluid">
						<div class="row">
							<div class="scrollcards ">
								{datas.map((data, index) => (
									<div
										key={data.id}
										className="card overflow-hidden text-wrap "
									>
										<div className=" d-flex flex-column">
											<div>
												<div className="d-flex justify-content-space-between">
													<div className="">
														<img src={data.image} alt="someimage" />
													</div>
													<div>
														<p className="date-style">{data.date}</p>
													</div>
												</div>
											</div>
											<div>
												<div className=" fw-bold mx-1 Texas">{data.name}</div>
												<span className="data-description my-1 ">
													{data.description1}
												</span>
												<span className="data-appoints my-1">
													{data.description2}
												</span>
												<span className="data-description my-1">
													{data.description3}
												</span>
												<span className="data-appoints my-1">
													{data.description4}
												</span>
											</div>
										</div>
									</div>
								))}
							</div>
						</div>
					</div>
				</section>
			</div>
			{/* <div className='providers-color'>
      <p className='viewalne'>View All News <img className='Arrow' src={Arrow}></img></p>
    </div> */}
			<div className="bottom-vh"></div>
		</div>
	);
}

export default SlideCard;
