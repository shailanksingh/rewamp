import React from "react";
import "./slidecard.scss";

const SlideCard = ({ tagTitle, date, description, silverTheme }) => {
  return (
    <div className={`news_slider_card ${silverTheme && "silver_theme"}`}>
      <div className="header_part">
        <button>{tagTitle}</button>
        <label>{date}</label>
      </div>
      <div className="header_news_description">
        <h6>{description}</h6>
      </div>
    </div>
  );
};

export default SlideCard;
