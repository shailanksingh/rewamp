import React from "react";
import "./style.scss"

export const OtherProductsCard = ({BackgroundIcon, icon, title, subtitle, topic, topicDisc1, topicDisc2}) => {
	return (
		<div className="OtherProCard">
            <div className="OtherProIconCon">
                <img src={BackgroundIcon} alt="" className="OtherProBackIcon" />
                <img src={icon} alt="" className="OtherProIcon" />
            </div>
            <div className="OtherProText">
                <h5>{title}</h5>
                <p>{subtitle}</p>
            </div>
            <div className="OtherProBenifits">
                <h5>{topic}</h5>
                <p className="OpTopicDisc1" >{topicDisc1}</p>
                <p className="OpTopicDisc2">{topicDisc2}</p>
            </div>
        </div>
	);
};
