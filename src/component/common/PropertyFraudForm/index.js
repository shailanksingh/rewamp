import React, { useState, useCallback } from "react";
import Dropzone from "react-dropzone";
import { NormalSearch } from "../NormalSearch";
import PhoneNumberInput from "component/common/PhoneNumberInput/index";
import { NormalRadioButton } from "../NormalRadioButton";
import { NormalButton } from "../NormalButton";
import { NormalSelect } from "../NormalSelect";
import addMail from "assets/svg/Add Mail.svg";
import Profile from "assets/svg/Forms/profilenew.svg";
import Calendar from "assets/svg/Forms/calendar2new.svg";
import File from "assets/svg/Forms/file.svg"
import Calendar2 from "assets/svg/Forms/calendarnew.svg";
import Vechile from "assets/svg/Forms/vechile.svg"
import Location from "assets/svg/Forms/location.svg"



import KSAFlagImage from "assets/images/ksaFlag.png";
import IndiaFlagImage from "assets/images/indiaFlag.png";
import officeBag from "assets/svg/Office Bag.svg";
import exclamation from "assets/svg/Exclamation Mark.svg";
import upload from "assets/svg/uploadFileIcon.svg";
import selectDropDown from "assets/svg/complaintSelectDropdown.svg";
import closeIcon from "assets/svg/canvasClose.svg";
import {InputGroup,Form,Button} from "react-bootstrap"
import "./style.scss";
import { history } from "service/helpers";

import FormValidation from "service/helpers/FormValidation";
import ErrorComponent from "component/common/ErrorComponent";

const errorMessages = {
  fraudDate: { required: "Email field is required" },
  reporterName: { required: "Reporter Name field is required" },
  region: { required: "Region field is required" },
  city: { required: "City field is required" },
  responsibilityName: { required: "Fraud Incident Responsible Name field is required" },
  phoneNumber: {  
    required: "Phone Number field is required",
    phone: "Phone Number you entered is not valid",
  },
  emailId: {  
    required: "Email field is required",
    emailError: "The Email you entered is not valid",
  },
  incidentDescription: { required: "Description of the Incident field is required" },
  files: { required: "Supporting Documents field is required" },
  certify: { required: "Checkbox field is required" }
};

const PropertyFraudForm = () => {
  const [files, setFiles] = useState([]);

  const [showPreview, setShowPreview] = useState(false);

  const onDrop = useCallback((acceptedFiles) => {
    setFiles(
      acceptedFiles.map((file) =>
        Object.assign(file, {
          preview: URL.createObjectURL(file),
        })
      )
    );
    if (acceptedFiles && acceptedFiles[0]) {
      const formdata = new FormData();
      formdata.append("image", acceptedFiles[0]);
    }
    setShowPreview(true);
  });

  const images = files.map((file) => (
    <div
      className="dnd-container"
      sx={{
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        width: "100%",
        height: "100%",
      }}
      key={file.name}
    >
      <img
        style={{ width: "100px" }}
        className="dnd-img mx-auto d-block"
        src={file.preview}
        alt="Screen"
      />
    </div>
  ));

  const removeImageHandler = (e) => {
    // const arr = files;
    // arr.splice(item, 1);
    // setFiles([...arr]);
    e.stopPropagation();
    setFiles([]);
    setShowPreview(false);
  };

  const [formInput, setFormInput] = useState({
    reporterName: "",
    fraudDate: "",
    responsibleName:"",
    region: "",
    city: "",
    phoneNumber: "",
    emailId:"",
    incidentDescription: "",
    files: "",
    certify: "",
  });

  let dialingCodes = [
    {
      code: "+966",
      image: KSAFlagImage,
    },
    {
      code: "+91",
      image: IndiaFlagImage,
    },
  ];

  let [selectedCode, setSelectedCode] = useState(dialingCodes[0]);

  let [phoneNumber, setPhoneNumber] = useState("");

  //initialiize state for radio buttons
  const [radioValue, setRadioValue] = useState("Motor");
  let [filename, setFilename] = useState("File-name.pdf");


  const handleFormInputChange = (e) => {
    const { name, value } = e.target || e || {};
    setFormInput({ ...formInput, [name]: value });
  };

  const uploadfile =(e)=>{
    console.log(e.target.value)
  }

  return (
    <div className="row">
      <div className="col-12">
        <div className="d-flex justify-content-center pt-4">
          <div className="openFormContainer">
            <p className="fs-30 fw-800 text-center complaint-formTitle m-0">
            Report Property & Casualty Fraud
            </p>
            <p className="fs-16 fw-400 text-center complaint-formPara">
            Any intentional act committed by one of the parties in the insurance contract to obtain undue indemnities or benefits to them or others through deception and/or concealment of required documents and/or misrepresentation of information
            </p>
            <p className="fs-20 fw-800 personalTitle">Incident Details</p>
            <div>
              <NormalSearch
                className="formInputFieldOne"
                name="reporterName"
                value={formInput.reporterName}
                placeholder="Reporter Name"
                onChange={handleFormInputChange}
                needLeftIcon={true}
                leftIcon={Profile}
              />
            </div>
            <div className="pt-3">
              <NormalSearch
                className="formInputFieldOne"
                name="fraudDate"
                value={formInput.fraudDate}
                placeholder="Fraud Incident Date"
                onChange={handleFormInputChange}
                needLeftIcon={true}
                leftIcon={Calendar2}
              />
            </div>

            <div className="pt-3">
              <NormalSearch
                className="formInputFieldOne"
                name="responsibleName"
                value={formInput.responsibleName}
                placeholder="Fraud Incident Responsible Name"
                onChange={handleFormInputChange}
                needLeftIcon={true}
                leftIcon={Profile}
              />
            </div>
           
            <div className="d-flex flex-row">
            <div className="col-6 pt-3 p-0">
              <div className="complaintSelectContainer-travel">
                <NormalSelect
                  className="complaintFormSelectInput complaintFormSelectInput2 "
                  paddingLeft="10px"
                  placeholder="Region"
                  selectArrow={selectDropDown}
                  selectFontWeight="400"
                    phColor="#455560"
                    fontSize="16px"
                />
              </div>
            </div>

            <div className="col-6 pt-3 pr-0">
              <div className="complaintSelectContainer-travel complaintFormSelectInput2">
               
                <NormalSelect
                  className="complaintFormSelectInput"
                  paddingLeft="10px"
                  placeholder="City"
                  selectArrow={selectDropDown}
                  selectFontWeight="400"
                    phColor="#455560"
                    fontSize="16px"
                />
              </div>
            </div>
            </div>
            
            {/* --- */}

            

            
            <div className="py-4">
              <PhoneNumberInput
                className="formPhoneInput"
                selectInputClass="formSelectInputWidth"
                selectInputFlexType="formSelectFlexType-travel"
                dialingCodes={dialingCodes}
                selectedCode={selectedCode}
                setSelectedCode={setSelectedCode}
                value={phoneNumber}
                name="phoneNumber"
                onChange={({ target: { value } }) => setPhoneNumber(value)}
              />
            </div>

            <div >
              <NormalSearch
                className="formInputFieldOne"
                name="emailId"
                value={formInput.emailId}
                placeholder="ex: email@tawuniya.com.sa"
                onChange={handleFormInputChange}
                needLeftIcon={true}
                leftIcon={addMail}
              />
            </div>

          


            <div className="borderLiningTwo mt-3"></div>
            <p className="fs-20 fw-800 m-0 pt-2 pb-3 requestTitle">
            Incident Details
            </p>

            <div className="pt-3 pb-2">
              <p className="messageTitle fs-14 fw-700 m-0 pb-2">Description of the Incident</p>
              <textarea className="complaintForm-textArea">
                Type here...
              </textarea>
            </div>
            <div className="d-flex flex-row">
              <div>
                <img src={exclamation} className="img-fluid pr-2" alt="icon" />
              </div>
              <div>
                <p className="fs-12 fw-400 formConditions m-0">
                  Be detailed as possible when submitting a request in order for
                  us to help you more effectively. The more detailed the
                  information you provide, the faster we will be able to resolve
                  your issue.
                </p>
                <p className="fs-12 fw-400 formConditions m-0">
                  Below are some tips:
                </p>
              </div>
            </div>
            <ul className="fs-12 fw-400 formConditionsList pl-5 mb-3">
              <li>
                Explain step-by-step how to reproduce the scenario or the
                problem that you are describing.
              </li>
              <li>If you think a document would help, please include one.</li>
              <li>
                State when the problem started and what changes were made
                immediately beforehand.
              </li>
            </ul>
            <div className="borderLiningThree mb-3"></div>
             <p className="messageTitle fs-14 fw-700 m-0 pb-2">Supporting Documents</p>


         
               
            <Dropzone onDrop={onDrop}>
              {({ getRootProps, getInputProps }) => (
                <div
                  {...getRootProps({
                    className: "dropzone",
                    onDrop: (event) => event.preventDefault(),
                  })}
                >
                  <div
                    style={{
                      display: "flex",
                      justifyContent: "center",
                      alignItems: "center",
                      flexDirection: "column",
                    }}
                    className="image-upload-wrap"
                  >
                    <input {...getInputProps()} />
                    {!showPreview ? (
                      <div className="drag-text">
                        <div>
                          <img
                            src={upload}
                            className="img-fluid mx-auto d-block pb-3"
                            alt="uploadicon"
                          />
                          <div className="d-flex justify-content-center">
                            <div>
                              <p className="uploadTitle fs-14 fw-400 m-0 pb-3">
                                Upload files by drag and drop or{" "}
                                <span className="uploadLink">
                                  click to upload
                                </span>
                                .
                              </p>
                            </div>
                          </div>
                          <p className="fs-12 fw-400 allowedFormats text-center">
                            Allowed files: JPEG, PNG, DOC, DOCX, PDF Maximum
                            Size: 10MB
                          </p>
                        </div>
                      </div>
                    ) : (
                      <>
                        <div className="drag-text">
                          <div>
                            <div>{images}</div>
                            {showPreview ? (
                              <img
                                src={closeIcon}
                                className="img-fluid removeImgBtn"
                                onClick={(e) => removeImageHandler(e)}
                                alt="icon"
                              />
                            ) : (
                              ""
                            )}
                            <div className="d-flex justify-content-center">
                              <div>
                                <p className="uploadTitle fs-14 fw-400 m-0 pb-3">
                                  Upload files by drag and drop or{" "}
                                  <span className="uploadLink">
                                    click to upload
                                  </span>
                                  .
                                </p>
                              </div>
                            </div>
                            <p className="fs-12 fw-400 allowedFormats text-center">
                              Allowed files: JPEG, PNG, DOC, DOCX, PDF Maximum
                              Size: 10MB
                            </p>
                          </div>
                        </div>
                      </>
                    )}
                  </div>
                </div>
              )}
            </Dropzone>

            <div className="d-flex flex-row pt-3">
              <div>
                <input type="checkbox" className="pr-2 pt-2"/>
              </div>
              <div>
                <p className="fs-12 pt-1 fw-400 formConditions pl-2 m-0">
                I hereby certify that all information mentioned above about the fraud / misuse of property and casualty insurance is correct to the best of my knowledge and belief.
                </p>
                </div>
               
                </div>

            <div className="borderLiningFour my-4"></div>

            <NormalButton
              label="Submit Report"
              className="complaintFormSubmitBtn p-4"
              onClick={() =>history.push("/home/customerservice")}
            />

          </div>
        </div>
      </div>
    </div>
  );
};

export default PropertyFraudForm;
