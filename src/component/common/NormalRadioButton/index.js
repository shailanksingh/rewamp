import React from "react";
import "./style.scss";

export const NormalRadioButton = ({
  isStyledRadio,
  radioContainer,
  type,
  name,
  value,
  onChange,
  radioValue,
  checked,
  isNormalRadioLayer,
  normalRadioLabelClass,
}) => {
  return (
    <React.Fragment>
      {isStyledRadio ? (
        <div
          className={`${radioContainer} d-flex justify-content-between px-3`}
        >
          <div>
            <span className="fs-16 fw-500 radioLabel">{radioValue}</span>
          </div>
          <div className="radioAlign">
            <input
              type={type}
              name={name}
              value={value}
              onChange={onChange}
              checked={checked}
            />
          </div>
        </div>
      ) : (
        <div className="d-flex flex-row">
          <div className="pt-1">
            <input
              type={type}
              name={name}
              value={value}
              onChange={onChange}
              checked={checked}
            />
          </div>
          {isNormalRadioLayer ? (
            <div className="pl-2">
              <span className={normalRadioLabelClass}>{radioValue}</span>
            </div>
          ) : (
            <div className="pl-3">
              <span className="fs-16 fw-500 radioLabel">{radioValue}</span>
            </div>
          )}
        </div>
      )}
    </React.Fragment>
  );
};
