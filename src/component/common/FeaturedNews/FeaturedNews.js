import React from "react";
import Vector from "assets/news/Vector.svg";
import verticalline from "assets/news/verticalline.png";
import News from "assets/news/News.png";
import image from "assets/news/i1.png"
import image1 from 'assets/news/i2.png'
import image2 from 'assets/news/i3.png'
import SlideCard from "component/common/SliderCard/SlideCard";
import {
  MediaCenterLatestNews,
} from "component/common/MockData";
import "./style.scss";

function Tawuniya() {
  const datas = [
    {
      id: 1,
      description: "7th November 2021",
      image: image
    },

    {
      id: 2,
      description: "7th November 2021",
      image: image1

    },
    {
      id: 3,
      description: "7th November 2021",
      image: image2

    },
  ];

  return (
    <div>
      <div className="media-center-feature"> Featured News</div>
      <div className="container-fluid">
        <div className="row">
          <div className="our-feature">
            {datas.map((data) => (
              <div key={data.id} className="our-feature-image" style={{backgroundImage: `url(${data.image})`}}>
                <div className="media-center-news"><img alt="..." src={News}></img></div>
                <div className="standard"><p>Standard & Poor's assigns Tawuniya an<br/> "A-" rating; outlook "stable"
                </p></div>
                <div className="d-flex media-center-body">
                  <div className="meadia-center-november">{data.description}</div>
                  <img alt="..." className="mx-2" src={verticalline}/>
                  <div className=" read">Read the full story</div>
                  <img alt="..." className="mx-2" src={Vector}/>
                </div>
              </div>
            ))}
          </div>
        </div>
      </div>
      <div className="d-flex">
        <div className="media-center-view">VIEW ALL NEWS <img alt="..." className="mx-3" src={Vector}/></div>
      </div>

      <div className="latest">Latest News</div>
      <div className="media-center-latest">
        {MediaCenterLatestNews?.map(
          ({tagTitle, date, description}) => {
            return (
              <SlideCard
                tagTitle={tagTitle}
                date={date}
                description={description}
              />
            );
          }
        )}
      </div>
      <div className="d-flex">
        <div className="media-center-view">VIEW ALL NEWS <img alt="..." className="mx-3" src={Vector}/></div>
      </div>
      <div className="latest-bottom"></div>
    </div>
  );
}

export default Tawuniya;
