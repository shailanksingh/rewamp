import React, {useEffect, useState} from "react";
import { history } from "service/helpers";
import { footerData, footerNextData } from "../MockData";
import { FooterCard } from "component/HomePage/LandingComponent/NavbarCards";
import { NormalButton } from "../NormalButton";
import tweet from "assets/svg/TwitterLight.svg";
import fb from "assets/svg/FacebookLight.svg";
import uTube from "assets/svg/YoutubeLight.svg";
import linkedIn from "assets/svg/LinkedinLight.svg";
import insta from "assets/svg/InstagramLight.svg";
import whatzApp from "assets/svg/WhatsappLight.svg";
import tawniyaLogo from "assets/svg/tawuniyaLogo.svg";
import appStore from "assets/images/appleStoreFooter.png";
import playStore from "assets/images/playStoreFooter.png";
import appGal from "assets/images/appGalleryFooter.png";
import backTop from "assets/svg/backTop.svg";
import union from "assets/svg/tawuniyaUnionGreyCircle.svg";
import "./style.scss";
import {useTranslation} from "react-i18next";
import {useSelector} from "react-redux";

export const CommonFooter = () => {
	const {t} = useTranslation();
	const currentLanguage = useSelector((data) => data.languageReducer.language);

	const iconData = [tweet, fb, uTube, linkedIn, insta, whatzApp];
	const appData = [appStore, playStore, appGal];
	const [animatedText, setAnimatedText] = useState(null);

	const handleBackTop = () => {
		window.scrollTo(0, 0);
	};

	const links = [
		{
			id: 0,
			link: "/home",
		},
		{
			id: 1,
			link: "/customerservice",
		},
		{
			id: 2,
			link: "/home/customerservice/opencomplaint",
		},
		{
			id: 3,
			link: "/home/customerservice/medicalfraud",
		},
		{
			id: 4,
			link: "/home/customerservice/motorfraud",
		},
		{
			id: 5,
			link: "/home/customerservice/travelfraud",
		},
		{
			id: 6,
			link: "/home/customerservice/surplus",
		},
		{
			id: 7,
			link: "/home/customerservice/servicequestions",
		},
		{
			id: 8,
			link: "/home/customerservice/reportpage",
		},
		{
			id: 9,
			link: "/home/customerservice/violationspage",
		},
		{
			id: 10,
			link: "/home/customerservice/SupportRequestConfirmation",
		},
		{
			id: 11,
			link: "/home/social",
		},
		{
			id: 12,
			link: "/home/health-insurance",
		},
		{
			id: 13,
			link: "/home-insurance/motor",
		},
		{
			id: 14,
			link: "/home/programs/hajjinsuranceprogram",
		},
		{
			id: 15,
			link: "/home/programs/UmrahInsuranceProgram",
		},
		{
			id: 16,
			link: "/home/aboutus/financialhighlights",
		},
		{
			id: 17,
			link: "/home/aboutus/directors",
		},
		{
			id: 18,
			link: "/home/aboutus",
		},
		{
			id: 19,
			link: "/home/contactus",
		},
		{
			id: 20,
			link: "/home/mediacenter",
		},
		{
			id: 21,
			link: "/home/mediacenter/medianewsdetails",
		},
		{
			id: 22,
			link: "/home/mediacenter/medianewscategory",
		},
		{
			id: 23,
			link: "/home/social/project",
		},
		{
			id: 24,
			link: "/home/customerservice/service-category",
		},
		{
			id: 25,
			link: "/home/customerservice",
		},
		{
			id: 26,
			link: "/home/insuranceagent",
		},
		{
			id: 27,
			link: "/products/individuals/homeinsurance",
		},
		{
			id: 28,
			link: "/products/individuals/internationaltravel",
		},
		{
			id: 29,
			link: "/products/individuals/medicalmalpractice",
		},
	];

	let texts = [
		t('footerNews.animatedText1'),
		t('footerNews.animatedText2'),
		t('footerNews.animatedText3'),
	]

	useEffect(() => {
		setAnimatedText(texts[0])
	}, [])

	useEffect(() => {
		setTimeout(() => {
			const current = texts.findIndex(item => animatedText === item);
			if (current !== -1 && current !== texts.length - 1) {
				setAnimatedText(texts[current + 1]);
			} else {
				setAnimatedText(texts[0]);
			}
		}, 2000)
	}, [animatedText])

	return (
		<>
			<div className="row footerLayout" id="footerDesktop">
				{links.map((item) => {
					return (
						<>
							{history.location.pathname === item.link && (
								<React.Fragment>
									<div className="col-12 commonFooterContainer">
										<div className="d-flex justify-content-start pt-5 pb-3">
											<div>
												<p className="tawuniyaTitle m-0">
													{t('footerNews.title')}{" "}
													<span
														className="tawuniyaSubTitle"
														id="animatedFooterText"
													>{animatedText}</span>
												</p>
												<p className="tawuniyaPara mr-1">
													{t('footerNews.paragraph')}
												</p>
											</div>
										</div>
									</div>
									<div className="cardFooterPosition">
										<div className="col-12 commonCardFooterContainer">
											<FooterCard />
										</div>
									</div>
								</React.Fragment>
							)}
						</>
					);
				})}

				<div className="col-12 curvedFooterContainer">
					<img src={union} className="img-fluid unionCircle" alt="union" />
					<div className="row">
						<div className="col-lg-9 col-12">
							<div className="d-flex flex-row">
								{footerData.map((item, index) => {
									return (
										<div
											key={index}
											className="pr-lg-5  pr-md-5 footerBoxContainer"
										>
											<p className="fs-16 fw-800 m-0 footerHeading text-uppercase pb-3">
												{currentLanguage === 'arabic' ? item.titleAr : item.title}
											</p>
											{item?.content?.map((items) => {
												return (
													<p
														className="fs-16 fw-400 footerContentText m-0 pb-2"
														onClick={() => items?.routeURL && history.push(items?.routeURL)}>
														<img
															src={item.footerIcon}
															className="img-fluid pr-lg-2 pr-md-2"
															alt="icon"
														/>
														{currentLanguage === 'arabic' ? items?.labelAr : items.label}
													</p>
												);
											})}
										</div>
									);
								})}
							</div>
							<div className="d-flex flex-row">
								{footerNextData.map((item, index) => {
									return (
										<div
											key={index}
											className="pr-lg-5 pr-md-5 footerNextHeadingContainer footerBoxContainer"
											id={item.id === 2 && "alignFooterContent"}
										>
											<p className="fs-16 fw-800 m-0 footerNextHeading text-uppercase pb-3">
												{currentLanguage === 'arabic' ? item.titleAr : item.title}
											</p>
											{item?.content?.map((items) => {
												return (
													<p
														className="fs-16 fw-400 footerNextContentText m-0 pb-2"
														onClick={() => items?.routeURL && history.push(items?.routeURL)}>
														<img
															src={item.footerIcon}
															className="img-fluid pr-lg-2 pr-md-2"
															alt="icon"
														/>
														{currentLanguage === 'arabic' ? items?.labelAr : items.label}
													</p>
												);
											})}
										</div>
									);
								})}
							</div>
						</div>
						<div className="col-lg-3 col-12">
							<div className="d-flex justify-content-end">
								<div className="footerIconContainer">
									<p className="followUsText fs-16 fw-700 text-light text-uppercase m-0 pb-2">
										{t('footerNews.follow')}
									</p>
									<div className="d-flex flex-row">
										{iconData.map((item) => {
											return (
												<div>
													<img
														src={item}
														className="img-fluid footerSocialIcon pr-lg-2 pr-md-2"
														alt="socialicons"
													/>
												</div>
											);
										})}
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div className="col-12 footerLastContainer">
					<div className="row footerContentBox">
						<div className="footerButtonPositon">
							<div className="col-12 footerButtonContainer">
								<NormalButton
									label={t('footer.backToTop')}
									className="backTopButton mx-auto d-block"
									onClick={handleBackTop}
									needBtnPic={true}
									src={backTop}
									adjustIcon="pl-lg-2"
								/>
							</div>
						</div>
						<div className="col-lg-8 col-12 footerBottomLeftSpacing">
							<p className="copyrightText fs-14 fw-400">
								<img
									src={tawniyaLogo}
									className="img-fluid footerLogo pr-lg-3 pr-md-3"
									alt="logo"
								/>
								{t('footer.copyright')}
							</p>
						</div>
						<div className="col-lg-4 col-12 p-md-0 footerBottomRightSpacing">
							<div className="footerPositionIcons">
								<div className="d-flex flex-lg-row flex-md-row flex-column pb-lg-0 pb-3">
									{appData.map((i) => {
										return (
											<div>
												<img
													src={i}
													className="img-fluid pr-lg-3 pr-md-3"
													alt="socialapp"
												/>
											</div>
										);
									})}
								</div>
							</div>
						</div>
					</div>
					<div className="w-100 footerLining"></div>
					<div className="row pt-3">
						<div className="col-lg-5 col-12">
							<div className="d-flex flex-row">
								<div>
									<p className="fs-12 fw-400 footerInlineText">
										{t('footer.termsAndConditions')}
									</p>
								</div>
								<div className="pl-lg-3 pl-3">
									<p className="fs-12 fw-400 footerInlineText">
										{t('footer.privacyPolicy')}
									</p>
								</div>
								<div className="pl-lg-3 pl-3">
									<p className="fs-12 fw-400 footerInlineText">{t('footer.cookiePolicy')}</p>
								</div>
							</div>
						</div>
						<div className="col-lg-7 col-12 pl-0">
							<div className="d-flex justify-content-lg-end justify-content-md-start">
								<div className="d-flex flex-lg-row">
									<div>
										<p className="fs-12 fw-400 footerInlineText">
											{t('footer.link1')}
										</p>
									</div>
									<div className="pl-lg-3 pl-3">
										<p className="fs-12 fw-400 footerInlineText">
											{t('footer.link2')}
										</p>
									</div>
									<div className="pl-lg-3 pl-3">
										<p className="fs-12 fw-400 footerInlineText">
											{t('footer.link3')}
										</p>
									</div>
									<div className="pl-lg-3 pl-3">
										<p className="fs-12 fw-400 footerInlineText">
											{t('footer.link4')}
										</p>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</>
	);
};
