import React from "react";
import TextField from "@material-ui/core/TextField";
import { makeStyles } from "@material-ui/core/styles";
import ErrorComponent from "component/common/ErrorComponent";
import "./NormalInput.scss";
/**
 * Input: The Common Re-usable input across website.
 * @return {JSX.Element} The JSX Code for Input
 */

const useStyles = makeStyles((theme) => ({
  textField: {
    border: "none",
    position: "relative",
    left: "0px",
  },
}));

export const NormalInput = ({
  placeholder = "",
  subClassName = "",
  label = "",
  onChange,
  value = "",
  name = "",
  disabled = false,
  type = "text",
  icon,
  errorMessage = "",
  maxLength,
  variant,
  textField,
  className,
}) => {
  const classes = useStyles();

  return (
    <>
      <div className={`normal-input ${subClassName ? subClassName : ""}`}>
        {label !== "" ? (
          <label className="font-weight-normal mb-1">{label}</label>
        ) : null}
        <div className="d-block">
          <TextField
            className={textField}
            InputProps={{
              classes: { notchedOutline: classes.textField },
            }}
            variant={variant}
            name={name}
            type={type}
            disabled={disabled}
            value={value}
            placeholder={placeholder}
            disableUnderline={false}
            maxLength={maxLength}
            onChange={(e) => {
              onChange({
                target: {
                  name: e.target.name,
                  value: e.target.value,
                },
              });
            }}
          />
          {icon ? <span className="icon">{icon}</span> : null}
        </div>
      </div>
      {errorMessage && errorMessage !== "" && (
        <ErrorComponent message={errorMessage} />
      )}
    </>
  );
};
