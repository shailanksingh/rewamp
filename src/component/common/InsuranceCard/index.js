import React from "react";
import insuranceArrow from "assets/svg/insuranceArrow.svg";
import "./style.scss";

export const InsuranceCard = ({ heathInsureCardData, textWidth }) => {
  return (
    <div className="row">
      {heathInsureCardData.map((item, index) => {
        return (
          <div className={`${item.class} col-lg-3 col-md-3`} key={index}>
            <div className="insuranceBannerCard">
              <img
                src={item.cardIcon}
                className="img-fluid"
                id={item.contentAlign}
                alt="icon"
              />
              <p
                className={`InsuranceCardText fs-16 fw-800  pt-2`}
                id={textWidth}
              >
                {item.content}
              </p>
              <img src={insuranceArrow} className="img-fluid pb-3" alt="icon" />
            </div>
          </div>
        );
      })}
    </div>
  );
};
