import React, { useState } from "react";
import { Button } from "@material-ui/core";
import "./style.scss";
import ChevronRightIcon from "@material-ui/icons/ChevronRight";
import plusIcon from "../../../assets/svg/plusIcon.svg";
import minusIcon from "../../../assets/svg/minusIcon.svg";
function FrequentlyAsked({ FAQ }) {
	const [showAnswer, setShowAnswer] = useState(0);

	return (
		<div className="FAQContainer FAQ">
			<div className="FAQText">
				<h5>Frequently Asked Questions</h5>
				<span>We provide the best and trusted service for our customers</span>
			</div>
			<div className="FAQBox">
				{FAQ.map((item, index) => {
					const { question, answer, id } = item;
					return (
						<div className="FAQBoxHead">
							<div className="FAQBoxInner" key={id}>
								<img
									className={item.id === showAnswer ? "iconMinus" : "iconPlus"}
									onClick={() => {
										setShowAnswer(item.id);
									}}
									src={item.id === showAnswer ? minusIcon : plusIcon}
									alt="plus icon"
								/>
								<p>{question}</p>
							</div>
							{item.id === showAnswer && (
								<div className="FAQBoxAns">{answer}</div>
							)}
						</div>
					);
				})}
			</div>
			<div className="FAQBoxBtn">
				<Button
					size="medium"
					color="primary"
					variant="outlined"
					endIcon={<ChevronRightIcon />}
				>
					View All FAQ
				</Button>
			</div>
		</div>
	);
}

export default FrequentlyAsked;
