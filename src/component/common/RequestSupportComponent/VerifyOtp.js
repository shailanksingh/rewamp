import React, { useState, useEffect } from "react";
import Modal from "react-bootstrap/Modal";
import PinInput from "react-pin-input";
import { NormalButton } from "../NormalButton";
import closeIcon from "assets/svg/canvasClose.svg";
import "./style.scss";

const VerifyOtp = ({ show, handleClose, toggleResult }) => {
  const [timer, setTimer] = useState(60);

  const [newTimer, setNewTimer] = useState(60);

  const [addNo, setAddNo] = useState(false);

  const [addNewNo, setAddNewNo] = useState(false);

  const [resend, setResend] = useState(false);

  const resendCode = () => {
    setResend(true);
    setNewTimer(60);
  };

  useEffect(() => {
    const time = setTimeout(() => {
      let timerLength = timer - 1;
      setTimer(timerLength);
      if (timerLength === 9) {
        setAddNo(true);
      }
      if (timerLength <= 0) {
        setTimer(0);
      }
      if (resend) {
        setNewTimer(newTimer - 1);
        if (newTimer - 1 === 9) {
          setAddNewNo(true);
        }
        if (newTimer - 1 <= 0) {
          setNewTimer(0);
        }
      }
    }, 1000);
    return () => clearTimeout(time);
  }, [timer, newTimer, addNo, resend]);

  return (
    <Modal
      show={show}
      onHide={handleClose}
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <div className="row">
        <div className="col-lg-12 col-12 complaintModal-authVerifyContainer px-5">
          <div className="complaintModal-authVerifyBox ">
            <img
              src={closeIcon}
              className="complaintModalCloseBtn img-fluid"
              onClick={handleClose}
              alt="closeIcon"
            />
            <p className="fs-30 fw-800 complaintModal-verifyLoginHeader text-center pt-5">
              Verification
            </p>
            <div className="d-flex justify-content-center">
              <p className="complaintModal-displayMessageText fs-14 fw-400 line-height-22 text-center">
                An SMS will be sent to the following mobile number you have
                registered with us:{" "}
                <span className="fs-14 fw-600 complaintModal-NumberText">
                  xxxxxxxx7313
                </span>
              </p>
            </div>
            <p className="fs-20 fw-800 complaintModal-verifyTitlePara text-center m-0 pb-2">
              Verification Code
            </p>
            <div>
              <PinInput
                length={4}
                initialValue=""
                onChange={(value, index) => {
                  console.log(value);
                }}
                inputMode="numeric"
                secret
                style={{
                  padding: "10px",
                  display: "flex",
                  justifyContent: "center",
                }}
                inputStyle={{
                  border: "none",
                  backgroundColor: "#F2F3F5",
                  borderRadius: "4px",
                  width: "100%",
                  marginRight: "2%",
                  minHeight: "90px",
                }}
                autoSelect={true}
                regexCriteria={/^[ A-Za-z0-9_@./#&+-]*$/}
              />
            </div>
            <div className="pt-3 px-2 mx-1">
              <NormalButton
                label="Verify"
                className="complaintModal-verifyLoginBtn p-4"
                onClick={toggleResult}
              />
            </div>
            <p className="fs-14 fw-400 complaintModal-resendText text-center pt-3 pb-1">
              Resend code in 00:{" "}
              {resend ? (
                <span>
                  {addNewNo && 0}
                  {newTimer}
                </span>
              ) : (
                <span>
                  {addNo && 0}
                  {timer}
                </span>
              )}
            </p>
            <p className="fs-14 fw-400 complaintModal-noVerifyCodeMsg text-center pb-5">
              Don’t receive Code?{" "}
              <span
                className="complaintModal-resendAgainLink"
                onClick={resendCode}
              >
                Resend
              </span>
            </p>
          </div>
        </div>
      </div>
    </Modal>
  );
};

export default VerifyOtp;
