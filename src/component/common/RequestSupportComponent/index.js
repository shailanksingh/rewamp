import React, { useState } from "react";
import { history } from "service/helpers";
import VerifyOtp from "./VerifyOtp";
import { ComplaintTabs } from "../ComplaintTabs";
import { ServiceCard } from "component/common/ServiceCard";
import { serviceData } from "component/common/MockData/index";
import OpenComplaintForm from "../OpenComplaintForm";
import HelpCenterCard from "./HelpCenterCard";
import { NormalInput } from "../NormalInput";
import { NormalButton } from "../NormalButton";
import PhoneNumberInput from "component/common/PhoneNumberInput/index";
import { resultData } from "component/common/MockData/index";
import greenClock from "assets/svg/greenClock.svg";
import orangeArrow from "assets/svg/orangeArrow.svg";
import KSAFlagImage from "assets/images/ksaFlag.png";
import IndiaFlagImage from "assets/images/indiaFlag.png";
import "./style.scss";

export const RequestSupportComponent = () => {
  const [show, setShow] = useState(false);

  const handleClose = () => setShow(false);

  const handleShow = () => setShow(true);

  const [inputValue, setInputValue] = useState("");

  const [toggleSearch, setToggleSearch] = useState(false);

  let dialingCodes = [
    {
      code: "+91",
      image: IndiaFlagImage,
    },
    {
      code: "+966",
      image: KSAFlagImage,
    },
  ];

  let [selectedCode, setSelectedCode] = useState(dialingCodes[0]);

  let [phoneNumber, setPhoneNumber] = useState("");

  const [file, setFile] = useState(false);

  const fileComplaintToggler = (routeURL) => {
    if (routeURL) {
      history.push(routeURL);
    } else {
      setFile(true);
    }
  };

  const toggleResult = () => {
    setToggleSearch(true);
    setShow(false);
  };

  const [pillIndex, setPillIndex] = useState(null);

  return (
    <React.Fragment>
      <VerifyOtp
        show={show}
        handleClose={handleClose}
        toggleResult={toggleResult}
      />
      <div className="commomQuestionBox">
        <p className="supportHeading fw-800 text-center m-0 pb-1">
          Can't find what you're looking for?
        </p>
        <p className="supportPara fw-400 fs-14 text-center line-height-25">
          Get help with common questions or reach out to our support team.
        </p>
        {!toggleSearch && (
          <div className="row differentSupportLayout pb-2">
            <div className="col-lg-3 col-12 pb-lg-0 pb-md-3 pb-3">
              <HelpCenterCard
                helpCenterTitle="Contact Our Support Team"
                helpCenterTitleClass="helpCenterTitle fs-20"
                timeData="We're here 24 hours a day, 7 days a week, to help you and provide full support"
                label="Open a Complaint"
                className="helpCenterBtnSuccess"
                clockIcon={greenClock}
                timeTextColour="timeTextColourSuccess"
                timeTextClass="successTextAlign fs-16"
                fileComplaintToggler={fileComplaintToggler}
                heightClass="h-100"
              />
            </div>
            <div className="col-lg-3 col-12 pb-lg-0 pb-md-3 pb-3 pl-lg-0">
              <HelpCenterCard
                helpCenterTitle="You don't need to wait anymore!"
                helpCenterTitleClass="helpCenterTitle fs-20"
                timeData="We don't want to keep you waiting for someone to answer the phone"
                label="Start Live Chat"
                className="helpCenterBtnDanger"
                statusText="Online"
                statusClass="onlineContainer"
                timeTextColour="timeTextColourDanger"
                timeTextClass="m-0 fs-16"
                fileComplaintToggler={fileComplaintToggler}
                routeURL={"/home/customerservice"}
              />
            </div>
            <div className="col-lg-6 col-12 pb-lg-0 pb-md-3 pb-3 pl-lg-0">
              <div className="supportRequestBox">
                <div className="supportRequestBoxLining">
                  <p className="supportRequestHeader fs-18 fw-800 m-0">
                    Find your Support Request
                  </p>
                  <p className="supportRequestPara fs-14 fw-400 m-0">
                    It looks like you are searching for a previous conversation
                    with Tawuniya Support.
                  </p>
                </div>
                <div className="row">
                  <div className="col-lg-5 col-12 pt-4 pb-2">
                    <NormalInput
                      textField="requestFieldOne"
                      value={inputValue}
                      variant="outlined"
                      placeholder="Saudi ID / IQAMA"
                      onChange={(e) => setInputValue(e.target.value)}
                    />
                  </div>
                  <div className="col-lg-5 col-12 pt-4 pb-2 pl-lg-0">
                    <PhoneNumberInput
                      className="complaintPhoneInput"
                      selectInputClass="complaintSelectInputWidth"
                      selectInputFlexType="complaintSelectFlexType"
                      dialingCodes={dialingCodes}
                      selectedCode={selectedCode}
                      setSelectedCode={setSelectedCode}
                      value={phoneNumber}
                      name="phoneNumber"
                      onChange={({ target: { value } }) =>
                        setPhoneNumber(value)
                      }
                    />
                  </div>
                  <div className="col-lg-2 col-12 pt-4 pb-2 pl-lg-0">
                    <NormalButton
                      label="Search"
                      className="complaintSearchBtn"
                      onClick={handleShow}
                    />
                  </div>
                </div>
              </div>
            </div>
          </div>
        )}
        {file && <OpenComplaintForm />}
        <React.Fragment>
          {toggleSearch ? (
            <div className="row searchContainerBox">
              <div className="col-12">
                <div className="row pt-5">
                  <div className="col-lg-9 col-md-9 col-12 mx-auto d-block">
                    <p className="searchTitle fw-800 text-center m-0">
                      File a Complaint Request
                    </p>
                    <p className="searchResultTitle fs-18 fw-400 text-center">
                      SHOWING 1–30 OF 58 RESULTS cc
                    </p>
                    <div className="row">
                      {resultData.map((item, index) => {
                        return (
                          <div
                            className={`${
                              item.pillNo === pillIndex ? "col-6" : "col-12"
                            } pb-2`}
                            key={index}
                          >
                            <div className="resultBar">
                              <div
                                className="d-flex justify-content-between accordionToggler p-3"
                                onClick={() =>
                                  pillIndex === null
                                    ? setPillIndex(item.pillNo)
                                    : setPillIndex(null)
                                }
                              >
                                <div className="pl-3">
                                  <p className="m-0 fs-16 fw-700">
                                    {item.code}
                                  </p>
                                  <p className="fs-14 fw-400 m-0">
                                    {item.date}
                                  </p>
                                </div>
                                <div className="pr-3">
                                  <div
                                    className={`${item.statusClass} p-1 px-3`}
                                  >
                                    <span className="fw-400 fs-14">
                                      {item.status}
                                    </span>
                                  </div>
                                </div>
                              </div>
                              {item.pillNo === pillIndex && (
                                <div className="detailReportContainer">
                                  <p className="fs-24 fw-800 complaintDetailTitle pt-4 pb-2">
                                    Complaint Details
                                  </p>
                                  {item?.detailsData?.map((items, index) => {
                                    return (
                                      <div
                                        className="d-flex justify-content-between detailInfoContainer"
                                        key={index}
                                      >
                                        <div>
                                          <p className="leftComplaintText fs-14 fw-400 m-0">
                                            {items.complaintTitle}
                                          </p>
                                        </div>
                                        <div>
                                          <p className="rightComplaintText fs-14 fw-400 m-0">
                                            {items.complaintText}
                                          </p>
                                        </div>
                                      </div>
                                    );
                                  })}
                                </div>
                              )}
                            </div>
                          </div>
                        );
                      })}
                    </div>
                  </div>
                </div>
              </div>
            </div>
          ) : null}
        </React.Fragment>
        <React.Fragment>
          {toggleSearch && (
            <div className="row differentNewSupportLayout py-5">
              <div className="col-5 mx-auto d-block">
                <div className="row">
                  <div className="col-lg-6 col-12 px-2">
                    <HelpCenterCard
                      helpCenterTitle="Contact Our Support Team"
                      helpCenterTitleClass="helpNewCenterTitleOne fs-14"
                      timeData="We're here 24 hours a day, 7 days a week, to help you and provide full support"
                      label="Open a Complaint"
                      className="helpCenterNewBtnSuccess"
                      clockIcon={greenClock}
                      timeTextColour="timeNewTextColourSuccess"
                      timeTextClass="successNewTextAlign"
                      fileComplaintToggler={fileComplaintToggler}
                    />
                  </div>
                  <div className="col-lg-6 col-12 px-2">
                    <HelpCenterCard
                      helpCenterTitle="You don't need to wait anymore!"
                      helpCenterTitleClass="helpNewCenterTitleTwo fs-14"
                      timeData="We don't want to keep you waiting for someone to answer the phone"
                      label="Start Live Chat"
                      className="helpCenterNewBtnDanger"
                      statusText="Online"
                      statusClass="onlineContainer"
                      timeTextColour="timeNewTextColourDanger"
                      timeTextClass="m-0"
                      fileComplaintToggler={fileComplaintToggler}
                      routeURL={"/home/customerservice"}
                    />
                  </div>
                </div>
              </div>
            </div>
          )}
        </React.Fragment>
        <div className="row productInfoContainer">
          <div className="col-12 pt-4 mt-1">
            <p className="productHeader fw-800 text-center m-0">
              Products & Services Support
            </p>
            <div className="d-flex justify-content-center pb-2">
              <div>
                <p className="productInfoText fs-17 fw-400 text-center line-height-25 m-0">
                  Here you will find the answers to many of the questions you
                  may have,
                </p>
                <p className="productInfoText fs-17 fw-400 text-center line-height-25">
                  including information about our comprehensive range of
                  insurance products and services.
                </p>
              </div>
            </div>
            <ComplaintTabs
              compOne={
                <ServiceCard
                  serviceData={serviceData}
                  columnClass="col-lg-2 col-12"
                  routeURL="/home/customerservice/medicalfraud"
                />
              }
            />
            <p className="viewProducts-ComplaintLink text-center fs-16 fw-800">
              View All Products & Services
              <img
                src={orangeArrow}
                className="img-fluid orangeArrowIcon pl-3"
                alt="arrow"
              />
            </p>
          </div>
        </div>
      </div>
    </React.Fragment>
  );
};
