import React from "react";
import { NormalButton } from "../NormalButton";
import buttonArrow from "assets/svg/buttonLightArrow.svg";
import "./style.scss";

const HelpCenterCard = ({
  helpCenterTitle,
  timeData,
  timeTextColour,
  timeTextClass,
  label,
  className,
  clockIcon,
  statusText,
  statusClass,
  fileComplaintToggler,
  routeURL,
  heightClass,
  helpCenterTitleClass
}) => {
  return (
    <div className={`${heightClass} helpCenterBox`}>
      <div className="helpboxlining">
        <div className="d-flex justify-content-between">
          <div>
            <p className={`${helpCenterTitleClass} fw-800 m-0 pb-1`}>
              {helpCenterTitle}
            </p>
          </div>
          <div>
            {/* <img src={clockIcon} className="img-fluid" alt="clockicon" /> */}
            <span className={statusClass}>{statusText}</span>
          </div>
        </div>
        <p id={timeTextColour} className={`${timeTextClass}`}>
          {timeData}
        </p>
      </div>
      <div className="pt-4 pb-1">
        <NormalButton
          label={label}
          className={className}
          onClick={() => fileComplaintToggler(routeURL)}
          needBtnPic={true}
          src={buttonArrow}
          adjustIcon="pl-3"
        />
      </div>
    </div>
  );
};

export default HelpCenterCard;
