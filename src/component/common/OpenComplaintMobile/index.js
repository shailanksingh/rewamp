import React, { useState, useCallback } from "react";
import { history } from "service/helpers";
import Dropzone from "react-dropzone";
import { NormalSearch } from "../NormalSearch";
import PhoneNumberInput from "component/common/PhoneNumberInput/index";
import { NormalRadioButton } from "../NormalRadioButton";
import { NormalButton } from "../NormalButton";
import { NormalSelect } from "../NormalSelect";
import addMail from "assets/svg/Add Mail.svg";
import KSAFlagImage from "assets/images/ksaFlag.png";
import IndiaFlagImage from "assets/images/indiaFlag.png";
import officeBag from "assets/svg/Office Bag.svg";
import exclamation from "assets/svg/Exclamation Mark.svg";
import upload from "assets/svg/uploadFileIcon.svg";
import profile from "assets/svg/userProfileIcon.svg";
import selectDropDown from "assets/svg/complaintSelectDropdown.svg";
import closeIcon from "assets/svg/canvasClose.svg";
import uploader from "assets/svg/browseFileUploader.svg";
import "./style.scss";
import { NormalCheckbox } from "../NormalCheckBox";
import { Data } from "@react-google-maps/api";

const OpenComplaintMobile = ({ needForm = false }) => {
	const [files, setFiles] = useState([]);

	const [check, setCheck] = useState([]);

	const [showPreview, setShowPreview] = useState(false);

	const [isChecked, setIsChecked] = useState(false);

	const handleCheck = (e) => {
		setIsChecked(e.target.checked);
		console.log(isChecked, "jsj");
	};

	const onDrop = useCallback((acceptedFiles) => {
		setFiles(
			acceptedFiles.map((file) =>
				Object.assign(file, {
					preview: URL.createObjectURL(file),
				})
			)
		);
		if (acceptedFiles && acceptedFiles[0]) {
			const formdata = new FormData();
			formdata.append("image", acceptedFiles[0]);
		}
		setShowPreview(true);
		console.log("ajj", files);
	});

	const images = files.map((file) => (
		<div
			className="dnd-container"
			sx={{
				display: "flex",
				justifyContent: "center",
				alignItems: "center",
				width: "100%",
				height: "100%",
			}}
			key={file.name}
		>
			<img
				style={{ width: "100px" }}
				className="dnd-img mx-auto d-block"
				src={file.preview}
				alt="Screen"
			/>
		</div>
	));

	const removeImageHandler = (e) => {
		// const arr = files;
		// arr.splice(item, 1);
		// setFiles([...arr]);
		e.stopPropagation();
		setFiles([]);
		setShowPreview(false);
	};

	const [formInput, setFormInput] = useState({
		userId: "",
		policyId: "",
		message: "",
	});

	let dialingCodes = [
		{
			code: "+966",
			image: KSAFlagImage,
		},
		{
			code: "+91",
			image: IndiaFlagImage,
		},
	];

	let [selectedCode, setSelectedCode] = useState(dialingCodes[0]);

	let [phoneNumber, setPhoneNumber] = useState("");

	//initialiize state for radio buttons
	const [radioValue, setRadioValue] = useState("Motor");

	const handleFormInputChange = (e) => {
		const { name, value } = e.target || e || {};
		setFormInput({ ...formInput, [name]: value });
	};

	const handleChange = (event) => {
		let isChecked = event.target.checked;
		let value = event.target.value;
		let array = [];
		if (isChecked) {
			array = [...check];
			array.push(+value);
		} else {
			array = check.filter((id) => +id !== +value);
		}
		setCheck([...array]);
		console.log(check, "bol");
	};

	return (
		<div className="row">
			<div className="col-12">
					<div className="d-flex justify-content-center pt-4">
						<div className="openFormContainer">
                        <p className="fs-20 fw-800 complaint-formTitle m-0">
								Open a Complaint Request
							</p>
							<p className="fs-16 fw-400 pt-1 complaint-formPara">
								Please fill out the form below to submit your request and we
								will start our review on it immediately.
							</p>
							<p className="fs-20 fw-800 personalTitle">Request Type</p>
							<div className="row">
								<div className="col-12 ">
									<NormalRadioButton
										type="radio"
										name="radioOne"
										value="Motor"
										onChange={(e) => setRadioValue(e.target.value)}
										radioValue="Motor"
										checked={radioValue === "Motor"}s
										isStyledRadio={true}
										radioContainer={
											radioValue === "Motor"
												? "higlightRadioContainer"
												: "normalRadioContainer"
										}
									/>
								</div>
								<div className="col-12 pt-3">
									<NormalRadioButton
										type="radio"
										name="radioOne"
										value="Medical"
										onChange={(e) => setRadioValue(e.target.value)}
										radioValue="Medical"
										checked={radioValue === "Medical"}
										isStyledRadio={true}
										radioContainer={
											radioValue === "Medical"
												? "higlightRadioContainer"
												: "normalRadioContainer"
										}
									/>
								</div>
							</div>
							<div className="row pt-3">
								<div className="col-12 ">
									<NormalRadioButton
										type="radio"
										name="radioOne"
										value="P&C (Property & Casualty)"
										onChange={(e) => setRadioValue(e.target.value)}
										radioValue="P&C (Property & Casualty)"
										checked={radioValue === "P&C (Property & Casualty)"}
										isStyledRadio={true}
										radioContainer={
											radioValue === "P&C (Property & Casualty)"
												? "higlightRadioContainer"
												: "normalRadioContainer"
										}
									/>
								</div>
								<div className="col-12 pt-3">
									<NormalRadioButton
										type="radio"
										name="radioOne"
										value="E-Services Support"
										onChange={(e) => setRadioValue(e.target.value)}
										radioValue="E-Services Support"
										checked={radioValue === "E-Services Support"}
										isStyledRadio={true}
										radioContainer={
											radioValue === "E-Services Support"
												? "higlightRadioContainer"
												: "normalRadioContainer"
										}
									/>
								</div>
							</div>
							<div className="pt-3">
								<div className="complaintSelectContainer">
									<p className="fs-12 fw-400 serviceTypeLabel">Service Type</p>
									<NormalSelect
										className="complaintFormSelectInput2"
										arrowVerticalAlign="complaintArrowAlign"
										placeholder="Recovery"
										selectArrow={selectDropDown}
										selectControlHeight="22px"
										selectFontWeight="700"
									/>
								</div>
							</div>
							<div className="pt-3">
								<NormalSearch
									className="formInputFieldOne"
									name="policyId"
									value={formInput.policyId}
									placeholder="Policy Number"
									onChange={handleFormInputChange}
									needLeftIcon={true}
									leftIcon={officeBag}
								/>
							</div>
							<div className="pt-3 pb-2">
								<p className="messageTitle fs-14 fw-700 m-0 pb-2">Message</p>
								<textarea className="complaintForm-textArea">
									Type here...
								</textarea>
							</div>
							<div className="d-flex flex-row">
								<div>
									<img
										src={exclamation}
										className="img-fluid exclamatorIcon pr-2"
										alt="icon"
									/>
								</div>
								<div>
									<p className="fs-12 fw-400 formConditions m-0">
										Be detailed as possible when submitting a request in order
										for us to help you more effectively. The more detailed the
										information you provide, the faster we will be able to
										resolve your issue.
									</p>
									<p className="fs-12 fw-400 formConditions m-0">
										Below are some tips:
									</p>
								</div>
							</div>
							<ul className="fs-12 fw-400 formConditionsList pl-5 mb-3">
								<li>
									Explain step-by-step how to reproduce the scenario or the
									problem that you are describing.
								</li>
								<li>If you think a document would help, please include one.</li>
								<li>
									State when the problem started and what changes were made
									immediately beforehand.
								</li>
							</ul>
							<div className="borderLiningThree mb-3"></div>
							<p className="fs-20 fw-800 doc-Title">Supporting Documents</p>

							<Dropzone onDrop={onDrop}>
								{({ getRootProps, getInputProps }) => (
									<div
										{...getRootProps({
											className: "dropzone",
											onDrop: (event) => event.preventDefault(),
										})}
									>
										<div
											style={{
												display: "flex",
												justifyContent: "center",
												alignItems: "center",
												flexDirection: "column",
											}}
											className="image-upload-wrap"
										>
											<input {...getInputProps()} />
											{!showPreview ? (
												<div className="drag-text">
													<div>
														<img
															src={upload}
															className="img-fluid mx-auto d-block pb-3"
															alt="uploadicon"
														/>
														<div className="d-flex justify-content-center">
															<div>
																<p className="uploadTitle fs-14 fw-400 m-0 pb-3">
																	Upload files by drag and drop or{" "}
																	<span className="uploadLink">
																		click to upload
																	</span>
																	.
																</p>
															</div>
														</div>
														<p className="fs-12 fw-400 allowedFormats text-center">
															Allowed files: JPEG, PNG, DOC, DOCX, PDF Maximum
															Size: 10MB
														</p>
													</div>
												</div>
											) : (
												<>
													<div className="drag-text">
														<div>
															<div>{images}</div>
															{showPreview ? (
																<img
																	src={closeIcon}
																	className="img-fluid removeImgBtn"
																	onClick={(e) => removeImageHandler(e)}
																	alt="icon"
																/>
															) : (
																""
															)}
															<div className="d-flex justify-content-center">
																<div>
																	<p className="uploadTitle fs-14 fw-400 m-0 pb-3">
																		Upload files by drag and drop or{" "}
																		<span className="uploadLink">
																			click to upload
																		</span>
																		.
																	</p>
																</div>
															</div>
															<p className="fs-12 fw-400 allowedFormats text-center">
																Allowed files: JPEG, PNG, DOC, DOCX, PDF Maximum
																Size: 10MB
															</p>
														</div>
													</div>
												</>
											)}
										</div>
									</div>
								)}
							</Dropzone>

							<div className="borderLiningFour my-4"></div>
							<NormalButton
								label="Submit"
								className="complaintFormSubmitBtn p-4"
								onClick={() =>
									history.push(
										"/home/customerservice/SupportRequestConfirmation"
									)
								}
							/>
						</div>
					</div>
			</div>
		</div>
	);
};

export default OpenComplaintMobile;
