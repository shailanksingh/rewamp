import React from "react";
import "./style.scss";

function FitContentIctxtCard({
	cardIcon,
	cardTitle,
	cardPara,
	cardTitleColor,
	alignfitCard,
}) {
	return (
		<div className="fcitContainer" id={alignfitCard}>
			{cardIcon && <img src={cardIcon} alt="" />}
			<h5 style={{ color: cardTitleColor }}>{cardTitle}</h5>
			<p>{cardPara}</p>
		</div>
	);
}

export default FitContentIctxtCard;
