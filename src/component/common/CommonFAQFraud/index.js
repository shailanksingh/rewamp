import React, { useContext } from "react";
import Accordion from "react-bootstrap/Accordion";
import { useAccordionButton, AccordionContext } from "react-bootstrap";
import addIcon from "assets/svg/addIcon.svg";
import hideCollapse from "assets/svg/hideCollapse.svg";
import "./style.scss";

function CustomToggle({ children, eventKey, callback }) {
  const { activeEventKey } = useContext(AccordionContext);

  const decoratedOnClick = useAccordionButton(
    eventKey,
    () => callback && callback(eventKey)
  );

  const isCurrentEventKey = activeEventKey === eventKey;

  return (
    <button type="button" className="addButton" onClick={decoratedOnClick}>
      <div className="d-flex flex-row align-items-center">
        <div>
          <img
            src={isCurrentEventKey ? hideCollapse : addIcon}
            className="img-fluid accordionCollapseIcon pr-3"
            alt="icon"
          />
        </div>
        <div>{children}</div>
      </div>
    </button>
  );
}

export const CommonFaqFraud = ({ faqList }) => {
  return (
    <div className="row faqContainer">
      <div className="col-lg-12 col-12">
        <div className="row">
          {faqList.map((item, index) => {
            return (
              <div className="col-lg-6 col-md-12 col-12 pb-2" key={index}>
                <div className="reportAccordionQuestion">
                  <Accordion defaultActiveKey="1">
                    <CustomToggle eventKey="0">
                      <p className="fs-14 fw-400 pt-3 accordionQuestion">
                        {item.question}
                      </p>
                    </CustomToggle>
                    <div>
                      <Accordion.Collapse eventKey="0">
                        <div className="pb-3">
                          <p className="fs-16 fw-400 p-3 reportAccordionAnswer">
                            {item.answer}
                          </p>
                        </div>
                      </Accordion.Collapse>
                    </div>
                  </Accordion>
                </div>
              </div>
            );
          })}
        </div>
      </div>
    </div>
  );
};
