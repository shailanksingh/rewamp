import React, { useState, useContext, useEffect } from "react";
import { history } from "service/helpers";
import Accordion from "react-bootstrap/Accordion";
import { useAccordionButton, AccordionContext } from "react-bootstrap";
import { NormalButton } from "../NormalButton";
import { NormalSearch } from "../NormalSearch";
import { ServiceFeedBackForm } from "../ServiceFeedBackForm";
import { feedbackContentData } from "component/common/MockData/NewMockData";
import { emojiFiData } from "component/common/MockData/NewMockData";
import { ratingData } from "component/common/MockData/NewMockData";
import searchIcon from "assets/svg/headerSearchLight.svg";
import languageIcon from "assets/svg/languageIcon.svg";
import modeIcon from "assets/svg/modeIcon.svg";
import addIcon from "assets/svg/addIcon.svg";
import hideCollapse from "assets/svg/hideCollapse.svg";
import orangeArrow from "assets/svg/orangeArrow.svg";
import appStore from "assets/images/appleStore.png";
import playStore from "assets/images/playStore.png";
import appGal from "assets/images/appGallery.png";
import messenger from "assets/svg/Facebook messenger.svg";
import editor from "assets/svg/Edit File.svg";
import locatePin from "assets/svg/Location pin.svg";
import grids from "assets/svg/Grid.svg";
import timer from "assets/svg/recentTimer.svg";
import phoneIcon from "assets/svg/phoneCircleIcon.svg";
import userCar from "assets/svg/userCarIcon.svg";
import chatSmile from "assets/svg/Chat-smile.svg";
import viewDetails from "assets/svg/viewDetailsIcon.svg";
import "./style.scss";

function CustomToggle({ children, eventKey, callback }) {
  const { activeEventKey } = useContext(AccordionContext);

  const decoratedOnClick = useAccordionButton(
    eventKey,
    () => callback && callback(eventKey)
  );

  const isCurrentEventKey = activeEventKey === eventKey;

  return (
    <button type="button" className="addButton" onClick={decoratedOnClick}>
      <div className="d-flex flex-row">
        <div>
          <img
            src={isCurrentEventKey ? hideCollapse : addIcon}
            className="Road-accordionCollapseIcon pr-3"
            alt="icon"
          />
        </div>
        <div>{children}</div>
      </div>
    </button>
  );
}

export const NeedHelpContainer = ({ isRecentFeed = false }) => {
  const [search, setSearch] = useState("");

  const arrayIcons = [appStore, playStore, appGal];

  const getHelpIconsData = [chatSmile, chatSmile];

  const arrayHelpIcons = [
    {
      id: 0,
      icon: messenger,
      label: "Chat Now",
      link: "",
    },
    {
      id: 1,
      icon: editor,
      label: "Complaints",
      link: "",
    },
    {
      id: 2,
      icon: locatePin,
      label: "Locate us",
      link: "",
    },
    {
      id: 3,
      icon: grids,
      label: "All Services",
      link: "/dashboard/service",
    },
  ];

  const accordianDashboardData = [
    {
      id: 0,
      question:
        "The insured is provided with an Insurance Card and Bail Bond approved by the Traffic Police together with SANAD Insurance Policy to prevent the arrest or detainment of the driver causing the accident?",
      answer:
        "The insured is provided with an Insurance Card and Bail Bond approved by the Traffic Police together with SANAD Insurance Policy to prevent the arrest or detainment of the driver causing the accident?",
    },
    {
      id: 1,
      question:
        "The insurance covers all types of cars without specifying their type or model?",
      answer:
        "The insured is provided with an Insurance Card and Bail Bond approved by the Traffic Police together with SANAD Insurance Policy to prevent the arrest or detainment of the driver causing the accident?",
    },
    {
      id: 2,
      question:
        "The age restriction can be extended to include the drivers less than 21 years old?",
      answer:
        "The insured is provided with an Insurance Card and Bail Bond approved by the Traffic Police together with SANAD Insurance Policy to prevent the arrest or detainment of the driver causing the accident?",
    },
    {
      id: 3,
      question:
        "The expansion of insurance cover to include the accidents caused by the insured vehicle even if the driver is not present at the accident time?",
      answer:
        "The insured is provided with an Insurance Card and Bail Bond approved by the Traffic Police together with SANAD Insurance Policy to prevent the arrest or detainment of the driver causing the accident?",
    },
  ];

  const [update, setUpdate] = useState({
    needForm: true,
    qOne: true,
    qTwo: true,
    qThree: true,
  });

  return (
    <React.Fragment>
      <div className="row">
        <div className="col-12 needHelpContainer">
          <div className="d-flex flex-row-reverse">
            <div>
              <img
                src={modeIcon}
                className="img-fluid dashboard-needHelp-top-icons"
                alt="icon"
              />
            </div>
            <div>
              <NormalButton
                label="Emergency?"
                className="emergency-needHelp-DashboardBtn"
              />
            </div>
            <div>
              <img
                src={phoneIcon}
                className="img-fluid dashboard-needHelp-top-icons"
                alt="icon"
              />
            </div>
            <div>
              <img
                src={searchIcon}
                className="img-fluid dashboard-needHelp-top-icons"
                alt="icon"
              />
            </div>
            <div>
              <img
                src={languageIcon}
                className="img-fluid dashboard-needHelp-languageIcon"
                alt="icon"
              />
            </div>
          </div>
          {isRecentFeed && (
            <React.Fragment>
              <div className="col-lg-12 col-12 recent-main-container">
                <p className="fs-20 fw-800 recentTitle">Recent Interactions</p>
                <div className="recentContainer">
                  <div className="d-flex justify-content-between">
                    <div>
                      <span className="fs-10 fw-400 timerText">
                        {" "}
                        <img
                          src={timer}
                          className="img-fluid pr-1"
                          alt="icon"
                        />{" "}
                        30 Minutes Ago
                      </span>
                    </div>
                    <div>
                      <p className="fs-12 fw-400 dashboard-progressLabel m-0">
                        In progress
                      </p>
                    </div>
                  </div>
                  <div className="recent-sub-container mt-3">
                    <div className="d-flex flex-row">
                      <div className="pr-2">
                        <img
                          src={userCar}
                          className="img-fluid userCarIcon"
                          alt="icon"
                        />
                      </div>
                      <div>
                        <p className="fs-12 fw-400 userCar m-0">Merc. Benz</p>
                        <p className="fs14 fw-500 dashboard-userName m-0">
                          Prashant Dixit
                        </p>
                      </div>
                    </div>
                  </div>
                  <div className="d-flex justify-content-between pt-3">
                    <div>
                      <div className="d-flex flex-column">
                        {getHelpIconsData.map((items) => {
                          return (
                            <div className="pb-3">
                              <img
                                src={items}
                                className="img-fluid pr-2"
                                alt="icon"
                              />{" "}
                              <span className="fs-12 fw-400 getHelpTxt">
                                Get Help
                              </span>
                            </div>
                          );
                        })}
                      </div>
                    </div>
                    <div>
                      <span className="fs-12 fw-400 vieDetailsTxt pr-2">
                        View Details
                      </span>{" "}
                      <img src={viewDetails} className="img-fluid" alt="icon" />
                    </div>
                  </div>
                </div>
              </div>
            </React.Fragment>
          )}
          <div className="subDashboardContainer p-3">
            {isRecentFeed && (
              <div className="pb-4 mb-2">
                <span className="fs-12 fw-800 pr-2 text-uppercase">
                  View All Interactions
                </span>{" "}
                <img
                  src={orangeArrow}
                  className="img-fluid orangeNewArrow"
                  alt="arrow"
                />
              </div>
            )}
            <div className="searchDashboardContainer">
              <p className="text-center fs-20 fw-800 helptext">
                How can we help you?
              </p>
              <NormalSearch
                className="dashBoardheaderSearch"
                name="search"
                value={search}
                placeholder="What you're looking for?"
                onChange={(e) => setSearch(e.target.value)}
                needRightIcon={true}
              />
              <div className="row pt-3 px-3">
                {arrayHelpIcons.map((item, index) => {
                  return (
                    <div
                      className="col-3 p-0 pr-1"
                      onClick={() => history.push(item.link)}
                    >
                      <div className="dashboard-helpIcons" key={index}>
                        <img
                          src={item.icon}
                          className="img-fluid mx-auto d-block"
                          alt="icon"
                        />
                        <p className="fs-9 fw-400 helpIconLabel text-center pt-2">
                          {item.label}
                        </p>
                      </div>
                    </div>
                  );
                })}
              </div>
            </div>
            <div className="row pt-3">
              <p className="fs-20 fw-800 mostFaq m-0">Most Asked Questions</p>
              {accordianDashboardData.map((item, index) => {
                return (
                  <div className="col-lg-12 col-12" key={index}>
                    <div className="reportDashboardAccordionQuestion">
                      <Accordion defaultActiveKey="1">
                        <CustomToggle eventKey="0">
                          <p className="fs-12 fw-400 reportDashboardQuestion m-0 py-2">
                            {item.question}
                          </p>
                        </CustomToggle>
                        <div>
                          <Accordion.Collapse eventKey="0">
                            <p className="fs-10 fw-400 p-3 reportDashboardAccordionAnswer">
                              {item.answer}
                            </p>
                          </Accordion.Collapse>
                        </div>
                      </Accordion>
                    </div>
                  </div>
                );
              })}
            </div>
            <div className="d-flex justify-content-end pt-3">
              <div>
                <span className="viewText fs-12 fw-800 pr-2">
                  View All Questions
                </span>
                <img
                  src={orangeArrow}
                  className="img-fluid orangeArrow"
                  alt="arrow"
                />
              </div>
            </div>
            <div className="d-flex flex-row dashboard-socialIconBox">
              {arrayIcons.map((items) => {
                return (
                  <div>
                    <img
                      src={items}
                      className="img-fluid socio-icons"
                      alt="buttonIcon"
                    />
                  </div>
                );
              })}
            </div>
          </div>
        </div>
        <div className="col-12 pl-5">
          <ServiceFeedBackForm
            feedbackContentData={feedbackContentData}
            emojiData={emojiFiData}
            ratingData={ratingData}
            update={update}
          />
        </div>
      </div>
    </React.Fragment>
  );
};
