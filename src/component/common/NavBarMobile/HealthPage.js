import React from "react";
import "./healthpage.scss";
import { CommonFaq } from "component/common/CommonFaq";
import { customerServiceFaqList } from "component/common/MockData";
import ArrowRight from "assets/images/mobile/right_arrow.png";
import { InsuranceCardMobile } from "component/common/MobileReuseable/InsuranceCardMobile";
import truck_black from "assets/images/mobile/refill.png";
import tele_medicine from "assets/images/mobile/medical.png";
import flightdelay from "assets/images/mobile/tele.png";
import arrowrightblue from "assets/images/mobile/arrow_blue.png";
import HealthInsuranceCard from "./HealthInsuranceCard";

function HealthPage() {
  const insuranceCardData = [
    {
      id: 1,
      content: "Periodic Inspection",
      cardIcon: truck_black,
    },
    {
      id: 2,
      content: "Road Side Assistance",
      cardIcon: tele_medicine,
    },
    {
      id: 3,
      content: "Car Maintenance",
      cardIcon: flightdelay,
    },
    {
      id: 4,
      content: "Car Accident",
      cardIcon: truck_black,
    },
    {
      id: 5,
      content: "Car Wash",
      cardIcon: tele_medicine,
    },
    {
      id: 6,
      content: "Refill Medication",
      cardIcon: tele_medicine,
    },
    {
      id: 7,
      content: "Medical Reimbursement",
      cardIcon: tele_medicine,
    },
    {
      id: 8,
      content: "Eligibility letter",
      cardIcon: tele_medicine,
    },
    {
      id: 9,
      content: "Pregnancy Program",
      cardIcon: tele_medicine,
    },
    {
      id: 10,
      content: "Chronic Disease Management",
      cardIcon: tele_medicine,
    },
    {
      id: 11,
      content: "Home Child Vaccination",
      cardIcon: tele_medicine,
    },
    {
      id: 12,
      content: "Assist America",
      cardIcon: tele_medicine,
    },
    {
      id: 13,
      content: "Flight Delay Assistance",
      cardIcon: tele_medicine,
    },
    {
      id: 14,
      content: "Flight Delay Assistance",
      cardIcon: tele_medicine,
    },
  ];
  return (
    <div>
      <HealthInsuranceCard />
      <div className="question_section">
        <h6>Frequently Asked Questions</h6>
        <CommonFaq faqList={customerServiceFaqList} />
        <div className="d-flex pt-2">
          <div className="txt-size my-1">View All FAQs</div>
          <div className="mx-2 pt-1 image-size" height="20">
            <img src={ArrowRight} alt="Arrow" className="ml-2" />
          </div>
        </div>
      </div>
      <div className="service_list">
        <h5>Services we offer</h5>
        <InsuranceCardMobile
          heathInsureCardData={insuranceCardData}
          isArrow={true}
        />
        <div className="d-flex pt-2">
          <div className="txt-size my-1">View All Services</div>
          <div className="mx-2 pt-1 image-size" height="20">
            <img src={ArrowRight} alt="Arrow" className="ml-2" />
          </div>
        </div>
      </div>
      <div className="tawuniya-sec">
        <h5 className="now_heading">Tawuniya Now</h5>
        <div className="container nowscrolling">
          <div className="d-flex now-scroll">
            <div className=" now_box container">
              <p className="mb-0 news_color text-light mt-2">News</p>
              <p className="mb-4 day">2 days ago</p>
              <p className="tawuniyanow_content">
                Tawuniya is the first company in the Kingdom to install vehicle
                insurance policies
              </p>
            </div>
            <div className=" now_box container">
              <p className="mb-0 news_color text-light mt-2">News</p>
              <p className="mb-4 day">2 days ago</p>
              <p className="tawuniyanow_content">
                Tawuniya is the first company in the Kingdom to install vehicle
                insurance policies
              </p>
            </div>
            <div className=" now_box container">
              <p className="mb-0 news_color text-light mt-2">News</p>
              <p className="mb-4 day">2 days ago</p>
              <p className="tawuniyanow_content">
                Tawuniya is the first company in the Kingdom to install vehicle
                insurance policies
              </p>
            </div>
            <div className=" now_box container">
              <p className="mb-0 news_color text-light mt-2">News</p>
              <p className="mb-4 day">2 days ago</p>
              <p className="tawuniyanow_content">
                Tawuniya is the first company in the Kingdom to install vehicle
                insurance policies
              </p>
            </div>
          </div>
        </div>
        <div className="d-flex now_heading services">
          <div className="txt-size">View All Services</div>
          <div className="image-size" height="20">
            <img src={ArrowRight} alt="Arrow" className="ml-2" />
          </div>
        </div>
      </div>

      <div className="container help">
        <div className="card container">
          <p className="mb-1 help_font">Helpful Links</p>
          <div className="d-flex">
            <div className="insurance_fonts my-1">
              <h6> Covid-19 Travel insurance</h6>
            </div>
            <div className="mx-2 pt-1 image-size" height="20">
              <img src={arrowrightblue} alt="Arrow" className="ml-2" />
            </div>
          </div>

          <div className="d-flex">
            <div className="insurance_fonts my-1">
              <h6> Visit Visa Medical Insurance</h6>
            </div>
            <div className="mx-2 pt-1 image-size" height="20">
              <img src={arrowrightblue} alt="Arrow" className="ml-2" />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default HealthPage;
