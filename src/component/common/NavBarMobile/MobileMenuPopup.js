import React, { useState } from "react";
import "./mobilemenupopup.scss";
import RightArrowIcon from "../../../assets/images/menuicons/right-arrow.svg";
import { NavLink } from "react-router-dom";
import MenuCloseIcon from "../../../assets/images/mobile/menu-close.svg";
import RightArrowIconBlack from "../../../assets/images/mobile/rightarrow-black.svg";
import SearchIcon from "../../../assets/images/mobile/search.svg";
import UserIcon from "../../../assets/images/mobile/user-default.png";
import { InsuranceCardMobile } from "component/common/MobileReuseable/InsuranceCardMobile";
import CloseDark from "assets/images/mobile/close_dark.png";
import truck_black from "assets/images/mobile/truck_black.png";
import tele_medicine from "assets/images/mobile/tele_medicine.png";
import flightdelay from "assets/images/mobile/flightdelay.png";
import { history } from "service/helpers";
import LoginPageMobile from "component/Auth/LoginPageMobile";
import NormalPopup from "../NormalPopup";
import SearchBoxMobile from "./SearchBoxMobile";
import HealthPage from "./HealthPage";
import { NormalSearch } from "../NormalSearch";

const individualMenuList = [
  {
    title: "Notifications",
    count: "2",
    url: "#",
    activeMenu: true,
  },
  {
    title: "Home",
    url: "/home",
    activeMenu: true,
  },
  {
    title: "Products",
    url: "/home/all-products",
    activeMenu: true,
  },
  {
    title: "Claims",
    url: "#",
  },
  {
    title: "Loyalty Programs ",
    url: "#",
  },
  {
    title: "Support Center",
    url: "/home/customerservice/supportcenter",
  },
  {
    title: "Our Network",
    url: "#",
  },
  {
    title: "Careers",
    url: "/home/careers",
  },
  {
    title: "Contact us",
    url: "#",
  },
  {
    title: "About",
    url: "/home/aboutus",
  },
];

const corporateMenuList = [
  {
    title: "Notifications",
    count: "2",
    url: "#",
    activeMenu: true,
  },
  {
    title: "Home",
    url: "/home",
    activeMenu: true,
  },
  {
    title: "Products",
    url: "/home/all-products",
    activeMenu: true,
  },
  {
    title: "Claims",
    url: "#",
  },
  {
    title: "Loyalty Programs ",
    url: "/home/tawuniya-vitality",
  },
  {
    title: "Support Center",
    url: "#",
  },
  {
    title: "Our Network",
    url: "#",
  },
  {
    title: "Careers",
    url: "/home/careers",
  },
  {
    title: "Contact us",
    url: "/home/contactus",
  },
  {
    title: "About",
    url: "/home/aboutus",
  },
];

const investorMenuList = [
  {
    title: "Notifications",
    url: "#",
    count: "2",
    activeMenu: true,
  },
  {
    title: "Home",
    url: "/home",
    activeMenu: true,
  },
  {
    title: "Investor Relations",
    url: "/home/investor",
    activeMenu: true,
  },
  {
    title: "Fact Sheet",
    url: "/home/investor",
  },
  {
    title: "Financial Information",
    url: "/home/investor",
  },
  {
    title: "Share Information",
    url: "/home/investor",
  },
  {
    title: "Dividends",
    url: "/home/investor",
  },
  {
    title: "Analyst Coverage",
    url: "/home/investor",
  },
  {
    title: "Announcements",
    url: "/home/investor",
  },
  {
    title: "About Us",
    url: "/home/aboutus",
  },
  {
    title: "Media Center",
    url: "/home/mediacenter/medianewsdetails",
  },
  {
    title: "Contact Us",
    url: "/home/contactus",
  },
];

const insuranceCardData = [
  {
    id: 1,
    content: "Road Side Assistance",
    cardIcon: truck_black,
  },
  {
    id: 2,
    content: "Request Telemedicine",
    cardIcon: tele_medicine,
  },
  {
    id: 3,
    content: "Flight Delay Claim",
    cardIcon: flightdelay,
  },
  {
    id: 4,
    content: "Road Side Assistance",
    cardIcon: truck_black,
  },
  {
    id: 5,
    content: "Request Telemedicine",
    cardIcon: tele_medicine,
  },
];

const MobileMenuPopup = (props) => {
  const { setMenuOpen } = props;
  const [menuIndex, setMenuIndex] = useState(0);
  const [isOpenLoginModel, setIsOpenLoginModel] = useState(false);
  const [isSearchModel, setIsSearchModel] = useState(false);
  const [search, setSearch] = useState("Al Shamel");
  return (
    <div className={"page-navbar-menu " + (props.active ? "active" : "")}>
      <div className="mobile-menu-close">
        <div className="close-menu" onClick={() => props.closeMenu()}>
          <img src={MenuCloseIcon} alt="..." /> Close the menu
        </div>
      </div>
      <div className="mobile-menu-login-option">
        <div className="user-image">
          <img src={UserIcon} alt="..." />
        </div>
        <div className="login-text">
          {sessionStorage.getItem("loginDetails") ? (
            JSON.parse(sessionStorage.getItem("loginDetails"))
          ) : (
            <label onClick={() => setIsOpenLoginModel(true)}>
              Login Or Join
            </label>
          )}
        </div>
        <div className="right-arrow">
          <img src={RightArrowIcon} alt="..." />
        </div>
      </div>
      <div className="mobile-menu-search">
        <input
          type="text"
          placeholder="What you're looking for?"
          onClick={() => {
            setIsSearchModel(true);
          }}
        />
        <img src={SearchIcon} alt="..." />
      </div>
      <div className="mobile-menu-box-content">
        {isSearchModel && (
          <NormalPopup
            isSearchModel={isSearchModel}
            setIsSearchModel={setIsSearchModel}
          >
            <div>
              <div className="mobile-search-header">
                <img src={CloseDark} alt="..." />
                <NormalSearch
                  needRightIcon={true}
                  onChange={(e) => setSearch(e.target.value)}
                  type="text"
                  value={search}
                />
              </div>

              {search === "Health" ? <HealthPage /> : <SearchBoxMobile />}
            </div>
          </NormalPopup>
        )}
        <div className="looking-for-text">I'm Looking for</div>
        <div className="mobile-menu-tabs">
          <div
            onClick={() => setMenuIndex(0)}
            className={menuIndex === 0 ? "active-tab-index" : ""}
          >
            Individual
          </div>
          <div
            onClick={() => setMenuIndex(1)}
            className={menuIndex === 1 ? "active-tab-index" : ""}
          >
            Corporate
          </div>
          <div
            onClick={() => setMenuIndex(2)}
            className={menuIndex === 2 ? "active-tab-index" : ""}
          >
            Investor
          </div>
        </div>
        <div className="menu-title-text">Everything</div>
        {menuIndex === 0 && (
          <div className="individuals-menu">
            <div className="right-side-menu-list">
              {individualMenuList?.map(
                ({ title, count, url, activeMenu }, index) => {
                  return (
                    <NavLink
                      to={url}
                      key={index.toString()}
                      className={
                        "bold-menu " + (activeMenu ? "active-menu" : "")
                      }
                      onClick={() => {
                        history.push(url);
                        setMenuOpen(null);
                      }}
                    >
                      {title}
                      {count !== undefined && (
                        <span className="count">{count}</span>
                      )}
                      {activeMenu ? (
                        <img src={RightArrowIcon} alt="..." />
                      ) : (
                        <img src={RightArrowIconBlack} alt="..." />
                      )}
                    </NavLink>
                  );
                }
              )}
            </div>
          </div>
        )}
        {menuIndex === 1 && (
          <div className="enterprise-menu">
            <div className="right-side-menu-list">
              {corporateMenuList?.map(
                ({ title, url, count, activeMenu }, index) => {
                  return (
                    <NavLink
                      to={url}
                      key={index.toString()}
                      className={
                        "bold-menu " + (activeMenu ? "active-menu" : "")
                      }
                      onClick={() => {
                        history.push(url);
                        setMenuOpen(null);
                      }}
                    >
                      {title}
                      {count !== undefined && (
                        <span className="count">{count}</span>
                      )}
                      {activeMenu ? (
                        <img src={RightArrowIcon} alt="..." />
                      ) : (
                        <img src={RightArrowIconBlack} alt="..." />
                      )}
                    </NavLink>
                  );
                }
              )}
            </div>
          </div>
        )}
        {menuIndex === 2 && (
          <div className="sme-menu">
            <div className="right-side-menu-list">
              {investorMenuList?.map(
                ({ title, url, count, activeMenu }, index) => {
                  return (
                    <NavLink
                      to={url}
                      key={index.toString()}
                      className={
                        "bold-menu " + (activeMenu ? "active-menu" : "")
                      }
                      onClick={() => {
                        history.push(url);
                        setMenuOpen(null);
                      }}
                    >
                      {title}
                      {count !== undefined && (
                        <span className="count">{count}</span>
                      )}
                      {activeMenu ? (
                        <img src={RightArrowIcon} alt="..." />
                      ) : (
                        <img src={RightArrowIconBlack} alt="..." />
                      )}
                    </NavLink>
                  );
                }
              )}
            </div>
          </div>
        )}
      </div>
      <div className="mobile-menu-common-services">
        <div className="common-services-title">Common Services</div>
        <InsuranceCardMobile heathInsureCardData={insuranceCardData} />
      </div>
      <LoginPageMobile
        isOpenLoginModel={isOpenLoginModel}
        setIsOpenLoginModel={setIsOpenLoginModel}
      />
    </div>
  );
};

export default MobileMenuPopup;
