import React from "react";
import "./style.scss";
import dfappstore from "assets/svg/dfappstore.svg";
import dfgoogleplay from "assets/svg/dfgoogleplay.svg";
import dfappgallery from "assets/svg/dfappgallery.svg";
import textVitality from "assets/svg/textVitality.svg";
import appadverthand from "assets/svg/appadverthand.svg";
import gotoiconOrange from "assets/svg/gotoiconOrange.svg";
import AssistinAccident from "assets/svg/AssistinAccident.svg";
import Assistinmedical from "assets/svg/Assistinmedical.svg";
import RoadSideAssistance from "assets/svg/RoadSideAssistance.svg";
import gotoiconOffwhite from "assets/svg/gotoiconOffwhite.svg";
import advertManImage from "assets/images/advertManImage.png";
import appAdvertTopLogo from "assets/svg/appAdvertTopLogo.svg";
import appAdvertBtmLogo from "assets/svg/appAdvertBtmLogo.svg";
import { DashboardServiceCards } from "component/DashBoard/DashBoardComponents/DashboardServiceCards";
import { SubReportViolation } from "component/CustomerService/Violations/ViolationsPage/SubReportViolation";
import ReportViolation from "component/CustomerService/Violations/ViolationsPage/ReportViolation";

const TwuniyaAppAdvertContent = [
	{
		heading: "Tawuniya App",
		headingNext: "Quick, Easy & Hassle free",
		subHeading:
			"Our friendly customer support team is your extended family. Speak your heart out.",
		preface: "Download Now",
		prefaceIcon1: dfappstore,
		prefaceIcon2: dfgoogleplay,
		prefaceIcon3: dfappgallery,
		btmTitle: "If You Still Need Help,",
		btmSubTitile: "Contact the legendary support team right now.",
		advertImgCard: {
			advertManImage: advertManImage,
			textVitality: textVitality,
			title: "Get 30% Off annual fitness time gym membership!",
			gotoText: "Get it now",
			iconBtn: gotoiconOrange,
		},
		appadverthand: appadverthand,
		advertgotoCardleft: [
			{
				id: 1,
				cardIcon: AssistinAccident,
				content: "Assist in Accident",
				gotoicon: true,
				alignBannerCard: "alignAccidentCard",
			},
			{
				id: 1,
				cardIcon: RoadSideAssistance,
				content: "Hire Car Facility",
				gotoicon: true,
				alignBannerCard: "alignHireCard",
			},
		],
		advertgotoCardRight: [
			{
				id: 1,
				cardIcon: Assistinmedical,
				content: "Assist in medical",
				gotoicon: true,
				alignBannerCard: "",
			},
			{
				id: 1,
				cardIcon: RoadSideAssistance,
				content: "Road Side Assistance",
				gotoicon: true,
				alignBannerCard: "",
			},
		],
	},
];

export const TwuniyaAppAdvert = () => {
	return (
		<div>
			{TwuniyaAppAdvertContent.map((item, index) => {
				return (
					<div
						key={index}
						className="app-advert-container  mt-5 d-flex justify-content-between"
					>
						<div className="app-advert-left">
							<div className="app-advert-download-container">
								<div className="app-advert-text">
									<h2>{item.heading}</h2>
									<h2>{item.headingNext}</h2>
									<p className="mt-2 pt-2">{item.subHeading}</p>
								</div>
								<div className="app-advert-download">
									<p>{item.preface}</p>
									<div className="app-advert-download-btn-container d-flex justify-content-between mt-2 pt-1">
										<img
											className="app-advert-download-btn"
											src={item.prefaceIcon1}
											alt="download on the app store button"
										/>
										<img
											className="app-advert-download-btn"
											src={item.prefaceIcon2}
											alt="get it on google play button"
										/>
										<img
											className="app-advert-download-btn"
											src={item.prefaceIcon3}
											alt="explore it on huawei app gallery button"
										/>
									</div>
								</div>
								<div className="app-advert-support-text">
									<h5>{item.btmTitle}</h5>
									<p>{item.btmSubTitile}</p>
								</div>
							</div>
						</div>
						<div className="app-advert-right">
							<div
								className="app-advert-fitness-card"
								style={{
									backgroundImage: `url(${item.advertImgCard.advertManImage})`,
									backgroundRepeat: "no-repeat",
									backgroundSize: "cover",
								}}
							>
								<img src={item.advertImgCard.textVitality} alt="" />
								<h5>{item.advertImgCard.title}</h5>
								<div className="advert-fitness-card-btn">
									<img src={item.advertImgCard.iconBtn} alt="" />
									<span className="pl-2">{item.advertImgCard.gotoText}</span>
								</div>
							</div>

							<div className="d-flex justify-content-between motorBannerCardLayout">
								<div>
									<div className="motor-BannerCard-container-one">
										<div className="motor-Banner-Card-one">
											<img
												src={AssistinAccident}
												className="img-fluid pb-2"
												alt="icon"
											/>
											<p className="fs-10 fw-700 motorBanner-card-Title-one m-0">
												Assist in Accident
											</p>
											<img
												src={gotoiconOffwhite}
												className="img-fluid pt-1"
												alt="icon"
											/>
										</div>
									</div>

									<div className="motor-BannerCard-container-two">
										<div className="motor-Banner-Card-two">
											<img
												src={RoadSideAssistance}
												className="img-fluid pb-2"
												alt="icon"
											/>
											<p className="fs-10 fw-700 motorBanner-card-Title-two m-0">
												Hire Car Facility
											</p>
											<img
												src={gotoiconOffwhite}
												className="img-fluid pt-1"
												alt="icon"
											/>
										</div>
									</div>
								</div>
								<div className="advert-card-hand">
									<img src={item.appadverthand} alt="" />
								</div>
								<div>
									<div className="motor-BannerCard-container-three">
										<div className="motor-Banner-Card-three">
											<img
												src={Assistinmedical}
												className="img-fluid pb-2"
												alt="icon"
											/>
											<p className="fs-10 fw-700 motorBanner-card-Title-three m-0">
												Assist in medical
											</p>
											<img
												src={gotoiconOffwhite}
												className="img-fluid pt-1"
												alt="icon"
											/>
										</div>
									</div>

									<div className="motor-BannerCard-container-four">
										<div className="motor-Banner-Card-four">
											<img
												src={RoadSideAssistance}
												className="img-fluid pb-2"
												alt="icon"
											/>
											<p className="fs-10 fw-700 motorBanner-card-Title-four m-0">
												Road Side Assistance
											</p>
											<img
												src={gotoiconOffwhite}
												className="img-fluid pt-1"
												alt="icon"
											/>
										</div>
									</div>
									{/* <DashboardServiceCards
								isfullWidth="col-6"
											dashboardServiceCardData={item.advertgotoCardRight}
										/> */}
								</div>
								{/* <div className="advert-right-servicecard-lft">
									<DashboardServiceCards
										dashboardServiceCardData={item.advertgotoCardleft}
									/>
								</div>
								<div className="advert-right-servicecard-right ">
									<div>
										<DashboardServiceCards
											dashboardServiceCardData={item.advertgotoCardRight}
										/>
									</div>
								</div> */}
							</div>
						</div>
						<img className="appAdvertTopLogo" src={appAdvertTopLogo} alt="" />
						<img className="appAdvertBtmLogo" src={appAdvertBtmLogo} alt="" />
					</div>
				);
			})}
			<ReportViolation
				isLayout={true}
				isMotor={true}
				cardWidth="motorReportCard"
			/>
		</div>
	);
};
