import React, { useState, useCallback } from "react";
import { GoogleMap, InfoBox, InfoWindow, Marker, useJsApiLoader } from "@react-google-maps/api";
import Logo from 'assets/images/mobile/tawuniya_logo_blue.svg'
import './style.scss'

const containerStyle = {
	width: "100%",
	height: "100%",
	borderRadius: "4px",
};

const center = {
	lat: 25.352593,
	lng: 49.603326,
};

function GoogleMapComp({ branches = [] }) {
	const { isLoaded } = useJsApiLoader({
		id: "google-map-script",
		googleMapsApiKey: "AIzaSyDb6LonQW9ottovHBcIq-J1kR2fXfIw4G0",
	});

	const [map, setMap] = useState(null);
	const [infoCoords, setInfoCoords] = useState({lat: null, lng: null})
	const [branchDetails, setBranchDetails] = useState(null)

	const onLoad = useCallback(function callback(map) {
		const bounds = new window.google.maps.LatLngBounds(center);
		map.fitBounds(bounds);
		setMap(map);
	}, []);

	const onUnmount = useCallback(function callback(map) {
		setMap(null);
	}, []);

	return isLoaded ? (
		<GoogleMap
			mapContainerStyle={containerStyle}
			center={center}
			onLoad={onLoad}
			zoom={10}
			onUnmount={onUnmount}
		>
			{/* Child components, such as markers, info windows, etc. */}
			{branches.map((branch, idx) => {
				const lat = branch.latitudelongitude.split(',')[0]
				const lng = branch.latitudelongitude.split(',')[1]
				if(lat && lng) {
					return <><Marker
						key={idx}
						position={{lat: lat * 1, lng: lng * 1}}
						icon={Logo}
						onClick={e => {
							setBranchDetails(branch)
							setInfoCoords({lat: e.latLng.lat(), lng: e.latLng.lng()})
						}}
					/>
					{infoCoords.lat !== null && infoCoords.lng !== null && branchDetails !== null &&
						<InfoWindow
							position={{lat: infoCoords.lat, lng: infoCoords.lng}}
							onCloseClick={() => setInfoCoords({lat: null, lng: null})}>
							<div className="infoCard">
								<h5>{branchDetails.name}</h5>
								<p>Phone: <strong>{branchDetails.mobilenumber}</strong> </p>
								{/* <p>Fax: <strong>{branchDetails.}</strong> </p> */}
								<p>Email: <strong>{branchDetails.email}</strong> </p>
								<p>Working Hours: <strong>{branchDetails.workhours}</strong> </p>
								<h6>Get Directions</h6>
							</div>
						</InfoWindow>
					}
					</>
				}
			})}
		</GoogleMap>
	) : (
		<></>
	);
}

export default React.memo(GoogleMapComp);
