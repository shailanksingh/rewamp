import React from "react";
import { history } from "service/helpers";
import "./style.scss";
export const ServiceCard = ({
  serviceData,
  routeURL,
  cardLayerOneHeight,
  columnClass = "col-lg-2 col-12",
}) => {
  return (
    <div className="row Servicecard-Container">
      {serviceData.map((item, index) => {
        return (
          <div className={`${columnClass} pb-4`} key={index}>
            <div className="serviceCardLayoutOne">
              <div className={`${cardLayerOneHeight} servicePaddingOne`}>
                <p className="m-0 fs-15 fw-800 serviceTitle pt-4">
                  {item.title}
                </p>
                <p
                  className={`${item.contentAlignClass} m-0 fs-12 fw-400 serviceContent`}
                >
                  {item.content}
                </p>
              </div>
              <div className="servicePaddingTwo">
                <div className="d-flex justify-content-between py-2 my-1">
                  <div>
                    <img
                      src={item.imgOne}
                      className="img-fluid serviceIcon"
                      alt="serviceicon"
                    />
                  </div>
                  <div>
                    <img
                      src={item.imgTwo}
                      className="img-fluid serviceIcon cursor-pointer"
                      alt="arrow"
                      onClick={() =>
                        routeURL ? history.push(routeURL) : console.log("")
                      }
                    />
                  </div>
                </div>
              </div>
            </div>
          </div>
        );
      })}
    </div>
  );
};
