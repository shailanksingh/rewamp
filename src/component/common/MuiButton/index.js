import React from "react";
import { Button } from "@material-ui/core";

const MuiButton = ({
	children,
	className = "",
	label = "",
	onClick,
	id,
	disabled = false,
	viewButtonHeader = false,
	viewButtonColorHeader = false,
	primary = false,
	...otherProps
}) => {
	// const configButton = {
	// 	variant: "contained",
	// 	fullWidth: true,
	// };

	return (
		<Button
			// {...configButton}
			{...otherProps}
			id={id}
			className={`btn cursor-pointer d-flex justify-content-center align-items-center
      ${primary ? "primary-btn" : ""}
      ${viewButtonHeader ? "viewButtonHeader" : ""}
      ${viewButtonColorHeader ? "viewButtonColorHeader" : ""}
      ${className}`}
			onClick={onClick}
			disabled={disabled}
		>
			{children}
		</Button>
	);
};

export default MuiButton;
