import React from "react";
import "./style.scss";
import { Cards } from "../index";
import { history } from "service/helpers";

export const ProgramBenefits = ({ programBenefits, banner, routeURL }) => {
  let programBenefitsKeys = Object.keys(programBenefits);
  return (
    <div
      className="insurance-program-benefits"
      style={{ backgroundImage: "url(" + banner + ")" }}
    >
      <div className="benefits-title">The Program Benefits</div>
      <div className="cards-list-block">
        {programBenefitsKeys?.map((key, keyIndex) => {
          return (
            <div className="cards-column" key={keyIndex.toString()}>
              {programBenefits?.[key]?.map((item, index) => {
                return (
                  <div
                    className="benefits-card-root"
                    key={index.toString()}
                    onClick={() => history.push(routeURL)}
                  >
                    <Cards cardType="3" data={item} />
                  </div>
                );
              })}
            </div>
          );
        })}
      </div>
    </div>
  );
};
