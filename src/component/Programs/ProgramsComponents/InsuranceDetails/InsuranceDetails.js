import React from "react";
import "./style.scss";
import { Cards } from "../index";

export const InsuranceDetails = ({
	insuranceDetails
}) => {
	let { insuranceForForeignPilgrims, features } = insuranceDetails;
	return (
		<div className="insurance-program-details">
            {insuranceForForeignPilgrims && <div className="foreign-pilgrims">
				<div className="left-side-content main-title">
					<div className="title">{insuranceForForeignPilgrims?.title}</div>
					<p className="description">{insuranceForForeignPilgrims?.description}</p>
				</div>
				<div className="right-side-content">
					<div className="column">
						{insuranceForForeignPilgrims?.content?.block1?.map((item, index) => {
							return (
								<div className="program-card-root" key={index.toString()}>
									<Cards 
										cardType="1"
										data={item}
									/>
								</div>
							);
						})}
					</div>
					<div className="column">
						{insuranceForForeignPilgrims?.content?.block2?.map((item, index) => {
							return (
								<div className="program-card-root" key={index.toString()}>
									<Cards 
										cardType="1"
										data={item}
									/>
								</div>
							);
						})}
					</div>
				</div>
			</div>}
            {features && <div className="program-features">
				<div className="left-side-content">
					<div className="column">
						{features?.content?.block1?.map((item, index) => {
							return (
								<div className="program-card-root" key={index.toString()}>
									<Cards 
										cardType="2"
										data={item}
									/>
								</div>
							);
						})}
					</div>
					<div className="column">
						{features?.content?.block2?.map((item, index) => {
							return (
								<div className="program-card-root" key={index.toString()}>
									<Cards 
										cardType="2"
										data={item}
									/>
								</div>
							);
						})}
					</div>
				</div>
				<div className="right-side-content main-title">
					<div className="title">{features?.title}</div>
					<p className="description">{features?.description}</p>
				</div>
			</div>}
		</div>
	);
};
