import React from "react";
import { Cards } from "../index";
import "./style.scss";

export const RegisterHere = ({ details, routeURL }) => {
  return (
    <div className="insurance-program-register-here">
      <Cards cardType="4" data={details?.card1} />
      <Cards cardType="5" data={details?.card2} />
      <Cards cardType="6" data={details?.card3} routeURL={routeURL} />
    </div>
  );
};
