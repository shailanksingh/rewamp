import React from "react";
import "./style.scss";

export const Banner = ({
	image,
	title,
	cardContent = {},
}) => {
	return (
		<div className="insurance-program-banner">
            <div className="banner-content" style={{ backgroundImage: "url(" + image + ")" }}>
				<h1 className="banner-title">{title}</h1>
			</div>
			<div className="banner-card">
				<h2>{cardContent?.description1}</h2>
				<p>{cardContent?.description2}</p>
			</div>
		</div>
	);
};
