import React from "react";
import "./style.scss";
import RightArrowImage from "assets/images/menuicons/right-arrow.svg";
import { history } from "service/helpers";

export const Cards = ({ cardType, data, routeURL }) => {
  if (!cardType) return "";
  return (
    <>
      {cardType === "1" && (
        <div className="insurance-program-cards card-type-1">
          <div className="icon">
            <img src={data?.icon} alt="..." />
          </div>
          <div className="description">{data?.title}</div>
        </div>
      )}
      {cardType === "2" && (
        <div className="insurance-program-cards card-type-2">
          <div className="title">{data?.title}</div>
          <div className="description">{data?.description}</div>
        </div>
      )}
      {cardType === "3" && (
        <div className="insurance-program-cards card-type-3">
          <div className="text">Maximum of</div>
          <div className="price">
            {data?.price} <span>SAR</span>
          </div>
          <div className="description">{data?.description}</div>
        </div>
      )}
      {cardType === "4" && (
        <div className="insurance-program-cards card-type-4">
          <div className="description">
            {data?.description?.map((text, index) => {
              return <p key={index.toString()}>{text}</p>;
            })}
          </div>
          <div className="link">
            <a href="#">
              REGISTER HERE <img src={RightArrowImage} alt="..." />
            </a>
          </div>
        </div>
      )}
      {cardType === "5" && (
        <div className="insurance-program-cards card-type-5">
          <div className="contact-title">
            For inquiries and emergencies, contact TCS Company
          </div>
          <div className="contact-details">
            {data?.contact?.map(({ title, contact }, index) => {
              return (
                <div key={index.toString()} className="contact-list">
                  <div className="title">{title}</div>
                  <div className="contact">{contact}</div>
                </div>
              );
            })}
          </div>
        </div>
      )}
      {cardType === "6" && (
        <div className="insurance-program-cards card-type-6">
          <div className="title">
            Want to download our {data?.name} Insurance Program Policy? <br />
            Here you can find it.
          </div>
          <button
            className="btn cursor-pointer d-flex justify-content-center align-items-center p-3"
            onClick={() =>
              routeURL ? history.push(routeURL) : console.log("")
            }
          >
            View {data?.name} Insurance Program Policy
          </button>
        </div>
      )}
    </>
  );
};
