import React from "react";
import "./style.scss";
import {
  Banner,
  InsuranceDetails,
  ProgramBenefits,
  RegisterHere,
} from "../ProgramsComponents";
import BannerImg from "../../../assets/images/insurance-banner-1.png";
import CenterBannerImg from "../../../assets/images/insurance-banner-4.png";
import Icon1 from "../../../assets/images/insurance-icon-1.svg";
import Icon2 from "../../../assets/images/insurance-icon-4.svg";
import Icon3 from "../../../assets/images/insurance-icon-3.svg";

let insuranceDetails = {
  insuranceForForeignPilgrims: {
    title: "Why Hajj Insurance for foreign pilgrims?",
    description:
      "The comprehensive insurance program for Umrah pilgrims was launched at the beginning of 2020.",
    content: {
      block1: [
        {
          icon: Icon1,
          title:
            "It provides safety and peace of mind during the holy journey in the kingdom",
        },
        {
          icon: Icon3,
          title:
            "It bears the medical & quarantine expenses in the event of infection with the emerging Corona Virus",
        },
      ],
      block2: [
        {
          icon: Icon2,
          title:
            "It bears the burdens resulting from flight disruption, including accommodation and meals costs",
        },
      ],
    },
  },
  features: {
    title: "The Program Features",
    description:
      "The comprehensive insurance program for Umrah pilgrims was launched at the beginning of 2020.",
    content: {
      block1: [
        {
          title: "Insurance",
          description: "Covering General Accidents cases",
        },
        {
          title: "Up to 100,000 SAR",
          description: "Insurance Cover Limit per insured Pilgrim",
        },
      ],
      block2: [
        {
          title: "75 Days",
          description:
            "Coverage term starting from the date of entry to the Kingdom of Saudi Arabia",
        },
        {
          title: "Coverage Geographical Scope",
          description: "Inside the Kingdom of Saudi Arabia",
        },
      ],
    },
  },
};

let programBenefits = {
  block1: [
    {
      price: "100,000",
      description: "in case of Accidental Death",
    },
  ],
  block2: [
    {
      price: "100,000",
      description: "for Accidental Permanent Disability",
    },
    {
      price: "450",
      description:
        "per day for accommodation cost related to COVID 19 quarantine and for maximum of 14 days",
    },
  ],
  block3: [
    {
      price: "100,000",
      description: "for Repatriation of Mortal Remains",
    },
  ],
  block4: [
    {
      price: "5,000",
      description:
        "for Flight Cancellation cases to cover accommodation, meals and transportation costs",
    },
    {
      price: "650,000",
      description: "for COVID-19 Medical and hospital costs",
    },
  ],
  block5: [
    {
      price: "500",
      description: "per ticket for Departure Flight Delay cases",
    },
  ],
};

let registerHere = {
  card1: {
    description: [
      "Please contact (Total Care Saudi Company) at any time of the day, as soon as the situation is expected to involve expenses that fall within the scope of Hajj Insurance cover",
      "For more information about the beneficiary status and to follow up with pre-approvals and claims",
    ],
  },
  card2: {
    contact: [
      {
        title: "Inside KSA",
        contact: "8004400008",
      },
      {
        title: "Outside KSA",
        contact: "+966 138129700",
      },
      {
        title: "Website",
        contact: "www.enaya-ksa.com",
      },
    ],
  },
  card3: {
    name: "Umrah",
  },
};

const HajjInsuranceProgram = () => {
  return (
    <div className="hajj-insurance-program">
      <div className="banner-root">
        <Banner
          image={BannerImg}
          title="The insurance program for Hajj Pilgrims was launched in conjunction with the 2022 Hajj Season"
          cardContent={{
            description1:
              "The insurance program for Hajj Pilgrims was launched in conjunction with 2022 Hajj Season. It is one of the initiatives that contribute in improving the quality of the provided services to pilgrims by allocating this insurance coverage for general accidents cases to compensate the beneficiaries under these covered cases and to find the fair solutions and immediate procedures in case of the occurrence of any covered incident.",
            description2:
              "Hajj insurance product covers (General Accidents) under certain conditions. Tawuniya has been honored to provide this service to the Pilgrims of the Holy Mosque in cooperation with the participating insurance companies in the Kingdom of Saudi Arabia by launching a joint insurance pool managed by Tawuniya.",
          }}
        />
      </div>
      <div className="insurance-details-root">
        <InsuranceDetails insuranceDetails={insuranceDetails} />
      </div>
      <div className="program-benefits-root">
        <ProgramBenefits
          programBenefits={programBenefits}
          banner={CenterBannerImg}
          routeURL="/home/programs/UmrahInsuranceProgram"
        />
      </div>
      <div className="program-register-here-root">
        <RegisterHere
          details={registerHere}
          routeURL="/home/programs/UmrahInsuranceProgram"
        />
      </div>
    </div>
  );
};

export default HajjInsuranceProgram;
