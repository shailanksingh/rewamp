import React from "react";
import "./style.scss";
import {
  Banner,
  InsuranceDetails,
  ProgramBenefits,
  RegisterHere,
} from "../ProgramsComponents";
import BannerImg from "../../../assets/images/insurance-banner-1.png";
import CenterBannerImg from "../../../assets/images/insurance-banner-3.png";
import Icon1 from "../../../assets/images/insurance-icon-1.svg";
import Icon2 from "../../../assets/images/insurance-icon-2.svg";
import Icon3 from "../../../assets/images/insurance-icon-3.svg";
import Icon4 from "../../../assets/images/insurance-icon-4.svg";

let insuranceDetails = {
  insuranceForForeignPilgrims: {
    title: "Why Umrah Insurance for foreign pilgrims?",
    description:
      "The comprehensive insurance program for Umrah pilgrims was launched at the beginning of 2020.",
    content: {
      block1: [
        {
          icon: Icon1,
          title:
            "It provides safety and peace of mind during the holy journey in the kingdom",
        },
        {
          icon: Icon3,
          title:
            "It bears the medical & quarantine expenses in the event of infection with the emerging Corona Virus COVID-19",
        },
      ],
      block2: [
        {
          icon: Icon2,
          title: "It provides the necessary health care",
        },
        {
          icon: Icon4,
          title:
            "It bears the burdens resulting from flight disruption, including accommodation and meals costs.",
        },
      ],
    },
  },
};

let programBenefits = {
  block1: [
    {
      price: "100,000",
      description: "for Emergency Medical benefit limit",
    },
    {
      price: "100,000",
      description: "for Accidental Permanent Disability",
    },
  ],
  block2: [
    {
      price: "5,000",
      description: "for Emergency Pregnancy and Delivery cases",
    },
    {
      price: "10,000",
      description: "for Repatriation of Mortal Remains",
    },
  ],
  block3: [
    {
      price: "500",
      description: "for Emergency Dental treatments",
    },
    {
      price: "500",
      description: "per ticket for Departure Flight Delay cases",
    },
    {
      price: "5,000",
      description:
        "for Flight Cancellation cases to cover accommodation, meals and transportation costs",
    },
  ],
  block4: [
    {
      price: "100,000",
      description: "for Medical Evacuation inside and outside the Kingdom",
    },
    {
      price: "450",
      description:
        "per day for accommodation cost related to COVID 19 quarantine and for maximum of 14 days",
    },
  ],
  block5: [
    {
      price: "100,000",
      description: "in case of Accidental Death",
    },
    {
      price: "650,000",
      description: "for COVID-19 Medical and hospital costs",
    },
  ],
};

let registerHere = {
  card1: {
    description: [
      "The beneficiary needs to visit one of the approved medical providers or contact (Total Care Saudi Company) at any time of the day, as soon as the situation is expected to involve expenses that fall within the scope of Umrah Insurance cover",
      "For more information about the beneficiary status and to follow up with pre-approvals and claims",
    ],
  },
  card2: {
    contact: [
      {
        title: "Inside KSA",
        contact: "8004400008",
      },
      {
        title: "Outside KSA",
        contact: "+966 138129700",
      },
      {
        title: "Website",
        contact: "www.enaya-ksa.com",
      },
    ],
  },
  card3: {
    name: "Hajj",
  },
};

const UmrahInsuranceProgram = () => {
  return (
    <div className="umrah-insurance-program">
      <div className="banner-root">
        <Banner
          image={BannerImg}
          title="The comprehensive insurance program for Umrah pilgrims was launched at the beginning of 2020"
          cardContent={{
            description1:
              "The comprehensive insurance program for Umrah pilgrims was launched at the beginning of 2020. It is one of the initiatives that contribute in improving the quality of the provided services to pilgrims by allocating this comprehensive insurance coverage for emergency cases to compensate the beneficiaries under these covered cases and to find the fair solutions and immediate procedures in case of the occurrence of any covered incident.",
            description2:
              "Umrah insurance product includes (Medical Emergencies & General Accidents) Coverages under certain conditions. Tawuniya has been honored to provide this service to the Pilgrims of the Holy Mosque in cooperation with the participating insurance companies in the Kingdom of Saudi Arabia by launching a joint insurance pool managed by Tawuniya.",
          }}
        />
      </div>
      <div className="insurance-details-root">
        <InsuranceDetails insuranceDetails={insuranceDetails} />
      </div>
      <div className="program-benefits-root">
        <ProgramBenefits
          programBenefits={programBenefits}
          banner={CenterBannerImg}
          routeURL="/home/programs/hajjinsuranceprogram"
        />
      </div>
      <div className="program-register-here-root">
        <RegisterHere
          details={registerHere}
          routeURL="/home/programs/hajjinsuranceprogram"
        />
      </div>
    </div>
  );
};

export default UmrahInsuranceProgram;
