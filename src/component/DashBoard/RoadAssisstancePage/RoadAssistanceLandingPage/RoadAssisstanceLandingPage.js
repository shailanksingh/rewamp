import React from "react";
import { DashBoardLandingPage } from "component/common/DashBoardLandingPage";
import {
	dashboardRoadHeaderData,
	dashboardRoadProgressData,
	faqRoadDashboardData,
	roadAssisstantContentData,
} from "component/common/MockData";
import "./style.scss";

export const RoadAssisstanceLandingPage = () => {

	return (
		<div className="roadAssistance-LandingContainer">
			<DashBoardLandingPage
				dashboardHeaderData={dashboardRoadHeaderData}
				dashboardProgressData={dashboardRoadProgressData}
				faqDashboardData={faqRoadDashboardData}
				dashBoardContentData={roadAssisstantContentData}
			/>
		</div>
	);
};
