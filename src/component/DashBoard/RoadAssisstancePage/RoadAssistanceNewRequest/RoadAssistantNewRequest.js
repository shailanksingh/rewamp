import React, { useState } from "react";
import { RequestPage } from "component/common/DashBoardLandingPage";
import { roadAssistanceRequestHeaderData } from "component/common/MockData/NewMockData";
import { NormalSelect } from "component/common/NormalSelect";
import { NormalButton } from "component/common/NormalButton";
import { DashboardSelector } from "component/common/DashBoardSubComponents";
import highlightRoadTow from "assets/svg/highlightGlucoseMeter.svg";
import normalRoadTow from "assets/svg/glucose-meter (Traced).svg";
import selectArrow from "assets/svg/complaintSelectDropdown.svg";
import highlightTow from "assets/svg/dashboardIcons/towHighlight.svg";
import normalBattery from "assets/svg/dashboardIcons/normalBattery.svg";
import normalGas from "assets/svg/dashboardIcons/normalGas.svg";
import normalTire from "assets/svg/dashboardIcons/normalTire.svg";
import "./style.scss";
import { history } from "service/helpers";

export const RoadAssistantNewRequest = () => {
  const [roadPill, setRoadPill] = useState([
    {
      id: 0,
      name: "Regular Tow",
      discription: "Take your car to a specialized center",
      status: false,
      highlightIcon: highlightTow,
      normalIcon: normalRoadTow,
    },
    {
      id: 1,
      name: "Battery",
      discription: "Charge or replace it with a new one",
      status: false,
      highlightIcon: highlightRoadTow,
      normalIcon: normalBattery,
    },
    {
      id: 2,
      name: "Gas",
      discription: "Gasoline or diesel delivery inside or outside the city",
      status: false,
      highlightIcon: highlightRoadTow,
      normalIcon: normalGas,
    },
    {
      id: 3,
      name: "Tire",
      discription: "Dealing with all tire malfunctions",
      status: false,
      highlightIcon: highlightRoadTow,
      normalIcon: normalTire,
    },
  ]);

  const urlName = "1";

  return (
    <RequestPage requestHeaderData={roadAssistanceRequestHeaderData}>
      <div className="row">
        <div className="col-lg-12 col-12">
          <div className="roadMapContainer p-4">
            <div className="pb-3">
              <iframe
                height="540px"
                width="100%"
                frameborder="0"
                scrolling="no"
                src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3333.967410377096!2d-111.89998968453055!3d33.31966746342457!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x0!2zMzPCsDE5JzEwLjgiTiAxMTHCsDUzJzUyLjEiVw!5e0!3m2!1sen!2sus!4v1516690469899"
              />
            </div>
            <div>
              <NormalSelect
                className="roadSelectInput"
                arrowVerticalAlign="roadArrowAlign"
                placeholder="Select your vehicle"
                paddingLeft="10px"
                selectArrow={selectArrow}
                selectFontWeight="400"
                phColor="#455560"
                fontSize="16px"
              />
            </div>
          </div>
          <div className="col-lg-12 col-12 p-0">
            <p className="fs-20 fw-800 selectRoadService-title pt-4">
              Select the Service
            </p>
            <div className="roadSelector-liner pb-3 px-2">
              <DashboardSelector
                toggle={roadPill}
                setToggle={setRoadPill}
                division="col-3 pr-1 pl-1"
              />
            </div>
          </div>
          <div className="pt-4">
            <NormalButton
              label="Submit Road Assistance Request"
              className="submit-RoadAssist-Button p-3"
              onClick={() =>
                history.push(
                  `/dashboard/service/road-assisstance/request-confirmation/${urlName}`
                )
              }
            />
          </div>
        </div>
      </div>
    </RequestPage>
  );
};
