import React from "react";
import { RequestIdPage } from "component/common/DashBoardLandingPage";
import { roadAssistRequestIdData } from "component/common/MockData/NewMockData";

export const RoadAssistanceTrackId = () => {
	return (
		<div>
			<RequestIdPage requestIdData={roadAssistRequestIdData} />
		</div>
	);
};
