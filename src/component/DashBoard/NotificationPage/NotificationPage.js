import React  from "react";
import "./style.scss";
import { AccidentClaimCard } from "../DashBoardComponents/AccidentClaimCard";
import _Vehicle from "assets/svg/_Vehicle.svg";
import file from "assets/svg/file.svg";
import calender from "assets/svg/Calendar.svg";
import PregnancyProgram from "assets/svg/PregnancyProgram.svg";
import RequestTelemedicine from "assets/svg/RequestTelemedicine.svg";
import questionmarkcircle from "assets/svg/question-mark-circle.svg";
import time from "assets/svg/time.svg";
import HelpIcon from "assets/svg/Help.svg";
import percentText from "assets/svg/percentText.svg";
import carImage from "assets/svg/carImage.svg";
import blueHIcon from "assets/svg/blueHIcon.svg";
import bluecar from "assets/svg/bluecar.svg";
import bluefileicon from "assets/svg/bluefileicon.svg";
import { DiscountClaimCard } from "../DashBoardComponents/DiscountClaimCard";
import { DashBoardDownloadCard } from "../DashBoardComponents/DashBoardDownloadCard";
import PageTitleCard from "../DashBoardComponents/PageTitleCard";
import { NotificationStatusUpdateCard } from "../DashBoardComponents/NotificationStatusUpdateCard";
import { NormalSearch } from "component/common/NormalSearch";
import map_car from "assets/images/mobile/map_car.svg";
import map_H from "assets/images/mobile/map_H.svg";
import Unionminiblue from "assets/svg/Union-mini-blue.svg";
import ChronicDisease from "assets/svg/ChronicDisease.svg";
import { DashboardServiceCards } from "../DashBoardComponents/DashboardServiceCards";
import orange from "assets/svg/orangeArrow.svg";
import { history } from "service/helpers";

const accidentClaimCardData = [
  {
    id: 1,
    mt: true,
    top: true,
    toplfticon: time,
    date: "02/06/2022",
    tag: "Reported",
    title:
      "We are sorry to hear that you had an accident. here is the details in case you needed to raise a claim",
    iconmain: _Vehicle,
    icona: file,
    iconb: calender,
    platenumber: "3576 TND",
    casenumber: "1234",
    accidentdate: "1/1/2022",
    iconbtmlft: questionmarkcircle,
    btmlftlabel: "Get Help",
    btnlabel: "Create Claim",
    btmsection: true,
    getHelpIcon: HelpIcon,
    viewDetailsComp: false,
    createClaimBtn: true,
    createClaimStandAlone: false,
  },
];

const DashBoardDownloadCardData = [
  {
    id: 1,
    title: "Get the most out of your car insurance",
    subTitle: "Save up to 20% on your car insurance renewal.",
    btnTitle: "Dowload Tawuniya Drive",
    imagea: percentText,
    imageb: carImage,
  },
];

const NotificationStatusUpdateCardData = [
  {
    id: 1,
    toplfticon: time,
    date: "30 Minutes Ago",
    requestandclaim: "Your Car Wash Request ",
    randcIcon: bluefileicon,
    message: "Has been updated.",
    serviceIcon: bluecar,
    service: "Car Wash",
    customer: "Merc. Benz  - 3576 TND",
    btnlabel: "Compeleted",
  },
  {
    id: 2,
    toplfticon: time,
    date: "30 Minutes Ago",
    requestandclaim: "Your Medical Approval Request ",
    randcIcon: bluefileicon,
    message: "Has been updated.",
    serviceIcon: blueHIcon,
    service: "CBC Blood Test - Mouasat Hospital",
    customer: "Prashant Dixit Vinod dsdd",
    btnlabel: "Compeleted",
  },
  {
    id: 3,
    toplfticon: time,
    date: "30 Minutes Ago",
    requestandclaim: "Your Motor Claim  ",
    randcIcon: bluefileicon,
    message: "Has been updated.",
    serviceIcon: bluecar,
    service: "Comperhnsive",
    customer: "Merc. Benz  - 3576 TND",
    btnlabel: "Compeleted",
  },
];

const dashboardServicesCardData = [
  {
    id: 0,
    content: "Pregnancy Program",
    cardIcon: PregnancyProgram,
    class: "pr-1",
    url: "pregnancy-program",
  },
  {
    id: 1,
    content: "Request Telemedicine",
    cardIcon: RequestTelemedicine,
    class: "pr-1",
    url: "request-telemedicine",
  },
  {
    id: 2,
    content: "Chronic Disease Managemente",
    cardIcon: ChronicDisease,
    class: "pr-1",
    url: "chronic-disease-management",
  },
];

export const NotificationPage = () => {

  return (
    <React.Fragment>
      <div className="dashboard-notifications-page">
        <PageTitleCard count={3} title="Your Notifications" />
        <div className="row pb-4">
          <div className="col-lg-7 col-12 pr-0">
            <DiscountClaimCard />
            {accidentClaimCardData.map((item, id) => {
              return (
                <AccidentClaimCard
                  key={id}
                  mt={item.mt}
                  top={item.top}
                  toplfticon={item.toplfticon}
                  date={item.date}
                  tag={item.tag}
                  title={item.title}
                  iconmain={item.iconmain}
                  icona={item.icona}
                  iconb={item.iconb}
                  platenumber={item.platenumber}
                  casenumber={item.casenumber}
                  accidentdate={item.accidentdate}
                  btnlabel={item.btnlabel}
                  discount={false}
                  btmsection={item.btmsection}
                  getHelpIcon={item.getHelpIcon}
                  viewDetailsComp={item.viewDetailsComp}
                  createClaimBtn={item.createClaimBtn}
                  createClaimStandAlone={item.createClaimStandAlone}
                />
              );
            })}
            {DashBoardDownloadCardData.map((data, id) => {
              return (
                <DashBoardDownloadCard
                  key={id}
                  title={data.title}
                  subTitle={data.subTitle}
                  btnTitle={data.btnTitle}
                  imagea={data.imagea}
                  imageb={data.imageb}
                />
              );
            })}
            {NotificationStatusUpdateCardData.map((item, id) => {
              return (
                <NotificationStatusUpdateCard
                  toplfticon={item.toplfticon}
                  date={item.date}
                  requestandclaim={item.requestandclaim}
                  randcIcon={item.randcIcon}
                  id={item.id}
                  message={item.message}
                  serviceIcon={item.serviceIcon}
                  service={item.service}
                  customer={item.customer}
                  btnlabel={item.btnlabel}
                />
              );
            })}
          </div>
          <div className="col-lg-5 col-12">
            <div className="iframe-map-policies position-relative">
              <iframe
                height={"240px"}
                width="95%"
                frameborder="0"
                scrolling="no"
                src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3333.967410377096!2d-111.89998968453055!3d33.31966746342457!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x0!2zMzPCsDE5JzEwLjgiTiAxMTHCsDUzJzUyLjEiVw!5e0!3m2!1sen!2sus!4v1516690469899"
              />

              <form class="dashboard-map-form row align-items-center input-width pt-0 map-search">
                <div className="pr-lg-1">
                  <NormalSearch
                    className="header-Search"
                    name="search"
                    placeholder="What you're looking for?"
                    needRightIcon={true}
                    searchAligner="alignIframeSearchIcon"
                  />
                </div>
                <ul>
                  <li>
                    <img src={map_H} height="20" alt="Chat" />
                    <span>Hospitals</span>
                  </li>
                  <li>
                    <img src={map_car} height="20" alt="Chat" />
                    <span>Workshops</span>
                  </li>
                  <li>
                    <img src={Unionminiblue} height="20" alt="Chat" />
                    <span>Our Offices</span>
                  </li>
                </ul>
              </form>
            </div>
            <div className="w-100">
              <p className="fs-20 fw-800 pt-4 mb-0 dashboard-notifications-services">
                Your Services
              </p>
              <DashboardServiceCards
                dashboardServiceCardData={dashboardServicesCardData}
                isfullWidth="col-12"
                textWidth="dashNotificationsTextWidth"
              />
              <div
                className="pt-2 cursor-pointer"
                onClick={() => history.push("/dashboard/service")}
              >
                <span className="fs-14 fw-800 text-uppercase viewInteractionLink">
                  View all services
                </span>{" "}
                <img
                  src={orange}
                  className="img-fluid orange-arrow-services"
                  alt="icon"
                />
              </div>
            </div>
          </div>

          <div className="col-lg-6 col-12"></div>
          <div className="col-lg-6 col-12 pt-4"></div>
        </div>
        <div className="home-dashBoard-liner mb-3"></div>
      </div>
    </React.Fragment>
  );
};
