import React from "react";
import { history } from "service/helpers";
import { RequestPage } from "component/common/DashBoardLandingPage";
import { carMaintainRequestHeaderData } from "component/common/MockData/NewMockData";
import { NormalSelect } from "component/common/NormalSelect";
import { NormalButton } from "component/common/NormalButton";
import selectArrow from "assets/svg/complaintSelectDropdown.svg";
import "./style.scss";

export const PeriodicInspectRequestPage = () => {
	const urlName = "1";
	return (
		<RequestPage requestHeaderData={carMaintainRequestHeaderData}>
			<div className="row inspect-ParentContainer">
				<div className="col-lg-12 col-12">
					<p className="inspect-requestMap-title fs-20 fw-800 pt-3">
						Request Details
					</p>
					<div className="inspect-MapContainer p-4">
						<div className="pb-3">
							<iframe
								height="540px"
								width="100%"
								frameborder="0"
								scrolling="no"
								src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3333.967410377096!2d-111.89998968453055!3d33.31966746342457!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x0!2zMzPCsDE5JzEwLjgiTiAxMTHCsDUzJzUyLjEiVw!5e0!3m2!1sen!2sus!4v1516690469899"
							/>
						</div>
						<div>
							<NormalSelect
								className="inspect-SelectInput"
								arrowVerticalAlign="inspect-ArrowAlign"
								placeholder="Select your vehicle"
								paddingLeft="10px"
								selectArrow={selectArrow}
								selectFontWeight="400"
								phColor="#455560"
								fontSize="16px"
							/>
						</div>
					</div>
					<div className="col-lg-12 col-12 p-0">
						<div className="inspect-Selector-liner pt-2 pb-3"></div>
					</div>
					<div className="pt-4">
						<NormalButton
							label="Submit Periodic Inspection Request"
							className="submit-inspect-Button p-3"
							onClick={() =>
								history.push(
									`/dashboard/service/periodic-inspection/request-confirmation/${urlName}`
								)
							}
						/>
					</div>
				</div>
			</div>
		</RequestPage>
	);
};
