import React from "react";
import { DashBoardLandingPage } from "component/common/DashBoardLandingPage";
import {
  dashboardPeriodicInspectHeaderData,
  dashboardPeriodicInspectionProgressData,
  faqPeriodicInspectDashboardData,
  periodicInspectContentData,
} from "component/common/MockData";
import "./style.scss";

export const HomePeriodicInspection = () => {
  return (
    <div className="periodicInspect-LandingContainer">
      <DashBoardLandingPage
        dashboardHeaderData={dashboardPeriodicInspectHeaderData}
        dashboardProgressData={dashboardPeriodicInspectionProgressData}
        faqDashboardData={faqPeriodicInspectDashboardData}
        dashBoardContentData={periodicInspectContentData}
      />
    </div>
  );
};
