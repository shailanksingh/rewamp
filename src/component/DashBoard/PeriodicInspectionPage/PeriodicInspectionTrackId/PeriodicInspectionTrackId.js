import React from "react";
import { RequestIdPage } from "component/common/DashBoardLandingPage";
import { periodicInspectionRequestIdData } from "component/common/MockData/NewMockData";

export const PeriodicInspectionTrackId = () => {
	return (
		<div>
			<RequestIdPage requestIdData={periodicInspectionRequestIdData} />
		</div>
	);
};
