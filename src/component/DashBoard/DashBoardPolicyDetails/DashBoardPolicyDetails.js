import React, { useEffect, useState, useRef } from "react";
import "./style.scss";
import { history } from "service/helpers";
import { NormalSearch } from "component/common/NormalSearch";
import map_car from "assets/images/mobile/map_car.svg";
import map_H from "assets/images/mobile/map_H.svg";
import Unionminiblue from "assets/svg/Union-mini-blue.svg";
import { DashboardMap } from "../DashBoardComponents/DashBoardMap";
import { ZeroInteractions } from "../DashBoardComponents/ZeroInteractions";
import PolicyCard from "component/common/MobileReuseable/PolicyCard";
import ShareIcon from "assets/svg/share-icon.svg";
import AppleWalletIcon from "assets/svg/apple-wallet.svg";
import QuickActions from "../DashBoardComponents/QuickActions";
import TravelPolicyCard from "component/common/MobileReuseable/PolicyCard/TravelPolicyCard/TravelPolicyCard";
import MotorPolicyCard from "component/common/MobileReuseable/PolicyCard/MotorPolicyCard/MotorPolicyCard";
import _Vehicle from "assets/svg/_Vehicle.svg";
import file from "assets/svg/file.svg";
import calender from "assets/svg/Calendar.svg";
import { AccidentClaimCard } from "../DashBoardComponents/AccidentClaimCard";
import CarGreyIcon from "assets/svg/car-grey.svg";
import { RecentPolicyCard } from "../DashBoardComponents/RecentPolicyCard";
import viewInteract from "assets/svg/viewInteractIcon.svg";
import AlertCard from "../DashBoardComponents/AlertCard";
import { DashboardServiceCards } from "../DashBoardComponents/DashboardServiceCards";
import PregnancyProgram from "assets/svg/PregnancyProgram.svg";
import RequestTelemedicine from "assets/svg/RequestTelemedicine.svg";
import MedicalReimbursement from "assets/svg/MedicalReimbursement.svg";
import roadAssisst from "assets/svg/dashBoardRoadAssisst.svg";
import claim from "assets/svg/dashBoardClaim.svg";
import inspect from "assets/svg/dashBoardInspection.svg";
import Slider from "react-slick";

const accidentClaimCardData = [
	{
		id: 1,
		// title:
		// 	"We are sorry to hear that you had an accident. here is the details in case you needed to raise a claim",
		iconmain: _Vehicle,
		icona: file,
		iconb: calender,
		platenumber: "3576 TND",
		casenumber: "1234",
		// accidentdate: "1/1/2022",
		btnlabel: "Create Claim",
		createClaimStandAlone: true,
	},
];

const recentPloicyCardData = [
	{
		id: 1,
		TRequestDate: "02/06/2022",
		SStatus: "Completed",
		policyIcon: CarGreyIcon,
		SProviderName: "Refill Medication",
		SMemberName: "Prashant Dixit",
		fileName: "17364427"
	},
];

const dashboardServicesCardData = [
	{
		id: 0,
		content: "Pregnancy Program",
		cardIcon: PregnancyProgram,
		class: "pr-1 col-sm-2",
		url: "pregnancy-program",
	},
	{
		id: 1,
		content: "Request Telemedicine",
		cardIcon: RequestTelemedicine,
		class: "pr-1 col-sm-2",
		url: "request-telemedicine",
	},
	{
		id: 2,
		content: "Medical Reimbursement",
		cardIcon: MedicalReimbursement,
		class: "pr-1 col-sm-2",
		url: "medical-reimbursment",
	},
	{
		id: 3,
		content: "Road Side Assistance",
		cardIcon: roadAssisst,
		class: "pr-1 col-sm-2",
		url: "road-assistance",
	},
	{
		id: 4,
		content: "Claim Assistance",
		cardIcon: claim,
		class: "pr-1 col-sm-2",
		url: "claim-assistance",
	},
	{
		id: 5,
		content: "Periodic Inspection",
		cardIcon: inspect,
		class: "pr-1 col-sm-2",
		url: "periodic-inspection",
	},
];

export const DashBoardPolicyDetails = () => {
	let [ currentPage, setCurrentPage ] = useState();
	const [discount, setDiscount] = useState(false);
	useEffect(() => {
		let pathname = history.location.pathname; 
		let pathnameArray = pathname.split("/");
		setCurrentPage(pathnameArray[pathnameArray.length - 1]);
	}, []);
	const sliderRef = useRef(null);
	const settings = {
		slidesToShow: 1,
		slidesToScroll: 1,
		arrows: false,
		infinite: false,
		dots: true,
		speed: 500,
	};
	const discountHandler = () => {
		setDiscount(true);
	};
	return (
		<React.Fragment>
			<div className="row pt-4 dashboard-policy-details-page">
				<div className="col-12">
					<div className="alert-card-root">
						<AlertCard 
							icon="warning"
							title="CCHI Upload Faild"
							description="The addition of Youssef, Your son has been rejected by CCHI “Daman”. You can check the issue and re-add him. a gentel action is required by your company HR department. "
						/>
					</div>
				</div>
				<div className="col-lg-6 col-12 mb-4">
					{(currentPage === "health" || currentPage === "travel") && <>
						{recentPloicyCardData?.length > 0 ? <>
							<div className="recent-policy-card-root">
								<div className="requests-content-title-policy">Recent Interactions</div>
								{recentPloicyCardData.map((data, id) => {
									return (
										<RecentPolicyCard
											// time={data.time}
											// progressLabel={data.progressLabel}
											// policyIcon={data.policyIcon}
											// title={data.title}
											// subtitle={data.subtitle}
											// getHelpIcon={data.getHelpIcon}
											data={data}
											greyArrow={true}
											chatSmileIcon={false}
											greyBg={true}
										/>
									);
								})}
								<div>
									<div className="view-more-link-policy">View More Interactions <span>+</span></div>
								</div>
							</div> 
						</> :
							<ZeroInteractions />
						}
					</>}
					{(currentPage === "motor") && <>
						{accidentClaimCardData?.length > 0 ? <>
							{accidentClaimCardData.map((item, id) => {
								return (
									<AccidentClaimCard
										key={id}
										iconmain={item.iconmain}
										icona={item.icona}
										iconb={item.iconb}
										platenumber={item.platenumber}
										casenumber={item.casenumber}
										btnlabel={item.btnlabel}
										discount={discount}
										discountHandler={discountHandler}
										createClaimStandAlone={item.createClaimStandAlone}
									/>
								);
							})}
						</>: <ZeroInteractions />}
						
					</>}
					<DashboardMap />
				</div>
				<div className="col-lg-6 col-12 mb-4">
					<div className={"dashboard_new_main_container_details"}>
						<div className="dashboard_card_container">
							<div className={" policy_list_view"}>
								<div className="requests-content-title-policy">Your Policy Details</div>
								{currentPage === "health" && <>
									<PolicyCard pageName="your-policies" />
									{/* <Slider
										ref={sliderRef}
										{...settings}
										className="dashboard-your-policydetails-slider "
									>
										<PolicyCard pageName="your-policies" />
										<PolicyCard pageName="your-policies" />
									</Slider> */}
								</>}
								{currentPage === "motor" && <>
									<MotorPolicyCard
										isShrink={false}
									/>
								</>}
								{currentPage === "travel" && <>
									<TravelPolicyCard
										isShrink={false}
									/>
								</>}
								<div className="policy-card-btns">
									<div className="left-btn">
										<a href="#">
											<img src={ShareIcon} alt="..." /> Save or Share
										</a>
									</div>
									<div className="right-btn">
										<button>
											<img src={AppleWalletIcon} alt="..." /> <div><span>Add to</span> <br />Apple Wallet</div>
										</button>
									</div>
								</div>
							</div>
						</div>
					</div>
					<QuickActions 
						currentPage={currentPage}
					/>
				</div>

				{/* <div className="row">
					<div className="col-lg-6 col-12 pr-0"></div>
				</div> */}

				{/* <div className="row">
					<div className="col-lg-6 col-12">
						
					</div>
					<div className="col-lg-6 col-12"></div>
				</div> */}
				<div className="col-12">
					<div className="your-services-row-policy">
						<div className="">
							<div className="requests-content-title-policy">Your Services</div>
							<DashboardServiceCards
								dashboardServiceCardData={dashboardServicesCardData}
								textWidth="insureCardTextWidth"
								isfullWidth="col-4"
							/>
							<div className="view-more-link-policy">View More Services <span>+</span></div>
						</div>
					</div>
				</div>
			</div>

		</React.Fragment>
	);
};
