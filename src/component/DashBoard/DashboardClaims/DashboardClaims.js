import React from "react";
import "./style.scss";
import PageTitleCard from "../DashBoardComponents/PageTitleCard";
import {RecentPolicyCard} from "../DashBoardComponents/RecentPolicyCard";
import chatSmile from "assets/svg/Chat-smile.svg";
import CarGreyIcon from "assets/svg/car-grey.svg";
import FileIcon from "assets/images/menuicons/file-orange.svg";
import CreditCardIcon from "assets/images/menuicons/credit-card-orange.svg";
import DocumentIcon from "assets/images/menuicons/document-orange.svg";
import {NavLink} from "react-router-dom";
import RightArrowIcon from "assets/images/menuicons/right-arrow.svg";

const recentPloicyCardData = [
  {
    id: 1,
    time: "30 Minutes Ago",
    progressLabel: "In progress",
    policyIcon: CarGreyIcon,
    title: "Almouasat Hospital",
    subtitle: "Brain MRI Approval.",
    getHelpIcon: chatSmile,
  },
  {
    id: 2,
    time: "30 Minutes Ago",
    progressLabel: "In progress",
    policyIcon: CarGreyIcon,
    title: "Almouasat Hospital",
    subtitle: "Brain MRI Approval.",
    getHelpIcon: chatSmile,
  },
];

const leftMenulist = [{
  title: "Motor",
  url: "#"
}, {
  title: "Medical",
  url: "#"
}, {
  title: "Property & Casuality",
  url: "#"
}, {
  title: "Travel",
  url: "#"
}, {
  title: "Medical Malpractice",
  url: "#"
}];

export const DashboardClaims = () => {
  return (
    <div className="dashboard-claims-page">
      <PageTitleCard count={3} title="Your Claims"/>
      <div className="row top-row-content">
        <div className="col-7">
          {recentPloicyCardData.map((data) => {
            return (
              <RecentPolicyCard
                time={data.time}
                progressLabel={data.progressLabel}
                policyIcon={data.policyIcon}
                key={data.title}
                title={data.title}
                subtitle={data.subtitle}
                getHelpIcon={data.getHelpIcon}
              />
            );
          })}
          <div className="view-more-link">View More Claims <span>+</span></div>
        </div>
        <div className="col-5">
          <div className="form-card">
            <div className="small-title-claims">Track more claims</div>
            <div className="menu-section-form">
              <div className="input-div">
                <img src={FileIcon} alt="..."/>
                <input type="text" placeholder="Policy No	"/>
              </div>
              <div className="input-div">
                <img src={CreditCardIcon} alt="..."/>
                <input type="text" placeholder="National ID or Iqama"/>
              </div>
              <div className="input-div">
                <img src={DocumentIcon} alt="..."/>
                <input type="text" placeholder="Claim Number"/>
              </div>
              <div className="form-action">
                <button>Continue</button>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="submit-a-claim-section-root">
        <div className="submit-a-claim-section">
          <div className="section-left">
            <div className="small-title-claims">Submit a Claim</div>
            {leftMenulist?.map(({title, url}, index) => {
              return (
                <div key={index.toString()} className="nav-link-root">
                  <NavLink to={url} exact>
                    {title}
                    <img src={RightArrowIcon} className="orange-color" alt="..."/>
                  </NavLink>
                </div>
              );
            })}
          </div>
          <div className="section-right">
            <div className="submit-section-box">
              <div className="title">Individuals</div>
              <div className="section-list">
                <div className="subtitle">Alshamel & Sanad Plus</div>
                <div className="description">Vehicles Comprehensive Insurance for Corporate</div>
              </div>
              <div className="section-list">
                <div className="subtitle">Sanad</div>
                <div className="description">Third Party Insurance for Corporate</div>
              </div>
            </div>
            <div className="submit-section-box">
              <div className="title">SMEs</div>
              <div className="section-list">
                <div className="subtitle">Alshamel & Sanad Plus</div>
                <div className="description">Medcial Insurance for Corporates</div>
              </div>
              <div className="section-list">
                <div className="subtitle">Sanad</div>
                <div className="description">Program for Employees Collaborative Insurance</div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
