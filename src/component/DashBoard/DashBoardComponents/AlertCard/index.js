import React from "react";
import "./style.scss";
import WarningIcon from "assets/svg/warning.svg";

const AlertCard = ({
    icon = "warning",
    title,
    description
}) => {
    return (
        <div className="dashboard-pages-alert-card">
            {icon === "warning" && <img src={WarningIcon} alt="..." />}
            <div>
                <div className="alert-card-title">{title}</div>
                <p>{description}</p>
            </div>
        </div>
    );
}

export default AlertCard