import { NormalButton } from "component/common/NormalButton";
import React from "react";
import "./style.scss";

export const DashBoardDownloadCard = (props) => {
	let { title, subTitle, btnTitle, imagea, imageb } = props;
	return (
		<React.Fragment>
			<div className="dash-download-card ddc w-100 d-flex justify-content-between p-4 mt-3">
				<div className="ddc-text">
					<h5>{title}</h5>
					<p>{subTitle}</p>
					<div>
						<NormalButton label={btnTitle} className="ddc-button" />
					</div>
				</div>
				<div className="ddc-lft-img">
					<img className="imagea" src={imagea} alt="" />
					<img className="imageb" src={imageb} alt="" />
				</div>
			</div>
		</React.Fragment>
	);
};
