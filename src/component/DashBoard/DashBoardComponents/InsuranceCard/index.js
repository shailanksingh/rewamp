import React from "react";
import "./style.scss";
import RightArrowIcon from "assets/images/right-arrow-white-circle.svg";
import MotorIcon from "assets/images/motor-icon.svg";
import HealthIcon from "assets/images/health-icon.svg";
import PropertyIcon from "assets/images/property-icon.svg";
import MotorBgImage from "assets/images/motor-bg-image.png";
import HealthBgImage from "assets/images/health-bg-image.png";
import PropertyBgImage from "assets/images/property-bg-image.png";
import RightArrowOrangeIcon from "assets/images/menuicons/right-arrow.svg";

const contentDetails = {
    Motor: {
        tag: {
            icon: MotorIcon,
            text: "Motor"
        },
        title: "Motor Insurance",
        description: "Sanad for Private Motor Vehicle Liability Insurance ",
        image: MotorBgImage,
    },
    Health: {
        tag: {
            icon: HealthIcon,
            text: "Health"
        },
        title: "Health Insurance",
        description: "Sanad for Private Motor Vehicle Liability Insurance ",
        image: HealthBgImage,
    },
    Property: {
        tag: {
            icon: PropertyIcon,
            text: "Property & Casualty"
        },
        title: "Property & Casualty Insurance",
        description: "Sanad for Private Motor Vehicle Liability Insurance ",
        image: PropertyBgImage,
    },
};

const InsuranceCard = ({ type, cardContent }) => {
    let cardDetails;
    if (type) {
        cardDetails = contentDetails[type];
    };
    return (
        <div className="dashboard-products-and-offerings-insurance-card">
            {cardDetails &&
                <div 
                    className="card-with-background"
                    style={{ 
                        backgroundImage: "url(" + cardDetails?.image + ")",
                    }}
                >
                    <div className="top-content">
                        <img src={cardDetails?.tag?.icon} alt="..." />
                        <span>{cardDetails?.tag?.text}</span>
                    </div>
                    <div className="bottom-content">
                        <div>
                            <div className="insurance-card-title">{cardDetails?.title}</div>
                            <div className="insurance-card-description">{cardDetails?.description}</div>
                        </div>
                        <div>
                            <img src={RightArrowIcon} alt="..." />
                        </div>
                    </div>
                </div>
            }
            {cardContent && 
                <div className="card-with-white-bg">
                    <div className="top-content">
                        {cardContent?.tag && 
                            <div className="tag">
                                <img src={cardContent?.tag?.icon} alt="..." />
                                <span>{cardContent?.tag?.text}</span>
                            </div>
                        }
                        <div className="white-card-title">
                            {cardContent?.title}
                        </div>
                        <div className="white-card-subtitle">
                            {cardContent?.subtitle}
                        </div>
                        <div className="white-card-description">
                            {cardContent?.description}
                        </div>
                    </div>
                    <div className="bottom-content">
                        <div className="explore-more">
                            Explore More <img src={RightArrowOrangeIcon} alt="..." />
                        </div>
                        <div className="get-quote">
                            <button>Get Quote</button>
                        </div>
                    </div>
                </div>
            }
        </div>
    );
}

export default InsuranceCard;