import { NormalButton } from "component/common/NormalButton";
import React from "react";
import "./style.scss";

export const NotificationStatusUpdateCard = (props) => {
	let {
		toplfticon,
		date,
		requestandclaim,
		randcIcon,
		id,
		message,
		serviceIcon,
		service,
		customer,
		btnlabel,
	} = props;
	return (
		<React.Fragment>
			<div className="notification-status-update n-s-u-container mt-3">
				<div className="n-s-u-timeline d-flex justify-content-between w-100">
					<span className="d-flex align-items-center">
						<img src={toplfticon} alt="" className="mr-2" />
						<p className="fs-12 m-0 ">{date}</p>
					</span>
					{/* <span className="fs-12 a-c-c-tag">{props.tag}</span> */}
				</div>
				<div className="n-s-u-details py-3 d-flex">
					<p className="fs-12 mb-0 mr-2">{requestandclaim}</p>
					<img className="mr-2" src={randcIcon} alt="" />
					<span className="fs-12 mr-2">{id}</span>
					<p className="fs-12 mb-0">{message}</p>
				</div>
				<div className="n-s-u-status justify-content-start d-flex align-items-center">
					<div className="col-9 d-flex p-0">
						<img className="n-s-u-serviceIcon mr-2" src={serviceIcon} alt="" />
						<p className="fs-12">{service}</p>
						<p className="fs-12">{customer}</p>
					</div>
					<div>
						<NormalButton label={btnlabel} className="n-s-u-button" />
					</div>
				</div>
			</div>
		</React.Fragment>
	);
};
