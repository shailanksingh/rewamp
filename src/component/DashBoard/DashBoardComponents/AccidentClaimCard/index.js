import React from "react";
import { NormalButton } from "component/common/NormalButton";
import discountClaim from "assets/svg/DiscountClaimer.svg";
import discountCar from "assets/svg/vehicleimgCar.svg";
import "./style.scss";
import viewDetails from "assets/svg/viewDetailsIcon.svg";
import moment from "moment";
import viewDetailsGrey from "assets/svg/viewDetailsIconGrey.svg";

export const AccidentClaimCard = (props) => {
  let {
    discount,
    discountHandler,
    btmsection,
    details,
    viewDetailsComp,
    createClaimBtn,
    createClaimStandAlone,
  } = props;
  return (
    <React.Fragment>
      {discount ? (
        <div className="discountClaimCard pt-1">
          <div className="d-flex flex-row">
            <div className="pt-2">
              <img
                src={discountClaim}
                className="img-fluid discountIcon "
                alt="icon"
              />
            </div>
            <div className="alignDiscount-Tag pt-3">
              <p className="fs-14 fw-700 renewDiscountTxt m-0">
                Renuew your Purchase of Peugeot and get{" "}
                <span className="percentageRenewDiscount">20% Discount!</span>{" "}
              </p>
            </div>
          </div>
          <div className="align-discount-cardLayout pb-2">
            <div className="discountProductBox">
              <div className="d-flex justify-content-between">
                <div>
                  <img src={discountCar} className="img-fluid" alt="icon" />
                  <p className="fs-12 fw-400 car-Series pt-1 m-0">
                    {" "}
                    <span className="car-Variety">Peugeot</span> - 3008 Allure
                  </p>
                </div>
                <div className="plateContainerLayout">
                  {/* <img src={plateNo} className="img-fluid" alt="icon" /> */}

                  <div className="plateContainer">
                    <div className="d-flex flex-row">
                      <div className="col-5 plateSubLayerOne py-1">
                        <p className="m-0 fs-9 fw-700 plateText">٣٥٧٦</p>
                      </div>
                      <div className="col-5 plateSubLayerTwo py-1">
                        <p className="m-0 fs-9 fw-700 plateText">د ن ط</p>
                      </div>
                      <div className="plateSubLayerFive">
                        <p className="m-0 fs-6 fw-700 plateText pt-1">K</p>
                        <p className="m-0 fs-6 fw-700 plateText">S</p>
                        <p className="m-0 fs-6 fw-700 plateText">A</p>
                        <div className="dark-point mt-1"></div>
                      </div>
                    </div>
                    <div className="d-flex flex-row">
                      <div className="col-5 plateSubLayerThree py-1">
                        <p className="m-0 fs-9 fw-700 plateText">3576</p>
                      </div>
                      <div className="col-5 plateSubLayerFour py-1">
                        <p className="m-0 fs-9 fw-700 plateText">T N D</p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="pt-3 px-3 mx-1">
              <NormalButton
                label="Get 20% Discount Now"
                className="get-Discount-Now-Btn"
              />
            </div>
          </div>
        </div>
      ) : (
        <div
          className={
            props.mt ? "accident-claim-card mt-3" : "accident-claim-card"
          }
        >
          {props.top ? (
            <div className="a-c-c-details-timeline d-flex justify-content-between w-100">
              <span className="d-flex align-items-center">
                <img src={props.toplfticon} alt="" className="mr-2" />
                <p className="fs-12">{props.date}</p>
              </span>
              <span className="fs-12 a-c-c-tag">{props.tag}</span>
            </div>
          ) : (
            ""
          )}
          {props.title && <p>{props.title}</p>}
          <div className="a-c-c-details">
            <div className="a-c-c-detailstxt">
              <img
                src={props.iconmain}
                className="a-c-c-details-img"
                alt="car logo"
              />
              <div className="vertical-line"></div>
              <div className="a-c-c-detailstxtcontainer">
                <span>Plate Number</span>
                <p>{props?.platenumber}</p>
              </div>
            </div>
            <div className="a-c-c-detailstxt">
              <div className="vertical-line"></div>
              <img src={props.icona} className="a-c-c-details-icon" alt="" />
              <div className="a-c-c-detailstxtcontainer">
                <span>Case Number</span>
                <p>{props?.casenumber}</p>
              </div>
            </div>
            {props?.accidentdate && (
              <div className="a-c-c-detailstxt">
                <div className="vertical-line"></div>
                <img src={props.iconb} className="a-c-c-details-icon" alt="" />
                <div className="a-c-c-detailstxtcontainer">
                  <span>Accident date</span>
                  <p>{moment(props?.accidentdate).format("DD-MM-YYYY")}</p>
                </div>
              </div>
            )}
          </div>
          {btmsection ? (
            <div className="d-flex justify-content-between pt-1 w-100">
              <div>
                <div className="d-flex flex-column">
                  <div className="pb-3">
                    <img
                      src={props.getHelpIcon}
                      className="img-fluid pr-2"
                      alt="icon"
                    />{" "}
                    <span className="fs-12 fw-400 getHelpTxt">Get Help</span>
                  </div>
                </div>
              </div>
              {viewDetailsComp ? (
                <div>
                  <span className="fs-12 fw-400 vieDetailsTxt pr-2">
                    View Details
                  </span>{" "}
                  {props.greyArrow ? (
                    <img
                      src={viewDetailsGrey}
                      className="img-fluid"
                      alt="icon"
                    />
                  ) : (
                    <img src={viewDetails} className="img-fluid" alt="icon" />
                  )}
                </div>
              ) : (
                ""
              )}
              {createClaimBtn ? (
                <NormalButton
                  label={props.btnlabel}
                  className="a-c-c-button"
                  onClick={discountHandler}
                />
              ) : (
                ""
              )}
            </div>
          ) : (
            ""
          )}
          {createClaimStandAlone ? (
            <NormalButton
              label={props.btnlabel}
              className="a-c-c-button"
              onClick={discountHandler}
            />
          ) : (
            ""
          )}
        </div>
      )}
    </React.Fragment>
  );
};
