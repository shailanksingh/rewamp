import React from "react";
import { NormalButton } from "component/common/NormalButton";
import discountClaim from "assets/svg/DiscountClaimer.svg";
import discountCar from "assets/svg/discountVehicle.svg";
import plateNo from "assets/svg/Plate Number.svg";
import "./style.scss";
import viewDetails from "assets/svg/viewDetailsIcon.svg";
import viewDetailsGrey from "assets/svg/viewDetailsIconGrey.svg";

export const RenewalClaimCard = (props) => {
  let { discount, discountHandler, details } = props;
  return (
    <React.Fragment>
      <div className="discountClaimCard pt-1">
        <div className="d-flex flex-row">
          <div className="pt-2">
            <img
              src={discountClaim}
              className="img-fluid discountIcon "
              alt="icon"
            />
          </div>
          <div className="alignDiscount-Tag pt-3">
            <p className="fs-14 fw-700 renewDiscountTxt m-0">
              Renuew your Purchase of Peugeot and get{" "}
              <span className="percentageRenewDiscount">20% Discount!</span>{" "}
            </p>
          </div>
        </div>
        <div className="align-discount-cardLayout pb-2">
          <div className="discountProductBox">
            <div className="d-flex justify-content-between">
              <div>
                <img src={discountCar} className="img-fluid" alt="icon" />
                <p className="fs-12 fw-400 car-Series pt-1 m-0">
                  {" "}
                  <span className="car-Variety">Peugeot</span> -{" "}
                  {details?.renewalPolicyDetail?.policyList?.policyListArray[0]}
                </p>
              </div>
              <div className="plateContainerLayout">
                {/* <img src={plateNo} className="img-fluid" alt="icon" /> */}

                <div className="plateContainer">
                  <div className="d-flex flex-row">
                    <div className="col-5 plateSubLayerOne py-1">
                      <p className="m-0 fs-9 fw-700 plateText">٣٥٧٦</p>
                    </div>
                    <div className="col-5 plateSubLayerTwo py-1">
                      <p className="m-0 fs-9 fw-700 plateText">د ن ط</p>
                    </div>
                    <div className="plateSubLayerFive">
                      <p className="m-0 fs-6 fw-700 plateText pt-1">K</p>
                      <p className="m-0 fs-6 fw-700 plateText">S</p>
                      <p className="m-0 fs-6 fw-700 plateText">A</p>
                      <div className="dark-point mt-1"></div>
                    </div>
                  </div>
                  <div className="d-flex flex-row">
                    <div className="col-5 plateSubLayerThree py-1">
                      <p className="m-0 fs-9 fw-700 plateText">3576</p>
                    </div>
                    <div className="col-5 plateSubLayerFour py-1">
                      <p className="m-0 fs-9 fw-700 plateText">T N D</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="pt-3 px-3 mx-1">
            <NormalButton
              label="Get 20% Discount Now"
              className="get-Discount-Now-Btn"
            />
          </div>
        </div>
      </div>
    </React.Fragment>
  );
};
