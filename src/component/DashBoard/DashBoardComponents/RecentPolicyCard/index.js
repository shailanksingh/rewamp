import React from "react";
import "./style.scss";
import timer from "assets/svg/recentTimer.svg";
import viewDetails from "assets/svg/viewDetailsIcon.svg";
import viewDetailsGrey from "assets/svg/viewDetailsIconGrey.svg";
import hospitalicon from "assets/svg/hospitalicon.svg";
import chatSmile from "assets/svg/Chat-smile.svg";
import HelpIcon from "assets/svg/Help.svg";
import DocumentIcon from "assets/svg/document-icon-grey.svg";

export const RecentPolicyCard = (props) => {
	var timeNow = props.data?.TRequestDate;
	let { chatSmileIcon = true } = props;
	return (
		<div className={"recentPolicyContainer" + (props?.greyBg ? " grey-bg" : "")}>
			<div className="d-flex justify-content-between">
				<span className="fs-10 fw-400 timerText">
					{" "}
					<img src={timer} className="img-fluid pr-1" alt="icon" /> {timeNow}
				</span>
				<div className={props.data?.fileName ? "status-with-file-name" : ""}>
					{props.data?.fileName && <span className="file-name fs-10 fw-400 ">
						<img src={DocumentIcon} alt="..." /> {props.data?.fileName}
					</span>}
					<p
						className={
							"fs-12 fw-400 rpc-progressLabel m-0 " +
							(props.data?.SStatus === "Completed" ? "completed" : "")
						}
					>
						{props.data?.SStatus}
					</p>
				</div>
			</div>
			<div className="rpc-sub-container">
				<div className="d-flex flex-row">
					<div className="pr-2 policyIconContainer">
						{props.data?.policyIcon ? 
							<img
								src={hospitalicon}
								className="img-fluid policyIcon"
								alt="icon"
							/> : 
							<img
								src={hospitalicon}
								className="img-fluid policyIcon"
								alt="icon"
							/>
						}
						
					</div>
					<div>
						<p className="fs-12 fw-400 policytitle m-0">
							{props.data?.SProviderName}
						</p>
						<p className="fs14 fw-500 rpc-userName m-0">
							{props.data?.SMemberName}
						</p>
					</div>
				</div>
			</div>
			<div className="d-flex justify-content-between pt-3">
				<div>
					<div className="d-flex flex-column">
						<div className="pb-3">
							{chatSmileIcon ? <img src={chatSmile} className="img-fluid pr-2" alt="icon" /> :
								<img src={HelpIcon} className="img-fluid pr-2" alt="icon" />
							}
							<span className="fs-12 fw-400 getHelpTxt">Get Help</span>
						</div>
					</div>
				</div>
				<div>
					<span className="fs-12 fw-400 vieDetailsTxt pr-2">View Details</span>{" "}
					{props.greyArrow ? (
						<img src={viewDetailsGrey} className="img-fluid" alt="icon" />
					) : (
						<img src={viewDetails} className="img-fluid" alt="icon" />
					)}
				</div>
			</div>
		</div>
	);
};
