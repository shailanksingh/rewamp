import React from "react";
import "./style.scss";

const PageTitleCard = ({ title, count }) => {
    return (
        <div className="dashboard-page-title-card">
            {title} {count !== undefined && <span>{count}</span>}
        </div>
    );
}

export default PageTitleCard;