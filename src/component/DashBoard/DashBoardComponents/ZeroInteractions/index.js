import React, { useState } from "react";
import confirmationwoman from "assets/svg/confirmationwoman.svg";

import "./style.scss";
export const ZeroInteractions = () => {
	return (
		<>
			<div className="zero-interactions">
				<img src={confirmationwoman} alt="Confirmation women" />
				<p>You do not have any interactions yet.</p>
			</div>
		</>
	);
};
