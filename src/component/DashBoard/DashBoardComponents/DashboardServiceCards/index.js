import React from "react";
import { history } from "service/helpers";
import insuranceArrow from "assets/svg/insuranceArrow.svg";
import gotoiconOffwhite from "assets/svg/gotoiconOffwhite.svg";
import "./style.scss";

export const DashboardServiceCards = ({
	dashboardServiceCardData,
	textWidth,
	isfullWidth,
	// onClick,
}) => {
	function serviceNavigator(url) {
		history.push(`/dashboard/service/${url}`);
	}

	return (
		<div className="row">
			{dashboardServiceCardData.map((item, index) => {
				return (
					<div className={`${item.class} mt-3 ${isfullWidth}`} key={index}>
						<div className="dashboard-ServiceCard" id={item.alignBannerCard}>
							<img
								src={item.cardIcon}
								className="img-fluid"
								id={item.contentAlign}
								alt="icon"
							/>
							<p
								className={`dashboard-Service-CardText fw-800  pt-2`}
								id={textWidth}
							>
								{item.content}
							</p>
							<img
								src={item.gotoicon ? gotoiconOffwhite : insuranceArrow}
								className="img-fluid pb-3"
								alt="icon"
								onClick={() => {
									serviceNavigator(item.url);
								}}
							/>
						</div>
					</div>
				);
			})}
		</div>
	);
};
