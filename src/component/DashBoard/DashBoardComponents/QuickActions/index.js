import React from "react";
import "./style.scss";
import CoverageBenefitsIcon from "assets/svg/coverage-benefits.svg";
import CoverageBenefitsIconOrange from "assets/svg/coverage-benefits-orange.svg";
import UsageIcon from "assets/svg/detailed-usage.svg";
import UsageIconOrange from "assets/svg/detailed-usage-orange.svg";
import HealthCareIcon from "assets/svg/healthcare.svg";
import DoctorIcon from "assets/svg/live-doctor.svg";
import CarIcon from "assets/svg/car-grey-outline.svg";
import CarOrangeIcon from "assets/svg/car-orange-outline.svg";
import PercentageIcon from "assets/svg/percentage.svg";

const QuickActions = ({ currentPage }) => {
    return (
        <div className="dashboard-page-quick-actions">
            <div className="content-title">Quick Actions</div>
            {currentPage && currentPage === "health" && <>
                <div className="quick-actions-list">
                    <div className="actions-list">
                        <img src={CoverageBenefitsIcon} alt="..." /> Your covrage & <br />Benifiets 
                    </div>
                    <div className="actions-list">
                        <img src={UsageIcon} alt="..." /> Your Detailed <br />Usage 
                    </div>
                    <div className="actions-list">
                        <img src={HealthCareIcon} alt="..." /> Find healthcare <br />provider
                    </div>
                    <div className="actions-list">
                        <img src={DoctorIcon} alt="..." /> Consult Live <br />doctor
                    </div>
                </div>
            </>}
            {currentPage && currentPage === "motor" && <>
                <div className="upgrade-section">
                    <div className="upgrade-section-title">
                        <img src={PercentageIcon} alt="..." /> <div>Get <span>20% Discount!</span> When you upgrade to Alshamel Now!</div>
                    </div>
                    <button>Upgrade To Alshamel Now!</button>
                </div>
                <div className="quick-actions-list">
                    <div className="actions-list justify-center">
                        <img src={CarIcon} alt="..." /> Locate Agency/workshops
                    </div>
                </div>
            </>}
            {currentPage && currentPage === "travel" && <>
                <div className="quick-actions-list">
                    <div className="actions-list">
                        <img src={CarOrangeIcon} alt="..." /> Locate Agency/ <br />workshops
                    </div>
                    <div className="actions-list">
                        <img src={CoverageBenefitsIconOrange} alt="..." /> Your covrage & <br />Benifiets 
                    </div>
                    <div className="actions-list justify-center">
                        <img src={UsageIconOrange} alt="..." /> Your Detailed <br />Usage 
                    </div>
                </div>
            </>}
        </div>
    );
}

export default QuickActions;