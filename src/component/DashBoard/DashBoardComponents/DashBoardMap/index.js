import React, { useState } from "react";
import { NormalSearch } from "component/common/NormalSearch";
import map_car from "assets/images/mobile/map_car.svg";
import map_H from "assets/images/mobile/map_H.svg";
import Unionminiblue from "assets/svg/Union-mini-blue.svg";
import "./style.scss";
export const DashboardMap = () => {
	return (
		<>
			<div className="iframe-map position-relative">
				<iframe
					height="370px"
					width="100%"
					frameborder="0"
					scrolling="no"
					style={{ marginLeft: "0" }}
					src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3333.967410377096!2d-111.89998968453055!3d33.31966746342457!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x0!2zMzPCsDE5JzEwLjgiTiAxMTHCsDUzJzUyLjEiVw!5e0!3m2!1sen!2sus!4v1516690469899"
				/>

				<form class="dashboard-map-form row align-items-center input-width pt-0 map-search">
					<div className="pr-lg-1">
						<NormalSearch
							className="headerSearch"
							name="search"
							placeholder="What you're looking for?"
							needRightIcon={true}
						/>
					</div>
					<ul>
						<li>
							<img src={map_H} height="20" alt="Chat" />
							<span>Hospitals</span>
						</li>
						<li>
							<img src={map_car} height="20" alt="Chat" />
							<span>Workshops</span>
						</li>
						<li>
							<img src={Unionminiblue} height="20" alt="Chat" />
							<span>Our Offices</span>
						</li>
					</ul>
				</form>
			</div>
		</>
	);
};
