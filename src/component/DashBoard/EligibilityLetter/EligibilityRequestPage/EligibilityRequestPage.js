import React, { useState } from "react";
import { history } from "service/helpers";
import { RequestRadioController } from "component/DashBoard/TeleMedicinePage/RequestDetailPage/RequestDetailComponents";
import { RequestPage } from "component/common/DashBoardLandingPage";
import { NormalSearch } from "component/common/NormalSearch";
import { eligibilityRequestHeaderData } from "component/common/MockData/NewMockData";
import { NormalButton } from "component/common/NormalButton";
import iquamaIcon from "assets/svg/iquamaIcon.svg";
import dateIcon from "assets/svg/datePickerIcon.svg";
import male from "assets/svg/maleGender.svg";
import female from "assets/svg/femaleGender.svg";
import "./style.scss";

export const EligibilityRequestPage = () => {
  const [toggler, setToggler] = useState(false);

  const togglerHandler = () => {
    setToggler(!toggler);
  };

  //initialiize state for radio buttons
  const [radioValue, setRadioValue] = useState("abdul");

  const [search, setSearch] = useState("");

  const searchRadioData = [
    {
      id: 0,
      radioName: "abdul",
      radioColumnOneLabel: "Member Name",
      radioColumnOneLabelContent: "Abdulrahman Fahad",
      radioColumnOneLabelImg: male,
      radioColumnTwoLabel: "Member Card",
      radioColumnTwoLabelContent: "001064190364001",
      radioColumnTwoLabelImg: iquamaIcon,
      radioColumnThreeLabel: "Date of Birth",
      radioColumnThreeLabelContent: "12 Jun 2022",
      radioColumnThreeLabelImg: dateIcon,
      showRadio: true,
    },
    {
      id: 1,
      radioName: "rana",
      radioColumnOneLabel: "Member Name",
      radioColumnOneLabelContent: "Rana Faisal",
      radioColumnOneLabelImg: female,
      radioColumnTwoLabel: "Member Card",
      radioColumnTwoLabelContent: "001064190364001",
      radioColumnTwoLabelImg: iquamaIcon,
      radioColumnThreeLabel: "Date of Birth",
      radioColumnThreeLabelContent: "12 Jun 2022",
      radioColumnThreeLabelImg: dateIcon,
      showRadio: true,
    },
    {
      id: 2,
      radioName: "turkey",
      radioColumnOneLabel: "Member Name",
      radioColumnOneLabelContent: "Turkey Abdullah",
      radioColumnOneLabelImg: male,
      radioColumnTwoLabel: "Member Card",
      radioColumnTwoLabelContent: "001064190364001",
      radioColumnTwoLabelImg: iquamaIcon,
      radioColumnThreeLabel: "Date of Birth",
      radioColumnThreeLabelContent: "12 Jun 2022",
      radioColumnThreeLabelImg: dateIcon,
      showRadio: true,
    },
    {
      id: 3,
      radioName: "Rahaf Abullah",
      radioColumnOneLabel: "Member Name",
      radioColumnOneLabelContent: "Rahaf Abullah",
      radioColumnOneLabelImg: female,
      radioColumnTwoLabel: "Member Card",
      radioColumnTwoLabelContent: "001064190364001",
      radioColumnTwoLabelImg: iquamaIcon,
      radioColumnThreeLabel: "Date of Birth",
      radioColumnThreeLabelContent: "12 Jun 2022",
      radioColumnThreeLabelImg: dateIcon,
      showRadio: toggler,
    },
    {
      id: 4,
      radioName: "Saud Faisal",
      radioColumnOneLabel: "Member Name",
      radioColumnOneLabelContent: "Saud Faisal",
      radioColumnOneLabelImg: male,
      radioColumnTwoLabel: "Member Card",
      radioColumnTwoLabelContent: "001064190364001",
      radioColumnTwoLabelImg: iquamaIcon,
      radioColumnThreeLabel: "Date of Birth",
      radioColumnThreeLabelContent: "12 Jun 2022",
      radioColumnThreeLabelImg: dateIcon,
      showRadio: toggler,
    },
  ];
  const urlName = "1";
  return (
    <RequestPage requestHeaderData={eligibilityRequestHeaderData}>
      <div className="row eligibility-ParentContainer">
        <div className="col-lg-12 col-12">
          <p className="eligibility-requestMap-title fs-20 fw-800 pt-3">
            Request Details
          </p>
          <p className="eligibility-member-titlename fs-16 fw-400">
            Member Name
          </p>
          <div className="eligibility-search-liner pb-4">
            <div className="eligibility-Search-box p-3">
              <NormalSearch
                className="eligibility-headerSearch"
                name="search"
                value={search}
                placeholder="Search"
                onChange={(e) => setSearch(e.target.value)}
                needRightIcon={true}
              />
              <RequestRadioController
                toggler={toggler}
                togglerHandler={togglerHandler}
                radioData={searchRadioData}
                radioValue={radioValue}
                setRadioValue={setRadioValue}
              />
            </div>
          </div>
          <p className="eligibility-ProviderName fs-20 fw-800 pt-2 mt-1">
            Provider
          </p>
          <div className="eligibility-MapContainer">
            <div className="eligibility-mapBoxLayout">
              <iframe
                title="eligibility-mapBoxLayout"
                height="540px"
                width="100%"
                frameborder="0"
                scrolling="no"
                src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3333.967410377096!2d-111.89998968453055!3d33.31966746342457!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x0!2zMzPCsDE5JzEwLjgiTiAxMTHCsDUzJzUyLjEiVw!5e0!3m2!1sen!2sus!4v1516690469899"
              />
            </div>
          </div>
          <div className="col-lg-12 col-12 p-0">
            <div className="eligibility-Selector-liner pt-2 pb-3"></div>
          </div>
          <div className="pt-4">
            <NormalButton
              label="Submit Eligibility letter Request"
              className="submit-eligibility-Button p-3"
              onClick={() =>
                history.push(
                  `/dashboard/service/eligibility-letter/request-confirmation/${urlName}`
                )
              }
            />
          </div>
        </div>
      </div>
    </RequestPage>
  );
};
