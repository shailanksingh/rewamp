import React from "react";
import { DashBoardLandingPage } from "component/common/DashBoardLandingPage";
import {
  dashboardEligibilityHeaderData,
  dashboardEligibilityProgressData,
  faqEligibilityDashboardData,
  eligibilityContentData,
} from "component/common/MockData";
import "./style.scss";

export const HomeEligibility = () => {
  return (
    <div className="eligibility-LandingContainer">
      <DashBoardLandingPage
        dashboardHeaderData={dashboardEligibilityHeaderData}
        dashboardProgressData={dashboardEligibilityProgressData}
        faqDashboardData={faqEligibilityDashboardData}
        dashBoardContentData={eligibilityContentData}
      />
    </div>
  );
};
