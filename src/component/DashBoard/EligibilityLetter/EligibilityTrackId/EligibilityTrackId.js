import React from "react";
import { RequestIdPage } from "component/common/DashBoardLandingPage";
import { eligibilityLetterRequestIdData } from "component/common/MockData/NewMockData";

export const EligibilityTrackId = () =>{
    return(
        <div>
            <RequestIdPage requestIdData={eligibilityLetterRequestIdData} />
        </div>
    )
}