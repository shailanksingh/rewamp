import React, { useState, useRef, useEffect } from "react";
import "./style.scss";
import { DashboardServiceCards } from "../DashBoardComponents/DashboardServiceCards";
import viewInteract from "assets/svg/viewInteractIcon.svg";
import PregnancyProgram from "assets/svg/PregnancyProgram.svg";
import RequestTelemedicine from "assets/svg/RequestTelemedicine.svg";
import MedicalReimbursement from "assets/svg/MedicalReimbursement.svg";
import roadAssisst from "assets/svg/dashBoardRoadAssisst.svg";
import claim from "assets/svg/dashBoardClaim.svg";
import inspect from "assets/svg/dashBoardInspection.svg";
import map_car from "assets/images/mobile/map_car.svg";
import map_H from "assets/images/mobile/map_H.svg";
import Unionminiblue from "assets/svg/Union-mini-blue.svg";
import { NormalSearch } from "component/common/NormalSearch";
// import NewPolicyCard from "component/common/MobileReuseable/PolicyCard/NewPolicyCard/NewPolicyCard";
import TravelPolicyCard from "component/common/MobileReuseable/PolicyCard/TravelPolicyCard/TravelPolicyCard";
import MotorPolicyCard from "component/common/MobileReuseable/PolicyCard/MotorPolicyCard/MotorPolicyCard";
import PolicyCard from "component/common/MobileReuseable/PolicyCard";
import Slider from "react-slick";
import { history } from "service/helpers";

const dashboardServicesCardData = [
  {
    id: 0,
    content: "Pregnancy Program",
    cardIcon: PregnancyProgram,
    class: "pr-1",
    url: "pregnancy-program",
  },
  {
    id: 1,
    content: "Request Telemedicine",
    cardIcon: RequestTelemedicine,
    class: "pr-1",
    url: "request-telemedicine",
  },
  {
    id: 2,
    content: "Medical Reimbursement",
    cardIcon: MedicalReimbursement,
    class: "pr-1",
    url: "medical-reimbursment",
  },
  {
    id: 3,
    content: "Road Side Assistance",
    cardIcon: roadAssisst,
    class: "pr-1",
    url: "road-assistance",
  },
  {
    id: 4,
    content: "Claim Assistance",
    cardIcon: claim,
    class: "pr-1",
    url: "claim-assistance",
  },
  {
    id: 5,
    content: "Periodic Inspection",
    cardIcon: inspect,
    class: "pr-1",
    url: "periodic-inspection",
  },
];

export const DashboardPolicies = () => {
  // const [policyListView, setPolicyListView] = useState(false);
  const sliderRef = useRef(null);
  const [viewDesktopPolicyDetails, setViewDesktopPolicyDetails] =
    useState(false);

  useEffect(() => {
    window.innerWidth > 600
      ? setViewDesktopPolicyDetails(true)
      : setViewDesktopPolicyDetails(false);
  });

  const settings = {
    slidesToShow: 2,
    slidesToScroll: 1,
    arrows: false,
    infinite: false,
    dots: true,
    speed: 500,
  };
  return (
    <div className="dashboard-policies-page">
      <div className={"dashboard_new_main_container_policies"}>
        <div className="dashboard_card_container">
          <div className={" policy_list_view"}>
            <p className={`fs-18 fw-800 mb-3 `}>
              Your Policies{" "}
              <span className="dashboard-notify-numbers fs-12 fw-400 ml-3">
                3
              </span>
            </p>

            {/* {policyListView && <NewPolicyCard />} */}
            <Slider
              ref={sliderRef}
              {...settings}
              className="dashboard-your-policies-slider"
            >
              <div className="policy-card-root">
                <TravelPolicyCard
                  isShrink={false}
                  viewDesktopPolicyDetails={viewDesktopPolicyDetails}
                  // setPolicyListView={setPolicyListView}
                />
              </div>
              <div className="policy-card-root">
                <MotorPolicyCard
                  isShrink={false}
                  viewDesktopPolicyDetails={viewDesktopPolicyDetails}
                  // setPolicyListView={setPolicyListView}
                />
              </div>
              <div className="policy-card-root">
                <PolicyCard
                  pageName="your-policies"
                  viewDesktopPolicyDetails={viewDesktopPolicyDetails}
                />
              </div>
              {/* <div className="policy-card-root">
                                <PolicyCard pageName="your-policies" />
                            </div> */}
            </Slider>
          </div>
        </div>
      </div>
      <div className="row">
        <div className="col-lg-6 col-12">
          <div>
            <p className="fs-20 fw-800 dashboard-service-Header">
              Your Services
            </p>
            <DashboardServiceCards
              dashboardServiceCardData={dashboardServicesCardData}
              textWidth="insureCardTextWidth"
              isfullWidth="col-4"
              // onClick={navigateService}
            />
            <div
              className="pt-3 cursor-pointer"
              onClick={() => history.push("/dashboard/service")}
            >
              <span className="fs-14 fw-400 view-ServiceLink">
                View More Services
              </span>{" "}
              <img src={viewInteract} className="img-fluid" alt="icon" />
            </div>
          </div>
        </div>
        <div className="col-lg-6 col-12">
          <div className="iframe-map-policies position-relative">
            <iframe
              height={"410px"}
              width="95%"
              frameborder="0"
              scrolling="no"
              src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3333.967410377096!2d-111.89998968453055!3d33.31966746342457!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x0!2zMzPCsDE5JzEwLjgiTiAxMTHCsDUzJzUyLjEiVw!5e0!3m2!1sen!2sus!4v1516690469899"
            />

            <form class="dashboard-map-form row align-items-center input-width pt-0 map-search">
              <div className="pr-lg-1">
                <NormalSearch
                  className="header-Search"
                  name="search"
                  placeholder="What you're looking for?"
                  needRightIcon={true}
                  searchAligner="alignIframeSearchIcon"
                />
              </div>
              <ul>
                <li>
                  <img src={map_H} height="20" alt="Chat" />
                  <span>Hospitals</span>
                </li>
                <li>
                  <img src={map_car} height="20" alt="Chat" />
                  <span>Workshops</span>
                </li>
                <li>
                  <img src={Unionminiblue} height="20" alt="Chat" />
                  <span>Our Offices</span>
                </li>
              </ul>
            </form>
          </div>
        </div>
      </div>
    </div>
  );
};
