import React from "react";
import { RequestIdPage } from "component/common/DashBoardLandingPage";
import { teleMedicineRequestIdData } from "component/common/MockData/NewMockData";

export const TeleRequestIdPage = () => {
  return (
    <div>
      <RequestIdPage requestIdData={teleMedicineRequestIdData} />
    </div>
  );
};
