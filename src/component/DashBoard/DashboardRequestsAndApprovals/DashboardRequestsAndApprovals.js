import React from "react";
import "./style.scss";
import PageTitleCard from "../DashBoardComponents/PageTitleCard";
import HelpCenterCard from "../DashBoardComponents/HelpCenterCard";
import { history } from "service/helpers";
import greenClock from "assets/svg/greenClock.svg";
import { RecentPolicyCard } from "../DashBoardComponents/RecentPolicyCard";
import HelpIcon from "assets/svg/Help.svg";
import hospitalicon from "assets/svg/h-icon.svg";
import CarGreyIcon from "assets/svg/car-grey.svg";
import { DashboardServiceCards } from "../DashBoardComponents/DashboardServiceCards";
import PregnancyProgram from "assets/svg/PregnancyProgram.svg";
import RequestTelemedicine from "assets/svg/RequestTelemedicine.svg";
import MedicalReimbursement from "assets/svg/MedicalReimbursement.svg";
import roadAssisst from "assets/svg/dashBoardRoadAssisst.svg";
import claim from "assets/svg/dashBoardClaim.svg";
import inspect from "assets/svg/dashBoardInspection.svg";

const recentPloicyCardData = [
  {
    id: 1,
    time: "30 Minutes Ago",
    progressLabel: "Completed",
    policyIcon: CarGreyIcon,
    title: "Almouasat Hospital",
    subtitle: "Brain MRI Approval.",
    getHelpIcon: HelpIcon,
  },
  {
    id: 2,
    time: "30 Minutes Ago",
    progressLabel: "Completed",
    policyIcon: hospitalicon,
    title: "Almouasat Hospital",
    subtitle: "Brain MRI Approval.",
    getHelpIcon: HelpIcon,
  },
  {
    id: 3,
    time: "30 Minutes Ago",
    progressLabel: "Completed",
    policyIcon: hospitalicon,
    title: "Almouasat Hospital",
    subtitle: "Brain MRI Approval.",
    getHelpIcon: HelpIcon,
  },
  {
    id: 4,
    time: "30 Minutes Ago",
    progressLabel: "Completed",
    policyIcon: hospitalicon,
    title: "Almouasat Hospital",
    subtitle: "Brain MRI Approval.",
    getHelpIcon: HelpIcon,
  },
];

const dashboardServicesCardData = [
  {
    id: 0,
    content: "Pregnancy Program",
    cardIcon: PregnancyProgram,
    class: "pr-1 col-sm-2",
    url: "pregnancy-program",
  },
  {
    id: 1,
    content: "Request Telemedicine",
    cardIcon: RequestTelemedicine,
    class: "pr-1 col-sm-2",
    url: "request-telemedicine",
  },
  {
    id: 2,
    content: "Medical Reimbursement",
    cardIcon: MedicalReimbursement,
    class: "pr-1 col-sm-2",
    url: "medical-reimbursment",
  },
  {
    id: 3,
    content: "Road Side Assistance",
    cardIcon: roadAssisst,
    class: "pr-1 col-sm-2",
    url: "road-assistance",
  },
  {
    id: 4,
    content: "Claim Assistance",
    cardIcon: claim,
    class: "pr-1 col-sm-2",
    url: "claim-assistance",
  },
  {
    id: 5,
    content: "Periodic Inspection",
    cardIcon: inspect,
    class: "pr-1 col-sm-2",
    url: "periodic-inspection",
  },
];

export const DashboardRequestsAndApprovals = () => {
  const fileComplaintToggler = (routeURL) => {
    if (routeURL) {
      history.push(routeURL);
    } else {
      //   setFile(true);
    }
  };
  return (
    <div className="dashboard-requests-and-approvals">
      <PageTitleCard count={3} title="Your Requests & Approvals" />
      <div className="row top-row-content">
        <div className="col-7">
          <div className="requests-content-title">Approvals History</div>
          {recentPloicyCardData.map((data, id) => {
            return (
              <RecentPolicyCard
                time={data.time}
                progressLabel={data.progressLabel}
                policyIcon={data.policyIcon}
                title={data.title}
                subtitle={data.subtitle}
                getHelpIcon={data.getHelpIcon}
                greyArrow={true}
              />
            );
          })}
          <div className="view-more-link">
            View More Requests <span>+</span>
          </div>
        </div>
        <div className="col-5">
          <div className="help-center-card-root">
            <HelpCenterCard
              helpCenterTitle="Contact Our Support Team"
              helpCenterTitleClass="helpNewCenterTitleOne"
              timeData="We're here 24 hours a day, 7 days a week, to help you and provide full support"
              label="Open a Complaint"
              className="helpCenterBtnSuccess"
              clockIcon={greenClock}
              timeTextColour="timeTextColourSuccess"
              timeTextClass="successTextAlign"
              fileComplaintToggler={fileComplaintToggler}
              routeURL={"/home/customerservice/opencomplaint"}
              heightClass="h-100"
            />
          </div>
          <div className="help-center-card-root">
            <HelpCenterCard
              helpCenterTitle="You don't need to wait anymore!"
              helpCenterTitleClass="helpNewCenterTitleTwo"
              timeData="We don't want to keep you waiting for someone to answer the phone"
              label="Start Live Chat"
              className="helpCenterNewBtnDanger"
              statusText="Online"
              statusClass="onlineContainer"
              timeTextColour="timeNewTextColourDanger"
              timeTextClass="m-0"
              fileComplaintToggler={fileComplaintToggler}
              routeURL={"/home/customerservice"}
            />
          </div>
        </div>
      </div>
      <div className="your-services-row">
        <div className="">
          <div className="requests-content-title mb-0">Your Services</div>
          <DashboardServiceCards
            dashboardServiceCardData={dashboardServicesCardData}
            textWidth="insureCardTextWidth"
            isfullWidth="col-4"
          />
          <div
            className="view-more-link cursor-pointer"
            onClick={() => history.push("/dashboard/service")}
          >
            View More Services <span>+</span>
          </div>
        </div>
      </div>
    </div>
  );
};
