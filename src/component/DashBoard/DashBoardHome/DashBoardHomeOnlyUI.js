import React, { useState, useEffect } from "react";
import { useParams } from "react-router-dom";
import "./style.scss";
import { AccidentClaimCard } from "../DashBoardComponents/AccidentClaimCard";
import { RecentPolicyCard } from "../DashBoardComponents/RecentPolicyCard";
import _Vehicle from "assets/svg/_Vehicle.svg";
import hospitalicon from "assets/svg/hospitalicon.svg";
import file from "assets/svg/file.svg";
import calender from "assets/svg/Calendar.svg";
import chatSmile from "assets/svg/Chat-smile.svg";
import PregnancyProgram from "assets/svg/PregnancyProgram.svg";
import RequestTelemedicine from "assets/svg/RequestTelemedicine.svg";
import MedicalReimbursement from "assets/svg/MedicalReimbursement.svg";
import HomeChildVaccination from "assets/svg/HomeChildVaccination.svg";
import ChronicDiseaseManagement from "assets/svg/ChronicDiseaseManagement.svg";
import AssistMedical from "assets/svg/AssistMedical.svg";
import Search from "assets/images/mobile/search.png";
import ArrowRight from "assets/images/mobile/right_arrow.png";
import map_car from "assets/images/mobile/map_car.svg";
import map_H from "assets/images/mobile/map_H.svg";
import Unionminiblue from "assets/svg/Union-mini-blue.svg";
import searchIcon from "assets/svg/headerSearchLight.svg";
import { DashboardServiceCards } from "../DashBoardComponents/DashboardServiceCards";
import { NormalSearch } from "component/common/NormalSearch";
import NewPolicyCard from "component/common/MobileReuseable/PolicyCard/NewPolicyCard/NewPolicyCard";
import TravelPolicyCard from "component/common/MobileReuseable/PolicyCard/TravelPolicyCard/TravelPolicyCard";
import MotorPolicyCard from "component/common/MobileReuseable/PolicyCard/MotorPolicyCard/MotorPolicyCard";
import PolicyCard from "component/common/MobileReuseable/PolicyCard";
import grid_orange from "assets/images/mobile/grid_orange.svg";
import viewInteract from "assets/svg/viewInteractIcon.svg";
import roadAssisst from "assets/svg/dashBoardRoadAssisst.svg";
import claim from "assets/svg/dashBoardClaim.svg";
import inspect from "assets/svg/dashBoardInspection.svg";

const dashboardServicesCardData = [
	{
		id: 0,
		content: "Pregnancy Program",
		cardIcon: PregnancyProgram,
		class: "pr-1",
		url: "pregnancy-program",
	},
	{
		id: 1,
		content: "Request Telemedicine",
		cardIcon: RequestTelemedicine,
		class: "pr-1",
		url: "request-telemedicine",
	},
	{
		id: 2,
		content: "Medical Reimbursement",
		cardIcon: MedicalReimbursement,
		class: "pr-1",
		url: "medical-reimbursment",
	},
	{
		id: 3,
		content: "Road Side Assistance",
		cardIcon: roadAssisst,
		class: "pr-1",
		url: "road-assistance",
	},
	{
		id: 4,
		content: "Claim Assistance",
		cardIcon: claim,
		class: "pr-1",
		url: "claim-assistance",
	},
	{
		id: 5,
		content: "Periodic Inspection",
		cardIcon: inspect,
		class: "pr-1",
		url: "periodic-inspection",
	},
];

const accidentClaimCardData = [
	{
		id: 1,
		title:
			"We are sorry to hear that you had an accident. here is the details in case you needed to raise a claim",
		iconmain: _Vehicle,
		icona: file,
		iconb: calender,
		platenumber: "3576 TND",
		casenumber: "1234",
		accidentdate: "1/1/2022",
		btnlabel: "Create Claim",
		createClaimStandAlone: true,
	},
];

const recentPloicyCardData = [
	{
		id: 1,
		time: "30 Minutes Ago",
		progressLabel: "In progress",
		policyIcon: hospitalicon,
		title: "Almouasat Hospital",
		subtitle: "Brain MRI Approval.",
		getHelpIcon: chatSmile,
	},
];

export const DashBoardHomeOnlyUI = () => {
	const [policyListView, setPolicyListView] = useState(true);
	const [viewDesktopPolicyDetails, setViewDesktopPolicyDetails] =
		useState(false);

	const [discount, setDiscount] = useState(false);

	const id = useParams();

	const [state, setState] = useState(0);

	// const toggle = () =>{
	//   setState((i) => i + 1);
	//   history.push(`/road-assisstance/${state}`)
	// }

	useEffect(() => {
		window.innerWidth > 600
			? setViewDesktopPolicyDetails(true)
			: setViewDesktopPolicyDetails(false);
	});

	const discountHandler = () => {
		setDiscount(true);
	};

	return (
		<React.Fragment>
			<div className="row py-4">
				<div className="col-lg-6 col-12 pr-0">
					{accidentClaimCardData.map((item, id) => {
						return (
							<AccidentClaimCard
								key={id}
								title={item.title}
								iconmain={item.iconmain}
								icona={item.icona}
								iconb={item.iconb}
								platenumber={item.platenumber}
								casenumber={item.casenumber}
								accidentdate={item.accidentdate}
								btnlabel={item.btnlabel}
								discount={discount}
								discountHandler={discountHandler}
								createClaimStandAlone={item.createClaimStandAlone}
							/>
						);
					})}
					{recentPloicyCardData.map((data, id) => {
						return (
							<RecentPolicyCard
								time={data.time}
								progressLabel={data.progressLabel}
								policyIcon={data.policyIcon}
								title={data.title}
								subtitle={data.subtitle}
								getHelpIcon={data.getHelpIcon}
							/>
						);
					})}
					<div className="pt-2">
						<span className="fs-14 fw-400 viewInteractionLink">
							View More Interactions
						</span>{" "}
						<img src={viewInteract} className="img-fluid" alt="icon" />
					</div>

					{!policyListView && (
						<div className="row pr-3">
							<div className="col-12">
								<p className="fs-20 fw-800 dashboard-service-Header m-0 pt-4">
									Your Services
								</p>
								<DashboardServiceCards
									dashboardServiceCardData={dashboardServicesCardData}
									textWidth="insureCardTextWidth"
									isfullWidth="col-4"
								/>
								<div className="pt-3">
									<span className="fs-14 fw-400 view-ServiceLink">
										View More Services
									</span>{" "}
									<img src={viewInteract} className="img-fluid" alt="icon" />
								</div>
							</div>
						</div>
					)}
				</div>
				<div className="col-lg-6 col-12">
					<div
						className={
							!policyListView
								? "dashboard_new_main_container"
								: "dashboard_main_container"
						}
					>
						<div className="dashboard_card_container">
							<div
								className={` ${
									policyListView ? "card_swap" : "policy_list_view"
								}`}
							>
								<p className={`${!policyListView && "pb-3"} fs-18 fw-800 m-0`}>
									Your Policies{" "}
									<span className="dashboard-notify-numbers fs-12 fw-400 ml-3">
										3
									</span>
								</p>

								{policyListView && <NewPolicyCard />}

								<TravelPolicyCard
									isShrink={policyListView}
									setPolicyListView={setPolicyListView}
									viewDesktopPolicyDetails={viewDesktopPolicyDetails}
								/>

								<MotorPolicyCard
									isShrink={policyListView}
									setPolicyListView={setPolicyListView}
									viewDesktopPolicyDetails={viewDesktopPolicyDetails}
								/>
								<PolicyCard
									viewDesktopPolicyDetails={viewDesktopPolicyDetails}
								/>
							</div>
						</div>
						<label
							className="view_all_cards text-uppercase"
							onClick={() => setPolicyListView(false)}
						>
							<img src={grid_orange} alt="Arrow" /> View all policies
						</label>
					</div>
				</div>

				{policyListView && (
					<div className="col-lg-6 col-12">
						<p className="fs-20 fw-800 dashboard-service-Header m-0">
							Your Services
						</p>
						<DashboardServiceCards
							dashboardServiceCardData={dashboardServicesCardData}
							textWidth="insureCardTextWidth"
							isfullWidth="col-4"
							// onClick={navigateService}
						/>
						<div className="pt-3">
							<span className="fs-14 fw-400 view-ServiceLink">
								View More Services
							</span>{" "}
							<img src={viewInteract} className="img-fluid" alt="icon" />
						</div>
					</div>
				)}
				{policyListView && (
					<div className="col-lg-6 col-12 pt-4">
						<div className="iframe-map position-relative">
							<iframe
								height={policyListView ? "370px" : "410px"}
								width="95%"
								frameborder="0"
								scrolling="no"
								src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3333.967410377096!2d-111.89998968453055!3d33.31966746342457!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x0!2zMzPCsDE5JzEwLjgiTiAxMTHCsDUzJzUyLjEiVw!5e0!3m2!1sen!2sus!4v1516690469899"
							/>

							<form class="dashboard-map-form row align-items-center input-width pt-0 map-search">
								<div className="pr-lg-1">
									<NormalSearch
										className="header-Search"
										name="search"
										placeholder="What you're looking for?"
										needRightIcon={true}
										searchAligner="alignIframeSearchIcon"
									/>
								</div>
								<ul>
									<li>
										<img src={map_H} height="20" alt="Chat" />
										<span>Hospitals</span>
									</li>
									<li>
										<img src={map_car} height="20" alt="Chat" />
										<span>Workshops</span>
									</li>
									<li>
										<img src={Unionminiblue} height="20" alt="Chat" />
										<span>Our Offices</span>
									</li>
								</ul>
							</form>
						</div>
					</div>
				)}
			</div>

			{/* <div className="dashboard">
			<div className="dashboard-items">
				{accidentClaimCardData.map((item, id) => {
					return (
						<AccidentClaimCard
							key={id}
							title={item.title}
							iconmain={item.iconmain}
							icona={item.icona}
							iconb={item.iconb}
							platenumber={item.platenumber}
							casenumber={item.casenumber}
							accidentdate={item.accidentdate}
							btnlabel={item.btnlabel}
						/>
					);
				})}
				{recentPloicyCardData.map((data, id) => {
					return (
						<RecentPolicyCard
							time={data.time}
							progressLabel={data.progressLabel}
							policyIcon={data.policyIcon}
							title={data.title}
							subtitle={data.subtitle}
							getHelpIcon={data.getHelpIcon}
						/>
					);
				})}
			</div>
			<div className="dashboard-items"></div>
			
			
		</div> */}
			<div className="home-dashBoard-liner mb-3"></div>
		</React.Fragment>
	);
};
