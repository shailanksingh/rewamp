import React from "react";
import { DashboardServiceCards } from "component/DashBoard/DashBoardComponents/DashboardServiceCards";
import { ComplaintTabs } from "component/common/ComplaintTabs";
import PregnancyProgram from "assets/svg/PregnancyProgram.svg";
import RequestTelemedicine from "assets/svg/RequestTelemedicine.svg";
import MedicalReimbursement from "assets/svg/MedicalReimbursement.svg";
import roadAssisst from "assets/svg/dashBoardRoadAssisst.svg";
import claim from "assets/svg/dashBoardClaim.svg";
import inspect from "assets/svg/dashBoardInspection.svg";
import vaccinate from "assets/svg/dashboardIcons/childVaccination.svg";
import chronic from "assets/svg/dashboardIcons/chronic.svg";
import assistAccident from "assets/svg/dashboardIcons/accidentAssist.svg";
import assistMedic from "assets/svg/dashboardIcons/medicalAssist.svg";
import carManitainance from "assets/svg/dashboardIcons/carMaintainance.svg";
import carWash from "assets/svg/dashboardIcons/carWash.svg";
import "./style.scss";

export const DashBoardAllServices = () => {
  const dashboardServicesCardData = [
    {
      id: 0,
      content: "Pregnancy Program",
      cardIcon: PregnancyProgram,
      class: "pr-1 col-2",
      url: "pregnancy-program",
    },
    {
      id: 1,
      content: "Request Telemedicine",
      cardIcon: RequestTelemedicine,
      class: "pr-1 col-2",
      url: "request-telemedicine",
    },
    {
      id: 2,
      content: "Medical Reimbursement",
      cardIcon: MedicalReimbursement,
      class: "pr-1 col-2",
      url: "medical-reimbursment",
    },
    {
      id: 3,
      content: "Home Child Vaccination",
      cardIcon: vaccinate,
      class: "pr-1 col-2",
      url: "road-assistance",
    },
    {
      id: 4,
      content: "Chronic Disease Management",
      cardIcon: chronic,
      class: "pr-1 col-2",
      url: "claim-assistance",
    },
    {
      id: 5,
      content: "Assist in medical",
      cardIcon: assistMedic,
      class: "col-2",
      url: "periodic-inspection",
    },
    {
      id: 6,
      content: "Road Side Assistance",
      cardIcon: roadAssisst,
      class: "pr-1 col-2",
      url: "road-assistance",
    },
    {
      id: 7,
      content: "Assist in Accident",
      cardIcon: assistAccident,
      class: "pr-1 col-2",
      url: "periodic-inspection",
    },
    {
      id: 8,
      content: "Claim Assistance",
      cardIcon: claim,
      class: "pr-1 col-2",
      url: "claim-assistance",
    },
    {
      id: 9,
      content: "Periodic Inspection",
      cardIcon: inspect,
      class: "pr-1 col-2",
      url: "periodic-inspection",
    },
    {
      id: 10,
      content: "Car Maintenance",
      cardIcon: carManitainance,
      class: "pr-1 col-2",
      url: "car-maintainance",
    },
    {
      id: 11,
      content: "Car Wash",
      cardIcon: carWash,
      class: "col-2",
      url: "car-wash",
    },
  ];

  return (
    <div className="row pb-2 mb-1">
      <div className="col-lg-12 col-12 all-services-dashboard-container pt-4">
        <div className="all-tawuniya-service-header-container p-3">
          <p className="all-tawuniya-service-header-title fs-14 fw-800 m-0">
            Tawuniya Services
          </p>
        </div>
        <div className="tawniya-allService-tab-liner pt-4 mt-2 pb-4">
          <ComplaintTabs
            justifyContentTabsClass="justify-content-start"
            compOne={
              <DashboardServiceCards
                dashboardServiceCardData={dashboardServicesCardData}
                textWidth="insureCardTextWidth"
              />
            }
          />
        </div>
      </div>
    </div>
  );
};
