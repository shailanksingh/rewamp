import React from "react";
import "./style.scss";
import InsuranceCard from "../DashBoardComponents/InsuranceCard";
import HealthIconGrey from "assets/images/health-icon-grey.svg";
import homesubproducts1 from "assets/images/homesubproducts1.png";
import DriveTextImage from "assets/images/drive-text.png";
import RightArrowIcon from "assets/images/menuicons/right-arrow-white-sm.svg";
import PageTitleCard from "../DashBoardComponents/PageTitleCard";

let motorList = [{
    title: "Al Shamel",
    subtitle: '"Comprehensive Motor Insurance Program  (AL-SHAMEL)"',
    description: "The maximum cover of Al-Shamel insurance is determined by the value of the insured vehicle at the time of accident, in addition to the public liability towards third party affected by the incident. ",
}, {
    title: "Sanad",
    subtitle: 'Sanad for Private Motor Vehicle Liability Insurance ',
    description: "Cover up to SR 10 million in respect of the third party bodily injury and property damage",
}, {
    title: "Mechanical Breakdown",
    subtitle: 'Mechanical Breakdown Insurance',
    description: "Cover the repair of mechanical and electrical failures of the car and the wages of the necessary human resources.",
}, {
    title: "Sanad Plus",
    subtitle: 'Sanad Plus for Limited Motor Compreheinsive Insurance',
    description: "Provides limited insurance cover for the damage of the insured private car as well as the liability to third parties.",
}];

let healthList = [{
    tag: {
        icon: HealthIconGrey,
        text: "Health"
    },
    title: "My Family",
    subtitle: 'My Family Medical Insurance',
    description: "Provides adequate healthcare for all family members with four categories of benefits",
}, {
    tag: {
        icon: HealthIconGrey,
        text: "Health"
    },
    title: "Visit Visa Medical Insurance",
    subtitle: 'Visitors Insurance Program',
    description: "The visitors to the Kingdom of Saudi Arabia will obtain the healthcare for medical emergencies or accidents",
}, {
    tag: {
        icon: HealthIconGrey,
        text: "Health"
    },
    title: "Umrah Health Insurance",
    subtitle: 'Umrah Insurance Program (for Foreign Pilgrims)',
    description: "Provides insurance coverage for medical emergency cases and general accidents for foreign Ummrah performers",
}, {
    tag: {
        icon: HealthIconGrey,
        text: "Health"
    },
    title: "Hajj Health Insurance",
    subtitle: 'Hajj Insurance (for Foreign Pilgrims)',
    description: "Provides insurance coverage for medical emergency cases  and general accidents for foreign Hajj performers.",
}, {
    tag: {
        icon: HealthIconGrey,
        text: "Health"
    },
    title: "Tourist Insurance Program",
    subtitle: 'Tourist Insurance Program',
    description: "Allows the citizens of a number of countries to obtain Tourist Visa, health care for emergency medical cases and accidents coverage",
}];

let propertyList = [{
    title: "Home Insurance",
    subtitle: 'Insurance against theft, fire and additional risks',
    description: "Provides protection to residential buildings and their contents against natural disasters, and covers losses resulting from any incidents leading to the deterioration or damage to the house",
}, {
    title: "International Travel Insurance",
    subtitle: 'Travel Insurance for Europe and rest of the world',
    description: "Provides comprehensive coverage for travelers regarding the risks related to travel outside Saudi Arabia",
}, {
    title: "COVID-19 Travel Insurance",
    subtitle: 'Covid-19 Travel Insurance - for Citizens',
    description: "Protect from the Covide-19 risk of infection and the associated problems resulting from the government's preventive measures to limit the spread of the virus",
}, {
    title: "Medical Malpractice Insurance",
    subtitle: 'Medical Malpractice Insurance',
    description: "Provide coverage for those practicing medical professions from the risks associated with their work and the legal third party liability that may arise out of any error, negligence, or omission incurred during the performance of their work",
}, {
    title: "Shop Owners Insurance",
    subtitle: 'Shop Owners Insurance',
    description: "Provides comprehensive coverage for all types of shops except for certain activities specified in the insurance policy",
}];

export const DashboardProductsAndOfferingsComp = () => {
    return (
        <div className="dashboard-products-and-offerings">
            <PageTitleCard title="Our Products & Offerings" />
            <div className="products-offerings-cards">
                <div className="products-offerings-card-root">
                    <div className="products-offerings-card">
                        <InsuranceCard 
                            type="Motor"
                        />
                    </div>
                </div>
                {motorList?.map((item, index) => {
                    return (
                        <div className="products-offerings-card-root">
                            <div className="products-offerings-card">
                                <InsuranceCard 
                                    cardContent={item}
                                    key={index.toString()}
                                />
                            </div>
                        </div>
                    );
                })}   
                <div className="products-offerings-card-root">
                    <div className="products-offerings-card">
                        <div className="explore-more-card" style={{
                            backgroundImage: "url(" + homesubproducts1 + ")",
                        }}>
                            <div className="image">
                                <img src={DriveTextImage} alt="..." />
                            </div>
                            <div className="bottom-content">
                                <div className="explore-more-title">
                                    Tawuniya Drive <br />Program
                                </div>
                                <div className="explore-more-description">
                                    It mesures your driving behavior and rewards you weekly, as well as a special discount on your motor insurance
                                </div>
                                <div className="explore-more-button">
                                    <button>
                                        Explore More <img src={RightArrowIcon} alt="..." />
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div className="products-offerings-cards">
                <div className="products-offerings-card-root">
                    <div className="products-offerings-card">
                        <InsuranceCard 
                            type="Health"
                        />
                    </div>
                </div>
                {healthList?.map((item, index) => {
                    return (
                        <div className="products-offerings-card-root">
                            <div className="products-offerings-card">
                                <InsuranceCard 
                                    cardContent={item}
                                    key={index.toString()}
                                />
                            </div>
                        </div>
                    );
                })}   
            </div>
            <div className="products-offerings-cards">
                <div className="products-offerings-card-root">
                    <div className="products-offerings-card">
                        <InsuranceCard 
                            type="Property"
                        />
                    </div>
                </div>
                {propertyList?.map((item, index) => {
                    return (
                        <div className="products-offerings-card-root">
                            <div className="products-offerings-card">
                                <InsuranceCard 
                                    cardContent={item}
                                    key={index.toString()}
                                />
                            </div>
                        </div>
                    );
                })}   
            </div>
        </div>
    );
}