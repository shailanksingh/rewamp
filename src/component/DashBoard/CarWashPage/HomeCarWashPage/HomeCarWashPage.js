import React from "react";
import { DashBoardLandingPage } from "component/common/DashBoardLandingPage";
import {
  dashboardCarWashHeaderData,
  dashboardCarWashProgressData,
  faqCarWashDashboardData,
  carWashContentData,
} from "component/common/MockData";
import "./style.scss";

export const HomeCarWashPage = () => {
  return (
    <div className="carWash-LandingContainer">
      <DashBoardLandingPage
        dashboardHeaderData={dashboardCarWashHeaderData}
        dashboardProgressData={dashboardCarWashProgressData}
        faqDashboardData={faqCarWashDashboardData}
        dashBoardContentData={carWashContentData}
      />
    </div>
  );
};
