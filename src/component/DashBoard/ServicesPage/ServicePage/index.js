import React from "react";
import { history } from "service/helpers";
import { DashBoardLandingPage } from "component/common/DashBoardLandingPage";
import {
	dashboardTeleMedHeaderData,
	dashboardTeleMedProgressData,
	dashboardRoadProgressData,
	faqRoadDashboardData,
	dashboardPeriodicInspectionProgressData,
	dashboardRoadHeaderData,
	teleMedicineContentData,
	dashboardPeriodicInspectHeaderData,
	roadAssisstantContentData,
	faqPeriodicInspectDashboardData,
	periodicInspectContentData,
	dashboardPregnancyHeaderData,
	dashboardPregnancyProgressData,
	faqPregnancyDashboardData,
	pregnancyContentData,
	dashboardReimBursmentHeaderData,
	dashboardReimBursmentProgressData,
	faqReimBursmentDashboardData,
	reimBursmentContentData,
} from "component/common/MockData";
import "./style.scss";

const ServicesData = [
	{
		id: 0,
		url: "/dashboard/service/pregnancy-program",
		dashboardHeaderData: dashboardPregnancyHeaderData,
		dashboardProgressData: dashboardPregnancyProgressData,
		faqDashboardData: faqPregnancyDashboardData,
		dashBoardContentData: pregnancyContentData,
		class: "pregnancy-LandingContainer",
		needLayout: true,
	},
	{
		id: 1,
		url: "/dashboard/service/request-telemedicine",
		dashboardHeaderData: dashboardTeleMedHeaderData,
		dashboardProgressData: dashboardTeleMedProgressData,
		faqDashboardData: faqRoadDashboardData,
		dashBoardContentData: teleMedicineContentData,
		class: "teleMedicine-LandingContainer",
	},
	{
		id: 2,
		url: "/dashboard/service/medical-reimbursment",
		dashboardHeaderData: dashboardReimBursmentHeaderData,
		dashboardProgressData: dashboardReimBursmentProgressData,
		faqDashboardData: faqReimBursmentDashboardData,
		dashBoardContentData: reimBursmentContentData,
		class: "reimBursment-LandingContainer",
		needLayout: true,
	},
	{
		id: 3,
		url: "/dashboard/service/road-assistance",
		dashboardHeaderData: dashboardRoadHeaderData,
		dashboardProgressData: dashboardRoadProgressData,
		faqDashboardData: faqRoadDashboardData,
		dashBoardContentData: roadAssisstantContentData,
		class: "roadAssistance-LandingContainer",
	},
	{
		id: 5,
		url: "/dashboard/service/periodic-inspection",
		dashboardHeaderData: dashboardPeriodicInspectHeaderData,
		dashboardProgressData: dashboardPeriodicInspectionProgressData,
		faqDashboardData: faqPeriodicInspectDashboardData,
		dashBoardContentData: periodicInspectContentData,
		class: "periodicInspect-LandingContainer",
	},
];

export const ServicePage = () => {
	const liveLink = history.location.pathname;

	return (
		<>
			{ServicesData.map((item, id) => {
				return (
					<React.Fragment>
						{liveLink === item.url && (
							<div className={item.class} key={id}>
								<DashBoardLandingPage
									dashboardHeaderData={item.dashboardHeaderData}
									dashboardProgressData={item.dashboardProgressData}
									faqDashboardData={item.faqDashboardData}
									dashBoardContentData={item.dashBoardContentData}
									isLayout={item.needLayout}
								/>
							</div>
						)}
					</React.Fragment>
				);
			})}
		</>
	);
};
