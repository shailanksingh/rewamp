import React from "react";
import { DashBoardLandingPage } from "component/common/DashBoardLandingPage";
import {
	dashboardHomeChildVaccinateHeaderData,
	dashboardHomeChildVaccinateProgressData,
	faqHomeChildVaccinateDashboardData,
	HomeChildVaccinateContentData,
} from "component/common/MockData";
import "./style.scss";

export const HomeChildVaccinate = () => {
	return (
		<div className="homeVaccinate-LandingContainer">
			<DashBoardLandingPage
				dashboardHeaderData={dashboardHomeChildVaccinateHeaderData}
				dashboardProgressData={dashboardHomeChildVaccinateProgressData}
				faqDashboardData={faqHomeChildVaccinateDashboardData}
				dashBoardContentData={HomeChildVaccinateContentData}
			/>
		</div>
	);
};
