import React from "react";
import { DashBoardLandingPage } from "component/common/DashBoardLandingPage";
import {
  dashboardCarMaintainHeaderData,
  dashboardCarMaintainProgressData,
  faqCarMaintainDashboardData,
  carMaintainContentData,
} from "component/common/MockData";
import "./style.scss";

export const HomeCarMaintainPage = () => {
  return (
    <div className="carMaintain-LandingContainer">
      <DashBoardLandingPage
        dashboardHeaderData={dashboardCarMaintainHeaderData}
        dashboardProgressData={dashboardCarMaintainProgressData}
        faqDashboardData={faqCarMaintainDashboardData}
        dashBoardContentData={carMaintainContentData}
      />
    </div>
  );
};
