import React from "react";
import { RequestIdPage } from "component/common/DashBoardLandingPage";
import { carMaintainRequestIdData } from "component/common/MockData/NewMockData";

export const CarMaintainTrackId = () =>{
    return(
        <div>
            <RequestIdPage requestIdData={carMaintainRequestIdData} />
        </div>
    )
}