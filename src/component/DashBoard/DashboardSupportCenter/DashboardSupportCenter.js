import React, { useState } from "react";
import "./style.scss";
import PageTitleCard from "../DashBoardComponents/PageTitleCard";
import { resultData } from "component/common/MockData/index";
import HelpCenterCard from "../DashBoardComponents/HelpCenterCard";
import { history } from "service/helpers";
import greenClock from "assets/svg/greenClock.svg";
import { ComplaintTabs } from "component/common/ComplaintTabs";
import { ServiceCard } from "component/common/ServiceCard";
import { serviceSupportData } from "component/common/MockData/index";
// import OpenComplaintForm from "component/common/OpenComplaintForm"

export const DashboardSupportCenter = () => {
  const [pillIndex, setPillIndex] = useState(null);
  // const [file, setFile] = useState(false);
  const fileComplaintToggler = (routeURL) => {
    if (routeURL) {
      history.push(routeURL);
    } else {
      //   setFile(true);
    }
  };
  return (
    <div className="dashboard-support-center-page">
      <PageTitleCard count={3} title="Support Center" />
      <div className="row top-row-content">
        <div className="col-7">
          <div className="resultBar-title">Previous Support Tickets</div>
          <div className="row">
            {resultData.map((item, index) => {
              return (
                <div
                  className={`${
                    item.pillNo === pillIndex ? "col-12" : "col-12"
                  } pb-3`}
                  key={index}
                >
                  <div className="resultBar">
                    <div
                      className="d-flex justify-content-between accordionToggler p-3"
                      onClick={() =>
                        pillIndex === null
                          ? setPillIndex(item.pillNo)
                          : setPillIndex(null)
                      }
                    >
                      <div className="pl-3">
                        <p className="m-0 fs-16 fw-700">{item.code}</p>
                        <p className="fs-14 fw-400 m-0">{item.date}</p>
                      </div>
                      <div className="pr-3">
                        <div className={`${item.statusClass} p-1 px-3`}>
                          <span className="fw-400 fs-14">{item.status}</span>
                        </div>
                      </div>
                    </div>
                    {item.pillNo === pillIndex && (
                      <div className="detailReportContainer">
                        <p className="fs-24 fw-800 complaintDetailTitle pt-4 pb-2">
                          Complaint Details
                        </p>
                        {item?.detailsData?.map((items, index) => {
                          return (
                            <div
                              className="d-flex justify-content-between detailInfoContainer"
                              key={index}
                            >
                              <div>
                                <p className="leftComplaintText fs-14 fw-400 m-0">
                                  {items.complaintTitle}
                                </p>
                              </div>
                              <div>
                                <p className="rightComplaintText fs-14 fw-400 m-0">
                                  {items.complaintText}
                                </p>
                              </div>
                            </div>
                          );
                        })}
                      </div>
                    )}
                  </div>
                </div>
              );
            })}
          </div>
          <div className="view-more-link">
            View More Tickets <span>+</span>
          </div>
        </div>
        <div className="col-5">
          <div className="help-center-card-root">
            <HelpCenterCard
              helpCenterTitle="Contact Our Support Team"
              helpCenterTitleClass="helpNewCenterTitleOne"
              timeData="We're here 24 hours a day, 7 days a week, to help you and provide full support"
              label="Open a Complaint"
              className="helpCenterBtnSuccess"
              clockIcon={greenClock}
              timeTextColour="timeTextColourSuccess"
              timeTextClass="successTextAlign"
              fileComplaintToggler={fileComplaintToggler}
              routeURL={"/home/customerservice/opencomplaint"}
              heightClass="h-100"
            />
          </div>
          <div className="help-center-card-root">
            <HelpCenterCard
              helpCenterTitle="You don't need to wait anymore!"
              helpCenterTitleClass="helpNewCenterTitleTwo"
              timeData="We don't want to keep you waiting for someone to answer the phone"
              label="Start Live Chat"
              className="helpCenterNewBtnDanger"
              statusText="Online"
              statusClass="onlineContainer"
              timeTextColour="timeNewTextColourDanger"
              timeTextClass="m-0"
              fileComplaintToggler={fileComplaintToggler}
              routeURL={"/home/customerservice"}
            />
          </div>
          {/* {file && <OpenComplaintForm />} */}
        </div>
      </div>
      <div className="product-services-support">
        <div className="product-services-support-title">
          Products & Services Support
        </div>
        {/* <ComplaintTabs
					justifyContentTabsClass="justify-content-start"
					columnClass={"col-lg-3 col-12"}
				/> */}
        <ComplaintTabs
          justifyContentTabsClass="justify-content-start"
          compOne={
            <ServiceCard
              serviceData={serviceSupportData}
              columnClass="col-lg-3 col-12"
              routeURL="/home/customerservice/medicalfraud"
            />
          }
        />
        <div className="view-more-link">
          View More Tickets <span>+</span>
        </div>
      </div>
    </div>
  );
};
