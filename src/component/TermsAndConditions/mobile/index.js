import React from "react";
import HeaderBackNav from "component/common/MobileReuseable/HeaderBackNav";
import "./style.scss";

const TermsAndConditionsMobile = () => {
  return (
       <div className="privacy_policy_container">
      <HeaderBackNav pageName="Terms" title="Terms and Conditions" />
      <div className="container">
      <div className="Conditions_Heading">Terms and conditions</div>
      <div className="card container Terms_conditions">
        <div className="Conditions_Start">
        <p>TAWUNIYA is committed to protecting your privacy. TAWUNIYA’s Online Privacy Statement applies to data collected by TAWUNIYA through this online service access, as well as its offline services.</p>
         </div>
         <div className="Conditions_collection">Collection of Your Personal Information</div>
         <div className="Conditions_Information">
          <p>Additional personal information, such as your home or work address or telephone number, your age, gender, preferences, interests and favorites may be required. If you choose to make a purchase, you may be required to submit information, such as your credit card number and billing address. We may collect information about your visit, including the pages you view, the links you click and other actions taken in connection with TAWUNIYA’s site and services. We also collect certain standard information that your browser sends to every website you visit, such as your IP address, browser type and language, access times and referring Web site addresses. When you receive newsletters or promotional e-mail from TAWUNIYA, we may use web beacons, customized links or similar technologies to determine if the e-mail was delivered, read and links that you click. This will help us server you with information that is relevant to your requirement.</p>
         </div>

         <div className="Conditions_Personal_Topic">Use of Your Personal Information</div>
         <div className="Conditions_Personal_Customerservice">
          <p>
          more effective customer service; making the services easier to use by eliminating the need for you to repeatedly enter the same information; performing research and analysis aimed at improving our products and services. We also use your personal information to communicate with you. We may send certain communications such as welcome letters, billing reminders, information regarding your insurance contracts, offers and products. We may also occasionally send you product or service surveys and promotional mails to inform you of other products or services available at TAWUNIYA.
          </p>
         </div>

         <div className="Conditions_Personal_Sharing" >Sharing of Your Personal Information</div>
         <div className="Conditions_Personal_Except">
          <p>
          Except as described in this statement, we will not disclose your personal information to anyone other than TAWUNIYA, its service providers, subsidiaries and affiliates. We may access and/or disclose your personal information if we believe such action is necessary to: (a) comply with the law or legal process; (b) protect and defend the rights or property of TAWUNIYA (including the enforcement of our agreements); or (c) act in urgent circumstances to protect the personal safety of TAWUNIYA’s customers or members of the public.
          </p>
          </div>

          <div className="Conditions_accessing">Accessing Your Personal Information</div>
          <div className="Conditions_ability">
         <p>
         You may have the ability to view or edit your personal information online. In order to help prevent your personal information from being viewed by others, you will be required to sign in with your credentials.
         </p>
          </div>

          <div className="Conditions_Communication">Communication Preferences</div>
          <div className="Conditions_promotional">
            <p>
            You can stop delivery of future promotional e-mail from TAWUNIYA by proactively making choices about communication that you receive from TAWUNIYA by visiting your preferences page. These communication choices do not apply to mandatory service communications that are considered part of certain TAWUNIYA services and contracts, which you may receive periodically.
            </p>
          </div>

          <div className="Conditions_Security">Security of Your Personal Information</div>
          <div className="Conditions_Socket">
            <p>
            through the use of encryption, such as the Secure Socket Layer (SSL) protocol. If a password is used to help protect your accounts and personal information, it is your responsibility to keep your password confidential. Do not share this information with anyone. If you are sharing a computer with anyone you should always choose to log out before leaving a site or service to protect access to your information from subsequent users.
            </p>
          </div>

          <div className="Conditions_Cookies">Use of Cookies</div>
          <div className="Conditions_decline">
            <p>
            You have the ability to accept or decline cookies. Most Web browsers automatically accept cookies, but you can usually modify your browser setting to decline cookies if you prefer.
            </p>
          </div>

          <div className="Conditions_Enforcement">Enforcement of This Privacy Statement</div>
          <div className="Conditions_regarding">
            <p>
            If you have questions regarding this statement, you should first contact us by using the contact us link.
            </p>
          </div>

          <div className="Conditions_privacystatement">Changes to This Privacy Statement</div>
       <div className="Conditions_occasionally">
        <p>
        We will occasionally update this privacy statement to reflect changes in our services and customer feedback. When we post changes to this Statement, we will revise the "last updated" date at the top of this statement. We encourage you to periodically review this statement to be informed of how TAWUNIYA is protecting your information.
        </p>
       </div>
      
      </div>
      

      </div>
      <div className="i"></div>
    </div>
  );
};

export default TermsAndConditionsMobile;
