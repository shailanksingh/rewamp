import React, { useState, useEffect } from "react";
import { Typography, CardContent } from "@material-ui/core";
import { useSelector } from "react-redux";
import { useLocation } from "react-router-dom";
import { Card } from "react-bootstrap";
import { requestedServiceSelector } from "action/LanguageAct";

import Logo1 from "../../../assets/svg/MotorInsurance.svg";
import Logo2 from "../../../assets/svg/Vector.svg";
import Logo3 from "../../../assets/svg/Vector5.svg";
import Car from "../../../assets/svg/ReportFraud/car-white.svg";

import { NormalRadioButton } from "component/common/NormalRadioButton";

import MedicalFraudForm from "component/common/MedicalFraudForm";
import MotorFraudForm from "component/common/MotorFraudForm";
import PropertyFraudForm from "component/common/PropertyFraudForm";
import OpenComplaintMobile from "component/common/OpenComplaintMobile";

import "./style.scss";

export const ReportFraud = () => {
	const location = useLocation();

	// useEffect(() => {
	// 	console.log(location.state.fraudService); // result: 'some_value'
	// }, [location]);

	const [showform1, setShowform1] = useState(false);
	const [showform2, setShowform2] = useState(false);
	const [showform3, setShowform3] = useState(false);

	const [activecard, setActivecard] = useState("maincard activemaincard");
	const [maincard1, setMaincard1] = useState("maincard");
	const [maincard2, setMaincard2] = useState("maincard");
	const [maincard3, setMaincard3] = useState("maincard");

	const getRequestedService = useSelector(
		(data) => data.languageReducer.requestedService
	);

	const data = {
		title: "Report Motor Fraud",
		content:
			"Any intentional act or accident by the owner of the insured vehicle to the vehicle or distortion or concealment of the claim documents, or/and deliberately providing false information from the vehicle insurance card holder, a third party, or a repair service provider, to obtain compensation or benefits, not due to them or others",
	};

	const [radioValue, setRadioValue] = useState("0");
	const [radioButtonChecked, setRadioButtonChecked] = useState(
		getRequestedService ? getRequestedService : "medicalFraud"
	);

	useEffect(() => {
		handlechange(getRequestedService ? getRequestedService : "medicalFraud");
	}, []);

	const handlechange = (value) => {
		console.log("clicked", showform1);
		if (value == "medicalFraud") {
			setRadioButtonChecked("medicalFraud");
			setShowform1(true);
			setShowform2(false);
			setShowform3(false);
			setMaincard1("maincard activemaincard");
			setMaincard2("maincard");
			setMaincard3("maincard");
		}
		if (value == "motorFraud") {
			setRadioButtonChecked("motorFraud");
			setShowform1(false);
			setShowform2(true);
			setShowform3(false);
			setMaincard2("maincard activemaincard");
			setMaincard1("maincard");
			setMaincard3("maincard");
		}
		if (value == "travelFraud") {
			setRadioButtonChecked("travelFraud");
			setShowform1(false);
			setShowform2(false);
			setShowform3(true);
			setMaincard3("maincard activemaincard");
			setMaincard2("maincard");
			setMaincard1("maincard");
		}
	};

	return (
		<React.Fragment>
			<div className="row mainBannerContainer pt-5">
				<div className="col-lg-12 col-md-12 col-12">
					<p className="fs-35 fw-800 text-center pt-5  ">Report A Fraud</p>

					<div className="col-lg-8 alignmiddle">
						<p className="mainBannerPara text-center fs-16 fw-400 line-height-25 ">
							An e-service provided by Tawuniya enables the beneficiary to
							submit complaints related to fraud. The team going to research,
							study, and analysis of the report pertaining to fraud, technically
							and legally.
						</p>
					</div>

					<div className="container-fluid">
						<div className="row aligncenter pt-3">
							<div className="col-4 pb-5 mb-5">
								<Card
									className={maincard1}
									value="1"
									onClick={() => {
										handlechange("medicalFraud");
									}}
								>
									<div className="alignright pt-3">
										<NormalRadioButton
											type="radio"
											name="radioOne"
											checked={
												radioButtonChecked === "medicalFraud" ? true : false
											}
										/>
									</div>
									<Typography gutterBottom variant="h5" component="div">
										{showform1 ? (
											<>
												<img className="alignimage" src={Car} />
											</>
										) : (
											<>
												<img className="alignimage" src={Logo1} />
											</>
										)}
									</Typography>

									<h6 className="mainBannerPara text-center fw-800">
										{" "}
										Report Medical Fraud
									</h6>

									<p className="mainBannerPara fs-15 pb-4 text-center  line-height-25">
										Unlimited Third party liability
									</p>
								</Card>
							</div>

							<div className="col-4 pb-5 mb-5">
								<Card
									className={maincard2}
									onClick={() => {
										handlechange("motorFraud");
									}}
								>
									<div className="alignright pt-3">
										<NormalRadioButton
											type="radio"
											name="radioOne"
											checked={
												radioButtonChecked === "motorFraud" ? true : false
											}
										/>
									</div>

									<Typography gutterBottom variant="h5" component="div">
										{showform2 ? (
											<>
												<img className="alignimage" src={Car} />
											</>
										) : (
											<>
												<img className="alignimage" src={Logo2} />
											</>
										)}
									</Typography>

									<h6 className="mainBannerPara text-center fw-800">
										Report Motor Fraud
									</h6>

									<p className="mainBannerPara fs-15 pb-4  text-center  line-height-25">
										Unlimited Third party liability
									</p>
								</Card>
							</div>

							<div className="col-4 pb-5 mb-5">
								<Card
									className={maincard3}
									onClick={() => {
										handlechange("travelFraud");
									}}
								>
									<div className="alignright pt-3">
										<NormalRadioButton
											type="radio"
											name="radioOne"
											checked={
												radioButtonChecked === "travelFraud" ? true : false
											}
										/>
									</div>

									<Typography gutterBottom variant="h5" component="div">
										{showform3 ? (
											<>
												<img className="alignimage" src={Car} />
											</>
										) : (
											<>
												<img className="alignimage" src={Logo3} />
											</>
										)}
									</Typography>

									<h6 className="mainBannerPara text-center fw-800">
										Report Property & Casualty Fraud{" "}
									</h6>

									<p className="mainBannerPara fs-15 pb-4  text-center  line-height-25">
										Unlimited Third party liability
									</p>
								</Card>
							</div>
						</div>
					</div>
					{showform1 ? (
						<>
							<MedicalFraudForm data={data} />
						</>
					) : (
						<></>
					)}
					{showform2 ? (
						<>
							<MotorFraudForm />
						</>
					) : (
						<></>
					)}
					{showform3 ? (
						<>
							<PropertyFraudForm />
						</>
					) : (
						<></>
					)}
				</div>
			</div>
		</React.Fragment>
	);
};
