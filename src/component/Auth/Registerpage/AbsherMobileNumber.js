import React, { useState } from "react";
import { history } from "service/helpers";
import { NormalButton } from "component/common/NormalButton/index";
import "./style.scss";
import { withRouter } from "react-router-dom";
import PhoneNumberInput from "component/common/PhoneNumberInput";
import { dialingCodes } from "component/common/MockData/index";

const AbsherMobileNumberMain = () => {
	const [absherInput, setAbsherInput] = useState({ phoneNumber: "" });
	let [selectedCode, setSelectedCode] = useState(dialingCodes[0]);

	const handleInputChange = (e) => {
		const { name, value } = e.target || e || {};
		setAbsherInput({ ...absherInput, [name]: value });
	};
	const { phoneNumber } = absherInput;

	return (
		<div className="absher-authentication-sublayer pb-3">
			<p className="fs-34 fw-800 absher-authentication-Header text-center pt-5">
				Please provide your new mobile number
			</p>
			<p className="fs-18 fw-400 absher-authentication-Header text-center pt-2">
				The provided number will be used for all future logins and interactions.
			</p>

			<div className="pt-4 mb-4">
				<PhoneNumberInput
					className="complaintPhoneInput"
					selectInputClass="complaintSelectInputWidth"
					selectInputFlexType="complaintSelectFlexType"
					dialingCodes={dialingCodes}
					selectedCode={selectedCode}
					setSelectedCode={setSelectedCode}
					value={phoneNumber}
					name="phoneNumber"
					onChange={handleInputChange}
				/>
			</div>
			<div className="pb-5">
				<NormalButton
					label="Continue"
					className="absher-authentication-ContinueBtn p-4"
					onClick={() => history.push("/register/verify")}
				/>
			</div>
		</div>
	);
};

export const AbsherMobileNumber = withRouter(AbsherMobileNumberMain);
