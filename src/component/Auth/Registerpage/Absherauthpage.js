import React, { useEffect, useState } from "react";
import axios from "axios";
import { history } from "service/helpers";
import { withRouter } from "react-router-dom";
import { AbsherMobileNumber } from "./AbsherMobileNumber";
import { DatePickerInput } from "component/common/DatePickerInput";
import { NormalButton } from "component/common/NormalButton/index";
import { NormalSearch } from "component/common/NormalSearch";
import iquamaIcon from "assets/svg/iquamaIcon.svg";
import absherLogo from "assets/svg/absherAuthLogo.svg";
import "./style.scss";

const AbsherauthpageData = (props) => {
	const [absherAuth, setAbsherAuth] = useState(true);

	const [absherInput, setAbsherInput] = useState({ userId: "" });

	const [changeMobileNumber, setChangeMobileNumber] = useState(false);

	const handleAbsherInputChange = (e) => {
		const { name, value } = e.target || e || {};
		setAbsherInput({ ...absherInput, [name]: value });
	};

	// const [transactionId, setTransactionId] = useState("");

	// const [saveOtp, setSaveOtp] = useState("");

	const handleSubmit = async (e) => {
		e.preventDefault();
		let body = {
			doCustomerRegistrationRequest: {
				password: "1234",
				iqamaId: absherInput.userId,
				mobileNumber: "966566515190",
				"re-enterPassword": "1234",
				langId: "E",
				eMail: "test24@mail.com",
			},
		};
		axios
			.post(
				"https://webapispreprod.tawuniya.com.sa:5556/gateway/TawnTawtheeq/1.0/TawnTawtheeq/restful/doCustomerRegistration",
				body,
				{
					headers: {
						twanapikey: "537fa365-100f-4c96-a054-e2ae30a97268",
						"Content-Type": "application/json",
						Accept: "application/json",
						"Access-Control-Allow-Methods": "GET,PUT,POST,DELETE,PATCH,OPTIONS",
						"Access-Control-Allow-Origin": "http://localhost:3000",
						"Access-Control-Allow-Headers":
							"Content-Type, Authorization, X-Requested-With",
						"Access-Control-Allow-Credentials": "true",
					},
				}
			)
			.then((data) => {
				console.log(data, "hi");
				if (data.data.doCustomerRegistrationResponse.errorCode === "S") {
					// setTransactionId(
					//   data.data.doCustomerRegistrationResponse.transactionId
					// );
					// setSaveOtp(data.data.doCustomerRegistrationResponse.otp);
					alert(data.data.doCustomerRegistrationResponse.errorDescription);
					const timer = setTimeout(() => {
						let body = {
							confirmRequest: {
								otp: data.data.doCustomerRegistrationResponse.verificationCode,
								transactionId:
									data.data.doCustomerRegistrationResponse.transactionId,
								langId: "E",
							},
						};
						axios
							.post(
								"https://webapispreprod.tawuniya.com.sa:5556/gateway/TawnTawtheeq/1.0/TawnTawtheeq/restful/confirmOTP",
								body,
								{
									headers: {
										twanapikey: "537fa365-100f-4c96-a054-e2ae30a97268",
										"Content-Type": "application/json",
										Accept: "application/json",
										"Access-Control-Allow-Methods":
											"GET,PUT,POST,DELETE,PATCH,OPTIONS",
										"Access-Control-Allow-Origin": "http://localhost:3000",
										"Access-Control-Allow-Headers":
											"Content-Type, Authorization, X-Requested-With",
										"Access-Control-Allow-Credentials": "true",
									},
								}
							)
							.then((data) => {
								console.log(data, "hi");
								if (data.data.confirmResponse.errorCode === "S") {
									history.push("/login");
								} else {
									alert(data.data.confirmResponse.errorDescription);
								}
							})
							.catch((error) => console.log(error));
					}, 3000);
					return () => clearTimeout(timer);
					// history.push("/register/verify")
				} else {
					alert(data.data.doCustomerRegistrationResponse.errorDescription);
				}
			})
			.catch((error) => console.log(error));
	};

	const linkYourPolicy = () => {
		return (
			<div className="absherBox pt-4">
				<p className="fs-34 fw-800 absherHeader text-center pt-5">
					Link your policies
				</p>
				<div className="d-flex justify-content-center">
					<p className="absher-displayMessageText fs-16 fw-400 line-height-22 text-center">
						We can not find any policy authorised with a phone number;
						additional authentications are required once to manage your
						policies.
					</p>
				</div>
				<div className="py-3 px-4">
					<NormalButton
						label="Authurise my ID through Absher"
						className="absherBtn p-4"
						onClick={() => setAbsherAuth(false)}
					/>
				</div>
				<div className="pb-5">
					<NormalButton label="Skip" className="absherSkipBtn p-4" />
				</div>
			</div>
		);
	};

	const numberAssociated = () => {
		return (
			<div className="absherBox pt-4">
				<div className="">
					<p className="absher-displayMessageTextAssociated">
						Your registered mobile number associated is
					</p>
					<label className="text-center w-100 fw-800 fs-30 mb-0">
						0551222631
					</label>
					<p className="absher-displayMessageTextAssociated">
						Still need to change the number?
					</p>
				</div>
				<div className="py-3 px-4">
					<NormalButton
						label="Change My Mobile Number"
						className="absherBtn p-4"
						onClick={() => setAbsherAuth(false)}
					/>
				</div>
				<div className="pb-5">
					<NormalButton label="Skip" className="absherSkipBtn p-4" />
				</div>
			</div>
		);
	};

	const paramsData = new URLSearchParams(props.location.search);
	const numberFind = paramsData.get("numberFind");
	useEffect(() => {
		setAbsherAuth(true);
	}, [numberFind]);
	console.log(absherAuth, "numberFindnumberFind");

	return (
		<div className="row">
			<div className="col-lg-12 col-12 absherContainer px-5 pt-0 mt-lg-4">
				<div className="pt-lg-0">
					{absherAuth ? (
						numberFind ? (
							numberAssociated()
						) : (
							linkYourPolicy()
						)
					) : (
						<div className="absher-authentication-Box">
							{!numberFind && (
								<div className="absher-authentication-sublayer pb-3">
									<p className="fs-34 fw-800 absher-authentication-Header text-center pt-5">
										Absher Authintication
									</p>
									<img
										src={absherLogo}
										className="img-fluid mx-auto d-block"
										alt="logo"
									/>
									<div className="pt-4">
										<NormalSearch
											className="absherInputFieldOne"
											name="userId"
											value={absherInput.userId}
											placeholder="Saudi ID or Iqama Number"
											onChange={handleAbsherInputChange}
											needLeftIcon={true}
											leftIcon={iquamaIcon}
										/>
									</div>
									<div className="py-4">
										<DatePickerInput
											datePickerClass="absherDatePadding"
											alignDateIconTop="absherDateIcon"
											placeHolder="Year of Birth"
											needSelectIcon={true}
										/>
									</div>
									<div className="pb-5">
										<NormalButton
											label="Continue"
											className="absher-authentication-ContinueBtn p-4"
											onClick={handleSubmit}
										/>
									</div>
								</div>
							)}

							{numberFind && <AbsherMobileNumber />}
						</div>
					)}
				</div>
			</div>
		</div>
	);
};

export const Absherauthpage = withRouter(AbsherauthpageData);
