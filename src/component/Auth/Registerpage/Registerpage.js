import React, { useState } from "react";
import axios from "axios";
import { history } from "service/helpers";
import PhoneNumberInput from "component/common/PhoneNumberInput";
import { dialingCodes } from "component/common/MockData/index";
import { NormalSearch } from "component/common/NormalSearch";
import { NormalButton } from "component/common/NormalButton";
import { DatePickerInput } from "component/common/DatePickerInput";
import iquamaIcon from "assets/svg/iquamaIcon.svg";
import userProfile from "assets/svg/userProfileIcon.svg";
import "react-datepicker/dist/react-datepicker.css";
import "./style.scss";

export const Registerpage = () => {
	const [registerInput, setRegisterInput] = useState({ userId: "" });

	const [register, setRegister] = useState(false);

	let [selectedCode, setSelectedCode] = useState(dialingCodes[0]);

	const handleInputChange = (e) => {
		const { name, value } = e.target || e || {};
		setRegisterInput({ ...registerInput, [name]: value });
	};

	return (
		<div className="row">
			<div className="col-12 authRegisterContainer">
				<div>
					<div className="authRegisterBox">
						{register ? (
							<div className="subRegisterLayer">
								<p className="fs-34 fw-800 text-center registerHeader pt-5">
									Login & Join!
								</p>
								<div>
									<NormalSearch
										className="registerInputFieldOne"
										name="userId"
										value={registerInput.userId}
										placeholder="Saudi ID or Iqama Number"
										onChange={handleInputChange}
										needLeftIcon={true}
										leftIcon={iquamaIcon}
									/>
								</div>
								<div className="py-4">
									<DatePickerInput
										alignDateIconTop="registerDateIcon"
										placeHolder="Year of Birth"
										needSelectIcon={true}
									/>
								</div>
								<div className="pb-4">
									<NormalButton
										label="Continue"
										className="registerContinueBtn p-4"
										onClick={() => history.push("/register/verify")}
									/>
								</div>
								<div className="d-flex justify-content-center">
									<p className="fs-14 fw-400 register-termsConditionText text-center">
										By continuing, you agree to Tawuniya's{" "}
										<span className="register-termsText">Terms of Service</span>
										, <span className="register-termsText">Privacy Policy</span>
									</p>
								</div>
								<div className="pt-2 pb-5">
									<NormalButton
										label="Reset Mobile Number"
										className="register-resetMobileNoBtn p-4"
										onClick={() => setRegister(false)}
									/>
								</div>
							</div>
						) : (
							<div className="subNewRegisterLayer">
								<div className="d-flex justify-content-center">
									<p className="fs-28 fw-800 text-center registerNewHeader text-center m-0 pt-5">
										Welcome to Tawuniya,
									</p>
								</div>
								<p className="fs-14 fw-400 registerNewPara text-center pt-2 pb-3 m-0">
									Glad to have you here, few more data are required for joining
									us.{" "}
								</p>
								<div>
									<PhoneNumberInput
										className="registerPhoneInput"
										selectInputClass="registerSelectInputWidth"
										selectInputFlexType="registerSelectFlexType"
										dialingCodes={dialingCodes}
										selectedCode={selectedCode}
										setSelectedCode={setSelectedCode}
										value={registerInput.userId}
										name="phoneNumber"
										onChange={handleInputChange}
									/>
								</div>
								<div className="py-3">
									<NormalSearch
										className="registerNewInputFieldOne"
										name="userId"
										value={registerInput.userId}
										placeholder="Your Name"
										onChange={handleInputChange}
										needLeftIcon={true}
										leftIcon={userProfile}
									/>
								</div>
								<div className="pb-2">
									<NormalButton
										label="Continue"
										className="registerNewContinueBtn p-4"
										onClick={() =>
											history.push("/register/absher-authentication")
										}
									/>
								</div>
								<div className="pt-3 pb-2">
									<NormalButton
										label="Reset Mobile Number"
										className="register-new-resetMobileNoBtn p-4"
									/>
								</div>
							</div>
						)}
					</div>
				</div>
			</div>
		</div>
	);
};
