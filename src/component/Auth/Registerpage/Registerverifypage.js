import React, { useEffect, useState } from "react";
import axios from "axios";
import { history } from "service/helpers";
import { NormalButton } from "component/common/NormalButton/index";
import PinInput from "react-pin-input";
import "./style.scss";

export const Registerverifypage = () => {
  const [timer, setTimer] = useState(60);

  const [newTimer, setNewTimer] = useState(60);

  const [addNo, setAddNo] = useState(false);

  const [addNewNo, setAddNewNo] = useState(false);

  const [resend, setResend] = useState(false);

  const [getOtp, setGetOtp] = useState("");

  const resendCode = () => {
    setResend(true);
    setNewTimer(60);
  };

  useEffect(() => {
    const time = setTimeout(() => {
      let timerLength = timer - 1;
      if (history.location.pathname === "/register/verify") {
        setTimer(timerLength);
      }
      if (timerLength === 9) {
        setAddNo(true);
      }
      if (timerLength <= 0) {
        setTimer(0);
      }
      if (resend) {
        setNewTimer(newTimer - 1);
        if (newTimer - 1 === 9) {
          setAddNewNo(true);
        }
        if (newTimer - 1 <= 0) {
          setNewTimer(0);
        }
      }
    }, 1000);
    return () => clearTimeout(time);
  }, [timer, newTimer, addNo, resend]);

  const validateRegisterOtpHandler = () => {
    let body = {
      confirmRequest: {
        transactionId: "7000911508-6594217717",
        langId: "E",
        otp: getOtp,
      },
    };
    axios
      .post(
        "https://webapispreprod.tawuniya.com.sa:5556/gateway/TawnTawtheeq/1.0/TawnTawtheeq/restful/confirmOTP",
        body,
        {
          headers: {
            twanapikey: "537fa365-100f-4c96-a054-e2ae30a97268",
            "Content-Type": "application/json",
            Accept: "application/json",
            "Access-Control-Allow-Methods": "GET,PUT,POST,DELETE,PATCH,OPTIONS",
            "Access-Control-Allow-Origin": "http://localhost:3000",
            "Access-Control-Allow-Headers":
              "Content-Type, Authorization, X-Requested-With",
            "Access-Control-Allow-Credentials": "true",
          },
        }
      )
      .then((data) => {
        if (data) {
        }
      })
      .catch((error) => console.log(error));
  };

  return (
    <div className="row">
      <div className="col-lg-12 col-12 registerVerifyContainer px-5 pt-5 mt-lg-5">
        <div className="pt-lg-5">
          <div className="registerVerifyBox">
            <p className="fs-34 fw-800 verifyRegisterHeader text-center pt-5">
              Verification
            </p>
            <div className="d-flex justify-content-center">
              <p className="register-displayMessageText fs-16 fw-400 line-height-22 text-center">
                An SMS will be sent to the following mobile number you have
                registered with us:{" "}
                <span className="fs-16 fw-500">xxxxxxxx7313</span>
              </p>
            </div>
            <p className="fs-20 fw-800 register-verifyTitlePara text-center m-0 pb-2">
              Verification Code
            </p>
            <div className="px-lg-5">
              <PinInput
                length={4}
                initialValue={getOtp}
                onChange={(value, index) => {
                  setGetOtp(getOtp);
                  console.log(value);
                }}
                inputMode="numeric"
                secret
                style={{
                  padding: "10px",
                  display: "flex",
                  justifyContent: "center",
                }}
                inputStyle={{
                  border: "none",
                  backgroundColor: "#F2F3F5",
                  borderRadius: "4px",
                  width: "100%",
                  marginRight: "2%",
                  minHeight: "90px",
                }}
                autoSelect={true}
                regexCriteria={/^[ A-Za-z0-9_@./#&+-]*$/}
              />
            </div>
            <div className="pt-3 px-5 mx-3">
              <NormalButton label="Verify" className="verifyRegisterBtn p-4" onClick={validateRegisterOtpHandler} />
            </div>
            <p className="fs-14 fw-400 register-resendText text-center pt-3 pb-1">
              Resend code in 00:{" "}
              {resend ? (
                <span>
                  {addNewNo && 0}
                  {newTimer}
                </span>
              ) : (
                <span>
                  {addNo && 0}
                  {timer}
                </span>
              )}
            </p>
            <p className="fs-14 fw-400 register-noVerifyCodeMsg text-center pb-5">
              Don’t receive Code?{" "}
              <span className="register-resendAgainLink" onClick={resendCode}>
                Resend
              </span>
            </p>
          </div>
        </div>
      </div>
    </div>
  );
};
