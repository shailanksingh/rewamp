import React, { useEffect, useState } from "react";
import axios from "axios";
import { useDispatch, useSelector } from "react-redux";
import { history } from "service/helpers";
import { loginVerifyTranslateData } from "component/common/MockData";
import { NormalButton } from "component/common/NormalButton/index";
import PinInput from "react-pin-input";
import "./style.scss";
import { setLoginResponse } from "action/LoginResponse";

export const Loginverifypage = () => {
	const [timer, setTimer] = useState(60);

	const [newTimer, setNewTimer] = useState(60);

	const [addNo, setAddNo] = useState(false);

	const [addNewNo, setAddNewNo] = useState(false);

	const [resend, setResend] = useState(false);

	const [saveOtp, setSaveOtp] = useState("");

	const resendCode = () => {
		setResend(true);
		setNewTimer(60);
	};

	const dispatch = useDispatch();

	const displayPhnNumber = localStorage.getItem("phoneNumber");

	const getArabStore = useSelector((data) => data.languageReducer.languageArab);

	const validateOtpHandler = () => {
		const getPhnNumber = localStorage.getItem("phoneNumber");
		let body = {
			validateOTP_in: {
				mobile: getPhnNumber,
				channel: "Portal",
				otpCode: saveOtp,
				language: "1",
			},
		};
		axios
			.post(
				"https://webapispreprod.tawuniya.com.sa:5556/gateway/OTP_REST/1.0/TawnAdapterOTP.v1.restful.validateOTP",
				body,
				{
					headers: {
						twanapikey: "537fa365-100f-4c96-a054-e2ae30a97268",
						"Content-Type": "application/json",
						Accept: "application/json",
						"Access-Control-Allow-Methods": "GET,PUT,POST,DELETE,PATCH,OPTIONS",
						"Access-Control-Allow-Origin": "http://localhost:3000",
						"Access-Control-Allow-Headers":
							"Content-Type, Authorization, X-Requested-With",
						"Access-Control-Allow-Credentials": "true",
					},
				}
			)
			.then((data) => {
				if (data.data.validateOTP_out.status === "1") {
					handleSubmit();
				} else {
					alert("رقم غير صحيح. برجاء أعد المحاولة");
				}
				console.log(data);
			})
			.catch((error) => console.log(error));
	};

	const handleSubmit = async (e) => {
		const IqamaId = localStorage.getItem("IqamaID");
		let body = {
			loginRequest: {
				username: IqamaId,
				yearOfBirth: "1988",
				langId: "E",
				channel: "MOBILE",
			},
		};

		axios
			.post(
				"https://webapispreprod.tawuniya.com.sa:5556/gateway/TawnTawtheeq/TawnTawtheeq/v2/restful/customerLogin",
				body,
				{
					headers: {
						twanapikey: "537fa365-100f-4c96-a054-e2ae30a97268",
						"Content-Type": "application/json",
						Accept: "application/json",
						"Access-Control-Allow-Methods": "GET,PUT,POST,DELETE,PATCH,OPTIONS",
						"Access-Control-Allow-Origin": "http://localhost:3000",
						"Access-Control-Allow-Headers":
							"Content-Type, Authorization, X-Requested-With",
						"Access-Control-Allow-Credentials": "true",
					},
				}
			)
			.then((data) => {
				console.log(data);
				if (data.data.loginResponse.status === "Success") {
					dispatch(setLoginResponse(data.data.loginResponse));
					history.push("/dashboard");
				} else {
					alert("login failed");
				}
			})
			.catch((error) => console.log(error));
	};

	useEffect(() => {
		const time = setTimeout(() => {
			let timerLength = timer - 1;
			if (history.location.pathname === "/login/verify") {
				setTimer(timerLength);
			}
			if (timerLength === 9) {
				setAddNo(true);
			}
			if (timerLength <= 0) {
				setTimer(0);
			}
			if (resend) {
				setNewTimer(newTimer - 1);
				if (newTimer - 1 === 9) {
					setAddNewNo(true);
				}
				if (newTimer - 1 <= 0) {
					setNewTimer(0);
				}
			}
		}, 1000);
		return () => clearTimeout(time);
	}, [timer, newTimer, addNo, resend]);

	return (
		<div className="row">
			<div className="col-lg-12 col-12 authVerifyContainer px-5">
				<div>
					<div className="authVerifyBox" id={getArabStore && "authVerifyBox"}>
						<p className="fs-22 fw-800 verifyLoginHeader text-center">
							{getArabStore
								? loginVerifyTranslateData.cardTitleAr
								: loginVerifyTranslateData.cardTitleEn}
						</p>
						<div className="d-flex justify-content-center">
							<p className="displayMessageText fs-12 fw-400 line-height-18 text-center m-0 pb-2">
								{getArabStore
									? loginVerifyTranslateData.cardParaAr
									: loginVerifyTranslateData.cardParaEn}
								<span className="fs-16 fw-500">{displayPhnNumber}</span>
							</p>
						</div>
						<p className="fs-18 fw-800 verifyTitlePara text-center m-0 pb-2">
							{getArabStore
								? loginVerifyTranslateData.cardVerifyTitleAr
								: loginVerifyTranslateData.cardVerifyTitleEn}
						</p>
						<div >
							<PinInput
								length={4}
								initialValue={saveOtp}
								onChange={(value, index) => {
									setSaveOtp(value);
									console.log(value);
								}}
								inputMode="numeric"
								// secret
								style={{
									padding: "10px",
									display: "flex",
									justifyContent: "center",
								}}
								inputStyle={{
									border: "none",
									backgroundColor: "#F2F3F5",
									borderRadius: "4px",
									width: "100%",
									marginRight: "2%",
									minHeight: "90px",
								}}
								autoSelect={true}
								regexCriteria={/^[ A-Za-z0-9_@./#&+-]*$/}
							/>
						</div>
						<div className="pt-3 px-2 mx-1">
							<NormalButton
								label="Verify"
								className="verifyLoginBtn"
								onClick={validateOtpHandler}
							/>
						</div>
						{getArabStore ? (
							<React.Fragment>
								<p className="fs-14 fw-400 resendText text-center pt-3">
									{loginVerifyTranslateData.resendTxtAr}
									{resend ? (
										<span>
											{addNewNo && 0}
											{newTimer}
										</span>
									) : (
										<span>
											{addNo && 0}
											{timer}
										</span>
									)}
								</p>

								<p className="fs-14 fw-400 noVerifyCodeMsg text-center m-0 ">
									{getArabStore
										? loginVerifyTranslateData.noResendTxtOneAr
										: "Don’t receive Code?"}
									<span className="resendAgainLink" onClick={resendCode}>
										{getArabStore
											? loginVerifyTranslateData.noResendTxtTwoAr
											: "Resend"}
									</span>
								</p>
							</React.Fragment>
						) : (
							<React.Fragment>
								<p className="fs-14 fw-400 resendText text-center pt-3">
									Resend code in 00:{" "}
									{resend ? (
										<span>
											{addNewNo && 0}
											{newTimer}
										</span>
									) : (
										<span>
											{addNo && 0}
											{timer}
										</span>
									)}
								</p>
								<p className="fs-14 fw-400 noVerifyCodeMsg text-center m-0 ">
									Don’t receive Code?{" "}
									<span className="resendAgainLink" onClick={resendCode}>
										Resend
									</span>
								</p>
							</React.Fragment>
						)}
					</div>
				</div>
			</div>
		</div>
	);
};
