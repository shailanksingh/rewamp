import React, { useState } from "react";
import axios from "axios";
import { useSelector } from "react-redux";
import { history } from "service/helpers";
import { loginTranslateData } from "component/common/MockData/index";
import PhoneNumberInput from "component/common/PhoneNumberInput";
import { dialingCodes } from "component/common/MockData/index";
import { NormalSearch } from "component/common/NormalSearch";
import { NormalButton } from "component/common/NormalButton";
import { DatePickerInput } from "component/common/DatePickerInput";
import iquamaIcon from "assets/svg/iquamaIcon.svg";
import userProfile from "assets/svg/userProfileIcon.svg";
import "react-datepicker/dist/react-datepicker.css";
import "./style.scss";

export const Loginpage = ({ arabToggle }) => {
	const [inpOne, setInpOne] = useState({ userId: "", phn: "" });

	const [loginInput, setLoginInput] = useState({ userId: "" });

	const [login, setLogin] = useState(true);

	let [selectedCode, setSelectedCode] = useState(dialingCodes[0]);

	const [phnNo, setPhnNo] = useState("");

	const [checkLogin, setCheckLogin] = useState(false);

	const getArabStore = useSelector((data) => data.languageReducer.languageArab);

	console.log(getArabStore, "jsjs");

	const handleInputChange = (e) => {
		const { name, value } = e.target || e || {};
		setInpOne({ ...inpOne, [name]: value });
	};

	const handleLoginInputChange = (e) => {
		const { name, value } = e.target || e || {};
		setLoginInput({ ...loginInput, [name]: value });
	};

	const resetMobileNumber = () => {
		setLogin(false);
		history.push("/register/absher-authentication");
	};

	const sendOtp = () => {
		localStorage.setItem("phoneNumber", selectedCode.code + inpOne.phn);
		const getPhnNumber = localStorage.getItem("phoneNumber");
		let body = {
			sendOTP_in: {
				mobile: getPhnNumber,
				channel: "Portal",
				businessService: "TRAVEL",
				language: "2",
			},
		};
		if (getPhnNumber) {
			setPhnNo(getPhnNumber);
			axios
				.post(
					"https://webapispreprod.tawuniya.com.sa:5556/gateway/OTP_REST/1.0/TawnAdapterOTP.v1.restful.sendOTP",
					body,
					{
						headers: {
							twanapikey: "537fa365-100f-4c96-a054-e2ae30a97268",
							"Content-Type": "application/json",
							Accept: "application/json",
							"Access-Control-Allow-Methods":
								"GET,PUT,POST,DELETE,PATCH,OPTIONS",
							"Access-Control-Allow-Origin": "http://localhost:3000",
							"Access-Control-Allow-Headers":
								"Content-Type, Authorization, X-Requested-With",
							"Access-Control-Allow-Credentials": "true",
						},
					}
				)
				.then((data) => {
					history.push("/login/verify");
					localStorage.setItem("IqamaID", inpOne.userId);
					console.log(data, "hhaha");
				})
				.catch((error) => console.log(error));
		}
	};

	const validateLoginHandler = async (e) => {
		e.preventDefault();
		let body = {
			doCustomerValidateRequest: {
				iqamaId: inpOne.userId,
				langId: "E",
			},
		};

		axios
			.post(
				"https://webapispreprod.tawuniya.com.sa:5556/gateway/TawnTawtheeq/1.0/TawnTawtheeq/restful/doCustomerValidation",
				body,
				{
					headers: {
						twanapikey: "537fa365-100f-4c96-a054-e2ae30a97268",
						"Content-Type": "application/json",
						Accept: "application/json",
						"Access-Control-Allow-Methods": "GET,PUT,POST,DELETE,PATCH,OPTIONS",
						"Access-Control-Allow-Origin": "http://localhost:3000",
						"Access-Control-Allow-Headers":
							"Content-Type, Authorization, X-Requested-With",
						"Access-Control-Allow-Credentials": "true",
					},
				}
			)
			.then((data) => {
				if (data.data.doCustomerValidateResponse.registrationFlag === "1") {
					setCheckLogin(true);
				} else {
					alert("Invalid User!");
					history.push("/register");
				}
			})
			.catch((error) => console.log(error));
	};

	return (
		<div className="row">
			<div
				className="col-12 authLoginContainer px-lg-5"
				id={checkLogin ? "" : "authLoginContainer"}
			>
				<div
					className="authLoginBox"
					id={getArabStore ? "alignArabLoginCardRight" : null}
				>
					{login ? (
						<div className="subAuthLayer">
							<p className="fs-22 fw-800 text-center font-Lato loginHeader">
								{getArabStore
									? loginTranslateData.cardTitleAr
									: loginTranslateData.cardTitleEn}
							</p>
							<div className="pb-3">
								<NormalSearch
									className="loginInputFieldOne"
									name="userId"
									value={inpOne.userId}
									placeholder={
										getArabStore
											? loginTranslateData.userIdPlaceHolderAr
											: loginTranslateData.userIdPlaceHolderEn
									}
									onChange={handleInputChange}
									needLeftIcon={true}
									leftIcon={iquamaIcon}
								/>
							</div>
							{checkLogin && (
								<React.Fragment>
									<div className="pb-3">
										<DatePickerInput
											datePickerClass="loginDatePadding"
											alignDateIconTop="loginDateIcon"
											placeHolder={
												getArabStore
													? loginTranslateData.dobPlaceHolderAr
													: loginTranslateData.dobPlaceHolderEn
											}
											needSelectIcon={true}
										/>
									</div>
									<div className="pb-3">
										<PhoneNumberInput
											className="loginPhoneInput"
											selectInputClass="loginSelectInputWidth"
											selectInputFlexType="loginSelectFlexType"
											dialingCodes={dialingCodes}
											selectedCode={selectedCode}
											setSelectedCode={setSelectedCode}
											value={inpOne.phn}
											name="phn"
											onChange={handleInputChange}
										/>
									</div>
								</React.Fragment>
							)}
							<div className="pb-3">
								<NormalButton
									label={
										getArabStore
											? loginTranslateData.continueBtnAr
											: loginTranslateData.continueBtnEn
									}
									className="authContinueBtn font-Lato py-4"
									onClick={checkLogin ? sendOtp : validateLoginHandler}
								/>
							</div>
							<div className="d-flex justify-content-center pb-4 mb-1">
								{getArabStore ? (
									<p className="fs-16 fw-400 termsConditionTextArab font-Lato text-center m-0">
										{loginTranslateData.termsAr}
									</p>
								) : (
									<p className="fs-11 fw-400 termsConditionText font-Lato text-center m-0">
										By continuing, you agree to Tawuniya's{" "}
										<span className="termsText">Terms of Service</span>,{" "}
										<span className="termsText">Privacy Policy</span>
									</p>
								)}
							</div>
							<div className="pb-5">
								<NormalButton
									label={
										getArabStore
											? loginTranslateData.resetBtnAr
											: loginTranslateData.resetBtnEn
									}
									className="resetMobileNoBtn font-Lato"
									onClick={resetMobileNumber}
								/>
							</div>
						</div>
					) : (
						<div className="subNewLoginLayer">
							<div className="d-flex justify-content-center">
								<p className="fs-34 fw-800 text-center font-Lato loginNewHeader text-center m-0 pt-4">
									Welcome to Tawuniya,
								</p>
							</div>
							<p className="fs-16 fw-400 registerNewPara text-center pt-2 pb-4 m-0">
								Glad to have you here, few more data are required for joining
								us.{" "}
							</p>
							<div>
								<NormalSearch
									className="loginNewInputFieldOne font-Lato"
									name="userId"
									value={loginInput.userId}
									placeholder="Saudi ID or Iqama Number"
									onChange={handleLoginInputChange}
									needLeftIcon={true}
									leftIcon={iquamaIcon}
								/>
							</div>
							<div className="py-4">
								<NormalSearch
									className="loginNewInputFieldOne"
									name="userId"
									value={loginInput.userId}
									placeholder="Your Name"
									onChange={handleLoginInputChange}
									needLeftIcon={true}
									leftIcon={userProfile}
								/>
							</div>
							<div className="pb-2">
								<NormalButton
									label="Continue"
									className="loginNewContinueBtn font-Lato "
									onClick={() => history.push("/login/verify")}
								/>
							</div>
							<div className="pt-4 pb-3">
								<NormalButton
									label="Reset Mobile Number"
									className="login-new-resetMobileNoBtn font-Lato"
								/>
							</div>
						</div>
					)}
				</div>
			</div>
		</div>
	);
};
