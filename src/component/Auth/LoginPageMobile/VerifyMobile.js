import React, { useEffect, useState } from "react";
import PinInput from "react-pin-input";
import { history } from "service/helpers";
import { NormalButton } from "component/common/NormalButton";
import BottomPopup from "../../../component/common/MobileReuseable/BottomPopup";
import "./style.scss";
import axios from "axios";

const VerifyMobile = ({
  isVerifyOpen,
  setIsVerifyOpen,
  setIsOpenLoginModel,
}) => {
  const [resend, setResend] = useState(false);
  const [timer, setTimer] = useState(60);

  const [newTimer, setNewTimer] = useState(60);

  const [addNo, setAddNo] = useState(false);

  const [addNewNo, setAddNewNo] = useState(false);

  const [saveOtp, setSaveOtp] = useState("");

  const resendCode = () => {
    setResend(true);
    setNewTimer(60);
  };

  useEffect(() => {
    const time = setTimeout(() => {
      let timerLength = timer - 1;
      setTimer(timerLength);

      if (timerLength === 9) {
        setAddNo(true);
      }
      if (timerLength <= 0) {
        setTimer(0);
      }
      if (resend) {
        setNewTimer(newTimer - 1);
        if (newTimer - 1 === 9) {
          setAddNewNo(true);
        }
        if (newTimer - 1 <= 0) {
          setNewTimer(0);
        }
      }
    }, 1000);
    return () => clearTimeout(time);
  }, [timer, newTimer, addNo, resend]);

  // submit action
  const validateOtpHandler = () => {
    const getPhnNumber = localStorage.getItem("phoneNumber");
    let body = {
      validateOTP_in: {
        mobile: getPhnNumber,
        channel: "Portal",
        otpCode: saveOtp,
        language: "1",
      },
    };
    axios
      .post(
        "https://webapispreprod.tawuniya.com.sa:5556/gateway/OTP_REST/1.0/TawnAdapterOTP.v1.restful.validateOTP",
        body,
        {
          headers: {
            twanapikey: "537fa365-100f-4c96-a054-e2ae30a97268",
            "Content-Type": "application/json",
            Accept: "application/json",
            "Access-Control-Allow-Methods": "GET,PUT,POST,DELETE,PATCH,OPTIONS",
            "Access-Control-Allow-Origin": "http://localhost:3000",
            "Access-Control-Allow-Headers":
              "Content-Type, Authorization, X-Requested-With",
            "Access-Control-Allow-Credentials": "true",
          },
        }
      )
      .then((data) => {
        if (data.data.validateOTP_out.status === "1") {
          history.push("/home");
          setIsVerifyOpen(false);
          setIsOpenLoginModel(false);
        } else {
          alert("رقم غير صحيح. برجاء أعد المحاولة");
        }
        console.log(data);
      })
      .catch((error) => console.log(error));
  };
  const getPhnNumber = localStorage.getItem("phoneNumber");

  return (
    <BottomPopup open={isVerifyOpen} setOpen={setIsVerifyOpen}>
      <div className="login_page_mobile verify_page">
        <h5>Verification</h5>
        <p>The code has been sent to {getPhnNumber} </p>
        <div className="">
          <PinInput
            length={4}
            initialValue=""
            onChange={(value, index) => {
              setSaveOtp(value);
              console.log(value);
            }}
            inputMode="numeric"
            secret
            style={{
              display: "flex",
              justifyContent: "center",
            }}
            inputStyle={{
              border: "none",
              backgroundColor: "#F2F3F5",
              borderRadius: "4px",
              width: "100%",
              marginRight: "2%",
              minHeight: "90px",
            }}
            autoSelect={true}
            regexCriteria={/^[ A-Za-z0-9_@./#&+-]*$/}
          />
        </div>
        <NormalButton
          label="Verify"
          className="authContinueBtn font-Lato p-4 mt-4"
          onClick={validateOtpHandler}
        />
        <p className="resendText">
          Resend code in 00:
          {resend ? (
            <span>
              {addNewNo && 0}
              {newTimer}
            </span>
          ) : (
            <span>
              {addNo && 0}
              {timer}
            </span>
          )}
        </p>
        <p className="noVerifyCodeMsg">
          Don’t receive Code?{" "}
          <span className="resendAgainLink" onClick={resendCode}>
            Resend
          </span>
        </p>
      </div>
    </BottomPopup>
  );
};

export default VerifyMobile;
