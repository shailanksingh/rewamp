import React, { useState } from "react";
import { DatePickerInput } from "component/common/DatePickerInput";
import { NormalButton } from "component/common/NormalButton";
import { NormalSearch } from "component/common/NormalSearch";
import BottomPopup from "../../../component/common/MobileReuseable/BottomPopup";
import "./style.scss";
import PhoneNumberInput from "component/common/PhoneNumberInput";
import { dialingCodes } from "component/common/MockData";
import VerifyMobile from "./VerifyMobile";
import axios from "axios";
import iquamaIcon from "assets/svg/iquamaIcon.svg";
import "react-datepicker/dist/react-datepicker.css";

const LoginPageMobile = ({ isOpenLoginModel, setIsOpenLoginModel }) => {
  const [isVerifyOpen, setIsVerifyOpen] = useState(false);

  const [inpOne, setInpOne] = useState({ userId: "", phn: "" });

  let [selectedCode, setSelectedCode] = useState(dialingCodes[0]);

  const [phnNo, setPhnNo] = useState("");
  const [checkLogin, setCheckLogin] = useState(false);

  const validateLoginHandler = async (e) => {
    e.preventDefault();
    let body = {
      doCustomerValidateRequest: {
        iqamaId: inpOne.userId,
        langId: "E",
      },
    };

    axios
      .post(
        "https://webapispreprod.tawuniya.com.sa:5556/gateway/TawnTawtheeq/1.0/TawnTawtheeq/restful/doCustomerValidation",
        body,
        {
          headers: {
            twanapikey: "537fa365-100f-4c96-a054-e2ae30a97268",
            "Content-Type": "application/json",
            Accept: "application/json",
            "Access-Control-Allow-Methods": "GET,PUT,POST,DELETE,PATCH,OPTIONS",
            "Access-Control-Allow-Origin": "http://localhost:3000",
            "Access-Control-Allow-Headers":
              "Content-Type, Authorization, X-Requested-With",
            "Access-Control-Allow-Credentials": "true",
          },
        }
      )
      .then((data) => {
        if (data.data.doCustomerValidateResponse.registrationFlag === "1") {
          alert(data.data.doCustomerValidateResponse.errorDescription);
          setCheckLogin(true);
        }
        if (data.data.doCustomerValidateResponse.registrationFlag === "2") {
          alert(data.data.doCustomerValidateResponse.errorDescription);
          // history.push("/register");
        } else {
          alert(data.data.doCustomerValidateResponse.errorDescription);
        }
      })
      .catch((error) => console.log(error));
  };

  const handleInputChange = (e) => {
    const { name, value } = e.target || e || {};
    setInpOne({ ...inpOne, [name]: value });
  };

  const sendOtp = () => {
    const getPhnNumber = localStorage.getItem("phoneNumber");
    let body = {
      sendOTP_in: {
        mobile: getPhnNumber,
        channel: "Portal",
        businessService: "TRAVEL",
        language: "2",
      },
    };
    if (getPhnNumber) {
      setPhnNo(getPhnNumber);
      axios
        .post(
          "https://webapispreprod.tawuniya.com.sa:5556/gateway/OTP_REST/1.0/TawnAdapterOTP.v1.restful.sendOTP",
          body,
          {
            headers: {
              twanapikey: "537fa365-100f-4c96-a054-e2ae30a97268",
              "Content-Type": "application/json",
              Accept: "application/json",
              "Access-Control-Allow-Methods":
                "GET,PUT,POST,DELETE,PATCH,OPTIONS",
              "Access-Control-Allow-Origin": "http://localhost:3000",
              "Access-Control-Allow-Headers":
                "Content-Type, Authorization, X-Requested-With",
              "Access-Control-Allow-Credentials": "true",
            },
          }
        )
        .then((data) => {
          console.log(data, "hhaha");
        })
        .catch((error) => console.log(error));
    }
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    let body = {
      loginRequest: {
        username: inpOne.userId,
        yearOfBirth: "1988",
        langId: "E",
        channel: "MOBILE",
      },
    };

    axios
      .post(
        "https://webapispreprod.tawuniya.com.sa:5556/gateway/TawnTawtheeq/TawnTawtheeq/v2/restful/customerLogin",
        body,
        {
          headers: {
            twanapikey: "537fa365-100f-4c96-a054-e2ae30a97268",
            "Content-Type": "application/json",
            Accept: "application/json",
            "Access-Control-Allow-Methods": "GET,PUT,POST,DELETE,PATCH,OPTIONS",
            "Access-Control-Allow-Origin": "http://localhost:3000",
            "Access-Control-Allow-Headers":
              "Content-Type, Authorization, X-Requested-With",
            "Access-Control-Allow-Credentials": "true",
          },
        }
      )
      .then((data) => {
        console.log(data);
        if (data.data.loginResponse.status === "Success") {
          localStorage.setItem("phoneNumber", selectedCode.code + inpOne.phn);
          sessionStorage.setItem(
            "loginDetails",
            JSON.stringify(data.data?.loginResponse.user_info.fullName)
          );
          sendOtp();
          setIsVerifyOpen(true);
        } else {
          alert("login failed");
        }
      })
      .catch((error) => console.log(error));
  };

  return (
    <div>
      <BottomPopup open={isOpenLoginModel} setOpen={setIsOpenLoginModel}>
        <div className="login_page_mobile">
          <h5>Login & Join!</h5>
          <div className={"card_container"}>
            <div>
              <NormalSearch
                className="loginInputFieldOne"
                name="userId"
                value={inpOne.userId}
                placeholder="Saudi ID or Iqama Number"
                onChange={handleInputChange}
                needLeftIcon={true}
                leftIcon={iquamaIcon}
              />
            </div>

            <DatePickerInput
              alignDateIconTop="loginDateIcon"
              placeHolder="Year of Birth"
              needSelectIcon={true}
            />
            <PhoneNumberInput
              className="loginMobilePhoneInput"
              selectInputClass="loginMobileSelectInputWidth"
              selectInputFlexType="loginMobileSelectFlexType"
              dialingCodes={dialingCodes}
              selectedCode={selectedCode}
              setSelectedCode={setSelectedCode}
              value={inpOne.phn}
              name="phn"
              onChange={handleInputChange}
              placeholder="Mobile number"
            />
            <NormalButton
              label="Continue"
              className={"authContinueBtn font-Lato p-4 mt-4 "}
              onClick={checkLogin ? handleSubmit : validateLoginHandler}
            />
          </div>
        </div>
      </BottomPopup>
      <VerifyMobile
        isVerifyOpen={isVerifyOpen}
        setIsVerifyOpen={setIsVerifyOpen}
        setIsOpenLoginModel={setIsOpenLoginModel}
      />
    </div>
  );
};

export default LoginPageMobile;
