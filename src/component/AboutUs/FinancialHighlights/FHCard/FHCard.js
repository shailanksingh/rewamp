import React from "react";
import "./style.scss";

const FHCard = ({ fhCardTitle, fhCardValue }) => {
	return (
		<div className="fhCard">
			<div className="fhCardTitile">{fhCardTitle}</div>
			<div className="fhCardValue">
				{fhCardValue}
				<span>SR</span>
			</div>
		</div>
	);
};

export default FHCard;
