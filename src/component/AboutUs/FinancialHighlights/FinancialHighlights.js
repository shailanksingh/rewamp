import React, { useRef, useEffect } from "react";
import "./style.scss";
import { FHCardData } from "component/common/MockData";
import FHCard from "./FHCard/FHCard";
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import ArrowForward from "../../../assets/svg/HomeServiceFroward1Arrow.svg";
import ArrowBack from "../../../assets/svg/HomeServiceBackArrow.svg";
import edubadge from "../../../assets/svg/edubadge.svg";
import { FHTimeLineData } from "component/common/MockData";

export const FinancialHighlights = () => {
	useEffect(() => {
		window.scrollTo(0, 0);
	});

	const sliderRef = useRef(null);

	const settings = {
		slidesToShow: 4,
		slidesToScroll: 1,
		arrows: false,
		infinite: true,
		dots: false,
		infinite: true,
		speed: 500,
	};

	return (
		<React.Fragment>
			<div className="financialHighlightsContainer">
				<div className="financialHighlights">
					<h1>
						Financial Highlights of Tawuniya in <span>2021</span>
					</h1>
					<p>
						With the success of its insurance business, Tawuniya has been
						recognized in KSA as an industry leader in insurance and is now
						ranked as a top 10 brand.
					</p>
					<div className="financialHighlightsCards">
						{FHCardData.map((item) => {
							return (
								<FHCard
									key={item}
									fhCardTitle={item.fhCardTitle}
									fhCardValue={item.fhCardValue}
								/>
							);
						})}
					</div>
				</div>
				<div className="financialHighlightsBannerContainer">
					<div className="financialHighlightsBanner">
						<div className="fhBannerText">
							<h5>Competitive Advantages</h5>
							<p>
								We leverage our distinct competitive advantages to drive value
								and unlock opportunities for our business and our stakeholders
							</p>
						</div>
						<div className="fhBannerCards">
							<Slider
								ref={sliderRef}
								{...settings}
								className="fhBannerCardsSlider"
							>
								<div className="fhBannerCardRoot">
									<div className="fhBannerCard">
										<p>
											The highest insurance <span>professional experience</span>{" "}
											in Saudi Arabia that exceeds 37 years
										</p>
									</div>
								</div>
								<div className="fhBannerCardRoot">
									<div className="fhBannerCard">
										<p>
											The highest paid up capital in the Saudi insurance sector
											is
											<span>1,250 million</span> riyals
										</p>
									</div>
								</div>
								<div className="fhBannerCardRoot">
									<div className="fhBannerCard">
										<p>
											A distinct <span>market share of more than 25%</span> of
											the total Saudi insurance market
										</p>
									</div>
								</div>
								<div className="fhBannerCardRoot">
									<div className="fhBannerCard">
										<p>
											An outstanding <span>rating A-</span>; Stable Outlook from
											the Standard and Poor’s Global Ratings
										</p>
									</div>
								</div>
								<div className="fhBannerCardRoot">
									<div className="fhBannerCard">
										<p>
											An outstanding <span>rating A-</span>; Stable Outlook from
											the Standard and Poor’s Global Ratings
										</p>
									</div>
								</div>
							</Slider>

							<div className="arrowDotContainer">
								<div
									className="arrowContainer"
									onClick={() => sliderRef.current.slickPrev()}
								>
									<img src={ArrowBack} />
								</div>
								<div className="dotContainer">
									<p className="dotEl"></p>
									<p className="dotEl"></p>
									<p className="dotEl"></p>
									<p className="dotEl"></p>
									<p className="dotEl"></p>
								</div>

								<div
									className="arrowContainer"
									onClick={() => sliderRef.current.slickNext()}
								>
									<img src={ArrowForward} />
								</div>
							</div>
						</div>
					</div>
				</div>

				<div className="fhRanking">
					<h5>
						Tawuniya <span>Awards & Ranking</span>
					</h5>
					<p>
						See our accomplishments as a globally recognized and trusted
						company.
					</p>
					{FHTimeLineData.map((item) => {
						return (
							<div className="fhRankingTimeline">
								<div className="fhyear">{item.fhyear}</div>
								<div className="fhTimeLineList">
									{item.fhTimeLineList.map((data) => {
										return (
											<div className="fhTimeLine" key={data}>
												<div className="fhEduIcon">
													<img src={edubadge} alt="Education badge" />
													{data.lineTrue && <div className="fhLine"></div>}
												</div>

												<div className="fhText">
													<p>{data.fhTitile}</p>
													<span>{data.fhReference}</span>
												</div>
											</div>
										);
									})}
								</div>
							</div>
						);
					})}
				</div>
			</div>
		</React.Fragment>
	);
};
