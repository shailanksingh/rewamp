import React from "react";
import "./style.scss";

function LatestNewsCard(props) {
  const { Item } = props;
  return (
    <div className="latestNewsCard">
      <div className="DateContainer">
        <button className="buttonEl">{Item.buttonData}</button>
        <p className="dateEl">{Item.date}</p>
      </div>
      <p className="latestNewsCardPar">{Item.description}</p>
    </div>
  );
}

export default LatestNewsCard;
