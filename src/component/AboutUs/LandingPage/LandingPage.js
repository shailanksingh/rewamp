import React from "react";
import TawuniyaLogoAboutus from "../../../assets/svg/TawuniyaLogoAboutus.svg";
import UnionTopHalf from "../../../assets/svg/UnionTopHalf.svg";
import fingerPrint from "../../../assets/svg/fingerPrint.svg";
import AbtUsBannerCard from "component/common/AbtUsBannerCard";
import FixedHeightCard from "../AboutUsComponents/FixedHeightCard/FixedHightCard";
import { AboutUsFixedHeightCard } from "component/common/MockData";
import { AboutUsBannerCards } from "component/common/MockData";
import yr1 from "../../../assets/svg/yr1.svg";
import yr2 from "../../../assets/svg/yr2.svg";
import yr3 from "../../../assets/svg/yr3.svg";
import yr4 from "../../../assets/svg/yr4.svg";
import yr5 from "../../../assets/svg/yr5.svg";
import kpIcon1 from "../../../assets/svg/kpIcon1.svg";
import kpIcon2 from "../../../assets/svg/kpIcon2.svg";
import kpIcon3 from "../../../assets/svg/kpIcon3.svg";
import kpIcon4 from "../../../assets/svg/kpIcon4.svg";
import TawuniyaLogomicro from "../../../assets/svg/Tawuniya-Logo-micro.svg";
import abttawuniyalogobtm from "../../../assets/svg/abttawuniyalogobtm.svg";
import abttawuniyalogotop from "../../../assets/svg/abttawuniyalogotop.svg";
import abtus1 from "../../../assets/images/abtus1.png";
import abtus2 from "../../../assets/images/abtus2.png";
import abtusline from "../../../assets/svg/abtusline.svg";
import { Button } from "@material-ui/core";
import ArrowForwardIcon from "@material-ui/icons/ArrowForward";
import "../style.scss";
import "./AboutUs.scss";
import "./AboutUSMediaQuery.scss";
import LatestNewsCard from "../HelightCard/index";
import FrowardArrowAboutUs from "../../../assets/svg/FrowardArrowAboutUs.svg";

export const AboutUsMain = () => {
	let highlightsCard = [
		{
			buttonData: "News",
			date: "10 May 2022",
			description:
				"Tawuniya appoints Dr. Ammr Kurdi as Chief Financial Officer",
		},
		{
			buttonData: "News",
			date: "2 days ago",
			description:
				"Tawuniya is the first company in the Kingdom to install vehicle insurance policies",
		},
		{
			buttonData: "Promotion",
			date: "Week ago",
			description: "Tawuniya launches Covid-19 Travel Insurance program",
		},
		{
			buttonData: "Event",
			date: "2 weeks ago",
			description:
				"“Tawuniya Vitality” changes the healthy lifestyle of the Saudi Society New program is a first for the Kingdom, the Middle East and North Africa",
		},
		{
			buttonData: "News",
			date: "10 May 2022",
			description:
				"Tawuniya appoints Dr. Ammr Kurdi as Chief Financial Officer",
		},
	];
	return (
		<React.Fragment>
			<div className="AboutUsContainer AbtUs">
				<div className="AbtUsTopBanner">
					<div className="AbtUsLeft">
						<img className="AbtTawuiyaLogo" src={TawuniyaLogoAboutus} alt="" />
						<div className="AbtUsTextContainer">
							<img src={UnionTopHalf} alt="" />
							<div className="AbtUsText">
								<h5>
									Our mission <br />& values
								</h5>
								<p>
									Exceed expectations through superior customer experience and
									service excellence.
								</p>
							</div>
						</div>
					</div>
					<div className="AbtUsCardsContainer">
						{AboutUsBannerCards.map((item) => {
							const { cardTitle, cardPara } = item;
							return (
								<AbtUsBannerCard
									cardTitleColor="#EE7500"
									cardTitle={cardTitle}
									cardPara={cardPara}
								/>
							);
						})}
					</div>
				</div>
				<div style={{ margin: "0px" }}>
					<div className="AbtUsMain">
						<img src={fingerPrint} alt="" />
						<div className="AbtUsMainText">
							<h1>We are </h1>
							<h1>
								the <span>Saudi Insurance</span> Pioneer
							</h1>
							<p>
								The Company for Cooperative Insurance (Tawuniya) is a Saudi
								Joint Stock Company, and it was incorporated on January 18,
								1986, under Commercial Registration No. 1010061695. Tawuniya is
								the first national insurance company licensed in the Kingdom of
								Saudi Arabia to practice all types of insurance business in
								accordance with the cooperative insurance principle that is
								accepted by Islamic Sharyia.
							</p>
							<p>
								Tawuniya has obtained the license number ت م ن / 1/200412 from
								the Saudi Central Bank (SAMA), as the first license in the
								insurance sector under the Cooperative Insurance Companies
								Control Law. The company is also regulated and supervised by
								SAMA.
							</p>
							<p>
								Tawuniya provides its customers with more than 60 types of
								insurance including medical, motor, fire, property, engineering,
								casualty, marine, aviation, Takaful, liability insurance, and
								many other types of insurance. Through its 37 years of
								experience, the company has been able to enhance its competitive
								advantages and quality of services to become the first choice
								for all insurance customer segments in Saudi Arabia.
							</p>
							<p>
								The Company’s Head Office is located in Riyadh, Kingdom of Saudi
								Arabia and its addresses are as follows:
							</p>
							<div className="AbtUsMainAddress">
								<div className="AbtUsMainPostal">
									<h5>Postal Address:</h5>
									<p>P.O. Box 86959, Riyadh 11632, Kingdom of Saudi Arabia.</p>
									<p>Tel.: +966 11 2525800 - Fax: +966 11 400 0844</p>
									<p>Toll Free: 800 124 9990</p>
								</div>
								<div className="AbtUsMainNational">
									<h5>National Address:</h5>
									<p>6507 Ath Thumamah Road- Al-Rabie District- Unit No.</p>
									<p>
										55- Riyadh- Zip Code 13315- Additional No. 3445- Saudi
										Arabia.
									</p>
								</div>
							</div>
						</div>
					</div>
					<div className="AbtUsFixedHeightCardContainer">
						{AboutUsFixedHeightCard.map((item) => {
							const {
								cardIcon,
								cardTitle,
								cardPara1,
								cardPara2,
								cardPara3,
								cardBackgroundColor,
								cardTitleColor,
								cardSubtitleColor,
								cardUrl,
							} = item;
							return (
								<FixedHeightCard
									cardIcon={cardIcon}
									cardTitle={cardTitle}
									cardPara1={cardPara1}
									cardPara2={cardPara2}
									cardPara3={cardPara3}
									cardUrl={cardUrl}
									cardBackgroundColor={cardBackgroundColor}
									cardTitleColor={cardTitleColor}
									cardSubtitleColor={cardSubtitleColor}
								/>
							);
						})}
					</div>
					<div className="AbtUsTimeline">
						<h2>
							You guessed it, We’re changing <br />
							the game.
							<span>Creating milestone</span>!
						</h2>
						<p>
							With the success of its insurance business, Tawuniya has been
							recognized in KSA as an industry leader in insurance and is now
							ranked as a top 10 brand.
						</p>
						<div className="content-overflow AbtUsTimeLineSliderContainer">
							<div className="AbtUsTimeLineSlider">
								<div className="AbtUsYr">
									<img className="AbtUsbgImg" src={yr1} alt="year 2001" />
									<img
										className="AbtUsTopLogo"
										src={TawuniyaLogomicro}
										alt="tawuniya logo"
									/>
									{/* <img src={abtusline} alt="dashed line" /> */}
									<div className="dashline"></div>
									<div className="AbtUsYrText">
										<h5>2001</h5>
										<p>
											To keep pace with the changes in customer needs, community
											demands, new technology.
										</p>
									</div>
								</div>
								<div className="AbtUsYr">
									<img className="AbtUsbgImg" src={yr2} alt="year 2001" />
									<img
										className="AbtUsTopLogo"
										src={TawuniyaLogomicro}
										alt="tawuniya logo"
									/>
									{/* <img src={abtusline} alt="dashed line" /> */}
									<div className="dashline"></div>
									<div className="AbtUsYrText">
										<h5>2003</h5>
										<p>
											To keep pace with the changes in customer needs, community
											demands, new technology.
										</p>
									</div>
								</div>
								<div className="AbtUsYr">
									<img className="AbtUsbgImg" src={yr3} alt="year 2001" />
									<img
										className="AbtUsTopLogo"
										src={TawuniyaLogomicro}
										alt="tawuniya logo"
									/>
									{/* <img src={abtusline} alt="dashed line" /> */}
									<div className="dashline"></div>
									<div className="AbtUsYrText">
										<h5>2005</h5>
										<p>
											To keep pace with the changes in customer needs, community
											demands, new technology.
										</p>
									</div>
								</div>
								<div className="AbtUsYr">
									<img className="AbtUsbgImg" src={yr4} alt="year 2001" />
									<img
										className="AbtUsTopLogo"
										src={TawuniyaLogomicro}
										alt="tawuniya logo"
									/>
									{/* <img src={abtusline} alt="dashed line" /> */}
									<div className="dashline"></div>
									<div className="AbtUsYrText">
										<h5>2010</h5>
										<p>
											To keep pace with the changes in customer needs, community
											demands, new technology.
										</p>
									</div>
								</div>
								<div className="AbtUsYr">
									<img className="AbtUsbgImg" src={yr5} alt="year 2001" />
									<img
										className="AbtUsTopLogo"
										src={TawuniyaLogomicro}
										alt="tawuniya logo"
									/>
									{/* <img src={abtusline} alt="dashed line" /> */}
									<div className="dashline"></div>
									<div className="AbtUsYrText">
										<h5>2011</h5>
										<p>
											To keep pace with the changes in customer needs, community
											demands, new technology.
										</p>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div className="AbtUsKeyPromises">
						<div className="AbtUskpCardsContainer">
							<div className="AbtUskpCardLeft">
								<div className="AbtUskpCard">
									<img src={kpIcon1} alt="" />
									<h5>Passionate about our people</h5>
									<p>To keep pace with the changes in customer needs.</p>
								</div>
								<div className="AbtUskpCard">
									<img src={kpIcon2} alt="" />
									<h5>Customer centric</h5>
									<p>
										To keep pace with the changes in customer needs, community
										demands.
									</p>
								</div>
							</div>
							<div className="AbtUskpCardRight">
								<div className="AbtUskpCard">
									<img src={kpIcon3} alt="" />
									<h5>Digital-first</h5>
									<p>
										To keep pace with the changes in customer needs, community
										demands, new technology.
									</p>
								</div>
								<div className="AbtUskpCard">
									<img src={kpIcon4} alt="" />
									<h5>Extraordinary results</h5>
									<p>
										To keep pace with the changes in customer needs, community
										demands, new technology.
									</p>
								</div>
							</div>
						</div>
						<div className="AbtUskpTextContainer">
							<div className="AbtUsKpText">
								<h2>
									Tawuniya has structured
									<br /> multiple programs to <br />
									support delivering its <span> key promises</span>,
								</h2>
								<p>
									achieve success and transfer its ambitious strategy into
									reality.
								</p>
							</div>
						</div>
					</div>
					<div className="AbtUsCorpGov">
						<div className="AbtusCorpGovLeft">
							<h2>Corporate Governanace</h2>
							<div className="CorpGovCard">
								<h5>Corporate Governance</h5>
								<p>
									Tawuniya updated its governance regulations to comply with the
									Insurance Companies Governance Regulations issued by the Saudi
									Arabian Monetary Authority and the Corporate Governance
									Regulations issued by the Capital Market Authority (CMA). The
									Company complies with both governance regulations.
								</p>
								<p>
									The governance guidelines include standards covering the
									company's corporate governance policies, its strategic
									direction, the required culture, and the guiding principles
									followed at the core of the functional areas of the company.
								</p>
								<Button
									size="small"
									color="primary"
									href="#outlined-buttons"
									endIcon={<ArrowForwardIcon />}
									style={{ fontWeight: 800 }}
								>
									Tawuniya Corporate Governance Regulations
								</Button>
							</div>
						</div>
						<div className="AbtUsCorpGovRight">
							<div className="CorpGovCard">
								<h5>Shariah Review</h5>
								<p>
									Tawuniya transacts insurance business in accordance with the
									cooperative principle that is Islamiclly approved. The Company
									contracted with the Shariyah Review Bureau, to conduct a
									comprehensive review of the company's activities, products and
									related polices as well as its investments.
								</p>
								<p>
									According to the Annual Shari'a Supervisory Report issued in
									10.2.2022, the Tawuniya’s business and activities are in
									compliance with Shariah provisions and controls.
								</p>
								<Button
									size="small"
									color="primary"
									href="#outlined-buttons"
									endIcon={<ArrowForwardIcon />}
									style={{ fontWeight: 800 }}
								>
									the Annual Shari’a Supervisory Report- 2022
								</Button>
							</div>
						</div>
					</div>
					<div className="AbtUsStrategy">
						<img className="AbtUsStaBgTop" src={abttawuniyalogotop} alt="" />
						<img className="AbtUsStaBgBtm" src={abttawuniyalogobtm} alt="" />
						<div className="AbtUsStaText">
							<h5>Our Strategy </h5>
							<div className="AbtUsStaTextPara">
								<p>
									Tawuniya launched its 2025 Strategy to keep pace with the
									fundamental changes in the market affecting locally and
									globally, and to contribute more in supporting the national
									economy and serving the Saudi society at large, where the
									entire world is witnessing today rapid and pivotal changes in
									the business sector and all areas of life. In addition, the
									needs and expectations of customers and requirements have
									varied as well the society and its perceptions, and the
									introduction of modern technologies to the labor market that
									made it more competitive and dynamic, and raised the limit of
									expectations and increased the challenges size.
								</p>
								<p>
									Through its ambitious strategy, Tawuniya looks forward to new
									horizons that define unique features for the insurance sector
									in the Kingdom, and aspires to be the largest insurance
									company in the Middle East and North Africa region, through
									excellence in business, services and product innovation to
									excel in serving its valued customers, and progress in
									supporting the passion of its employees and achieving market
									leadership, providing digital solutions that stimulate the
									insurance sector, and expanding in the region to achieve
									sustainable financial results and profits that go beyond the
									market.
								</p>
							</div>
						</div>
						<div className="AbtUsStaCard">
							<img className="abtus1" src={abtus1} alt="" />
							<img className="abtus2" src={abtus2} alt="" />
						</div>
					</div>

					{/* <div className="highLightsContainer">
						<div className="highLightsContainerText">
							<div>Join our community</div>
							<h1 className="highLightsHead">
								Tawuniya Now,
								<span style={{ color: "#B3B9C3" }}>Highlights</span>
							</h1>
							<p className="highLightsPar">
								We provide the best and trusted products for our customers
							</p>
						</div>
						<div className="cardContainer">
							{highlightsCard.map((item, i) => {
								return <LatestNewsCard Item={item} key={i} />;
							})}
						</div>
						<div className="ViewAllProductContainer">
							<p className="viewAllProductPar">View All updates</p>
							<img src={FrowardArrowAboutUs} alt="FrowardArrowAboutUs" />
						</div>
					</div> */}
				</div>
			</div>
		</React.Fragment>
	);
};
