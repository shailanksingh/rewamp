import React from "react";
import Card from "../Card/Card";
import "./style.scss";

import ChairmanImage from "../../../../assets/images/chairman.png";
import ViceChairmanImage from "../../../../assets/images/viceChairman.png";
import Director1Image from "../../../../assets/images/director1.png";
import Director2Image from "../../../../assets/images/director2.png";
import Director3Image from "../../../../assets/images/director3.png";

let chairmanData = [{
    image: ChairmanImage,
	name: "Mr. Abdulaziz Ibrahim AlNowiaser",
    position: "Chairman",
}, {
    image: ViceChairmanImage,
	name: "Mr. Abdulaziz Abdulrahman AlKhamis",
    position: "Vice - Chairman",
}];
let directorsData = [{
    image: ViceChairmanImage,
	name: "Mr. Waleed Abdulrahman AlEisa",
    position: "Director",
}, {
    image: Director1Image,
	name: "Mr. Hamood Abdullah AlTuwaijri",
    position: "Director",
}, {
    image: Director2Image,
	name: "Mr. Ehab Mohammed AlDabbagh",
    position: "Director",
}, {
    image: Director3Image,
	name: "Mr. Ghassan Abdulkarim AlMalki",
    position: "Director",
}, {
    image: ViceChairmanImage,
	name: "Mr. Jasser Abdullah AlJasser",
    position: "Director",
}, {
    image: Director3Image,
	name: "Mr. Abdulrahman Mohammed AlOdan",
    position: "Director",
}, {
    image: Director2Image,
	name: "Dr. Khaled Abdulaziz AlGhunaim",
    position: "Director",
}];

let officersData = [{
    image: ViceChairmanImage,
	name: "Mr. Abdulaziz H. Alboug",
    position: "Chief Executive Officer (CEO)",
}, {
    image: Director1Image,
	name: "Dr. Othman Alkassabi",
    position: "Health Sector Chief Executive Officer",
}, {
    image: Director2Image,
	name: "Mr. Mansoor Abuthnein",
    position: "Motor Sector Chief Executive Officer",
}, {
    image: Director3Image,
	name: "Mr. Sultan Alkhomashi",
    position: "P&C Sector Chief Executive Officer",
}, {
    image: ViceChairmanImage,
	name: "Mr. Adel Alhamoudi",
    position: "Chief CX and Marketing Officer",
}, {
    image: Director3Image,
	name: "Dr. Ammr Kurdi",
    position: "Chief Financial Officer",
}, {
    image: Director2Image,
	name: "Mr. Fahad Almoammar",
    position: "Chief Investment Officer",
}, {
    image: Director2Image,
	name: "Mr . Thamer Alharthi",
    position: "Chief Human Capital Officer",
}, {
    image: Director2Image,
	name: "Mr . Marwan Alghamdi",
    position: "Chief Strategy Officer",
}];

const TreeView = () => {
	return (
		<React.Fragment>
            <div className="board-of-directors-tree-view">
                <div className="chairman-details">
                    {chairmanData.map(({ image, name, position }, index) => {
                        return (
                            <div className="card-root">
                                <Card 
                                    key={index.toString()}
                                    image={image}
                                    name={name}
                                    position={position}
                                />
                            </div>
                        );
                    })}
                </div>
                <div className="director-details">
                    {directorsData.map(({ image, name, position }, index) => {
                        return (
                            <div className="card-root">
                                <Card 
                                    key={index.toString()}
                                    image={image}
                                    name={name}
                                    position={position}
                                />
                            </div>
                        );
                    })}
                </div>
                <div className="officer-details">
                    {officersData.map(({ image, name, position }, index) => {
                        return (
                            <div className="card-root">
                                <Card 
                                    key={index.toString()}
                                    image={image}
                                    name={name}
                                    position={position}
                                />
                            </div>
                        );
                    })}
                </div>
            </div>
		</React.Fragment>
	);
};

export default TreeView;
