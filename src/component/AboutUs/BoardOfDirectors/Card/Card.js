import React from "react";
import "./style.scss";

const Card = ({ 
    image = "",
    name,
    position,
}) => {
	return (
		<React.Fragment>
			<div className="board-of-directors-card">
				<div className="image">
                    <img src={image} alt="..." />
                </div>
                <div className="details-card">
                    <div className="name fw-700">{name}</div>
                    <div className="position fw-400">{position}</div>
                </div>
			</div>
		</React.Fragment>
	);
};

export default Card;
