import React, { useEffect } from "react";
import TreeView from "./TreeView/TreeView";
import "./style.scss";
import App from "component/AboveTest/App";

export const BoardOfDirectors = () => {
	useEffect(() => {
		window.scrollTo(0, 0);
	});

	return (
		<React.Fragment>
			<div className="board-of-directors-page">
				{/* <App /> */}
				<div className="title-section">
					<div className="page-title fw-700">
						Board Of Directors & Senior Executives
					</div>
					<p className="page-description fw-700">
						With the success of its insurance business,Tawuniya has been
						recognized in KSA as an industry leader in insurance and is now
						ranked as a top 10 brand.
					</p>
				</div>
				<TreeView />
			</div>
		</React.Fragment>
	);
};
