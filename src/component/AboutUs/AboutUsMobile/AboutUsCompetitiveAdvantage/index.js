import React from "react";
import "./style.scss";
import building from "assets/news/building.png";
import FooterMobile from "component/common/MobileReuseable/FooterMobile";

const AboutUsCompetitiveAdvantage = () => {
  return (
    <div>
      <div className="Aboutus_Competitive_AdvantagePage">
        <div className="building">
          <img className="competitive_image" src={building} />
          <div className="competitive_body">
          <h5>Competitive Advantages</h5>
          <p>
            We leverage our distinct competitive advantages to drive value and
            unlock opportunities for our business and our stakeholders
          </p>
          </div>
        </div>

        <div className="white_box">
          <div className="competitive_white_box_one">
            <p>
              {" "}
              The highest insurance professional experience in Saudi Arabia that
              exceeds 37 years
            </p>
          </div>
          <div className="competitive_white_box_two">
            <p>
              The highest paid up capital in the Saudi insurance sector is 1,250
              million riyals
            </p>
          </div>
          <div className="competitive_white_box_three">
            <p>
              A distinct market share of more than 25% of the total Saudi
              insurance market
            </p>
          </div>
          <div className="competitive_white_box_four">
            <p>
              An outstanding rating A-; Stable Outlook from the Standard and
              Poor’s Global Ratings
            </p>
          </div>
        </div>
        <br />

        <FooterMobile />
      </div>
   </div>
  );
};

export default AboutUsCompetitiveAdvantage;
