import React from "react";
import salambhai from "../../../../assets/svg/Social/salambhai.png";
import shakebhai from "../../../../assets/svg/Social/shakebhai.png";
import salmankhan from "../../../../assets/svg/Social/salmankhan.png";

function Executive() {
  return (
    <div>
      <p className="ms-2 mb-1 fw-bold director-text mx-3">Executive</p>
      <div className="next-pic">
        <div className="ms-2 mt-2 mx-2 ">
          <div className="">
            <img alt="..." className="ms-2" src={shakebhai} />
          </div>
          <div className="salam-text ms-3 mx-2">
            <p className=" mt-2 mb-0 text-center fw-bold directors_card_text">
              Mr. Abdulaziz H. Alboug
            </p>
            <p className=" text-muted text-center chair-text">
              Chief Executive Officer (CEO)
            </p>
          </div>
        </div>
        <div className="ms-2 mt-2 mx-2 ">
          <div className="">
            <img alt="..." className="ms-2" src={shakebhai} />
          </div>
          <div className="salam-text ms-3 mx-2">
            <p className=" mt-2 mb-0 text-center fw-bold directors_card_text">
              Mr. Abdulaziz H. Alboug
            </p>
            <p className=" text-muted text-center chair-text">
              Chief Executive Officer (CEO)
            </p>
          </div>
        </div>

        <div className="ms-2 mt-2 mx-1">
          <div>
            <img alt="..." className="ms-2" src={salmankhan} />
          </div>
          <div className="salam-text ms-3 mx-2">
            <p className=" mt-2 mb-0 text-center fw-bold directors_card_text">
              Dr. Othman Alkassabi
            </p>
            <p className=" text-muted text-center chair-text">
              Health Sector Chief Executive Officer
            </p>
          </div>
        </div>

        <div className="ms-2 mt-2 mx-2">
          <div>
            <img alt="..." className="ms-2" src={salambhai} />
          </div>
          <div className="salam-text ms-3 mx-2">
            <p className=" mt-2 mb-0 text-center fw-bold directors_card_text">
              Mr. Mansoor Abuthnein
            </p>
            <p className=" text-muted text-center chair-text">
              Motor Sector Chief Executive Officer
            </p>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Executive;
