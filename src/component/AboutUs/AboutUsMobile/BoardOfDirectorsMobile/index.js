import HeaderStepsSticky from "component/common/MobileReuseable/HeaderStepsSticky";
import HeaderStickyMenu from "component/common/MobileReuseable/HeaderStickyMenu";
import React from "react";
import Aboutpage from "./Aboutpage";
import "./style.scss";
import "bootstrap/dist/css/bootstrap.css";
import Directors from "component/AboutUs/AboutUsMobile/BoardOfDirectorsMobile/Directors";
import Executive from "./Executive";
import FooterMobile from "component/common/MobileReuseable/FooterMobile";

const BoardOfDirectorsMobile = () => {
  return (
    <div>
      <Aboutpage />
      <Directors />
      <Executive />
      <FooterMobile />
    </div>
  );
};

export default BoardOfDirectorsMobile;
