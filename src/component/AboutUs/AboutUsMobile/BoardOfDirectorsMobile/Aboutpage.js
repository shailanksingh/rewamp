import React from "react";
import "./style.scss";
import salambhai from "../../../../assets/svg/Social/salambhai.png";
import shakebhai from "../../../../assets/svg/Social/shakebhai.png";
import frame from "../../../../assets/svg/Social/Frame.png";
function Aboutpage() {
  return (
    <div>
      <div className=" board_footer mb-5 mx-2">
        <h5 className="mt-3 ms-3 mx-2">
          Board Of Directors & Senior Executives
        </h5>
        <p className="success-font mt-3 ms-3 mx-2">
          With the success of its insurance business, Tawuniya <br/>has been
          recognized in KSA as an industry leader in insurance and is now ranked
          as a top 10 brand.
        </p>
      </div>
      <div className="board_directors_card mb-2">
        <h5 className="ms-3  mx-3">Chairmain</h5>
        {/* <div className="salam-text">
                <p className="ms-2 mb-5 salampic">MrAbdulaziz Ibrahim AlNowiaser </p>
                </div> */}
      </div>
      <div className="next-pic ">
        <div className="ms-2 mt-2 mx-2">
          <div>
            <img className="ms-2" src={salambhai} />
          </div>
          <div className="salam-text ms-3 mx-2">
            <p className=" mt-2 mb-0  text-center fw-bold directors_card_text">
              Mr. Abdulaziz Ibrahim <br/>AlNowiaser
            </p>
            <p className=" text-muted text-center chair-text">Chairmain</p>
          </div>
        </div>

        <div className="ms-2 mt-2 mx-1">
          <div>
            <img className="ms-2" src={shakebhai} />
          </div>
          <div className="salam-text ms-3 mx-2">
            <p className=" mt-2 mb-0 text-center fw-bold directors_card_text">
              Mr. Abdulaziz Ibrahim <br/>AlNowiaser
            </p>
            <p className=" text-muted text-center chair-text">Vice - Chairman
</p>
          </div>
        </div>
      </div>
    </div>
  );
}
export default Aboutpage;
