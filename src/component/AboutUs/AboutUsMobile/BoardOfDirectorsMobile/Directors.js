import React from "react";
import "./style.scss";
import salambhai from "../../../../assets/svg/Social/salambhai.png";
import shakebhai from "../../../../assets/svg/Social/shakebhai.png";
import salmankhan from "../../../../assets/svg/Social/salmankhan.png";

function Director() {
  return (
    <div>
      <p className="ms-2 mb-1 fw-bold director-text mx-3">Director</p>
      <div className="next-pic">
        <div className="ms-2 mt-2 mx-2 ">
          <div>
            <img alt="..." className="ms-2" src={shakebhai} />
          </div>
          <div className="salam-text ms-3 mx-2">
            <p className=" mt-2 mb-0 text-center fw-bold directors_card_text">
              Mr. Waleed Abdulrahman <br />
              AlEisa
            </p>
            <p className=" text-muted text-center chair-text">Director</p>
          </div>
        </div>

        <div className="ms-2 mt-2 mx-1">
          <div>
            <img alt="..." className="ms-2" src={salmankhan} />
          </div>
          <div className="salam-text ms-3 mx-2">
            <p className=" mt-2 mb-0 text-center fw-bold directors_card_text">
              Mr. Hamood Abdullah <br />
              AlTuwaijri
            </p>
            <p className=" text-muted text-center chair-text">Director</p>
          </div>
        </div>

        <div className="ms-2 mt-2 mx-2">
          <div>
            <img alt="..." className="ms-2" src={salambhai} />
          </div>
          <div className="salam-text ms-3 mx-2">
            <p className=" mt-2 mb-0 text-center fw-bold directors_card_text">
              Mr. Ehab Mohammed <br />
              AlDabbagh
            </p>
            <p className=" text-muted text-center chair-text">Chairmain</p>
          </div>
        </div>
      </div>
    </div>
  );
}
export default Director;
