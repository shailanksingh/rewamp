import React from "react";
import HeaderStepsSticky from "component/common/MobileReuseable/HeaderStepsSticky";
import HeaderStickyMenu from "component/common/MobileReuseable/HeaderStickyMenu";
import Competitive from "assets/images/mobile/competitive.svg";
import award from "assets/images/mobile/award.png";
import HighlightBg from "assets/images/mobile/highlight-bg.png";
import ArrowRightWhite from "assets/images/mobile/right-arrow-white.png";
import "./style.scss";
import { relatedNewsData } from "component/common/MockData";
import SlideCard from "component/common/SliderCard/SlideCard";
import RightArrowWithText from "component/common/RightArrowWithText";
import FooterMobile from "component/common/MobileReuseable/FooterMobile";

const FinancialHighlightsMobile = () => {
  return (
    <div>
      <div className="financial-mobile">
        <div className="awards">
          <h5>
            Tawuniya <span>Awards & Ranking</span>
          </h5>
          <p className="awards-accomplishments">
            See our accomplishments as a globally recognized and trusted
            company.
          </p>
          <div className="awards-title">
            <h6>2021</h6>
            <ul>
              <li>
                <div className="about_icon">
                  <img src={award} />
                </div>
                <div className="content">
                  <h6> Excellence in Customer Service- KSA</h6>
                  <p>By Global Brands Magazine</p>
                </div>
              </li>
              <li>
                <div className="about_icon">
                  <img src={award}></img>
                </div>
                <div className="content">
                  <h6>Best Health Insurance Brand in Saudi Arabia</h6>
                  <p>By Global Brands Magazine</p>
                </div>
              </li>
              <li>
                <div className="about_icon">
                  <img src={award}></img>
                </div>
                <div className="content">
                  <h6>Best Auto Insurance Company-KSA </h6>
                  <p>By Global Banking and Finance Review</p>
                </div>
              </li>
              <li>
                <div className="about_icon">
                  <img src={award}></img>
                </div>
                <div className="content">
                  <h6>
                    Best Shared Value Insurance Concept – Tawuniya Vitality –
                    Saudi Arabia 2021
                  </h6>
                  <p>By International Finance Awards</p>
                </div>
              </li>
              <li>
                <div className="about_icon">
                  <img src={award}></img>
                </div>
                <div className="content">
                  <h6>
                    Outstanding Contribution to Social Impact in Saudi Arabia
                  </h6>
                  <p>By CFI British Magazine </p>
                </div>
              </li>
              <li>
                <div className="about_icon">
                  <img src={award}></img>
                </div>
                <div className="content">
                  <h6>Best Innovative Insurance Company </h6>
                  <p>International Business Magazine</p>
                </div>
              </li>
              <li>
                <div className="about_icon">
                  <img src={award}></img>
                </div>
                <div className="content">
                  <h6>The best Takaful insurance company in the world </h6>
                  <p>Global Islamic Finance Awards</p>
                </div>
              </li>
              <li>
                <div className="about_icon">
                  <img src={award}></img>
                </div>
                <div className="content">
                  <h6>
                    Top 10 Most Valuable Insurance Brands in the Middle East{" "}
                  </h6>
                  <p>By Brand Finance</p>
                </div>
              </li>
              <li>
                <div className="about_icon">
                  <img src={award}></img>
                </div>
                <div className="content">
                  <h6>Top 100 Companies in the Middle East</h6>
                  <p>By Forbes Magazine</p>
                </div>
              </li>
              <li>
                <div className="about_icon">
                  <img src={award}></img>
                </div>
                <div className="content">
                  <h6>
                    Selection of Tawuniya CEO among Top 50 Influential Leaders-
                    KSA
                  </h6>
                  <p>By Arabia Business Magazine</p>
                </div>
              </li>
            </ul>
          </div>
          <div className="awards-title">
            <h6 className="awards-2020">2020</h6>
            <ul>
              <li>
                <div className="about_icon">
                  <img src={award}></img>
                </div>
                <div className="content">
                  <h6> Excellence in Customer Service- KSA</h6>
                  <p>By Global Brands Magazine</p>
                </div>
              </li>
              <li>
                <div className="about_icon">
                  <img src={award}></img>
                </div>
                <div className="content">
                  <h6>
                    Best Shared Value Insurance Concept – Tawuniya Vitality –
                    Saudi Arabia 2021
                  </h6>
                  <p>By International Finance Awards</p>
                </div>
              </li>
              <li>
                <div className="about_icon">
                  <img src={award}></img>
                </div>
                <div className="content">
                  <h6>
                    Selection of Tawuniya CEO among Top 50 Influential Leaders-
                    KSA
                  </h6>
                  <p>By Arabia Business Magazine</p>
                </div>
              </li>
            </ul>
          </div>
        </div>
      </div>
      <FooterMobile />
    </div>
  );
};

export default FinancialHighlightsMobile;
