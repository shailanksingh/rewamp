import Carddisplay from "component/common/MobileReuseable/CardDisplay/Carddisplay";
import FooterMobile from "component/common/MobileReuseable/FooterMobile";
import HeaderStepsSticky from "component/common/MobileReuseable/HeaderStepsSticky";
import HeaderStickyMenu from "component/common/MobileReuseable/HeaderStickyMenu";
import React from "react";
import "./style.scss";

const AboutUsFinancialNew = () => {
  return (
    <div>
      <div className="container financial_news_header pt-3">
        <div>
          <h2>
            Financial Highlights <br />
            of Tawuniya in 2021
          </h2>
        </div>
        <div className="pb-3">
          <p>
            With the success of its insurance business,Tawuniya has been
            recognized in KSA as an industry leader in insurance and is now
            ranked as a top 10 brand.
          </p>
        </div>
      </div>
      <div>
        <Carddisplay />
        <FooterMobile />
      </div>
    </div>
  );
};

export default AboutUsFinancialNew;
