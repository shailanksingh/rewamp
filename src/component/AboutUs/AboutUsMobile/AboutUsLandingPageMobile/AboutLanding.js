import React from "react";
import About from "./About";
import Aboutstart from "./Aboutstart";
import Years from "./Years";
import Promise from "./Promise";
import Aboutcard from "./Aboutcard";

const AboutUsLandingPage = () => {
  return (
    <div>
      <About />
      <Aboutcard />
      <Years />
      <Promise />
      <Aboutstart />
    </div>
  );
};

export default AboutUsLandingPage;
