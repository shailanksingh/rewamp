import React, { useState } from "react";
import many from "assets/about/many.png";
import three from "assets/about/three.png";

function Promise() {
  const [datas, setDatas] = useState([
    {
      id: 1,
      image: many,
      title: "Passionate about our people",
      description: "To keep pace with the changes in customer needs.",
    },
    {
      id: 2,
      image: three,
      title: "Customer centric",
      description: "To keep pace with the changes in customer needs.",
    },
    {
      id: 3,
      image: many,
      title: "Digital-First",
      description: "To keep pace with the changes in customer needs.",
    },
    {
      id: 4,
      image: many,
      title: "Extraordinary Results",
      description: "To keep pace with the changes in customer needs.",
    },
  ]);
  return (
    <div>
      <div className="pro">
        <div>
          Tawuniya has structured multiple programs to support delivering its
        </div>
        <div className="d-flex">
          <div>key promises</div>
          <p>,</p>
        </div>
      </div>
      <div className="promise_achieve">
      achieve success and transfer its ambitious strategy into reality.
      </div>
      <div class="container-fluid">
        <div class="row">
          <div class="promise">
            {datas.map((data) => (
              <div key={data.id} className="promises">
                <div className="">
                  <img src={data.image} alt=""></img>
                </div>
                <div className="promise-title">{data.title}</div>
                <div className="promise-des">{data.description}</div>
              </div>
            ))}
          </div>
        </div>
      </div>
    </div>
  );
}

export default Promise;
