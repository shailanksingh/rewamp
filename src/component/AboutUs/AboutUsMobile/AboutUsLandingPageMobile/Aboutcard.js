import React from "react";
import "./style.scss";

function About() {
  return (
    <div className="d-flex justify-content-center mb-5">
      <div className="card-about">
        <div className="card-body">
          <div className="we">
            We are the
            <div className="d-flex">
              <div className="saudi">Saudi Insurance</div>
              <div className="mx-2">Pioneer</div>
            </div>
          </div>
          <div className="paragraph pt-4">
            The Company for Cooperative Insurance (Tawuniya) is a Saudi Joint
            Stock Company, and it was incorporated on January 18, 1986, under
            Commercial Registration No. 1010061695.{" "}
          </div>
          <div className="paragraph mt-3">
            Tawuniya is the first national insurance company licensed in the
            Kingdom of Saudi Arabia to practice all types of insurance business
            in accordance with the cooperative insurance principle that is
            accepted by Islamic Sharyia.{" "}
          </div>
          <div className="adress">Postal Address:</div>
          <div className="padress">
            P.O. Box 86959, Riyadh 11632, Kingdom of Saudi Arabia. Tel.: +966 11
            2525800 - Fax: +966 11 400 0844 Toll Free: 800 124 9990
          </div>
          <div className="adress">National Address:</div>
          <div className="padress">
            6507 Ath Thumamah Road- Al-Rabie District- Unit No. 55- Riyadh- Zip
            Code 13315- Additional No. 3445- Saudi Arabia.
          </div>
        </div>
      </div>
      <div className="reward"></div>
    </div>
  );
}

export default About;
