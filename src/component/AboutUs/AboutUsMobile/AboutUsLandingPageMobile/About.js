import React, { useState } from "react";
import "./style.scss";
import Rectangle from "assets/about/Rectangle.png";
import Union from "assets/about/Union.png";
import { history } from "service/helpers";
function About() {
  const [datas, setDatas] = useState([
    {
      id: 1,
      title: "Our Mission",
      description:
        "Exceed expectations through superior customer experience and service excellence",
    },
    {
      id: 2,
      title: "Our Purpose",
      description: "Together for a safer life and bigger dreams",
    },
    {
      id: 3,
      title: "Our vision",
      description: "Largest insurer in the MENA region",
    },
  ]);
  return (
    <div>
      <div className="about">About Tawuniya</div>
      <div className="Tawuniya_Rectangle">
        <img src={Rectangle} alt="" ></img>
      </div>
      <div class="">
        <div class="row mission">
          <div class="our-mission">
            {datas?.map((data) => (
              <div
                key={data.id}
                className="mission text-wrap"
                onClick={() => 
                  history.push("/home/aboutus/financialhighlights")
                }
              >
                <div className="our-title">{data.title}</div>
                <div className="our-des d-flex">{data.description}</div>
              </div>
            ))}
          </div>
        </div>
      </div>
    </div>
  );
}

export default About;
