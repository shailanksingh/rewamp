import React, { useState } from "react";
import HeaderStepsSticky from "component/common/MobileReuseable/HeaderStepsSticky";
import HeaderStickyMenu from "component/common/MobileReuseable/HeaderStickyMenu";
import AboutUsLanding from "./AboutLanding.js";
import AboutUsFinancialNew from "../AboutUsFinancialNew";
import FinancialHighlightsMobile from "../FinancialHighlightsMobile/index.js";
import BoardOfDirectorsMobile from "../BoardOfDirectorsMobile/index.js";
import AboutUsCompetitiveAdvantage from "../AboutUsCompetitiveAdvantage";

const AboutUsLandingPageMobile = () => {
  const menuList = [
    {
      id: 1,
      label: "Introduction",
    },
    {
      id: 2,
      label: "Awards & Ranking",
    },
    {
      id: 3,
      label: "Competitive Advantage",
    },
    {
      id: 4,
      label: "Financial Highlights",
    },
    {
      id: 5,
      label: "Directors & Senior Executives",
    },
  ];
  const [activeMenuOption, setActiveMenuOption] = useState(menuList[0]);
  const selectedMenuOption = (data) => {
    setActiveMenuOption(data);
  };
  const { id } = activeMenuOption;
  return (
    <div>
      <div>
        <HeaderStickyMenu />
        <HeaderStepsSticky
          title={"About Tawuniya"}
          isSelect={true}
          selectTitle={activeMenuOption}
          menuList={menuList}
          selectedMenuOption={(data) => selectedMenuOption(data)}
          flexPart={true}
        />
      </div>
      {id === 1 && <AboutUsLanding />}
      {id === 2 && <FinancialHighlightsMobile />}
      {id === 3 && <AboutUsCompetitiveAdvantage />}
      {id === 4 && <AboutUsFinancialNew />}
      {id === 5 && <BoardOfDirectorsMobile />}
    </div>
  );
};

export default AboutUsLandingPageMobile;
