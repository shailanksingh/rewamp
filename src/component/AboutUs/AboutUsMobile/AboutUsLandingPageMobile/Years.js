import React, { useState } from "react";
import Union from "assets/about/Union.png";
import white from "assets/about/white.png";
import black from "assets/about/black.png";

function Years() {
  const [datas, setDatas] = useState([
    {
      id: 1,
      image: Union,
      year: "2001",
      description:
        "To keep pace with the changes in customer needs, community demands, new technology.",
        image_year:white,
    },
    {
      id: 2,
      image: Union,
      year: "2003",
      description:
        "To keep pace with the changes in customer needs, community demands, new technology.",
        image_year:black,
    },
    {
      id: 3,
      image: Union,
      year: "2005",
      description:
        "To keep pace with the changes in customer needs, community demands, new technology.",
        image_year:white,
    },
    {
      id: 4,
      image:Union,
      year: "2010",
      description:
        "To keep pace with the changes in customer needs, community demands, new technology.",
        image_year:white,
    },
    {
      id: 5,
      image:Union,
      year: "2011",
      description:
        "To keep pace with the changes in customer needs, community demands, new technology.",
        image_year:white,
    },
  ]);
  return (
    <div className="about_year">
      <div className="about_milestone">You guessed it, We’re changing the game. Creating milestone!</div>
      <div className="about_insurance">
        With the success of its insurance business, Tawuniya has been recognized
        in KSA as an industry leader in insurance and is now ranked as a top 10
        brand.
      </div>
      <div class="container-fluid">
        <div class="row">
          <div className="d-flex about_customer_year">
            {datas.map((data) => (
              <div key={data.id} className="about_years text-wrap">
                <div>
                  <div className="about_image"><img src={data.image}></img></div>
                  <div className="d-flex">
                    <img src={data.image_year} alt=""></img>
                   <div className="about_milestone_line"></div>              
                  </div>
                </div>
                <div className="about_year_date">{data.year}</div>
                <div className="about_card_des">{data.description}</div>
              </div>
            ))}
          </div>
        </div>
      </div>
    </div>
  );
}

export default Years;