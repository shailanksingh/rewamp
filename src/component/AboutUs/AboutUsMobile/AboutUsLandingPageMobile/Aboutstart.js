import React from "react";
import "./style.scss";
import right_arrow from "assets/images/mobile/right_arrow.png";
import strategy_banner from "assets/images/mobile/strategy_banner.svg";
import ImageSliderText from "component/common/MobileReuseable/ImageSliderText/ImageSliderText";
import choose_tawuniya_image_one from "assets/images/mobile/choose_tawuniya_image_one.svg";
import SlideCard from "component/common/SliderCard/SlideCard";

import { relatedNewsData } from "component/common/MockData";

import RightArrowWithText from "component/common/RightArrowWithText";
import FooterMobile from "component/common/MobileReuseable/FooterMobile";

function Story() {
  let reviewDataList = [
    {
      id: 1,
      title: "Shariah Review",
      des1: "Tawuniya transacts insurance business in accordance with the cooperative principle that is Islamiclly approved. The Company contracted with the Shariyah Review Bureau, to conduct a comprehensive review of the company's activities, products and related polices as well as its investments.",
      des2: " According to the Annual Shari'a Supervisory Report issued in 10.2.2022, the Tawuniya’s business and activities are in compliance with Shariah provisions and controls.",
      fotter: "the Annual Shari’a Supervisory Report- 2022 ",
    },
    {
      id: 2,
      title: "Corporate Governance",
      des1: "Tawuniya updated its governance regulations to comply with the Insurance Companies Governance Regulations issued by the Saudi Arabian Monetary Authority and the Corporate Governance Regulations issued by the Capital Market Authority (CMA). The Company complies with both governance regulations.",
      des2: "The governance guidelines include standards covering the company's corporate governance policies, its strategic direction, the required culture, and the guiding principles followed at the core of the functional areas of the company.",
      fotter: "Tawuniya Corporate Governance Regulations",
    },
  ];

  const chooseTawuniyaData = [
    {
      id: 1,
      banner: choose_tawuniya_image_one,
      label: "Super-Simple Claims",
      description:
        "Smartphone enabled self-inspection processes takes minutes!",
    },
    {
      id: 2,
      banner: choose_tawuniya_image_one,
      label: "Loved by Customers",
      description: "We are trusted by customers in our years of operations!",
    },
    {
      id: 3,
      banner: choose_tawuniya_image_one,
      label: "Super-Simple Claims",
      description: "We are trusted by customers in our years of operations!",
    },
  ];

  return (
    <div className="corporate_governance_container">
      <h6 className="title_section">
        Corporate
        <br /> Governanace
      </h6>
      <div className="review_section">
        {reviewDataList?.map((val) => {
          return (
            <div className="review_section_cards">
              <h6>{val.title}</h6>
              <div className="corporate-des">{val.des1}</div>
              <div className="corporate-des">{val.des2}</div>
              <div className="bottom_review_text">
                <div className="corporate-bottom">{val.fotter}</div>
                <div>
                  <img src={right_arrow} alt="right_arrow" />
                </div>
              </div>
            </div>
          );
        })}
      </div>
      <div className="corporate-our">Our Strategy </div>
      <div className="corporate-strategy">
        <div>
          Tawuniya launched its 2025 Strategy to keep pace with the fundamental
          changes in the market affecting locally and globally, and to
          contribute more in supporting the national economy and serving the
          Saudi society at large, where the entire world is witnessing today
          rapid and pivotal changes in the business sector and all areas of
          life. In addition, the needs and expectations of customers and
          requirements have varied as well the society and its perceptions, and
          the introduction of modern technologies to the labor market that made
          it more competitive and dynamic, and raised the limit of expectations
          and increased the challenges size.
        </div>
        <div className="corporate-ambitious">
          Through its ambitious strategy, Tawuniya looks forward to new horizons
          that define unique features for the insurance sector in the Kingdom,
          and aspires to be the largest insurance company in the Middle East and
          North Africa region, through excellence in business, services and
          product innovation to excel in serving its valued customers, and
          progress in supporting the passion of its employees and achieving
          market leadership, providing digital solutions that stimulate the
          insurance sector, and expanding in the region to achieve sustainable
          financial results and profits that go beyond the market.
        </div>
      </div>

      <FooterMobile />
    </div>
  );
}

export default Story;
