import React from "react";
import "./style.scss";
import { NavLink } from "react-router-dom";

function FixedHeightCard({
  cardIcon,
  cardTitle,
  cardPara1,
  cardPara2,
  cardPara3,
  cardBackgroundColor,
  cardTitleColor,
  cardSubtitleColor,
  cardUrl,
}) {
  return (
    <div
      className="FixedHeightCard"
      style={{ backgroundColor: cardBackgroundColor }}
    >
      <h5 className="fhcTitle" style={{ color: cardTitleColor }}>
        {cardTitle}
      </h5>
      <p className="fhcSubtitle" style={{ color: cardSubtitleColor }}>
        <NavLink to={cardUrl}>
          {cardPara1}{" "}
          {cardPara1 && cardIcon ? (
            <img src={cardIcon} style={{ marginLeft: "20px" }} alt="" />
          ) : (
            ""
          )}
        </NavLink>
      </p>
      <p className="fhcSubtitle" style={{ color: cardSubtitleColor }}>
        <NavLink to={cardUrl}>
          {cardPara2}{" "}
          {cardPara2 && cardIcon ? (
            <img src={cardIcon} style={{ marginLeft: "20px" }} alt="" />
          ) : (
            ""
          )}
        </NavLink>
      </p>
      {cardPara3 && (
        <p className="fhcSubtitle" style={{ color: cardSubtitleColor }}>
          <NavLink to={cardUrl}>
            {cardPara3}{" "}
            {cardPara3 && cardIcon ? (
              <img src={cardIcon} style={{ marginLeft: "20px" }} alt="" />
            ) : (
              ""
            )}
          </NavLink>
        </p>
      )}
    </div>
  );
}

export default FixedHeightCard;
