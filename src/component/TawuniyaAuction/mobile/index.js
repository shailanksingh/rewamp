import React from "react";
import "./style.scss";
import FooterMobile from "component/common/MobileReuseable/FooterMobile";
import HeaderStickyMenu from "component/common/MobileReuseable/HeaderStickyMenu";
const TawuniyaAuctionMobile = () => {
  return (
    <div className="tawuniya_auction_container">
       <HeaderStickyMenu />
      <div className="container">
      <div className="TawuniyaAuction_heading">Tawuniya Auction </div>
      <div className="card container TawuniyaAuction_content">
        <p>Our team was handpicked for their passion for insurance products and services. Whether you are browsing our site or visiting our branches, we are always willing to share our deep knowledge. The most commonly asked questions are covered in our Customer Service.<br/>If you have any specific questions please do not hesitate to contact us by completing Enquiry form or calling our customer services team.g our customer services team.</p>
      </div>
      </div>
      <FooterMobile/>
      
    </div>
  );
};

export default TawuniyaAuctionMobile;
