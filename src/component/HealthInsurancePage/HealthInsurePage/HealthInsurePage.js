import React from "react";
import { CheckBenifits, HealthAppSlider, HealthBlog, HealthInsuranceBanner } from "../HealthInsuranceComponents";
import { IntegratedPrograms } from "../HealthInsuranceComponents/IntegratedPrograms";
import { PreferredPlan } from "../HealthInsuranceComponents/PreferredPlan";
import { FAQMotor } from "../../MotorPage/MotorInsuranceComponent/schema/FAQmotor";
import FrequentlyAsked from "component/common/FrequentlyAsked";

export const HealthInsurePage = () =>{
    return(
        <React.Fragment>
            <HealthInsuranceBanner/>
            <IntegratedPrograms />
            <PreferredPlan />
            <CheckBenifits />
            <HealthAppSlider />
            <FrequentlyAsked FAQ={FAQMotor} />
            <HealthBlog />
        </React.Fragment>
    )
}