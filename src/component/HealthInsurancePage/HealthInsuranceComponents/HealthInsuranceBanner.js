import React, { useState } from "react";
import { NormalRadioButton } from "component/common/NormalRadioButton";
import { InsuranceCard } from "component/common/InsuranceCard";
import { NormalButton } from "component/common/NormalButton/index";
import { NormalSearch } from "component/common/NormalSearch";
import PhoneNumberInput from "component/common/PhoneNumberInput/index";
import claim from "assets/svg/claim.svg";
import telemed from "assets/svg/telemedicine.svg";
import chronic from "assets/svg/chronic.svg";
import memberCard from "assets/svg/memberCard.svg";
import iquamaIcon from "assets/svg/iquamaIcon.svg";
import single from "assets/svg/single.svg";
import group from "assets/svg/Groupsme.svg";
import "react-datepicker/dist/react-datepicker.css";
import KSAFlagImage from "../../../assets/images/ksaFlag.png";
import IndiaFlagImage from "../../../assets/images/indiaFlag.png";
import "./style.scss";

export const HealthInsuranceBanner = () => {
  //initialize state for input field
  const [inpOne, setInpOne] = useState({ userId: "", phone: "" });

  //initialiize state for radio buttons
  const [radioOne, setRadioOne] = useState("Yes");

  const handleInputChange = (e) => {
    const { name, value } = e.target || e || {};
    setInpOne({ ...inpOne, [name]: value });
  };

  const healthInsuranceCardData = [
    {
      id: 0,
      content: "Submit / Track a Claim",
      cardIcon: claim,
      contentAlign: null,
      class: "pr-0",
    },
    {
      id: 1,
      content: "Request Telemedicine",
      cardIcon: telemed,
      contentAlign: null,
      class: "pr-0",
    },
    {
      id: 2,
      content: "Chronic Medication",
      cardIcon: chronic,
      contentAlign: "chronicContent",
      class: "pr-0",
    },
    {
      id: 3,
      content: "Download Health Member Card",
      cardIcon: memberCard,
      contentAlign: "memberContent",
      class: "pr-0",
    },
  ];

  let dialingCodes = [
    {
      code: "+91",
      image: IndiaFlagImage,
    },
    {
      code: "+966",
      image: KSAFlagImage,
    },
  ];

  let [selectedCode, setSelectedCode] = useState(dialingCodes[0]);

  let [phoneNumber, setPhoneNumber] = useState("");

  return (
    <div className="row healthInsuranceContainer">
      <div className="col-lg-7 col-12 pt-5">
        <div className="row">
          <div className="col-lg-12 col-12 pt-5 mt-3">
            <p className="healthTitle fw-800 m-0 pb-4">
              Health Insurance Plan for SME
            </p>
            <p className="healthPara fs-20 fw-400 pb-3 m-0">
              Insurance experts at Tawuniya, the Saudi insurance pioneer, will
              help you to identify, analyze and manage such risks and suggest
              appropriate insurance solutions.
            </p>
            <p className="fw-800 managePolicyTitle m-0">Manage your Policy</p>
            <p className="fs-18 fw-400 policyPara">
              We provide the best and trusted service for our customers
            </p>
            <div className="col-lg-11 col-12 p-0">
            <InsuranceCard
              heathInsureCardData={healthInsuranceCardData}
              textWidth="healthCardTextWidth"
            />
            </div>
          </div>
        </div>
      </div>
      <div className="col-lg-5 col-12 pt-5">
        <div className="insureHealthCard h-100">
          <div className="insureHealthLayerOne">
            <p className="insureHealthCardTitle fw-800 m-0">
              Insure Your Health Now!
            </p>
          </div>

          <div className="insureHealthLayerTwo">
            <div className="row insureSubHealthLayerTwo px-4 mx-1">
              <div className="col-lg-6 col-12">
                <NormalButton
                  alignBtn={true}
                  label="Individual"
                  className="indivudualBtn"
                  needNewBtnIcon={true}
                  newSrc={single}
                  adjustNewIcon="img-fluid pr-3"
                />
              </div>
              <div className="col-lg-6 col-12 pl-0">
                <NormalButton
                  label="Corporate & SMEs"
                  className="smeBtn"
                  needNewBtnIcon={true}
                  newSrc={group}
                  adjustNewIcon="img-fluid pr-3"
                />
              </div>
            </div>

            <div className="subHealthInsureLayer p-4">
              <div className="row">
                <div className="col-12">
                  <NormalSearch
                    className="healthInputFieldOne"
                    name="userId"
                    value={inpOne.userId}
                    placeholder="Natioal ID, Iqama"
                    onChange={handleInputChange}
                    needLeftIcon={true}
                    leftIcon={iquamaIcon}
                  />
                </div>
                <div className="col-12 pt-4">
                  <PhoneNumberInput
                    className="healthPhoneInput"
                    selectInputClass="healthSelectInputWidth"
                    selectInputFlexType="healthFlexType"
                    dialingCodes={dialingCodes}
                    selectedCode={selectedCode}
                    setSelectedCode={setSelectedCode}
                    value={phoneNumber}
                    name="phoneNumber"
                    onChange={({ target: { value } }) => setPhoneNumber(value)}
                  />
                  <p className="fs-16 fw-400 pt-3 companyTerms">
                    Do you want to insure vehicles for your company?
                  </p>
                </div>

                <div className="d-flex flex-row">
                  <div className="pl-3">
                    <NormalRadioButton
                      type="radio"
                      name="radioOne"
                      value="Yes"
                      onChange={(e) => setRadioOne(e.target.value)}
                      radioValue="Yes"
                      checked={radioOne === "Yes"}
                    />
                  </div>
                  <div className="pl-3">
                    <NormalRadioButton
                      type="radio"
                      name="radioOne"
                      value="No"
                      onChange={(e) => setRadioOne(e.target.value)}
                      radioValue="No"
                      checked={radioOne === "No"}
                    />
                  </div>
                </div>
              </div>
            </div>
            <div className="px-5" id="insureHeathBoxConatiner">
              <p className="fs-14 fw-400 insureHealthTerms pb-3">
                By continuing you give Tawuniya advance consent to obtain my
                and/or my dependents' information from the National Information
                Center.
              </p>
              <div className="pb-3">
                <NormalButton
                  label="Buy Now"
                  className="insureHealthNowBtn p-4"
                />
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};
