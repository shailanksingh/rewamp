import React from "react";
import { TawuniyaAppSlider } from "component/common/TawuniyaAppSlider";
import { HomeServices } from "component/HomePage/LandingComponent";
import serviceIcons2 from "../../../assets/svg/TawuniyaServicesIcons/serviceIcons2.svg";
import serviceIcons6 from "../../../assets/svg/TawuniyaServicesIcons/serviceIcons6.svg";
import chat from "../../../assets/svg/chat.svg";
import locate from "../../../assets/svg/locate.svg";
import group from "../../../assets/svg/Group.svg";
import track from "../../../assets/svg/track.svg";
import telMed from "assets/svg/healthTeleMed.svg";
import medHeart from "assets/svg/chronicHealth.svg";

export const HealthAppSlider = () =>{
     const healthServicesData = [
        {
            source: "motorservices",
            serviceTitle: "We are always here for you!",
            serviceSubtitle: "We provide the best and trusted service for our customer",
            serviceCardData: [
                {
                    headingEl: "Request Telemedicine",
                    discrptionEl: "Our services can be trustable, honest",
                    iconE1: `${telMed}`,
                },
                {
                    headingEl: "Chronic Medication Request",
                    discrptionEl: "Our services can be trustable, honest",
                    iconE1: `${medHeart}`,
                },
                {
                    headingEl: "Approval Request Tracking",
                    discrptionEl: "Our services can be trustable, honest.",
                    iconE1: `${serviceIcons2}`,
                },
                {
                    headingEl: "Chat",
                    discrptionEl: "Our services can be trustable, honest and worthy",
                    iconE1: `${chat}`,
                },
                {
                    headingEl: "Complaints",
                    discrptionEl: "Our services can be trustable, honest and worthy",
                    iconE1: `${group}`,
                },
                {
                    headingEl: "Track Claim",
                    discrptionEl: "Our services can be trustable, honest and worthy",
                    iconE1: `${track}`,
                },
                {
                    headingEl: "Locate Us",
                    discrptionEl: "Our services can be trustable, honest and worthy",
                    iconE1: `${locate}`,
                },
                {
                    headingEl: "Road Side Assistance",
                    discrptionEl: "Our services can be trustable, honest and worthy",
                    iconE1: `${serviceIcons6}`,
                },
                {
                    headingEl: "Request Telemedicine",
                    discrptionEl: "Our services can be trustable, honest. ",
                    iconE1: `${serviceIcons2}`,
                },
            ],
        },
    ];
    return(
        <div className="row py-5">
            <div className="col-lg-12 col-12">
                <TawuniyaAppSlider/>
            </div>
            <div className="col-lg-12 col-12">
                <HomeServices HomeServicesData={healthServicesData} />
            </div>
        </div>
    )
}