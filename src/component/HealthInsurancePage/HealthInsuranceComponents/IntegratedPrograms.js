import React from "react";
import { NormalButton } from "component/common/NormalButton";
import { LegendarySupport } from "component/common/FraudDetails";
import { heathInsureCardData } from "component/common/MockData/index";
import vitality from "assets/svg/vitality.svg";
import taj from "assets/svg/taj.svg";
import "./style.scss";

export const IntegratedPrograms = () => {
  const integratedCardData = [
    {
      id: 0,
      icon: vitality,
      title: "Tawuniya Vitality Program",
      content:
        "Tawuniya Vitality program helps you understanding your health status and works with you to have health lifestyle and be more active. Move more to get health improved, and you will have more rewards",
    },
    {
      id: 1,
      icon: taj,
      title: "Taj Program",
      content:
        "This program provides integrated health services for your employees and enables them to take advantages of the health insurance program and have the best healthcare they deserve",
    },
  ];
  return (
    <div className="row integratedContainer">
      <div className="col-lg-12 col-12 pt-5">
        <p className="fw-800 integratedTitle">
          Integrated programs for Health insurance clients
        </p>
      </div>
      {integratedCardData.map((item, index) => {
        return (
          <div className="col-lg-6 col-12 pl-2 px-3 pb-3" key={index}>
            <div className="health-integratedCard p-4">
              <img src={item.icon} className="img-fluid pb-3" alt="icon" />
              <p className="integratedCard-Title fs-28 fw-800 m-0 pb-1">
                {item.title}
              </p>
              <p className="integratedCard-Content fs-14 fw-400">
                {item.content}
              </p>
              <div className="pb-2">
                <NormalButton label="Know More" className="knowmoreBtn p-4" />
              </div>
            </div>
          </div>
        );
      })}
      <div className="col-lg-12 col-12 py-5">
        <LegendarySupport legendarySupportData={heathInsureCardData} />
      </div>
    </div>
  );
};
