import React from "react";
import { FooterAbout } from "component/HomePage/LandingComponent";
import { NormalButton } from "component/common/NormalButton";
import corona from "assets/svg/coronavirus.svg";
import passport from "assets/svg/passport.svg";
import family from "assets/svg/family.svg";
import leftArrow from "assets/svg/blogLeftArrow.svg";
import rightArrow from "assets/svg/blogRightArrow.svg";
import bannerOne from "assets/images/Blog 1.png";
import bannerTwo from "assets/images/Blog 2.png";
import "./style.scss";

export const HealthBlog = () => {
	const otherProductData = [
		{
			id: 0,
			icon: corona,
			otherProductTitle: "Covid-19 Insurance",
			otherProductPara:
				"Covid-19 Travel insurance - for Saudis Program was designed to provide protection for Saudi citizens and supports.",
			otherProductSubTitle: "Benefits:",
			otherProductSubParaOne: "Provides you with the necessary health care",
			otherProductSubParaTwo: "Medical expenses incurred around the world.",
		},
		{
			id: 1,
			icon: passport,
			otherProductTitle: "International Travel Insurance",
			otherProductPara:
				"The company has identified the amount of compensation so that it starts from SR 920 and up to SR 3,000,000 ",
			otherProductSubTitle: "Benefits:",
			otherProductSubParaOne: "Medical Emergency Expenses",
			otherProductSubParaTwo: "Free coverage for children (Under 2 Years)",
		},
		{
			id: 2,
			icon: family,
			otherProductTitle: "My Family Health Insurance",
			otherProductPara:
				"The program covers most of the medical services provided in the outpatient as well as inpatient services",
			otherProductSubTitle: "Benefits:",
			otherProductSubParaOne: "Upto 60 Days Claim Periods",
			otherProductSubParaTwo: "Waive Depreciation",
		},
	];
	return (
		<div className="row blogContainer">
			<div className="col-lg-12 col-12 pt-5">
				<div className="d-flex justify-content-between">
					<div>
						<p className="blogTitle fw-800 m-0">
							Blogs Related to Health Insurance
						</p>
						<p className="blogPara fs-19 fw-400">
							We provide the best and trusted service for our customers
						</p>
					</div>
					<div>
						<div className="d-flex flex-row">
							<div>
								<img src={leftArrow} className="img-fluid" alt="icon" />
							</div>
							<div>
								<img src={rightArrow} className="img-fluid" alt="icon" />
							</div>
						</div>
					</div>
				</div>
			</div>
			<div className="col-lg-12 col-12 blogBannerContainer">
				<div className="row">
					<div className="col-lg-6 col-12 p-0">
						<img
							src={bannerOne}
							className="img-fluid position-relative bannerOnePicture"
							alt="banner"
						/>
						<div className="col-12 bannerOne">
							<p className="blogHeaderContent fw-800 m-0 pb-4">
								Walking daily keeps you away from visiting the doctor!
							</p>
							<p className="blogParaContent fs-14 fw-400">
								Physical activity is vital to health. Despite this fact, the
								survey conducted by the General Authority for
							</p>
							<div className="blogBtnContainerOne">
								<NormalButton label="ReadMore" className="readMoreBtn p-4" />
							</div>
						</div>
					</div>
					<div className="col-lg-6 col-12 p-0">
						<img
							src={bannerTwo}
							className="img-fluid position-relative bannerTwoPicture"
							alt="banner"
						/>
						<div className="col-12 bannerTwo">
							<p className="blogHeaderContent fw-800 m-0 pb-4">
								Just 20 minutes could save your life
							</p>
							<p className="blogParaContent fs-14 fw-400">
								On the occasion of the International Breast Cancer Awareness
								Month, we are honored to collaborate
							</p>
							<div className="blogBtnContainerTwo">
								<NormalButton label="ReadMore" className="readMoreBtn p-4" />
							</div>
						</div>
					</div>
				</div>
			</div>
			<div className="col-lg-12 col-12 pt-5">
				<p className="otherProduct-title fw-800 m-0">
					You may also like our Other Products
				</p>
				<p className="otherProduct-para fs-22 fw-400 pb-3">
					We provide the best and trusted service for our customers
				</p>
			</div>
			{otherProductData.map((item, index) => {
				return (
					<div className="col-lg-4 col-12 pl-3" key={index}>
						<div className="otherProductCard p-4">
							<img src={item.icon} className="img-fluid pt-1 pb-3" alt="icon" />
							<p className="otherProduct-header fs-24 fw-800 m-0 pb-2">
								{item.otherProductTitle}
							</p>
							<p className="otherProduct-para fs-16 fw-400">
								{item.otherProductPara}
							</p>
							<p className="otherProduct-Subheader fs-18 fw-400 m-0 pb-1">
								{item.otherProductSubTitle}
							</p>
							<p className="otherProduct-Subpara fs-18 fw-400 m-0 pb-1">
								{item.otherProductSubParaOne}
							</p>
							<p className="otherProduct-Subpara fs-18 fw-400 m-0 pb-1">
								{item.otherProductSubParaTwo}
							</p>
						</div>
					</div>
				);
			})}
			<div className="col-lg-12 col-12">
				<FooterAbout />
			</div>
		</div>
	);
};
