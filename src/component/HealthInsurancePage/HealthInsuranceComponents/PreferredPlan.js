import React from "react";
import healthPlan from "assets/svg/healthPlan.svg";
import "./style.scss";

export const PreferredPlan = () => {
  const planCardData = [
    {
      id: 0,
      planIcon: healthPlan,
      planTitle: "VIP A/B",
      planContents: [
        {
          id: 0,
          leftText: "Network",
          rightText: "1/+2",
        },
        {
          id: 1,
          leftText: "Annual Limit Per Person",
          rightText: "1,000,000 SR",
        },
        {
          id: 2,
          leftText: "Out patient deductible/coinsurance",
          rightText: "20%",
        },
        {
          id: 3,
          leftText: "Max limit Out patient",
          rightText: "75 SR",
        },
        {
          id: 4,
          leftText: "Max limit Out patient",
          rightText: "100 SR",
        },
      ],
    },
    {
      id: 1,
      planIcon: healthPlan,
      planTitle: "Platinum A/B",
      planContents: [
        {
          id: 0,
          leftText: "Network",
          rightText: "1/+2",
        },
        {
          id: 1,
          leftText: "Annual Limit Per Person",
          rightText: "800,000 SR",
        },
        {
          id: 2,
          leftText: "Out patient deductible/coinsurance",
          rightText: "20%",
        },
        {
          id: 3,
          leftText: "Max limit Out patient",
          rightText: "75 SR",
        },
        {
          id: 4,
          leftText: "Max limit Out patient",
          rightText: "100 SR",
        },
      ],
    },
    {
      id: 2,
      planIcon: healthPlan,
      planTitle: "Gold A/B/C",
      planContents: [
        {
          id: 0,
          leftText: "Network",
          rightText: "1/+2",
        },
        {
          id: 1,
          leftText: "Annual Limit Per Person",
          rightText: "700,000 SR",
        },
        {
          id: 2,
          leftText: "Out patient deductible/coinsurance",
          rightText: "20%",
        },
        {
          id: 3,
          leftText: "Max limit Out patient",
          rightText: "75 SR",
        },
        {
          id: 4,
          leftText: "Max limit Out patient",
          rightText: "100 SR",
        },
      ],
    },
    {
      id: 3,
      planIcon: healthPlan,
      planTitle: "Silver A/B",
      planContents: [
        {
          id: 0,
          leftText: "Network",
          rightText: "1/+2",
        },
        {
          id: 1,
          leftText: "Annual Limit Per Person",
          rightText: "600,000 SR",
        },
        {
          id: 2,
          leftText: "Out patient deductible/coinsurance",
          rightText: "20%",
        },
        {
          id: 3,
          leftText: "Max limit Out patient",
          rightText: "75 SR",
        },
        {
          id: 4,
          leftText: "Max limit Out patient",
          rightText: "100 SR",
        },
      ],
    },
    {
      id: 4,
      planIcon: healthPlan,
      planTitle: "Bronze A/B",
      planContents: [
        {
          id: 0,
          leftText: "Network",
          rightText: "1/+2",
        },
        {
          id: 1,
          leftText: "Annual Limit Per Person",
          rightText: "500,000 SR",
        },
        {
          id: 2,
          leftText: "Out patient deductible/coinsurance",
          rightText: "20%",
        },
        {
          id: 3,
          leftText: "Max limit Out patient",
          rightText: "75 SR",
        },
        {
          id: 4,
          leftText: "Max limit Out patient",
          rightText: "100 SR",
        },
      ],
    },
    {
      id: 5,
      planIcon: healthPlan,
      planTitle: "Basic A/B",
      planContents: [
        {
          id: 0,
          leftText: "Network",
          rightText: "1/+2",
        },
        {
          id: 1,
          leftText: "Annual Limit Per Person",
          rightText: "500,000 SR",
        },
        {
          id: 2,
          leftText: "Out patient deductible/coinsurance",
          rightText: "20%",
        },
        {
          id: 3,
          leftText: "Max limit Out patient",
          rightText: "75 SR",
        },
        {
          id: 4,
          leftText: "Max limit Out patient",
          rightText: "100 SR",
        },
      ],
    },
  ];

  return (
    <div className="row preferPlanContainer pb-5">
      <div className="col-lg-12 col-12">
        <p className="planTitle fw-800 m-0">Choose your Prefered Plan</p>
        <p className="planPara fs-22 fw-400 pb-2">
          We provide the best and trusted service for our customers
        </p>
      </div>
      {planCardData.map((item, index) => {
        return (
          <div className="col-lg-4 col-12 pb-4" key={index}>
            <div className="planCard">
              <div className="d-flex flex-row planContainer">
                <div>
                  <img
                    src={item.planIcon}
                    className="img-fluid pr-4"
                    alt="icon"
                  />
                </div>
                <div>
                  <p className="fs-28 fw-800 planCardHeading">
                    {item.planTitle}
                  </p>
                </div>
              </div>
              {item?.planContents?.map((items, index) => {
                return (
                  <div className="d-flex justify-content-between" key={index}>
                    <div>
                      <p className="planCardLeft fs-16 fw-400">
                        {items.leftText}
                      </p>
                    </div>
                    <div>
                      <p className="planCardRight fs-16 fw-800">
                        {items.rightText}
                      </p>
                    </div>
                  </div>
                );
              })}
              <p className="fs-16 fw-800 show-more-plans-text m-0">Show more</p>
            </div>
          </div>
        );
      })}
    </div>
  );
};
