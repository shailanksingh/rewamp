import React from "react";
import dialysis from "assets/svg/dialysis.svg";
import baby from "assets/svg/baby.svg";
import disability from "assets/svg/disability.svg";
import heart from "assets/svg/heartdamage.svg";
import autism from "assets/svg/autism.svg";
import "./style.scss";

export const CheckBenifits = () => {
  const benifitData = [
    {
      id: 0,
      benifitIcon: dialysis,
      amt: "1,00,000 SR",
      type: "Dialysis",
      class:null,
    },
    {
      id: 1,
      benifitIcon: baby,
      amt: "1,00,000 SR",
      type: "National Program for Early Diagnosis in Newborns",
      class:"babyIcon",
    },
    {
      id: 2,
      benifitIcon: disability,
      amt: "1,00,000 SR",
      type: "Disability",
      class:null,
    },
    {
      id: 3,
      benifitIcon: heart,
      amt: "1,00,000 SR",
      type: "Acquired Damage in Heart Valves",
      class:null,
    },
    {
      id: 4,
      benifitIcon: autism,
      amt: "1,00,000 SR",
      type: "Autism Cases",
      class:null,
    },
  ];
  return (
    <div className="row benifitContainer pb-5">
      <div className="col-lg-12 col-12">
        <p className="fw-800 benifitTitle m-0">Check Out Your Benefits</p>
        <p className="fs-16 fw-400 benifitPara">
          We provide the best and trusted service for our customers
        </p>
      </div>
      {benifitData.map((item, index) => {
        return (
          <div className={`${item.class} benifitCardLayout pr-3 px-3`} key={index}>
            <div className="benifitCard">
              <img src={item.benifitIcon} className={`${item.class} img-fluid`} alt="icon" />
              <p className="fs-34 fw-800 benifitAmt m-0 py-3">{item.amt}</p>
              <p className="fs-17 fw-400 benifitType">{item.type}</p>
            </div>
          </div>
        );
      })}
    </div>
  );
};
