import {
  corporateProductList,
  motorInsuraceData,
  SMEProductList,
} from "component/common/MockData";
import React, { useState } from "react";
import "./style.scss";

const buttonListData = [
  {
    id: 1,
    label: "Individuals",
    sourceList: motorInsuraceData,
  },
  {
    id: 2,
    label: "Enterprise",
    sourceList: corporateProductList,
  },
  {
    id: 3,
    label: "SMEs",
    sourceList: SMEProductList,
  },
];
const ProductHeaderSticky = ({ getActiveProduct }) => {
  const [activeProduct, setActiveProduct] = useState(0);
  const activeProductSource = (index, sourceList) => {
    setActiveProduct(index);
    getActiveProduct(sourceList);
  };
  return (
    <div className="product_sticky_header">
      <div className="all_products">
        <div className="product_body">
          {buttonListData.map((val, index) => {
            return (
              <button
                className={activeProduct === index && "active"}
                onClick={() => activeProductSource(index, val.sourceList)}
              >
                {val.label}
              </button>
            );
          })}
        </div>
      </div>
    </div>
  );
};

export default ProductHeaderSticky;
