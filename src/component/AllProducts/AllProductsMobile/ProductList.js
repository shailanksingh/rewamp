import React from "react";
import "./style.scss";

const ProductList = ({ cardsListData }) => {
  return (
    <>
      {cardsListData.map((val, index) => {
        return (
          <div className="product_list_container">
            <img
              src={val.imageData.imgFile}
              alt="banner"
              className={`${
                index === 0 ? "banner_image_first" : "banner_image"
              }`}
            />
            <div className="image_body_content">
              <h6>{val.imageData.title}</h6>
              <p>{val.imageData.description}</p>
            </div>
            <div className="product_card_container row">
              {val.cardList.map((item, itemIndex) => {
                return (
                  <div
                    className={`${
                      val.cardList.length === 1 ? "col-12 card_full" : "col-6"
                    } product_card_lists  ${
                      val.cardList.length === itemIndex + 1 && "mb-0"
                    }`}
                  >
                    <img src={item.bannerImage} alt="Banner" />
                    <h6>{item.title}</h6>
                    <p>{item.subTitle}</p>
                  </div>
                );
              })}
            </div>
          </div>
        );
      })}
    </>
  );
};

export default ProductList;
