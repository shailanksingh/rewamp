import React, { useState } from "react";
import HeaderStickyMenu from "component/common/MobileReuseable/HeaderStickyMenu";
import "./style.scss";
import ProductHeaderSticky from "./ProductHeaderSticky";
import ProductList from "./ProductList";
import { motorInsuraceData } from "component/common/MockData";

const AllProductsMobile = () => {
  const [activeProductData, setActiveProductData] = useState(motorInsuraceData);
  const getActiveProduct = (data) => {
    setActiveProductData(data);
  };
  return (
    <>
      <HeaderStickyMenu />
      <div className="all_products_container">
        <ProductHeaderSticky
          getActiveProduct={(data) => getActiveProduct(data)}
        />
        <ProductList cardsListData={activeProductData} />
      </div>
    </>
  );
};

export default AllProductsMobile;
