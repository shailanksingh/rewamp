import React, { useRef, useState } from "react";

import bluecorona from "assets/images/mobile/bluecorona.png";
import blueflight from "assets/images/mobile/blueflight.png";
import "./style.scss";

export const Insurancecard = ({ insurancecard }) => {
  const sliderRef = useRef(null);
  const [state, updateState] = useState(false);
  const moveRight = () => {
    // console.log(sliderRef.current.scrollLeft , sliderRef.current.clientWidth/4)
    if (sliderRef.current.scrollLeft < sliderRef.current.clientWidth / 4) {
      sliderRef.current.scrollLeft += 250;
    } else {
      sliderRef.current.scrollLeft -= 250;
    }
  };
  const moveFirst = () => {
    // console.log(sliderRef.current.scrollLeft , sliderRef.current.clientWidth/4)

    sliderRef.current.scrollLeft = 0;
  };

  const moveLast = () => {
    // console.log(sliderRef.current.scrollLeft , sliderRef.current.clientWidth/4)

    sliderRef.current.scrollLeft = sliderRef.current.clientWidth;
  };

  const otherProductData = [
    {
      id: 0,
      icon: bluecorona,
      otherProductTitle: "Covid-19 Insurance",
      otherProductPara:
        "Covid-19 Travel insurance - for Saudis Program was designed to provide protection for Saudi citizens and supports.",
      otherProductSubTitle: "Benefits:",
      otherProductSubParaOne: "Provides you with the necessary health care",
      otherProductSubParaTwo: "Medical expenses incurred around the world.",
    },
    {
      id: 1,
      icon: blueflight,
      otherProductTitle: "International Travel Insurance",
      otherProductPara:
        "The company has identified the amount of compensation so that it starts from SR 920 and up to SR 3,000,000 ",
      otherProductSubTitle: "Benefits:",
      otherProductSubParaOne: "Medical Emergency Expenses",
      otherProductSubParaTwo: "Free coverage for children (Under 2 Years)",
    },
    {
      id: 2,
      icon: bluecorona,
      otherProductTitle: "My Family Health Insurance",
      otherProductPara:
        "The program covers most of the medical services provided in the outpatient as well as inpatient services",
      otherProductSubTitle: "Benefits:",
      otherProductSubParaOne: "Upto 60 Days Claim Periods",
      otherProductSubParaTwo: "Waive Depreciation",
    },
  ];
  const [InsuranceSliderButton, updateSlider] = useState([
    {
      id: 0,
      checked: false,
    },
    {
      id: 1,
      checked: false,
    },
    {
      id: 2,
      checked: false,
    },
  ]);

  const getUpdatedSlider = (index) => {
    let updateSliders = InsuranceSliderButton.map((item, i) => {
      if (index == i) {
        return { ...item, checked: true };
      } else {
        return { ...item, checked: false };
      }
    });
    updateSlider(updateSliders);

    console.log(index, "INDWX", updateSliders);
    //
    if (index == 0) {
      moveFirst();
    } else if (index == 1) {
      moveRight();
    } else {
      moveLast();
    }
  };
  return (
    <div className="MotrProContainer">
      <div className="MotrProText mt-4 ml-3 mb-3 ">
        <h5>You may also like our other products</h5>
        <span>We provide the best and trusted service for our customers</span>
      </div>
      <div ref={sliderRef} className="insurancecardslider">
        {otherProductData.map((item, index) => {
          return (
            <div
              className="col-lg-4 col-md-6 col-12 otherProductCard-Container pb-lg-0 pb-3"
              key={index}
            >
              <div className="otherProductCard">
                <img
                  src={item.icon}
                  className="img-fluid pt-1 pb-3"
                  alt="icon"
                />
                <p className="otherProduct-header fs-16 fw-800 m-0 pb-2">
                  {item.otherProductTitle}
                </p>
                <p className="otherProduct-para fs-14 fw-400">
                  {item.otherProductPara}
                </p>
                <p className="otherProduct-Subheader fs-18 fw-800 m-0 pb-1">
                  {item.otherProductSubTitle}
                </p>
                <p className="otherProduct-Subpara fs-12 fw-400 m-0 pb-1">
                  {item.otherProductSubParaOne}
                </p>
                <p className="otherProduct-Subpara fs-12 fw-400 m-0 pb-1">
                  {item.otherProductSubParaTwo}
                </p>
              </div>
            </div>
          );
        })}
      </div>
      <div className="orange d-flex ">
        {InsuranceSliderButton.map((item, index) => {
          return (
            <div
              className={
                "orange_circle_three " +
                (item.checked ? "orange_circle_one" : "")
              }
              onClick={() => getUpdatedSlider(index)}
            ></div>
          );
        })}
      </div>
    </div>
  );
};

export default Insurancecard;
