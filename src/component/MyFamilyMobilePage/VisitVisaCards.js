import React from "react";
import visitvisacancel from "assets/images/mobile/visitvisacancel.png";
import visitvisa from "assets/images/mobile/visitvisa.png";
import chronicdisease from "assets/images/mobile/chronicdisease.png";
import pregnancy from "assets/images/mobile/pregnancy.png";
import babyvacinate from "assets/images/mobile/babyvacinate.png";
import request from "assets/images/mobile/request.png";
import "./style.scss";

export const VisitVisaCards = ({ VisitVisaCards }) => {
  const otherProductData = [
    {
      id: 0,
      icon: visitvisacancel,
      otherProductTitle: "Visit Visa Extension Cancellation",
      otherProductPara:
        "A service that enables the visitor to cancel visit visa extension",
    },
    {
      id: 1,
      icon: visitvisa,
      otherProductTitle: "Visit Visa Extension",
      otherProductPara:
        "A service that enables the visitor to extend his policy ",
    },
    {
      id: 2,
      icon: chronicdisease,
      otherProductTitle: "Chronic Disease Management",
      otherProductPara: "A program that serves people with Chronic Disease",
    },
    {
      id: 3,
      icon: pregnancy,
      otherProductTitle: "Pregnancy Follow-up Program",
      otherProductPara: "A program that servers Pregnant woman ",
    },
    {
      id: 4,
      icon: request,
      otherProductTitle: "Request for Eligibility Letter",
      otherProductPara:
        "A letter issues from Tawuniya explaining your entitlement to treatment in case of any accidental problem ",
    },
    {
      id: 5,
      icon: babyvacinate,
      otherProductTitle: "Home Children Vaccination",
      otherProductPara:
        "A servers enable a member to vaccine his child from home ",
    },
  ];
  return (
    <div className="MotrProContainer">
      <div className="insurancecardslider1">
        {otherProductData.map((item, index) => {
          return (
            <div
              className="col-lg-4 col-md-6 col-12 otherProductCard-Container1 pb-lg-0 pb-3"
              key={index}
            >
              <div className="otherProductCard1">
                <img
                  src={item.icon}
                  className="img-fluid pt-1 pb-2"
                  alt="icon"
                />
                <p className="otherProduct-header fs-16 fw-800 m-0 pb-1">
                  {item.otherProductTitle}
                </p>
                <p className="otherProduct-para fs-16 fw-400">
                  {item.otherProductPara}
                </p>
              </div>
            </div>
          );
        })}
      </div>
    </div>
  );
};

export default VisitVisaCards;
