import React, { useState } from "react";
import HeaderStepsSticky from "component/common/MobileReuseable/HeaderStepsSticky";
import HeaderStickyMenu from "component/common/MobileReuseable/HeaderStickyMenu";
import BackImage from "assets/images/mobile/backImage.png";
import PolicyCard from "component/common/PolicyCard";
import claimassistance from "../../assets/images/mobile/claimassistance.png";
import { CommonFaq } from "component/common/CommonFaq";
import { myFamilyExclusive, PolicyList } from "component/common/MockData";
import { Insurancecard } from "component/MyFamilyMobilePage/Insurancecard";
import TawuniyaAppQuick from "component/common/MobileReuseable/TawuniyaAppQuick/Ithra";
import SupportRequestHelper from "component/common/MobileReuseable/SupportRequest";
import "./style.scss";

const MyFamilyMobilePage = () => {
  const [activeProduct, setActiveProduct] = useState(0);
  const activeProductSource = (index) => {
    setActiveProduct(index);
  };

  const familyProgramData = [
    {
      id: 1,
      label: "Program Conditions",
      familyList: (
        <ul className="insurancefooter_box insurance_bg mt-3">
          <li className="mt-3">
            Insurance must include all family members, without exception or
            selection
          </li>
          <li>The Saudi families only can benefit from "My Family" program</li>
          <li>Coverage begins 30 days after the policy issuance</li>
          <li>
            Percentage of deductible is 20% per each claim in both outpatient
            and inpatient cases
          </li>
          <li>
            The insurance provided covers the male until the age of 18 years and
            females (unmarried / widowed / divorced) until the age of 60 years
          </li>
          <li>
            The program does not allow adding new beneficiaries during the
            currency of insurance policy with the exception of newborn or new
            wife
          </li>
          <li>The program covers the elderly up to 60 years</li>
        </ul>
      ),
    },
    {
      id: 2,
      label: "program Advantages",
      familyList: (
        <ul className="insurancefooter_box  insurance_bg mt-3">
          <li className="mt-3">
            2 Insurance must include all family members, without exception or
            selection
          </li>
          <li>The Saudi families only can benefit from "My Family" program</li>
          <li>Coverage begins 30 days after the policy issuance</li>
          <li>
            Percentage of deductible is 20% per each claim in both outpatient
            and inpatient cases
          </li>
          <li>
            The insurance provided covers the male until the age of 18 years and
            females (unmarried / widowed / divorced) until the age of 60 years
          </li>
          <li>
            The program does not allow adding new beneficiaries during the
            currency of insurance policy with the exception of newborn or new
            wife
          </li>
          <li>The program covers the elderly up to 60 years</li>
        </ul>
      ),
    },
    {
      id: 3,
      label: " Payment in installments",
      familyList: (
        <ul className="insurancefooter_box  insurance_bg mt-3">
          <li className="mt-3">
            3 Insurance must include all family members, without exception or
            selection
          </li>
          <li>The Saudi families only can benefit from "My Family" program</li>
          <li>Coverage begins 30 days after the policy issuance</li>
          <li>
            Percentage of deductible is 20% per each claim in both outpatient
            and inpatient cases
          </li>
          <li>
            The insurance provided covers the male until the age of 18 years and
            females (unmarried / widowed / divorced) until the age of 60 years
          </li>
          <li>
            The program does not allow adding new beneficiaries during the
            currency of insurance policy with the exception of newborn or new
            wife
          </li>
          <li>The program covers the elderly up to 60 years</li>
        </ul>
      ),
    },
  ];

  return (
    <div className="my-family">
      <HeaderStickyMenu />
      <HeaderStepsSticky
        title={"My Family Insurace"}
        buttonTitle="Get A Quote"
      />
      <div className="insure-card">
        <img src={BackImage} alt="backImage"></img>
        <h4>Insure your family within minutes!</h4>
        <p>
          The Saudi insurance pioneer, will help you to identify, analyze and
          manage such risks and suggest appropriate insurance solutions.
        </p>
      </div>
      <div className="policy-card">
        <h5>Mange Your Policy</h5>
        <PolicyCard policyList={PolicyList} />
      </div>
      <div className="future-card">
        <h4>Plans For Your Future</h4>
        <p classname="simple_text">
          Simple prices, No hidden fees, Adanced features for your business
        </p>

        <div className="d-flex">
          <div className="card container four_box mr-2">
            <span className="plans_font mt-3">
              My Family <span className="silver_blue plans_font">silver</span>
            </span>
            <p className="golden_fonts">Golden/+1</p>
            <p className="mb-0 annual_font">Annual Maximum Limit</p>
            <p className="plans_font mb-0">1,000,000 SR </p>
            <hr />
            <button className="enquiry_btn mb-0">Enquiry Now</button>
            <p className="plan_details text-center">PLAN DETAILS</p>
          </div>
          <div className="card container four_box">
            <span className="plans_font mt-3">
              My Family <span className="silver_blue plans_font">Gold</span>
            </span>
            <p className="golden_fonts">Golden/+1</p>
            <p className="mb-0 annual_font">Annual Maximum Limit</p>
            <p className="plans_font mb-0">1,000,000 SR </p>
            <hr />
            <button className="enquiry_btn mb-0">Enquiry Now</button>
            <p className="plan_details text-center">PLAN DETAILS</p>
          </div>
        </div>
        <div className="d-flex">
          <div className="card container mt-3 mr-2 four_box">
            <span className="plans_font mt-3">
              My Family <span className="silver_blue plans_font">Platinum</span>
            </span>
            <p className="golden_fonts">Golden/+1</p>
            <p className="mb-0 annual_font">Annual Maximum Limit</p>
            <p className="plans_font mb-0">1,000,000 SR </p>
            <hr />
            <button className="enquiry_btn mb-0">Enquiry Now</button>
            <p className="plan_details text-center">PLAN DETAILS</p>
          </div>
          <div className="card container mt-3 four_box">
            <span className="plans_font mt-3">
              My Family <span className="silver_blue plans_font">Diamond</span>
            </span>
            <p className="golden_fonts">Golden/+1</p>
            <p className="mb-0 annual_font">Annual Maximum Limit</p>
            <p className="plans_font mb-0">1,000,000 SR </p>
            <hr />
            <button className="enquiry_btn mb-0">Enquiry Now</button>
            <p className="plan_details text-center">PLAN DETAILS</p>
          </div>
        </div>
        <p className="mt-3 center_content">
          The plans of My Family program include many of the benefits,
          privileges and services that are compatible with your needs and that
          of your family to provide comprehensive medical cover up to a maximum
          limit of SR 250,000 per person per policy year.
        </p>
        <div className="total_smallscroll">
          <div className="smallscroll_tabs first_tab">
            <div className="program_text  text-center mb-0">
              {familyProgramData.map((val, index) => {
                return (
                  <p
                    className={`program_btns ${
                      activeProduct === index ? "active" : ""
                    }`}
                    onClick={() => activeProductSource(index, val)}
                  >
                    {val.label}
                  </p>
                );
              })}
            </div>
          </div>
        </div>
      </div>
      <div className="question-section">
        <h6>Our Exclusive and Added-Value Services</h6>
        <p className="center_content">
          In addition to benefits provided by the policy, Tawuniya also provides
          a number of exclusive and added value services that help you get the
          health care you deserve
        </p>
        <CommonFaq faqList={myFamilyExclusive} />
      </div>
      <div className="question-section">
        <h6>Our Exclusive and Added-Value Services</h6>
        <p>
          In addition to benefits provided by the policy, Tawuniya also provides
          a number of exclusive and added value services that help you get the
          health care you deserve
        </p>
      </div>
      <div className="mx-3">
        <div className="makeaclaim">
          <label>How do I make a claim?</label>
          <p>
            Tawuniya believes in simplification and automation wherever
            possible. Our approach to the claims journey for Tawuniya is no
            different. We want you to notify us of claims as soon as possible
            and we want to get your claim moving as soon as possible. Our simple
            steps approach helps us to keep that focus throughout the claim
            process.
          </p>
          <button className="claimbutton">Claim Assistance</button>
          <img
            src={claimassistance}
            alt="claimassistance"
            className="w-100"
          ></img>
        </div>
      </div>
      <TawuniyaAppQuick />
      <div className="mt-4">
        <SupportRequestHelper isContact={false} />
      </div>
      <div className="y">
        <Insurancecard />
      </div>
    </div>
  );
};

export default MyFamilyMobilePage;
