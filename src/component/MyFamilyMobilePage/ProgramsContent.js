import React from 'react'

export const ProgramsContent=[
  {
    id:1,
    description:<ul className="insurancefooter_box">
    <li className="mt-3">
      Insurance must include all family members, without exception or
      selection
    </li>
    <li>
      The Saudi families only can benefit from "My Family" program
    </li>
    <li>Coverage begins 30 days after the policy issuance</li>
    <li>
      Percentage of deductible is 20% per each claim in both outpatient
      and inpatient cases
    </li>
    <li>
      The insurance provided covers the male until the age of 18 years
      and females (unmarried / widowed / divorced) until the age of 60
      years
    </li>
    <li>
      The program does not allow adding new beneficiaries during the
      currency of insurance policy with the exception of newborn or new
      wife
    </li>
    <li>The program covers the elderly up to 60 years</li>
  </ul>
  }
];


export default ProgramsContent