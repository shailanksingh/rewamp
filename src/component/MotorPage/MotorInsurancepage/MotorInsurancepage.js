import React from "react";
import { AppBanner, BreakDownTable } from "../MotorInsuranceComponent";
import { Insurance } from "../MotorInsuranceComponent/Insurance";
import { Faq } from "component/common/FraudDetails";
// import { TableData } from "../MotorInsuranceComponent/MotorTableData";
import { TawuniyaAppSlider } from "component/common/TawuniyaAppSlider";
import { HomeServiceCard, HomeServices } from "component/HomePage/LandingComponent";
import { ExtensionCard } from "../MotorInsuranceComponent";
import {
	headerBreakDownData,
	tableBreakDownData,
	headerPerkData,
	tablePerkData,
} from "component/common/MockData";
import "./style.scss";
import { MotorServicesData } from "../MotorInsuranceComponent/schema/MotorServicesData";
import FrequentlyAsked from "component/common/FrequentlyAsked";
import { FAQMotor } from "../MotorInsuranceComponent/schema/FAQmotor";
import { MotorBlogs } from "../MotorInsuranceComponent/MotorBlogs";
import { MotorProductCard } from "../MotorInsuranceComponent";
import { MotorBlogsData } from "../MotorInsuranceComponent/schema/MotorBlogsData";
import { MotorOtherProducts } from "../MotorInsuranceComponent/MotorComponennts/MotorOtherProducts/index";
import { TwuniyaAppAdvert } from "component/common/TwuniyaAppAdvert";
import { motorOtherProductsData } from "../MotorInsuranceComponent/schema/MotorOtherProductsData";
import { WhyChooseTawuniya } from "../MotorInsuranceComponent/WhyChooseTawuniya";
import { FooterAbout } from "component/HomePage/LandingComponent";
import { MotorCards } from "../MotorInsuranceComponent/MotorCards";
import { CommonTable } from "component/common/CommonTable";
import carDamage from "assets/svg/damageCar.svg";
import xCircle from "assets/svg/xCircle.svg";
import {
	FraudBanner,
	FraudAssociates,
	LegendarySupport,
} from "component/common/FraudDetails";

export const faqCategoryDataMotor = [
	{
		id: 0,
		question: "What is medical insurance?",
		answer:
			"It's important to distinguish between Medical insurance fraud/abuse and mistakes. Medical insurance fraud is an act of deception by misrepresentation of material facts, or silence when good faith requires expression, resulting in material damage to one who relies on it and the right to rely on it. Simply, it is obtaining something of value from someone else through deceit. By contrast, mistakes are made unknowingly by anyone who lacks knowledge, in other words, it’s a misstatement or unintentional deviation from the truth. Therefore, Tawuniya always recommend reading the Medical Insurance Guidelines and visiting Tawuniya social media accounts for more information about the ideal use of your Medical Card.",
	},
	{
		id: 1,
		question: "How much does medical insurance cost?",
		answer:
			"It's important to distinguish between Medical insurance fraud/abuse and mistakes. Medical insurance fraud is an act of deception by misrepresentation of material facts, or silence when good faith requires expression, resulting in material damage to one who relies on it and the right to rely on it. Simply, it is obtaining something of value from someone else through deceit. By contrast, mistakes are made unknowingly by anyone who lacks knowledge, in other words, it’s a misstatement or unintentional deviation from the truth. Therefore, Tawuniya always recommend reading the Medical Insurance Guidelines and visiting Tawuniya social media accounts for more information about the ideal use of your Medical Card.",
	},
	{
		id: 2,
		question: "How can I obtain medical insurance?",
		answer:
			"It's important to distinguish between Medical insurance fraud/abuse and mistakes. Medical insurance fraud is an act of deception by misrepresentation of material facts, or silence when good faith requires expression, resulting in material damage to one who relies on it and the right to rely on it. Simply, it is obtaining something of value from someone else through deceit. By contrast, mistakes are made unknowingly by anyone who lacks knowledge, in other words, it’s a misstatement or unintentional deviation from the truth. Therefore, Tawuniya always recommend reading the Medical Insurance Guidelines and visiting Tawuniya social media accounts for more information about the ideal use of your Medical Card.",
	},
	{
		id: 3,
		question: "What are the required documents to acquire medical insurance?",
		answer:
			"It's important to distinguish between Medical insurance fraud/abuse and mistakes. Medical insurance fraud is an act of deception by misrepresentation of material facts, or silence when good faith requires expression, resulting in material damage to one who relies on it and the right to rely on it. Simply, it is obtaining something of value from someone else through deceit. By contrast, mistakes are made unknowingly by anyone who lacks knowledge, in other words, it’s a misstatement or unintentional deviation from the truth. Therefore, Tawuniya always recommend reading the Medical Insurance Guidelines and visiting Tawuniya social media accounts for more information about the ideal use of your Medical Card.",
	},
	{
		id: 4,
		question: "What are the required documents to acquire medical insurance?",
		answer:
			"It's important to distinguish between Medical insurance fraud/abuse and mistakes. Medical insurance fraud is an act of deception by misrepresentation of material facts, or silence when good faith requires expression, resulting in material damage to one who relies on it and the right to rely on it. Simply, it is obtaining something of value from someone else through deceit. By contrast, mistakes are made unknowingly by anyone who lacks knowledge, in other words, it’s a misstatement or unintentional deviation from the truth. Therefore, Tawuniya always recommend reading the Medical Insurance Guidelines and visiting Tawuniya social media accounts for more information about the ideal use of your Medical Card.",
	},
	{
		id: 5,
		question: "How can I obtain medical insurance?",
		answer:
			"It's important to distinguish between Medical insurance fraud/abuse and mistakes. Medical insurance fraud is an act of deception by misrepresentation of material facts, or silence when good faith requires expression, resulting in material damage to one who relies on it and the right to rely on it. Simply, it is obtaining something of value from someone else through deceit. By contrast, mistakes are made unknowingly by anyone who lacks knowledge, in other words, it’s a misstatement or unintentional deviation from the truth. Therefore, Tawuniya always recommend reading the Medical Insurance Guidelines and visiting Tawuniya social media accounts for more information about the ideal use of your Medical Card.",
	},
	{
		id: 6,
		question: "How much does medical insurance cost?",
		answer:
			"It's important to distinguish between Medical insurance fraud/abuse and mistakes. Medical insurance fraud is an act of deception by misrepresentation of material facts, or silence when good faith requires expression, resulting in material damage to one who relies on it and the right to rely on it. Simply, it is obtaining something of value from someone else through deceit. By contrast, mistakes are made unknowingly by anyone who lacks knowledge, in other words, it’s a misstatement or unintentional deviation from the truth. Therefore, Tawuniya always recommend reading the Medical Insurance Guidelines and visiting Tawuniya social media accounts for more information about the ideal use of your Medical Card.",
	},
	{
		id: 7,
		question: "What is medical insurance?",
		answer:
			"It's important to distinguish between Medical insurance fraud/abuse and mistakes. Medical insurance fraud is an act of deception by misrepresentation of material facts, or silence when good faith requires expression, resulting in material damage to one who relies on it and the right to rely on it. Simply, it is obtaining something of value from someone else through deceit. By contrast, mistakes are made unknowingly by anyone who lacks knowledge, in other words, it’s a misstatement or unintentional deviation from the truth. Therefore, Tawuniya always recommend reading the Medical Insurance Guidelines and visiting Tawuniya social media accounts for more information about the ideal use of your Medical Card.",
	},
];

export const MotorInsurancepage = () => {
	return (
		<React.Fragment>
			<div className="MotorInsuranceMainContainer">
				<AppBanner />
				<Insurance />
				<MotorProductCard />
				{/* <MotorCards /> */}
				<div className="row">
					<div className="col-12 pt-5">
						<p className="perkHeading fw-800 m-0">
							Check Out All The Perks You Get{" "}
						</p>
						<p className="perkPara fw-400 pb-2">
							We provide the best and trusted service for our customers
						</p>
						<CommonTable
							headerData={headerPerkData}
							tableData={tablePerkData}
						/>
					</div>
				</div>
				<ExtensionCard />
				<div className="row">
					<div className="col-12 pt-5">
						<p className="breakdownHeading fw-800 m-0">
							Mechanical Breakdown Insurance
						</p>
						<p className="breakingPara fw-400 pb-2">
							We provide the best and trusted service for our customers
						</p>
						<CommonTable
							headerData={headerBreakDownData}
							tableData={tableBreakDownData}
						/>
					</div>
				</div>
				{/* <TawuniyaAppSlider /> */}
				{/* <HomeServices HomeServicesData={MotorServicesData} /> */}
				<HomeServiceCard HomeServicesData={MotorServicesData} pillNumber={2} />
				<div className="motor-faq-container">
					<div className="pb-3">
						<h3 className="surplush4 fs-36 fw-800 text-center faqTitle  pt-4 m-0">
							You’ve got questions, we’ve got answers
						</h3>
						<p className="mainBannerPara motor-fag-para w-60 fs-16 fw-400 text-center faqPara">
							Review answers to commonly asked questions at Tawuniya, which
							enable you to be directly involved in improving our support
							experience.
						</p>
					</div>
					<Faq faqData={faqCategoryDataMotor} />
				</div>
				<TwuniyaAppAdvert />
				{/* <MotorBlogs /> */}
				{/* <MotorOtherProducts motorOtherProductsData={motorOtherProductsData} /> */}
				<MotorOtherProducts />
				<FooterAbout />
			</div>
		</React.Fragment>
	);
};
