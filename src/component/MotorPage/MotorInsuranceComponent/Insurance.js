import React, { useState } from "react";
import { InsuranceCard } from "component/common/InsuranceCard";
import { NormalSearch } from "component/common/NormalSearch";
import { NormalRadioButton } from "component/common/NormalRadioButton";
import { NormalButton } from "component/common/NormalButton";
import { RewardCard } from "component/common/RewardCard";
import PhoneNumberInput from "component/common/PhoneNumberInput/index";
import policyCar from "assets/svg/policyCarBlue.svg";
import coverage from "assets/svg/coverageBlue.svg";
import renew from "assets/svg/renewBlue.svg";
import claim from "assets/svg/claimBlue.svg";
import accident from "assets/svg/towcarBlue.svg";
import agent from "assets/svg/carAgentBlue.svg";
import mvpi from "assets/svg/mvpiBlue.svg";
import single from "assets/svg/single.svg";
import group from "assets/svg/Groupsme.svg";
import KSAFlagImage from "../../../assets/images/ksaFlag.png";
import IndiaFlagImage from "../../../assets/images/indiaFlag.png";
import iquamaIcon from "assets/svg/iquamaIcon.svg";
import buynowArrow from "assets/svg/buynowArrow.svg";
import addManage from "assets/svg/addManage.svg";
import "./style.scss";

export const Insurance = () => {
	const openInNewTab = (url1) => {
		const newWindow = window.open(url1, "_blank", "noopener,noreferrer");
		if (newWindow) newWindow.opener = null;
	};
	const insuranceMotorCardData = [
		{
			id: 0,
			content: "Submit / Track a Claim",
			cardIcon: claim,
			class: "pr-lg-0 pb-lg-0 pb-2",
		},
		{
			id: 1,
			content: "Road Side and Accident Assistance",
			cardIcon: accident,
			class: "pr-lg-0 pb-lg-0 pb-2",
		},
		{
			id: 2,
			content: "Get free MVPI Services at your Doorstep",
			cardIcon: mvpi,
			class: "pr-lg-0 pb-lg-0 pb-2",
		},
		{
			id: 3,
			content: "Get free Trip to you Car Agency for Maintenance",
			cardIcon: agent,
			class: "pr-lg-0 pb-lg-0 pb-2",
		},
	];

	let dialingCodes = [
		{
			code: "+966",
			image: KSAFlagImage,
		},
		{
			code: "+91",
			image: IndiaFlagImage,
		},
	];

	//initialize state for input field
	const [valueOne, setValueOne] = useState("");

	//initialize state for input field
	const [inpOne, setInpOne] = useState({ userId: "", phone: "" });

	//initialiize state for radio buttons
	const [radioOne, setRadioOne] = useState("Yes");

	let [selectedCode, setSelectedCode] = useState(dialingCodes[0]);

	let [phoneNumber, setPhoneNumber] = useState("");

	const handleInputChange = (e) => {
		const { name, value } = e.target || e || {};
		setInpOne({ ...inpOne, [name]: value });
	};

	return (
		// motor insurance banner starts here
		<div className="row motorInsuranceContainer pt-3 pb-5">
			<div className="col-lg-7 col-12 pt-5">
				<div className="row">
					<div className="col-lg-11 col-12">
						<p className="motorTitle fw-800 m-0 pt-3">Motor Insurance</p>
						<p className="motorPara fs-21 fw-400">
							Motor insurance is a mandatory policy that covers the cost of
							damage caused to your car due to unforseen events such as theft,
							collision etc.,{" "}
						</p>
						<InsuranceCard
							heathInsureCardData={insuranceMotorCardData}
							textWidth="insureCardTextWidth"
						/>
						<div className="row positionReward-ManageCard">
							<div className="col-lg-6 col-md-12 col-12 pt-1 pb-lg-0 pb-md-3 pr-lg-0">
								<div className="managePolicy">
									<div className="policyContainer">
										<div className="d-flex flex-row">
											<div className="pr-3">
												<img
													src={addManage}
													className="img-fluid addManageIcon"
													alt="icon"
												/>
											</div>
											<div>
												<p className="manageText fs-20 fw-800 m-0">
													Manage Your Policy
												</p>
												<span className="fw-600 addText">
													Add more vehicles and coverage instantly
												</span>
											</div>
										</div>
									</div>
									<div className="policyTypeContainer p-4">
										<div className="row">
											<div className="col-lg-4 col-md-4 col-12 vehilcleBox pr-3">
												<img
													src={policyCar}
													className="img-fluid mx-auto d-block pb-2"
													alt="icon"
												/>
												<p className="policyBoxPara fs-14 fw-400 m-0">
													Add Vehicles
												</p>
											</div>
											<div className="col-lg-4 col-md-4 col-12 pl-2 pr-2 coverageBox">
												<img
													src={coverage}
													className="img-fluid mx-auto d-block pb-2"
													alt="icon"
												/>
												<p className="policyBoxPara fs-14 fw-400 m-0">
													Buy Coverages
												</p>
											</div>
											<div className="col-lg-4 col-md-4 col-12 renewBox pl-2">
												<img
													src={renew}
													className="img-fluid mx-auto d-block pb-2"
													alt="icon"
												/>
												<p className="renewBoxPara fs-14 fw-400 m-0">
													Renew Policy
												</p>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div className="col-lg-6 col-md-12 col-12 pr-0">
								<RewardCard
									cardTitle="Drive"
									cardSubTitle="Drive Well Save Money"
									titleClass="rewardTitle"
									subtitleClass="rewardSubTitle"
									label="Claim your Rewards"
									className="rewardBtnClass"
								/>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div className="col-lg-5 col-md-12 col-12 pt-lg-5 pt-2">
				<div className="insureMotorCard h-100">
					<div className="insureLayerOne">
						<p className="insureCardTitle fw-800 m-0">
							Insure Your Vehicles Now!
						</p>
					</div>
					<div className="insureLayerTwo">
						<div className="row insureSubMotorLayerTwo pt-3">
							<div className="col-lg-6 col-12 pb-lg-0 pb-4">
								<NormalButton
									alignBtn={true}
									label="Individual"
									className="individualMotorBtn"
									needNewBtnIcon={true}
									newSrc={single}
									adjustNewIcon="img-fluid singleIcon pr-3"
								/>
							</div>
							<div className="col-lg-6 col-12 pl-lg-0">
								<NormalButton
									label="Corporate & SMEs"
									className="smeBtn"
									needNewBtnIcon={true}
									newSrc={group}
									adjustNewIcon="img-fluid groupIcon pr-3"
								/>
							</div>
						</div>

						<div className="subInsureLayer p-4">
							<div className="row">
								<div className="col-12">
									<NormalSearch
										className="motorInputfieldOne"
										name="userId"
										value={inpOne.userId}
										placeholder="Natioal ID, Iqama"
										onChange={handleInputChange}
										needLeftIcon={true}
										leftIcon={iquamaIcon}
									/>
								</div>
								<div className="col-12 pt-4">
									<PhoneNumberInput
										className="motorPhoneInput"
										selectInputClass="motorSelectInputWidth"
										selectInputFlexType="motorFlexType"
										dialingCodes={dialingCodes}
										selectedCode={selectedCode}
										setSelectedCode={setSelectedCode}
										value={phoneNumber}
										name="phoneNumber"
										onChange={({ target: { value } }) => setPhoneNumber(value)}
									/>
								</div>
							</div>
							<p className="fs-16 fw-400 pt-3 companyTerms">
								Do you want to insure vehicles for your company?
							</p>
							<div className="d-flex flex-row">
								<div>
									<NormalRadioButton
										type="radio"
										name="radioOne"
										value="Yes"
										onChange={(e) => setRadioOne(e.target.value)}
										radioValue="Yes"
										checked={radioOne === "Yes"}
									/>
								</div>
								<div className="pl-3">
									<NormalRadioButton
										type="radio"
										name="radioOne"
										value="No"
										onChange={(e) => setRadioOne(e.target.value)}
										radioValue="No"
										checked={radioOne === "No"}
									/>
								</div>
							</div>
						</div>
						<div className="termsContainer">
							<p className="fs-15 fw-400 insureTerms py-2">
								By continuing you give Tawuniya advance consent to obtain my
								and/or my dependents' information from the National Information
								Center.
							</p>
							<div className="pb-3 mb-2" id="position-BuyBtn">
								<NormalButton
									label="Buy Now"
									className="insureNowBtn p-4"
									adjustIcon="pl-3"
									needBtnPic={true}
									src={buynowArrow}
									onClick={() =>
										openInNewTab("https://beta.tawuniya.com.sa/web/#/motor")
									}
								/>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		// motor insurance banner ends here
	);
};
