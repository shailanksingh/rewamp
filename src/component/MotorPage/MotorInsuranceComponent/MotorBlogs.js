import React from "react";
import { Button } from "@material-ui/core";
import leftArrow from "assets/svg/blogLeftArrow.svg";
import rightArrow from "assets/svg/blogRightArrow.svg";
import { NormalButton } from "component/common/NormalButton";

export const MotorBlogs = () => {
  return (
    <div className="MotorBlogs">
      <div className="d-flex justify-content-between MotorBlogsHead pt-5">
        <div className="MotorBlogsText pb-2">
          <h5>Blogs Related to Motor Insurance</h5>
          <span>We provide the best and trusted service for our customers</span>
        </div>
        <div>
          <div className="d-flex flex-row">
            <div>
              <img src={leftArrow} className="img-fluid" alt="icon" />
            </div>
            <div>
              <img src={rightArrow} className="img-fluid" alt="icon" />
            </div>
          </div>
        </div>
      </div>
      <div className="MotorBlogsImgContainer">
        <div className="MotorBlogsImga">
          <div className="MotorBlogsImgText">
            <h5>Insurance News: Cultivating a Sustainable future</h5>
            <p>
              Get quality car insurance at an affordable price. with Tawuniya,
              find a car insurance that suits your needs.
            </p>
            <div className="MotorBlogsBtn">
              <NormalButton label="Read More" className="blog-readMore-Btn" />
            </div>
          </div>
        </div>
        <div className="MotorBlogsImgb">
          <div className="MotorBlogsImgText">
            <h5>Insuring your car became easier from your place</h5>
            <p>
              Get quality car insurance at an affordable price. with Tawuniya,
              find a car insurance that suits your needs.
            </p>
            <div className="MotorBlogsBtn">
              {/* <Button size="medium" color="secondary" variant="outlined">
                Read More
              </Button> */}
              <NormalButton label="Read More" className="blog-readMore-Btn" />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};
