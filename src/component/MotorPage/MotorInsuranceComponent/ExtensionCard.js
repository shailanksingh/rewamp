import React from "react";
import depreciation from "assets/svg/depreciation.svg";
import hire from "assets/svg/hire.svg";
import geo from "assets/svg/geo.svg";
import accident from "assets/svg/carAccidents.svg";
import agencyRepair from "assets/svg/agencyRepair.svg";
import "./style.scss";

export const ExtensionCard = () => {
  const extensionData = [
    {
      id: 0,
      icon: depreciation,
      class: "extension-layout pb-lg-5 pb-3",
      subtitle: "Waiver of Depreciation Clause",
      extensionIconClass:"extensionIconOne"
    },
    {
      id: 1,
      icon: hire,
      class: "extension-layout pl-lg-3 pl-md-2 pb-lg-5 pb-3",
      subtitle: "Hire Care Facility",
      addedClass: "pt-lg-3 pt-md-2",
      extensionIconClass:"extensionIconTwo"
    },
    {
      id: 2,
      icon: geo,
      class: "extension-layout pl-lg-3 pl-md-2 pb-lg-5 pb-3",
      subtitle: "Geographical Extension",
      extensionIconClass:"extensionIconThree"
    },
    {
      id: 3,
      icon: accident,
      class: "extension-layout pl-lg-3 pl-md-2 pb-lg-5 pb-3",
      subtitle: "Personal Accident",
      addedClass: "pt-lg-5 pt-2",
      extensionIconClass:"extensionIconFour"
    },
    {
      id: 4,
      icon: agencyRepair,
      class: "extension-layout pl-lg-3 pl-md-2 pb-lg-5 pb-3",
      subtitle: "Agency Repairs",
      addedClass: "pt-lg-4 pt-2",
      extensionIconClass:"extensionIconFive"
    },
  ];

  return (
    <div className="row extensionCardContainer pt-5">
      <div className="col-lg-12 col-12">
        <p className="fw-800 additionExtensionHeader m-0">
          Additional Extensions of the Program
        </p>
        <p className="fs-18 fw-400  additionExtensionPara">
          We provide the best and trusted service for our customers
        </p>
      </div>
      <div className="col-lg-12 col-12">
        <div className="row px-3">
          {extensionData.map((item, index) => {
            return (
              <div className={item.class} key={index}>
                <div className="extension-service-card pb-lg-5">
                  <img src={item.icon} className={`${item.extensionIconClass} img-fluid extensionIcon py-3`} alt="icon" />
                  <p
                    className={`${item.addedClass} fs-24 fw-700 extension-content m-0`}
                  >
                    {item.subtitle}
                  </p>
                </div>
              </div>
            );
          })}
        </div>
      </div>
    </div>
  );
};
