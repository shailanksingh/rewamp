export { AppBanner } from "../MotorInsuranceComponent/AppBanner";
export { BreakDownTable } from "./BreakDownTable";
export { MotorOtherProducts } from "../MotorInsuranceComponent/MotorOtherProduct";
export { WhyChooseTawuniya } from "../MotorInsuranceComponent/WhyChooseTawuniya";
export { ExtensionCard } from "../MotorInsuranceComponent/ExtensionCard";
export { MotorProductCard } from "../MotorInsuranceComponent/MotorProductCard";