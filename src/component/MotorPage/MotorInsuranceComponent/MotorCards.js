import React from "react";
import { Card, Button, Accordion } from "react-bootstrap";
import "./style.scss";

import Groupcar from "../../../assets/svg/Groupcar.svg";
import Statusbar from "../../../assets/svg/statusbar.svg";
import Statusbar2 from "../../../assets/svg/statusbar2.svg";
import Motorcar from "../../../assets/svg/motor-car.svg";
import Flash from "../../../assets/svg/flash.svg";

export const MotorCards = () => {
	return (
		<div className="motorInsuranceCardContainer pt-2 mb-5">
			<div className="row pt-4">
				<div className="col-lg-4">
					<Card className="motorcard">
						<Card.Body>
							<Card.Text>
								<div className="">
									<div className="motorcard-title">
										<div className="">
											<h6>Comprehensive </h6>
										</div>

										<div className="">
											<img className="motor-cardcar" src={Groupcar} />
										</div>
									</div>
									<div className="row">
										<div className="col-lg-8">
											<img className="img-fluid" src={Statusbar} alt="icon" />
										</div>

										<div className="col-lg-10">
											<p className="mainBannerPara ">Fully Covered</p>
											<p className="mainBannerPara motor-ptag">
												Covers both third-party liabilities and damages to your
												own car as well.
											</p>
										</div>

										<div className="col-lg-10">
											<h6>Available Products </h6>
										</div>
									</div>
								</div>
							</Card.Text>
							<div className="motorcard-bottom-root">
								<div className="motorcard-bottom">
									<div className="motorcard-name">
										<img className="motor-cardcar2 pr-2" src={Motorcar} />{" "}
										<h5 className="pt-1">Al Shamel</h5>
									</div>

									<div className="">
										<Button className="motorbtn">Buy Now</Button>
									</div>
								</div>
							</div>
						</Card.Body>
					</Card>
				</div>

				{/* --------------------- */}

				<div className="col-lg-4">
					<Card className="motorcard">
						<Card.Body>
							<Card.Text>
								<div className="">
									<div className="motorcard-title">
										<div className="">
											<h6>Third Party Liability </h6>
										</div>
										<div className="">
											<img className="motor-cardcar" src={Groupcar} />
										</div>
									</div>
									<div className="row">
										<div className="col-lg-8">
											<img src={Statusbar2} className="img-fluid" alt="icon" />
										</div>

										<div className="col-lg-10">
											<p className="mainBannerPara ">
												Cover for the party who is not at fault
											</p>
											<p className="mainBannerPara motor-ptag">
												Damages & losses caused to a third-party vehicle are
												covered.
											</p>
										</div>

										<div className="col-lg-10">
											<h6>Available Products </h6>
										</div>
									</div>
								</div>
							</Card.Text>
							<div className="motorcard-bottom-root">
								<div className="motorcard-bottom">
									<div className="motorcard-name">
										<img className="motor-cardcar2 pr-2" src={Flash} />{" "}
										<h5 className="pt-2">Sanad</h5>
									</div>
									<div className="">
										<Button className="motorbtn">Buy Now</Button>
									</div>
								</div>
								<div className="motorcard-bottom">
									<div className="motorcard-name">
										<img className="motor-cardcar2 pr-2" src={Flash} />{" "}
										<h5 className="pt-2">Sanad Plus</h5>
									</div>
									<div className="">
										<Button className="motorbtn">Buy Now</Button>
									</div>
								</div>
							</div>
						</Card.Body>
					</Card>
				</div>

				<div className="col-lg-4">
					<Card className="motorcard">
						<Card.Body>
							<Card.Text>
								<div className="">
									<div className="motorcard-title">
										<div className="">
											<h6>Mechanical Breakdown </h6>
										</div>

										<div className="">
											<img className="motor-cardcar" src={Groupcar} />
										</div>
									</div>
									<div className="row">
										<div className="col-lg-8">
											<img src={Statusbar} className="img-fluid" alt="icon" />
										</div>

										<div className="col-lg-10">
											<p className="mainBannerPara ">
												Cover for Mechanical or Electrical Failures
											</p>
											<p className="mainBannerPara motor-ptag">
												MBI covers your vehicle for mechanical or electrical
												failures
											</p>
										</div>
									</div>
								</div>
							</Card.Text>
							<div className="motorcard-bottom-root">
								<div className="motorcard-bottom">
									<div className="motorcard-name">
										<Button className="motorbtn outline mr-3">
											Learn More
										</Button>
										<Button className="motorbtn">Buy Now</Button>
									</div>
								</div>
							</div>
						</Card.Body>
					</Card>
				</div>
			</div>
		</div>
	);
};
