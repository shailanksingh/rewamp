import { NormalButton } from "component/common/NormalButton";
import React from "react";
import Motorcar from "../../../assets/svg/alshammelCarBlue.svg";
import Groupcar from "../../../assets/svg/Groupcar.svg";
import liabilityCar from "../../../assets/svg/liabilityCar.svg";
import breakDown from "../../../assets/svg/carBreakdown.svg";
import sanad from "../../../assets/svg/motorSanadBlue.svg";
import sanadPlus from "../../../assets/svg/motorSanadPlusBlue.svg";
import "./style.scss";

export const MotorProductCard = () => {
	const motorProductCardData = [
		{
			id: 0,
			headerTitle: "Comprehensive",
			headerIcon: Groupcar,
			progress: "individual-ProgressLevelOne",
			subHeader: "Fully Covered",
			subPara:
				"Covers both third-party liabilities and damages to your own car as well.",
			subParaClass: "motorCard-SubParaOne",
			newPara: "Available Products",
			paddClass: "pt-4 mt-4",
			needAllProducts: true,
			newButtonConatinerData: [
				{
					id: 0,
					contentIcon: Motorcar,
					content: "Al Shamel",
				},
			],
		},
		{
			id: 1,
			headerTitle: "Third Party Liability",
			headerIcon: liabilityCar,
			progress: "individual-ProgressLevelTwo",
			subHeader: "Cover for the party who is not at fault",
			subPara: "Damages & losses caused to a third-party vehicle are covered.",
			subParaClass: "motorCard-SubParaTwo",
			newPara: "Available Products",
			paddClass: "pt-3",
			needAllProducts: true,
			newButtonConatinerData: [
				{
					id: 0,
					contentIcon: sanad,
					content: "Sanad",
				},
				{
					id: 1,
					contentIcon: sanadPlus,
					content: "Sanad Plus",
				},
			],
		},
		{
			id: 2,
			headerTitle: "Mechanical Breakdown",
			headerIcon: breakDown,
			progress: "individual-ProgressLevelOne",
			subHeader: "Cover for Mechanical or Electrical Failures",
			subPara: "MBI covers your vehicle for mechanical or electrical failures",
			subParaClass: "motorCard-SubParaThree",
			newPara: "Available Products",
			needAllProducts: false,
		},
	];
	const openInNewTab = (url1) => {
		const newWindow = window.open(url1, "_blank", "noopener,noreferrer");
		if (newWindow) newWindow.opener = null;
	};
	return (
		<div className="row motorProduct-CardContainer font-Lato py-5">
			{motorProductCardData.map((item, index) => {
				return (
					<div
						className={`${
							item.id === 1 && "pl-lg-0 pr-lg-0"
						} col-lg-4 col-12 pb-lg-0 pb-3`}
						key={index}
					>
						<div className="motorProduct-CardLayout">
							<div className="d-flex justify-content-between pb-3">
								<div>
									<p className="m-0 fs-24 fw-800 motorCard-headerContent">
										{item.headerTitle}
									</p>
								</div>
								<div>
									<img
										className="motor-cardcarIcon img-fluid"
										src={item.headerIcon}
										alt="icon"
									/>
								</div>
							</div>
							<div className="motorCard-ProgressContainer">
								<div className="progress-LevelMotor" id={item.progress}></div>
							</div>
							<p className="fs-16 fw-400 motorCard-SubHeader pt-3 m-0 pb-2">
								{item.subHeader}
							</p>
							<p
								className={`${item.subParaClass} motorCard-SubParas fs-14 fw-400 m-0 line-height-19`}
							>
								{item.subPara}
							</p>
							{item.needAllProducts && (
								<div className={item.paddClass}>
									<p className="available-motorProductTitle fs-14 fw-400 m-0">
										{item.newPara}
									</p>
									{item?.newButtonConatinerData?.map((items, index) => {
										return (
											<div
												className="d-flex justify-content-between pb-2"
												key={index}
											>
												<div key={index}>
													<img
														className="img-fluid contentMotorIcon pr-2"
														src={items.contentIcon}
														alt="icon"
													/>
													<span className="fs-24 fw-800 alShameel-Title">
														{items.content}
													</span>
												</div>
												<div>
													<NormalButton
														label="Buy Now"
														className="buyNowMotorBtn"
														onClick={() =>
															openInNewTab(
																"https://beta.tawuniya.com.sa/web/#/motor"
															)
														}
													/>
												</div>
											</div>
										);
									})}
								</div>
							)}
							{item.id === 2 && (
								<div className="d-flex flex-row breakdown-CardContainer">
									<div>
										<NormalButton
											label="Learn More"
											className="motorLearnBtn px-4"
										/>
									</div>
									<div className="pl-4">
										<NormalButton
											label="Buy Now"
											className="buyNowMotorBtnNew px-4"
											onClick={() =>
												openInNewTab("https://beta.tawuniya.com.sa/web/#/motor")
											}
										/>
									</div>
								</div>
							)}
						</div>
					</div>
				);
			})}
		</div>
	);
};
