import tawuniyaDrive from "assets/svg/tawuniyaDrivePrimary.svg";
import roadSide from "assets/svg/roadSidePrimary.svg";
import ithra from "assets/svg/ithraPrimary.svg";
import motorInspect from "assets/svg/motorInspectPrimary.svg";
import freeCarMaintainence from "assets/svg/freeCarMaintainence.svg";
import endtoendClaim from "assets/svg/endtoendClaim.svg";

export const MotorServicesData = [
	{
		serviceTitle: "Our Exclusive and Added-Value Services",
		textCenter: true,
		serviceSubtitle:
			"In addition to benefits provided by the policy, Tawuniya also provides a number of exclusive and added value services that help you get the motor care you deserve",
		Services: [
			{
				id: 1,
				pillName: "MOTOR",
				Search: true,
				serviceCardData: [
					{
						headingEl: "Tawuniya Drive",
						discrptionEl:
							"It mesures your driving behavior and rewards you weekly,",
						iconE1: `${tawuniyaDrive}`,
					},
					{
						headingEl: "Road Side Assistance",
						discrptionEl: "Our services can be trustable, honest and worthy",
						iconE1: `${roadSide}`,
					},
					{
						headingEl: "Motor Vehicle Periodic Inspection",
						discrptionEl: "Motor Vehicle Periodic Inspection",
						iconE1: `${motorInspect}`,
					},
					{
						headingEl: "Free Car Maintenance trips to Agency",
						discrptionEl:
							"Towing vehicle from a place you choose to your car’s dealer and returning your vehicle back for Free",
						iconE1: `${freeCarMaintainence}`,
					},
					{
						headingEl: "End to end Claim service",
						discrptionEl: "---------------------------------------------------",
						iconE1: `${endtoendClaim}`,
					},
					{
						headingEl: "Ithra’a",
						discrptionEl:
							"Special discounts on a wide range of services and products that are supplied by distinguished partners and famous brands in various sectors",
						iconE1: `${ithra}`,
					},
					{
						headingEl: "Tawuniya Drive",
						discrptionEl:
							"It mesures your driving behavior and rewards you weekly,",
						iconE1: `${tawuniyaDrive}`,
					},
					{
						headingEl: "Road Side Assistance",
						discrptionEl: "Our services can be trustable, honest and worthy",
						iconE1: `${roadSide}`,
					},
					{
						headingEl: "Motor Vehicle Periodic Inspection",
						discrptionEl: "Motor Vehicle Periodic Inspection",
						iconE1: `${motorInspect}`,
					},
					{
						headingEl: "Free Car Maintenance trips to Agency",
						discrptionEl:
							"Towing vehicle from a place you choose to your car’s dealer and returning your vehicle back for Free",
						iconE1: `${freeCarMaintainence}`,
					},
					{
						headingEl: "End to end Claim service",
						discrptionEl: "---------------------------------------------------",
						iconE1: `${endtoendClaim}`,
					},
					{
						headingEl: "Ithra’a",
						discrptionEl:
							"Special discounts on a wide range of services and products that are supplied by distinguished partners and famous brands in various sectors",
						iconE1: `${ithra}`,
					},
				],
			},
		],
	},
];


export const travelServicesData = [
	{
		serviceTitle: "Our Exclusive and Added-Value Services",
		textCenter: true,
		serviceSubtitle:
			"In addition to benefits provided by the policy, Tawuniya also provides a number of exclusive and added value services that help you get the motor care you deserve",
		Services: [
			{
				id: 1,
				pillName: "MOTOR",
				Search: true,
				serviceCardData: [
					{
						headingEl: "Tawuniya Drive",
						discrptionEl:
							"It mesures your driving behavior and rewards you weekly,",
						iconE1: `${tawuniyaDrive}`,
					},
					{
						headingEl: "Road Side Assistance",
						discrptionEl: "Our services can be trustable, honest and worthy",
						iconE1: `${roadSide}`,
					},
					{
						headingEl: "Motor Vehicle Periodic Inspection",
						discrptionEl: "Motor Vehicle Periodic Inspection",
						iconE1: `${motorInspect}`,
					},
					{
						headingEl: "Free Car Maintenance trips to Agency",
						discrptionEl:
							"Towing vehicle from a place you choose to your car’s dealer and returning your vehicle back for Free",
						iconE1: `${freeCarMaintainence}`,
					},
					{
						headingEl: "End to end Claim service",
						discrptionEl: "---------------------------------------------------",
						iconE1: `${endtoendClaim}`,
					},
					{
						headingEl: "Ithra’a",
						discrptionEl:
							"Special discounts on a wide range of services and products that are supplied by distinguished partners and famous brands in various sectors",
						iconE1: `${ithra}`,
					},
					{
						headingEl: "Tawuniya Drive",
						discrptionEl:
							"It mesures your driving behavior and rewards you weekly,",
						iconE1: `${tawuniyaDrive}`,
					},
					{
						headingEl: "Road Side Assistance",
						discrptionEl: "Our services can be trustable, honest and worthy",
						iconE1: `${roadSide}`,
					},
					{
						headingEl: "Motor Vehicle Periodic Inspection",
						discrptionEl: "Motor Vehicle Periodic Inspection",
						iconE1: `${motorInspect}`,
					},
					{
						headingEl: "Free Car Maintenance trips to Agency",
						discrptionEl:
							"Towing vehicle from a place you choose to your car’s dealer and returning your vehicle back for Free",
						iconE1: `${freeCarMaintainence}`,
					},
					{
						headingEl: "End to end Claim service",
						discrptionEl: "---------------------------------------------------",
						iconE1: `${endtoendClaim}`,
					},
					{
						headingEl: "Ithra’a",
						discrptionEl:
							"Special discounts on a wide range of services and products that are supplied by distinguished partners and famous brands in various sectors",
						iconE1: `${ithra}`,
					},
				],
			},
		],
	},
];
