import React  from "react";
import "./style.scss";
import HeaderStepsSticky from "component/common/MobileReuseable/HeaderStepsSticky";
import HeaderStickyMenu from "component/common/MobileReuseable/HeaderStickyMenu";
import taj_bg from "assets/news/Taj Program bg.png";
import consult from "assets/news/consult.png";
import refill from "assets/news/refill.png";
import dispense_medication from "assets/news/dispense_medication.png"
import child_vaccine from "assets/news/child_vaccine.png"
import chronic_manage from "assets/news/chronic_manage.png"
import FooterMobile from "component/common/MobileReuseable/FooterMobile";

const TajProgram = () => {
  const services = [
    {
      id: 1,
      image: consult,
      title: "Reliable Online Consultation",
      description:
        "This service allows you to book a reliable online consultation  appointment with accredited doctors in all health specialties via the smartphone app and website.",
    },
    {
      id: 2,
      image: refill,
      title: "Re-ﬁll of Chronic Diseases Medication",
      description:
        "If you have any chronic diseases such as diabetes, pressure, and other conditions. As well as use medication for long periods ranging from one to three month. This service allows you to refill your medicines from an approved pharmacy network without the need to see your doctor. ",
    },
    {
      id: 3,
      image: dispense_medication,
      title: "Dispensing Medication",
      description:
        "This service enables dispensing your medicines through prescription directly from the approved pharmacies.",
    },
    {
      id: 4,
      image: child_vaccine,
      title: "Home Children Vaccination",
      description:
        "For your child's health and in order to ensure they get vaccines. We provide vaccination service at home to Tawuniya members whose age between 0 – 7 years based on the basic vaccinations schedule issued by MOH and included in the Cooperative Health Insurance Uniﬁed Policy published by CCHI. ",
    },
    {
      id: 5,
      image: chronic_manage,
      title: "Chronic Disease Management",
      description:
        "If you suffer from any chronic disease, this service allows you to: 1-	Get comprehensive health care from medical consultations to receive the necessary medication. Provide you laboratory services in some cases at your residence. ",
    },
  ]

  const smartphones = [
    {
      id: 1,
      description: "Viewing details of family member’s ",
    },
    {
      id: 2,
      description: "Updating personal information ",
    },


    {
      id: 3,
      description: "Viewing beneﬁts and limits of coverage ",
    },
    {
      id: 4,
      description: "Viewing status of insurance in the CCHI system",
    },
    {
      id: 5,
      description: "Applying for reimbursement of medical expenses",
    },
    {
      id: 6,
      description: "Following up on approvals",
    },
    {
      id: 7,
      description: "Following up on reimbursement of medical costs ",
    },
    {
      id: 8,
      description: "Viewing locations of service providers",
    },
  ]

  return (
    <div>
      <HeaderStickyMenu />
      <HeaderStepsSticky title="Taj Program" buttonTitle="Get A Quote" />

      <div className="ithra_container">
        <div className="taj_program_image">
          <img alt="..." src={taj_bg}/>
          <h5>The New "Taj" Program from Tawuniya		</h5>
          <p>Special services and programmes included under TAJ umberlla to cover your needs.</p>
          <button>Get A Quote</button>
        </div>
        
        <h5>
          We offer you a variety of services that suit your different needs
        </h5>

        <div className="d-flex taj_services_slider ">
          {services.map((services, index) => (
            <div key={services.title} className="taj_services_card">
              <img alt="..." className="" src={services.image} />
              <h5>{services.title}</h5>
              <p>{services.description}</p>
            </div>
          ))}
        </div>
        <h4>Tawuniya Application Services for Smartphones</h4>
        <p>
          Tawuniya smartphone application helps you to complete transactions and
          insurance procedure securely
        </p>

        <div className="taj_application_slider">
          {smartphones.map((smartphones) => (
            <div key={smartphones.id} className="taj_application_card">
              <h5>{smartphones.description}</h5>
            </div>
          ))}
        </div>

        <FooterMobile />
      </div>
    </div>
  );
};

export default TajProgram;
