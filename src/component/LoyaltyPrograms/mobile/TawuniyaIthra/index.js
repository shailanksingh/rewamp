import React from "react";
import HeaderStepsSticky from "component/common/MobileReuseable/HeaderStepsSticky";
import HeaderStickyMenu from "component/common/MobileReuseable/HeaderStickyMenu";
import IthraContainer from "assets/images/mobile/IthraContainer.png";
import play_button from "assets/images/mobile/play_button.svg";
import "./style.scss";
import { FooterCard } from "component/HomePage/LandingComponent/NavbarCards";
import FooterMobile from "component/common/MobileReuseable/FooterMobile";
import { QuestionSection } from "component/RoadAssistanceMobile/RoadAssistanceMobile/QuestionSection";
import SupportRequestHelper from "component/common/MobileReuseable/SupportRequest";
import TawuniyaAppQuick from "component/common/MobileReuseable/TawuniyaAppQuick/Ithra";
import SlideCard from "component/common/SliderCard/SlideCard";
import Vehicle from "assets/images/mobile/Vehicle_maintenance.png";
import Sports from "assets/images/mobile/Sports_maintenance.png";
const TawuniyaIthra = () => {
  const items = [
    {
      id: 1,
      Partners: "Ziebart",
      discount:
        "40% discount on the installation of thermal insulation films and tinting for full vehicle glass, 35% on all other services.",
      How: "Show the Insurance Card",
    },
    {
      id: 2,
      Partners: "Carspa",
      discount:
        "20% discount on car caring services to all Tawuniya customers.",
      How: "Show the Insurance Card",
    },
    {
      id: 3,
      Partners: "LLumar",
      discount:
        "40% discount on the installation of thermal insulation films for full vehicle glass, 20% discount on the installation of car-paint protection films.",
      How: "Show the Insurance Card",
    },
    {
      id: 4,
      Partners: "Ezhalha",
      discount: "Up to 15% discount to all Tawuniya customers.",
      How: "Use the discount code: TAWUNIYA",
    },
    {
      id: 5,
      Partners: "Joi",
      discount: "15% discount on all products available in the website.",
      How: "Use the discount code: TAWUNIYA",
    },
    {
      id: 6,
      Partners: "9 Round",
      discount:
        "3 months subscription (SR 1,035), 6 months subscription (SR 1,860) and 12 months subscription (SR 3,435).",
      How: "Show the Insurance Card or Insurance Policy",
    },
    {
      id: 7,
      Partners: "The Healthy home",
      discount: "15% discount on all services provided.",
      How: "Show the Insurance Card or Insurance Policy",
    },
    {
      id: 8,
      Partners: "Show the Insurance Card or Insurance Policy",
      discount:
        "40% discount on thermal insulation provided and 50% discount on the other services.",
      How: "Show the Insurance Card or Insurance Policy",
    },
    {
      id: 9,
      Partners: "Al Mosafer",
      discount:
        "6% discount on hotels and tourist packages and 3% discount on flight fares.",
      How: "Use the discount code: TAWUNIYA Alternatively, call 920000997 – special Ext. (3) or visit Al-Mosafer branches. The Insurance Card or Insurance Policy must be shown",
    },
    {
      id: 10,
      Partners: "Al Bazai Toyota & Lexus",
      discount:
        "30% discount on Lexus rentals, 35% on Toyota rentals. 17% discount on Lexus spare parts, 25% discount on Toyota spare parts.",
      How: "30% discount on Lexus rentals, 35% on Toyota rentals. 17% discount on Lexus spare parts, 25% discount on Toyota spare parts.",
    },
    {
      id: 11,
      Partners: "Daily Mealz",
      discount: "20% discount on meals and delivery.",
      How: "Use the discount code: TAWUNIYA",
    },
    {
      id: 12,
      Partners: "Lumi",
      discount: "25% discount on car rentals.",
      How: "Use the discount code: TAWUNIYA",
    },
    {
      id: 13,
      Partners: "Cambridge Weight Plan",
      discount: "50% discount on examination and 40% discount on subscription.",
      How: "Show the Insurance Card or Insurance Policy",
    },
    {
      id: 14,
      Partners: "V-Kool",
      discount: "15% to 40% discount on services packages.",
      How: "Show the Insurance Card or Insurance Policy",
    },
    {
      id: 15,
      Partners: "Noon",
      discount:
        "New Users: 15% discount up to SR 75 on (Noon Express) products.Current Users: 10% discount up to SR 50 on (Noon Express) products.",
      How: "Use the discount code: TAW",
    },
    {
      id: 16,
      Partners: "Aljudaie",
      discount:
        "15% discount on men fabrics, excluding the seasonal big sales and special offers at the retail outlets.",
      How: "Show the Insurance Card or Insurance Policy",
    },
    {
      id: 17,
      Partners: "SPACE Home and Office Furniture",
      discount:
        "5%-10% discount on all products in the Store or through the website.",
      How: "Through the Store: Show the Insurance Card or Insurance Policy Through the website: Use the discount code: TA1",
    },
    {
      id: 18,
      Partners: "Zaatar W Zeit",
      discount: "10% on all products available in all branches.",
      How: "Through the Branch: Show the Insurance Card or Insurance Policy",
    },
    {
      id: 19,
      Partners: "Budget Car Rental",
      discount: "42% discounts on car rentals.",
      How: "Through the Branch: Show the Insurance Card or Insurance Policy",
    },
    {
      id: 20,
      Partners: "Abdul Samad Al Qurashi",
      discount: "50% discount on all products",
      How: "Through the Branch: Show the Insurance Card or Insurance Policy",
    },
    {
      id: 21,
      Partners: "Golden Scent",
      discount:
        "15% discount on orders from the website, excluding Channel, Dior, Benefit and make up forever products",
      How: "Through the website: Use the Discount code: TAWU21",
    },
    {
      id: 22,
      Partners: "Sun Gard",
      discount: "55% discount on all products in the Store.",
      How: "Through the Branch: Show the Insurance Card or Insurance Policy",
    },
    {
      id: 23,
      Partners: "Dar Habka",
      discount:
        "15% discount on all products and services available in the Showroom.",
      How: "Through the Branch: Show the Insurance Card or Insurance Policy",
    },
    {
      id: 24,
      Partners: "Frdah",
      discount: "10% discount on all products.",
      How: "Through the website: Use the Discount code: TAW",
    },
    {
      id: 25,
      Partners: "Creative Closets",
      discount: "20% discount on all products.",
      How: "Through the Branch: Show the Insurance Card or Insurance Policy",
    },
    {
      id: 26,
      Partners: "Closet World",
      discount: "15% discount on all products",
      How: "Show the Insurance Card",
    },
    {
      id: 27,
      Partners: "Prince Thobe",
      discount: "20% discount on Prince fabrics only,",
      How: "Through the Branch: Show the Insurance Card or Insurance Policy",
    },
    {
      id: 28,
      Partners: "Spring Rose",
      discount: "15% discount on all products.",
      How: "Through the Branch: Show the Insurance Card or Insurance Policy",
    },
  ];
  return (
    <div className="ithra_source">
      <HeaderStickyMenu />
      <HeaderStepsSticky title={"Ithra"} buttonTitle="Get Started" />
      <div>
        <div className="ithra_career_banner">
          <img
            src={IthraContainer}
            alt="Banner"
            className="w-100 ithra_image_size"
          />
          <div className="ithra_banner_body">
            <h6>The New "Ithra" Program from Tawuniya </h6>
            <h4>Enrich your life</h4>
            <div className="d-flex ">
              <label>
                Your insurance with Tawuniya protects you, increases your
                distinction, and gives you benefits that add to your daily life.
                Tawuniya has developed the ""Ithra"" Program for you, which is
                the first loyalty program of its kind in the Saudi insurance
                sector, designed specifically to enrich your life with many
                benefits and discounts on your purchases from our partners in
                the various business sectors and more to come, all at special
                offers for our customers throughout the year.
              </label>
            </div>
          </div>
        </div>
      </div>
      <div className="ithra_container_benefits">
        <h4>Services and benefits provided by the "Ithra" program</h4>
        <p>
          The Ithra Program provides many discounts on a wide range of services
          and products provided by distinguished partners and famous brands in
          the various sectors, including, for example:
        </p>
      </div>

      <div>
        <div className="ithra_maintenance">
          <div className="ms-2 mt-2 ">
            <div className="ithra_desc_text ms-3 mx-2">
              <img src={Vehicle} className="ithra_image_position" />
              <p className=" mt-2 mb-0 text-center fw-bold ithra_maintenance_display">
                Vehicle <br />
                Maintenance
              </p>
            </div>
          </div>
          <div className="ms-2 mt-2">
            <div className=""></div>
            <div className="ithra_desc_text ms-3 mx-2">
              <img src={Sports} className="ithra_image_position" />
              <p className=" mt-2 mb-0 text-center fw-bold ithra_maintenance_display">
                Sport <br />
                Clubs
              </p>
            </div>
          </div>
          <div className="ms-2 mt-2">
            <div className=""></div>
            <div className="ithra_desc_text ms-3 mx-2">
              <img src={Vehicle} className="ithra_image_position" />
              <p className=" mt-2 mb-0 text-center fw-bold ithra_maintenance_display">
                Vehicle <br />
                Maintenance
              </p>
            </div>
          </div>
        </div>
        <div className="ithra_maintenance_more">
          <p className="mt-0">and MORE TO COME...</p>
        </div>
      </div>

      <div className="ithra_header">
        <h4>Beneficiaries of the "Ithra" Program</h4>
        <p>All Tawuniya customers having valid insurance. </p>
        <div className="ithra_table">
          <div className="total_table">
            <table>
              <tr className="ithratable_headings">
                <th>Partners</th>
                <th>Percentage of Discount</th>
                <th>How to Get the Discount</th>
              </tr>
              {items.map((item) => (
                <tr>
                  <td className="partners_color">{item.Partners} </td>
                  <td className="ithra_discount">{item.discount} </td>
                  <td className="ithra_insurance">{item.How} </td>
                </tr>
              ))}
            </table>
          </div>
        </div>
      </div>
      <div className="ithra_clients pt-2">
        <p>* Tawuniya will continue adding new partners to this List</p>
      </div>
      <div>
        <TawuniyaAppQuick />
      </div>
      <SupportRequestHelper />
      <div className="ithra_terms">
        <h4>Terms & Conditions</h4>
        <p>
          • You should have valid insurance policy from Tawuniya. <br />• The
          benefits of Ithra Program are available when purchasing directly from
          our partners.
          <br />
          •To avail of the above discounts, you have to show a proof of your
          insurance at Tawuniya. (You may download Tawuniya Application through
          Apple Store for iOS and Google Play for Android to display the
          insurance policy).
        </p>
      </div>
      <FooterMobile />
    </div>
  );
};

export default TawuniyaIthra;
