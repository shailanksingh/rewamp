import React, { useState } from "react";
import HeaderStickyMenu from "component/common/MobileReuseable/HeaderStickyMenu";
import HeaderStepsSticky from "component/common/MobileReuseable/HeaderStepsSticky";
import Container from "assets/about/Container.png";
import "./style.scss";
import device from "assets/about/device.png";
import discount from "assets/about/discount.png";
import motor from "assets/about/motor.png";
import reward from "assets/about/reward.png";
import safe from "assets/about/safe.png";
import FooterMobile from "component/common/MobileReuseable/FooterMobile";
import SupportRequestHelper from "component/common/MobileReuseable/SupportRequest";

const TawuniyaDrive = () => {
  const [data, setData] = useState([
    {
      id: 1,
      image: discount,
      content: "Up to 20% discount on car insurance",
    },
    {
      id: 2,
      image: reward,
      content: "Weekly active rewards",
    },
    {
      id: 3,
      image: safe,
      content: "Safer driving habits",
    },
    {
      id: 4,
      image: motor,
      content: "Safety features",
    },
  ]);
  const [content, setContent] = useState([
    {
      id: 1,
      number: "1",
      title: "Install Tawuniya Drive",
      content:
        "Complete your request details and attached the needed documents",
    },
    {
      id: 2,
      number: "2",
      title: "Drive Safe",
      content: "The application will be reviewed in 3 days by our team",
    },
    {
      id: 3,
      number: "3",
      title: "Get Rewarded",
      content:
        "Reimbursement will be deposited to your bank account within 3 working days after approval",
    },
  ]);
  return (
    <div className="tawuniya-drive">
      <HeaderStickyMenu />
      <HeaderStepsSticky
        title={"Tawuniya Drive"}
        buttonTitle="Claim Your Rewards"
      />
      <div className="tawuniya_image_container">
        <img src={Container} className="tawuniya_image" />
        <div className="device">
          <img src={device} />
        </div>
      </div>
      <div className="total">
        <div className="drive_most">
          <label className="car">Get the most out of your car Insurance</label>
          <p className="save">
            Save up to 20% on your car insurance premiums. Download Tawuniya
            Drive stay safe and earn your rewards
          </p>
        </div>
        <div className="drive_what">
          <label className="drive_tawuniya">What is Tawuniya Drive?</label>
          <p className="drive_para">
            Tawuniya Drive is a new driver behavior program that gives you the
            opportunity to get weekly rewards and discounts on your car
            insurance by earning points based on your driving habits. Once you
            set up the sensor, the Drive app will collect your driving data,
            including acceleration, braking, cornering, speeding, and mobile
            phone use, in addition to other factors that affect driving
            behavior, such as driving late and the distance travelled at night.
            <p className="mt-3">
              Tawuniya has partnered with Discovery Insure and Vitality Group to
              encourage and reward safe driving on the roads. Once you have
              joined Tawuniya Drive, getting your rewards is easy. The safer you
              drive, the more points you earn and the greater the reward.{" "}
            </p>
            <p>
              This program is intended for Al-Shamel insurance policy holders.
            </p>
          </p>
        </div>
        <div className="drive_features">
          <label className="drive_title">Drive features</label>
          <p className="drive_content">
            An engaging and impactful app to improve driving behavior over time
          </p>
          <div className="drive_box row">
            {data.map((datas) => (
              <div className="drive_white">
                <div className="drive_reward">
                  <img src={datas.image} />
                </div>
                <p className="drive_discount">{datas.content}</p>
              </div>
            ))}
          </div>
        </div>
        <div className="drive_work">
          <div className="drive_how">How it works</div>
          <p className="drive_earn">
            You earn Tawuniya Drive points every week based on how well you
            drive. Download the Tawuniya drive app, drive safe and get rewarded.
          </p>
          <div className="dirve_install">
            {content.map((contents) => (
              <div className="drive_orange">
                <div className="drive_number">{contents.number}</div>
                <div>
                  <label className="drive_safe">{contents.title}</label>
                  <p className="drive_complete">{contents.content}</p>
                </div>
              </div>
            ))}
          </div>
        </div>
        <div className="drive_bottom">
          <label className="drive_drive">Drive features</label>
          <p className="drive_engage">
            An engaging and impactful app to improve driving behavior over time
          </p>
        </div>
        <SupportRequestHelper isContact={false} />
        <FooterMobile />
      </div>
      <div className="pc_button_white">
        <div className="pc_button">Get Quote</div>
      </div>
    </div>
  );
};

export default TawuniyaDrive;
