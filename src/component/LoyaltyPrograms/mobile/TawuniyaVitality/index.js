import React from "react";
import "./style.scss";
import HeaderStickyMenu from "component/common/MobileReuseable/HeaderStickyMenu";
import HeaderStepsSticky from "component/common/MobileReuseable/HeaderStepsSticky";
import Tawuniya_Vitality_Image from "assets/images/mobile/Tawuniya_Vitality_Image.svg";
import Tawuniya_Typo from "assets/images/mobile/Tawuniya_Typo.svg";
import Vitality_Typo from "assets/images/mobile/Vitality_Typo.svg";
import Vitality_Lifestyle1 from "assets/images/mobile/Vitality_Lifestyle1.svg";
import Vitality_Group1 from "assets/images/mobile/Vitality_Group1.svg";
import Vitality_work from "assets/images/mobile/Vitality_work.svg";
import right from "assets/about/right.png";
import FooterMobile from "component/common/MobileReuseable/FooterMobile";
import SupportRequestHelper from "component/common/MobileReuseable/SupportRequest";
import TawuniyaAppQuick from "component/common/MobileReuseable/TawuniyaAppQuick/Ithra";
import right_arrow_white_2 from "assets/images/mobile/right_arrow_white_2.png";
import vitality_home from "assets/images/mobile/Vitality_Home.svg";
import Play_Button1 from "assets/images/mobile/Play_Button1.svg";

const TawuniyaVitality = () => {
  const watchFlim = () => {
    window.open(
      "https://careers.tawuniya.com.sa/assets/images/custom-images/ATS/tawuniya.mp4",
      "_self"
    );
  };
  return (
    <div className="vitality_container">
      <HeaderStickyMenu />
      <HeaderStepsSticky
        title={"Tawuniya Vitality"}
        buttonTitle="Get Started"
      />
      <div className="Tawuniya_Vitality">
        <img src={Tawuniya_Vitality_Image} />
        <img src={Tawuniya_Typo} />
        <img src={Vitality_Typo} />
      </div>
      <div className="Vitality_Topic">
        <p className="vitality_more">Do More, Get More..</p>
        <p className="vitality_pioneer">
          Tawuniya is pioneering a new era in the Saudi insurance industry by
          launching for the very first time its unique wellness program,
          Tawuniya Vitality
        </p>
      </div>
      <div className="Vitality_lifestyle mx-3">
        <p className="paraa-text">
          The program helps you in understanding your health status, and works
          with you to elevate it and adopt a healthier, more active, and
          rewarding lifestyle
        </p>
      </div>
      <div className="Vitality_content">
        <h4 className="Vitality_Question">What is Tawuniya Vitality?</h4>
        <p>
          Tawuniya is pioneering a new era in the Saudi insurance industry by
          launching for the very first time its unique wellness program,
          Tawuniya Vitality. The program helps you in understanding your health
          status, and works with you to elevate it and adopt a healthier, more
          active, and rewarding lifestyle
        </p>
        <p>
          It is based on behavioral science along with proven methodologies,
          advanced systems, and a fun app that creates a customized wellness
          program which fits your needs and rewards you for your healthy choices
        </p>
        <p>
          Tawuniya has partnered with the Vitality Group, the global leader in
          wellness based insurance with over 20 million members globally, to
          bring this life changing program to you. It is built on the shared
          value model in insurance, where all our clients, their members, and
          the society benefit from it.
        </p>
        <div className="Vitality_Group">
          <h3>Vitality Group</h3>
          <span>
            Guided by the core purpose of making people healthier, Vitality is
            the leader in improving health to unlock outcomes that matter. By
            blending smart tech, data, incentives, and behavioral science, we
            inspire healthy changes in individuals and organizations. Vitality
            brings a global perspective through successful partnerships with the
            smartest insurers and most forward-thinking employers around the
            world. More than 20 million people in over 20 markets engage in the
            Vitality program.
          </span>
          {/* <div className="playbutton"><img
              src={Play_Button1}
              alt="Play"
              className=" cursor-pointer"
              onClick={watchFlim}
            />
          <h4>Watch the film</h4>
          </div> */}
          <div className="playbutton">
            <img
              src={Play_Button1}
              alt="Play"
              className=" cursor-pointer mt-0"
              onClick={watchFlim}
            />
            <div className="watchfilm">Watch the film</div>
          </div>
        </div>
        <h4 className="Vitality_Question">What is Tawuniya Vitality?</h4>
        <p>
          Tawuniya is pioneering a new era in the Saudi insurance industry by
          launching for the very first time its unique wellness program,
          Tawuniya Vitality. The program helps you in understanding your health
          status, and works with you to elevate it and adopt a healthier, more
          active, and rewarding lifestyle
        </p>
        <p>
          It is based on behavioral science along with proven methodologies,
          advanced systems, and a fun app that creates a customized wellness
          program which fits your needs and rewards you for your healthy choices
        </p>
        <p>
          Tawuniya has partnered with the Vitality Group, the global leader in
          wellness based insurance with over 20 million members globally, to
          bring this life changing program to you. It is built on the shared
          value model in insurance, where all our clients, their members, and
          the society benefit from it.
        </p>
      </div>
      <div className="Vitality_Work">
        <img src={Vitality_work} />
        <div className="Vitality_work_content ">
          <h4>@WORK</h4>
          <h6>
            Increase productivity at work and improve the daily habits of
            employees with a program that has proven its ability in helping
            members understand their health risks and become healthier
          </h6>
          <p>
            The program will promote your company’s culture by having more
            engagement, lower claims, better productivity, and a healthier work
            environment. Along with understanding how healthy and active
            employees are through our unique wellness report.
          </p>
        </div>
      </div>
      <div className="vitality_banner">
        <img src={vitality_home} />
        <div className="vitality_home_body">
          <h5>@HOME</h5>
          <h6>
            Get realistic health and activity goals that are custom made for
            you, and start gaining points and earning rewards every step of the
            way. You and your family will be able to become more active and earn
            instant weekly, monthly, and yearly rewards as you progress.
          </h6>
          <p>
            You don’t need to be an athlete to be healthy, just simply be more
            aware of your health, move whenever and wherever you are, and have
            fun while you do it.
          </p>
        </div>
      </div>

      <div className="Vitality_benefits">
        <h4>Vitality benefits</h4>
        <p>
          Tawuniya is pioneering a new era in the Saudi insurance industry by
          launching for the very first time its unique wellness program,
          Tawuniya Vitality. The program helps you in understanding your health
          status, and works with you to elevate it and adopt a healthier, more
          active, and rewarding lifestyle
        </p>
        <p>
          It is based on behavioral science along with proven methodologies,
          advanced systems, and a fun app that creates a customized wellness
          program which fits your needs and rewards you for your healthy choices
        </p>
        <p>
          Tawuniya has partnered with the Vitality Group, the global leader in
          wellness based insurance with over 20 million members globally, to
          bring this life changing program to you. It is built on the shared
          value model in insurance, where all our clients, their members, and
          the society benefit from it.
        </p>
      </div>
      <TawuniyaAppQuick />
      <SupportRequestHelper />
      <FooterMobile />
      <div className="pc_button_vitality">
        <div className="pc_button_pink">Get Started</div>
      </div>
    </div>
  );
};

export default TawuniyaVitality;
