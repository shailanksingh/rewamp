import HeaderStepsSticky from "component/common/MobileReuseable/HeaderStepsSticky";
import HeaderStickyMenu from "component/common/MobileReuseable/HeaderStickyMenu";
import PolicyCard from "component/common/PolicyCard";
import React from "react";
import "../common/RecentFeeds/style.scss";
import { PlansForFuture } from "./PlansForFuture";
import { myFamilyExclusive, HomePolicyList } from "component/common/MockData";
import "./style.scss";
import { CommonFaq } from "component/common/CommonFaq";
import SupportRequestHelper from "component/common/MobileReuseable/SupportRequest";
import TawuniyaAppQuick from "component/common/MobileReuseable/TawuniyaAppQuick/Ithra";
import { Insurancecard } from "component/MyFamilyMobilePage/Insurancecard";
import VisitVisaCards from "component/MyFamilyMobilePage/VisitVisaCards";
import orangeArrow from "assets/svg/orangeArrow.svg";

function VisitVisa() {
  return (
    <div>
      <HeaderStickyMenu />
      <HeaderStepsSticky title="Visit Visa Insurance" buttonTitle="Buy Now" />

      <div className="visitor_img">
        <h4 className="title">Manage your insurance within minutes!</h4>
        <p className="text-center text-white manage_para">
          The Saudi insurance pioneer, will help you to identify, analyze and
          manage such risks and suggest appropriate insurance solutions.
        </p>
        <div className="container passport_card">
          <p className=" font-weight-bold pt-3">Passport Number</p>
          <input
            type="number"
            placeholder="Enter Passport Number"
            className="pb-3 passport_text"
          />
          <p className=" font-weight-bold pt-3">Mobile Number</p>
          <input
            type="number"
            placeholder="+966 5xxxxxxxx"
            className="pb-3 passport_text"
          />
          <br />
          <button className="buy_button">Get A Quote</button>
        </div>
        <p className="continue">
          By continuing you give Tawuniya advance consent to obtain my and/or my
          dependents' information from the National Information Center.
        </p>
      </div>
      <div className="policy-card">
        <h5>Mange Your Policy</h5>
        <PolicyCard policyList={HomePolicyList} />
      </div>
      <PlansForFuture />
      <div className="question-section">
        <h6>Our Exclusive and Added-Value Services</h6>
        <p>
          In addition to benefits provided by the policy, Tawuniya also provides
          a number of exclusive and added value services that help you get the
          health care you deserve
        </p>
      </div>
      <div>
        <VisitVisaCards />
      </div>
      <div className="question-section">
        <h6>You’ve got questions, we’ve got answers</h6>
        <p className="center_content">
          Review answers to commonly asked questions at Tawuniya, which enable
          you to be directly involved in improving our support experience.
        </p>
        <CommonFaq faqList={myFamilyExclusive} />
      </div>
      <div className="d-flex justify-content-left question">
        <div>
          <span className="viewText fs-12 fw-800 pr-2">View All Questions</span>
          <img
            src={orangeArrow}
            className="img-fluid orangeArrow"
            alt="arrow"
          />
        </div>
      </div>
      {/* <div className="mx-3">
        <MakeClaimCard />
      </div> */}
      <TawuniyaAppQuick />
      <div className="mt-4">
        <SupportRequestHelper isContact={false} />
      </div>
      <div className="y">
        <Insurancecard />
      </div>
    </div>
  );
}

export default VisitVisa;
