import React, { useState } from "react";
import down from "assets/images/mobile/down.png";
import Vector1 from "assets/images/mobile/Vector1.png";
import "./style.scss";

export const PlansForFuture = () => {
  const [state, updateState] = useState(false);
  return (
    <div>
      <div className="MotrProContainer">
        <div className="MotrProText mt-4 ml-3 mb-3 ">
          <h5>Plans For Your Futures</h5>
          <span>
            Simple prices, No hidden fees, Adanced features for your business
          </span>
        </div>
        <div className={`plan ${state ? "fullwidth" : ""}`}>
          <div className="plan-card">
            <div className="plansforfuture">
              <h5 className="futureplancoverage fw-700 fs-16">Coverage</h5>
              <span className="fs-14">
                The maximum benefits limit for each person for the duration of
                the policy and that includes lower limits specified in this
                policy
              </span>
              <h5 className="futureplancoverage fw-700 fs-16 mt-3">Limit</h5>
              <span className="SR fs-16 fw-800">SR 100,000</span>
            </div>
            <div className="plansforfuture1">
              <h5 className="futureplancoverage fw-700 fs-16">Coverage</h5>
              <span className="fs-14">
                The Expenses Of examination And Treatment of Emergency Cases
              </span>
              <h5 className="futureplancoverage fw-700 fs-16 mt-3">Limit</h5>
              <span className="SR fs-16 fw-800">Up to the policy limit</span>
            </div>
            <div className="plansforfuture2">
              <h5 className=" fw-800 fs-20">Hospitalization Expenses</h5>
            </div>
            <div className="plansforfuture2">
              <h5 className="futureplancoverage fw-700 fs-16">Coverage</h5>
              <span className="fs-14">
                Excess percentage (contribution in payment){" "}
              </span>
              <h5 className="futureplancoverage fw-700 fs-16 mt-3">Limit</h5>
              <span className="SR fs-16 fw-800">
                No contribution in payment
              </span>
            </div>
            <div className="plansforfuture1">
              <h5 className="futureplancoverage fw-700 fs-16">Coverage</h5>
              <span className="fs-14 fw-400">Hospitalization</span>
              <h5 className="futureplancoverage fw-700 fs-16 mt-3">Limit</h5>
              <span className="SR fs-16 fw-800">Up to the policy limit</span>
            </div>
            <div className="plansforfuture2">
              <h5 className="futureplancoverage fw-700 fs-16">Coverage</h5>
              <span className="fs-14 fw-400">
                Accommodation and daily subsistence limit for the patient which
                include bed wage, nursing services, visitations, medical
                supervision and life support services, but it does not include
                the cost of medicines and medical supplies, as prescribed by the
                physician)
              </span>
              <h5 className="futureplancoverage fw-700 fs-16 mt-3">Limit</h5>
              <span className="SR fs-16 fw-800">
                (shared room and up to a limit of SR 600/day)
              </span>
            </div>
            <div className="plansforfuture1">
              <h5 className="futureplancoverage fw-700 fs-16">Coverage</h5>
              <span className="fs-14">
                Accommodation limit for patient's escort{" "}
              </span>
              <h5 className="futureplancoverage fw-700 fs-16 mt-3">Limit</h5>
              <span className="SR fs-16 fw-800">
                (shared room and up to a limit of SR 150/day)
              </span>
            </div>
            <div className="plansforfuture2">
              <h5 className="futureplancoverage fw-700 fs-16">Coverage</h5>
              <span className="fs-14">
                Treatment of emergency maternity and delivery cases
              </span>
              <h5 className="futureplancoverage fw-700 fs-16 mt-3">Limit</h5>
              <span className="SR fs-16 fw-800">
                Up to SR 5000 for the duration of the policy
              </span>
            </div>
            <div className="plansforfuture1">
              <h5 className="futureplancoverage fw-700 fs-16">Coverage</h5>
              <span className="fs-14">
                Cost of travel and accompanying of one direct family member{" "}
              </span>
              <h5 className="futureplancoverage fw-700 fs-16 mt-3">Limit</h5>
              <span className="SR fs-16 fw-800">
                Up to SR 5000 for the duration of the policy
              </span>
            </div>
            <div className="plansforfuture2">
              <h5 className="futureplancoverage fw-700 fs-16">Coverage</h5>
              <span className="fs-14">Emergency dental treatment</span>
              <h5 className="futureplancoverage fw-700 fs-16 mt-3">Limit</h5>
              <span className="SR fs-16 fw-800">
                Up to SR 500 for the duration of the policy
              </span>
            </div>
            <div className="plansforfuture1">
              <h5 className="futureplancoverage fw-700 fs-16">Coverage</h5>
              <span className="fs-14">
                The expenses of the birth and treatment of premature babies{" "}
              </span>
              <h5 className="futureplancoverage fw-700 fs-16 mt-3">Limit</h5>
              <span className="SR fs-16 fw-800">Up to the policy limit</span>
            </div>
            <div className="plansforfuture2">
              <h5 className="futureplancoverage fw-700 fs-16">Coverage</h5>
              <span className="fs-14">
                Treatment to injuries resulting from road traffic accidents
              </span>
              <h5 className="futureplancoverage fw-700 fs-16 mt-3">Limit</h5>
              <span className="SR fs-16 fw-800">Up to the policy limit</span>
            </div>
            <div className="plansforfuture1">
              <h5 className="futureplancoverage fw-700 fs-16">Coverage</h5>
              <span className="fs-14">
                The expenses of emergency kidney Dialysis{" "}
              </span>
              <h5 className="futureplancoverage fw-700 fs-16 mt-3">Limit</h5>
              <span className="SR fs-16 fw-800">Up to the policy limit</span>
            </div>
            <div className="plansforfuture2">
              <h5 className="futureplancoverage fw-700 fs-16">Coverage</h5>
              <span className="fs-14">
                Emergency Medical Evacuation inside and outside the Kingdom
              </span>
              <h5 className="futureplancoverage fw-700 fs-16 mt-3">Limit</h5>
              <span className="SR fs-16 fw-800">Up to the policy limit</span>
            </div>
            <div className="plansforfuture1">
              <h5 className="futureplancoverage fw-700 fs-16">Coverage</h5>
              <span className="fs-14">
                Repatriation of mortal remains to the country of origin.
              </span>
              <h5 className="futureplancoverage fw-700 fs-16 mt-3">Limit</h5>
              <span className="SR fs-16 fw-800">
                Up to SR 10,000 for the duration of the policy
              </span>
            </div>
            <div className="plansforfuture2">
              <h5 className="futureplancoverage fw-700 fs-16">Coverage</h5>
              <span className="fs-14">Scope of coverage</span>
              <h5 className="futureplancoverage fw-700 fs-16 mt-3">Limit</h5>
              <span className="SR fs-16 fw-800">Kingdom of Saudi Arabia</span>
              <p className="fs-14">
                <ul>
                  <li>
                    This policy is issued once, and upon expiry a new policy
                    shall be issued and does not need to be valid for one
                    complete year.
                  </li>
                  <li>
                    These conditions apply to all applicants intending to obtain
                    a visa to enter the Kingdom for the purpose of visit or
                    extension, transit, or tourism.
                  </li>
                </ul>{" "}
              </p>
            </div>
          </div>
        </div>
        <div className="d-flex plansforfuture3">
          <h5
            className="futureplancoverage1 fw-700 fs-16 mr-2"
            onClick={() => updateState(!state)}
          >
            More Benefits
          </h5>
          {state && <img src={Vector1} alt="Vector1"></img>}
          {!state && <img src={down} alt="down"></img>}
        </div>
      </div>
      <div className="pb-5 p-3">
        <div className="card visit-download mt-4 p-2">
          <h6 className="font-weight-bold text-center mt-4">
            {" "}
            Want to download our Policies? Here you can find it.
          </h6>
          <button className="visit_btn">Visit Visa Insurance Policy</button>
          <button className="covid_btn mb-4">Covid-19 Policy</button>
        </div>
      </div>

      <div className="benefit-use">
        <h4>Useful Information</h4>
        <div className="UsefulInformation">
          <h5 className=" fw-800 fs-24">
            Beneficiaries of Visitor Insurance Program
          </h5>
          <span className="UsefulInformationcontent fs-16">
            All members and their companions who apply for entry visas to enter
            the Kingdom of Saudi Arabia for the purpose of visiting the Kingdom,
            extending the visit or for transiting through the country, are
            eligible to benefit from this insurance as they have to submit a
            valid insurance certificate within the Kingdom which covers
            emergency medical cases and medical evacuation. Considering that
            this insurance excludes Umrah & Hajj pilgrims, holders of diplomatic
            and special passports, companies and international organizations
            visitors, diplomatic treatment in accordance with the reciprocity
            principle, and guests of the Kingdom.
          </span>
        </div>

        <div className=" UsefulInformation">
          <h5 className=" fw-800 fs-24">
            Extension of Visitor Insurance Policy
          </h5>
          <span className="UsefulInformationcontent fs-16">
            Visitors of the Kingdom of Saudi Arabia holding insurance policy can
            easily extend the tenure of the policy through Tawuniya Company
            Online Store. It worth noting that all policies and conditions of
            the Visitor Insurance Program will apply in the event of the policy
            extension and the extended Insurance Coverage starts from the expiry
            date of the valid policy.
          </span>
        </div>

        <div className=" UsefulInformation">
          <h5 className=" fw-800 fs-24">
            Beneficiaries of Tourist Insurance Program
          </h5>
          <span className="UsefulInformationcontent fs-16">
            In line with the decision of the Kingdom of Saudi Arabia to allow
            the citizens of a number of countries to obtain Tourist Visa as of
            September 2019, Tawuniya Insurance Company launched the Tourist
            Insurance Program which mainly aims to provide the highest standards
            of health care for Emergency Medical Cases and Accidents. All
            applicants for Tourist Visas to enter the Kingdom of Saudi Arabia
            are eligible to benefit from this insurance as they must present a
            valid insurance certificate in the Kingdom Saudi Arabia.
          </span>
        </div>

        <div className=" UsefulInformation">
          <h5 className=" fw-800 fs-24">
            Beneficiaries of the Mandatory COVID-19 Risks Coverage Program
          </h5>
          <span className=" UsefulInformationcontent fs-16">
            All travelers coming to the Kingdom of Saudi Arabia for the purpose
            of visit/tourism are eligible for this coverage in order to cover
            the damage and problems resulting from the infection with
            Coronavirus disease (COVID-19) to preserve the insured’s health and
            ensure his safety. This coverage provide a package of benefits for
            the treatment of emergency medical cases, costs of medical
            quarantine, medical evacuation, and the return of the mortal remains
            of the insured to his home country. Considering that this coverage
            does not include the PCR test costs.
          </span>
        </div>
      </div>
    </div>
  );
};
