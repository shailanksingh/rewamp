import WhiteHeaderSticky from "component/common/MobileReuseable/WhiteHeaderSticky";
import React from "react";
import "./style.scss";
import CarGreyIcon from "assets/svg/car-grey.svg";
import chatSmile from "assets/svg/Chat-smile.svg";
import { RecentPolicyCard } from "component/DashBoard/DashBoardComponents/RecentPolicyCard";

const recentPloicyCardData = [
  {
    id: 1,
    time: "30 Minutes Ago",
    progressLabel: "In progress",
    policyIcon: CarGreyIcon,
    title: "Almouasat Hospital",
    subtitle: "Brain MRI Approval.",
    getHelpIcon: chatSmile,
  },
  {
    id: 2,
    time: "30 Minutes Ago",
    progressLabel: "In progress",
    policyIcon: CarGreyIcon,
    title: "Almouasat Hospital",
    subtitle: "Brain MRI Approval.",
    getHelpIcon: chatSmile,
  },
];

const ApprovalRequestList = () => {
  return (
    <div className="approval_request_list_container">
      <WhiteHeaderSticky
        isMenu={true}
        title={"Service"}
        SubTitle="Approvals Requests"
        menuOption={"All Approvals"}
      />
      {recentPloicyCardData.map((data, id) => {
        return (
          <RecentPolicyCard
            time={data.time}
            progressLabel={data.progressLabel}
            policyIcon={data.policyIcon}
            title={data.title}
            subtitle={data.subtitle}
            getHelpIcon={data.getHelpIcon}
          />
        );
      })}
    </div>
  );
};

export default ApprovalRequestList;
