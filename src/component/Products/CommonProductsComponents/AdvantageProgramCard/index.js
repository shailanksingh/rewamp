import React from "react";
import primaryArrow from "assets/svg/TawuniyaServicesIcons/primaryArrow.svg";
import "./style.scss";

export const AdvantageProgramCard = ({
  isTransparent,
  classDivision,
  headerData,
  advantageCardData = false,
  tranparentadvantagetitle,
  tranparentadvantagepara,
  lightAdvantageHeaderClass,
  lightAdvantageParaClass,
  isOddDivision = false,
  isCardLayout = false,
  isCardLayoutTransparent = false,
  tarnsparentFlexTitle,
  lightFlexTitle,
  advantageFlexCardData,
  advantageFlexDivision,
  isLearnMore = false,
  learnMoreClass,
  learnMoreArrowClass,
}) => {
  return (
    <>
      {isCardLayout ? (
        <React.Fragment>
          <div className="row advantage-flex-card-layout px-3">
            {advantageFlexCardData.map((item, i) => {
              return (
                <div className={advantageFlexDivision} key={i}>
                  <div
                    className={`${
                      isCardLayoutTransparent
                        ? "transparent-advantage-flex-card-box"
                        : "light-advantage-flex-card-box"
                    } p-3`}
                  >
                    <div className="d-flex flex-row">
                      <div className="pr-3">
                        <img
                          src={item.icon}
                          className="img-fluid advantageFlexIcon"
                          alt="icon"
                        />
                      </div>

                      <div>
                        <p
                          className={`${
                            isCardLayoutTransparent
                              ? tarnsparentFlexTitle
                              : lightFlexTitle
                          } fw-800 mb-0`}
                        >
                          {item.title}
                        </p>
                      </div>
                    </div>
                  </div>
                </div>
              );
            })}
          </div>
        </React.Fragment>
      ) : (
        <React.Fragment>
          <div className="row home-insurance-landing-conatiner">
            <div className="col-lg-12 col-12 pt-5 pb-3">
              <div className="d-flex justify-content-center">
                <div>
                  <p className="fs-34 fw-800 home-tawuniya-card-header text-center pt-5 m-0">
                    {headerData.title}
                  </p>
                </div>
              </div>
              <div className="d-flex justify-content-center">
                <div>
                  <p className="fs-16 fw-400 home-tawuniya-card-para text-center">
                    {headerData.para}
                  </p>
                </div>
              </div>
            </div>
          </div>
          <div
            className={
              isOddDivision
                ? "row advantageProgramContainer row-cols-2 row-cols-lg-5 g-2 g-lg-3"
                : "row advantageProgramContainer pb-5"
            }
          >
            {advantageCardData.map((item, i) => {
              return (
                <div className={classDivision} key={i}>
                  <div
                    className={
                      isTransparent
                        ? "transparent-advantage-card"
                        : "light-advantage-card"
                    }
                  >
                    <img
                      src={item.icon}
                      className={`${
                        isTransparent
                          ? "tranparent-advantage-icon"
                          : "light-advantage-icon"
                      } img-fluid`}
                      alt="icon"
                    />
                    <p
                      className={`${
                        isTransparent
                          ? tranparentadvantagetitle
                          : `${lightAdvantageHeaderClass} light-advantage-title`
                      } fw-800 m-0 pt-3`}
                    >
                      {item.title}
                    </p>
                    <p
                      className={`${
                        isTransparent
                          ? tranparentadvantagepara
                          : `${lightAdvantageParaClass} light-advantage-para`
                      } fw-400 m-0`}
                    >
                      {item.para}
                    </p>
                    {isLearnMore && <div className="py-4" />}
                    {isLearnMore && (
                      <div className="learnMoreContainer">
                        <span className={`${learnMoreClass} fw-800 pr-2`}>
                          Learn More
                        </span>{" "}
                        <img
                          src={primaryArrow}
                          className={`${learnMoreArrowClass} img-fluid`}
                          alt="icon"
                        />
                      </div>
                    )}
                  </div>
                </div>
              );
            })}
          </div>
        </React.Fragment>
      )}
    </>
  );
};
