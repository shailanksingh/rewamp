import React from "react";
import "./style.scss";

export const ProgramTermsBanner = ({ programTermsBannerData }) => {
  return (
    <div className="row ">
      <div className="col-12 pb-5">
        <div className="Program-terms-container">
          <p className="fs-34 fw-800 programTerms-title text-center pt-5 pb-2">
            {programTermsBannerData.title}
          </p>
          <div className="row pb-5 px-2">
            {programTermsBannerData.data?.map((item, i) => {
              return (
                <div className="col-3 pb-3 px-2" key={i}>
                  <div
                    className="program-terms-banner-card p-4"
                    id={item.alignCard}
                  >
                    <p className="fs-28 fw-800 m-0 pb-2 program-terms-banner-card-title">
                      {item.heading}
                    </p>
                    <p className="fs-17 fw-800 m-0 program-terms-banner-card-para">
                      {item.txt}
                    </p>
                  </div>
                </div>
              );
            })}
          </div>
        </div>
      </div>
    </div>
  );
};
