import React from "react";
import "./style.scss";

export const ProgramAdvantages = () => {
	return (
		<div className="ProgramAdvantages px-4">
			<div className="ProgramAdvantagesLeft">
				<h2 className="mb-4 pb-2">Program Advantages</h2>
				<div className="ProgramAdvantagesCard">
					<h5>Program Cover</h5>
					<p>
						This includes any physical or mental injury to any patient committed
						by the Insured in the course of his work due to negligence, omission
						or error or be linked to the medical profession practiced in Saudi
						Arabia. The insurance cover available under this Program is for 4
						options of compensation limits required.
					</p>
					<p>
						The cover includes all defense costs incurred with the Company's
						consent in respect of any claim, provided that the total amount
						payable in respect of damages shall not exceed the payable limits of
						indemnity.
					</p>
				</div>
			</div>
			<div className="ProgramAdvantagesRight">
				<div className="ProgramAdvantagesCard">
					<h5>The Value</h5>
					<p>
						The program covers the amount of indemnities decided by virtue of an
						official verdict pronounced by the competent courts or legal
						authorities versus the Insured who causes medical harm to the
						patient arising out of an error or negligence or omissions during
						his practicing to his profession.
					</p>
					<p>
						This indemnity reaches up to a maximum limit ranging between SR
						100,000 and SR 1000,000 per one occurrence or in the annual
						aggregate in accordance with the policy terms and conditions.
					</p>
				</div>
			</div>
		</div>
	);
};
