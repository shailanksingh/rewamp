import React from "react";
import bluetickwhitebg from "assets/svg/TawuniyaServicesIcons/bluetickwhitebg.svg";
import "./style.scss";

export const KeyBenefits = ({ KeyBenefitsTitle, keyBenefitsData }) => {
	return (
		<div className="key-benefits-container d-flex justify-content-between px-4 align-items-center">
			<div className="key-benefits-title">
				<h5>{KeyBenefitsTitle.title}</h5>
				<p>{KeyBenefitsTitle.subtitle}</p>
			</div>
			<div className="key-bemefits-box-container">
				{keyBenefitsData.map((item, id) => {
					return (
						<div
							className="key-benefits d-flex align-items-center px-3 py-2"
							key={id}
						>
							<img className="mr-3" src={bluetickwhitebg} alt="Tick icon" />
							<p>{item.content}</p>
						</div>
					);
				})}
			</div>
		</div>
	);
};
