import React, { useState } from "react";
import { history } from "service/helpers";
import "./style.scss";
import MembersInformationIcon from "assets/svg/members-information.svg";
import ExistingMembersIcon from "assets/svg/existing-members-icon.svg";
import MembersIcon from "assets/svg/members-icon.svg";
import PolicyDetailsIcon from "assets/svg/policy-details-icon.svg";
import { NormalButton } from "component/common/NormalButton";
import { NormalInput } from "component/common/NormalInput";
import selectArrow from "assets/svg/selectArrowIconHome.svg";
import { NormalSelect } from "component/common/NormalSelect";
import PlansForFuture from "./PlansForFuture";
import PlansForYourTrip from "./PlansForYourTrip";
import PlansForFutureMyFamily from "./PlansForFutureMyFamily";

let manageYourPolicy = [
	{
		icon: PolicyDetailsIcon,
		text: "Check Your policy details",
	},
	{
		icon: MembersIcon,
		text: "Add/Delete members ",
	},
	{
		icon: ExistingMembersIcon,
		text: "Upgrade existing members ",
	},
	{
		icon: MembersInformationIcon,
		text: "Update members information",
	},
];

export const ProductsBanner = ({ bannerDetails = {}, manageYourPolicy: manageYourPolicyProps }) => {
	let { bannerImage, topTextContent, bannerText } = bannerDetails;
	const [value, setValue] = useState("");
	const openInNewTab = (url1) => {
		const newWindow = window.open(url1, "_blank", "noopener,noreferrer");
		if (newWindow) newWindow.opener = null;
	};
	let pathArray = history.location.pathname.split("/");
	let currentPage = pathArray[pathArray.length - 1];
	let hideBannerForm = currentPage === "healthinsurancevisitvisa" || currentPage === "healthinsurancemyfamily";
	return (
		<div className="property-casualty-products-banner-common px-4">
			{topTextContent && (
				<div className="top-text-content">
					<div className="text-content-left">
						<div className="title">{topTextContent?.title}</div>
						<div className="description">{topTextContent?.description1}</div>
					</div>
					<div className="text-content-right">
						<div className="title">{topTextContent?.subtitle}</div>
						<div className="description">{topTextContent?.description2}</div>
					</div>
				</div>
			)}
			<div className="banner-bg">
				<div
					className="banner-image"
					style={{ backgroundImage: "url(" + bannerImage + ")" }}
				>
					<div className="banner-text-content-root">
						<div className="banner-top">
							<div className="title">{bannerText?.title}</div>
							<div className="description">{bannerText?.description}</div>
							{!hideBannerForm &&
								<>
									<div className="inputContainer-product-banner mt-2">
										<div className="d-flex flex-row pl-3">
											{currentPage === "homeinsurance" && (
												<div className="inputOne">
													<span className="inputTitleOne fs-16 fw-800">Name</span>
													<NormalInput
														className="inpOneField"
														value={value}
														variant="outlined"
														placeholder="Enter your Name"
														onChange={(e) => setValue(e.target.value)}
													/>
												</div>
											)}
											{currentPage === "internationaltravel" && (
												<div className="inputOne">
													<span className="inputTitleOne fs-16 fw-800">
														Passport Number
													</span>
													<NormalInput
														className="inpOneField"
														value={value}
														variant="outlined"
														placeholder="Enter your Passport Number"
														onChange={(e) => setValue(e.target.value)}
													/>
												</div>
											)}
											{currentPage === "medicalmalpractice" && (
												<>
													<div className="inputOne">
														<span className="inputTitleOne fs-16 fw-800">
															National ID
														</span>
														<NormalInput
															className="inpOneField"
															value={value}
															variant="outlined"
															placeholder="Enter your ID number"
															onChange={(e) => setValue(e.target.value)}
														/>
													</div>
													<div className="inputTwo">
														<span className="inputTitleTwo fs-16 fw-800">
															Year of Birth
														</span>
														<div className="w-100">
															<NormalSelect
																className="bannerSelectInput"
																arrowVerticalAlign="bannerArrowAlign"
																placeholder="Select year"
																selectArrow={selectArrow}
																selectControlHeight="22px"
																selectFontWeight="400"
															/>
														</div>
													</div>
												</>
											)}
											<div className="inputThree">
												<span className="inputTitleThree fs-16 fw-800">
													Mobile Number
												</span>
												<NormalInput
													className="inpThreeField"
													value={value}
													variant="outlined"
													placeholder="+966 5xxxxxxxx"
													onChange={(e) => setValue(e.target.value)}
												/>
											</div>
											<div className="inpBtnField">
												<div>
													<NormalButton
														className="buyButton p-3"
														label="Buy Now"
														onClick={() => openInNewTab("#")}
													/>
												</div>
											</div>
										</div>
									</div>
								</>
							}
							{bannerText?.note && <div className="note">{bannerText?.note}</div>}
						</div>
						<div className="banner-bottom-root">
							<div className="banner-bottom-title">Mange Your Policy</div>
							<div className="banner-bottom">
								{(manageYourPolicyProps ? manageYourPolicyProps : manageYourPolicy)?.map(({ icon, text }, index) => {
									return (
										<div className="bottom-card" key={index.toString()}>
											<img src={icon} alt="..." /> {text}
										</div>
									);
								})}
							</div>
						</div>
					</div>
				</div>
				{currentPage === "internationaltravel" && (<PlansForFuture />)}
				{currentPage === "healthinsurancevisitvisa" && (<PlansForYourTrip />)}
				{currentPage === "healthinsurancemyfamily" && (<PlansForFutureMyFamily />)}
			</div>
		</div>
	);
};
