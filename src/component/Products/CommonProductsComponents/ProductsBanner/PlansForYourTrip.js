import React from "react";
import RightArrowIcon from "assets/images/menuicons/right-arrow.svg";

const PlansForYourTrip = () => {
    return (
        <div className="plans-for-your-trip">
            <div className="title">Plans For Your Trip</div>
            <div className="subtitle">The benefits and limit of coverage under Visitors Insurance Program</div>
            <div  className="plans-for-your-trip-content-root">
                <div className="plans-for-your-trip-content">
                    <div className="title-block row bottom-bordered">
                        <div className="col-6">Coverage</div>
                        <div className="col-6">Limit</div>
                    </div>
                    <div className="row bottom-bordered">
                        <div className="col-6">The maximum benefits limit for each person for the duration of the policy and that includes lower limits specified in this policy</div>
                        <div className="col-6"><strong>SR 100,000</strong></div>
                    </div>
                    <div className="row bottom-bordered grey-bg">
                        <div className="col-6">The expenses of examination and treatment of emergency cases</div>
                        <div className="col-6"><strong>Up to the policy limit</strong></div>
                    </div>
                    <div className="title-block-black bottom-bordered">
                        Hospitalization Expenses
                    </div>
                    <div className="row bottom-bordered">
                        <div className="col-6">Excess percentage (contribution in payment) </div>
                        <div className="col-6"><strong>No contribution in payment</strong></div>
                    </div>
                    <div className="row bottom-bordered grey-bg">
                        <div className="col-6">Hospitalization </div>
                        <div className="col-6"><strong>Up to the policy limit</strong></div>
                    </div>
                    <div className="row">
                        <div className="col-6">Accommodation and daily subsistence limit for the patient which include bed wage, nursing services, visitations, medical supervision and life support services, but it does not include the cost of medicines and medical supplies, as prescribed by the physician </div>
                        <div className="col-6"><strong>(shared room and up to a limit of SR 600/day)</strong></div>
                    </div>
                </div>
                <div className="more-benefits">
                    <div>More Benefits <img src={RightArrowIcon} alt="..." /></div>
                </div>
                <div className="bottom-box-content">
                    <div className="content-left">
                        Want to download our Policies? <br />
                        Here you can find it.
                    </div>
                    <div className="content-right">
                        <button>Visit Visa Insurance Policy</button>
                        <button>Covid-19 Policy</button>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default PlansForYourTrip;