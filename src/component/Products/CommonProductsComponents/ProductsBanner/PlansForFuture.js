import React from "react";
import ExpandIcon from "assets/svg/expand-black-bg-icon.svg";

const PlansForFuture = () => {
    let plansForFutureList = [{
        title: "Schengen",
        description: "Including 26 Countries"
    }, {
        title: "KSA",
        description: "Kingdom of Saudi Arabia"
    }, {
        title: "GCC",
        description: "Gulf Cooperation Council"
    }, {
        title: "Worldwide",
        description: "Including USA & Canada"
    }];
    return (
        <div className="plans-for-future">
            <div className="title">Plans For Your Future</div>
            <div className="subtitle">Simple prices, No hidden fees, Adanced features for your business</div>
            <div className="plans-for-future-list">
                {plansForFutureList?.map(({
                    title,
                    description
                }, index) => {
                    return (
                        <div className="content-box" key={index.toString()}>
                            <div className="box-top">
                                <div className="box-top-left">
                                    <div className="box-title">{title}</div>
                                    <div className="box-description">{description}</div>
                                </div>
                                <div>
                                    <img src={ExpandIcon} alt="..." />
                                </div>
                            </div>
                            <div className="box-bottom">
                                <button>Buy Now</button>
                            </div>
                        </div>
                    );
                })}
            </div>
        </div>
    );
}

export default PlansForFuture;