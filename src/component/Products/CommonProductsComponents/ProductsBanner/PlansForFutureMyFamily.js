import React from "react";
import ExpandIcon from "assets/svg/expand-black-bg-icon.svg";
import PlanForFutureImage from "assets/images/plan-for-future-image1.svg";

const PlansForFutureMyFamily = () => {
    let plansForFutureList = [{
        title: "Silver",
        description: "30,000 SR"
    }, {
        title: "Gold",
        description: "50,000 SR"
    }, {
        title: "Platinum",
        description: "100,000 SR"
    }, {
        title: "Diamond",
        description: "250,000 SR"
    }];
    return (
        <div className="plans-for-future-my-family">
            <div className="title">Plans For Your Future</div>
            <div className="subtitle">Simple prices, No hidden fees, Adanced features for your business</div>
            <div className="plans-for-future-list">
                {plansForFutureList?.map(({
                    title,
                    description
                }, index) => {
                    return (
                        <div className="content-box" key={index.toString()}>
                            <div className="box-top">
                                <div className="box-top-left">
                                    <div className="box-title">{title}</div>
                                    <div className="box-subtitle">Maximum limit per person</div>
                                    <div className="box-price">{description}</div>
                                </div>
                                <div>
                                    <img src={ExpandIcon} alt="..." />
                                </div>
                            </div>
                            <div className="box-bottom">
                                <button>Enquiry Now</button> <div className="compare">Compare</div>
                            </div>
                        </div>
                    );
                })}
            </div>
            <div className="plans-description">
                The plans of My Family program include many of the benefits, privileges and services that are compatible with your needs and that of your family to provide comprehensive medical cover up to a maximum limit of <strong>SR 250,000</strong> per person per policy year.
            </div>
            <div className="grey-bg-box">
                <img src={PlanForFutureImage} className="bg-image" alt="..." />
                <div className="box-content">
                    <div className="box-content-left">
                        <div className="box-content-title">Can't find a suitable plan for you?</div>
                        <div className="box-content-description">Simple prices, No hidden fees, Adanced features for your business</div>
                    </div>
                    <div className="box-content-right">
                        <button>Contact Us Now</button>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default PlansForFutureMyFamily;