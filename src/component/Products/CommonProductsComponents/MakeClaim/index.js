import React from "react";
import { NormalButton } from "component/common/NormalButton";
import "./style.scss";

export const MakeClaim = ({ claimCardData }) => {
  return (
    <div className="row makeClaimContainer pt-5">
      <div className="col-lg-12 col-12">
        <div className="makeClaimCard">
          <img
            src={claimCardData.banner}
            className="img-fluid makeClaim-Banner"
            alt="banner"
          />
          <p className="fs-36 fw-800 makeClaimTitle m-0">
            {claimCardData.title}
          </p>
          <p className="fs-14 fw-400 makeClaimPara">{claimCardData.para}</p>
          <NormalButton
            label={claimCardData.buttonLabel}
            className="claimCardButtom"
          />
        </div>
      </div>
    </div>
  );
};
