import { ProductsBanner } from "component/Products/CommonProductsComponents/ProductsBanner";
import React from "react";
import BannerImage from "assets/images/medical-malpractice-banner.jpeg";

export const MedicalMalpracticeBanner = () => {
	return (
		<div>
			<ProductsBanner 
				bannerDetails={{
					bannerImage: BannerImage,
					topTextContent: {
						title: "For A Peace Of Mind Against Reputational Risks",
						subtitle: "Medical Malpractice Insurance Program",
						description1: "The program protects those persons practicing medical professions from the risks associated with their work, and the third-party legal liability that may arise from any error, negligence, or omission incurred during their work.",
						description2: "For Surgeons, obstetrics & gynecology and anesthesia specialists, Doctors of other medical categories, Pharmacists, Nurses and Technicians, Paramedics and technicians",
					},
					bannerText: {
						title: "Insure yourself within minutes!",
						description: "The Saudi insurance pioneer, will help you to identify, analyze and manage such risks and suggest appropriate insurance solutions.",
						note: "By continuing you give Tawuniya advance consent to obtain my and/or my dependents' information from the National Information Center."
					}
				}}
			/>
		</div>
	);
};
