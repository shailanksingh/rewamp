import React from "react";
import { Faq } from "component/common/FraudDetails";
import {
  faqTravelData,
  faqContainerTravelData,
} from "component/common/MockData/index";
import { MotorOtherProducts } from "component/MotorPage/MotorInsuranceComponent/MotorComponennts/MotorOtherProducts/index";
import { TwuniyaAppAdvert } from "component/common/TwuniyaAppAdvert";
import { MakeClaim } from "component/Products/CommonProductsComponents/MakeClaim";
import { KeyBenefits } from "component/Products/CommonProductsComponents/KeyBenefits";
import docsblue from "assets/svg/TawuniyaServicesIcons/docsblue.svg";
import makeClaimBanner from "assets/svg/TawuniyaServicesIcons/makeClaimBanner.svg";
import "./style.scss";
import { AdvantageProgramCard } from "component/Products/CommonProductsComponents/AdvantageProgramCard";
import icon from "assets/svg/TawuniyaServicesIcons/calamities.svg";

const MedicalmalkeyBenefitsText = {
  title: "Key Benefits of a the Medical Malpractice Policy by Tawuniya",
  subtitle: "Coverage for individuals and healthcare facilities",
};

const MedicalMalkeyBenefitsData = [
  {
    id: 1,
    content:
      "The policy is compatible with the laws and regulations of the Saudi Commission for Health Specialties",
  },
  {
    id: 1,
    content:
      "The policy provides protection against the financial consequences arising from any error, omission, or negligence committed to practicing the medical profession",
  },
  {
    id: 1,
    content:
      "Your legal liability towards the third parties is covered for all financial amounts and legal costs against you",
  },
  {
    id: 1,
    content:
      "Proving the existence of insurance cover protects you in the case has been deprived or prohibited from traveling outside the Kingdom during the course of any lawsuit versus you",
  },
];

const relatedDocsData = [
  {
    id: 1,
    content: "Medical Malpractice leaflet",
  },
  {
    id: 2,
    content: "Medical Malpractice Insurance Claim Form",
  },
  {
    id: 3,
    content: "Medical Malpractice Policy Wording",
  },
  {
    id: 4,
    content: "Insurance Consumer Protection Principles",
  },
  {
    id: 5,
    content: "Medical Malpractice Proposal",
  },
  {
    id: 6,
    content: "Medical Malpractice Proposal Form",
  },
];

const medicalMalpracticeClaimCardData = {
  banner: makeClaimBanner,
  title: "How do I make a claim?",
  para: "Tawuniya believes in simplification and automation wherever possible. Our approach to the claims journey for Tawuniya is no different. We want you to notify us of claims as soon as possible and we want to get your claim moving as soon as possible. Our simple steps approach helps us to keep that focus throughout the claim process.",
  buttonLabel: "Claim Assistance",
};

const keyBenifitsFlexCardData = [
  {
    id: 0,
    icon: icon,
    title: "Medical Malpractice leaflet",
  },
  {
    id: 1,
    icon: icon,
    title: "Medical Malpractice Insurance Claim Form",
  },
  {
    id: 2,
    icon: icon,
    title: "Medical Malpractice Policy Wording",
  },
  {
    id: 3,
    icon: icon,
    title: "Insurance Consumer Protection Principles",
  },
  {
    id: 3,
    icon: icon,
    title: "Medical Malpractice Proposal",
  },
  {
    id: 3,
    icon: icon,
    title: "Medical Malpractice Proposal Form",
  },
];

export const KeyBenefitsRelatedDocs = () => {
  return (
    <>
      <div className="row">
        <div className="col-lg-12 col-12 py-5 ">
          {/* <KeyBenefits
            KeyBenefitsTitle={MedicalmalkeyBenefitsText}
            keyBenefitsData={MedicalMalkeyBenefitsData}
          />
          <div className="related-docs px-4 mt-5">
            <h5 className="text-center mb-5">Related Documents</h5>
            <div className="row">
              {relatedDocsData.map((item, id) => {
                return (
                  <div className="col mx-2 pt-4 related-docs-box" key={id}>
                    <img className="mr-2" src={docsblue} alt="" />
                    <p>{item.content}</p>
                  </div>
                );
              })}
            </div>
          </div> */}
          <AdvantageProgramCard
              isCardLayout={true}
              advantageFlexDivision="col-2 px-2"
              advantageFlexCardData={keyBenifitsFlexCardData}
              lightFlexTitle="fs-14 keyBenifiCardTitle"
            />
        </div>
        <div className="col-lg-12 col-12">
          <Faq
            title="Frequently Asked Questions"
            faqHeaderClass="fs-26 fw-800 medicalMalpractice-faqHeader text-center m-0"
            faqParaClass="fs-16 fw-400 medicalMalpractice-faqPara text-center pb-3"
            description="Review answers to commonly asked questions at Tawuniya, which enable you to be directly involved in improving our support experience."
            faqData={faqTravelData}
            faqContainerData={faqContainerTravelData}
          />
        </div>
        <div className="col-lg-12 col-12 py-5">
          <MakeClaim claimCardData={medicalMalpracticeClaimCardData} />
        </div>
        <div className="col-lg-12 col-12 pt-3">
          <TwuniyaAppAdvert />
        </div>
        <div className="col-lg-12 col-12">
          <MotorOtherProducts isSpace="pb-5" />
        </div>
      </div>
    </>
  );
};
