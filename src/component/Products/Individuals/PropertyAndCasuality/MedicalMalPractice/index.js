import React from "react";
import { ProgramAdvantages } from "component/Products/CommonProductsComponents/ProgramAdvantages";
import { KeyBenefitsRelatedDocs } from "./KeyBenefitsRelatedDocs";
import { MedicalMalpracticeWhy } from "./MedialMalpracticeWhy";
import { MedicalMalpracticeBanner } from "./MedicalMalpracticeBanner";

export const MedicalMalPractice = () => {
  return (
    <div>
      <MedicalMalpracticeBanner />
      <MedicalMalpracticeWhy />
      <ProgramAdvantages />
      <KeyBenefitsRelatedDocs />
    </div>
  );
};