import React from "react";
import medicalmalwh1 from "assets/svg/TawuniyaServicesIcons/medicalmalwhy1.svg";
import medicalmalwh2 from "assets/svg/TawuniyaServicesIcons/medicalmalwhy2.svg";
import medicalmalwh3 from "assets/svg/TawuniyaServicesIcons/medicalmalwhy3.svg";
import medicalmalwh4 from "assets/svg/TawuniyaServicesIcons/medicalmalwhy4.svg";
import medicalmalwh5 from "assets/svg/TawuniyaServicesIcons/medicalmalwhy5.svg";
import { AdvantageProgramCard } from "component/Products/CommonProductsComponents/AdvantageProgramCard";

const programCardHeaderData = {
	title: "Person Eligible for this Program",
	para: "This program has been duly approved by the Shariah Committee for rendering the insurance cover available amongst 4 options of compensation limits required pursuant to your needs. You can also obtain one insurance cover for up to 5 years with no intermittence.",
};

const medicalMalpracticedvantageCardData = [
	{
		id: 0,
		icon: medicalmalwh1,
		title: "Doctors of Other Medical Categories",
		para: "Health insurance will not only give your employees and their families enough financial security, but an overall sense of satisfaction that their employer actually cares about them.",
	},
	{
		id: 1,
		icon: medicalmalwh2,
		title: "Gynecology & Anesthesia Specialists",
		para: "Happy employees make happy workspaces and evidently successful companies! It’s no surprise that the safer and satisfied employees feel happier and motivated!",
	},
	{
		id: 2,
		icon: medicalmalwh3,
		title: "Pharmacists",
		para: "Health insurance will not only safeguard the employees' savings but, also enhance their overall mental well-being with the right support. ",
	},
	{
		id: 3,
		icon: medicalmalwh4,
		title: "Nurses & Technicians",
		para: "Safeguard your employees from the same, amongst other diseases; the earlier these issues are diagnosed, the earlier they can be treated and resolved. ",
	},
	{
		id: 4,
		icon: medicalmalwh5,
		title: "Paramedics & Technicians",
		para: "With more than 6400+ cashless hospitals across KSA, your employees can be covered at ease no matter where they are!",
	},
];
export const MedicalMalpracticeWhy = () => {
	return (
		<div className="col-lg-12 col-12">
			<AdvantageProgramCard
				isTransparent={true}
				classDivision="col px-2"
				headerData={programCardHeaderData}
				advantageCardData={medicalMalpracticedvantageCardData}
				tranparentadvantagetitle="tranparent-advantage-title"
				tranparentadvantagepara="tranparent-advantage-para fs-15"
			/>
		</div>
	);
};
