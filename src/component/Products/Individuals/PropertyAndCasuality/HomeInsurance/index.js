import React from "react";
import { HomeInsuranceBanner } from "./HomeInsuranceBanner";
import { HomeWhyInsurance } from "./HomeWhyInsurance";

export const HomeInsurance = () => {
	return (
		<div>
			<HomeInsuranceBanner />
			<HomeWhyInsurance />
		</div>
	);
};
