import React from "react";
import BannerImage from "assets/images/home-insurance-banner.jpeg";
import { ProductsBanner } from "component/Products/CommonProductsComponents/ProductsBanner";
import { AdvantageProgramCard } from "component/Products/CommonProductsComponents/AdvantageProgramCard";
import { TwuniyaAppAdvert } from "component/common/TwuniyaAppAdvert";
import { Faq } from "component/common/FraudDetails";
import {
	faqTravelData,
	faqContainerTravelData,
} from "component/common/MockData/index";
import fire from "assets/svg/TawuniyaServicesIcons/fireAccident.svg";
import explosion from "assets/svg/TawuniyaServicesIcons/explosion.svg";
import storm from "assets/svg/TawuniyaServicesIcons/storms.svg";
import flood from "assets/svg/TawuniyaServicesIcons/floods.svg";
import robbery from "assets/svg/TawuniyaServicesIcons/robbery.svg";
import earthquake from "assets/svg/TawuniyaServicesIcons/earthquake.svg";
import lock from "assets/svg/TawuniyaServicesIcons/uncertainity.svg";
import social from "assets/svg/TawuniyaServicesIcons/socialSecurity.svg";
import calamity from "assets/svg/TawuniyaServicesIcons/calamities.svg";
import "./style.scss";

export const HomeInsuranceBanner = () => {
	const homeInsurancedvantageCardData = [
		{
			id: 0,
			icon: fire,
			title: "Fires",
			para: "Fires are dreadful and can lead to plenty of damage and losses. Our home insurance policy will be there for you",
		},
		{
			id: 1,
			icon: explosion,
			title: "Fires",
			para: "Damages and losses caused by explosions or even aircraft damage will be covered for too in our home insurance",
		},
		{
			id: 2,
			icon: storm,
			title: "Storms",
			para: "Covers your home and your belongings from dire storms that could lead to potential damages and losses",
		},
		{
			id: 3,
			icon: flood,
			title: "Floods",
			para: "Protects your home from losses and damages when the rains are out of control, leading to floods that impact your home and your belongings",
		},
		{
			id: 4,
			icon: robbery,
			title: "Burglaries",
			para: "Your home insurance will help protect both your home and its contents from any losses that occur due to the same",
		},
		{
			id: 5,
			icon: earthquake,
			title: "Earthquakes",
			para: "Nobody can avoid nature’s furies but, what you can do is ensure you’re covered for any potential losses that could come your way due to the same",
		},
	];

	const programCardHeaderData = {
		title: "What’s Covered in Home Insurance by Tawuniya?",
		para: "The maximum limit of insurance cover is determined on the basis of the house insured value and that of its contents based on reasonable estimates, in addition to the benefit of third party liability, liability to domestic employees, and the value of additional expenses to find alternative accommodation in the event of damage to the insured dwelling. The compensation is made on the basis of repair, replacement, or monetary compensation.",
	};

	const protectHomeAdvantageCardData = [
		{
			id: 0,
			icon: lock,
			title: "Secured through Uncertainty",
			para: "Home burglaries can happen anywhere, anytime- even in the most secure places. Home insurance helps you recover from losses that could occur due to such uncertain and unforeseen circumstances.",
		},
		{
			id: 1,
			icon: social,
			title: "Complete Financial & Social Security",
			para: "Contrary to popular belief, home insurance goes beyond just the physical property of your home. It covers everything right from your garage to your home’s contents. There is no one to watch your home when you’re out during the day, at work, or traveling. That’s why having home insurance helps your home stay secure when you’re away.",
		},
		{
			id: 2,
			icon: calamity,
			title: "Protection from Natural Calamities",
			para: "Floods, storms, and the like can be a homeowner’s worst nightmare! And going through the hassle of rebuilding your home and recovering from the damages can not only be stressful but also a loss of a lot of money! Luckily, having home insurance can cover you for it all!",
		},
	];

	const protectHomeCardHeaderData = {
		title:
			"What are the advantages of protecting your home with a Home Insurance?",
		para: "The Saudi insurance pioneer, will help you to identify, analyze and manage such risks and suggest appropriate insurance solutions.",
	};

	return (
		<div className="row home-landing-insurance-layout">
			<div>
				<ProductsBanner
					bannerDetails={{
						bannerImage: BannerImage,
						topTextContent: {
							title: "You have a fancy home! Glad you’re protecting it",
							subtitle: "Home Insurance Program",
							description1:
								"Home insurance is a property insurance policy to help you cover your home, and your personal belongings within, from unforeseen circumstances such as burglaries, fires, floods, storms, and explosions. ",
							description2:
								"Buying a home is one of the most vital investments people spend all their life working towards. Yet, there are so many who forget to secure and protect this very important investment in their life.",
						},
						bannerText: {
							title: "Home Insurance",
							description:
								"The Saudi insurance pioneer, will help you to identify, analyze and manage such risks and suggest appropriate insurance solutions.",
							note: "By continuing you give Tawuniya advance consent to obtain my and/or my dependents' information from the National Information Center.",
						},
					}}
				/>
			</div>
			<div className="col-lg-12 col-12">
				<AdvantageProgramCard
					isTransparent={true}
					classDivision="col-2 px-2"
					headerData={programCardHeaderData}
					advantageCardData={homeInsurancedvantageCardData}
					tranparentadvantagetitle="tranparent-advantage-title"
					tranparentadvantagepara="tranparent-advantage-para fs-15"
				/>
			</div>
			<div className="col-lg-12 col-12">
				<AdvantageProgramCard
					classDivision="col-4"
					headerData={protectHomeCardHeaderData}
					advantageCardData={protectHomeAdvantageCardData}
					lightAdvantageHeaderClass="protect-HomeCard-Header fs-17"
					lightAdvantageParaClass="protect-HomeCard-Para fs-13 pb-1"
				/>
			</div>
			<div className="col-lg-12 col-12 pt-5">
				<TwuniyaAppAdvert />
			</div>
			<div className="col-lg-12 col-12 pb-4">
				<Faq
					title="You’ve got questions, we’ve got answers"
					faqHeaderClass="fs-26 fw-800 homeInsure-faqHeader text-center m-0"
					faqParaClass="fs-16 fw-400 homeInsure-faqPara text-center pb-3"
					description="Review answers to commonly asked questions at Tawuniya, which enable you to be directly involved in improving our support experience."
					faqData={faqTravelData}
					faqContainerData={faqContainerTravelData}
					viewAll={true}
				/>
			</div>
		</div>
	);
};
