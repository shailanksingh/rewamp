import React from "react";
import { WhyInsuranceCard } from "component/Products/CommonProductsComponents/WhyInsuranceCard";
import { MakeClaim } from "component/Products/CommonProductsComponents/MakeClaim";
import makeClaimBanner from "assets/svg/TawuniyaServicesIcons/makeClaimBanner.svg";
import { MotorOtherProducts } from "component/MotorPage/MotorInsuranceComponent/MotorComponennts/MotorOtherProducts/index";
import "./style.scss";

export const HomeWhyInsurance = () => {
	const claimCardData = {
		banner: makeClaimBanner,
		title: "How do I make a claim?",
		para: "Tawuniya believes in simplification and automation wherever possible. Our approach to the claims journey for Tawuniya is no different. We want you to notify us of claims as soon as possible and we want to get your claim moving as soon as possible. Our simple steps approach helps us to keep that focus throughout the claim process.",
		buttonLabel: "Claim Assistance",
	};

	return (
		<div>
			<MakeClaim claimCardData={claimCardData} />
			<MotorOtherProducts isSpace="pb-5" />		
		</div>
	);
};
