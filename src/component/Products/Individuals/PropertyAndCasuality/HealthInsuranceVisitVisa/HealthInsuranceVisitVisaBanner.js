import React from "react";
import BannerImage from "assets/images/health-insurance-visit-visa.jpeg";
import { ProductsBanner } from "component/Products/CommonProductsComponents/ProductsBanner";
import "./style.scss";
import SubmitTrackIcon from "assets/svg/submit-track-claim.svg";
import RequestIcon from "assets/svg/approval-request-tracking.svg";
import FileIcon from "assets/svg/file-reimbursement.svg";
import MemberIcon from "assets/svg/health-member-card.svg";


const manageYourPolicy = [
	{
		icon: SubmitTrackIcon,
		text: "Submit/Track Claim",
	},
	{
		icon: RequestIcon,
		text: "Approval request tracking ",
	},
	{
		icon: FileIcon,
		text: "File a reimbursement",
	},
	{
		icon: MemberIcon,
		text: "Download Health member card",
	},
];

export const HealthInsuranceVisitVisaBanner = () => {
	return (
		<div className="row">
			<div className="col-12">
				<ProductsBanner
					bannerDetails={{
						bannerImage: BannerImage,
						topTextContent: {
							title: "Travel to Saudi Arbia at ease!",
							subtitle: "Visitors Insurance Program",
							description1:
								"This insurance product is specifically designed to suit the needs of visitors to the KSA",
							description2:
								"Insurnace cover details, costs and more..",
						},
						bannerText: {
							title: "Manage your insurance within minutes!",
							description:
								"The Saudi insurance pioneer, will help you to identify, analyze and manage such risks and suggest appropriate insurance solutions.",
						},
					}}
					manageYourPolicy={manageYourPolicy}
				/>
			</div>
		</div>
	);
};
