import React from "react";
import { ProductsBanner } from "component/Products/CommonProductsComponents/ProductsBanner";
import BannerImage from "assets/images/health-insurance-my-family-banner.jpeg";
import "./style.scss";

export const HealthInsuranceMyFamilyBanner = () => {

  return (
    <div className="row ">
      <div className="col-lg-12 col-12">
        <ProductsBanner
          bannerDetails={{
            bannerImage: BannerImage,
            topTextContent: {
              title: "Prepare Everything From Now On To Live More Calmly ",
              subtitle: '"My Family" Medical Insurance Program',
              description1:
                "This is an insurance program that provides adequate healthcare for all family members, in compliant with the provisions of Islamic Sharia. The program consists of four categories commensurate with the various members of the community.",
              description2: "My Family program covers most of the medical services provided in the outpatient as well as inpatient services, including the expenses of medical examination, diagnosis, medical treatment, medicines, vaccination, pre existing diseases, pregnancy and maternity at the largest approved medical service providers network KSA.",
            },
            bannerText: {
              title: "Insure your family within minutes!",
              description:
                "The Saudi insurance pioneer, will help you to identify, analyze and manage such risks and suggest appropriate insurance solutions.",
            },
          }}

        />
      </div>
    </div>
  );
};
