import React, { useState } from "react";
import FormValidation from "service/helpers/FormValidation";
import PhoneNumberInput from "component/common/PhoneNumberInput/index";
import KSAFlagImage from "assets/images/ksaFlag.png";
import IndiaFlagImage from "assets/images/indiaFlag.png";
import Profile from "assets/svg/user-icon-grey.svg";
import EmailIcon from "assets/svg/email-plus.svg";
import ErrorComponent from "component/common/ErrorComponent";
import { NormalSearch } from "component/common/NormalSearch";

const errorMessages = {
    firstName: { required: "First Name field is required" },
    lastName: { required: "Last Name field is required" },
    email: {  
        required: "Email field is required",
        emailError: "The Email you entered is not valid",
    },
    phoneNumber: { 
        required: "Phone Number field is required",
        phone: "Phone Number you entered is not valid", 
    },
    message: { required: "Message field is required" },
};
let dialingCodes = [
    {
      code: "+966",
      image: KSAFlagImage,
    },
    {
      code: "+91",
      image: IndiaFlagImage,
    },
];

export const EnquiryForm = () => {
    const [formInput, setFormInput] = useState({
        firstName: "",
        lastName: "",
        email: "",
        phoneNumber: "",
        message: "",
    });
    let [selectedCode, setSelectedCode] = useState(dialingCodes[0]);
    const [formErrors, setFormErrors] = useState(null);
    const handleFormInputChange = (e) => {
        const { name, value } = e.target || e || {};
        setFormInput({ ...formInput, [name]: value });
    };
    const formSubmit = () => {
        let errors = FormValidation(errorMessages, formInput, formErrors);
        setFormErrors(null);
        if (!errors) {
            console.log("validated")
        } else {
            setFormErrors({ ...errors });
        }
    }
    return (
        <div className="enquiry-form-root">
            <div className="form-title">Whenever you need us, we are here for you, and your insurance needs</div>
            <div className="forms-list-root">
                <div className="forms-list">
                    <div className="input-root width-50">
                        <NormalSearch
                            className="formInputFieldOne"
                            name="firstName"
                            value={formInput.firstName}
                            placeholder="First Name"
                            onChange={handleFormInputChange}
                            needLeftIcon={true}
                            leftIcon={Profile}
                            errorMessage={formErrors?.firstName ? formErrors?.firstName?.required : null}
                        />
                    </div>
                    <div className="input-root width-50">
                        <NormalSearch
                            className="formInputFieldOne"
                            name="lastName"
                            value={formInput.lastName}
                            placeholder="Last Name"
                            onChange={handleFormInputChange}
                            needLeftIcon={true}
                            leftIcon={Profile}
                            errorMessage={formErrors?.lastName ? formErrors?.lastName?.required : null}
                        />
                    </div>
                    <div className="bordered"></div>
                    <div className="input-root">
                        <NormalSearch
                            className="formInputFieldOne"
                            name="email"
                            value={formInput.email}
                            placeholder="Enter Your Email"
                            onChange={handleFormInputChange}
                            needLeftIcon={true}
                            leftIcon={EmailIcon}
                            errorMessage={formErrors?.email ? (formErrors?.email?.emailError || formErrors?.email?.required) : null}
                        />
                    </div>
                    <div className="input-root">
                        <PhoneNumberInput
                            className="formPhoneInput"
                            selectInputClass="formSelectInputWidth"
                            selectInputFlexType="formSelectFlexType-medical"
                            dialingCodes={dialingCodes}
                            selectedCode={selectedCode}
                            setSelectedCode={setSelectedCode}
                            value={formInput.phoneNumber}
                            name="phoneNumber"
                            onChange={handleFormInputChange}
                            errorMessage={formErrors?.phoneNumber ? (formErrors?.phoneNumber?.phone || formErrors?.phoneNumber?.required) : null}
                        />
                    </div>
                    <div className="bordered"></div>
                    <div className="input-root">
                        <label>
                            Message, Notes..
                        </label>
                        <textarea className="complaintForm-textArea"
                            placeholder="Type your message here.."
                            name="message"
                            value={formInput?.message}
                            onChange={handleFormInputChange}
                        >
                            Type here...
                        </textarea>
                        {formErrors?.message && <ErrorComponent message={formErrors?.message?.required} />}
                    </div>
                </div>
            </div>
            <button onClick={() => formSubmit()}>Submit</button>
        </div>
    );
}