import React from "react";
import { HealthInsuranceMyFamilyBanner } from "./HealthInsuranceMyFamilyBanner";
import { ProgramTabs } from "./ProgramTabs";
import { EnquiryForm } from "./EnquiryForm";
import { MotorOtherProducts } from "component/MotorPage/MotorInsuranceComponent/MotorComponennts/MotorOtherProducts/index";
import { TwuniyaAppAdvert } from "component/common/TwuniyaAppAdvert";
import { Faq } from "component/common/FraudDetails";
import {
	faqTravelData,
	faqContainerTravelData,
  } from "component/common/MockData/index";

export const HealthInsuranceMyFamily = () => {
	return (
		<div className="health-insurance-my-family-root">
			<HealthInsuranceMyFamilyBanner />
			<ProgramTabs />
			<Faq
				title="You’ve got questions, we’ve got answers"
				faqHeaderClass="fs-26 fw-800 medicalMalpractice-faqHeader text-center m-0"
				faqParaClass="fs-16 fw-400 medicalMalpractice-faqPara text-center pb-3"
				description="Review answers to commonly asked questions at Tawuniya, which enable you to be directly involved in improving our support experience."
				faqData={faqTravelData}
				faqContainerData={faqContainerTravelData}
				viewAll={true}
			/>
			<TwuniyaAppAdvert />
			<MotorOtherProducts isSpace="pb-5" />
			<EnquiryForm />
		</div>
	);
};
