import React from "react";
import { HomeServiceCard } from "component/HomePage/LandingComponent";
import { MotorServicesData } from "../../../../MotorPage/MotorInsuranceComponent/schema/MotorServicesData";
import { MakeClaim } from "component/Products/CommonProductsComponents/MakeClaim";
import { ProgramTermsBanner } from "component/Products/CommonProductsComponents/ProgramTermsBanner";
import { AdvantageProgramCard } from "component/Products/CommonProductsComponents/AdvantageProgramCard";
import { ProductsBanner } from "component/Products/CommonProductsComponents/ProductsBanner";
import BannerImage from "assets/images/international-travel-banner.jpeg";
import peace from "assets/svg/TawuniyaServicesIcons/peace.svg";
import discount from "assets/svg/TawuniyaServicesIcons/discountprice.svg";
import cover from "assets/svg/TawuniyaServicesIcons/coverincident.svg";
import sixtyFive from "assets/svg/TawuniyaServicesIcons/65yrold.svg";
import childCoverage from "assets/svg/TawuniyaServicesIcons/childrencoverage.svg";
import makeClaimBanner from "assets/svg/TawuniyaServicesIcons/makeClaimBanner.svg";
import "./style.scss";

export const InternationalTravelBanner = () => {
  const travelCardHeaderData = {
    title: "Advantages Of The Program",
    para: "The Saudi insurance pioneer, will help you to identify, analyze and manage such risks and suggest appropriate insurance solutions.",
  };

  const travelInsurancedvantageCardData = [
    {
      id: 0,
      icon: peace,
      title: "Peace Of Mind",
      para: "The program provides the passenger with reassurance and peace of mind to enjoy the trip and offers comprehensive protection.",
    },
    {
      id: 1,
      icon: discount,
      title: "Discounted Prices",
      para: "Insurance cover for young people is available at discounted prices.",
    },
    {
      id: 2,
      icon: cover,
      title: "Covers Incidents",
      para: "It covers incidents of domestic flights if the flight is a part of an international flight covered by insurance.",
    },
    {
      id: 3,
      icon: sixtyFive,
      title: "Up to 65Yr",
      para: "Family cover is only available for persons under the age of 65 years.",
    },
    {
      id: 4,
      icon: childCoverage,
      title: "Children Coverage",
      para: "Provides free coverage for children under the age of two years when they travel accompanied by an adult.",
    },
  ];

  const travelProgramTermsBannerData = {
    title: "Program Terms",
    data: [
      {
        id: 0,
        heading: "International Trip",
        txt: "This insurance is not valid for trips taken within Saudi Arabia unless these form part of an international trip outsiden Saudi Arabia and do not exceed 24 hours in each case.",
      },
      {
        id: 1,
        heading: "Police Report",
        txt: "If your money, valuables or any items of personal baggage are lost or stolen, you must notify the local police and make sure you obtain a copy of the police report complete the claim procedures.",
      },
      {
        id: 2,
        heading: "Personal Accident",
        txt: "Schengen cover under the program is only limited to personal accident and emergency medical expenses up to SR 200,000.",
      },
      {
        id: 3,
        heading: "SR 200 Per Claim",
        txt: "The program cover is subject to an excess (deductible) up to SR 200 per claim.",
      },
      {
        id: 4,
        heading: "Commencing",
        txt: "This policy is only valid for trips commencing in and returning to Saudi Arabia.",
        alignCard: "align-commenceCard",
      },
      {
        id: 5,
        heading: "KSA Resident",
        txt: "This program is only available to persons resident in Saudi Arabia.",
      },
      {
        id: 6,
        heading: "Winter Sports",
        txt: "Winter sports are only available to persons under the age of 65 years.",
        alignCard: "align-WinterCard",
      },
      {
        id: 7,
        heading: "Over 65Yr",
        txt: "Basic cover is available for people aged over 65 years with double contribution.",
        alignCard: "align-OldCard",
      },
    ],
  };

  const claimCardData = {
    banner: makeClaimBanner,
    title: "How do I make a claim?",
    para: "Tawuniya believes in simplification and automation wherever possible. Our approach to the claims journey for Tawuniya is no different. We want you to notify us of claims as soon as possible and we want to get your claim moving as soon as possible. Our simple steps approach helps us to keep that focus throughout the claim process.",
    buttonLabel: "Claim Assistance",
  };

  return (
    <div className="row travelInsurance-layout-container">
      <div className="col-lg-12 col-12">
        <ProductsBanner
          bannerDetails={{
            bannerImage: BannerImage,
            topTextContent: {
              title: "Health Insurance That Makes You Trip Better",
              subtitle: "International Travel Insurance Program",
              description1:
                "A program duly approved by the Shraiah Committee for the provision of insurance cover for travelers on international flights in respect of the risks related to travel outside Saudi Arabia.",
              description2: "Insurnace cover details, costs and more..",
            },
            bannerText: {
              title: "Insure your trip within minutes!",
              description:
                "The Saudi insurance pioneer, will help you to identify, analyze and manage such risks and suggest appropriate insurance solutions.",
              note: "By continuing you give Tawuniya advance consent to obtain my and/or my dependents' information from the National Information Center.",
            },
          }}
        />
      </div>
      <div className="col-lg-12 col-12 px-4 pb-5">
        <AdvantageProgramCard
          isTransparent={true}
          isOddDivision={true}
          classDivision="col px-2"
          headerData={travelCardHeaderData}
          advantageCardData={travelInsurancedvantageCardData}
          tranparentadvantagetitle="travel-advantage-title"
          tranparentadvantagepara="travel-advantage-para fs-14 pr-2"
        />
      </div>
      <div className="col-lg-12 col-12">
        <HomeServiceCard HomeServicesData={MotorServicesData} pillNumber={0} />
      </div>
      <div className="col-lg-12 col-12 pt-5">
        <ProgramTermsBanner
          programTermsBannerData={travelProgramTermsBannerData}
        />
      </div>
      <div className="col-lg-12 col-12 pt-5">
        <MakeClaim claimCardData={claimCardData} />
      </div>
    </div>
  );
};
