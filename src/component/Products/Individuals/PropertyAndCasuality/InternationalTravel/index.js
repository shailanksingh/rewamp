import React from "react";
import { InternationalTravelBanner } from "./InternationalTravelBanner";
import { TawuniyaTravelProducts } from "./TawuniyaTravelProducts";

export const InternationalTravel = () => {
	return (
		<div>
			<InternationalTravelBanner />
			<TawuniyaTravelProducts />
		</div>
	);
};
