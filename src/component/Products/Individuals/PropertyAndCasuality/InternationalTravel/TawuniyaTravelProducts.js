import React from "react";
import { Faq } from "component/common/FraudDetails";
import {
  faqTravelData,
  faqContainerTravelData,
} from "component/common/MockData/index";
import { TwuniyaAppAdvert } from "component/common/TwuniyaAppAdvert";
import { MotorOtherProducts } from "component/MotorPage/MotorInsuranceComponent/MotorComponennts/MotorOtherProducts/index";
import "./style.scss";

export const TawuniyaTravelProducts = () => {
  return (
    <div className="row tawuniyaTravel-layout-container">
      <div className="col-lg-12 col-12 py-5">
        <Faq
          title="You’ve got questions, we’ve got answers"
          faqHeaderClass="fs-26 fw-800 travelInsure-faqHeader text-center m-0"
          faqParaClass="fs-16 fw-400 travelInsure-faqPara text-center pb-3"
          description="Review answers to commonly asked questions at Tawuniya, which enable you to be directly involved in improving our support experience."
          faqData={faqTravelData}
          faqContainerData={faqContainerTravelData}
          viewAll={true}
        />
      </div>
      <div className="col-lg-12 col-12 pt-3">
        <TwuniyaAppAdvert />
      </div>
      <div className="col-lg-12 col-12">
        <MotorOtherProducts isSpace="pb-5" />
      </div>
    </div>
  );
};
