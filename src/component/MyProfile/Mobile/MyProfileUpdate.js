import React, { useState } from "react";
import "./style.scss";
import KSA from "assets/images/mobile/KSA.png";
import downarrow from "assets/about/downarrow.svg";
import HeaderBackNav from "component/common/MobileReuseable/HeaderBackNav";
import emailplus from "assets/about/emailplus.png";
import { history } from "service/helpers";
import { NormalSearch } from "component/common/NormalSearch";
import BottomPopup from "component/common/MobileReuseable/BottomPopup";
import UpdateVerification from "./UpdateVerification";
import PhoneNumberInput from "component/common/PhoneNumberInput";
import { dialingCodes } from "component/common/MockData";

const MyProfileUpdateMobile = () => {
  const [updateProfileData, setUpdateProfileData] = useState({
    mobile: "",
    email: "",
  });
  const [isverficationModel, setIsverficationModel] = useState(false);
  let [selectedCode, setSelectedCode] = useState(dialingCodes[0]);
  const paramsData = new URLSearchParams(history.location.search);
  let isUpdateData = paramsData.get("isMobile") === "true" && "mobile";
  if (!isUpdateData) {
    isUpdateData = paramsData.get("isEmail") === "true" && "email";
  }
  const handleInputChange = (data) => {
    const {
      target: { name, value },
    } = data;
    setUpdateProfileData({ [name]: value });
  };
  const { email, mobile } = updateProfileData;
  return (
    <div>
      <HeaderBackNav
        pageName="My Profile"
        title={
          isUpdateData === "mobile" ? "Update Mobile Number" : "Update Email"
        }
      />
      <div className="my_profile_update">
        <h5>
          {isUpdateData === "mobile" ? "Update Mobile Number" : "Update Email"}
        </h5>
        <p>
          {isUpdateData === "mobile"
            ? "a verification code will be sent to the new number."
            : " "}
        </p>
        <div className="">
          {isUpdateData === "mobile" ? (
            <div className="country_code">
              <PhoneNumberInput
                dialingCodes={dialingCodes}
                selectedCode={selectedCode}
                setSelectedCode={setSelectedCode}
                value={mobile}
                name="mobile"
                onChange={handleInputChange}
              />
            </div>
          ) : (
            <div className="my_email">
              <NormalSearch
                className="loginNewInputFieldOne"
                name="email"
                value={email}
                placeholder="ex: email@tawuniya.com.sa"
                onChange={handleInputChange}
                needLeftIcon={true}
                leftIcon={emailplus}
              />
            </div>
          )}
        </div>
        <div
          className="orange_update_mobile"
          onClick={() => setIsverficationModel(true)}
        >
          {isUpdateData === "mobile" ? "Update Mobile Number" : "Update Email"}
        </div>
      </div>
      <BottomPopup open={isverficationModel} setOpen={setIsverficationModel}>
        <UpdateVerification />
      </BottomPopup>
    </div>
  );
};

export default MyProfileUpdateMobile;
