import React, {useState} from "react";
import PinInput from "react-pin-input";
import "./style.scss";

const UpdateVerification = () => {
  const [timer, setTimer] = useState(60);

	const [newTimer, setNewTimer] = useState(60);

	const [addNo, setAddNo] = useState(false);

	const [addNewNo, setAddNewNo] = useState(false);

	const [resend, setResend] = useState(false);
  return (
    <div className="verification_container">
      <div>
        <div className="verify">Verification</div>
        <label className="verify_code">The code has been sent to +9665*****631</label>
        <PinInput
          length={4}
          // initialValue={getOtp}
          // onChange={(value, index) => {
          //   setGetOtp(getOtp);
          //   console.log(value);
          // }}
          inputMode="numeric"
          secret
          style={{
            padding: "10px",
            display: "flex",
            justifyContent: "center",
          }}
          inputStyle={{
            border: "none",
            backgroundColor: "#F2F3F5",
            borderRadius: "4px",
            width: "100%",
            marginRight: "2%",
            minHeight: "90px",
          }}
          autoSelect={true}
          regexCriteria={/^[ A-Za-z0-9_@./#&+-]*$/}
        />
        <div className="orange_update_mobile">Verify</div>
        <label className="resend">	Resend code in 00:{" "}
									{resend ? (
										<span>
											{addNewNo && 0}
											{newTimer}
										</span>
									) : (
										<span>
											{addNo && 0}
											{timer}
										</span>
									)}</label>
        <div className="verify_dont">
        <p className="verify_receive">Don’t receive Code?</p>
        <p className="verify_resend">Resend</p>
        </div>
      </div>
    </div>
  );
};

export default UpdateVerification;
