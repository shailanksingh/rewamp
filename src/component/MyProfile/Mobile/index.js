import React from "react";
import "./style.scss";
import aproval from "assets/about/aproval.png";
import claims from "assets/about/claims.png";
import Default from "assets/about/Default.png";
import phone from "assets/about/phone.png";
import mail from "assets/about/mail.png";
import policy from "assets/about/policy.png";
import ticket from "assets/about/ticket.png";
import rightarrow from "assets/about/rightarrow.png";
import HeaderStickyMenu from "component/common/MobileReuseable/HeaderStickyMenu";
import { history } from "service/helpers";

const MyProfileMobile = () => {
  const updateProfile = (data) => {
    history.push(`/home/myprofile/update?${data + "=true"}`);
  };
  return (
    <div className="my_profile_container">
      <HeaderStickyMenu />
      <div className="name_container">
        <div className="my_profile_header">AA</div>
      </div>
      <div className="my_profile_name">Abdulla Ali</div>
      <div className="my_profile_number">1064190364</div>
      <div className="my_profile_body">
        <div className="my_profile_your">Your Dashboard</div>
        <div className="d-flex">
          <div className="my_profile_image">
            <img src={policy}></img>
          </div>
          <div className="my_profile_data">Your Policies</div>
        </div>
        <div className="d-flex">
          <div className="my_profile_image">
            <img src={claims}></img>
          </div>
          <div className="my_profile_data">Your Claims </div>
          <div class="div1">2</div>
        </div>
        <div className="d-flex">
          <div className="my_profile_image">
            <img src={aproval}></img>
          </div>
          <div className="my_profile_data">Your Approvals </div>
        </div>
        <div className="d-flex">
          <div className="my_profile_image">
            <img src={ticket}></img>
          </div>
          <div className="my_profile_data">Support Tickets</div>
        </div>
      </div>
      <div className="my_profile_content">
        <div className="my_profile_contact">Contact Information</div>
        <div className="d-flex">
          <div className="my_profile_content_image">
            <img src={phone}></img>
          </div>
          <div className="d-flex justify-content-between align-items-center"> 
          
            <div>
              <div className="my_profile_content_data">Mobile Number</div>
              <div className="my_profile_content_text">966503195993</div>
            </div>
            <div className="arrow1" onClick={() => updateProfile("isMobile")}>
              <img src={rightarrow}></img>
            </div>
          </div>
        </div>
        <hr className="line"></hr>
        <div className="d-flex">
          <div className="my_profile_content_image">
            <img src={mail}></img>
          </div>
          <div className="d-flex justify-content-between align-items-center">
            <div>
              <div className="my_profile_content_data">Email</div>
              <div className="my_profile_content_text">
                prashant@altawuniya.com.sa
              </div>
            </div>

            <div className="arrow2" onClick={() => updateProfile("isEmail")}>
              <img src={rightarrow}></img>
            </div>
          </div>
        </div>
      </div>
      <div className="my_profile_Topic">Language</div>
      <div className="my_profile_Topic_content">
        Choose the language you'd like to use
      </div>
      <div className="my_profile_english">
        <div className="my_profile_content_lang">English</div>
        <div className="my_profile_english_circle"></div>
      </div>
      <div className="my_profile_language">
        <div className="my_profile_language_image">
          <img src={Default}></img>
        </div>
        <div className="my_profile_circle"></div>
      </div>
      <div className="my_profile_notification">Notifications</div>
      <div className="my_profile_strong">
        We strongly recommend enabling notifications so that you'll know when
        important activity happens in your Tawuniya account.
      </div>
      <div className="my_profile_box">
        <div className="my_profile_switch">
          <label class="switch">
            <input type="checkbox"></input>
            <span class="slider round"></span>
          </label>
        </div>
        {/* <div className="my_profile_box_image"><img src={Switch}></img></div> */}
        <div className="my_profile_box_data">Push Notifications</div>
      </div>
      <div className="my_profile_notification">Privacy</div>
      <div className="my_profile_box">
        <div className="my_profile_switch">
          <label class="switch">
            <input type="checkbox"></input>
            <span class="slider round"></span>
          </label>
        </div>
        {/* <div className="my_profile_box_image"><img src={Switch}></img></div> */}
        <div className="my_profile_box_data">
          Allow Tawuniya to use Fingerprint or Face ID
        </div>
      </div>
      <div className=""></div>
      <div className="my_profile_button">Save</div>
      <div className="bottom"></div>
    </div>
  );
};

export default MyProfileMobile;
