import React from "react";
import MediaCenterImgCard1 from "assets/images/MediaCenterImgCard1.png";
import "./style.scss";

export const InvestorWelcome = () => {
	return (
		<React.Fragment>
			<div
				className="welcome-tab"
				// style={{
				// 	backgroundColor:
				// 		"linear-gradient(0deg,rgba(0, 0, 0, 0.6),rgba(0, 0, 0, 0.6)) !important",
				// 	backgroundImage: `url(${MediaCenterImgCard1})`,
				// 	backgroundRepeat: "no-repeat",
				// 	backgroundSize: "cover",
				// 	// backgroundRepeat: "no-repeat",
				// 	// backgroundSize: "cover",
				// 	// background:
				// 	// 	"linear-gradient(0deg, rgba(0, 0, 0, 0.6), rgba(0, 0, 0, 0.6)), url(" +
				// 	// 	MediaCenterImgCard1 +
				// 	// 	") no-repeat  right top cover",
				// }}
			>
				<div className="welcome-tab-text">
					<div className="welcome-tab-title">Tawuniya</div>
					<div className="welcome-tab-title2">
						<span>Investor</span> overview
					</div>
					<div className="welcome-tab-subtitle">
						Summary of key financials and investor information.
					</div>
					<div className="welcome-tab-para">
						<p>
							The Company for Cooperative Insurance (Tawuniya) is a Saudi Joint
							Stock Company, and it was incorporated on January 18, 1986 under
							the Commercial Registration No. 1010061695. Tawuniya is the first
							national insurance company licensed in the Kingdom of Saudi Arabia
							to practice all types of insurance business in accordance with the
							cooperative insurance principle that is accepted by Islamic
							Sharyia.
						</p>
						<p>
							The authorized, issued, and paid-up capital of the Company is SR
							1,250 million. Tawuniya has obtained the license number ت م ن /
							1/200412 from the Saudi Central Bank (SAMA), as the first license
							in the insurance sector under the Cooperative Insurance Companies
							Control Law. The company is also regulated and supervised by SAMA.
						</p>
					</div>
				</div>
				<div className="welcome-tab-tickerstatic">
					<iframe
						src="https://ksatools.eurolandir.com/tools/ticker/html/?companycode=sa-8010&v=staticr2022&lang=en-gb"
						width="100%"
						height="500"
						scrolling="no"
						frameborder="0"
					></iframe>
				</div>
			</div>
			<div className="welcome-tab-financialcalender">
				<iframe
					src="https://ksatools.eurolandir.com/tools/fincalendar2/?companycode=SA-8010&v=r2022&l&lang=en-GB"
					width="100%"
					height="800px"
					scrolling="no"
					frameborder="0"
				></iframe>
			</div>
			<div className="welcome-tab-subscriptioncenter">
				<h5>Get in touch</h5>
				<iframe
					src="https://ksatools.eurolandir.com/tools/SubscriptionCentre2/?companycode=SA-8010&v=r2022&lang=en-gb"
					width="100%"
					height="643px"
					scrolling="no"
					frameborder="0"
				></iframe>
			</div>
		</React.Fragment>
	);
};
