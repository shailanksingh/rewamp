import React, { useState } from "react";
import "./style.scss";
import { InvestorWelcome } from "../Welcomepage";
import { FactSheet } from "../ResultA";
import { FinancialInformation } from "../ResultB";
import { ShareInformation } from "../ResultC";
import { Dividens } from "../ResultD";
import { AnalystCoverage } from "../ResultE";
import { Announcements } from "../ResultF";

export const InvestorRelationsMain = () => {
	const [InvestorPage, setInvestorPage] = useState(0);

	return (
		<React.Fragment>
			<div className="ir-navigationContainer">
				<div className="ir-navigation">
					<div
						className="ir-navlinks"
						onClick={() => {
							setInvestorPage(0);
						}}
					>
						Welcome
					</div>
					<div
						className="ir-navlinks"
						onClick={() => {
							setInvestorPage(1);
						}}
					>
						Fact Sheet
					</div>
					<div
						className="ir-navlinks"
						onClick={() => {
							setInvestorPage(2);
						}}
					>
						Financial Information
					</div>
					<div
						className="ir-navlinks"
						onClick={() => {
							setInvestorPage(3);
						}}
					>
						Share information
					</div>
					<div
						className="ir-navlinks"
						onClick={() => {
							setInvestorPage(4);
						}}
					>
						Dividens
					</div>
					<div
						className="ir-navlinks"
						onClick={() => {
							setInvestorPage(5);
						}}
					>
						Analyst Coverage
					</div>
					<div
						className="ir-navlinks"
						onClick={() => {
							setInvestorPage(6);
						}}
					>
						Announcements
					</div>
				</div>
			</div>
			{InvestorPage === 0 && <InvestorWelcome />}
			{InvestorPage === 1 && <FactSheet />}
			{InvestorPage === 2 && <FinancialInformation />}
			{InvestorPage === 3 && <ShareInformation />}
			{InvestorPage === 4 && <Dividens />}
			{InvestorPage === 5 && <AnalystCoverage />}
			{InvestorPage === 6 && <Announcements />}
		</React.Fragment>
	);
};
