import React, { useState } from "react";
import HeaderStickyMenu from "component/common/MobileReuseable/HeaderStickyMenu";
import HeaderStepsSticky from "component/common/MobileReuseable/HeaderStepsSticky";
import "./style.scss";
import InvestIntroduction from "./Introduction";
import { FinancialInformation } from "../ResultB";
import { ShareInformation } from "../ResultC";
import { Dividens } from "../ResultD";
import { AnalystCoverage } from "../ResultE";
import { Announcements } from "../ResultF";
import { FactSheet } from "../ResultA";

const InvestorRelationsMobile = () => {
  const investorMenuList = [
    {
      id: 1,
      label: "Introduction",
    },
    {
      id: 2,
      label: "Fact Sheet",
    },
    {
      id: 3,
      label: "Financial Information",
    },
    {
      id: 4,
      label: "Share Graph",
    },
    {
      id: 5,
      label: "Dividens",
    },
    {
      id: 6,
      label: "Analyst Coverage",
    },
    {
      id: 7,
      label: "Announcements",
    },
  ];
  const [activeMenuOption, setActiveMenuOption] = useState(investorMenuList[0]);
  const selectedMenuOption = (data) => {
    setActiveMenuOption(data);
  };
  return (
    <div className="investors_relation_container">
      <HeaderStickyMenu />
      <HeaderStepsSticky
        title={"Investor Relations"}
        isSelect={true}
        selectTitle={activeMenuOption}
        menuList={investorMenuList}
        selectedMenuOption={(data) => selectedMenuOption(data)}
      />
      <div className="content_section">
        {activeMenuOption.id === 1 && <InvestIntroduction />}
        {activeMenuOption.id === 2 && <FactSheet mobileView={true} />}
        {activeMenuOption.id === 3 && (
          <FinancialInformation mobileView={true} />
        )}
        {activeMenuOption.id === 4 && <ShareInformation mobileView={true} />}
        {activeMenuOption.id === 5 && <Dividens mobileView={true} />}
        {activeMenuOption.id === 6 && <AnalystCoverage mobileView={true} />}
        {activeMenuOption.id === 7 && <Announcements mobileView={true} />}
      </div>
    </div>
  );
};

export default InvestorRelationsMobile;
