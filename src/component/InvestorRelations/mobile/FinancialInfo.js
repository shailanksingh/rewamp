import React from "react";

const FinancialInfo = () => {
  return (
    <div className="">
      <iframe
        src="https://tools.euroland.com/factsheet/SA-8010_r2022/factsheethtml.asp?lang=english"
        width="100%"
        height="2481.62px"
        scrolling="no"
        frameborder="0"
        title="Fact sheet"
      />
    </div>
  );
};

export default FinancialInfo;
