import React from "react";
import invest_relation_intro from "assets/images/mobile/invest_relation_intro.svg";

const InvestIntroduction = () => {
  return (
    <>
      <div className="mt-2 position-relative">
        <img src={invest_relation_intro} alt="banner" className="w-100" />
        <div className="invest_intro_container">
          <div className="invest_intro_header_iframe">
            <iframe
              src="https://ksatools.eurolandir.com/tools/ticker/html/?companycode=sa-8010&v=staticr2022&lang=en-gb"
              width="100%"
              height="263px"
              scrolling="no"
              frameborder="0"
              title="Card"
            />
          </div>
        </div>
      </div>
      <div className="invest_intro_para">
        <p>
          The Company for Cooperative Insurance (Tawuniya) is a Saudi Joint
          Stock Company, and it was incorporated on January 18, 1986 under the
          Commercial Registration No. 1010061695. Tawuniya is the first national
          insurance company licensed in the Kingdom of Saudi Arabia to practice
          all types of insurance business in accordance with the cooperative
          insurance principle that is accepted by Islamic Sharyia.
        </p>
        <p>
          The authorized, issued, and paid-up capital of the Company is SR 1,250
          million. Tawuniya has obtained the license number ت م ن / 1/200412
          from the Saudi Central Bank (SAMA), as the first license in the
          insurance sector under the Cooperative Insurance Companies Control
          Law. The company is also regulated and supervised by SAMA.
        </p>
      </div>
      <div className="welcome-tab-financialcalender">
        <iframe
          src="https://ksatools.eurolandir.com/tools/fincalendar2/?companycode=SA-8010&v=r2022&l&lang=en-GB"
          width="100%"
          height="800px"
          scrolling="no"
          frameborder="0"
          title="iframe"
        />
      </div>
      <div className="welcome-tab-subscriptioncenter">
        <h5>Get in touch</h5>
        <iframe
          src="https://ksatools.eurolandir.com/tools/SubscriptionCentre2/?companycode=SA-8010&v=r2022&lang=en-gb"
          width="100%"
          height="643px"
          scrolling="no"
          frameborder="0"
          title="iframe"
        />
      </div>
    </>
  );
};

export default InvestIntroduction;
