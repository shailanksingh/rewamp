import React from "react";
import Logo1 from "assets/svg/investor/market.svg";
import Logo2 from "assets/svg/investor/symbol.svg";
import Logo3 from "assets/svg/investor/isin.svg";
import Logo4 from "assets/svg/investor/home.svg";

import "../style.scss";
export const FactSheet = () => {
	return (
		<React.Fragment>
			<div className="container">
				<div className="d-flex pb-5 ">
					<p className="fw-800 fs-28 mt-3 mr-5">Fact Sheet</p>
					<div className="row ml-2">
						<div className="col-lg-0 pt-3 mr-2">
							<div className="container">
								<img src={Logo1} />
							</div>
						</div>

						<div className="col-lg-0 mr-5">
							<p className="mainBannerPara medical-ptag pt-3">Market</p>
							<h6 className="fw-800 mt-2">Tadawul</h6>
						</div>

						<div className="col-lg-0 ">
							<div class="vertical-line ml-3"></div>
						</div>

						<div className="col-lg-0 pt-3 ">
							<div className="container">
								<img src={Logo2} />
							</div>
						</div>

						<div className="col-lg-0 mr-5">
							<p className="mainBannerPara medical-ptag pt-3">Symbol</p>
							<h6 className="fw-800 mt-2">8010</h6>
						</div>

						<div className="col-lg-0 ">
							<div class="vertical-line ml-3 "></div>
						</div>

						<div className="col-lg-0 pt-3 mr-2">
							<div className="container">
								<img src={Logo3} alt="image" />
							</div>
						</div>

						<div className="col-lg-0 mr-5">
							<p className="mainBannerPara medical-ptag pt-3">ISIN</p>
							<h6 className="fw-800">SA000A0DPSH3</h6>
						</div>

						<div className="col-lg-0 ">
							<div class="vertical-line ml-3"></div>
						</div>

						<div className="col-lg-0 pt-3">
							<div className="container">
								<img src={Logo4} />
							</div>
						</div>

						<div className="col-lg-1 mr-5">
							<p className="mainBannerPara medical-ptag pt-3">Industry</p>
							<h6 className="fw-800 mt-2">Insurance</h6>
						</div>
					</div>
				</div>
			</div>

			<div className="factsheet" id="myIframe">
				<iframe
					src="https://tools.euroland.com/factsheet/SA-8010_r2022/factsheethtml.asp?lang=english"
					width="100%"
					height="2481.62px"
					scrolling="no"
					frameborder="0"
				></iframe>
			</div>
		</React.Fragment>
	);
};
