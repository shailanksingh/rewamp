import React from "react";
import Logo1 from "assets/svg/investor/market.svg";
import Logo2 from "assets/svg/investor/symbol.svg";
import Logo3 from "assets/svg/investor/isin.svg";
import Logo4 from "assets/svg/investor/home.svg";

import "../style.scss";

export const Dividens = () => {
	return (
		<React.Fragment>
			<div>
				<p className="fw-800 fs-28 mt-3">Dividends</p>
				<div className="float-right pb-5 div_aligntop">
					<div className="row">
						<div className="col-lg-0 pt-3 mr-2">
							<div className="container">
								<img src={Logo1} />
							</div>
						</div>

						<div className="col-lg-0 mr-5">
							<p className="mainBannerPara medical-ptag pt-3">Market</p>
							<h6 className="fw-800 mt-2">Tadawul</h6>
						</div>

						<div className="col-lg-0 ">
							<div class="vertical-line ml-3"></div>
						</div>

						<div className="col-lg-0 pt-3 ">
							<div className="container">
								<img src={Logo2} />
							</div>
						</div>

						<div className="col-lg-0 mr-5">
							<p className="mainBannerPara medical-ptag pt-3">Symbol</p>
							<h6 className="fw-800 mt-2">8010</h6>
						</div>

						<div className="col-lg-0 ">
							<div class="vertical-line ml-3 "></div>
						</div>

						<div className="col-lg-0 pt-3 mr-2">
							<div className="container">
								<img src={Logo3} alt="image" />
							</div>
						</div>

						<div className="col-lg-0 mr-5">
							<p className="mainBannerPara medical-ptag pt-3">ISIN</p>
							<h6 className="fw-800">SA000A0DPSH3</h6>
						</div>

						<div className="col-lg-0 ">
							<div class="vertical-line ml-3"></div>
						</div>

						<div className="col-lg-0 pt-3">
							<div className="container">
								<img src={Logo4} />
							</div>
						</div>

						<div className="col-lg-1 mr-5">
							<p className="mainBannerPara medical-ptag pt-3">Industry</p>
							<h6 className="fw-800 mt-2">Insurance</h6>
						</div>
					</div>
				</div>
			</div>

			<div className="dividens">
				<iframe
					src="https://ksatools.eurolandir.com/tools/tsr/iframe/?companycode=SA-8010&v=r2022&lang=en-GB"
					width="100%"
					height="1007px"
					scrolling="no"
					frameborder="0"
				></iframe>
			</div>
		</React.Fragment>
	);
};
