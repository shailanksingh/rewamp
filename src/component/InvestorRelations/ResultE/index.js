import React, { useState } from "react";
import { NormalButton } from "component/common/NormalButton";
import "./style.scss";

export const AnalystCoverage = ({ mobileView }) => {
	const [ShareInfoPage, setShareInfoPage] = useState(0);

	return (
		<React.Fragment>
			<div
				className={`analystCoverage ${mobileView && "analystCoverage_mobile"}`}
			>
				<h5>Analyst Coverage</h5>
				<div className="analyst-coverage-subnav">
					<NormalButton
						onClick={() => setShareInfoPage(0)}
						cd
						className={` buyButton p-3 ${
							ShareInfoPage === 0 && mobileView && "buyButtonActive"
						}`}
						label="Analyst List and Ratings"
					/>
					<NormalButton
						onClick={() => setShareInfoPage(1)}
						className={` buyButton p-3 ${
							ShareInfoPage === 1 && mobileView && "buyButtonActive"
						}`}
						label="Recommendation Overview"
					/>
					<NormalButton
						onClick={() => setShareInfoPage(2)}
						className={` buyButton p-3 ${
							ShareInfoPage === 2 && mobileView && "buyButtonActive"
						}`}
						label="Consensus Estimates"
					/>
				</div>
			</div>
			<div className="analyst-coverage-iframe">
				{ShareInfoPage === 0 && (
					<iframe
						src="https://ksatools.eurolandir.com/tools/sharegraph/?s=2522&companycode=SA-8010&lang=en-GB"
						width="100%"
						height="1312px"
						scrolling="no"
						frameborder="0"
					></iframe>
				)}
				{ShareInfoPage === 1 && (
					<iframe
						src="https://ksatools.eurolandir.com/tools/shareseries/?companycode=SA-8010&v=r2022&lang=en-GB"
						width="100%"
						height="700px"
						scrolling="no"
						frameborder="0"
					></iframe>
				)}
				{ShareInfoPage === 2 && (
					<iframe
						src="https://ksatools.eurolandir.com/tools/splookup/?companycode=sa-8010&v=r2022&lang=en-GB"
						width="100%"
						height="756px"
						scrolling="no"
						frameborder="0"
					></iframe>
				)}
			</div>
		</React.Fragment>
	);
};
