import React, { useState } from "react";
import { NormalButton } from "component/common/NormalButton";
import "./style.scss";

export const ShareInformation = ({ mobileView }) => {
  const [ShareInfoPage, setShareInfoPage] = useState(0);

  return (
    <React.Fragment>
      <div
        className={`shareinformation ${
          mobileView && "shareinformation_mobile"
        }`}
      >
        <h5>Share Information</h5>
        <div className="share-info-subnav">
          <NormalButton
            onClick={() => setShareInfoPage(0)}
            className={` buyButton p-3 ${
              ShareInfoPage === 0 && mobileView && "buyButtonActive"
            }`}
            label="Share Graph"
          />
          <NormalButton
            onClick={() => setShareInfoPage(1)}
            className={` buyButton p-3 ${
              ShareInfoPage === 1 && mobileView && "buyButtonActive"
            }`}
            label="Share Series"
          />
          <NormalButton
            onClick={() => setShareInfoPage(2)}
            className={` buyButton p-3 ${
              ShareInfoPage === 2 && mobileView && "buyButtonActive"
            }`}
            label="Share Price Look Up"
          />
          <NormalButton
            onClick={() => setShareInfoPage(3)}
            className={` buyButton p-3 ${
              ShareInfoPage === 3 && mobileView && "buyButtonActive"
            }`}
            label="Share Price Alert"
          />
          <NormalButton
            onClick={() => setShareInfoPage(4)}
            className={` buyButton p-3 ${
              ShareInfoPage === 4 && mobileView && "buyButtonActive"
            }`}
            label="Investment Calculator"
          />
        </div>
      </div>
      <div
        className={`share-info-iframe ${
          mobileView && "share-info-iframe_mobile"
        }`}
      >
        {ShareInfoPage === 0 && (
          <iframe
            src="https://ksatools.eurolandir.com/tools/sharegraph/?s=2522&companycode=SA-8010&lang=en-GB"
            width="100%"
            height="1312px"
            scrolling="no"
            frameborder="0"
            title="Share Graph"
          />
        )}
        {ShareInfoPage === 1 && (
          <iframe
            src="https://ksatools.eurolandir.com/tools/shareseries/?companycode=SA-8010&v=r2022&lang=en-GB"
            width="100%"
            height="700px"
            scrolling="no"
            frameborder="0"
            title="Share Series"
          />
        )}
        {ShareInfoPage === 2 && (
          <iframe
            src="https://ksatools.eurolandir.com/tools/splookup/?companycode=sa-8010&v=r2022&lang=en-GB"
            width="100%"
            height="756px"
            scrolling="no"
            frameborder="0"
            title="Share price lookup"
          />
        )}
        {ShareInfoPage === 3 && (
          <iframe
            src="https://ksatools.eurolandir.com/tools/sharealert/?cid=96855&companycode=sa-8010&lang=en-gb"
            width="100%"
            height="1134px"
            scrolling="no"
            frameborder="0"
            title="Share price alert"
          />
        )}
        {ShareInfoPage === 4 && (
          <iframe
            src="https://ksatools.eurolandir.com/tools/investmentcal2/?companycode=sa-8010&v=r2022&lang=en-GB"
            width="100%"
            height="413px"
            scrolling="no"
            frameborder="0"
            title="Investment calc"
          />
        )}
      </div>
    </React.Fragment>
  );
};
