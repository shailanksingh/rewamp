import React, { useState } from "react";
import { NormalButton } from "component/common/NormalButton";
import "./style.scss";

export const FinancialInformation = ({ mobileView }) => {
  const [ShareInfoPage, setShareInfoPage] = useState(2);

  return (
    <React.Fragment>
      <div
        className={`financialinformation ${mobileView && "financial_mobile"}`}
      >
        <h5>Financial Information</h5>
        <div className="financial-info-subnav">
          <NormalButton
            onClick={() => setShareInfoPage(0)}
            className={` buyButton p-3 ${
              ShareInfoPage === 0 && mobileView && "buyButtonActive"
            }`}
            label="Reports & Results"
          />
          <NormalButton
            onClick={() => setShareInfoPage(1)}
            className={` buyButton p-3 ${
              ShareInfoPage === 1 && mobileView && "buyButtonActive"
            }`}
            label="Earnings Webcast"
          />
          <NormalButton
            onClick={() => setShareInfoPage(2)}
            className={` buyButton p-3 ${
              ShareInfoPage === 2 && mobileView && "buyButtonActive"
            }`}
            label="Key Figures"
          />
        </div>
      </div>
      <div className="share-info-iframe">
        {/* {ShareInfoPage === 0 && (
          <iframe
            src="https://ksatools.eurolandir.com/tools/sharegraph/?s=2522&companycode=SA-8010&lang=en-GB"
            width="100%"
            height="1312px"
            scrolling="no"
            frameborder="0"
            title="1"
          />
        )}
        {ShareInfoPage === 1 && (
          <iframe
            src="https://ksatools.eurolandir.com/tools/shareseries/?companycode=SA-8010&v=r2022&lang=en-GB"
            width="100%"
            height="700px"
            scrolling="no"
            frameborder="0"
            title="2"
          />
        )} */}
        {ShareInfoPage === 2 && (
          <iframe
            src="https://ksatools.eurolandir.com/tools/ia/?companycode=SA-8010&v=r2022&lang=en-gb"
            width="100%"
            height="926px"
            scrolling="no"
            frameborder="0"
            title="3"
          />
        )}
      </div>
    </React.Fragment>
  );
};
