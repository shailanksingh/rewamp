import React from "react";
import "./style.scss";
export const Announcements = ({ mobileView }) => {
  return (
    <React.Fragment>
      <div className={`announcements ${mobileView && "announcements_mobile"}`}>
        <h5>Announcements</h5>
        <iframe
          src="https://ksatools.eurolandir.com/tools/pressreleases/?companycode=sa-8010&v=r2022&lang=en-GB"
          width="100%"
          height="1066px"
          scrolling="no"
          frameborder="0"
          title="Announcements"
        />
      </div>
    </React.Fragment>
  );
};
