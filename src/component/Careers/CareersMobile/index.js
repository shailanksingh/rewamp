import React, { useState } from "react";
import "./style.scss";
import HeaderStepsSticky from "component/common/MobileReuseable/HeaderStepsSticky";
import HeaderStickyMenu from "component/common/MobileReuseable/HeaderStickyMenu";
import play_button from "assets/images/mobile/play_button.svg";
import { careerContentData } from "component/common/MockData";
import JoinOurCommunity from "component/common/MobileReuseable/JoinOurCommunity";
import FooterMobile from "component/common/MobileReuseable/FooterMobile";
import ourmission from "assets/images/mobile/ourmission.png";
const CareersMobile = () => {
  const watchFlim = () => {
    window.open(
      "https://careers.tawuniya.com.sa/assets/images/custom-images/ATS/tawuniya.mp4",
      "_self"
    );
  };
  const [datas, setDatas] = useState([
    {
      id: 1,
      title: "Accessibility",
      description:
        "Technology is most powerful when everyone can make their mark.",
    },
    {
      id: 2,
      title: "Accessibility",
      description:
        "Technology is most powerful when everyone can make their mark.",
    },
    {
      id: 2,
      title: "Accessibility",
      description:
        "Technology is most powerful when everyone can make their mark.",
    },
    {
      id: 2,
      title: "Accessibility",
      description:
        "Technology is most powerful when everyone can make their mark.",
    },
  ]);
  return (
    <div className="careers_mobile_container">
      <HeaderStickyMenu />
      <HeaderStepsSticky title={"Careers"} buttonTitle={"Join Now"} />
      <div className="career_banner">
        <img src={ourmission} alt="Banner" className="w-100" />
        <div className="banner_body">
          <h6>
            Our mission is to help people do the <br />
            impossible
          </h6>
          <div className="d-flex align-items-center justify-content-center">
            <img
              src={play_button}
              alt="Play"
              className=" cursor-pointer"
              onClick={watchFlim}
            />
            <label onClick={watchFlim}>Watch the film</label>
          </div>
        </div>
      </div>
      <div className="innovation_part">
        <h6> Be a part of meaningful innovation</h6>
        <p>
          Work with the very best people and be a part of innovation that makes
          a real difference in the lives of millions.
        </p>
      </div>
      <div className="career_body">
        {careerContentData?.map(({ banner, label, description, id }, index) => {
          return (
            <>
              <img src={banner} alt="Banner" />
              <div
                className={`career_body_card ${id > 1 && "card_body_space"} ${
                  careerContentData.length === index + 1 && "mb-0"
                }`}
              >
                <h6>{label}</h6>
                <p>{description}</p>
              </div>
            </>
          );
        })}
      </div>
      <div className="careers_accessibility">
        <span>
          Our values are part of everything built here — including careers
        </span>
        <div className="careers_body_container">
          <div className="career-parent">
            {datas.map((data) => (
              <div key={data.id} className="career_box">
                <div className="career-title">{data.title}</div>
                <div className="career-des">{data.description}</div>
              </div>
            ))}
          </div>
        </div>
      </div>
      <JoinOurCommunity />
      <FooterMobile />
    </div>
  );
};

export default CareersMobile;
