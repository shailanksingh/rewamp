import React from "react";
import BannerImage1 from "../../../assets/images/careers-banner1.png";
import BannerImage2 from "../../../assets/images/careers-banner2.png";
import BannerImage3 from "../../../assets/images/careers-banner3.png";
import CareersImage1 from "../../../assets/images/careers-image1.png";
import CareersImage2 from "../../../assets/images/careers-image2.png";
import CareersImage3 from "../../../assets/images/careers-image3.png";
import PlayIcon from "../../../assets/images/playicon.svg";
import { ProgramBenefits } from "../Components";
import "./style.scss";

let programBenefitsContent = {
	block1: [
		{
			title: "Accessibility",
			description:
				"Technology is most powerful when everyone can make their mark.",
		},
		{
			title: "Privacy",
			description:
				"We design Tawuniya products to protect your privacy and give you control over your information",
		},
	],
	block2: [
		{
			title: "Education",
			description:
				"Education is the great equalizer and a powerful source of opportunity for all",
		},
		{
			title: "Environment",
			description: "Our goal is to leave the planet better than we found it",
		},
	],
	block3: [
		{
			title: "Inclusion and Diversity",
			description:
				"We're committed to making Tawuniya more inclusive and the world more just",
		},
		{
			title: "Supplier Responsibility",
			description:
				"We believe in a safe, respectful, and supportive workplace for everyone",
		},
	],
	block4: [
		{
			title: "Racial Equity and Justice Initiative",
			description:
				"This is a long-term effort to help ensure more positive outcomes for communities of color",
		},
	],
};

const CareersDesktop = () => {
	return (
		<div className="careers-page-desktop">
			<div
				className="banner-section"
				style={{ backgroundImage: "url(" + BannerImage1 + ")" }}
			>
				<div className="banner-title">
					Our mission is to help people do <br />
					the impossible
				</div>
				<div className="banner-link">
					<div>
						<img src={PlayIcon} alt="..." /> Watch the film
					</div>
				</div>
			</div>
			<div className="meaningful-innovation">
				<div>
					<div className="title">Be a part of meaningful innovation</div>
					<div className="description">
						Work with the very best people and be a part of innovation that
						makes a real difference in the lives of millions.
					</div>
					<div className="section-box">
						<div className="box-title">Take on big challenges</div>
						<p>
							Throughout our history, we've taken on big challenges and we
							strive to be the very best at what we do. This spirit is the
							driving force that has made us an insurance leader and sustains
							our ambition to develop cutting-edge technologies that push the
							boundaries of what’s possible. Come join us as we take on the next
							big challenges of the future.
						</p>
					</div>
					<div className="image">
						<img src={CareersImage1} alt="..." />
					</div>
				</div>
				<div>
					<div className="image">
						<img src={CareersImage2} alt="..." />
					</div>
					<div className="section-box">
						<div className="box-title">Shape the future of our world</div>
						<p>
							It is the diversity, creativity, and passion of the people who
							work at Tawuniya that have made us one of the world’s most
							innovative companies, and our people continue to drive our
							innovation forward. We work together in an open and collaborative
							environment that promotes sharing of the unique knowledge and
							expertise that each individual brings. This is a place where you
							can work with great people and your ideas can be brought to life
							in new products and solutions that are shaping the future of how
							we live.
						</p>
					</div>
				</div>
				<div className="align-top-content">
					<div className="section-box">
						<div className="box-title">Make a positive impact</div>
						<p>
							What makes working at Tawuniya meaningful is knowing that you are
							playing an important role in bringing new technologies to the
							world that improve people’s lives. Our innovations enable people
							in new ways, and we employ our technology to contribute to growth
							and development in the communities in which we operate around the
							world. Here at Tawuniya, you have the opportunity to do truly
							meaningful work that has a positive impact on the world.
						</p>
					</div>
					<div className="image">
						<img src={CareersImage3} alt="..." />
					</div>
				</div>
			</div>
			<ProgramBenefits
				programBenefits={programBenefitsContent}
				banner={BannerImage2}
			/>
			<div className="careers-page-text-content">
				We take inspiration from what people want to accomplish in their lives
				and work to create products that empower in new ways. This is what{" "}
				<br />
				drives our innovation forward.
				<br />
				Find your opportunity to do what can't be done at Tawuniya.
			</div>
			<div
				className="banner-section bottom-content"
				style={{ backgroundImage: "url(" + BannerImage3 + ")" }}
			>
				<div className="banner-subtitle">
					Join our community and help define it.
				</div>
				<p>
					Explore a collaborative culture of inclusion, growth, and originality,
					supported by resources that make a difference in your life.
				</p>
				<div>
					<button
						onClick={() =>
							window.open(
								"https://www.linkedin.com/company/tawuniya/jobs/",
								"_self"
							)
						}
					>
						Join Our Community
					</button>
				</div>
			</div>
		</div>
	);
};

export default CareersDesktop;
