import React from "react";
import "./style.scss";

export const Cards = ({ data }) => {
  return (
    <>
      <div className="insurance-program-cards-careers card-type-3">
        <div className="text">{data?.title}</div>
        <div className="description">{data?.description}</div>
      </div>
    </>
  );
};
