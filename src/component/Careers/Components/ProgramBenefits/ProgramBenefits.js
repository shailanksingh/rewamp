import React from "react";
import "./style.scss";
import { Cards } from "../index";
import { history } from "service/helpers";

export const ProgramBenefits = ({ programBenefits, banner, routeURL }) => {
  let programBenefitsKeys = Object.keys(programBenefits);
  return (
    <div
      className="insurance-program-benefits-careers"
      style={{ backgroundImage: "url(" + banner + ")" }}
    >
      <div className="benefits-title">Our values are part of everything <br />built here — including careers</div>
      <div className="cards-list-block">
        {programBenefitsKeys?.map((key, keyIndex) => {
          return (
            <div className="cards-column" key={keyIndex.toString()}>
              {programBenefits?.[key]?.map((item, index) => {
                return (
                  <div
                    className="benefits-card-root"
                    key={index.toString()}
                    onClick={() => history.push(routeURL)}
                  >
                    <Cards data={item} />
                  </div>
                );
              })}
            </div>
          );
        })}
      </div>
    </div>
  );
};
