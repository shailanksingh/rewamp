import React, { useState } from "react";
import { Banner, Advantages, GeneralInsuranceAgent } from "./InsuranceAgentComponents";
import "./style.scss";
import RewardsIcon from "assets/svg/rewards.svg";
import LearnIcon from "assets/svg/learn.svg";
import HelpIcon from "assets/svg/help2.svg";
import FlexibleIcon from "assets/svg/flexible.svg";
import FinancialIcon from "assets/svg/financial.svg";
import kpIcon1 from "assets/svg/kpIcon1.svg";
import kpIcon2 from "assets/svg/kpIcon2.svg";
import kpIcon3 from "assets/svg/kpIcon3.svg";
import kpIcon4 from "assets/svg/kpIcon4.svg";
import { Faq } from "component/common/FraudDetails";
import { faqCategoryData } from "component/common/MockData/index";
import { NormalSearch } from "component/common/NormalSearch";
import Profile from "assets/svg/user-icon-grey.svg";
import BuildingIcon from "assets/svg/building-icon-grey.svg";
import OfficeBagIcon from "assets/svg/office-bag-grey.svg";
import EmailIcon from "assets/svg/email-plus.svg";

const advantages = [{
    icon: RewardsIcon,
    title: "Rewards & Recognition",
    description: "A dedicated team is appointed to ensure claim are properly processed, adjusted and settled in timely manner as necessary",
}, {
    icon: LearnIcon,
    title: "Learn from Industry Experts",
    description: "A dedicated team is appointed to ensure claim are properly processed, adjusted and settled in timely manner as necessary",
}, {
    icon: HelpIcon,
    title: "Help Secure Lives",
    description: "A dedicated team is appointed to ensure claim are properly processed, adjusted and settled in timely manner as necessary",
}, {
    icon: FlexibleIcon,
    title: "Flexible Working",
    description: "A dedicated team is appointed to ensure claim are properly processed, adjusted and settled in timely manner as necessary",
}, {
    icon: FinancialIcon,
    title: "Fianancial Freedom",
    description: "A dedicated team is appointed to ensure claim are properly processed, adjusted and settled in timely manner as necessary",
}];

let errorMessages = {
    firstName: { required: "First Name field is required" },
    lastName: { required: "Last Name field is required" },
    email: {  
        required: "Email field is required",
        emailError: "The Email you entered is not valid",
    },
    companyName: { required: "Company Name field is required" },
    country: { required: "Country field is required" },
};

const InsuranceAgent = () => {
    const [formInput, setFormInput] = useState({
        firstName: "",
        lastName: "",
        email: "",
        companyName: "",
        country: "",
    });
    const [ error, setError ] = useState(null);
    const handleFormInputChange = (e) => {
        const { name, value } = e.target || e || {};
        setFormInput({ ...formInput, [name]: value });
    };
    const FaqDescription = () => {
        return <>Review answers to commonly asked questions at Tawuniya, which enable you to be directly involved in <br />improving our support experience.</>;
    }
    const formValidation = (keyName) => {
        let tempError = Object.assign({}, error);
        const mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        if (keyName) {
            if (typeof tempError === "object") delete tempError[keyName];
            if (errorMessages[keyName].required) {
                if (!formInput[keyName]) {
                    if (!tempError[keyName]) tempError[keyName] = {};
                    tempError[keyName].required = errorMessages[keyName].required;
                }
            }
            if (errorMessages[keyName].emailError) {
                if (formInput[keyName] && !(mailformat.test(formInput[keyName]))) {
                    if (!tempError[keyName]) tempError[keyName] = {};
                    tempError[keyName].emailError = errorMessages[keyName].emailError;
                }
            }
        } else {
            let formInputKeys = Object.keys(formInput);
            tempError = {};
            formInputKeys.map((key) => {
                if (errorMessages[key].required) {
                    if (!formInput[key]) {
                        if (!tempError[key]) tempError[key] = {};
                        tempError[key].required = errorMessages[key].required;
                    } 
                }
                if (errorMessages[key].emailError) {
                    if (formInput[key] && !(mailformat.test(formInput[key]))) {
                        if (!tempError[key]) tempError[key] = {};
                        tempError[key].emailError = errorMessages[key].emailError;
                    }
                }
            });
        }
        if (Object.keys(tempError).length !== 0) {
            setError({ ...tempError });
            return true;
        } else {
            setError(null);
            return false;
        }
    }
    const formSubmit = () => {
        let isFormHaveError = formValidation();
        if (!isFormHaveError) {
            console.log("Form Validated");
        }
    }
    return (
        <div className="insurance-agent-page">
            <div className="insurance-agent-banner-root"><Banner /></div>
            <div className="advantages-root">
                <Advantages 
                    title={"Advantages Of The Program"}
                    description={"The Saudi insurance pioneer, will help you to identify, analyze and manage such risks and suggest appropriate insurance solutions."}
                    advantages={advantages}
                />
            </div>
            <div className="general-insurance-agent-root"><GeneralInsuranceAgent /></div>
            <div className="AbtUsKeyPromises">
                <div className="AbtUskpCardsContainer">
                <div className="AbtUskpCardLeft">
                    <div className="AbtUskpCard">
                    <img src={kpIcon1} alt="" />
                    <h5>Work directly with Tawanya</h5>
                    <p>To keep pace with the changes in customer needs.</p>
                    </div>
                    <div className="AbtUskpCard">
                    <img src={kpIcon2} alt="" />
                    <h5>Insurance made simple</h5>
                    <p>
                        To keep pace with the changes in customer needs, community demands.
                    </p>
                    </div>
                </div>
                <div className="AbtUskpCardRight">
                    <div className="AbtUskpCard">
                    <img src={kpIcon3} alt="" />
                    <h5>Wide Range of Products</h5>
                    <p>
                        To keep pace with the changes in customer needs, community demands, new technology.
                    </p>
                    </div>
                    <div className="AbtUskpCard">
                    <img src={kpIcon4} alt="" />
                    <h5>Strong backend support</h5>
                    <p>
                        To keep pace with the changes in customer needs, community demands, new technology.
                    </p>
                    </div>
                </div>
                </div>
                <div className="AbtUskpTextContainer">
                <div className="AbtUsKpText">
                    <h2>
                        Why become a General Insurance <br /><span>Agent</span> with Tawanya?  
                    </h2>
                    <p>
                    Achieve success and transfer its ambitious strategy into
                    reality.
                    </p>
                    <button>Contact Us</button>
                </div>
                </div>
            </div>
            <div className="faq-section-root">
                <Faq 
                    title="You've got questions, we've got answers"
                    description={FaqDescription()}
                    faqData={faqCategoryData} 
                    viewAll={true} 
                />
            </div>
            <div className="tawuniya-partner-form">
                <div className="form-title">Become a Tawuniya Partner</div>
                <div className="form-description">1,000+ Partners have earned with Tawuniya</div>
                <div className="forms-list-root">
                    <div className="forms-list">
                        <div className="input-root width-50">
                            <NormalSearch
                                className="formInputFieldOne"
                                name="firstName"
                                value={formInput.firstName}
                                placeholder="First Name"
                                onChange={handleFormInputChange}
                                needLeftIcon={true}
                                leftIcon={Profile}
                                errorMessage={error?.firstName ? error?.firstName?.required : null}
                            />
                        </div>
                        <div className="input-root width-50">
                            <NormalSearch
                                className="formInputFieldOne"
                                name="lastName"
                                value={formInput.lastName}
                                placeholder="Last Name"
                                onChange={handleFormInputChange}
                                needLeftIcon={true}
                                leftIcon={Profile}
                                errorMessage={error?.lastName ? error?.lastName?.required : null}
                            />
                        </div>
                        <div className="input-root">
                            <NormalSearch
                                className="formInputFieldOne"
                                name="email"
                                value={formInput.email}
                                placeholder="Enter Your Email"
                                onChange={handleFormInputChange}
                                needLeftIcon={true}
                                leftIcon={EmailIcon}
                                errorMessage={!error?.email ? null : error?.email?.required ? error?.email?.required : error?.email?.emailError}
                            />
                        </div>
                        <div className="input-root">
                            <NormalSearch
                                className="formInputFieldOne"
                                name="companyName"
                                value={formInput.companyName}
                                placeholder="Company Name"
                                onChange={handleFormInputChange}
                                needLeftIcon={true}
                                leftIcon={BuildingIcon}
                                errorMessage={error?.companyName ? error?.companyName?.required : null}
                            />
                        </div>
                        <div className="input-root">
                            <NormalSearch
                                className="formInputFieldOne"
                                name="country"
                                value={formInput.country}
                                placeholder="Country"
                                onChange={handleFormInputChange}
                                needLeftIcon={true}
                                leftIcon={OfficeBagIcon}
                                errorMessage={error?.country ? error?.country?.required : null}
                            />
                        </div>
                    </div>
                </div>
                <button onClick={() => formSubmit()}>Submit</button>
            </div>
        </div>
    );
}

export default InsuranceAgent;