import React from "react";
import "./style.scss";

export const Advantages = ({
    title,
    description,
    advantages
}) => {
    return (
        <div className="insurance-agent-component">
            <div className="insurance-agent-advantages">
                {title && <div className="title">{title}</div>}
                {description && <div className="description">{description}</div>}
                {advantages && <div className="advantages-list-root">
                    {advantages?.map(({
                        icon,
                        title: listTitle, 
                        description: listDescription
                    }, index) => {
                        return (
                            <div className="list" key={index.toString()}>
                                {icon && <img src={icon} alt="..." />}
                                {listTitle && <div className="list-title">
                                    {listTitle}    
                                </div>}
                                {listDescription && <div className="list-description">
                                    {listDescription}    
                                </div>}
                            </div>
                        );
                    })}    
                </div>}
            </div>
        </div>
    );
}