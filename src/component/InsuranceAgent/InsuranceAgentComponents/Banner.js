import React from "react";
import "./style.scss";
import BannerImage from "assets/images/MediaCenterImgCard1.png";

export const Banner = () => {
    return (
        <div className="insurance-agent-component">
            <div className="Insurance-agent-banner" style={{ backgroundImage: "url(" + BannerImage + ")" }}>
                <div className="right-side-content">
                    <div className="website">Tawuniya</div>
                    <div className="page-name">Insurance <span>Agent</span></div>
                    <div className="description">Summary of key financials and investor information.</div>
                    <p>The Company for Cooperative Insurance (Tawuniya) is a Saudi Joint Stock Company, and it was incorporated on January 18, 1986 under the Commercial Registration No. 1010061695. Tawuniya is the first national insurance company licensed in the Kingdom of Saudi Arabia to practice all types of insurance business in accordance with the cooperative insurance principle that is accepted by Islamic Sharyia.</p>
                </div>
            </div>
        </div>
    );
}