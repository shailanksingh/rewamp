import React from "react";
import "./style.scss";
import BannerImage from "assets/images/general-insurance-banner.png";

export const GeneralInsuranceAgent = () => {
    return (
        <div className="insurance-agent-component">
            <div className="general-insurance-agent">
                <div className="image">
                    <img src={BannerImage} alt="..." />
                </div>
                <div className="content">
                    <div className="title">General Insurance Agent</div>
                    <p>A General Insurance includes all non-life insurance policies such as car insurance, bike insurance, travel insurance, SFSP insurance, and health insurance among others.</p>
                    <p>These general insurance policies help people to protect themselves from losses in case of an unfortunate incident.</p>
                    <p>For example, a car insurance could protect someone from losses and damages caused to their car during an accident whilst a SFSP insurance would cover for damages in case of a burglary or natural calamity.</p>
                </div>
            </div>
        </div>
    );
}