import React, { useState } from "react";
import { TitleBannerMobile } from "component/common/TitleBannerMobile";
import Tick from "assets/images/mobile/tick.svg";
import Vehicle from "assets/images/mobile/vehicle.png";
import Location from "assets/images/mobile/location.png";
import Service from "assets/images/mobile/service.png";
import Eye from "assets/images/mobile/eye.png";
import Date from "assets/images/mobile/date.png";
import ConfirmationWoman from "../../../assets/images/mobile/woman.svg";
import "./style.scss";
import { BottomButtonMobile } from "component/common/BottomButtonMobile";
import AnimatedBottomPopup from "component/common/MobileReuseable/AnimatedBottomPopup";

const RoadAssistanceRequestMobile = () => {
  const [open, setOpen] = useState(false);
  return (
    <div>
      <TitleBannerMobile title="Road Assistance" subTitle="Request #123456" />
      <div className="road-assistance-request">
        <div className="request-progress">
          <ul>
            <li>
              <p>Approved</p>
              <img src={Tick} alt="Tick" />
            </li>
            <li className={`${open && "icon-circle-active"}`}>
              <p>In Progress</p>
              <img src={open ? Eye : Tick} alt="Tick" />
            </li>
            <li className={`${open && "in-active"}`}>
              <p>Completed</p>
              {open ? (
                <span className="in-active">3</span>
              ) : (
                <img src={Tick} alt="Tick" />
              )}
            </li>
          </ul>
        </div>
        <div className="request-card">
          <ul>
            <li>
              <div className="icon-block">
                <img src={Vehicle} alt="Vehicle" />
              </div>
              <div className="head-block">
                <span>Vehicle</span>
                <p>Merc. Benz</p>
              </div>
            </li>
            <li className="active">
              <div className="icon-block">
                <img src={Service} alt="Service" />
              </div>
              <div className="head-block">
                <span>Service</span>
                <p>Tire</p>
              </div>
            </li>
            <li>
              <div className="icon-block">
                <img src={Location} alt="Location" />
              </div>
              <div className="head-block">
                <span>Location</span>
                <p>Alyasmin, Riyadh 13331</p>
              </div>
            </li>
            <li>
              <div className="icon-block">
                <img src={Date} alt="Date"></img>
                <span className="date">12 Jun 2022</span>
              </div>
            </li>
          </ul>
        </div>
        {!open && (
          <div className="woman-block">
            <img src={ConfirmationWoman} alt="ConfirmationWoman"></img>
            <p>Your request has been completed</p>
          </div>
        )}
      </div>

      <AnimatedBottomPopup open={open} setOpen={setOpen}>
        <div className="request-model">
          <img src={ConfirmationWoman} alt="ConfirmationWoman" />
          <p>We have received your request</p>
          <h4># 123456</h4>
          <BottomButtonMobile
            title="View request"
            onClick={() => {
              setOpen(false);
            }}
          />
        </div>
      </AnimatedBottomPopup>
    </div>
  );
};

export default RoadAssistanceRequestMobile;
