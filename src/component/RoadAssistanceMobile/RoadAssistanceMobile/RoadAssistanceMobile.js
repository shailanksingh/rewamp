import React, { useEffect, useState } from "react";
import "./style.scss";
import { TitleBannerMobile } from "component/common/TitleBannerMobile";
import MotorFraud from "assets/images/mobile/Motor.png";

import right_arrow from "assets/images/mobile/right_arrow.png";
import Chatsmile from "assets/images/mobile/Chatsmile.png";
import car_image from "assets/images/mobile/car_image.png";
import pdf_icon from "assets/images/mobile/pdf_icon.png";
import time from "assets/images/mobile/time.png";
import vertical_pointer from "assets/images/mobile/vertical_pointer.png";
import { QuestionSection } from "./QuestionSection";
import { BottomButtonMobile } from "component/common/BottomButtonMobile";
import { history } from "service/helpers";
import HeaderBackNav from "component/common/MobileReuseable/HeaderBackNav";
import { MobileStepper } from "@material-ui/core";
import HeaderStickyMenu from "component/common/MobileReuseable/HeaderStickyMenu";
import HeaderStepsSticky from "component/common/MobileReuseable/HeaderStepsSticky";
import HeaderCloseNav from "component/common/MobileReuseable/HeaderCloseNav";
import RoadAssistanceMotorService from "component/RoadAssistanceMotorService";

export const RoadAssistanceMobile = (viewservice) => {
  useEffect(() => {
    sessionStorage.setItem("title", viewservice.viewservice.services.subtitle);
  });

  return (
    <>
      <div className="road_assistance_container">
        <HeaderCloseNav
          pageName={viewservice.viewservice.services.title}
          title={viewservice.viewservice.services.subtitle}
        />
        <div>
          <div className="motor_fraud_card">
            <img src={MotorFraud} alt="Motor_fraud" />
            <p>{viewservice.viewservice.services.content}</p>
          </div>
        </div>
        <div className="motor_fraud_title">
          <h4>How it works?</h4>
        </div>
        <div className="service_card">
          <div className="">
            <img src={vertical_pointer} alt="Line" />
            <h6>Submit Application</h6>
            <p>
              Complete your request details and attached the needed documents
            </p>
          </div>
          <div className="">
            <img src={vertical_pointer} alt="Line" />
            <h6>Review</h6>
            <p>The application will be reviewed in 3 days by our team</p>
          </div>
          <div className="">
            <img src={vertical_pointer} alt="Line" />
            <h6>Get Reimburse</h6>
            <p>
              Reimbursement will be deposited to your bank account within 3
              working days after approval
            </p>
          </div>
        </div>
        <div className="interactions_status">
          <h6>Recent Interactions</h6>
          <div className="interaction_card">
            <div className="body">
              <label>
                <img src={time} alt="Time" className="mr-1" />
                02/06/2022
              </label>
              <div>
                <label>
                  <img src={pdf_icon} alt="Pdf" className="mr-1" />
                  17364427
                </label>
                <span>In Progress</span>
              </div>
            </div>
            <div className="interaction_card_body">
              <div className="content">
                <div>
                  <img src={car_image} alt="Car" className="mr-1" />
                </div>
                <div>
                  <h6>Merc. Benz</h6>
                  <h6>Prashant Dixit</h6>
                </div>
              </div>
              <div className="help_content">
                <img src={Chatsmile} alt="Smile" />
                <label>Get Help</label>
              </div>
            </div>
          </div>
          <div
            className="view_all_question"
            onClick={() => history.push("/home/service/approvalist")}
          >
            <label>View All Interactions</label>
            <img src={right_arrow} alt="Arrow" />
          </div>
        </div>
        <div className="question_section">
          <h6>Frequently Asked Questions</h6>

          <QuestionSection />
        </div>
        <div className="view_all_question_bottom">
          <label>VIEW MORE FAQS</label>
          <img src={right_arrow} alt="Arrow" />
        </div>
      </div>

      <div className="card container-fluid report_button align_sticky pt-2">
        <button onClick={() => history.push("/dashboard/motor-service")}>
          Request this Service
        </button>
      </div>

      {/* <BottomButtonMobile
        onClick={() => history.push("/road-assisstance/motor-service")}
        title="Request this Service"
      /> */}
    </>
  );
};
