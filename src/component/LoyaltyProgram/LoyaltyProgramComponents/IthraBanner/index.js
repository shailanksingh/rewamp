import React from "react";
import { NormalButton } from "component/common/NormalButton";
import ithraJim from "assets/svg/LoyaltyProgram/ithrajim.svg";
import ithraRelation from "assets/svg/LoyaltyProgram/ithraRelations.svg";
import ithraOfficeFamily from "assets/svg/LoyaltyProgram/ithraOfficeFamily.svg";
import unionOne from "assets/svg/LoyaltyProgram/ithraUnionOne.svg";
import unionTwo from "assets/svg/LoyaltyProgram/ithraUnionTwo.svg";
import mouseScroll from "assets/svg/LoyaltyProgram/ithraMouseScroll.svg";
import buynowArrow from "assets/svg/buynowArrow.svg";
import "./style.scss";

export const IthraBanner = ({ ithraBannerData, isGetQuote = false }) => {
  return (
    <div className="row">
      <div className="col-lg-12 col-12 ithra-banner-layout">
        <div className="ithra-banner-box">
          <div className="d-flex justify-content-center">
            <div>
              <p className="ithra-banner-header fw-800 text-center m-0">
                {ithraBannerData.header}
              </p>
              {!isGetQuote && (
                <p className="ithra-banner-para fs-20 fw-400 text-center">
                  {ithraBannerData.text}
                </p>
              )}
            </div>
          </div>
          {isGetQuote && (
            <div className="d-flex justify-content-center">
              <div>
                <p className="ithra-banner-quotes fs-15 fw-400 text-center pt-2">
                  {ithraBannerData.qoutes}
                </p>
                <NormalButton
                  label="Get Quote"
                  className="get-quote-taj-btn mx-auto d-block p-4"
                  adjustIcon="pl-3"
                  needBtnPic={true}
                  src={buynowArrow}
                />
              </div>
            </div>
          )}
          <img
            src={mouseScroll}
            className="img-fluid ithraMouseScrollPic cursor-pointer"
            alt="scroll"
          />
        </div>
        <div className="ithra-banner-content-box">
          <img
            src={ithraOfficeFamily}
            className="img-fluid ithraOfficeFamilyPic"
            alt="relation"
          />
          <img src={ithraJim} className="img-fluid ithraJimPic" alt="jim" />
          <img
            src={ithraRelation}
            className="img-fluid ithraRelationPic"
            alt="relation"
          />
          <div className="d-flex justify-content-center">
            <div>
              <p className="fs-15 fw-400 ithra-banner-content-para mb-0 text-center py-4">
                {ithraBannerData.para}
              </p>
            </div>
          </div>
          <img src={unionOne} className="img-fluid ithraUnionOne" alt="union" />
          <img src={unionTwo} className="img-fluid ithraUnionTwo" alt="union" />
        </div>
      </div>
    </div>
  );
};
