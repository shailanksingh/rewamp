import React from "react";
import "./style.scss";
import installDrive from "assets/svg/LoyaltyProgram/installDrive.svg";
import driveSafe from "assets/svg/LoyaltyProgram/driveSafe.svg";
import getRewarded from "assets/svg/LoyaltyProgram/getRewarded.svg";
import curveBtm from "assets/svg/LoyaltyProgram/curveBtm.svg";
import curveTop from "assets/svg/LoyaltyProgram/curveTop.svg";
import { NormalButton } from "component/common/NormalButton";
import buynowArrow from "assets/svg/buynowArrow.svg";

export const HowitWorks = () => {
	return (
		<div>
			<div className="how-it-work-Features mb-5">
				<h5>How it works:</h5>
				<p>
					You earn Tawuniya Drive points every week based on how well you drive.
					Download the Tawuniya drive app, drive safe and get rewarded.
				</p>
				<div className="how-it-work-fetBox">
					<div className="how-it-work-FetBoxItem">
						<img src={installDrive} alt="login icon" />
						<span>
							Install <br /> Tawuniya Drive
						</span>
					</div>
					<img
						className="how-it-work-CurvLineA"
						src={curveBtm}
						alt="curved line icon"
					/>
					<div className="how-it-work-FetBoxItem">
						<img src={driveSafe} alt="drive icon" />
						<span>
							Drive <br /> Safe
						</span>
					</div>
					<img
						className="how-it-work-CurvLineB"
						src={curveTop}
						alt="curved line icon"
					/>
					<div className="how-it-work-FetBoxItem">
						<img src={getRewarded} alt="medal icon" />
						<span>
							Get <br /> Rewarded
						</span>
					</div>
				</div>
				<NormalButton
					label="Claim Your Rewards"
					className="how-it-work-claim-btn mt-4"
					adjustIcon="pl-3"
					needBtnPic={true}
					src={buynowArrow}
				/>
			</div>
		</div>
	);
};
