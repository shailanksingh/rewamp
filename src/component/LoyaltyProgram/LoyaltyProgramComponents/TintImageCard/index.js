import React from "react";
import "./style.scss";

export const TintImageCard = ({ TintImgCardData }) => {
	return (
		<div className="d-flex justify-content-between px-4 w-100 tint-image-card-container">
			{TintImgCardData.map((item, id) => {
				return (
					<div
						className="tint-image-container "
						style={{
							backgroundImage: `url(${item.bgImg})`,
							backgroundRepeat: "no-repeat",
							backgroundSize: "cover",
							height: "590px",
							width: "400px",
						}}
					>
						<p
							className={`tint-img-innerText d-flex flex-column ${item.position}`}
						>
							{item.content}
						</p>
					</div>
				);
			})}
		</div>
	);
};
