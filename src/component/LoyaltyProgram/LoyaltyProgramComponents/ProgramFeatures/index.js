import React from "react";
import "./style.scss";

export const ProgramFeatures = ({ programFeaturesData }) => {
	return (
		<div>
			{programFeaturesData.map((item, id) => {
				return (
					<div key={id} className="program-features-container">
						<div className="program-features-text d-flex flex-column align-items-center">
							<h5>{item.title}</h5>
							<p>{item.subTitle}</p>
						</div>
						<div className="d-flex">
							<div className="program-features-content d-flex align-items-center mt-4 pt-3">
								<div className="program-feature-box-left">
									{item.iconCardDataLft ? (
										<div>
											{item.iconCardDataLft.map((data, id) => {
												return (
													<div key={id} className="program-feature-icon-box">
														<img src={data.cardIcon} alt="" />
														<p>{data.cardContent}</p>
													</div>
												);
											})}
										</div>
									) : (
										<div>
											{item.contentCardDataLft.map((item, id) => {
												return (
													<div className="program-feature-text-box">
														<p>{item.cardContent}</p>
													</div>
												);
											})}
										</div>
									)}
								</div>
								<img
									className="program-feature-center-image px-3"
									src={item.imageCenter}
									alt="PhoneImage"
								/>
								<div className="program-feature-box-right">
									{item.iconCardDataRt ? (
										<div>
											{item.iconCardDataRt.map((data, id) => {
												return (
													<div key={id} className="program-feature-icon-box">
														<img src={data.cardIcon} alt="" />
														<p>{data.cardContent}</p>
													</div>
												);
											})}
										</div>
									) : (
										<div>
											{item.contentCardDataRt.map((item, id) => {
												return (
													<div className="program-feature-text-box">
														<p>{item.cardContent}</p>
													</div>
												);
											})}
										</div>
									)}
								</div>
							</div>
						</div>
					</div>
				);
			})}
		</div>
	);
};
