import React from "react";
import "./style.scss";
import DriveDevice from "assets/svg/LoyaltyProgram/DriveDevice-034-Final-1.svg";

export const WhatIsTawuniyaDrive = () => {
	return (
		<div className="what-tawuniya-drive d-flex flex-column-reverse justify-center align-items-center">
			<img src={DriveDevice} alt="TawuniyaDriveImage" />
			<div className="what-tawuniay-drive-text px-2">
				<h5 className="text-center">What is Tawuniya Drive?</h5>
				<p className="text-center">
					Tawuniya Drive is a new driver behavior program that gives you the
					opportunity to get weekly rewards and discounts on your car insurance
					by earning points based on your driving habits. Once you set up the
					sensor, the Drive app will collect your driving data, including
					acceleration, braking, cornering, speeding, and mobile phone use, in
					addition to other factors that affect driving behavior, such as
					driving late and the distance travelled at night.
				</p>
				<p className="text-center">
					Tawuniya has partnered with Discovery Insure and Vitality Group to
					encourage and reward safe driving on the roads. Once you have joined
					Tawuniya Drive, getting your rewards is easy. The safer you drive, the
					more points you earn and the greater the reward.
				</p>
				<p>This program is intended for Al-Shamel insurance policy holders.</p>
			</div>
		</div>
	);
};
