import React from "react";
import "./style.scss";

export const IthraTable = () => {
  const ithraTableData = {
    headerTitle: 'Beneficiaries of the "Ithra" Program',
    title: "All Tawuniya customers having valid insurance.",
    ithraSubHeaderData: [
      {
        headerDivision: "ithra-header-one",
        headerContent: "Partners",
      },
      {
        headerDivision: "ithra-header-two",
        headerContent: "Percentage of Discount",
      },
      {
        headerDivision: "ithra-header-three",
        headerContent: "How to Get the Discount",
      },
    ],
    ithraSubContentData: [
      {
        id: 0,
        cellContentOne: "Ziebart",
        cellContentTwo:
          "40% discount on the installation of thermal insulation films and tinting for full vehicle glass, 35% on all other services.",
        cellContentThree: ["Show the Insurance Card"],
      },
      {
        id: 1,
        cellContentOne: "Carspa",
        cellContentTwo:
          "20% discount on car caring services to all Tawuniya customers.",
        cellContentThree: ["Show the Insurance Card"],
      },
      {
        id: 2,
        cellContentOne: "LLumar",
        cellContentTwo:
          "40% discount on the installation of thermal insulation films for full vehicle glass, 20% discount on the installation of car-paint protection films.",
        cellContentThree: ["Show the Insurance Card"],
      },
      {
        id: 3,
        cellContentOne: "Ezhalha",
        cellContentTwo: "Up to 15% discount to all Tawuniya customers.",
        cellContentThree: ["Use the discount code: TAWUNIYA"],
      },
      {
        id: 4,
        cellContentOne: "Joi",
        cellContentTwo:
          "15% discount on all products available in the website.",
        cellContentThree: ["Use the discount code: TAWUNIYA"],
      },
      {
        id: 5,
        cellContentOne: "9 Round",
        cellContentTwo:
          "3 months subscription (SR 1,035), 6 months subscription (SR 1,860) and 12 months subscription (SR 3,435).",
        cellContentThree: ["Show the Insurance Card or Insurance Policy"],
      },
      {
        id: 6,
        cellContentOne: "The Healthy home",
        cellContentTwo: "15% discount on all services provided.",
        cellContentThree: ["Show the Insurance Card or Insurance Policy"],
      },
      {
        id: 7,
        cellContentOne: "3M Atlas Trading Company",
        cellContentTwo:
          "40% discount on thermal insulation provided and 50% discount on the other services.",
        cellContentThree: ["Show the Insurance Card or Insurance Policy"],
      },
      {
        id: 8,
        cellContentOne: "Al Mosafer",
        cellContentTwo:
          "6% discount on hotels and tourist packages and 3% discount on flight fares.",
        cellContentThree: [
          "Use the discount code: TAWUNIYA/ Alternatively, call 920000997 – special Ext. (3) or visit Al-Mosafer branches. The Insurance Card or Insurance Policy must be shown.",
        ],
      },
      {
        id: 9,
        cellContentOne: "Al Bazai Toyota & Lexus",
        cellContentTwo:
          "30% discount on Lexus rentals, 35% on Toyota rentals. 17% discount on Lexus spare parts, 25% discount on Toyota spare parts.",
        cellContentThree: ["Show the Insurance Card or Insurance Policy"],
      },
      {
        id: 10,
        cellContentOne: "Daily Mealz",
        cellContentTwo: "20% discount on meals and delivery.",
        cellContentThree: ["Use the discount code: TAWUNIYA"],
      },
      {
        id: 11,
        cellContentOne: "Lumi",
        cellContentTwo: "25% discount on car rentals.",
        cellContentThree: ["Use the discount code: TAWUNIYA"],
      },
      {
        id: 12,
        cellContentOne: "Cambridge Weight Plan",
        cellContentTwo:
          "50% discount on examination and 40% discount on subscription.",
        cellContentThree: ["Show the Insurance Card or Insurance Policy"],
      },
      {
        id: 13,
        cellContentOne: "V-Kool",
        cellContentTwo: "15% to 40% discount on services packages.",
        cellContentThree: ["Show the Insurance Card or Insurance Policy"],
      },
      {
        id: 14,
        cellContentOne: "Noon",
        cellContentTwo:
          "New Users: 15% discount up to SR 75 on (Noon Express) products.Current Users: 10% discount up to SR 50 on (Noon Express) products.",
        cellContentThree: ["Use the discount code: TAW"],
      },
      {
        id: 15,
        cellContentOne: "Aljudaie",
        cellContentTwo:
          "15% discount on men fabrics, excluding the seasonal big sales and special offers at the retail outlets.",
        cellContentThree: ["Show the Insurance Card or Insurance Policy"],
      },
      {
        id: 16,
        cellContentOne: "SPACE Home and Office Furniture",
        cellContentTwo:
          "5%-10% discount on all products in the Store or through the website.",
        cellContentThree: [
          "Through the Store:/Show the Insurance Card or Insurance Policy/ Through the website:/Use the discount code: TA1",
        ],
      },
      {
        id: 17,
        cellContentOne: "Zaatar W Zeit",
        cellContentTwo: "10% on all products available in all branches.",
        cellContentThree: [
          "Through the Branch:/Show the Insurance Card or Insurance Policy",
        ],
      },
      {
        id: 18,
        cellContentOne: "Budget Car Rental",
        cellContentTwo: "42% discounts on car rentals.",
        cellContentThree: [
          "Through the Branch:/Show the Insurance Card or Insurance Policy",
        ],
      },
      {
        id: 19,
        cellContentOne: "Abdul Samad Al Qurashi",
        cellContentTwo: "50% discount on all products",
        cellContentThree: [
          "Through the Branch:/Show the Insurance Card or Insurance Policy",
        ],
      },
      {
        id: 20,
        cellContentOne: "Golden Scent",
        cellContentTwo:
          "15% discount on orders from the website, excluding Channel, Dior, Benefit and make up forever products",
        cellContentThree: [
          "Through the website:/Use the Discount code: TAWU21",
        ],
      },
      {
        id: 21,
        cellContentOne: "Sun Gard",
        cellContentTwo: "55% discount on all products in the Store.",
        cellContentThree: [
          "Through the Branch:/Show the Insurance Card or Insurance Policy",
        ],
      },
      {
        id: 22,
        cellContentOne: "Dar Habka",
        cellContentTwo:
          "15% discount on all products and services available in the Showroom.",
        cellContentThree: [
          "Through the Branch:/Show the Insurance Card or Insurance Policy",
        ],
      },
      {
        id: 23,
        cellContentOne: "Frdah",
        cellContentTwo: "10% discount on all products.",
        cellContentThree: ["Through the website:/Use the Discount code: TAW"],
      },
      {
        id: 24,
        cellContentOne: "Creative Closets",
        cellContentTwo: "20% discount on all products.",
        cellContentThree: [
          "Through the Branch:/Show the Insurance Card or Insurance Policy",
        ],
      },
      {
        id: 25,
        cellContentOne: "Closet World",
        cellContentTwo: "15% discount on all products",
        cellContentThree: ["Show the Insurance Card"],
      },
      {
        id: 26,
        cellContentOne: "Prince Thobe",
        cellContentTwo: "20% discount on Prince fabrics only,",
        cellContentThree: [
          "Through the Branch:/Show the Insurance Card or Insurance Policy",
        ],
      },
      {
        id: 27,
        cellContentOne: "Spring Rose",
        cellContentTwo: "15% discount on all products.",
        cellContentThree: [
          "Through the Branch:/Show the Insurance Card or Insurance Policy",
        ],
      },
    ],
  };
  return (
    <div className="row ithra-table-container">
      <div className="col-lg-12 col-12">
        <p className="ithra-table-header fw-800 text-center">
          {ithraTableData.headerTitle}
        </p>
      </div>
      <div className="col-lg-12 col-12">
        <div className="ithra-table-box">
          <p className="fs-18 fw-700 ithra-table-title text-center pt-3">
            {ithraTableData.title}
          </p>
          <div className="d-flex flex-row ithra-table-header-layout">
            {ithraTableData?.ithraSubHeaderData?.map((item, i) => {
              return (
                <div className={item.headerDivision} key={i}>
                  <p className="fs-14 fw-800 ithra-header-label px-3">
                    {item.headerContent}
                  </p>
                </div>
              );
            })}
          </div>
          {ithraTableData?.ithraSubContentData?.map((item, i) => {
            return (
              <div
                className="d-flex flex-row ithra-table-content-layout py-2"
                key={i}
              >
                <div className="ithra-cell-one">
                  <p className="m-0 px-3 fs-20 fw-800 ithra-cell-one-para">
                    {item.cellContentOne}
                  </p>
                </div>
                <div className="ithra-cell-two">
                  <p className="m-0 px-3 fs-14 fw-400 ithra-cell-two-para pt-1">
                    {item.cellContentTwo}
                  </p>
                </div>
                <div className="ithra-cell-three">
                  {item?.cellContentThree?.map((items) => {
                    let splitItems = items.split("/").join("</br>");
                    return (
                      <p
                        className="m-0 px-3 fs-13 fw-400 ithra-cell-three-para pt-1"
                        dangerouslySetInnerHTML={{
                          __html: splitItems,
                        }}
                      />
                    );
                  })}
                </div>
              </div>
            );
          })}
          <p className="fs-14 fw-400 tawuniya-partner-condition text-center pt-3">
            * Tawuniya will continue adding new partners to this List
          </p>
        </div>
      </div>
    </div>
  );
};
