import React from "react";
import IthraBannerSection from "../Ithra/IthraBannerSection/IthraBannerSection";
import { AdvantageProgramCard } from "component/Products/CommonProductsComponents/AdvantageProgramCard";
import reliable from "assets/svg/LoyaltyProgram/reliableIcon.svg";
import refill from "assets/svg/LoyaltyProgram/refillMedic.svg";
import pregnant from "assets/svg/LoyaltyProgram/pregnant.svg";
import homeChild from "assets/svg/LoyaltyProgram/childVaccine.svg";
import chronic from "assets/svg/LoyaltyProgram/chronicManage.svg";
import america from "assets/svg/LoyaltyProgram/assistAmerica.svg";
import eligibility from "assets/svg/LoyaltyProgram/eligibility.svg";
import reimbursment from "assets/svg/LoyaltyProgram/reimbursment.svg";
import rejection from "assets/svg/LoyaltyProgram/callBack.svg";
import sms from "assets/svg/LoyaltyProgram/sms.svg";
import "./style.scss";

export const Taj = () => {
  const tajCardHeaderData = {
    title: "We offer you a variety of services that suit your different needs",
    para: null,
  };

  const tajInsurancedvantageCardData = [
    {
      id: 0,
      icon: reliable,
      title: "Reliable Online Consultation",
      para: "This service allows you to book a reliable online consultation appointment with accredited doctors in all health specialties via the smartphone app and website.",
    },
    {
      id: 1,
      icon: refill,
      title: "Re-ﬁll of Chronic Diseases Medication",
      para: "If you have any chronic diseases such as diabetes, pressure, and other conditions. As well as use medication for long periods ranging from one to three months. This service allows you to refill your medicines from an approved pharmacy network without the need to see your doctor. ",
    },
    {
      id: 2,
      icon: pregnant,
      title: "Pregnancy Program",
      para: "We will do our best to make your pregnancy journey full of memorable and beautiful memories through the pregnancy follow-up program that takes care of a pregnant mother and increases her awareness of all procedures and provide tips during all the phases of pregnancy",
    },
    {
      id: 3,
      icon: homeChild,
      title: "Home Children Vaccination",
      para: "For your child's health and in order to ensure they get vaccines. We provide vaccination service at home to Tawuniya members whose age between 0 – 7 years based on the basic vaccinations schedule issued by MOH and included in the Cooperative Health Insurance Uniﬁed Policy published by CCHI. ",
    },
    {
      id: 4,
      icon: chronic,
      title: "Chronic Disease Management",
      para: "If you suffer from any chronic disease, this service allows you to: 1-	Get comprehensive health care from medical consultations to receive the necessary medication. Provide you laboratory services in some cases at your residence. ",
    },
    {
      id: 5,
      icon: america,
      title: "Assist America",
      para: "Our international partner “Assist America,” provide you while you are traveling abroad the Kingdom: -	A medical evacuation. -	Second medical opinion. -	Medical repatriation. -	The return of mortal remains. ",
    },
    {
      id: 6,
      icon: eligibility,
      title: "Eligibility Letter",
      para: "If you have any problem while visiting the approved medical service provider, Tawuniya will issue an eligibility letter, conﬁrming your right to get the necessary treatment. ",
    },
    {
      id: 7,
      icon: reimbursment,
      title: "Medical Reimbursement",
      para: "While you are being treated outside the approved network, and pay to the medical provider. In this case, you can apply for reimbursement of your medical expenses by submitting a request via Tawuniya’s website or Tawuniya App and uploading all necessary support documents. Tawuniya will settle the claim, and the funds will be transferred to your bank account. ",
    },
    {
      id: 8,
      icon: rejection,
      title: "Rejection Call Back",
      para: "Tawuniya team will call you back to clarify medically rejected cases and proposes alternative solutions in the medical conditions that are not covered under the terms of the policy. ",
    },
    {
      id: 9,
      icon: sms,
      title: "SMS Notiﬁcations",
      para: "We follow up on the services provided to you and after registering on the website, and we will send SMSs to conﬁrm your registration and track the medical approvals and claims, as well as vaccination alerts for children. ",
    },
  ];
  return (
    <div>
      <IthraBannerSection />
      <AdvantageProgramCard
        isOddDivision={true}
        classDivision="col px-2 mb-4"
        headerData={tajCardHeaderData}
        advantageCardData={tajInsurancedvantageCardData}
        lightAdvantageHeaderClass="taj-advantage-title fs-20"
        lightAdvantageParaClass="taj-advantage-para fs-14 pr-2"
        isLearnMore={true}
        learnMoreClass="fs-16 mt-auto"
      />
    </div>
  );
};
