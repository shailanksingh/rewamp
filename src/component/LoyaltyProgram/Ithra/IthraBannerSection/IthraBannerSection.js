import React from "react";
import { IthraBanner } from "component/LoyaltyProgram/LoyaltyProgramComponents/IthraBanner";

const IthraBannerSection = () => {
  const ithraBannerData = {
    header: 'The New "Ithra" Program from Tawuniya',
    text: "Enrich your life",
    para: 'Your insurance with Tawuniya protects you, increases your distinction, and gives you benefits that add to your daily life.Tawuniya has developed the ""Ithra"" Program for you, which is the first loyalty program of its kind in the Saudi insurance sector, designed specifically to enrich your life with many benefits and discounts on your purchases from our partners in the various business sectors and more to come, all at special offers for our customers throughout the year.',
  };

  return (
    <div className="row">
      <div className="col-lg-12 col-12 pt-2 pb-5">
        <IthraBanner ithraBannerData={ithraBannerData} />
      </div>
    </div>
  );
};

export default IthraBannerSection;
