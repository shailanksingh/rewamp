import React from "react";
import { TwuniyaAppAdvert } from "component/common/TwuniyaAppAdvert";
import { IthraTable } from "component/LoyaltyProgram/LoyaltyProgramComponents/IthraTable";

const IthraProgramBenificiary = () => {
  return (
    <div className="row">
      <div className="col-lg-12 col-12 pb-5">
        <IthraTable />
      </div>
      <div className="col-lg-12 col-12 pt-2">
        <TwuniyaAppAdvert />
      </div>
    </div>
  );
};

export default IthraProgramBenificiary;
