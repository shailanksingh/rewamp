import React from "react";
import "./style.scss";

const IthraTermsCondition = () => {
  return (
    <div className="row">
      <div className="col-lg-12 col-12 px-5 mx-4">
        <p className="ithra-terms-condition-title">Terms & Conditions</p>
        <ul className="pl-4">
          <li className="ithra-terms-list pl-3">You should have valid insurance policy from Tawuniya.</li>
          <li className="ithra-terms-list pl-3">
            {" "}
            The benefits of Ithra Program are available when purchasing directly
            from our partners.
          </li>
          <li className="ithra-terms-list pl-3">
            To avail of the above discounts, you have to show a proof of your
            insurance at Tawuniya. (You may download Tawuniya Application
            through Apple Store for iOS and Google Play for Android to display
            the insurance policy).
          </li>
        </ul>
      </div>
    </div>
  );
};

export default IthraTermsCondition;
