import React from "react";
import vehicleMaintain from "assets/svg/LoyaltyProgram/vehicleMaintainance.svg";
import sports from "assets/svg/LoyaltyProgram/clubs.svg";
import family from "assets/svg/LoyaltyProgram/familyEntertain.svg";
import delivery from "assets/svg/LoyaltyProgram/deliveryService.svg";
import "./style.scss";

const IthraServiceBenifit = () => {
  const ithraBenifitCardData = [
    {
      id: 0,
      icon: vehicleMaintain,
      title: "Vehicle Maintenance",
    },
    {
      id: 1,
      icon: sports,
      title: "Sport Clubs",
    },
    {
      id: 2,
      icon: family,
      title: "Family Entertainment",
    },
    {
      id: 3,
      icon: delivery,
      title: "Delivery Services & Applications",
    },
  ];
  return (
    <div className="row ithraService-Benifit-Container pb-5">
      <div className="col-lg-12 col-12">
        <div className="d-flex justify-content-center">
          <div>
            <p className="ithraServiceTitle fw-800 text-center m-0 pb-2">
              Services and benefits provided by the "Ithra" program
            </p>
          </div>
        </div>
        <div className="d-flex justify-content-center">
          <div>
            <p className="ithraServicePara fs-15 fw-400 text-center">
              The Ithra Program provides many discounts on a wide range of
              services and products provided by distinguished partners and
              famous brands in the various sectors, including, for example:
            </p>
          </div>
        </div>
      </div>
      <div className="col-lg-12 col-12">
        <div className="ithraBenifitCardLayout d-flex justify-content-center">
          <div>
            <div className="ithraBenifitCard-box">
              <div className="row pt-3 px-3">
                {ithraBenifitCardData.map((item, i) => {
                  return (
                    <div className="col-3 px-2" key={i}>
                      <div className="ithra-benifit-service-card">
                        <img
                          src={item.icon}
                          className="img-fluid mx-auto d-block"
                          alt="icon"
                        />
                        <div className="d-flex justify-content-center pt-3">
                          <div>
                            <p className="ithra-benifit-card-title fs-17 fw-800 text-center m-0">
                              {item.title}
                            </p>
                          </div>
                        </div>
                      </div>
                    </div>
                  );
                })}
              </div>
              <p className="ithra-more text-center pt-4 mt-2 fs-16 fw-400">
                and MORE TO COME...
              </p>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default IthraServiceBenifit;
