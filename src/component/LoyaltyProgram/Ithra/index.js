import React from "react";
import IthraBannerSection from "./IthraBannerSection/IthraBannerSection";
import IthraProgramBenificiary from "./IthraProgramBenificiary/IthraProgramBenificiary";
import IthraServiceBenifit from "./IthraServiceBenifit/IthraServiceBenifit";
import IthraTermsCondition from "./IthraTermsConditions/IthraTermsConditions";

export const Ithra = () => {
  return (
    <React.Fragment>
      <IthraBannerSection />
      <IthraServiceBenifit />
      <IthraProgramBenificiary />
      <IthraTermsCondition />
    </React.Fragment>
  );
};
