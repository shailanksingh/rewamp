import React from "react";
import { HowitWorks } from "../LoyaltyProgramComponents/HowItWorks";
import { DriveFeatures } from "./DriveFeatures";
import { DriveHead } from "./DriveHead";
import { TwuniyaAppAdvert } from "component/common/TwuniyaAppAdvert";

export const Drive = () => {
	return (
		<>
			<DriveHead />
			<DriveFeatures />
			<HowitWorks />
			<TwuniyaAppAdvert />
		</>
	);
};
