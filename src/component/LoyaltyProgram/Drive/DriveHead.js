import React from "react";
import { NormalButton } from "component/common/NormalButton";
import { TintImageCard } from "../LoyaltyProgramComponents/TintImageCard";
import { WhatIsTawuniyaDrive } from "../LoyaltyProgramComponents/WhatIsTawuniyaDrive";
import _TintImage1 from "assets/svg/LoyaltyProgram/_Tint-Image1.svg";
import _TintImage2 from "assets/svg/LoyaltyProgram/_Tint-Image2.svg";
import _TintImage3 from "assets/svg/LoyaltyProgram/_Tint-Image3.svg";
import DriveDevice from "assets/svg/LoyaltyProgram/DriveDevice-034-Final-1.svg";
import buynowArrow from "assets/svg/buynowArrow.svg";
import primaryArrow from "assets/svg/LoyaltyProgram/primaryRightArrow.svg";
import "./style.scss";

const driveTintCardData = [
  {
    id: 1,
    bgImg: _TintImage1,
    position: "justify-content-end",
    content:
      "An engaging and impactful app to improve driving behavior over time",
  },
  {
    id: 2,
    bgImg: _TintImage2,
    position: "justify-content-start",
    content:
      "An engaging and impactful app to improve driving behavior over time",
  },
  {
    id: 1,
    bgImg: _TintImage3,
    position: "justify-content-end",
    content:
      "The collected and enriched data is transformed into a behavioral analysis",
  },
];

export const DriveHead = () => {
  return (
    <div className="row drive-head">
      <div className="col-lg-12 col-12 pb-5 text-center drive-head-text pt-5">
        <div className="d-flex justify-content-center">
          <div>
            <h5 className="get-most-drive-heading fw-800">
              Get the most out of your car Insurance
            </h5>
          </div>
        </div>
        <div className="d-flex justify-content-center">
          <div>
            <p className="get-most-drive-para fs-18 fw-400 text-center m-0">
              Save up to 30% on your car insurance premiums. Download Tawuniya
              Drive stay safe and earn your rewards
            </p>
          </div>
        </div>
        <div className="d-flex justify-content-center">
          <div>
            <div className="drive-button-container">
              <div className="d-flex justify-content-center">
                <div>
                  <div className="d-flex flex-row py-4">
                    <div className="pr-3">
                      <NormalButton
                        label="Claim Your Rewards"
                        className="get-most-claim-btn p-4"
                        adjustIcon="pl-3"
                        needBtnPic={true}
                        src={buynowArrow}
                      />
                    </div>
                    <div>
                      <NormalButton
                        label="Download"
                        className="get-most-download-btn p-4"
                        adjustIcon="pl-3"
                        needBtnPic={true}
                        src={primaryArrow}
                      />
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="drive-head-btn-container"></div>
      <TintImageCard TintImgCardData={driveTintCardData} />
      <WhatIsTawuniyaDrive />
    </div>
  );
};
