import React from "react";
import { ProgramFeatures } from "../LoyaltyProgramComponents/ProgramFeatures";
import drivefeature1 from "assets/svg/LoyaltyProgram/drivefeature1.svg";
import drivefeature2 from "assets/svg/LoyaltyProgram/drivefeature2.svg";
import drivefeature3 from "assets/svg/LoyaltyProgram/drivefeature3.svg";
import drivefeature4 from "assets/svg/LoyaltyProgram/drivefeature4.svg";
import _iPhoneXWhite from "assets/svg/LoyaltyProgram/_iPhoneXWhite.svg";

const DriveFeaturesData = [
	{
		title: "Drive features",
		subTitle:
			"An engaging and impactful app to improve driving behavior over time",
		iconCardDataLft: [
			{
				id: 1,
				cardIcon: drivefeature1,
				cardContent: "Up to 20% discount on car insurance",
			},
			{
				id: 2,
				cardIcon: drivefeature2,
				cardContent: "Weekly active rewards",
			},
		],

		imageCenter: _iPhoneXWhite,

		iconCardDataRt: [
			{
				id: 1,
				cardIcon: drivefeature3,
				cardContent: "Safer driving habits",
			},
			{
				id: 2,
				cardIcon: drivefeature4,
				cardContent: "Safety features",
			},
		],
	},
];

export const DriveFeatures = () => {
	return (
		<div>
			<ProgramFeatures programFeaturesData={DriveFeaturesData} />
		</div>
	);
};
