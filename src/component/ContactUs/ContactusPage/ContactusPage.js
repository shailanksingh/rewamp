import React, { useEffect, useState } from "react";

import {
	Card,
	Form,
	InputGroup,
	FormControl,
	Dropdown,
	DropdownButton,
	Button,
	FloatingLabel,
} from "react-bootstrap";

import Phone from "../../../assets/svg/phone.svg";
import Line from "../../../assets/svg/line.svg";
import Email from "../../../assets/svg/email.svg";
import Communication from "../../../assets/svg/communication.svg";
import Fax from "../../../assets/svg/Social/fax.svg";
import KSAFlagImage from "assets/images/ksaFlag.png";
import IndiaFlagImage from "assets/images/indiaFlag.png";
import Location from "../../../assets/svg/location.svg";

import { NormalRadioButton } from "component/common/NormalRadioButton";
import PhoneNumberInput from "component/common/PhoneNumberInput/index";
import { getBranchDetails } from 'service/contactUsService'
import "./style.scss";
import GoogleMapComp from "component/common/GoogleMapComp";

export const ContactusPage = () => {
	let dialingCodes = [
		{
			code: "+91",
			image: IndiaFlagImage,
		},
		{
			code: "+966",
			image: KSAFlagImage,
		},
	];

	let [selectedCode, setSelectedCode] = useState(dialingCodes[0]);

	const [highlight, setHighlight] = useState("highlight");

	const [highlight1, setHighlight1] = useState("");
	const [highlight2, setHighlight2] = useState("");
	const [highlight3, setHighlight3] = useState("");
	const [highlight4, setHighlight4] = useState("");
	const [highlight5, setHighlight5] = useState("");
	const [highlight6, setHighlight6] = useState("");
	const [highlight7, setHighlight7] = useState("");
	const [highlight8, setHighlight8] = useState("");
	const [highlight9, setHighlight9] = useState("");

	const [setclassname1, setClassname1] = useState("inactiveradio");
	const [setclassname2, setClassname2] = useState("inactiveradio");
	const [setclassname3, setClassname3] = useState("inactiveradio");
	const [setclassname4, setClassname4] = useState("inactiveradio");
	const [setclassname5, setClassname5] = useState("inactiveradio");

	const [radioBtn, setRadioBtn] = useState('All Offices')

	const [setenquiryradio1, setEnquiryRadio1] = useState(false);
	const [setenquiryradio2, setEnquiryRadio2] = useState(false);
	const [setenquiryradio3, setEnquiryRadio3] = useState(false);
	const [setenquiryradio4, setEnquiryRadio4] = useState(false);
	const [setenquiryradio5, setEnquiryRadio5] = useState(false);
	const [changebgcolor, setChangebgcolor] = useState("");
	const [checknav, setChecknav] = useState(false);

	let [phoneNumber, setPhoneNumber] = useState("");
	const [address, setAddress] = useState(
		"King Fahad road - Gulf Monetary Council Building Riyadh - Ground floor"
	);
	const [regions, setRegions] = useState(['All Regions'])
	const [cities, setCities] = useState(['All Cities'])
	const [allBranches, setAllBranches] = useState([])
	const [branches, setBranches] = useState([])
	const [selectedRegion, setSelectedRegion] = useState('All Regions')
	const [selectedCity, setSelectedCity] = useState('All Cities')

	useEffect(() => {
		fetchBranchDetails()
	}, [])

	const fetchBranchDetails = async () => {
		try {
			const data = await getBranchDetails()
			if(Boolean(data.length)) {
				const rgns = ['All Regions']
				const cts = ['All Cities']
				data.forEach(item => {
					if(!rgns.includes(item.area)) rgns.push(item.area)
					if(!cts.includes(item.city)) cts.push(item.city)
				})
				setRegions(rgns)
				setCities(cts)
				setAllBranches(data)
				setBranches(data)
			}			
		} catch (error) {
		}
	}

	const handlechange = (e) => {
		setRadioBtn(e.target.value);
		if (e.target.value === "All Offices") {
			if(selectedRegion !== "All Regions" && selectedCity !== "All Cities") {
				const fBranches = allBranches.filter(branch => branch.area === selectedRegion && branch.city === selectedCity)
				setBranches(fBranches)
			} else if (selectedRegion !== "All Regions") {
				const fBranches = allBranches.filter(branch => branch.area === selectedRegion)
				setBranches(fBranches)
			} else if (selectedCity !== "All Cities") {
				const fBranches = allBranches.filter(branch => branch.city === selectedCity)
				setBranches(fBranches)
			} else {
				setBranches(allBranches)
			}
		}
		if (e.target.value === "Sales Office") {
			if(selectedRegion !== "All Regions" && selectedCity !== "All Cities") {
				const fBranches = allBranches.filter(branch => 
					branch.type === "Sales Office" && branch.area === selectedRegion && branch.city === selectedCity)
				setBranches(fBranches)
			} else if (selectedRegion !== "All Regions") {
				const fBranches = allBranches.filter(branch => branch.type === "Sales Office" && branch.area === selectedRegion)
				setBranches(fBranches)
			} else if (selectedCity !== "All Cities") {
				const fBranches = allBranches.filter(branch => branch.type === "Sales Office" && branch.city === selectedCity)
				setBranches(fBranches)
			} else {
				const fBranches = allBranches.filter(branch => branch.type === "Sales Office")
				setBranches(fBranches)
			}
		}
		if (e.target.value === "Head Office") {
			if(selectedRegion !== "All Regions" && selectedCity !== "All Cities") {
				const fBranches = allBranches.filter(branch => 
					branch.type === "Head Office" && branch.area === selectedRegion && branch.city === selectedCity)
				setBranches(fBranches)
			} else if (selectedRegion !== "All Regions") {
				const fBranches = allBranches.filter(branch => branch.type === "Head Office" && branch.area === selectedRegion)
				setBranches(fBranches)
			} else if (selectedCity !== "All Cities") {
				const fBranches = allBranches.filter(branch => branch.type === "Head Office" && branch.city === selectedCity)
				setBranches(fBranches)
			} else {
				const fBranches = allBranches.filter(branch => branch.type === "Head Office")
				setBranches(fBranches)
			}
		}
		if (e.target.value === "Claim Office") {
			if(selectedRegion !== "All Regions" && selectedCity !== "All Cities") {
				const fBranches = allBranches.filter(branch => 
					branch.type === "Claim Office" && branch.area === selectedRegion && branch.city === selectedCity)
				setBranches(fBranches)
			} else if (selectedRegion !== "All Regions") {
				const fBranches = allBranches.filter(branch => branch.type === "Claim Office" && branch.area === selectedRegion)
				setBranches(fBranches)
			} else if (selectedCity !== "All Cities") {
				const fBranches = allBranches.filter(branch => branch.type === "Claim Office" && branch.city === selectedCity)
				setBranches(fBranches)
			} else {
				const fBranches = allBranches.filter(branch => branch.type === "Claim Office")
				setBranches(fBranches)
			}
		}
	};

	const handleRegionChange = e => {
		setSelectedRegion(e.target.value)
		if(e.target.value === "All Regions") {
			if(radioBtn !== 'All Offices' && selectedCity !== 'All Cities') {
				const fBranches = allBranches.filter(branch => branch.type === radioBtn && branch.city === selectedCity)
				setBranches(fBranches)
			} else if(radioBtn !== 'All Offices') {
				const fBranches = allBranches.filter(branch => branch.type === radioBtn)
				setBranches(fBranches)
			} else if(selectedCity !== 'All Cities') {
				const fBranches = allBranches.filter(branch => branch.city === selectedCity)
				setBranches(fBranches)
			} else {
				setBranches(allBranches)
			}
		} else {
			if(radioBtn !== 'All Offices' && selectedCity !== 'All Cities') {
				const fBranches = allBranches.filter(branch => 
					branch.area === e.target.value && branch.type === radioBtn && branch.city === selectedCity)
				setBranches(fBranches)
			} else if(radioBtn !== 'All Offices') {
				const fBranches = allBranches.filter(branch => branch.area === e.target.value && branch.type === radioBtn)
				setBranches(fBranches)
			} else if(selectedCity !== 'All Cities') {
				const fBranches = allBranches.filter(branch => branch.area === e.target.value && branch.city === selectedCity)
				setBranches(fBranches)
			} else {
				const fBranches = allBranches.filter(branch => branch.area === e.target.value)
				setBranches(fBranches)
			}
		}
	}

	const handleCityChange = e => {
		setSelectedCity(e.target.value)
		if(e.target.value === "All Cities") {
			if(radioBtn !== 'All Offices' && selectedRegion !== 'All Regions') {
				const fBranches = allBranches.filter(branch => branch.type === radioBtn && branch.area === selectedRegion)
				setBranches(fBranches)
			} else if(radioBtn !== 'All Offices') {
				const fBranches = allBranches.filter(branch => branch.type === radioBtn)
				setBranches(fBranches)
			} else if(selectedRegion !== 'All Regions') {
				const fBranches = allBranches.filter(branch => branch.area === selectedRegion)
				setBranches(fBranches)
			} else {
				setBranches(allBranches)
			}
		} else {
			if(radioBtn !== 'All Offices' && selectedRegion !== 'All Regions') {
				const fBranches = allBranches.filter(branch => 
					branch.city === e.target.value && branch.type === radioBtn && branch.area === selectedRegion)
				setBranches(fBranches)
			} else if(radioBtn !== 'All Offices') {
				const fBranches = allBranches.filter(branch => branch.city === e.target.value && branch.type === radioBtn)
				setBranches(fBranches)
			} else if(selectedRegion !== 'All Regions') {
				const fBranches = allBranches.filter(branch => branch.city === e.target.value && branch.area === selectedRegion)
				setBranches(fBranches)
			} else {
				const fBranches = allBranches.filter(branch => branch.city === e.target.value)
				setBranches(fBranches)
			}
		}
	}

	const handleradiochange = (e) => {
		console.log("herer", e.target.value);
		if (e.target.value == 1) {
			setClassname1("");
			setClassname2("inactiveradio");
			setClassname3("inactiveradio");
			setClassname4("inactiveradio");
			setClassname5("inactiveradio");
			setEnquiryRadio1(true);
			setEnquiryRadio2(false);
			setEnquiryRadio3(false);
			setEnquiryRadio4(false);
			setEnquiryRadio5(false);
		}
		if (e.target.value == 2) {
			setClassname1("inactiveradio");
			setClassname2("");
			setClassname3("inactiveradio");
			setClassname4("inactiveradio");
			setClassname5("inactiveradio");

			setEnquiryRadio1(false);
			setEnquiryRadio2(true);
			setEnquiryRadio3(false);
			setEnquiryRadio4(false);
			setEnquiryRadio5(false);
		}
		if (e.target.value == 3) {
			setClassname1("inactiveradio");
			setClassname2("inactiveradio");
			setClassname3("");
			setClassname4("inactiveradio");
			setClassname5("inactiveradio");
			setEnquiryRadio1(false);
			setEnquiryRadio2(false);
			setEnquiryRadio3(true);
			setEnquiryRadio4(false);
			setEnquiryRadio5(false);
		}
		if (e.target.value == 4) {
			setClassname1("inactiveradio");
			setClassname2("inactiveradio");
			setClassname3("inactiveradio");
			setClassname4("");
			setClassname5("inactiveradio");

			setEnquiryRadio1(false);
			setEnquiryRadio2(false);
			setEnquiryRadio3(false);
			setEnquiryRadio4(true);
			setEnquiryRadio5(false);
		}
		if (e.target.value == 5) {
			setClassname1("inactiveradio");
			setClassname2("inactiveradio");
			setClassname3("inactiveradio");
			setClassname4("inactiveradio");
			setClassname5("");
			setEnquiryRadio1(false);
			setEnquiryRadio2(false);
			setEnquiryRadio3(false);
			setEnquiryRadio4(false);
			setEnquiryRadio5(true);
		}
	};

	const handlehighlight = (e) => {
		console.log(e.target.innerText);
		if (e.target.innerText === "HEAD QUARTER") {
			setHighlight1("highlight");
			setHighlight2("");
			setHighlight3("");
			setHighlight4("");
			setHighlight5("");
			setHighlight6("");
			setChangebgcolor("");
			setChecknav(false);
		}
		if (e.target.innerText === "CUSTOMERS") {
			setHighlight1("");
			setHighlight2("highlight");
			setHighlight3("");
			setHighlight4("");
			setHighlight5("");
			setHighlight6("");
			setChangebgcolor("cardbg_change");
			setChecknav(true);
		}
		if (e.target.innerText === "INVESTORS") {
			setHighlight1("");
			setHighlight2("");
			setHighlight3("highlight");
			setHighlight4("");
			setHighlight5("");
			setHighlight6("");
			setChangebgcolor("");
			setChecknav(false);
		}
		if (e.target.innerText === "AGENT & BROKERS") {
			setHighlight1("");
			setHighlight2("");
			setHighlight3("");
			setHighlight4("highlight");
			setHighlight5("");
			setHighlight6("");
			setChangebgcolor("");
			setChecknav(false);
		}
		if (e.target.innerText === "CAREERS") {
			setHighlight1("");
			setHighlight2("");
			setHighlight3("");
			setHighlight4("");
			setHighlight5("highlight");
			setHighlight6("");
			setChangebgcolor("");
			setChecknav(false);
		}
		if (e.target.innerText === "MEDIA") {
			setHighlight1("");
			setHighlight2("");
			setHighlight3("");
			setHighlight4("");
			setHighlight5("");
			setHighlight6("highlight");
			setChangebgcolor("");
			setChecknav(false);
		}

		if (e.target.innerText === "RIYADH") {
			setHighlight7("highlight");
			setHighlight8("");
			setHighlight9("");
			setAddress(
				"King Fahad road - Gulf Monetary Council Building Riyadh - Ground floor"
			);
		}

		if (e.target.innerText === "JEDDAH") {
			setHighlight7("");
			setHighlight8("highlight");
			setHighlight9("");
			setAddress(
				"Al Madina Almonawarh Road - Alkhorayaf Center - the third floor"
			);
		}

		if (e.target.innerText === "DAMMAM") {
			setHighlight7("");
			setHighlight8("");
			setHighlight9("highlight");
			setAddress("King Fahad road - Almajdouie Tower - the sixth floor ");
		}
	};

	return (
		<React.Fragment>
			<div className="maincontact">
				<div className=" pt-5 ">
					<h2 className="fw-800 text-center">Contacting Tawuniya</h2>
					<div className="row">
						<div className="col-lg-8  aligncenterdiv">
							<p className="contactptag text-center">
								Our team was handpicked for their passion for insurance products
								and services. Whether you are browsing our site or visiting our
								branches, we are always willing to share our deep knowledge. The
								most commonly asked questions are covered in our
								<span className="textclr"> Customer Service</span>.
							</p>
							<p className="contactptag text-center">
								If you have any specific questions please do not hesitate to
								contact us by completing{" "}
								<span className="textclr">Enquiry</span> form or calling our
								customer services team.
							</p>
						</div>
					</div>

					<div className="row justify-content-center pt-4">
						<div className="col-lg-1">
							<p
								className="contactptagalign fs-14 "
								id={highlight1}
								onClick={handlehighlight}
							>
								HEAD QUARTER
							</p>
						</div>
						<div className="col-lg-1">
							<p
								className="fs-14 pl-2 contactptag contactpta-sm "
								onClick={handlehighlight}
								id={highlight2}
							>
								CUSTOMERS
							</p>
						</div>
						<div className="col-lg-1 ">
							<p
								className="fs-14 contactptag pl-2"
								onClick={handlehighlight}
								id={highlight3}
							>
								INVESTORS
							</p>
						</div>
						<div className="col-lg-1 tab-ptag">
							<p
								className="fs-14 contactptag agent"
								onClick={handlehighlight}
								id={highlight4}
							>
								AGENT & BROKERS
							</p>
						</div>
						<div className="col-lg-1">
							<p
								className="fs-14 pl-4 contactptag carrer-ptag"
								onClick={handlehighlight}
								id={highlight5}
							>
								CAREERS
							</p>
						</div>
						<div className="col-lg-1">
							<p
								className="fs-14 contactptag pl-0 media-ptag"
								onClick={handlehighlight}
								id={highlight6}
							>
								MEDIA
							</p>
						</div>
					</div>

					{/* --main card---start----- */}
					<div className="pt-5">
						<Card className="maincard mb-5">
							<GoogleMapComp branches={branches} />
							{/* ---------- inner black card----- */}
							<div
								className="container social-card-containercustom"
								// style={{
								// 	display: "flex",
								// 	flexDirection: "column",
								// 	alignItems: "center",
								// 	justifyContent: "center",
								// }}
							>
								<Card className="social-card4 " id={changebgcolor}>
									{checknav ? (
										<>
											<div className="container-fluid">
												<div className="row">
													<div className="col-lg-0 pt-2 align_phone">
														<div className="container">
															<img src={Phone} />
														</div>
													</div>

													<div className="col-lg-2">
														<p className="mainBannerPara2 medical-ptag pt-3">
															Call Center
														</p>
														<h6 className=" mainBannerHeading2">
															800 124 9990
														</h6>
													</div>

													<div className="col-lg-0 pt-3 align_phone">
														<div className="container">
															<img src={Fax} />
														</div>
													</div>

													<div className="col-lg-2">
														<p className="mainBannerPara2 medical-ptag pt-3">
															Whatsapp
														</p>
														<h6 className="mainBannerHeading2">9200 19990</h6>
														<p className="mainBannerPara2 chat_width fs-12 pt-3">
															This is chat only number
														</p>
													</div>

													<div className="col-lg-0 pt-3">
														<div className="container">
															<p className="mainBannerPara2 medical-ptag">
																Chat with our executive
															</p>
															<h6 className="mainBannerHeading2">
																Start Live Chat{" "}
																<label className="online_badge">Online</label>
															</h6>
														</div>
													</div>

													<div className="col-lg-0 pt-3">
														<div className="container">
															<img src={Communication} />
														</div>
													</div>

													<div className="col-lg-3 pr-2 ">
														<p className="mainBannerPara2 pt-3"> Email</p>

														<h6 className="mainBannerHeading2">
															Care@tawuniya.com.sa{" "}
														</h6>
													</div>
												</div>

												<hr className="hrtag2" />

												<div className="row">
													<div className="col-lg-5">
														<h6 className="mainBannerHeading2">
															Want to know more about out network ?
														</h6>
													</div>

													<div className="col-lg-6 pb-1 pl-2">
														<Button className="ournetworkbtn ">
															Our Network
														</Button>
													</div>
												</div>
											</div>
										</>
									) : (
										<>
											<div className="container-fluid">
												<div className="row">
													<div className="col-lg-0 pt-3">
														<div className="container">
															<img src={Phone} />
														</div>
													</div>

													<div className="col-lg-3">
														<p className="mainBannerPara medical-ptag pt-3">
															Phone
														</p>
														<h6 className="mainBannerHeading">
															+966 11 252 5800
														</h6>
													</div>

													<div className="col-lg-0 pt-3 fax_align">
														<div className="container">
															<img src={Fax} />
														</div>
													</div>

													<div className="col-lg-3 ">
														<p className="mainBannerPara medical-ptag pt-3">
															Fax
														</p>
														<h6 className="mainBannerHeading">
															+966 11 400 0844
														</h6>
													</div>

													<div className="col-lg-0 pt-3">
														<div className="container">
															<img src={Communication} />
														</div>
													</div>

													<div className="col-lg-4  emailui">
														<p className="mainBannerPara pt-3"> Email</p>

														<h6 className="mainBannerHeading">
															Care@tawuniya.com.sa{" "}
														</h6>
													</div>
												</div>

												<hr className="hrtag" />

												<div className="row">
													<div className="col-lg-0 pt-1">
														<div className="container">
															<img src={Fax} />
														</div>
													</div>

													<div className="col-lg-2">
														<p className="mainBannerPara medical-ptag pt-1">
															P.O.Box
														</p>
														<h6 className="mainBannerHeading">86959</h6>
													</div>

													<div className="col-lg-0 pt-1">
														<div className="container">
															<img src={Location} />
														</div>
													</div>

													<div className="col-lg-7 pb-1">
														<p className="mainBannerPara medical-ptag pt-1">
															Address
														</p>
														<h6 className="mainBannerHeading">
															6507 Thomamah Road (Takhassusi) - Ar Rabi, Riyadh
															11632
														</h6>
													</div>
												</div>
											</div>
										</>
									)}
								</Card>
							</div>
							{/* ------------- */}

							{/* -------filter checkbox card start */}

							<div className="container-fluid pt-4 filter-card-containercustom">
								<Card className="filter-card pb-4">
									<div className="row">
										<div className=" col-lg-2  pt-4">
											<select class="selectdropdown"
												value={selectedRegion}
												onChange={handleRegionChange}>
												{regions.map(region => <option value={region}>{region}</option>)}
											</select>
										</div>

										<div className=" col-lg-2 pt-4">
											<select class="selectdropdown cityalign "
												value={selectedCity}
												onChange={handleCityChange}>
												{cities.map(city => <option value={city}>{city}</option>)}
											</select>
										</div>

										<div className="col-1.5 pt-4 mt-3 alignradio ">
											<NormalRadioButton
												type="radio"
												name="All Offices"
												value="All Offices"
												radioValue="All Offices"
												onChange={handlechange}
												checked={radioBtn === "All Offices"}
											/>
										</div>
										<div className="col-lg-1.5 mt-3 pl-4 pt-4   ">
											<NormalRadioButton
												type="radio"
												name="Sales Office"
												value="Sales Office"
												onChange={handlechange}
												radioValue="Sales Office"
												checked={radioBtn === "Sales Office"}
											/>
										</div>

										<div className="col-lg-1.5 mt-3 pl-4 pt-4 ">
											<NormalRadioButton
												type="radio"
												name="Head Office"
												value="Head Office"
												onChange={handlechange}
												radioValue="Head Office"
												checked={radioBtn === "Head Office"}
											/>
										</div>

										<div className="col-lg-1.5 mt-3 pl-4 pt-4 ">
											<NormalRadioButton
												type="radio"
												onChange={handlechange}
												name="Claims Office"
												value="Claim Office"
												radioValue="Claims Office"
												checked={radioBtn === "Claim Office"}
											/>
										</div>
									</div>
								</Card>
							</div>

							{/* filter checkbox card end */}
						</Card>

						{/* ------------3rd card start */}

						<div className="container pb-5">
							<Card className="form-cardnew ">
								<h2 className="pt-5 text-center fw-800">Inquiry Form</h2>
								<p className="text-center">Let us know how can we reach you</p>

								<div className="container  formbg">
									<hr className="alignhr" />
									<h6 className="fw-800">Personal Details</h6>

									<div className="row pt-3">
										<div className="col-lg-6">
											<Form>
												<Form.Group
													className="mb-3 inputbg"
													controlId="formBasicEmail"
												>
													<Form.Control type="email" placeholder="First Name" />
												</Form.Group>
											</Form>
										</div>

										<div className="col-lg-6">
											<Form>
												<Form.Group
													className="mb-3 inputbg"
													controlId="formBasicEmail"
												>
													<Form.Control type="email" placeholder="Last Name" />
												</Form.Group>
											</Form>
										</div>

										<div className="col-lg-10 ">
											<InputGroup className="mb-3">
												<img
													src={IndiaFlagImage}
													className="imageflag"
													alt="flag"
												/>
												<DropdownButton
													variant="outline-secondary"
													title="+966"
													id="input-group-dropdown-1"
													className="dropdownhg"
												>
													<Dropdown.Item href="#">
														<img
															src={IndiaFlagImage}
															className="image"
															alt="flag"
														/>{" "}
														+966
													</Dropdown.Item>
													<Dropdown.Item href="#">
														<img
															src={KSAFlagImage}
															className="image"
															alt="flag"
														/>{" "}
														+91
													</Dropdown.Item>
													<Dropdown.Divider />
												</DropdownButton>
												<FormControl placeholder="ex: 5xxxxxxxx" />
											</InputGroup>
										</div>

										<div className="col-lg-2">
											<Button className="sendoptbtn">Send OTP</Button>
										</div>

										<div className="col-lg-12">
											<InputGroup className="mb-3">
												<InputGroup.Text id="basic-addon1">
													{" "}
													<img src={Email} />
												</InputGroup.Text>
												<FormControl
													placeholder="ex: email@tawuniya.com.sa"
													aria-label="Username"
													aria-describedby="basic-addon1"
												/>
											</InputGroup>
										</div>

										<div className="col-lg-12">
											<p className="fw-500">Preferred way to contact you</p>
										</div>

										<div className="col-lg-6 ">
											<InputGroup className={setclassname1}>
												<FormControl placeholder="Email" />
												<InputGroup.Radio
													checked={setenquiryradio1}
													value="1"
													onClick={handleradiochange}
													aria-label="Radio button for following text input"
												/>
											</InputGroup>
										</div>

										<div className="col-lg-6 ">
											<InputGroup className={setclassname2}>
												<FormControl placeholder="Phone/Mobile Number" />
												<InputGroup.Radio
													checked={setenquiryradio2}
													value="2"
													onClick={handleradiochange}
													aria-label="Radio button for following text input"
												/>
											</InputGroup>
										</div>

										<div className="col-lg-12">
											<hr />
											<h5 className="fw-800">Reason of Contact</h5>
										</div>

										<div className="col-lg-4 ">
											<InputGroup className={setclassname3}>
												<FormControl placeholder="Inquiry" />
												<InputGroup.Radio
													checked={setenquiryradio3}
													value="3"
													onClick={handleradiochange}
													aria-label="Radio button for following text input"
												/>
											</InputGroup>
										</div>
										<div className="col-lg-4 ">
											<InputGroup className={setclassname4}>
												<FormControl placeholder="Suggestion" />
												<InputGroup.Radio
													checked={setenquiryradio4}
													value="4"
													onClick={handleradiochange}
													aria-label="Radio button for following text input"
												/>
											</InputGroup>
										</div>

										<div className="col-lg-4 ">
											<InputGroup className={setclassname5}>
												<FormControl placeholder="Question" />
												<InputGroup.Radio
													className="radiobg"
													checked={setenquiryradio5}
													value="5"
													onClick={handleradiochange}
													aria-label="Radio button for following text input"
												/>
											</InputGroup>
										</div>

										<div className="col-lg-12">
											<hr />
										</div>

										<div className="col-lg-12 ">
											<InputGroup>
												<FormControl placeholder="Subject" />
											</InputGroup>
										</div>

										<div className="col-lg-12 pt-4">
											<p>Your Message</p>
											<Form.Control
												as="textarea"
												placeholder="Type here.."
												style={{ height: "150px" }}
											/>
										</div>

										<div className="col-lg-12">
											<hr />
										</div>

										<div className="col-lg-12 pt-3 pb-5">
											<Button className="sendbtn">Send</Button>
										</div>
									</div>
								</div>
							</Card>
						</div>

						{/* 3card end */}
					</div>

					{/* main card end */}
				</div>

				<div className="text-center pb-5">
					<h3 className="fw-800">Committee of Insurance Disputes</h3>
					<p>Committee for the resolution of insurance disputes</p>
					<div className="row justify-content-center pt-5">
						<div className="col-lg-1">
							<p
								className="contactptagalign fs-16 "
								id={highlight7}
								onClick={handlehighlight}
							>
								RIYADH
							</p>
						</div>
						<div className="col-lg-1 align_left">
							<p
								className="fs-16 pl-2 contactptag"
								id={highlight8}
								onClick={handlehighlight}
							>
								JEDDAH
							</p>
						</div>

						<div className="col-lg-1">
							<p
								className="fs-16 pl-2 contactptag"
								id={highlight9}
								onClick={handlehighlight}
							>
								DAMMAM
							</p>
						</div>
					</div>
				</div>
				<div className="container pb-5 ">
					<Card className="social-card5 ">
						<div className="container-fluid">
							<div className="row">
								<div className="col-lg-0 pt-3">
									<div className="container">
										<img src={Phone} />
									</div>
								</div>

								<div className="col-lg-3">
									<p className="mainBannerPara medical-ptag pt-3">Phone</p>
									<h6 className="mainBannerHeading">+966 11 252 5800</h6>
								</div>

								<div className="col-lg-0 pt-3">
									<div className="container">
										<img src={Fax} />
									</div>
								</div>

								<div className="col-lg-3">
									<p className="mainBannerPara medical-ptag pt-3">Fax</p>
									<h6 className="mainBannerHeading">+966 11 400 0844</h6>
								</div>

								<div className="col-lg-0 pt-3">
									<div className="container">
										<img src={Communication} />
									</div>
								</div>

								<div className="col-lg-4 pb-3 emailui">
									<p className="mainBannerPara pt-3"> Email</p>

									<h6 className="mainBannerHeading">Care@tawuniya.com.sa </h6>
								</div>
							</div>

							<hr className="hrtag mt-0" />

							<div className="row">
								<div className="col-lg-0 pt-1">
									<div className="container">
										<img src={Fax} />
									</div>
								</div>

								<div className="col-lg-2">
									<p className="mainBannerPara medical-ptag pt-1">P.O.Box</p>
									<h6 className="mainBannerHeading">86959</h6>
								</div>

								<div className="col-lg-0 pt-1">
									<div className="container">
										<img src={Location} />
									</div>
								</div>

								<div className="col-lg-8 pb-1">
									<p className="mainBannerPara medical-ptag pt-1">Address</p>
									<h6 className="mainBannerHeading">{address}</h6>
								</div>
							</div>
						</div>
					</Card>
				</div>

				{/* --- */}
			</div>
		</React.Fragment>
	);
};
