import React, { useState } from "react";
import "./style.scss";
import {
  Form,
  InputGroup,
  FormControl,
  Dropdown,
  DropdownButton,
  Button,
} from "react-bootstrap";

import Email from "assets/svg/email.svg";
import KSAFlagImage from "assets/images/ksaFlag.png";
import IndiaFlagImage from "assets/images/indiaFlag.png";
import get_direction from "assets/images/mobile/get_direction.svg";
import map_filter from "assets/images/mobile/map_filter.svg";
import HeaderStickyMenu from "component/common/MobileReuseable/HeaderStickyMenu";
import SocialResponsibilityContactUs from "component/common/MobileReuseable/SocialResponsibilityContact";
import FooterMobile from "component/common/MobileReuseable/FooterMobile";
import MobileContactForm from "component/common/MobileContactForm";

import "../style.scss";
import AnimatedBottomPopup from "component/common/MobileReuseable/AnimatedBottomPopup";
import ConfirmationWoman from "assets/images/mobile/ConfirmationWoman.png";
import { history } from "service/helpers";
import BottomPopup from "component/common/MobileReuseable/BottomPopup";
import UpdateVerification from "component/MyProfile/Mobile/UpdateVerification";

const tagList = [
  {
    id: 1,
    label: "Head Quarter",
  },
  {
    id: 2,
    label: "Customer",
  },
  {
    id: 3,
    label: "Investors",
  },
  {
    id: 4,
    label: "Agent & Brokers",
  },
];
const ContactUsMobile = () => {
  let dialingCodes = [
    {
      code: "+91",
      image: IndiaFlagImage,
    },
    {
      code: "+966",
      image: KSAFlagImage,
    },
  ];

  let [selectedCode, setSelectedCode] = useState(dialingCodes[0]);

  const [setclassname1, setClassname1] = useState("inactiveradio");
  const [setclassname2, setClassname2] = useState("inactiveradio");
  const [setclassname3, setClassname3] = useState("inactiveradio");
  const [setclassname4, setClassname4] = useState("inactiveradio");
  const [setclassname5, setClassname5] = useState("inactiveradio");

  const [setradio1, setRadio1] = useState(false);
  const [setradio2, setRadio2] = useState(false);
  const [setradio3, setRadio3] = useState(false);
  const [setradio4, setRadio4] = useState(false);

  const [setenquiryradio1, setEnquiryRadio1] = useState(false);
  const [setenquiryradio2, setEnquiryRadio2] = useState(false);
  const [setenquiryradio3, setEnquiryRadio3] = useState(false);
  const [setenquiryradio4, setEnquiryRadio4] = useState(false);
  const [setenquiryradio5, setEnquiryRadio5] = useState(false);

  let [phoneNumber, setPhoneNumber] = useState("");
  const [open, setOpen] = useState(false);

  const handlechange = (e) => {
    if (e.target.value == 1) {
      setRadio1(true);

      setRadio2(false);
      setRadio3(false);
      setRadio4(false);
    }
    if (e.target.value == 2) {
      setRadio1(false);
      setRadio2(true);
      setRadio3(false);
      setRadio4(false);
    }
    if (e.target.value == 3) {
      setRadio1(false);
      setRadio2(false);
      setRadio3(true);
      setRadio4(false);
    }
    if (e.target.value == 4) {
      setRadio1(false);
      setRadio2(false);
      setRadio3(false);
      setRadio4(true);
    }
  };

  const handleradiochange = (e) => {
    console.log("herer", e.target.value);
    if (e.target.value == 1) {
      setClassname1("");
      setClassname2("inactiveradio");
      setClassname3("inactiveradio");
      setClassname4("inactiveradio");
      setClassname5("inactiveradio");
      setEnquiryRadio1(true);
      setEnquiryRadio2(false);
      setEnquiryRadio3(false);
      setEnquiryRadio4(false);
      setEnquiryRadio5(false);
    }
    if (e.target.value == 2) {
      setClassname1("inactiveradio");
      setClassname2("");
      setClassname3("inactiveradio");
      setClassname4("inactiveradio");
      setClassname5("inactiveradio");

      setEnquiryRadio1(false);
      setEnquiryRadio2(true);
      setEnquiryRadio3(false);
      setEnquiryRadio4(false);
      setEnquiryRadio5(false);
    }
    if (e.target.value == 3) {
      setClassname1("inactiveradio");
      setClassname2("inactiveradio");
      setClassname3("");
      setClassname4("inactiveradio");
      setClassname5("inactiveradio");
      setEnquiryRadio1(false);
      setEnquiryRadio2(false);
      setEnquiryRadio3(true);
      setEnquiryRadio4(false);
      setEnquiryRadio5(false);
    }
    if (e.target.value == 4) {
      setClassname1("inactiveradio");
      setClassname2("inactiveradio");
      setClassname3("inactiveradio");
      setClassname4("");
      setClassname5("inactiveradio");

      setEnquiryRadio1(false);
      setEnquiryRadio2(false);
      setEnquiryRadio3(false);
      setEnquiryRadio4(true);
      setEnquiryRadio5(false);
    }
    if (e.target.value == 5) {
      setClassname1("inactiveradio");
      setClassname2("inactiveradio");
      setClassname3("inactiveradio");
      setClassname4("inactiveradio");
      setClassname5("");
      setEnquiryRadio1(false);
      setEnquiryRadio2(false);
      setEnquiryRadio3(false);
      setEnquiryRadio4(false);
      setEnquiryRadio5(true);
    }
  };
  const [activeTagLabel, setActiveTagLabel] = useState(1);
  const dialogStyles = {
    dialogPaper: {
      minHeight: "80vh",
      maxHeight: "80vh",
    },
  };
  const [isverficationModel, setIsverficationModel] = useState(false);
  return (
    <div className="contact_us_container">
      <HeaderStickyMenu />
      <div className="contact_header">
        <h5>Contacting Tawuniya</h5>
        <p>
          Our team was handpicked for their passion for insurance products and
          services. Whether you are browsing our site or visiting our branches,
          we are always willing to share our deep knowledge. The most commonly
          asked questions are covered in our <span>Customer Service.</span>
          <br />
          If you have any specific questions please do not hesitate to contact
          us by completing <span> Enquiry</span> form or calling our customer
          services team.
        </p>
      </div>
      <div className="tags_scrollable">
        {tagList?.map((val) => {
          return (
            <label
              className={val.id === activeTagLabel && "active_tag"}
              onClick={() => setActiveTagLabel(val.id)}
            >
              {val.label}
            </label>
          );
        })}
      </div>
      <SocialResponsibilityContactUs />
      <div className="map_section">
        <h6>Customer Services Center</h6>
        <div className="map_container">
          <img src={map_filter} alt="Filter" className="filter_icon" />
          <iframe
            height="499.5px"
            width="100%"
            frameborder="0"
            scrolling="no"
            src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3333.967410377096!2d-111.89998968453055!3d33.31966746342457!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x0!2zMzPCsDE5JzEwLjgiTiAxMTHCsDUzJzUyLjEiVw!5e0!3m2!1sen!2sus!4v1516690469899"
            title="Map"
          />
          <div className="direct_location_section">
            <h6>P&C Claims - HO CRO</h6>
            <label>Phone: 011-4826969</label>
            <label>Fax: 011-4828086</label>
            <label>Email: mnujaim@tawuniya.com.sa</label>
            <label>Working Hours: 7:30 am to 3:30 pm</label>
            <div>
              <label>Get Direction</label>
              <img src={get_direction} alt="Direction" />
            </div>
          </div>
        </div>
      </div>
      <MobileContactForm />
      {/* <div className="inquery_container">
        <h6>Inquery Form</h6>
        <p className="reach_sub">Let us know how can we reach you</p>
        <div className="form_section">
          <div className="">
            <hr className="alignhr" />
            <h6 className="fw-800">Personal Details</h6>

            <div className="row pt-3">
              <div className="col-lg-6">
                <Form>
                  <Form.Group
                    className="mb-3 inputbg"
                    controlId="formBasicEmail"
                  >
                    <Form.Control type="email" placeholder="First Name" />
                  </Form.Group>
                </Form>
              </div>

              <div className="col-lg-6">
                <Form>
                  <Form.Group
                    className="mb-3 inputbg"
                    controlId="formBasicEmail"
                  >
                    <Form.Control type="email" placeholder="Last Name" />
                  </Form.Group>
                </Form>
              </div>

              <div className="col-lg-10 ">
                <InputGroup className="mb-3">
                  <img src={IndiaFlagImage} className="imageflag" alt="flag" />
                  <DropdownButton
                    variant="outline-secondary"
                    title="+966"
                    id="input-group-dropdown-1"
                    className="dropdownhg"
                  >
                    <Dropdown.Item href="#">
                      <img src={IndiaFlagImage} className="image" alt="flag" />{" "}
                      +966
                    </Dropdown.Item>
                    <Dropdown.Item href="#">
                      <img src={KSAFlagImage} className="image" alt="flag" />{" "}
                      +91
                    </Dropdown.Item>
                    <Dropdown.Divider />
                  </DropdownButton>
                  <FormControl placeholder="ex: 5xxxxxxxx" />
                </InputGroup>
              </div>

              <div className="col-lg-2">
                <Button className="sendoptbtn"  onClick={() => setIsverficationModel(true)}>Send OTP</Button>
              </div>
              <BottomPopup open={isverficationModel} setOpen={setIsverficationModel}>
                 <UpdateVerification />
              </BottomPopup>
              <div className="col-lg-12">
                <InputGroup className="mb-3">
                  <InputGroup.Text id="basic-addon1">
                    {" "}
                    <img src={Email} />
                  </InputGroup.Text>
                  <FormControl
                    placeholder="ex: email@tawuniya.com.sa"
                    aria-label="Username"
                    aria-describedby="basic-addon1"
                  />
                </InputGroup>
              </div>

              <div className="col-lg-12">
                <p className="fw-500">Preferred way to contact you</p>
              </div>

              <div className="col-12 mb-2">
                <InputGroup className={setclassname1}>
                  <FormControl placeholder="Email" />
                  <InputGroup.Radio
                    checked={setenquiryradio1}
                    value="1"
                    onClick={handleradiochange}
                    aria-label="Radio button for following text input"
                  />
                </InputGroup>
              </div>

              <div className="col-lg-12">
                <InputGroup className={setclassname2}>
                  <FormControl placeholder="Phone/Mobile Number" />
                  <InputGroup.Radio
                    checked={setenquiryradio2}
                    value="2"
                    onClick={handleradiochange}
                    aria-label="Radio button for following text input"
                  />
                </InputGroup>
              </div>

              <div className="col-lg-12">
                <hr />
                <h5 className="fw-800">Reason of Contact</h5>
              </div>

              <div className="col-12 mb-2">
                <InputGroup className={setclassname3}>
                  <FormControl placeholder="Inquiry" />
                  <InputGroup.Radio
                    checked={setenquiryradio3}
                    value="3"
                    onClick={handleradiochange}
                    aria-label="Radio button for following text input"
                  />
                </InputGroup>
              </div>
              <div className="col-12 mb-2">
                <InputGroup className={setclassname4}>
                  <FormControl placeholder="Suggestion" />
                  <InputGroup.Radio
                    checked={setenquiryradio4}
                    value="4"
                    onClick={handleradiochange}
                    aria-label="Radio button for following text input"
                  />
                </InputGroup>
              </div>

              <div className="col-12">
                <InputGroup className={setclassname5}>
                  <FormControl placeholder="Question" />
                  <InputGroup.Radio
                    className="radiobg"
                    checked={setenquiryradio5}
                    value="5"
                    onClick={handleradiochange}
                    aria-label="Radio button for following text input"
                  />
                </InputGroup>
              </div>

              <div className="col-lg-12">
                <hr />
              </div>

              <div className="col-lg-12">
                <p className="your_message">Your Message</p>
                <InputGroup>
                  <FormControl placeholder="Subject" />
                </InputGroup>
              </div>

              <div className="col-lg-12 pt-4">
                <p className="your_message">Your Message</p>
                <Form.Control
                  as="textarea"
                  placeholder="Type here.."
                  style={{ height: "100px" }}
                />
              </div>

              <div className="col-lg-12">
                <hr />
              </div>

              <div className="col-lg-12 pt-3 pb-5">
                <Button className="sendbtn" onClick={() => setOpen(true)}>
                  Send
                </Button>
              </div>
            </div>
          </div>
        </div>
      </div> */}

      <AnimatedBottomPopup open={open} setOpen={setOpen} minHeight={"444px"}>
        <div className="contact_us_thanks_model">
          <img src={ConfirmationWoman} alt="ConfirmationWoman" />
          <div className="contact_us_thanks">Thanks for getting in touch</div>
          <p>
            We have received your message. We aim to provide a response in 1-2
            business days. In the meantime, please feel free to head over to our
            <span className="contact_us_help">Help Center.</span> If you are
            unable to get an answer to your question, we'll still be getting
            back to you. Thank you for your patience!
          </p>
          <div className="contact_us_close" onClick={() => setOpen(false)}>
            Close
          </div>
        </div>
      </AnimatedBottomPopup>
      <FooterMobile />
    </div>
  );
};
export default ContactUsMobile;
