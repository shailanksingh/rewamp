import React, { useState } from "react";

import { Card } from "react-bootstrap";

import Phone from "../../../assets/svg/phone.svg";
import Location from "../../../assets/svg/location.svg";

import Communication from "../../../assets/svg/communication.svg";
import Fax from "../../../assets/svg/Social/fax.svg";
import Confirm from "../../../assets/svg/Confirmation-Girl.svg";
import { NormalRadioButton } from "component/common/NormalRadioButton";

import "./style.scss";

export const ContactFeedback = () => {
	const [setradio1, setRadio1] = useState(false);
	const [setradio2, setRadio2] = useState(false);
	const [setradio3, setRadio3] = useState(false);
	const [setradio4, setRadio4] = useState(false);

	const handlechange = (e) => {
		if (e.target.value == 1) {
			setRadio1(true);
			setRadio2(false);
			setRadio3(false);
			setRadio4(false);
		}
		if (e.target.value == 2) {
			setRadio1(false);
			setRadio2(true);
			setRadio3(false);
			setRadio4(false);
		}
		if (e.target.value == 3) {
			setRadio1(false);
			setRadio2(false);
			setRadio3(true);
			setRadio4(false);
		}
		if (e.target.value == 4) {
			setRadio1(false);
			setRadio2(false);
			setRadio3(false);
			setRadio4(true);
		}
	};

	return (
		<React.Fragment>
			<div className="maincontact">
				<div className=" pt-5 ">
					<h2 className="fw-800 text-center">Contacting Tawuniya</h2>
					<div className="row">
						<div className="col-lg-8 aligncenterdiv">
							<p className="contactptag text-center">
								Our team was handpicked for their passion for insurance products
								and services. Whether you are browsing our site or visiting our
								branches, we are always willing to share our deep knowledge. The
								most commonly asked questions are covered in our
								<span className="textclr"> Customer Service</span>.
							</p>
							<p className="contactptag text-center">
								If you have any specific questions please do not hesitate to
								contact us by completing{" "}
								<span className="textclr">Enquiry</span> form or calling our
								customer services team.
							</p>
						</div>
					</div>

					<div className="row justify-content-center pt-4">
						<div className="col-lg-1">
							<p className="contactptagalign fs-14 fw-800">HEAD QUARTER</p>
						</div>
						<div className="col-lg-1">
							<p className="fs-14 pl-2 contactptag">CUSTOMERS</p>
						</div>
						<div className="col-lg-1">
							<p className="fs-14 contactptag">INVESTORS</p>
						</div>
						<div className="col-lg-1">
							<p className="fs-14 contactptag agent">AGENT & BROKERS</p>
						</div>
						<div className="col-lg-1">
							<p className="fs-14 pl-4 contactptag carrer-ptag">CAREERS</p>
						</div>
						<div className="col-lg-1">
							<p className="fs-14 contactptag pl-0 media-ptag">MEDIA</p>
						</div>
					</div>

					{/* --main card---start----- */}
					<div className="pt-5">
						<Card className="maincard pb-5">
							{/* ---------- inner black card----- */}
							<div className="container pt-5 ">
								<Card className="social-card4 ">
									<div className="container-fluid">
										<div className="row">
											<div className="col-lg-0 pt-3">
												<div className="container">
													<img src={Phone} />
												</div>
											</div>

											<div className="col-lg-3">
												<p className="mainBannerPara medical-ptag pt-3">
													Phone
												</p>
												<h6 className="mainBannerHeading">+966 11 252 5800</h6>
											</div>

											<div className="col-lg-0 pt-3">
												<div className="container">
													<img src={Fax} />
												</div>
											</div>

											<div className="col-lg-3">
												<p className="mainBannerPara medical-ptag pt-3">Fax</p>
												<h6 className="mainBannerHeading">+966 11 400 0844</h6>
											</div>

											<div className="col-lg-0 pt-3">
												<div className="container">
													<img src={Communication} />
												</div>
											</div>

											<div className="col-lg-4 emailui">
												<p className="mainBannerPara pt-3"> Email</p>

												<h6 className="mainBannerHeading">
													Care@tawuniya.com.sa{" "}
												</h6>
											</div>
										</div>

										<hr className="hrtag" />

										<div className="row">
											<div className="col-lg-0 pt-1">
												<div className="container">
													<img src={Fax} />
												</div>
											</div>

											<div className="col-lg-2">
												<p className="mainBannerPara medical-ptag pt-1">
													P.O.Box
												</p>
												<h6 className="mainBannerHeading">86959</h6>
											</div>

											<div className="col-lg-0 pt-1">
												<div className="container">
													<img src={Location} />
												</div>
											</div>

											<div className="col-lg-7 pb-1">
												<p className="mainBannerPara medical-ptag pt-1">
													Address
												</p>
												<h6 className="mainBannerHeading">
													6507 Thomamah Road (Takhassusi) - Ar Rabi, Riyadh
													11632
												</h6>
											</div>
										</div>
									</div>
								</Card>
							</div>
							{/* ------------- */}

							{/* -------filter checkbox card start */}

							<div className="container-fluid pt-4">
								<Card className="filter-card pb-4">
									<div className="row">
										<div className=" col-lg-2  pt-4">
											<select class="selectdropdown  ">
												<option value="1">All Regions</option>
												<option value="2">uae</option>
											</select>
										</div>

										<div className=" col-lg-2 pt-4">
											<select class="selectdropdown cityalign ">
												<option value="1">All Cities</option>
												<option value="2">Chennai</option>
											</select>
										</div>

										<div className="col-1.5 pt-4 mt-3 alignradio ">
											<NormalRadioButton
												type="radio"
												name="All Offices"
												value="1"
												radioValue="All Offices"
												onChange={handlechange}
												checked={setradio1}
											/>
										</div>

										<div className="col-lg-1.5 mt-3 pl-4 pt-4   ">
											<NormalRadioButton
												type="radio"
												name="Sales Office"
												value="2"
												onChange={handlechange}
												radioValue="Sales Office"
												checked={setradio2}
											/>
										</div>

										<div className="col-lg-1.5 mt-3 pl-4 pt-4 ">
											<NormalRadioButton
												type="radio"
												name="Head Office"
												value="3"
												onChange={handlechange}
												radioValue="Head Office"
												checked={setradio3}
											/>
										</div>

										<div className="col-lg-1.5 mt-3 pl-4 pt-4 ">
											<NormalRadioButton
												type="radio"
												onChange={handlechange}
												name="Claims Office"
												value="4"
												radioValue="Claims Office"
												checked={setradio4}
											/>
										</div>
									</div>
								</Card>
							</div>

							{/* filter checkbox card end */}
						</Card>

						{/* ------------3rd card start */}

						<div className="container pb-5">
							<Card className="form-card ">
								<h2 className="pt-5 text-center fw-800">Inquiry Form</h2>
								<p className="text-center">Let us know how can we reach you</p>

								<img className="confirmimg" alt="" src={Confirm} />

								<h2 className="pt-5 text-center fw-800">
									Thanks for getting in touch
								</h2>
								<p className="text-center">We have received your message</p>

								<div className="row">
									<div className="col-lg-6 aligndivcenter">
										<p className="text-center">
											{" "}
											We aim to provide a response in 1-2 business days. In the
											meantime, please feel free to head over to our{" "}
											<span className="textclr">Help Center</span> . If you are
											unable to get an answer to your question, we'll still be
											getting back to you.{" "}
										</p>

										<p className="text-center">Thank you for your patience! </p>
									</div>
								</div>
							</Card>
						</div>

						{/* 3card end */}
					</div>

					{/* main card end */}
				</div>

				<div className="text-center pb-5">
					<h3 className="fw-800">Committee of Insurance Disputes</h3>
					<p>Committee for the resolution of insurance disputes</p>
					<div className="row justify-content-center pt-4">
						<div className="col-lg-1">
							<p className="contactptagalign fs-16 fw-800">RIYADH</p>
						</div>
						<div className="col-lg-1">
							<p className="fs-16 pl-2 contactptag">JEDDAH</p>
						</div>

						<div className="col-lg-1">
							<p className="fs-16 pl-2 contactptag">DAMMAM</p>
						</div>
					</div>
				</div>
				<div className="container pb-5">
					<Card className="social-card5 ">
						<div className="container-fluid">
							<div className="row">
								<div className="col-lg-0 pt-3">
									<div className="container">
										<img src={Phone} />
									</div>
								</div>

								<div className="col-lg-3">
									<p className="mainBannerPara medical-ptag pt-3">Phone</p>
									<h6 className="mainBannerHeading">+966 11 252 5800</h6>
								</div>

								<div className="col-lg-0 pt-3">
									<div className="container">
										<img src={Fax} />
									</div>
								</div>

								<div className="col-lg-3">
									<p className="mainBannerPara medical-ptag pt-3">Fax</p>
									<h6 className="mainBannerHeading">+966 11 400 0844</h6>
								</div>

								<div className="col-lg-0 pt-3">
									<div className="container">
										<img src={Communication} />
									</div>
								</div>

								<div className="col-lg-4 emailui">
									<p className="mainBannerPara pt-3"> Email</p>

									<h6 className="mainBannerHeading">Care@tawuniya.com.sa </h6>
								</div>
							</div>

							<hr className="hrtag" />

							<div className="row">
								<div className="col-lg-0 pt-1">
									<div className="container">
										<img src={Fax} />
									</div>
								</div>

								<div className="col-lg-2">
									<p className="mainBannerPara medical-ptag pt-1">P.O.Box</p>
									<h6 className="mainBannerHeading">86959</h6>
								</div>

								<div className="col-lg-0 pt-1">
									<div className="container">
										<img src={Location} />
									</div>
								</div>

								<div className="col-lg-7 pb-1">
									<p className="mainBannerPara medical-ptag pt-1">Address</p>
									<h6 className="mainBannerHeading">
										6507 Thomamah Road (Takhassusi) - Ar Rabi, Riyadh 11632
									</h6>
								</div>
							</div>
						</div>
					</Card>
				</div>

				{/* --- */}
			</div>
		</React.Fragment>
	);
};
