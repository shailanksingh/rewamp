import React ,{useState,useEffect} from "react"
import { useLocation } from 'react-router-dom';

import HeaderStickyMenu from "component/common/MobileReuseable/HeaderStickyMenu";
import HeaderStepsSticky from "component/common/MobileReuseable/HeaderStepsSticky";
import { NormalButton } from "../../component/common/NormalButton";

import logo from "assets/svg/MobileAssets/logo.svg";
import logo2 from "assets/svg/MobileAssets/logo2.svg";
import rightarrow from "assets/svg/MobileAssets/right-arw.svg";
import home from "assets/svg/MobileAssets/home.svg";


import {Card} from "react-bootstrap";
import "./style.scss";


export const InsuranceMobile = () => {

    const location = useLocation();
    const [setcardclass, setSetcardclass] = useState("");
    const [setTitle, setSetTitle] = useState("");
    const [setTitle2, setSetTitle2] = useState("");

    const [setcardptag, setSetcardptag] = useState("");
    const [setcardptag2, setSetcardptag2] = useState("");
    const [foregincarddata , setSetForegincarddata]=useState([])
    const [programcarddata , setSetProgramcarddata]=useState([])
    const [setcardbg, setSetcardbg] = useState("");



    const umraptag=['The comprehensive insurance program for Umrah pilgrims was launched at the beginning of 2020. It is one of the initiatives that contribute in improving the quality of the provided services to pilgrims by allocating this comprehensive insurance coverage for emergency cases to compensate the beneficiaries under these covered cases and to find the fair solutions and immediate procedures in case of the occurrence of any covered incident. ',
    "Umrah insurance product includes (Medical Emergencies & General Accidents) Coverages under certain conditions. Tawuniya has been honored to provide this service to the Pilgrims of the Holy Mosque in cooperation with the participating insurance companies in the Kingdom of Saudi Arabia by launching a joint insurance pool managed by Tawuniya."
    ]
    const hajjptag=['The insurance program for Hajj Pilgrims was launched in conjunction with 2022 Hajj Season. It is one of the initiatives that contribute in improving the quality of the provided services to pilgrims by allocating this insurance coverage for general accidents cases to compensate the beneficiaries under these covered cases and to find the fair solutions and immediate procedures in case of the occurrence of any covered incident. ',
    'Hajj insurance product covers (General Accidents) under certain conditions. Tawuniya has been honored to provide this service to the Pilgrims of the Holy Mosque in cooperation with the participating insurance companies in the Kingdom of Saudi Arabia by launching a joint insurance pool managed by Tawuniya.'
    ]

    const foreigndataumra=[{
        image:logo,
        data:" It provides safety and peace of mind during the holy journey in the kingdom"
    },
    {
        image:logo2,
        data:"It provides the necessary health care"
    }

]

    const foreigndatahajj=[{
        image:logo,
        data:"It provides safety and peace of mind during the holy journey in the kingdom"
    },
    {
        image:home,
        data:"It bears the medical & quarantine expenses in the event of infection with the emerging Corona Virus"
    }

    ]

    const benifitdata=[{
        amount:"100,000 SAR",
        data:"for Emergency Medical benefit limit"
    },
    {
        amount:"100,000 SAR",
        data:"for Accidental Permanent Disability"
    }

    ]

    const programdataumar=[{
        title:"Comprehensive Insurance",
        data:" It provides safety and peace of mind during the holy journey in the kingdom"
    },
    {
        title:"30 Days",
        data:"   Covering Medical emergencies & General Accidents cases"
    }

    ]

    const programdatahajj=[{
        title:"Insurance",
        data:"Covering General Accidents cases"
    },
    {
        title:"Up to 100,000 SAR",
        data:"Insurance Cover Limit per insured Pilgrim"
    }

    ]


    useEffect(()=>{
        if(location.pathname ==="/home/umrainsurancemobile"){
            setSetcardclass("main_card mt-4")
            setSetTitle("Umrah Insurance Program")
            setSetTitle2("Why Umrah Insurance for foreign pilgrims?")
            setSetcardptag(umraptag[0])
            setSetcardptag2(umraptag[1])
            setSetForegincarddata(foreigndataumra)
            setSetProgramcarddata(programdataumar)
            setSetcardbg("insurance_card4 mt-4")
        }
        if(location.pathname === "/home/hajjinsurancemobile"){
            setSetcardclass("main_card_hajj mt-4")
            setSetTitle("Hajj Insurance Program")
            setSetTitle2("Why Hajj Insurance for foreign pilgrims?")
            setSetcardptag(hajjptag[0])
            setSetcardptag2(hajjptag[1])
            setSetForegincarddata(foreigndatahajj)
            setSetProgramcarddata(programdatahajj)
            setSetcardbg("insurance_cardhajj mt-4")
        }


    })
    return(
        <>
        <div className="main_insurance">
        <HeaderStickyMenu/>
        <HeaderStepsSticky title={setTitle} buttonTitle="Buy Now" />

        <div className="container">
            {/* main card start */}
        <Card className={setcardclass}>
            <Card className="insurance_card2">

                <div className="container">
                <p className="pt-4 mainCardPara enquirytag">{setcardptag}</p>
                <p className="mainCardPara enquirytag">{setcardptag2}</p>
                </div>
             
            </Card>
        </Card>

        {/* main card end--------- */}
        <p className="fw-800 fs-17 insurance_htags pl-2">{setTitle2}</p>
        {/* ----- */}
        <div className="insurance_card">
            {foregincarddata.map((item,indx)=>{
                return(
                    <div className="ms-2 mt-1 ">
                    <div className="container insurance_desc_text ms-3 mx-2">
                      <img src={item.image} alt="image" className="image_position" />
                      <p className=" mt-2  fw-bold insurance_maintenance_display ">
                     {item.data}
                      </p>
                    </div>
                  </div>
                )
            })}
        </div>
        {/* ----- */}

        <p className="fw-800 fs-17 pt-1 pl-2">The Program Features</p>
        <div className="insurance_card3">
            {programcarddata.map((item,indx)=>{
                return(
                    <div className="ms-2 mt-1 ">
                    <div className="container insurance_desc_text2 ms-3 mx-2">
                     <p className="fw-800 pt-2">{item.title}</p>
                      <p className=" mt-2 mb-0 fs-14 ">
                      {item.data}
                      </p>
                    </div>
                  </div>
                )
            })}
        </div>
        {/* ---- */}
        </div>
        <Card className={setcardbg}>
        <div className="container">
        <p className="pt-4 mainCardParahead mt-5 pl-2">The Program Benefits</p>

        <div className="insurance_card">
            {benifitdata.map((item,indx)=>{
                return(
                    <div className="ms-2 mt-1 ">
                    <div className="container insurance_desc_text ms-3 mx-2">
                     <p className="fs-12 pt-3">Maximum of</p>
                      <p className=" mt-1 fw-800 mb-0 fs-18 amountbg  ">
                     {item.amount}
                      </p>
                      <p className="  pb-2 mainCardPara">
                     {item.data}
                      </p>
                    </div>
                  </div>
                )
            })}
        </div>
        </div>
        </Card>
{/* ---------------- */}

        <div className="container">
        <p className="fw-800 fs-18 pt-3 pl-2">How to benefit from the Program’s Services?</p>
        </div>
        <Card className="insurance_card5 mt-4">
        <div className="container">
        <p className="pt-4 mainCardPara enquirytag">The beneficiary needs to visit one of the approved medical providers or contact (Total Care Saudi Company) at any time of the day, as soon as the situation is expected to involve expenses that fall within the scope of Umrah Insurance cover</p>
        <p className="pt-3 mainCardPara enquirytag">For more information about the beneficiary status and to follow up with pre-approvals and claims</p>
        <div className="d-flex">
        <p className="fw-800">Register Here   <img src={rightarrow} className="pl-3" alt="arrow"/></p>
      
        </div>
        </div>
        </Card>

{/* -------------------- */}
        <Card className="insurance_card5 mt-4">
        <div className="container">
        <p className="pt-4 mainCardPara enquirytag">For inquiries and emergencies, contact TCS Company</p>
        <div className="row">
            <div className="col-6">
                <p className="enquirytag">Inside KSA</p>
                <p className="fw-800 amountbg space">8004400008</p>
            </div>

            <div className="col-6">
                <p className="enquirytag">Outside KSA</p>
                <p className="fw-800 amountbg space">+966 138129700</p>
            </div>

            <div className="col-8">
                <p className="enquirytag">Website</p>
                <p className="fw-800 amountbg space">www.enaya-ksa.com</p>
            </div>
        </div>
        </div>
        </Card>
        
        {/* ------------------- */}
                    <Card className="insurance_card5 mt-4">
                    <div className="container">
                    <p className="pt-4 mainCardPara enquirytag">Want to download our Umrah Insurance Program Policy?<br/>
                    Here you can find it.</p>
                    <NormalButton
                    label="View Umrah Insurance Program Policy"
                    className="complaintFormSubmitBtn p-4 enquirytag"
                />
                <p className="fw-800 pt-4 fs-14 enquirytag">Product ID: A-TAWU-2-I-20-061</p>

</div>
</Card>
        </div>
        
        
        </>
    )




}

export default InsuranceMobile