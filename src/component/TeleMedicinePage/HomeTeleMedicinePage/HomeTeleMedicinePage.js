import React from "react";
import { DashBoardLandingPage } from "component/common/DashBoardLandingPage";
import {
  dashboardTeleMedHeaderData,
  dashboardTeleMedProgressData,
  faqRoadDashboardData,
  teleMedicineContentData,
} from "component/common/MockData";
import "./style.scss";

export const HomeTeleMedicinePage = () => {
  return (
    <div className="teleMedicine-LandingContainer">
      <DashBoardLandingPage
        dashboardHeaderData={dashboardTeleMedHeaderData}
        dashboardProgressData={dashboardTeleMedProgressData}
        faqDashboardData={faqRoadDashboardData}
        dashBoardContentData={teleMedicineContentData}
      />
    </div>
  );
};
