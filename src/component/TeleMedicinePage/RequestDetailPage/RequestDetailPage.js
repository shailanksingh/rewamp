import React, { useState } from "react";
import { RequestRadioController } from "./RequestDetailComponents";
import PhoneNumberInput from "component/common/PhoneNumberInput";
import { NormalButton } from "component/common/NormalButton";
import { dialingCodes } from "component/common/MockData/index";
import { NormalSearch } from "component/common/NormalSearch";
import iquamaIcon from "assets/svg/iquamaIcon.svg";
import dateIcon from "assets/svg/datePickerIcon.svg";
import male from "assets/svg/maleGender.svg";
import female from "assets/svg/femaleGender.svg";
import "./style.scss";


export const RequestDetailPage = () => {
  const [toggler, setToggler] = useState(false);

  const togglerHandler = () => {
    setToggler(!toggler);
  };

  const searchRadioData = [
    {
      id: 0,
      radioName: "abdul",
      radioColumnOneLabel: "Member Name",
      radioColumnOneLabelContent: "Abdulrahman Fahad",
      radioColumnOneLabelImg: male,
      radioColumnTwoLabel: "Member Card",
      radioColumnTwoLabelContent: "001064190364001",
      radioColumnTwoLabelImg: iquamaIcon,
      radioColumnThreeLabel: "Date of Birth",
      radioColumnThreeLabelContent: "12 Jun 2022",
      radioColumnThreeLabelImg: dateIcon,
      showRadio: true,
    },
    {
      id: 1,
      radioName: "rana",
      radioColumnOneLabel: "Member Name",
      radioColumnOneLabelContent: "Rana Faisal",
      radioColumnOneLabelImg: female,
      radioColumnTwoLabel: "Member Card",
      radioColumnTwoLabelContent: "001064190364001",
      radioColumnTwoLabelImg: iquamaIcon,
      radioColumnThreeLabel: "Date of Birth",
      radioColumnThreeLabelContent: "12 Jun 2022",
      radioColumnThreeLabelImg: dateIcon,
      showRadio: true,
    },
    {
      id: 2,
      radioName: "turkey",
      radioColumnOneLabel: "Member Name",
      radioColumnOneLabelContent: "Turkey Abdullah",
      radioColumnOneLabelImg: male,
      radioColumnTwoLabel: "Member Card",
      radioColumnTwoLabelContent: "001064190364001",
      radioColumnTwoLabelImg: iquamaIcon,
      radioColumnThreeLabel: "Date of Birth",
      radioColumnThreeLabelContent: "12 Jun 2022",
      radioColumnThreeLabelImg: dateIcon,
      showRadio: true,
    },
    {
      id: 3,
      radioName: "Rahaf Abullah",
      radioColumnOneLabel: "Member Name",
      radioColumnOneLabelContent: "Rahaf Abullah",
      radioColumnOneLabelImg: female,
      radioColumnTwoLabel: "Member Card",
      radioColumnTwoLabelContent: "001064190364001",
      radioColumnTwoLabelImg: iquamaIcon,
      radioColumnThreeLabel: "Date of Birth",
      radioColumnThreeLabelContent: "12 Jun 2022",
      radioColumnThreeLabelImg: dateIcon,
      showRadio: toggler,
    },
    {
      id: 4,
      radioName: "Saud Faisal",
      radioColumnOneLabel: "Member Name",
      radioColumnOneLabelContent: "Saud Faisal",
      radioColumnOneLabelImg: male,
      radioColumnTwoLabel: "Member Card",
      radioColumnTwoLabelContent: "001064190364001",
      radioColumnTwoLabelImg: iquamaIcon,
      radioColumnThreeLabel: "Date of Birth",
      radioColumnThreeLabelContent: "12 Jun 2022",
      radioColumnThreeLabelImg: dateIcon,
      showRadio: toggler,
    },
  ];

  //initialiize state for radio buttons
  const [radioValue, setRadioValue] = useState("abdul");

  const [search, setSearch] = useState("");

  let [selectedCode, setSelectedCode] = useState(dialingCodes[0]);

  const [inpOne, setInpOne] = useState({ phn: "" });

  const [countLetter, setCountLetter] = useState(0);

  const handleInputChange = (e) => {
    const { name, value } = e.target || e || {};
    setInpOne({ ...inpOne, [name]: value });
  };

  return (
    <React.Fragment>
      <div className="teleMed-RequestDetail-Container mt-4 mb-3">
        <div className="teleMed-RequestDetail-Container-Liner">
          <div className="row">
            <div className="col-lg-12 col-12 teleMed-Search-Container">
              <p className="fs-20 fw-800 teleMed-searchTitle">
                Request Details
              </p>
              <p className="fs-14 fw-700 teleMed-searchPara">Member Name</p>
              <div className="telemed-Search-box p-3">
                <NormalSearch
                  className="teleMed-headerSearch"
                  name="search"
                  value={search}
                  placeholder="Search"
                  onChange={(e) => setSearch(e.target.value)}
                  needRightIcon={true}
                />
                <RequestRadioController toggler={toggler} togglerHandler={togglerHandler} radioData={searchRadioData} radioValue={radioValue} setRadioValue={setRadioValue} />
              </div>
              <div className="pt-3 consultation-Container mt-4">
                <p className="fs-20 fw-800 consult-Title">
                  Consultation Details
                </p>
                <div className="consult-sub-container mb-4">
                  <p className="fs-14 fw-700 consult-para m-0 pb-2">
                    Medical Consultation
                  </p>
                  <textarea
                    className="teleMedForm-textArea"
                    maxlength="150"
                    onChange={(e) => setCountLetter(e.target.value.length)}
                  >
                    Type here...
                  </textarea>
                  <p className="fs-14 fw-400 letterCountLabel m-0 pt-1">
                    {countLetter + "/150"}
                  </p>
                  <p className="fs-14 fw-700 tele-med-mobileNo pt-3 pb-2 m-0">
                    Mobile Number
                  </p>
                  <PhoneNumberInput
                    className="teleMedPhoneInput"
                    selectInputClass="teleMedSelectInputWidth"
                    selectInputFlexType="teleMedSelectFlexType"
                    dialingCodes={dialingCodes}
                    selectedCode={selectedCode}
                    setSelectedCode={setSelectedCode}
                    value={inpOne.phn}
                    name="phn"
                    onChange={handleInputChange}
                  />
                </div>
              </div>

              <div className="pt-4">
                <NormalButton
                  label="Submit Telemedicine Request"
                  className="SubmitTelemedicineRequestBtn p-4"
                />
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="telemed-bottom-liner mt-4 mb-3"></div>
    </React.Fragment>
  );
};
