import React, { useState } from "react";
import { NormalRadioButton } from "component/common/NormalRadioButton";
import orangeIcon from "assets/svg/orangeDropdown.svg";
import "./style.scss";

export const RequestRadioController = ({
  toggler,
  togglerHandler,
  radioData,
  radioValue,
  setRadioValue,
}) => {
  return (
    <div className="row search-RadioMainContainer m-0 pt-3">
      {radioData.map((item, index) => {
        return (
          <React.Fragment>
            {item.showRadio && (
              <div
                className={`${
                  radioValue === item.radioName
                    ? "search-HighlightRadioContainer"
                    : "search-RadioContainer"
                } col-12 mb-3`}
                key={index}
              >
                <div className="row">
                  <div className="col-lg-4 col-12">
                    <div className="d-flex flex-row search-Liner">
                      <div className="pr-3">
                        <img
                          src={item.radioColumnOneLabelImg}
                          className="img-fluid"
                          alt="icon"
                        />
                      </div>
                      <div>
                        <p className="fs-12 fw-400 pt-1 search-radioTitle m-0">
                          {item.radioColumnOneLabel}
                        </p>
                        <p className="fs-16 fw-800 m-0 search-radioPara">
                          {item.radioColumnOneLabelContent}
                        </p>
                      </div>
                    </div>
                  </div>
                  <div className="col-lg-4 col-12">
                    <div className="d-flex flex-row search-Liner">
                      <div className="pr-3">
                        <img
                          src={item.radioColumnTwoLabelImg}
                          className="img-fluid"
                          alt="icon"
                        />
                      </div>
                      <div>
                        <p className="fs-12 fw-400 pt-1 search-radioTitle m-0">
                          {item.radioColumnTwoLabel}
                        </p>
                        <p className="fs-16 fw-800 m-0 search-radioPara">
                          {item.radioColumnTwoLabelContent}
                        </p>
                      </div>
                    </div>
                  </div>
                  <div className="col-lg-4 col-12">
                    <div className="d-flex justify-content-between">
                      <div>
                        <div className="d-flex flex-row">
                          <div className="pr-3">
                            <img
                              src={item.radioColumnThreeLabelImg}
                              className="img-fluid"
                              alt="icon"
                            />
                          </div>
                          <div>
                            <p className="fs-12 fw-400 pt-1 search-radioTitle m-0">
                              {item.radioColumnThreeLabel}
                            </p>
                            <p
                              className="fs-16 fw-800 m-0"
                              id="search-radioPara"
                            >
                              {item.radioColumnThreeLabelContent}
                            </p>
                          </div>
                        </div>
                      </div>
                      <div className="position-radioSearchContainer">
                        <NormalRadioButton
                          type="radio"
                          name={item.radioName}
                          value={item.radioName}
                          onChange={(e) => setRadioValue(e.target.value)}
                          checked={radioValue === item.radioName}
                          isStyledRadio={true}
                        />
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            )}
          </React.Fragment>
        );
      })}
      <div className="mx-auto d-block" onClick={togglerHandler}>
        <span className="fs-16 fw-400 teleMed-ViewAll cursor-pointer">
          {!toggler ? "View all" : "View Less"}
        </span>{" "}
        <img
          src={orangeIcon}
          className="img-fluid cursor-pointer pl-1"
          alt="ion"
        />
      </div>
    </div>
  );
};
