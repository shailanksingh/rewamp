import React ,{useState} from "react";
import {Card,Modal,Button,Form } from "react-bootstrap"
import Tick from "assets/tele-request/tick.svg"
import User from "assets/tele-request/user.svg"
import Approval from "assets/tele-request/Horizontal.svg"
import Phone from "assets/tele-request/phone.svg"
import Contact from "assets/tele-request/contact.svg"
import Emoji1 from "assets/tele-request/emoji1.svg"
import Emoji2 from "assets/tele-request/emoji2.svg"
import Emoji3 from "assets/tele-request/emoji3.svg"
import Emoji4 from "assets/tele-request/emoji4.svg"
import Emoji5 from "assets/tele-request/emoji5.svg"
import Number1 from "assets/tele-request/number1.svg"
import Number2 from "assets/tele-request/number2.svg"
import Number3 from "assets/tele-request/number3.svg"
import Number4 from "assets/tele-request/number4.svg"
import Number5 from "assets/tele-request/number5.svg"
import Number6 from "assets/tele-request/number6.svg"
import Number7 from "assets/tele-request/number7.svg"
import Number8 from "assets/tele-request/number8.svg"
import Number9 from "assets/tele-request/number9.svg"
import Number10 from "assets/tele-request/number10.svg"
import Close from "assets/tele-request/close.svg"









import "./style.scss"
import { useParams } from "react-router-dom";

export const RequestIdPage = () =>{
    const { id } = useParams();
    const [show, setShow] = useState(false);

    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);
    

  
    
    return(
        <div className="requestMaincontent pt-5">
            <Card className="requestmaincard">
            <div className="container alert  mt-5">
                <div className="d-flex">
                <img src={Tick} alt="tick"/>
               <p className="pt-3 pl-5 alertptag">We have received your request</p>
                </div>
            </div>

            <div className="container-fluid ml-2">
                <p className="alertptag-heading">Road Assistance</p>
                <div class="d-flex">
                <h4 className="fw-800 requestid">Request #123456</h4>
                    <img className="alignimage " src={Approval} alt="Approval"/>
                    </div>
                    <hr className="hrtag mr-5 pb-2"/>
                <p className="fw-800 alertptag-heading pb-2">Request Details</p>
                <hr className="mb-3 hrtag mr-5 pb-2" />

                <div className="detailbg ">
                    <div className="container">
                        <div className="row">
                        <div className="col-lg-0 pt-3">
                        <div className="container">
                        <img className="" src={User} alt="contact"/>
                        </div>
                    </div>

                  <div className="col-lg-4  ">
                    <p className="pt-3 alertptag-heading">Member</p>
                    <p className="fw-800 reducespace">Prashant Dixit </p>
                  </div>

                  <div className="col-lg-0">
                    <div class="vertical-line "></div>
                  </div>

                  <div className="col-lg-0 pt-3">
                  <div className="container">
                        <img className="" src={Phone} alt="contact"/>
                        </div>
                    </div>

                  <div className="col-lg-4  ">
                    <p className="alertptag-heading pt-3">Mobile Number</p>
                    <p className="fw-800 reducespace">966503195993 </p>
                  </div>
                    </div>
                    {/* ------------ */}


                <div className="row">
                <div className="col-lg-0 pt-3">
                <div className="container">
                <img className="" src={Contact} alt="contact"/>
                </div>
                </div>

                  <div className="col-lg-8  ">
                    <p className="pt-3 alertptag-heading">Medical Consultation</p>
                    <p className="fw-800 alignwidth reducespace">Here is a sample text: Lorem ipsum dolor sit amet, consectetur adipiscing elit. Gravida eleifend nisl magnis quis eu, leo consequat pharetra aliquam. Tortor pharetra iaculis dictum amet non, nunc, non. Sit pretium amet nunc natoque at metus turpis nunc. Eget eget nulla justo, vel aliquam sagittis. </p>
                  </div>
                    </div>
                    </div>
                </div>
            </div>
            
            </Card>

{/* 
            <Button variant="primary" onClick={handleShow}>
        Launch demo modal
      </Button> */}

      {/* <Modal className="modal" show={show} onHide={handleClose}>
        <Modal.Body>
        <img className="closeicon pr-2" onClick={handleClose} src={Close} alt ="close"/>
          <div className="container pt-4">
            <p className="fw-800 fs-18">We would really appreciate it if you would just take a moment to let us know about your experience.</p>
            <hr/>

            <p className="fw-800">How satisfied were you with the effort it took to complete your request?</p>

            <div className="container row">
              <div className="col-lg-2">
              <img src={Emoji1} alt="emoji"/>
            

              </div>
              <div className="col-lg-2">
              <img className="pl-2" src={Emoji2} alt="emoji"/>
             

              </div>
              <div className="col-lg-2">
              <img className="pl-3"  src={Emoji3} alt="emoji"/>
           
              </div>
              <div className="col-lg-2">
              <img className="pl-3"  src={Emoji5} alt="emoji"/>
           

              </div>
              <div className="col-lg-2">
              <img className="pl-4"  src={Emoji5} alt="emoji"/>
              </div>
             
            </div>
            <hr/>
           
            <p className="fw-800">How likely are you would recommend Tawuniya to a friend, relative, or colleague?</p>
           
           
            <div className="d-flex">
              <img src={Number1} alt="number"/>
              <img src={Number2} alt="number"/>  
              <img src={Number3} alt="number"/>
              <img src={Number4} alt="number"/>
              <img src={Number5} alt="number"/>  
              <img src={Number6} alt="number"/>
              <img src={Number7} alt="number"/>
              <img src={Number8} alt="number"/>
              <img src={Number9} alt="number"/>  
              <img src={Number10} alt="number"/>

            

            </div>
            <div className="d-flex">
            <p className="fw-800 pt-2">Not at all Likely</p>
            <p className="textalign-right fw-800 pt-2">Extremely Likely</p>
            </div>
            <hr/>
           
              <p className="fw-800">How may we make your experience better in the future?</p>
              <Form.Control
              as="textarea"
              placeholder="Type here.."
              style={{ height: "100px" }}
            />
            <hr/>
              <Button className="sendbtn ">Send My Feedback</Button>

          </div>
        </Modal.Body>
       
      </Modal> */}
           
        </div>
    )
}