import React from "react";
import HeaderStepsSticky from "component/common/MobileReuseable/HeaderStepsSticky";
import HeaderStickyMenu from "component/common/MobileReuseable/HeaderStickyMenu";

const SocialProjectMobile = () => {
  return (
    <div className="social_responsibility_container">
      <HeaderStickyMenu />
      <HeaderStepsSticky title={"Social Responsability"} />
      <div>Social Responsibility</div>
    </div>
  );
};

export default SocialProjectMobile;
