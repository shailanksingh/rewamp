import React from "react";
import HeaderBackNav from "component/common/MobileReuseable/HeaderBackNav";
import "./style.scss";
import newsdetails from "assets/newsdetails/newsdetails.png";
import Facebook from "assets/newsdetails/Facebook.png";
import Twitter from "assets/newsdetails/Twitter.png";
import Linkedin from "assets/newsdetails/Linkedin.png";
import Whatsapp from "assets/newsdetails/Whatsapp.png";
import Messenger from "assets/newsdetails/Messenger.png";
import SocialResponsibilityProjects
  from "component/common/MobileReuseable/SocialResponsibilityProjects/SocialResponsibilityProjects";


const SocialResponsibilityDetails = () => {
  return (
    <div className="social_responsibility_container_detail">
      <HeaderBackNav pageName="Social Responsability" title="Projects"/>
      <div>
        <div className="social_responsibility_details">
          <div className="social_trending">Trending</div>
          <h1 className="social_headline">
            Tawuniya is the first company in the Kingdom to install vehicle
            insurance policies{" "}
          </h1>
          <h2 className="social_date"> 7th November 2021</h2>
          <div>
            <img alt="..." className="social_image" src={newsdetails}/>
          </div>
          <div className="social_body">
            <div className="d-flex justify-content-center">
              <div className="social_text">
                <div className="social_para_one">
                  The insurance industry is emerging as one of the important
                  sectors in Saudi Arabia as the Kingdom diversifies its
                  economy. It has been going through a structural evolution
                  under the Vision 2030 Financial Sector Development Program. In
                  recent years under the supervision of the Saudi Central Bank,
                  the Saudi insurance industry has shown flexibility in dealing
                  with changing market trends and customer requirements. In line
                  with this, and following the introduction of compulsory
                  vehicle liability insurance in 2002, demand has grown to
                  provide customers with a range of payment options to ensure
                  they are fully compliant.
                </div>
                <div className="social_para">
                  In response, Tawuniya has introduced a pay-by-installment
                  option that will be available for vehicle insurance customers
                  who account for 22% of the total size of the Saudi insurance
                  market, according to year-end results for 2020 issued by the
                  Saudi Central Bank. The option will make vehicle insurance
                  policies more accessible and increase the percentage of
                  vehicles insured under the mandatory insurance scheme.
                </div>
                <hr/>
                <p className="social_artical">Share article</p>
                <div className="social_image_face d-flex ">
                  <img
                    className="mx-1 "
                    src={Facebook}
                    alt="Facebook"
                  />
                  <img alt="..." className="mx-1" src={Twitter}/>
                  <img alt="..." className="mx-1" src={Linkedin}/>
                  <img alt="..." className="mx-1" src={Whatsapp}/>
                  <img alt="..."
                       className="mx-1"
                       src={Messenger}
                  />
                </div>
              </div>
            </div>
          </div>
          <div className="related_projects  mx-4">Related Projects</div>
          <SocialResponsibilityProjects
            days="10 May 2022"
            title="Tawuniya appoints Dr. Ammr Kurdi as Chief Financial Officer"/>
          <SocialResponsibilityProjects
            days="2 days ago"
            title="Tawuniya is the first company in the Kingdom to install vehicle insurance policies"/>
          <SocialResponsibilityProjects
            days="Week ago"
            title="Tawuniya launches Covid-19 Travel Insurance program" hideline={true}/>

          <div className=" height"/>
        </div>
      </div>
    </div>
  );
};

export default SocialResponsibilityDetails;
