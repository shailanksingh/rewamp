import React, { useState } from "react";

import { Card } from "react-bootstrap";

import customerHome from "assets/svg/customerHome.svg";
import customerBreadCrumb from "assets/svg/customerBreadCrumb.svg";
import Trend from "assets/svg/Social/trend.svg";
import Socialapp from "assets/svg/Social/socialapps.svg";
import Tag from "assets/svg/Social/tag3.svg";
import Tag2 from "assets/svg/Social/tag4.svg";
import leftArrowLight from "assets/svg/projectDetailLeftArrow.svg";
import rightArrowLight from "assets/svg/projectDetailRightArrow.svg";
import Rightarw from "assets/svg/Social/rightarw.svg";
import Slider from "react-slick";

import "./style.scss";

import { history } from "service/helpers";
import SocialResponsibilityDetails from "./mobile/SocialResponsibilityDetails";
export const SocialProject = () => {
  const settings = {
    dots: true,
    infinite: true,
    speed: 100,
    slidesToShow: 1,
    slidesToScroll: 1,
  };
  return (
    <React.Fragment>
      {window.innerWidth < 600 ? (
        <SocialResponsibilityDetails />
      ) : (
        <div className="mainSocialContainer">
          <img
            src={leftArrowLight}
            className="img-fluid socialLeftArrow cursor-pointer"
            alt="leftarrow"
          />
          <img
            src={rightArrowLight}
            className="img-fluid socialRightArrow cursor-pointer"
            alt="leftarrow"
          />
          {/* ----------header end-- */}

          <div className="container ">
            {/* -------top section------------ */}

            <div className="row pt-5 ">
              <div className="col-lg-6 aligncenterdiv">
                <img src={Trend} className="img-fluid" alt="icon" />{" "}
                <h2 className="mainBannerPara fw-800">
                  Tawuniya is the first company in the Kingdom to install
                  vehicle insurance policies
                </h2>
                <p>7th November 2021</p>
                <img
                  src={Socialapp}
                  className="img-fluid pr-2"
                  alt="icon"
                />{" "}
              </div>
            </div>

            {/* ----------end top section */}

            {/* card start */}
            <div className="container pt-5">
              <div class="row align-items-center">
                <div class="col-9 mx-auto">
                  <Card className="socialprojectcard"></Card>
                </div>

                {/* card2 */}

                <div class="col-7 mx-auto">
                  <Card className="socialprojectcard2">
                    <div className="container">
                      <p className="socialptag pt-4">
                        Tawuniya continued to provide full support and insurance
                        protection to the 21 "Taakad” centers for Covid-19
                        drive-through tests in the various regions of the
                        Kingdom, with the aim of strengthening national efforts
                        to combat coronavirus (Covid-19), and the Company also
                        organized several campaigns to raise awareness of the
                        importance of vaccination against this virus. Tawuniya
                        continued to provide full support and insurance
                        protection to the 21 "Taakad” centers for Covid-19
                        drive-through tests in the various regions of the
                        Kingdom, with the aim of strengthening national efforts
                        to combat coronavirus (Covid-19), and the Company also
                        organized several campaigns to raise awareness of the
                        importance of vaccination against this virus.
                      </p>
                      <p className="socialptag">
                        Tawuniya continued to provide full support and insurance
                        protection to the 21 "Taakad” centers for Covid-19
                        drive-through tests in the various regions of the
                        Kingdom, with the aim of strengthening national efforts
                        to combat coronavirus (Covid-19), and the Company also
                        organized several campaigns to raise awareness of the
                        importance of vaccination against this virus.
                      </p>
                      <hr />
                      <p className="pt-1">Share article</p>
                      <img
                        src={Socialapp}
                        className="img-fluid pb-3"
                        alt="icon"
                      />{" "}
                    </div>
                  </Card>
                </div>

                {/* card2 end */}
              </div>

              <div class="col-7 mx-auto">
                <div className="d-flex  ">
                  <h2 className="fw-800 pt-5">Related Projects</h2>
                  <p
                    className="fs-18 pl-5 fw-800 pt-5 mt-1 ml-5  socialptag"
                    onClick={() => history.push("/home/all-products")}
                  >
                    View All Projects
                    <span className="pl-4 ml-4">
                      {" "}
                      <img
                        src={Rightarw}
                        className="img-fluid "
                        alt="icon"
                      />{" "}
                    </span>
                  </p>
                </div>
              </div>
              {/* --card 3---*/}

              <div class="col-7 mx-auto pt-5">
                <Card className="socialprojectcard3 ">
                  <div className="container">
                    <img src={Tag} className="img-fluid pt-4" alt="icon" />{" "}
                    <h5 className="fs-20 fw-800 pt-4">
                      Tawuniya is the first company in the Kingdom to install
                      vehicle insurance policies
                    </h5>
                    <p className=" socialptag pt-2">
                      The Company launched the "Aljar Lil Jar" initiative
                      through our social responsibility program (Tawuniyat
                      Al-Kheir) where we shared the joy of Ramadan with our
                      neighbors in Riyadh, Khobar and Jeddah.
                    </p>
                    <p className="socialptag fs-18 fw-700">
                      Read the Full Story{" "}
                      <span className="pl-4">
                        {" "}
                        <img
                          src={Rightarw}
                          className="img-fluid "
                          alt="icon"
                        />{" "}
                      </span>
                    </p>
                  </div>
                </Card>
              </div>

              {/* ----card3 end */}

              {/* card 4 start */}

              <div class="col-7 pt-4 mx-auto pb-5">
                <Card className="socialprojectcard3 ">
                  <div className="container">
                    <img src={Tag2} className="img-fluid pt-4" alt="icon" />{" "}
                    <h5 className="fs-20 fw-800 pt-4">
                      Tawuniya is the first company in the Kingdom to install
                      vehicle insurance policies
                    </h5>
                    <p className="socialptag pt-2">
                      On the occasion of the International Children's Cancer
                      Day, Tawuniya implemented the "We achieved their wishes"
                      initiative in its second chapter for the children of King
                      Abdullah Specialist Hospital in Riyadh in cooperation with
                      the National Center for Social Responsibility to support
                      their fight against cancer.{" "}
                    </p>
                    <p className="mainBannerPara fs-18 fw-700">
                      Read the Full Story{" "}
                      <span className="pl-4">
                        {" "}
                        <img
                          src={Rightarw}
                          className="img-fluid "
                          alt="icon"
                        />{" "}
                      </span>
                    </p>
                  </div>
                </Card>
              </div>
            </div>

            {/* card end */}
          </div>

          <div></div>
        </div>
      )}
    </React.Fragment>
  );
};
