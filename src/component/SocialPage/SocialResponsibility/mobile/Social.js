import ArrowForward from "@material-ui/icons/ArrowForward";
import React from "react";
import nextkey from "../../../../assets/images/mobile/nextkey.png";
import phone from "../../../../assets/news/phone.png";
import fax from "../../../../assets/news/fax.png";
import email from "../../../../assets/news/email.png";
import location from "../../../../assets/news/locate.png";
import "./style.scss";
function Social() {
  return (
    <div>
      <div className="container social_responsibilities_content">
        <div className="">
          <span>Social Contributions </span>
        </div>
        <p>
          At Tawuniya, we know that our company thrives when our partners and
          communities thrive. We value our shared success and are dedicated to
          empowering our employees and communities now and in the future.
        </p>

        <div></div>
      </div>
      {/* <div>
        <div className="social_responsabilities_card">
          <div className="ms-2">
            <div className="card responsibility_scroll_card ">
              <div>
                <img className="ms-2 mt-2" src={tag} />
              </div>
              <div className="ms-3">
                <h6 className="mt-1 mb-2 mx-2">
                  Tawuniya is the first company in the Kingdom to install
                  vehicle insurance policies
                </h6>
                <p className="responsibility_scroll_content mb-2 mx-2">
                  Tawuniya continued to provide full support and insurance
                  protection to the 21 "Taakad” centers for Covid-19
                  drive-through tests in the various regions of the Kingdom,
                  with the aim of strengthening national efforts to combat
                  coronavirus (Covid-19), and the Company also organized several
                  campaigns to raise awareness of the importance of vaccination
                  against this virus.
                </p>
                <div className="d-flex">
                  <div>
                <h5 className='mx-2 my-2'>
                Read the Full Story
                </h5></div>
                <div>
                <img src={nextkey}/></div>
                </div>
              </div>
            </div>
          </div>

          <div className="ms-2">
            <div className="card responsibility_scroll_card ">
              <div>
                <img className="ms-2 mt-2" src={tag} />
              </div>
              <div className="ms-3">
                <h6 className=" mx-2">
                  Tawuniya is the first company in the Kingdom to install
                  vehicle insurance policies
                </h6>
                <p className="responsibility_scroll_content mb-2 mx-2">
                  Tawuniya continued to provide full support and insurance
                  protection to the 21 "Taakad” centers for Covid-19
                  drive-through tests in the various regions of the Kingdom,
                  with the aim of strengthening national efforts to combat
                  coronavirus (Covid-19), and the Company also organized several
                  campaigns to raise awareness of the importance of vaccination
                  against this virus.
                </p>
                <div className="d-flex">
                  <div>
                <h5 className='mx-2 my-2'>
                Read the Full Story
                </h5></div>
                <div>
                <img src={nextkey}/></div>
                </div>
              </div>
            </div>
          </div>
        </div>
        
      </div> */}
      <div>
        <div className="my-4 contact_us container">
          <h6>Contact us</h6>
        </div>
        <div className="contact_us_black_box">
          <div className="d-flex">
            <img
              className="black_box_image_phone"
              src={phone}
              width="20"
              height="20"
            />
            <div className="">
              <div className="black_box_phone ">Phone</div>
              <div className="black_box_phno ">+966 11 252 5800</div>
            </div>
          </div>
          <hr />

          <div className="d-flex">
            <img
              className="black_box_image_fax"
              src={fax}
              width="20"
              height="20"
            />
            <div className="">
              <div className="black_box_fax ">Fax</div>
              <div className="black_box_faxno ">+966 11 400 0844</div>
              <div className="fax_chat">This is a chat only number</div>
            </div>
          </div>

          <hr />

          <div className="d-flex">
            <img
              className="black_box_image_email"
              src={email}
              width="20"
              height="20"
            />
            <div className="">
              <div className="black_box_email ">Email</div>
              <div className="black_box_emailid">info@tawuniya.com.sa</div>
            </div>
          </div>

          <hr />

          <div className="d-flex">
            <img
              className="black_box_image_box"
              src={fax}
              width="20"
              height="20"
            />
            <div className="">
              <div className="black_box_faxbox ">P.O Box</div>
              <div className="black_box_boxno">86959</div>
            </div>
          </div>
          <hr />

          <div className="d-flex">
            <img
              className="black_box_image_location"
              src={location}
              width="20"
              height="20"
            />
            <div className="">
              <div className="black_box_location ">Address</div>
              <div className="black_box_address">
                6507 Thomamah Road (Takhassusi) - <br />
                Ar Rabi, Riyadh 11632
              </div>
            </div>
          </div>
          <div className="mb-5"></div>
        </div>
      </div>
    </div>
  );
}

export default Social;
