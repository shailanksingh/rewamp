import React, { useState } from "react";
import HeaderStepsSticky from "component/common/MobileReuseable/HeaderStepsSticky";
import HeaderStickyMenu from "component/common/MobileReuseable/HeaderStickyMenu";
import SocialResponsibilityContactUs from "component/common/MobileReuseable/SocialResponsibilityContact";
import health from "assets/about/health.png";
import safety from "assets/about/safety.png";
import productivity from "assets/about/productivity.png";
import environment from "assets/about/environment.png";
import downarrow from "assets/about/downarrow.svg";
import "./style.scss"

const SocialResponsibilityMobile = () => {
  const data = [
    {
      id: 1,
      image: health,
      title: "Health",
      des: "We’re partnering with communities and local leaders to make sure our environmental efforts are also a force for equity and justice",
    },
    {
      id: 2,
      image: safety,
      title: "Safety",
      des: "We work every day to put people first - by empowering them with accessible technology, being a force for equity and opportunity.",
    },
    {
      id: 3,
      image: productivity,
      title: "Productivity",
      des: "helps foster principled actions, informed and effective decision-making, and appropriate monitoring of our compliance and performance.",
    },
    {
      id: 4,
      image: environment,
      title: "Environmental",
      des: "We’re partnering with communities and local leaders to make sure our environmental efforts are also a force for equity and justice",
    },
  ];

  const content = [
    {
      id: 1,
      title: "Health",
      des: "Tawuniya is the first company in the Kingdom to install vehicle insurance policies",
    },
    {
      id: 2,
      title: "Productivity",
      des: "Tawuniya is the first company in the Kingdom to install vehicle insurance policies",
    },
    {
      id: 3,
      title: "Safety",
      des: "Tawuniya is the first company in the Kingdom to install vehicle insurance policies",
    },
    {
      id: 4,
      title: "Health",
      des: "Tawuniya is the first company in the Kingdom to install vehicle insurance policies",
    },
    {
      id: 5,
      title: "Productivity",
      des: "Tawuniya is the first company in the Kingdom to install vehicle insurance policies",
    },
    {
      id: 6,
      title: "Safety",
      des: "Tawuniya is the first company in the Kingdom to install vehicle insurance policies",
    },
    {
      id: 7,
      title: "Environmental",
      des: "Tawuniya is the first company in the Kingdom to install vehicle insurance policies",
    },
    {
      id: 8,
      title: "Health",
      des: "Tawuniya is the first company in the Kingdom to install vehicle insurance policies",
    },
  ];

  return (
    <div className="social_responsibility_mobile">
      <HeaderStickyMenu />
      <HeaderStepsSticky title={"Social Responsability"} />
      <div className="responsibility_thriving">Thriving Together</div>
      <div className="responsibility_tawuniya">
        At Tawuniya, we know that our company thrives when our partners and
        communities thrive. We value our shared success and are dedicated to
        empowering our employees and communities now and in the future.
      </div>
      <div className="responsibility_adopt">Adopting the CSR Strategy</div>
      <div className="responsibility_black row">
        {data.map((data) => (
          <div key={data.id} className="responsibility_white">
            <div className="responsibility_image">
              <img alt="..." src={data.image} />
            </div>
            <div className="responsibility_title">{data.title}</div>
            <div className="responsibility_datas">{data.des}</div>
          </div>
        ))}
      </div>
      <div className="responsibility_contribution">Social Contributions</div>
      <div className="responsibility_company">
        At Tawuniya, we know that our company thrives when our partners and
        communities thrive. We value our shared success and are dedicated to
        empowering our employees and communities now and in the future.
      </div>
      <div className="responsibility_year">
        <div>
          <div className="responsibility_one">Years</div>
          <div className="responsibility_two">2021</div>
        </div>
        <div className="responsibility_arow">
          <img alt="..." src={downarrow} />
        </div>
      </div>
      <div className="responsibility_social row">
        {content.map((contents) => (
          <div key={contents.title} className="responsibility_data">
            <div className="responsibility_blue">{contents.title}</div>
            <div className="responsibility_content">{contents.des}</div>
          </div>
        ))}
      </div>
      <div className="responsibility_contact">Contact Us</div>
      <SocialResponsibilityContactUs />
    </div>
  );
};

export default SocialResponsibilityMobile;
