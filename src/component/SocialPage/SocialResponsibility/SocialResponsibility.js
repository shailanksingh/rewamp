import React, { useState, useEffect } from "react";
import { Card, Button, Form } from "react-bootstrap";
import customerHome from "assets/svg/customerHome.svg";
import customerBreadCrumb from "assets/svg/customerBreadCrumb.svg";
import Socialicon1 from "../../../assets/svg/Social/socialicon1.svg";
import Socialicon2 from "../../../assets/svg/Social/socialicon2.svg";
import Socialicon3 from "../../../assets/svg/Social/socialicon3.svg";
import Socialicon4 from "../../../assets/svg/Social/socialicon4.svg";
import tag1 from "../../../assets/svg/Social/tag1.svg";
import tag2 from "../../../assets/svg/Social/tag2.svg";
import tag3 from "../../../assets/svg/Social/tag3.svg";
import tag4 from "../../../assets/svg/Social/tag4.svg";
import Rightarw from "../../../assets/svg/Social/rightarw.svg";
import Flight from "../../../assets/svg/flight.svg";
import Phone from "../../../assets/svg/phone.svg";
import Line from "../../../assets/svg/line.svg";
import Chat from "../../../assets/svg/chaticon.svg";
import Communication from "../../../assets/svg/communication.svg";
import Fax from "../../../assets/svg/Social/fax.svg";
import location from "../../../assets/svg/location.svg";
import { history } from "service/helpers";

import "./style.scss";
import SocialResponsibilityMobile from "./mobile/SocialResponsibilityMobile";

export const SocialResponsibility = () => {
  return (
    <React.Fragment>
      {window.innerWidth < 600 ? (
        <SocialResponsibilityMobile />
      ) : (
        <div>
          <div className="SocialMaincontent pt-4">
            <h1 className="mainBannerHeading  fw-800 pt-5 ">
              Thriving <span className="socialh2">Together</span>
            </h1>

            <div className="row pb-4">
              <div className="col-lg-6">
                <p className="socialptag fs-18  fw-800 pt-4 ">
                  At Tawuniya, we know that our company thrives when our
                  partners and communities thrive. We value our shared success
                  and are dedicated to empowering our employees and communities
                  now and in the future.
                </p>
              </div>
            </div>

            {/* card start */}
            <div className="pt-3">
              <Card className="social-card socialcardbg">
                <div className="row">
                  <div className="col-lg-6 socialcard-ptag">
                    <p className="mainBannerPara cardptag socialtabpatg  text-center pt-5">
                      At Tawuniya, our CSR vision is ‘Together for Tomorrow!
                      Enabling People’. By providing Education for Future
                      Generations, we empower future innovators to achieve their
                      full potential and become the next generations of leaders
                      to pioneer positive social change and build a better world
                      for all.
                    </p>
                  </div>
                </div>

                {/* inner card start----------------------- */}
                <div className=" col-lg-11 align-centercard">
                  <Card className="social-card2 aligncard2">
                    <h2 className="text-center fw-800 pt-4">
                      Adopting the CSR
                      <span className="socialh2"> Strategy</span>
                    </h2>

                    <div className="row">
                      <div className="col-lg-6 align-centercard">
                        <p className="text-center socialptag mainBannerPara pt-4">
                          The Corporate Social Responsibility Strategy (CSR) of
                          Tawuniya was <br /> developed based on the following
                          four pillars:
                        </p>
                      </div>
                    </div>

                    {/* ------------- */}

                    {/* ------------inner 4 card start */}

                    <div className="container pt-3">
                      <div className="row ">
                        <div className="col-lg-3">
                          <Card className="social-card3">
                            <div className="container">
                              <img
                                className="img-fluid pt-4"
                                src={Socialicon1}
                              />
                              <h5 className="pt-4 fw-800">Health</h5>
                              <p className="mainBannerPara pt-1">
                                We’re partnering with communities and local
                                leaders to make sure our environmental efforts
                                are also a force for equity and justice{" "}
                              </p>
                            </div>
                          </Card>
                        </div>

                        <div className="col-lg-3">
                          <Card className="social-card3">
                            <div className="container">
                              <img
                                className="img-fluid pt-4"
                                src={Socialicon2}
                              />
                              <h5 className="pt-4 fw-800">Safety</h5>

                              <p className="  mainBannerPara pt-1">
                                We work every day to put people first - by
                                empowering them with accessible technology,
                                being a force for equity and opportunity.
                              </p>
                            </div>
                          </Card>
                        </div>

                        <div className="col-lg-3">
                          <Card className="social-card3">
                            <div className="container">
                              <img
                                className="img-fluid pt-4"
                                src={Socialicon3}
                              />
                              <h5 className="pt-4 fw-800">Productivity</h5>
                              <p className="mainBannerPara pt-1">
                                helps foster principled actions, informed and
                                effective decision-making, and appropriate
                                monitoring of our compliance and performance.
                              </p>
                            </div>
                          </Card>
                        </div>

                        <div className="col-lg-3">
                          <Card className="social-card3">
                            <div className="container">
                              <img
                                className="img-fluid pt-4"
                                src={Socialicon4}
                              />
                              <h5 className="pt-4 fw-800">Environmental</h5>
                              <p className="  mainBannerPara pt-1">
                                We’re partnering with communities and local
                                leaders to make sure our environmental efforts
                                are also a force for equity and justice
                              </p>
                            </div>
                          </Card>
                        </div>
                      </div>

                      <div className="container pt-5 pb-4 ">
                        <div className="row">
                          <div className="col-lg-9 align-centercard">
                            <p className="text-center mainBannerPara ">
                              In line with this strategy, Tawuniya has developed
                              a precise mechanism to evaluate social
                              responsibility programs, which resulted in a
                              comprehensive change in the company's methodology
                              for selecting programs and activating them in
                              coordination with social institutions. The CSR
                              objectives, initiatives and activities are planned
                              in the light of the adopted social responsibility
                              strategy.
                            </p>
                          </div>
                        </div>
                      </div>
                    </div>

                    {/* -------inner 4 card end--- */}
                  </Card>
                  {/* ------------inner card end----------- */}
                </div>
              </Card>

              {/* -------card2 */}
            </div>

            {/* main card end */}

            {/* ------section2 */}
            <div className="section2">
              <h1 className="mainBannerHeading fw-800">Social Contributions</h1>

              <div className="row">
                <div className="col-lg-6">
                  <p className="socialptag mainBannerPara pt-4">
                    At Tawuniya, we know that our company thrives when our
                    partners and communities thrive. We value our shared success
                    and are dedicated to empowering our employees and
                    communities now and in the future.
                  </p>
                </div>
              </div>

              <div className="text-center pt-3d-flex">
                <span className="yearLabel">Year</span>

                <select class="selectdropdown  ">
                  <option value="1">2020</option>

                  <option text="Year" value="2">
                    2021
                  </option>
                  <option value="3">2022</option>
                </select>
              </div>

              <div className="row pt-5">
                <div className="col-lg-3">
                  <Card className="social-card2">
                    <div className="container">
                      <img src={tag1} className="img-fluid pt-3" />
                      <h6 className="pt-3 fw-800">
                        Tawuniya is the first company in the Kingdom to install
                        vehicle insurance policies
                      </h6>
                      <p className=" mainBannerPara pt-1">
                        Tawuniya continued to provide full support and insurance
                        protection to the 21 "Taakad” centers for Covid-19
                        drive-through tests in the various regions of the
                        Kingdom, with the aim of strengthening national efforts
                        to combat coronavirus (Covid-19), and the Company also
                        organized several campaigns to raise awareness of the
                        importance of vaccination against this virus.
                      </p>
                      <h6
                        className="pt-1 fw-800 pb-4 cursor-pointer"
                        onClick={() => history.push("/home/social/project")}
                      >
                        Read the Full Story{" "}
                        <img className="img-fluid pl-4" src={Rightarw} />
                      </h6>
                    </div>
                  </Card>
                </div>
                <div className="col-lg-3">
                  <Card className="social-card2">
                    <div className="container">
                      <img src={tag2} className="img-fluid pt-3" />

                      <h6 className="pt-5 fw-800">
                        Tawuniya is the first company in the Kingdom to install
                        vehicle insurance policies
                      </h6>
                      <p className=" mainBannerPara pt-2">
                        Based on our belief in the values of our society and
                        embodying our active social role, Tawuniya has
                        participated in the national campaign for charitable
                        work and donated one million Saudi riyals through the
                        "Ehsan" platform.
                      </p>
                      <h6
                        className="pt-1 fw-800 pb-4 cursor-pointer"
                        onClick={() => history.push("/home/social/project")}
                      >
                        Read the Full Story{" "}
                        <img className="img-fluid pl-4" src={Rightarw} />
                      </h6>
                    </div>
                  </Card>
                </div>
                <div className="col-lg-3">
                  <Card className="social-card2">
                    <div className="container">
                      <img src={tag3} className="img-fluid pt-3" />

                      <h6 className="pt-5 fw-800">
                        Tawuniya is the first company in the Kingdom to install
                        vehicle insurance policies
                      </h6>
                      <p className=" mainBannerPara pt-2">
                        The Company launched the "Aljar Lil Jar" initiative
                        through our social responsibility program (Tawuniyat
                        Al-Kheir) where we shared the joy of Ramadan with our
                        neighbors in Riyadh, Khobar and Jeddah.
                      </p>
                      <h6
                        className="pt-1 fw-800 pb-4 cursor-pointer"
                        onClick={() => history.push("/home/social/project")}
                      >
                        Read the Full Story{" "}
                        <img className="img-fluid pl-4" src={Rightarw} />
                      </h6>
                    </div>
                  </Card>
                </div>
                <div className="col-lg-3">
                  <Card className="social-card2">
                    <div className="container">
                      <img src={tag4} className="img-fluid pt-3" />

                      <h6 className="pt-5 fw-800">
                        Tawuniya is the first company in the Kingdom to install
                        vehicle insurance policies
                      </h6>
                      <p className=" mainBannerPara pt-2">
                        On the occasion of the International Children's Cancer
                        Day, Tawuniya implemented the "We achieved their wishes"
                        initiative in its second chapter for the children of
                        King Abdullah Specialist Hospital in Riyadh in
                        cooperation with the National Center for Social
                        Responsibility to support their fight against cancer.
                      </p>
                      <h6
                        className="pt-1 fw-800 pb-4 cursor-pointer"
                        onClick={() => history.push("/home/social/project")}
                      >
                        Read the Full Story{" "}
                        <img className="img-fluid pl-4" src={Rightarw} />
                      </h6>
                    </div>
                  </Card>
                </div>

                {/* -- */}
                <div className="col-lg-3 pt-4">
                  <Card className="social-card2 ">
                    <div className="container">
                      <img src={tag4} className="img-fluid pt-3" />

                      <h6 className="pt-5 fw-800">
                        Tawuniya is the first company in the Kingdom to install
                        vehicle insurance policies
                      </h6>
                      <p className=" mainBannerPara pt-2">
                        Tawuniya cooperated with the "Naqa" Association to
                        implement several activities to raise awareness of the
                        harms of tobacco and help those wishing to quit smoking,
                        on the occasion of the World No Tobacco Day.
                      </p>
                      <h6
                        className="pt-1 fw-800 pb-4 cursor-pointer"
                        onClick={() => history.push("/home/social/project")}
                      >
                        Read the Full Story{" "}
                        <img className="img-fluid pl-4" src={Rightarw} />
                      </h6>
                    </div>
                  </Card>
                </div>

                <div className="col-lg-3">
                  <Card className="social-card2">
                    <div className="container">
                      <img src={tag3} className="img-fluid pt-3" />

                      <h6 className="pt-5 fw-800">
                        Tawuniya is the first company in the Kingdom to install
                        vehicle insurance policies
                      </h6>
                      <p className=" mainBannerPara pt-2">
                        Tawuniya interacted with international days and health
                        awareness months to improve the health of different
                        groups of Saudi society by implementing a number of
                        campaigns and events, most notably the activities of the
                        International Breast Cancer Month, the International Day
                        for Persons with Special Needs, the World Heart Day, and
                        the World Diabetes Day.
                      </p>
                      <h6
                        className="pt-1 fw-800 pb-4 cursor-pointer"
                        onClick={() => history.push("/home/social/project")}
                      >
                        Read the Full Story{" "}
                        <img className="img-fluid pl-4" src={Rightarw} />
                      </h6>
                    </div>
                  </Card>
                </div>

                <div className="col-lg-3">
                  <Card className="social-card2">
                    <div className="container">
                      <img src={tag2} className="img-fluid pt-3" />

                      <h6 className="pt-5 fw-800">
                        Tawuniya is the first company in the Kingdom to install
                        vehicle insurance policies
                      </h6>
                      <p className=" mainBannerPara pt-2">
                        Under the slogan "We can take a selfie" and on the
                        occasion of the International Prostate Cancer Month, the
                        Tawuniya organized several events to raise awareness of
                        men's health in Riyadh, Jeddah and Dammam.
                      </p>
                      <h6
                        className="pt-1 fw-800 pb-4 cursor-pointer"
                        onClick={() => history.push("/home/social/project")}
                      >
                        Read the Full Story{" "}
                        <img className="img-fluid pl-4" src={Rightarw} />
                      </h6>
                    </div>
                  </Card>
                </div>

                <div className="col-lg-3">
                  <Card className="social-card2">
                    <div className="container">
                      <img src={tag1} className="img-fluid pt-3" />

                      <h6 className="pt-5 fw-800">
                        Tawuniya is the first company in the Kingdom to install
                        vehicle insurance policies
                      </h6>
                      <p className=" mainBannerPara pt-2">
                        The Company carried out several activities during the
                        National Day celebrations to spread awareness of the
                        Kingdom's civilization, culture and its tourist and
                        heritage areas by organizing a series of competitions
                        and recreational trips to the National Museum and the
                        city of Al-Ula.
                      </p>
                      <h6
                        className="pt-1 fw-800 pb-4 cursor-pointer"
                        onClick={() => history.push("/home/social/project")}
                      >
                        Read the Full Story{" "}
                        <img className="img-fluid pl-4" src={Rightarw} />
                      </h6>
                    </div>
                  </Card>
                </div>
              </div>
            </div>

            {/* section 2 end */}

            <div className="container pt-5 ">
              <h4 className="text-center fs-30 fw-800 pb-4">Contact us</h4>
              <Card className="social-card4 ">
                <div className="container-fluid">
                  <div className="row">
                    <div className="col-lg-0 pt-3">
                      <div className="container">
                        <img src={Phone} />
                      </div>
                    </div>

                    <div className="col-lg-3">
                      <p className="mainBannerPara medical-ptag pt-3">Phone</p>
                      <h6 className="mainBannerHeading">+966 11 252 5800</h6>
                    </div>

                    <div className="col-lg-0 pt-3">
                      <div className="container">
                        <img src={Fax} />
                      </div>
                    </div>

                    <div className="col-lg-3">
                      <p className="mainBannerPara medical-ptag pt-3">Fax</p>
                      <h6 className="mainBannerHeading">+966 11 400 0844</h6>
                    </div>

                    <div className="col-lg-0 pt-3">
                      <div className="container">
                        <img src={Communication} />
                      </div>
                    </div>

                    <div className="col-lg-4 emailui">
                      <p className="mainBannerPara pt-3"> Email</p>

                      <h6 className="mainBannerHeading">
                        Care@tawuniya.com.sa{" "}
                      </h6>
                    </div>
                  </div>

                  <hr className="hrtag" />

                  <div className="row">
                    <div className="col-lg-0 pt-1">
                      <div className="container">
                        <img src={Fax} />
                      </div>
                    </div>

                    <div className="col-lg-2">
                      <p className="mainBannerPara medical-ptag pt-1">
                        P.O.Box
                      </p>
                      <h6 className="mainBannerHeading">86959</h6>
                    </div>

                    <div className="col-lg-0 pt-1">
                      <div className="container">
                        <img src={location} />
                      </div>
                    </div>

                    <div className="col-lg-7 pb-4">
                      <p className="mainBannerPara medical-ptag pt-1">
                        Address
                      </p>
                      <h6 className="mainBannerHeading">
                        6507 Thomamah Road (Takhassusi) - Ar Rabi, Riyadh 11632
                      </h6>
                    </div>
                  </div>
                </div>
              </Card>
            </div>
          </div>

          <div>{/* -------------- */}</div>
        </div>
      )}
    </React.Fragment>
  );
};
