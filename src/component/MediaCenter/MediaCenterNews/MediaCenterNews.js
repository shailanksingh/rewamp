import React, { useState, useEffect, useRef } from "react";
import { Typography, Button } from "@material-ui/core";
import axios from "axios";
import { useSelector } from 'react-redux';

import MediaCenterHomeIcon from "../../../assets/svg/MediaCenterIcon/MediaCenterHome.svg";
import MediaCenterOrangeArrow from "../../../assets/svg/MediaCenterIcon/MediaCenterOrangeArrow.svg";
import MediaCenterBackArrow from "../../../assets/svg/MediaCenterIcon/MediaCenterBackArrow.svg";
import MediaCenterForwardArrow from "../../../assets/svg/MediaCenterIcon/MediaCenterForwardArrow.svg";
import ArrowForward from "../../../assets/svg/HomeServiceFroward1Arrow.svg";
import ArrowBack from "../../../assets/svg/HomeServiceBackArrow.svg";

import { MediaCenterData } from "../Schema/MediaCenterData";
import { MediaNewsLatest } from "../Schema/MediaNewsLatesData";
import MediaImgCard from "../../common/MediaCenterImgCard/MedaiCenterFeatureCard/index";
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import "./style.scss";
import "./medaiCenterNews-MedaiQuery.css";
import ReactPaginate from "react-paginate";
import LatestNewsCard from "../../common/MediaCenterImgCard/LatestNews/LatestNews";
import FirstButton from "../../../assets/svg/MediaCenterIcon/FirstButton.svg";
import LastButton from "../../../assets/svg/MediaCenterIcon/LastButton.svg";
import DownArrow from "../../../assets/svg/MediaCenterIcon/DownArrow.svg";
import PreButton from "../../../assets/svg/MediaCenterIcon/PreButton.svg";
import BackButton from "../../../assets/svg/MediaCenterIcon/BackButton.svg";
import { history } from "service/helpers";
import { formatToDMY } from 'service/utilities'

import { Theme, makeStyles } from "@material-ui/core";
const useStyles = makeStyles((theme) => ({
  dots: {
    zIndex: 0,
    "& li.slick-active button::before": {
      color: "#EE7500",
    },
    "& li": {
      width: "12px",
      "& button::before": {
        fontSize: theme.typography.pxToRem(9),
        color: "#4C565C",
      },
    },
  },
}));

export const MediaCenterNews = () => {
	const { newsUpdates } = useSelector(state => state.dashboardInformationReducer)

  const [newsData, setNewsData] = useState([]);
  const [featuredNews, setFeaturedNews] = useState([]);
  const [latestNews, setLatestNews] = useState([]);
  const [activeNewsType, setActiveNewsType] = useState('all')
  const [currentItems, setCurrentItems] = useState([]);
  const [pageCount, setPageCount] = useState(0);
  const [itemOffset, setItemOffset] = useState(0);
  // console.log(MediaCenterData[0]["MediaCenterImgCardData"]);
  const sliderRef = useRef(null);
  const classes = useStyles();

  const settings = {
    slidesToShow: 3,
    slidesToScroll: 1,
    arrows: false,
    dots: true,
    dotsClass: `slick-dots slickClass ${classes.dots}`,
  };

  useEffect(() => {
    if(Boolean(newsUpdates.length)) {
      setNewsData(newsUpdates)
      const fNews = []
      const lNews = []
      newsUpdates.forEach(news => {
        if(news.featured === 'Yes') {
          fNews.push({...news})
        } else {
          lNews.push({...news})
        }
      })
      setFeaturedNews(fNews)
      setLatestNews(lNews)
    }
  }, [newsUpdates])

  useEffect(() => {
    const BASIC_AUTH = {
      Username: "test@liferay.com",
      Password: "root",
    };
    axios
      .get(
        `http://localhost:8080/o/headless-delivery/v1.0/content-structures/40126/structured-contents?fields=dateCreated,contentFields.contentFieldValue.data,contentFields.contentFieldValue.image.contentUrl`,
        {
          headers: {
            "Access-Control-Allow-Origin": "*",
            Authorization: `Basic ${BASIC_AUTH}`,
            "Content-Type": "application/x-www-form-urlencoded; charset=UTF-8",
          },
          mode: "no-cors",
        }
      )
      .then((res) => {
        console.log(res);
      });
  }, []);

  useEffect(() => {
    // Fetch items from another resources.
    const endOffset = itemOffset + 10;
    console.log(`Loading items from ${itemOffset} to ${endOffset}`);
    setCurrentItems(
      MediaNewsLatest[0].MediaCenterLatestNewsData.slice(itemOffset, endOffset)
    );
    setPageCount(
      Math.ceil(MediaNewsLatest[0].MediaCenterLatestNewsData.length / 10)
    );
    console.log(currentItems);
  }, [itemOffset]);

  const handlePageClick = (event) => {
    const newOffset =
      (event.selected * 10) %
      MediaNewsLatest[0].MediaCenterLatestNewsData.length;
    setItemOffset(newOffset);
  };

  const handleNewsType = (type) => {
    if(type === 'all') {
      const fNews = newsUpdates.filter(news => news.featured === 'No')
      setLatestNews(fNews)
    } else {
      const fNews = newsUpdates.filter(news => news.featured === 'No' && news.category.toLowerCase() === type)
      setLatestNews(fNews)
    }
    setActiveNewsType(type)
  }

  return (
    <div className="mediaCenterContainer">
      {/* <div className="mediaHomeContainer">
        <img src={MediaCenterHomeIcon} alt="MediaCenterHomeIcon" />
        <p component={"p"} className="par">
          Media Center
        </p>
      </div> */}

      <div className="mediaMainImgCard">
        <button variant="contained" className="buttonEl">
          {newsData[0]?.category}
        </button>
        <div style={{ width: "100%" }}>
          <p className="mediaMainImgCardPar">
            {newsData[0]?.title}
          </p>

          <div className="mediaMainImgCardDateArrowContainer">
            {/* Date Container */}
            <div className="DateContainer">
              <p className="mediaMainImgDate">{formatToDMY(newsData[0]?.dateCreated)}</p>
              <p className="VerticalLine"></p>
              <div
                className="relatedContainer"
                onClick={() => history.push("/home/mediacenter/medianewsdetails", { title: newsData[0]?.title })}
              >
                <p
                  className="relatedPar"
                >
                  Read the full story{" "}
                </p>
                <img
                  src={MediaCenterOrangeArrow}
                  alt="MediaCenterOrangeArrow"
                  style={{ marginBottom: "10px" }}
                />
              </div>
            </div>

            {/* button Container */}
            <div className="ButtonContainer">
              <button className="mediaCenterArrowButton">
                <img
                  src={MediaCenterForwardArrow}
                  alt="MediaCenterForwardArrow"
                />
              </button>
              <div className="dotContainer">
                <p className="colorActive"></p>
                <p className="dotEl"></p>
                <p className="dotEl"></p>
                <p className="dotEl"></p>
                <p className="dotEl"></p>
              </div>
              <button className="mediaCenterArrowButton">
                <img src={MediaCenterBackArrow} alt="MediaCenterBackArrow" />
              </button>
            </div>
          </div>
        </div>
      </div>

      <div className="mediaCenterFeatureContainer">
        <h1 className="mediaCenterHeading">Featured News</h1>
        <Slider
          ref={sliderRef}
          {...settings}
          className="mediaCenterFeatureImgCard"
        >
          {featuredNews.map((item, i) => {
            return <MediaImgCard news={item} key={i} />;
          })}
        </Slider>

        <div className="arrowDotContainer">
          <div
            className="arrowContainer"
            onClick={() => sliderRef.current.slickPrev()}
          >
            <img src={ArrowBack} />
          </div>
          <div className="dotContainer">
            {/* <p className="colorActive"></p>
            <p className="dotEl"></p>
            <p className="dotEl"></p>
            <p className="dotEl"></p>
            <p className="dotEl"></p> */}
          </div>
          <div
            style={{ zIndex: 3 }}
            className="arrowContainer"
            onClick={() => sliderRef.current.slickNext()}
          >
            <img src={ArrowForward} />
          </div>
        </div>
      </div>

      <div className="latestNews">
        <h1 className="latestNewsHeading">Latest News</h1>
        <div className="latestNewsMenuContainer">
          <p className={activeNewsType === 'all' ? "itemActive" : "menuItemsStyle"} onClick={() => handleNewsType('all')}>All News</p>
          <p className={activeNewsType === 'news' ? "itemActive" : "menuItemsStyle"} onClick={() => handleNewsType('news')}>News</p>
          <p className={activeNewsType === 'events' ? "itemActive" : "menuItemsStyle"} onClick={() => handleNewsType('events')}>Events</p>
          <p className={activeNewsType === 'promotions' ? "itemActive" : "menuItemsStyle"} onClick={() => handleNewsType('promotions')}>Promotion</p>
        </div>
        <div className="latestNewsCardContainer">
          {latestNews.map((item, i) => {
            return <LatestNewsCard news={item} key={i} />;
          })}
        </div>
        <div className="paginationMainContainer">
          <div className="paginationContainer">
            <div className="FirstLastButtonContainer">
              <img class="mb-3" src={FirstButton} />
              <p class="ml-3">First</p>
            </div>
            <img class="mb-4 ml-3" src={PreButton} />

            <ReactPaginate
              breakLabel="..."
              previousLabel={` Pre `}
              nextLabel={" Next "}
              onPageChange={handlePageClick}
              renderOnZeroPageCount={null}
              containerClassName="paginationStyle pagination"
              activeClassName="boxContainer"
              pageCount={5}
            />
            <img class="mb-4 mr-3" src={BackButton} />
            <div className="FirstLastButtonContainer">
              <p class="mr-3">Last</p>
              <img class="mb-3" src={LastButton} />
            </div>
          </div>
          <div className="pageNumberContainer">
            <div className="FirstLastButtonContainer">
              <p class="mr-2">10</p>
              <img class="mb-3" src={DownArrow} />
            </div>
            <p>
              <span class="ml-3">1</span>of <span>20</span>
            </p>
          </div>
        </div>
      </div>
    </div>
  );
};
