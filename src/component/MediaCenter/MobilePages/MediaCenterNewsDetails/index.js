import React from "react";
import "./style.scss";
import newsdetails from "assets/newsdetails/newsdetails.png";
import Facebook from "assets/newsdetails/Facebook.png";
import Twitter from "assets/newsdetails/Twitter.png";
import newstag from "assets/news/newstag.png";
import Linkedin from "assets/newsdetails/Linkedin.png";
import Whatsapp from "assets/newsdetails/Whatsapp.png";
import Messenger from "assets/newsdetails/Messenger.png";
import HeaderBackNav from "component/common/MobileReuseable/HeaderBackNav";
import RelatedNewsCard from "component/common/MediaCenterImgCard/MediaRelatedNews";
import { MediaRelatedNews } from "../../Schema/NewsRelatedData";
import { MediaCenterLatestRelatedNews } from "component/common/MockData";
import SocialResponsibilityProjects from "component/common/MobileReuseable/SocialResponsibilityProjects/SocialResponsibilityProjects";

const MediaCenterNewsDetailsMobile = () => {
  return (
    <div className="media_center_news_details_container">
      <HeaderBackNav pageName="Media Center" title="News" />
      <div className="cards">Trending</div>
      <h1 className=" mt-3 headline">
      Tawuniya is the first company in the Kingdom to install vehicle insurance policies{" "}
      </h1>
      <h2 className="date mt-3"> 7th November 2021</h2>
      <img className="media_image" src={newsdetails} alt="" />

      <div className="media_body">
        <div className="d-flex justify-content-center">
          <div className="media_text">
            <div className="media-para-one">
              The insurance industry is emerging as one of the important sectors
              in Saudi Arabia as the Kingdom diversifies its economy. It has
              been going through a structural evolution under the Vision 2030
              Financial Sector Development Program. In recent years under the
              supervision of the Saudi Central Bank, the Saudi insurance
              industry has shown flexibility in dealing with changing market
              trends and customer requirements. In line with this, and following
              the introduction of compulsory vehicle liability insurance in
              2002, demand has grown to provide customers with a range of
              payment options to ensure they are fully compliant.
            </div>
            <div className="media_para">
              In response, Tawuniya has introduced a pay-by-installment option
              that will be available for vehicle insurance customers who account
              for 22% of the total size of the Saudi insurance market, according
              to year-end results for 2020 issued by the Saudi Central Bank. The
              option will make vehicle insurance policies more accessible and
              increase the percentage of vehicles insured under the mandatory
              insurance scheme.
            </div>
            <hr></hr>
            <p className="media_artical">Share article</p>
            <div className="media-image-face ">
              <img
                className="mx-1 "
                src={Facebook}
                alt="Facebook"
    
              />
              <img className="mx-1" src={Twitter}  />
              <img className="mx-1" src={Linkedin}  />
              <img className="mx-1" src={Whatsapp}  />
              <img className="mx-1" src={Messenger}  />
            </div>
          </div>
        </div>
      </div>
      <div className="related_news mx-3">Related News</div>
{/* 
      {MediaCenterLatestRelatedNews.map((item, i) => {
        return <RelatedNewsCard Item={item} key={i} isMobile={true} />;
      })} */}
      <SocialResponsibilityProjects
       days="10 May 2022"
       title="Tawuniya appoints Dr. Ammr Kurdi as Chief Financial Officer"/>
        <SocialResponsibilityProjects
       days="2 days ago"
       title="Tawuniya is the first company in the Kingdom to install vehicle insurance policies"/>
        <SocialResponsibilityProjects
       days="Week ago"
       title="Tawuniya launches Covid-19 Travel Insurance program"/>
       <SocialResponsibilityProjects
       days="2 weeks ago"
       title="“Tawuniya Vitality” changes the healthy lifestyle of the Saudi
        Society New program is a first for the Kingdom, the Middle East and North Africa"/>
       <SocialResponsibilityProjects
       days="10 May 2022"
       title="Tawuniya appoints Dr. Ammr Kurdi as Chief Financial Officer"/>
  
    </div>
  );
};

export default MediaCenterNewsDetailsMobile;
