import React ,{useState}from 'react';
import arrow from "assets/news/arrow.png";
import line from 'assets/news/verticalline.png'
import news from 'assets/news/News.png'

import "./style.scss"
function Tawuniya() {
    const [datas, setDatas] = useState([
        {
          id: 1,
          
          description: "7th November 2021",
          checked: false,
          class: "test11"
        },
      
        {
          id: 2,
         
          description: "7th November 2021",
          checked: false,
          class: "test12"
        },
        {
          id: 3,
         
          description: "7th November 2021",
          checked: false,
          class:"test13"
        },
      ]);
      
  return (
    <div>
      <div className='feature1'> Featured News </div>
       <div className='mx-3 pt-4 mt-4 pb-2'>
           
         </div>
       
        <div>
          <section>
            <div class="container-fluid">
              <div class="row">
                <div class="scrollcards ">
                  {datas.map((data, index) => (
                    <div
                      key={data.id}
                      className={" card card-style text-wrap "+data.class} 
                                      
                    >
                      <div className="d-flex flex-column w-100  text-wrap m-2">
                      <img  className='mt-2 news'src={news} height='40' width='70'/>
                        
                        <span className=" standard">Standard & Poor's assigns Tawuniya an<br/> "A-" rating; outlook "stable"</span>
                        <div className='d-flex '>
                        <div className='text-white november1'>{data.description}</div>
                        <img className ='line'src={line} width='1' height='15'/> 
                        <div className='readfullstory'>Read Full Story</div>
                        <div className='nexkey'><img src={arrow} height='10' width='10'></img></div></div>
                       
                       
                        
                      </div>
                  </div>
                  ))}
                </div>
              </div>
            </div>
          </section>
        </div>
        <div className='d-flex mt-2'>
        <div className='view1'>VIEW ALL NEWS</div>
        <img className='arrow1'src={arrow} height='10' width='10'/></div>

        <div className='latest mt-5'>Latest News</div>
        <div className='h'></div>
    </div>
  );
}

export default Tawuniya;
