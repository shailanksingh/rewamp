import React, { useState } from "react";
import News from "assets/news/News.png";
import ContentNews from "assets/news/ContentNews.png";
import image from "assets/news/i1.png";
import image1 from 'assets/news/i2.png';
import image2 from 'assets/news/i3.png';
import Leftspanarrow from 'assets/news/Leftspanarrow.png';
import Rightspanarrow from 'assets/news/Rightspanarrow.png';
import MediaCenterImage1 from 'assets/news/MediaCenterImage1.png';
import "./style.scss";
import { StylesContext } from "@material-ui/styles";
function Tawuniya() {
  const [datas, setDatas] = useState([
    {
      id: 1,
      image:MediaCenterImage1,
       },

    {
      id: 2,
      image:MediaCenterImage1,
    },
    {
      id: 3,
      image:MediaCenterImage1,
    },
  ]);


  const [content, setcontent] = useState([
    {
      id: 1,
      date: "2 days ago",
      description:"Tawuniya is the first company in the Kingdom to install vehicle insurance policies",
      title:"News"
    },
    {
      id: 2,
      date: "2 days ago",
      description:"Tawuniya is the first company in the Kingdom to install vehicle insurance policies",
      title:"News"
    },
    {
      id: 3,
      date: "2 days ago",
      description:"Tawuniya is the first company in the Kingdom to install vehicle insurance policies",
      title:"Promotion"
    },
    {
      id: 4,
      date: "2 days ago",
      description:"Tawuniya is the first company in the Kingdom to install vehicle insurance policies",
      title:"Event"
    },
    {
      id: 5,
      date: "2 days ago",
      description:"Tawuniya is the first company in the Kingdom to install vehicle insurance policies",
      title:"Promotion"
    },
    {
      id: 6,
      date: "2 days ago",
      description:"Tawuniya is the first company in the Kingdom to install vehicle insurance policies",
      title:"Event"
    },
    {
      id: 7,
      date: "2 days ago",
      description:"Tawuniya is the first company in the Kingdom to install vehicle insurance policies",
      title:"Event"
    },
      {
        id: 8,
        date: "2 days ago",
        description:"Tawuniya is the first company in the Kingdom to install vehicle insurance policies",
        title:"News"
      },
      {
        id: 9,
        date: "2 days ago",
        description:"Tawuniya is the first company in the Kingdom to install vehicle insurance policies",
        title:"News"
      },
      {
      id: 10,
      date: "2 days ago",
      description:"Tawuniya is the first company in the Kingdom to install vehicle insurance policies",
      title:"News"
    },

  ]);



  return (
    <div>
      <div className="media-center-topic">Media Center</div>
      <div class="container-fluid">
        <div class="row">
          <div class="our-feature">
            {datas.map((data) => (
              <div key={data.id} className="media-center-image" style={{backgroundImage: `url(${data.image})`}}>
               
                <div className="d-flex  flex-column justify-content-around media-center-body">
                <div className="mediacenter-title">{data.title}</div>
                  <div className="media-center-date">{data.date}</div>
                  <div className="media-center-description text-wrap">{data.description}</div>
                 
                </div>
              </div>
            ))}
          </div>
        </div>
      </div>
      <div className="media-news">News</div>
      <div className="contents-row">
          {content.map((contents) => (
            <div className="media-content">  
            <div className="mediacentercontent-title">{contents.title}</div>
            <div className="mediacentercontent-date">{contents.date}</div>
            <div className="mediacentercontent-description">{contents.description}</div> 
            </div>   
            ))}
          </div>
          <div className="Media-center-contentpages">
          <div className="Media-center-contentprevious"><img src={Leftspanarrow}/> <p>Prev</p></div>
          <div className="Media-center-pagenumbers"><span>1      of 20</span></div>
          <div className="Media-center-contentnext">Next <img src={Rightspanarrow}/></div>
          </div>
          <div className="h"></div>
        </div>
  );
}

export default Tawuniya;
