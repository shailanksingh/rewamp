import React, { useState } from "react";
import "./style.scss";
import HeaderStickyMenu from "component/common/MobileReuseable/HeaderStickyMenu";
import HeaderStepsSticky from "component/common/MobileReuseable/HeaderStepsSticky";
import media_news from "assets/images/mobile/media_news.png";

import Trending from "./Trending";
import FeaturedNews from "component/common/FeaturedNews/FeaturedNews";
import SlideCard from "component/common/SliderCard/SliderCard/SlideCard";
import FooterMobile from "component/common/MobileReuseable/FooterMobile";

const MediaCenterNewsMobile = () => {
  return (
    <div className="media_center_news_container">
      <HeaderStickyMenu />
      <HeaderStepsSticky title={"Media Center"} leftIcon={media_news} />
      <Trending />
       <FooterMobile/>
    </div>
  );
};

export default MediaCenterNewsMobile;
