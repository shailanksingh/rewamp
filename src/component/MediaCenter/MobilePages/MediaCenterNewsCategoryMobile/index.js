import React from "react";
import "./style.scss";
import HeaderStickyMenu from "component/common/MobileReuseable/HeaderStickyMenu";
import HeaderStepsSticky from "component/common/MobileReuseable/HeaderStepsSticky";
import media_center_icon from "assets/images/mobile/media_news.png";
import FeaturedNews from "component/common/FeaturedNews/FeaturedNews";

const MediaCenterNewsCategoryMobile = () => {
  return (
    <div className="media_center_news_category_container">
      <HeaderStickyMenu />
      <HeaderStepsSticky title={"Media Center"} leftIcon={media_center_icon} />
      <FeaturedNews/>
    </div>
  );
};

export default MediaCenterNewsCategoryMobile;
