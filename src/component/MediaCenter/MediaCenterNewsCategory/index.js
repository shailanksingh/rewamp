import React, { useState, useEffect, useRef } from "react";
import { Typography, Button } from "@material-ui/core";
import MediaCenterHomeIcon from "../../../assets/svg/MediaCenterIcon/MediaCenterHome.svg";
import MediaCenterOrangeArrow from "../../../assets/svg/MediaCenterIcon/MediaCenterOrangeArrow.svg";
import MediaCenterBackArrow from "../../../assets/svg/MediaCenterIcon/MediaCenterBackArrow.svg";
import MediaCenterForwardArrow from "../../../assets/svg/MediaCenterIcon/MediaCenterForwardArrow.svg";
import ArrowForward from "../../../assets/svg/HomeServiceFroward1Arrow.svg";
import ArrowBack from "../../../assets/svg/HomeServiceBackArrow.svg";
import GraterThan from "../../../assets/svg/MediaCenterIcon/BackGrterThan.svg";

import { MediaCenterData } from "../Schema/MediaCenterData";
import { MediaNewsLatest } from "../Schema/MediaNewsLatesData";
import MediaImgCard from "../../common/MediaCenterImgCard/MedaiCenterFeatureCard/index";
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import "./style.scss";
import "./MediaCenterNewsCategory-MediaQuery.css";

import ReactPaginate from "react-paginate";

import LatestNewsCard from "../../common/MediaCenterImgCard/LatestNews/LatestNews";
import FirstButton from "../../../assets/svg/MediaCenterIcon/FirstButton.svg";
import LastButton from "../../../assets/svg/MediaCenterIcon/LastButton.svg";
import DownArrow from "../../../assets/svg/MediaCenterIcon/DownArrow.svg";
import PreButton from "../../../assets/svg/MediaCenterIcon/PreButton.svg";
import BackButton from "../../../assets/svg/MediaCenterIcon/BackButton.svg";
import { Theme, makeStyles } from "@material-ui/core";
const useStyles = makeStyles((theme) => ({
  dots: {
    zIndex: 0,
    "& li.slick-active button::before": {
      color: "#EE7500",
    },
    "& li": {
      width: "12px",
      "& button::before": {
        fontSize: theme.typography.pxToRem(9),
        color: "#4C565C",
      },
    },
  },
}));

export const MediaNewsCategory = () => {
  const [currentItems, setCurrentItems] = useState([]);
  const [pageCount, setPageCount] = useState(0);
  const [itemOffset, setItemOffset] = useState(0);
  // console.log(MediaCenterData[0]["MediaCenterImgCardData"]);
  const sliderRef = useRef(null);
  const classes = useStyles();

  const settings = {
    slidesToShow: 3,
    slidesToScroll: 1,
    arrows: false,
    dots: true,
    dotsClass: `slick-dots slickClass ${classes.dots}`,
  };

  useEffect(() => {
    // Fetch items from another resources.
    const endOffset = itemOffset + 15;
    console.log(`Loading items from ${itemOffset} to ${endOffset}`);
    setCurrentItems(
      MediaNewsLatest[0].MediaCenterLatestNewsData.slice(itemOffset, endOffset)
    );
    setPageCount(
      Math.ceil(MediaNewsLatest[0].MediaCenterLatestNewsData.length / 15)
    );
    console.log(currentItems);
  }, [itemOffset]);

  const handlePageClick = (event) => {
    const newOffset =
      (event.selected * 15) %
      MediaNewsLatest[0].MediaCenterLatestNewsData.length;
    setItemOffset(newOffset);
  };

  return (
    <div className="mainContainerNewsCategory">
      <div className="mediaCenterFeatureContainer">
        <h1 className="mediaCenterHeading">Featured News</h1>
        <Slider
          ref={sliderRef}
          {...settings}
          className="mediaCenterFeatureImgCard"
        >
          {MediaCenterData[0].MediaCenterImgCardData.map((item, i) => {
            return <MediaImgCard Item={item} key={i} />;
          })}
        </Slider>

        <div className="arrowDotContainer">
          <div
            style={{ zIndex: 3 }}
            className="arrowContainer"
            onClick={() => sliderRef.current.slickPrev()}
          >
            <img src={ArrowBack} />
          </div>
          <div className="dotContainer">
            {/* <p className="dotEl"></p>
            <p className="dotEl"></p>
            <p className="dotEl"></p>
            <p className="dotEl"></p>
            <p className="dotEl"></p> */}
          </div>

          <div
            style={{ zIndex: 3 }}
            className="arrowContainer"
            onClick={() => sliderRef.current.slickNext()}
          >
            <img src={ArrowForward} />
          </div>
        </div>
      </div>

      <div className="latestNews">
        <h1 className="latestNewsHeading">Latest News</h1>
        <div className="latestNewsCardContainer">
          {currentItems.map((item, i) => {
            return <LatestNewsCard Item={item} key={i} />;
          })}
        </div>
        <div className="paginationMainContainer">
          <div className="paginationContainer">
            <div className="FirstLastButtonContainer">
              <img class="mb-3" src={FirstButton} />
              <p class="ml-3">First</p>
            </div>
            <img class="mb-4 ml-3" src={PreButton} />

            <ReactPaginate
              breakLabel="..."
              pageCount={5}
              previousLabel={" Pre "}
              nextLabel={" Next "}
              onPageChange={handlePageClick}
              renderOnZeroPageCount={null}
              containerClassName={"pagination paginationStyle"}
              activeClassName={"boxContainer"}
            />
            <img class="mb-4 mr-3" src={BackButton} />
            <div className="FirstLastButtonContainer">
              <p class="mr-3">Last</p>
              <img class="mb-3" src={LastButton} />
            </div>
          </div>
          <div className="pageNumberContainer">
            <div className="FirstLastButtonContainer">
              <p class="mr-2">10</p>
              <img class="mb-3" src={DownArrow} />
            </div>
            <p>
              <span class="mr-3">1</span>of20
            </p>
          </div>
        </div>
      </div>
    </div>
  );
};
