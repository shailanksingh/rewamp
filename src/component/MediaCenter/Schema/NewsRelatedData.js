import MediaCenterImgCard1 from "../../../assets/images/MediaCenterImgCard1.png";
import MediaCenterImgCard2 from "../../../assets/images/MediaCenterImgCard2.png";
import MediaCenterImgCard3 from "../../../assets/images/MediaCenterImgCard3.png";

export const MediaRelatedNews = [
  {
    MediaCenterLatestRelatedNews: [
      {
        buttonData: "News",
        date: "10 May 2022",
        description:
          "Tawuniya appoints Dr. Ammr Kurdi as Chief Financial Officer",
      },
      {
        buttonData: "News",
        date: "2 days ago",
        description:
          "Tawuniya is the first company in the Kingdom to install vehicle insurance policies",
      },
      {
        buttonData: "Promotion",
        date: "Week ago",
        description: "Tawuniya launches Covid-19 Travel Insurance program",
      },
      {
        buttonData: "News",
        date: "2 days ago",
        description:
          "Tawuniya is the first company in the Kingdom to install vehicle insurance policies",
      },
      {
        buttonData: "Promotion",
        date: "Week ago",
        description: "Tawuniya launches Covid-19 Travel Insurance program",
      },
    ],
  },
];
