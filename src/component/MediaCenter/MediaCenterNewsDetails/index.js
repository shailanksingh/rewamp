import React, { useEffect, useState } from "react";
import RelatedNewsCard from "../../common/MediaCenterImgCard/MediaRelatedNews/index";
import { MediaRelatedNews } from "../Schema/NewsRelatedData";
import { Card } from "react-bootstrap";
import customerHome from "assets/svg/customerHome.svg";
import customerBreadCrumb from "assets/svg/customerBreadCrumb.svg";
import Trend from "assets/svg/Social/trend.svg";
import Socialapp from "assets/svg/Social/socialapps.svg";
import Tag from "assets/svg/Social/tag3.svg";
import Tag2 from "assets/svg/Social/tag4.svg";
import Rightarw from "assets/svg/Social/rightarw.svg";
import leftArrowLight from "assets/svg/projectDetailLeftArrow.svg";
import rightArrowLight from "assets/svg/projectDetailRightArrow.svg";
import Slider from "react-slick";
import "./style.scss";
import { useHistory } from 'react-router-dom';
import { useSelector } from "react-redux";
import { formatToDMY } from 'service/utilities';
import SampleImg from '../../../assets/svg/Social/cardbg.svg'

export function MediaCenterNewsDetails() {
  const history = useHistory();
	const { newsUpdates } = useSelector(state => state.dashboardInformationReducer)

  const [newsArticle, setNewsArticle] = useState(null)

  useEffect(() => {
    if(Boolean(newsUpdates.length)) {
      if (history.location.state?.title) {
        const article = newsUpdates.find(item => item.title === history.location.state?.title)
        setNewsArticle(article)
      } else {
        setNewsArticle(newsUpdates[0])
      }
    }
  }, [newsUpdates, history.location.state?.title])

  if (!newsArticle) return <></>
  return (
    <React.Fragment>
      <div className="mainSocialContainer">
        <img
          src={leftArrowLight}
          className="img-fluid newsLeftArrow cursor-pointer"
          alt="leftarrow"
        />
        <img
          src={rightArrowLight}
          className="img-fluid newsRightArrow cursor-pointer"
          alt="leftarrow"
        />

        {/* ----------header end-- */}

        <div className="container ">
          {/* -------top section------------ */}

          <div className="row pt-5 ">
            <div className="col-lg-6 aligncenterdiv">
              <img src={Trend} className="img-fluid" alt="icon" />{" "}
              <h2 className="mainBannerPara fw-800">
                {newsArticle.title}
              </h2>
              <p>{formatToDMY(newsArticle.dateCreated)}</p>
              <img src={Socialapp} className="img-fluid pr-2" alt="icon" />{" "}
            </div>
          </div>

          {/* ----------end top section */}

          {/* card start */}
          <div className="container pt-5">
            <div class="row align-items-center">
              <div class="col-9 mx-auto">
                <Card className="socialprojectcard" style={{
                  background: `url(${SampleImg})`,
                  backgroundRepeat: 'no-repeat',
                }}></Card>
              </div>

              {/* card2 */}

              <div class="col-7 mx-auto">
                <Card className="socialprojectcard2">
                  <div className="container">
                    <p className="socialptag pt-4">
                      {newsArticle.description}
                    </p>
                    <hr />
                    <p className="pt-1">Share article</p>
                    <img
                      src={Socialapp}
                      className="img-fluid pb-3"
                      alt="icon"
                    />{" "}
                  </div>
                </Card>
              </div>

              {/* card2 end */}
            </div>

            <div class="col-7 mx-auto">
              <div className="d-flex  ">
                <h2 className="fw-800 pt-5">Related Projects</h2>
                <p className="fs-18 pl-5 fw-800 pt-5 mt-1 ml-5  socialptag">
                  View All Projects
                  <span className="pl-4 ml-4">
                    {" "}
                    <img
                      src={Rightarw}
                      className="img-fluid "
                      alt="icon"
                    />{" "}
                  </span>
                </p>
              </div>
            </div>
            {/* --card 3---*/}

            <div class="col-7 mx-auto pt-5">
              <div class="mb-5">
                {MediaRelatedNews[0].MediaCenterLatestRelatedNews.map(
                  (item, i) => {
                    return <RelatedNewsCard Item={item} key={i} />;
                  }
                )}
              </div>
            </div>
          </div>
        </div>
      </div>
    </React.Fragment>
  );
}
