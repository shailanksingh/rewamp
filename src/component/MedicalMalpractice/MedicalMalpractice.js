import React from "react";
import {
	AppBanner,
	BreakDownTable,
} from "../MotorPage/MotorInsuranceComponent";
import { Insurance } from "../MotorPage/MotorInsuranceComponent/Insurance";

// import { TableData } from "../MotorInsuranceComponent/MotorTableData";
import { TawuniyaAppSlider } from "component/common/TawuniyaAppSlider";
import { HomeServices } from "component/HomePage/LandingComponent";
import { ExtensionCard } from "../MotorPage/MotorInsuranceComponent";
import {
	headerBreakDownData,
	tableBreakDownData,
	headerPerkData,
	tablePerkData,
} from "component/common/MockData";
import "./style.scss";
import { MotorServicesData } from "../MotorPage/MotorInsuranceComponent/schema/MotorServicesData";
import FrequentlyAsked from "component/common/FrequentlyAsked";
import { FAQMotor } from "../MotorPage/MotorInsuranceComponent/schema/FAQmotor";
import { MotorBlogs } from "../MotorPage/MotorInsuranceComponent/MotorBlogs";
import { MotorBlogsData } from "../MotorPage/MotorInsuranceComponent/schema/MotorBlogsData";
import { MotorOtherProducts } from "../MotorPage/MotorInsuranceComponent";
import { motorOtherProductsData } from "../MotorPage/MotorInsuranceComponent/schema/MotorOtherProductsData";
import { WhyChooseTawuniya } from "../MotorPage/MotorInsuranceComponent/WhyChooseTawuniya";
import { FooterAbout } from "component/HomePage/LandingComponent";
import { MotorCards } from "../MotorPage/MotorInsuranceComponent/MotorCards";
import { CommonTable } from "component/common/CommonTable";
import carDamage from "assets/svg/damageCar.svg";
import xCircle from "assets/svg/xCircle.svg";
import { BannerProductForm } from "component/common/BannerProductForm/BannerProductForm";
import stethoscope from "../../assets/svg/stethoscope.svg";
import doctor from "../../assets/svg/doctor.svg";
import Gynecology from "../../assets/svg/Gynecology.svg";
import nurse from "../../assets/svg/nurse.svg";
import healthkit from "../../assets/svg/healthkit.svg";
import Paramedics from "../../assets/svg/Paramedics.svg";
import medmalproadv1 from "../../assets/images/medmalproadv1.png";
import medmalproadv2 from "../../assets/images/medmalproadv2.png";

export function MedicalMalpractice() {
	return (
		<React.Fragment>
			<div className="MotorInsuranceMainContainer">
				<AppBanner />
				<BannerProductForm />
				<div className="cardRightContainer">
					<div className="cardRightText">
						<h5>Person Eligible for this Program</h5>
						<p>
							This program has been duly approved by the Shariah Committee for
							rendering the insurance cover available amongst 4 options of
							compensation limits required pursuant to your needs. You can also
							obtain one insurance cover for up to 5 years with no
							intermittence.
						</p>
					</div>
					<div className="cardRightFeatures">
						<div className="cRFeaturesColumn">
							<div className="crCard crCard1">
								<img className="crCardIcon" src={stethoscope} alt="" />
								<p className="crCardIconDesc">
									Doctors of Other Medical Categories
								</p>
							</div>
							<div className="crCard crCard2">
								<img className="crCardIcon" src={doctor} alt="" />
								<p className="crCardIconDesc">Surgeons, Obstetrics</p>
							</div>
						</div>
						<div className="cRFeaturesColumn">
							<div className="crCard crCard3">
								<img className="crCardIcon" src={Gynecology} alt="" />
								<p className="crCardIconDesc">
									Gynecology and Anesthesia Specialists
								</p>
							</div>
							<div className="crCard crCard4">
								<img className="crCardIcon" src={nurse} alt="" />
								<p className="crCardIconDesc">Nurses and Technicians</p>
							</div>
						</div>
						<div className="cRFeaturesColumn">
							<div className="crCard crCard5">
								<img className="crCardIcon" src={healthkit} alt="" />
								<p className="crCardIconDesc">Pharmacists</p>
							</div>
							<div className="crCard crCard6">
								<img className="crCardIcon" src={Paramedics} alt="" />
								<p className="crCardIconDesc">Paramedics and Technicians</p>
							</div>
						</div>
					</div>
				</div>
				<div className="proAdvContainer">
					<h6>program advantages</h6>
					<p>We provide the best and trusted service for our customers</p>
					<div className="proAdvContentContainer">
						<div className="prosAdvContent">
							<img src={medmalproadv1} alt="" />
							<p className="prosAdvContentPara1">
								The policy provides protection against the financial
								consequences arising out from any error, omission or negligence
								committed in practicing the medical profession, The policy
								provides protection against the financial consequences arising
								out from any error, omission or negligence committed in
								practicing the medical profession
							</p>
						</div>
						<div className="prosAdvContent prosAdvContent2">
							<img src={medmalproadv2} alt="" />
							<p className="prosAdvContentPara2">
								The Insured's legal liability towards the third parties is
								covered for all financial amounts and legal costs pronounced
								against him, Proving the existence of insurance cover protects
								the Insured in the case he/ she has been deprived or prohibited
								from traveling outside the Kingdom during the course of any
								lawsuit versus him/ her
							</p>
						</div>
					</div>
				</div>
				<HomeServices HomeServicesData={MotorServicesData} />
				<FrequentlyAsked FAQ={FAQMotor} />
				<MotorBlogs />
				<MotorOtherProducts motorOtherProductsData={motorOtherProductsData} />
				<FooterAbout />
			</div>
		</React.Fragment>
	);
}
