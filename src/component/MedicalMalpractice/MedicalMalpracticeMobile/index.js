import React, { useState } from "react";
import "./style.scss";
import HeaderStepsSticky from "component/common/MobileReuseable/HeaderStepsSticky";
import HeaderStickyMenu from "component/common/MobileReuseable/HeaderStickyMenu";
import PolicyCard from "component/common/PolicyCard";
import {
  HomePolicyList
  // homecustomerServiceFaqList,
} from "component/common/MockData";
import { CommonFaq } from "component/common/CommonFaq";
import medical_bg_image from "assets/news/medical_bgimage.png";
import doctor from "assets/news/malpractice_doctor.png";
import gynaecology from "assets/news/malpractice_gynaecology.png";
import medical_arrow from "assets/news/arrow.png";

import medical_document from "assets/news/medical_document.png";

import tick from "assets/news/medical_tick.png";
import MobileSliderComp from "component/common/MobileReuseable/MobileSliderComp";
import MakeClaimCard from "component/common/MobileReuseable/MakeClaimCard";

import TawuniyaAppQuick from "component/common/MobileReuseable/TawuniyaAppQuick/Ithra";
import SupportRequestHelper from "component/common/MobileReuseable/SupportRequest";
import MyFamilyMobilePage from "component/MyFamilyMobilePage/Insurancecard";

const MedicalMalpracticeMobile = () => {
  const [datas, setDatas] = useState([
    {
      id: 1,
      description:
        "The policy is compatible with the laws and regulations of the Saudi Commission for Health Specialties",
      image: tick,
    },
    {
      id: 2,
      description:
        "The policy provides protection against the financial consequences arising from any error, omission, or negligence committed to practicing the medical profession",
      image: tick,
    },
    {
      id: 3,
      description:
        "Your legal liability towards the third parties is covered for all financial amounts and legal costs against you",
      image: tick,
    },
    {
      id: 4,
      description:
        "Provides a professional team of specialists in risk management to review the shops in order to assess the risks faced accurately and makes sound recommendations to control these risks and mitigate losses",
      image: tick,
    },
    {
      id: 5,
      description:
        "Proving the existence of insurance cover protects you in the case has been deprived or prohibited from traveling outside the Kingdom during the course of any lawsuit versus you",
      image: tick,
    },
  ]);

  const [programs, setPrograms] = useState([
    {
      id: 1,
      image: doctor,
      title: "Doctors of Other Medical Categories",
      description:
        "Health insurance will not only give your employees and their families enough financial security, but an overall sense of satisfaction that their employer actually cares about them.",
    },
    {
      id: 2,
      image: gynaecology,
      title: "Gynecology & Anesthesia Specialists",
      description:
        "Happy employees make happy workspaces and evidently successful companies! It’s no surprise that the safer and satisfied employees feel happier and motivated!",
    },
    {
      id: 3,
      image: gynaecology,
      title: "Pharmacists",
      description:
        "Health insurance will not only safeguard the employees' savings but, also enhance their overall mental well-being with the right support. ",
    },
    {
      id: 4,
      image: gynaecology,
      title: "Nurses & Technicians",
      description:
        "Safeguard your employees from the same, amongst other diseases; the earlier these issues are diagnosed, the earlier they can be treated and resolved. ",
    },
    {
      id: 5,
      image: gynaecology,
      title: "Paramedics & Technicians",
      description:
        "With more than 6400+ cashless hospitals across KSA, your employees can be covered at ease no matter where they are!",
    },
  ]);

  const [documents, setDocuments] = useState([
    {
      id: 1,
      title: "Medical Malpractice leaflet",
      image: medical_document,
    },
    {
      id: 2,
      title: "Medical Malpractice Insurance Claim Form",
      image: medical_document,
    },
    {
      id: 3,
      title: "Medical Malpractice Policy Wording",
      image: medical_document,
    },
    {
      id: 4,
      title: "Insurance Consumer Protection Principles",
      image: medical_document,
    },
    {
      id: 5,
      title: "Medical Malpractice Proposal",
      image: medical_document,
    },
    {
      id: 6,
      title: "Medical Malpractice Proposal Form",
      image: medical_document,
    },
  ]);
  return (
    <div className="mobile_medical_malpractice_container">
      <HeaderStickyMenu />
      <HeaderStepsSticky title="Medical Malpractice" buttonTitle="Buy Now" />

      <div className="medical_image">
        <img src={medical_bg_image} />
        <div className="medical_image_content">
          <h5>Insure yourself within minutes!</h5>
          <h6>
            The Saudi insurance pioneer, will help you to identify, analyze and
            manage such risks and suggest appropriate insurance solutions.
          </h6>
          {/* <div className="medical_white_box">
      <h4>National ID</h4>
      <input type='text' placeholder="Enter your ID number"></input>
      <div className="medical_hrline"></div>
      <h4>Year of Birth</h4>
      <input type='text' placeholder="Select year"/>
      <div className="medical_hrline"></div>
      <h4>Mobile Number</h4>
      <h4>+966 <input type='text' placeholder="5xxxxxxxx"></input></h4>
      <button>Buy Now</button>
    </div>  */}
          <p>
            By continuing you give Tawuniya advance consent to obtain my and/or
            my dependents' information from the National Information Center.
          </p>
        </div>
      </div>
      <h1>Mange Your Policy</h1>
      <div className="medical_malpractice_policy">
        <PolicyCard policyList={HomePolicyList} />
      </div>
      <div className="medical_personal">
        <h2>Person Eligible for this Program</h2>
        <p>
          This program has been duly approved by the Shariah Committee for
          rendering the insurance cover available amongst 4 options of
          compensation limits required pursuant to your needs. You can also
          obtain one insurance cover for up to 5 years with no intermittence.
        </p>
      </div>
      <div className="d-flex  medical_person_slider ">
        {programs.map((programs, index) => (
          <div key="id" className="medical_person_card">
            <img className="" src={programs.image} />
            <h5>{programs.title}</h5>
            <p>{programs.description}</p>
          </div>
        ))}
      </div>
      <h2>Program Advantages</h2>
      <div className="medical_program_cover">
        <h5>Program Cover</h5>
        <p>
          This includes any physical or mental injury to any patient committed
          by the Insured in the course of his work due to negligence, omission
          or error or be linked to the medical profession practiced in Saudi
          Arabia. The insurance cover available under this Program is for 4
          options of compensation limits required.
          <br />
          <div className="program_para">
            The cover includes all defense costs incurred with the Company's
            consent in respect of any claim, provided that the total amount
            payable in respect of damages shall not exceed the payable limits of
            indemnity.
          </div>
        </p>
      </div>
      <div className="medical_the_value">
        <h5>The Value</h5>
        <p>
          The program covers the amount of indemnities decided by virtue of an
          official verdict pronounced by the competent courts or legal
          authorities versus the Insured who causes medical harm to the patient
          arising out of an error or negligence or omissions during his
          practicing to his profession. This indemnity reaches up to a maximum
          limit ranging between SR 100,000 and SR 1000,000 per one occurrence or
          in the annual aggregate in accordance with the policy terms and
          conditions.
        </p>
      </div>
      <div className="medical_key_benefits">
        <h5>Key Benefits of of a the Medical Malpractice Policy by Tawuniya</h5>
        <p>Coverage for individuals and healthcare facilities</p>
        <div className="medical_benefits_card ">
          {datas.map((datas, index) => (
            <div key="id" className=" d-flex medical_benefits_content">
              <div className="tick_circle">
                <img src={datas.image} />
              </div>
              <p>{datas.description}</p>
            </div>
          ))}
        </div>
      </div>
      <h3>Related Documents</h3>
      <MobileSliderComp
        ref={
          <div className="d-flex  medical_person_slider ">
            {documents.map((document, index) => (
              <div key="id" className="medical_person_card">
                <img className="" src={document.image} />
                <h5>{document.title}</h5>
              </div>
            ))}
          </div>
        }
      />

      <div className="medical_malpractice_questions">
        <h5>You’ve got questions, we’ve got answers</h5>
        <p>
          Review answers to commonly asked questions at Tawuniya, which enable
          you to be directly involved in improving our support experience.
        </p>
        <div className="medical_questions">
          {/* <CommonFaq faqList={homecustomerServiceFaqList} /> */}
        </div>
      </div>
      <div className="d-flex medical_mal_view">
        <h5>View All quaestions</h5>
        <img src={medical_arrow} />
      </div>
      <div className="medical_claim">
        <MakeClaimCard />
      </div>
      <TawuniyaAppQuick />
      <SupportRequestHelper isContact={false} />
      <MyFamilyMobilePage />
    </div>
  );
};

export default MedicalMalpracticeMobile;
