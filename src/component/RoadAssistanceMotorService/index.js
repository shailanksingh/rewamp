import { NormalSelect } from "component/common/NormalSelect";
import { TitleBannerMobile } from "component/common/TitleBannerMobile";
import React ,{useEffect,useState} from "react";
import selectArrow from "assets/svg/selectArrowIconHome.svg";

import google_map from "assets/images/mobile/google_map.png";
import white_truck from "assets/images/mobile/white_truck.png";
import battery_black from "assets/images/mobile/battery_black.png";

import "./style.scss";
import { BottomButtonMobile } from "component/common/BottomButtonMobile";
import { history } from "service/helpers";

const RoadAssistanceMotorService = (state) => {

  const [title, setTitle] = useState("");

  useEffect(()=>{
    setTitle(sessionStorage.getItem("title"))
   
  })
  return (
    <div className="motor_service_container">
      <TitleBannerMobile title={title} subTitle="New Request" />
      <div className="motor_service_header">
        <NormalSelect
          className="bannerSelectInput"
          arrowVerticalAlign="bannerArrowAlign"
          placeholder="Select your vehicle"
          selectArrow={selectArrow}
          selectControlHeight="22px"
          selectFontWeight="400"
        />
      </div>
      <div className="google_map_body">
        <img className="map_img" src={google_map} alt="Map" />
      </div>
      <div className="select_service_card">
        {/* <h5>Select the Service</h5> */}
        {/* <div className="service_card_container">
          <div className={`service_card_body service_card_body_active ml-3`}>
            <img src={white_truck} alt="Icon" />
            <h6>Regular Tow</h6>
          </div>
          <div className={`service_card_body`}>
            <img src={battery_black} alt="Icon" />
            <h6>Battery</h6>
          </div>
          <div className="service_card_body">
            <img src={battery_black} alt="Icon" />
            <h6>Gas</h6>
          </div>
          <div className="service_card_body">
            <img src={battery_black} alt="Icon" />
            <h6>Regular Tow</h6>
          </div>
          <div className="service_card_body">
            <img src={battery_black} alt="Icon" />
            <h6>Battery</h6>
          </div>
        </div> */}
      

        <div className="card container-fluid report_button align_sticky pt-2">
        <div className="address_location">
          <h4>Selected Location</h4>
          <p>Alyasmin, Riyadh 13331, Riyadh 13325</p>
        </div>
        <button  onClick={() => history.push("/dashboard/request")}>Submit The Request</button>
        </div>
        {/* <BottomButtonMobile title="Submit The Request" />

        <BottomButtonMobile
          onClick={() => history.push("/road-assisstance/request")}
          title="Submit The Request"
        /> */}
      </div>
    </div>
  );
};

export default RoadAssistanceMotorService;
