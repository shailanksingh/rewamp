import React, { useState } from "react";
import HeaderStickyMenu from "component/common/MobileReuseable/HeaderStickyMenu";
import HeaderStepsSticky from "component/common/MobileReuseable/HeaderStepsSticky";
import "./style.scss";
import PolicyCard from "component/common/PolicyCard";
import {
  HomePolicyList,
  // homecustomerServiceFaqList,
} from "component/common/MockData";
import TawuniyaAppQuick from "component/common/MobileReuseable/TawuniyaAppQuick/Ithra";
import SupportRequestHelper from "component/common/MobileReuseable/SupportRequest";
import { CommonFaq } from "component/common/CommonFaq";
import Right_Arrow from "assets/about/Right_Arrow.svg";
import fire from "assets/about/fire.png";
import explosion from "assets/about/explosion.png";
import security from "assets/about/security.png";
import social from "assets/about/social.png";
import MakeClaimCard from "component/common/MobileReuseable/MakeClaimCard";
import home from 'assets/about/home.png'
import MyFamilyMobilePage from "component/MyFamilyMobilePage/Insurancecard";

const HomeInsurancePageMobile = () => {
  const [data, setData] = useState([
    {
      id: 1,
      image: fire,
      title: "Fires",
      content:
        "Fires are dreadful and can lead to plenty of damage and losses. Our home insurance policy will be there for you",
    },
    {
      id: 2,
      image: explosion,
      title: "Explosion",
      content:
        "Damages and losses caused by explosions or even aircraft damage will be covered for too in our home insurance",
    },
    {
      id: 3,
      image: explosion,
      title: "Storms",
      content:
        "Covers your home and your belongings from dire storms that could lead to potential damages and losses",
    },
    {
      id: 4,
      image: explosion,
      title: "Floods",
      content:
        "Protects your home from losses and damages when the rains are out of control, leading to floods that impact your home and your belongings",
    },
    {
      id: 5,
      image: explosion,
      title: "Burglaries",
      content:
        "Your home insurance will help protect both your home and its contents from any losses that occur due to the same",
    },
    {
      id: 6,
      image: explosion,
      title: "Earthquakes",
      content:
        "Nobody can avoid nature's furies but, what you can do is ensure you're covered for any potential losses that could come your way due to the same",
    },
  ]);
  const [content, setContent] = useState([
    {
      id: 1,
      image: security,
      title: "Secured through Uncertainty",
      content:
        "Home burglaries can happen anywhere, anytime- even in the most secure places. Home insurance helps you recover from losses that could occur due to such uncertain and unforeseen circumstances.",
    },
    {
      id: 2,
      image: social,
      title: "Complete Financial & Social Security",
      content:
        "Contrary to popular belief, home insurance goes beyond just the physical property of your home. It covers everything right from your garage to your home’s contents. There is no one to watch your home when you’re out during the day, at work, or traveling. That’s why having home insurance helps your home stay secure when you’re away.",
    },
    {
      id: 3,
      image: security,
      title: "Protection from Natural Calamities",
      content:
        "Floods, storms, and the like can be a homeowner's worst nightmare! And going through the hassle of rebuilding your home and recovering from the damages can not only be stressful but also a loss of a lot of money! Luckily, having home insurance can cover you for it all!",
    },
  ]);
  return (
    <div className="mobile_home_insurance_container">
      <HeaderStickyMenu />
      <HeaderStepsSticky title={"Home Insurance"} buttonTitle="Get A Quote" />
      <div className="mobile_home">
        <img src={home}/>
        <div className="mobile_mobile_insurance">
        <h6>Home Insurance</h6>
        <div className="mobile_saudi">
          The Saudi insurance pioneer, will help you to identify, analyze and
          manage such risks and suggest appropriate insurance solutions.
        </div>
        </div>
      </div>
      <div className="mobile_policy">Mange Your Policy</div>
      <div className="mobile_list">
        <PolicyCard policyList={HomePolicyList} />
      </div>
      <div className="mobile_cover">
        <p>What’s Covered in Home Insurance by Tawuniya?</p>
        <div className="mobile_limit">
          The maximum limit of insurance cover is determined on the basis of the
          house insured value and that of its contents based on reasonable
          estimates, in addition to the benefit of third party liability,
          liability to domestic employees, and the value of additional expenses
          to find alternative accommodation in the event of damage to the
          insured dwelling. The compensation is made on the basis of repair,
          replacement, or monetary compensation..
        </div>
      </div>
      <div className="mobile_home_box">
        {data.map((datas) => (
          <div className="mobile_home_white">
            <img src={datas.image} />
            <p>{datas.title}</p>
            <div className="mobile_home_content">{datas.content}</div>
          </div>
        ))}
      </div>
      <div className="mobile_advantage">
        <p>
          What are the advantages of protecting your home with a Home Insurance?
        </p>
        <div className="mobile_pioneer">
          The Saudi insurance pioneer, will help you to identify, analyze and
          manage such risks and suggest appropriate insurance solutions.
        </div>
      </div>
      <div className="mobile_home_advantage">
        {content.map((datas) => (
          <div className="mobile_home_white_advantage">
            <img src={datas.image} />
            <p>{datas.title}</p>
            <div className="mobile_home_content_advantage">{datas.content}</div>
          </div>
        ))}
      </div>
      <TawuniyaAppQuick />
      <SupportRequestHelper isContact={false} />
      <div className="mobile_ques">
        <p>You’ve got questions, we’ve got answers</p>
        <div className="mobile_ans">
          Review answers to commonly asked questions at Tawuniya, which enable
          you to be directly involved in improving our support experience.
        </div>
      </div>
      <div className="mobile_faq">
        {/* <CommonFaq faqList={homecustomerServiceFaqList} /> */}
      </div>
      <div className="mobile_view">
        <p>View All quaestions</p>
        <img src={Right_Arrow} />
      </div>
      <div className="mobile_claim">
        <MakeClaimCard />
      </div>
      <MyFamilyMobilePage />
    </div>
  );
};

export default HomeInsurancePageMobile;
