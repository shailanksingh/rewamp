import React, { useState } from "react";

import { Card } from "react-bootstrap";
import { Insurance } from "../Homeinsurancepage/InsuranceComponent/Insurance";
import { TawuniyaAppSlider } from "component/common/TawuniyaAppSlider";
import { InsuranceServicesData } from "../Homeinsurancepage/InsuranceComponent/schema/InsuranceServiceData";
import { Faq } from "../Homeinsurancepage/InsuranceComponent/schema/Faq";
import { InsuranceProductsData } from "../Homeinsurancepage/InsuranceComponent/schema/InsureanceProductData";

import { HomeServices } from "component/HomePage/LandingComponent";
import FrequentlyAsked from "component/common/FrequentlyAsked";
import { InsuranceBlogs } from "../Homeinsurancepage/InsuranceComponent/InsuranceBlogs";
import { FooterAbout } from "component/HomePage/LandingComponent";
import { InsuranceProducts } from "../Homeinsurancepage/InsuranceComponent/InsuranceProduct";

import Tag5 from "assets/svg/Homeinsurance/tag5.svg";
import Tag6 from "assets/svg/Homeinsurance/tag6.svg";
import Tag7 from "assets/svg/Homeinsurance/tag7.svg";
import Tag8 from "assets/svg/Homeinsurance/tag8.svg";
import Tag9 from "assets/svg/Homeinsurance/tag9.svg";
import Tag10 from "assets/svg/Homeinsurance/tag10.svg";
import Tag11 from "assets/svg/Homeinsurance/tag11.svg";

import "./style.scss";

export const Homeinsurancepage = () => {
  const insuranceCardData = [
    {
      id: 1,
      content: "Fire, Lightning, Explosion, Earthquake",
      image: Tag5,
    },
    {
      id: 2,
      content: "Storm and Flood",
      image: Tag6,
    },
    {
      id: 3,
      content: "Bursting or Overflowing of Water Tanks, Apparatus or Pipes",
      image: Tag7,
    },
    {
      id: 4,
      content:
        "Impact or Collision Involving any Animal, Land Vehicle, Aircraft ",
      image: Tag8,
    },
    {
      id: 5,
      content: "Riot, Strike, Labour Disturbances and Civil Commotion ",
      image: Tag9,
    },
  ];

  return (
    <React.Fragment>
      <Insurance />

      {/* section 2 start */}

      <div className="Maincontainerinsurance">
        <div className="row pt-3">
          <div className="col-lg-3">
            <h1 className="fw-800 fs-40">Check out all the perks you get </h1>
          </div>
          <div className="col-lg-12">
            <p className="insuranceptag">
              The policy covers, according to the client choices, either the
              buildings or contents, or both of them; and the insured perils
              include:
            </p>
          </div>
        </div>

        <div className="row pt-3">
          {insuranceCardData.map((item, index) => {
            return (
              <div className="container col-lg-2">
                <Card className="insurancecard">
                  <div className="container pt-3">
                    <img className="img-fluid imgsize" src={item.image} />
                    <p className="pt-5 fw-700 cardptag">{item.content}</p>
                  </div>
                </Card>
              </div>
            );
          })}
        </div>
        {/* section 2 end---------------- */}

        {/* section-3 start */}
        <br />
        <div className="pt-5">
          <h1 className="fw-800 fs-35">Value of the Program Cover</h1>
          <p className="insuranceptag">
            We provide the best and trusted service for our customers
          </p>

          <div className="row">
            <div className="col-lg-6">
              <Card className="insurancecard">
                <div className="container">
                  <img className="pt-4" src={Tag10} alt="" />

                  <p className="fw-800 fs-25 pt-4">Basic Coverages</p>
                  <p className="insuranceptag">
                    It covers losses resulting from fire, explosion, theft or
                    any incidents leading to the deterioration or damage to the
                    house according to the terms, conditions and exclusions set
                    forth in the insurance policy
                  </p>
                </div>
              </Card>
            </div>

            <div className="col-lg-6">
              <Card className="insurancecard">
                <div className="container">
                  <img className="pt-4" src={Tag11} alt="" />
                  <p className="fw-800 fs-25 pt-4">
                    Maximum Limit of Insurance Cover
                  </p>
                  <p className="insuranceptag">
                    Basis of the house insured value and that of its contents
                    based on reasonable estimates, in addition to the benefit of
                    third party liability, liability to domestic employees and
                    the value of additional expenses to find alternative
                    accommodation in the event of damage to the insured
                    dwelling.
                  </p>
                </div>
              </Card>
            </div>
          </div>
        </div>

        {/* section 3 end */}

        <div className="pt-5 alignslider">
          <TawuniyaAppSlider />
        </div>

        <div className="pt-5 ">
          <HomeServices HomeServicesData={InsuranceServicesData} />
        </div>

        <div className="pt-5 ">
          <FrequentlyAsked FAQ={Faq} />
        </div>

        <div className="pt-5 ">
          <InsuranceBlogs />
        </div>

        <InsuranceProducts motorOtherProductsData={InsuranceProductsData} />

        <div className="pt-5 ">
          <FooterAbout />
        </div>
      </div>
    </React.Fragment>
  );
};
