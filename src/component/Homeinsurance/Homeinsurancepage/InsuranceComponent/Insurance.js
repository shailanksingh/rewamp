import React, { useState } from "react";
import { NormalInput } from "component/common/NormalInput";
import { NormalRadioButton } from "component/common/NormalRadioButton";
import { NormalButton } from "component/common/NormalButton";
import {Button,Card} from "react-bootstrap"


import Person from "assets/svg/Homeinsurance/person.svg"
import Group from "assets/svg/Homeinsurance/group.svg"
import Tag1 from "assets/svg/Homeinsurance/tag1.svg"
import Tag2 from "assets/svg/Homeinsurance/tag2.svg"
import Tag3 from "assets/svg/Homeinsurance/tag3.svg"
import Tag4 from "assets/svg/Homeinsurance/tag4.svg"
import insuranceArrow from "assets/svg/insuranceArrow.svg";





import "./style.scss";

export const Insurance = () => {
  const insuranceCardData = [
    {
      id: 0,
      content: "Submit / Track a Claim",
      image:Tag1
    },
    {
      id: 1,
      content: "Report a Theft",
      image:Tag2
    },
    {
      id: 2,
      content: "Download Policy",
      image:Tag3
    },
    {
      id: 3,
      content: "Renew Policy",
      image:Tag4
    },
  ];

  //initialize state for input field
  const [valueOne, setValueOne] = useState("");

  //initialize state for input field
  const [valueTwo, setValueTwo] = useState("");

  //initialiize state for radio buttons
  const [radioOne, setRadioOne] = useState("Yes");

  return (
    // motor insurance banner starts here
    <div className="row motorInsuranceContainer pb-5">
      <div className="col-lg-7 col-12 pt-5">
        <p className="motorTitle fs-20 fw-800 m-0 pt-5">Home Insurance</p>
        <p className="motorPara fs-22 fw-400 pb-3">
        It covers losses resulting from fire, explosion, theft or any incidents leading to the deterioration or damage to the house according to the terms, conditions and exclusions set forth in the insurance policy.{" "}
        </p>
        <p className="fw-800 fs-35">Manage your Policy</p>
        <p className="fs-18">We provide the best and trusted service for our customers</p>
        <div className="row">
       
          {insuranceCardData.map((item, index) => {
            return (
              <div className="col-lg-3 col-md-3 col-12" key={index}>
                  <Card className="insurancecard pb-3">
                    <div className="container">
                        <div className="row pt-3">
                        <div className="col-lg-12">
                        <img src={item.image} alt=""/>
                        </div>
                        <div className="col-lg-12 pt-3">
                        {item.content}
                       
                        </div>

                        <div className="col-lg-12 pt-4">
                        <img src={insuranceArrow} alt=""/>
                        </div>
                        
                        

                       
                       
                       
                      
                        </div>
                   
                    </div>
                

                  </Card>
               
        
              </div>
            );
          })}
        </div>
      </div>
      <div className="col-lg-5 col-md-12 col-12 pt-5">
        <div className="insureMotorCard h-100">
          <div className="insureLayerOne">
            <p className="insureCardTitle fw-800 m-0">
              Insure Your Home Now!
            </p>
          </div>
          <div className="insureLayerTwo">
            <div className="subInsureLayer p-4">
              <div className="row">
                <div className="col-6">
                <Button size="lg" className="insureNowBtn" ><img className=" btn-img pr-4 " src={Person} alt=".."/><span className="fs-15 mr-5">Individual</span></Button>
                </div>
                <div className="col-6">
                <Button size="lg"  className="insureNowBtn2" ><img className=" btn-img pr-4 " src={Group} alt=".."/><span className="fs-15">Corporate & SMEs</span></Button>


                </div>
                
                <div className="col-12 pt-4">
                  <NormalInput
                    textField="motorInputfieldOne"
                    variant="outlined"
                    value={valueOne}
                    placeholder="Natioal ID, Iqama"
                    onChange={(e) => setValueOne(e.target.value)}
                  />
                </div>
                <div className="col-12 pt-4">
                  <NormalInput
                    variant="outlined"
                    textField="motorInputfieldOne"
                    value={valueTwo}
                    placeholder="+966 ex: 5xxxxxxxx"
                    onChange={(e) => setValueTwo(e.target.value)}
                  />
                </div>
              </div>
              <p className="fs-16 fw-400 pt-3">
                Do you want to insure vehicles for your company?
              </p>
              <div className="d-flex flex-row">
                <div>
                  <NormalRadioButton
                    type="radio"
                    name="radioOne"
                    value="Yes"
                    onChange={(e) => setRadioOne(e.target.value)}
                    radioValue="Yes"
                    checked={radioOne === "Yes"}
                  />
                </div>
                <div className="pl-3">
                  <NormalRadioButton
                    type="radio"
                    name="radioOne"
                    value="No"
                    onChange={(e) => setRadioOne(e.target.value)}
                    radioValue="No"
                    checked={radioOne === "No"}
                  />
                </div>
              </div>
            </div>
            <div className="px-5">
              <p className="fs-16 fw-400 insureTerms pb-3">
                By continuing you give Tawuniya advance consent to obtain my
                and/or my dependents' information from the National Information
                Center.
              </p>
              <div className="pb-5 mb-4">
                <NormalButton label="Buy Now" className="insureNowBtn" />
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    // motor insurance banner ends here
  );
};
