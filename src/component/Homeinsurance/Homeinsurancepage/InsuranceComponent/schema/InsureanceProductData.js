import ovalBackground from "assets/svg/oval-background.svg";
import coronavirus from "assets/svg/coronavirus.svg";
import passport from "assets/svg/passport.svg";
import family from "assets/svg/family.svg";

export const InsuranceProductsData = [
  {
    id: "1",
    BackgroundIcon: `${ovalBackground}`,
    icon: `${coronavirus}`,
    title: "Covid-19 Insurance",
    subtitle:
      "Covid-19 Travel insurance - for Saudis Program was designed to provide protection for Saudi citizens and supports.",
    topic: "Benefits:",
    topicDisc1: "Provides you with the necessary health care ",
    topicDisc2: "Medical expenses incurred around the world.",
  },
  {
    id: "2",
    BackgroundIcon: `${ovalBackground}`,
    icon: `${passport}`,
    title: "International Travel Insurance",
    subtitle:
      "The company has identified the amount of compensation so that it starts from SR 920 and up to SR 3,000,000 ",
    topic: "Benefits:",
    topicDisc1: "Medical Emergency Expenses",
    topicDisc2: "Free coverage for children (Under 2 Years)",
  },
  {
    id: "3",
    BackgroundIcon: `${ovalBackground}`,
    icon: `${family}`,
    title: "My Family Health Insurance",
    subtitle:
      "The program covers most of the medical services provided in the outpatient as well as inpatient services",
    topic: "Benefits:",
    topicDisc1: "Upto 60 Days Claim Periods",
    topicDisc2: "Waive Depreciation",
  },
];
