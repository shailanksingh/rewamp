import serviceIcons1 from "assets/svg/TawuniyaServicesIcons/serviceIcons1.svg";
import serviceIcons2 from "assets/svg/TawuniyaServicesIcons/serviceIcons2.svg";
import serviceIcons3 from "assets/svg/TawuniyaServicesIcons/serviceIcons3.svg";
import serviceIcons4 from "assets/svg/TawuniyaServicesIcons/serviceIcons4.svg";
import serviceIcons5 from "assets/svg/TawuniyaServicesIcons/serviceIcons5.svg";
import serviceIcons6 from "assets/svg/TawuniyaServicesIcons/serviceIcons6.svg";
import chat from "assets/svg/chat.svg";
import locate from "assets/svg/locate.svg";
import group from "assets/svg/Group.svg";
import track from "assets/svg/track.svg";

export const InsuranceServicesData = [
	{
		source: "motorservices",
		serviceTitle: "We are always here for you!",
		serviceSubtitle: "We provide the best and trusted service for our customers",
		serviceCardData: [
			{
				headingEl: "protection Principles",
				discrptionEl: "Our services can be trustable, honest and worthy",
				iconE1: `${serviceIcons1}`,
			},
			{
				headingEl: "Assist in Accident",
				discrptionEl: "Our services can be trustable, honest and worthy",
				iconE1: `${serviceIcons5}`,
			},
			{
				headingEl: "Najm Status",
				discrptionEl: "Our services can be trustable, honest.",
				iconE1: `${serviceIcons2}`,
			},
			{
				headingEl: "Chat",
				discrptionEl: "Our services can be trustable, honest and worthy",
				iconE1: `${chat}`,
			},
			{
				headingEl: "Complaints",
				discrptionEl: "Our services can be trustable, honest and worthy",
				iconE1: `${group}`,
			},
			{
				headingEl: "Submit/Track Claim",
				discrptionEl: "Our services can be trustable, honest and worthy",
				iconE1: `${track}`,
			},
			{
				headingEl: "Locate Us",
				discrptionEl: "Our services can be trustable, honest and worthy",
				iconE1: `${locate}`,
			},
			{
				headingEl: "Road Side Assistance",
				discrptionEl: "Our services can be trustable, honest and worthy",
				iconE1: `${serviceIcons6}`,
			},
			{
				headingEl: "Request Telemedicine",
				discrptionEl: "Our services can be trustable, honest. ",
				iconE1: `${serviceIcons2}`,
			},
		],
	},
];
