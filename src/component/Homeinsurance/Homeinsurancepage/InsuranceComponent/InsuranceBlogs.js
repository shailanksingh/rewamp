import React from "react";
import { Button } from "@material-ui/core";

import "../style.scss"

export const InsuranceBlogs = () => {
	return (
		<div className="MotorBlogs">
			<div className="MotorBlogsHead">
				<div className="MotorBlogsText">
					<h5>Blogs Related to Home Insurance</h5>
					<span>We provide the best and trusted service for our customers</span>
				</div>
				<div className="MotorBlogsNavigation">
					{/* <div>{"<"}</div>
					<div>{">"}</div> */}
				</div>
			</div>
			<div className="MotorBlogsImgContainer">
				<div className="MotorBlogsImga">
					<div className="MotorBlogsImgText">
						<h5>Insurance News: Cultivating a Sustainable future</h5>
						<span>
							Get quality car insurance at an affordable price. with Tawuniya,
							find a car insurance that suits your needs.
						</span>
						<div className="MotorBlogsBtn">
							<Button size="medium" color="secondary" variant="outlined">
								Read More
							</Button>
						</div>
					</div>
				</div>
				<div className="MotorBlogsImgb">
					<div className="MotorBlogsImgText">
						<h5>Insuring your car became easier from your place</h5>
						<span>
							Get quality car insurance at an affordable price. with Tawuniya,
							find a car insurance that suits your needs.
						</span>
						<div className="MotorBlogsBtn">
							<Button size="medium" color="secondary" variant="outlined">
								Read More
							</Button>
						</div>
					</div>
				</div>
			</div>
		</div>
	);
};
