import React from "react";
import { OtherProductsCard } from "component/common/OtherProductsCard";
import "./style.scss";
export const InsuranceProducts = ({ motorOtherProductsData }) => {
  return (
    <div className="MotrProContainer">
      <div className="MotrProText">
        <h5>You may also like our other products</h5>
        <span>We provide the best and trusted service for our customers</span>
      </div>
      <div className="MotrProCardsContainer">
        {motorOtherProductsData.map((item) => {
          return (
            <OtherProductsCard
              BackgroundIcon={item.BackgroundIcon}
              icon={item.icon}
              title={item.title}
              subtitle={item.subtitle}
              topic={item.topic}
              topicDisc1={item.topicDisc1}
              topicDisc2={item.topicDisc2}
            />
          );
        })}
      </div>
    </div>
  );
};
