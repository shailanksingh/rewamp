import React, { useState } from "react";
import { InsuranceCard } from "component/common/InsuranceCard";
import { NormalSearch } from "component/common/NormalSearch";
import { NormalRadioButton } from "component/common/NormalRadioButton";
import { NormalButton } from "component/common/NormalButton";
import { RewardCard } from "component/common/RewardCard";
import policyCar from "assets/svg/policyCar.svg";
import coverage from "assets/svg/coverage.svg";
import renew from "assets/svg/renew.svg";
import claim from "assets/svg/claim.svg";
import accident from "assets/svg/towcar.svg";
import agent from "assets/svg/carAgent.svg";
import mvpi from "assets/svg/mvpi.svg";
import single from "assets/svg/single.svg";
import group from "assets/svg/Groupsme.svg";
import PhoneNumberInput from "component/common/PhoneNumberInput/index";
import KSAFlagImage from "../../../assets/images/ksaFlag.png";
import IndiaFlagImage from "../../../assets/images/indiaFlag.png";
import iquamaIcon from "assets/svg/iquamaIcon.svg";
import buynowArrow from "assets/svg/buynowArrow.svg";
import addManage from "assets/svg/addManage.svg";
import bagageVector1 from "assets/svg/bagageVector1.svg";
import submitVector from "assets/svg/submitVector.svg";
import coversVector from "assets/svg/coversVector.svg";
import planeVector from "assets/svg/planeVector.svg";
import "./style.scss";
import "./InsuranceComponent-MediaQuery.css";

export const Insurance = () => {
  const insuranceMotorCardData = [
    {
      id: 0,
      content: "Submit / Track a Claim",
      cardIcon: submitVector,
      class: "pr-0",
    },
    {
      id: 1,
      content: "Flight delay claim",
      cardIcon: planeVector,
      class: "pr-0",
    },
    {
      id: 2,
      content: "Bagage delay claim",
      cardIcon: bagageVector1,
      class: "pr-0",
    },
    {
      id: 3,
      content: "Covers health care to recover",
      cardIcon: coversVector,
      class: "pr-0",
    },
  ];

  let dialingCodes = [
    {
      code: "+91",
      image: IndiaFlagImage,
    },
    {
      code: "+966",
      image: KSAFlagImage,
    },
  ];

  //initialize state for input field
  const [valueOne, setValueOne] = useState("");

  //initialize state for input field
  const [inpOne, setInpOne] = useState({ userId: "", phone: "" });

  //initialiize state for radio buttons
  const [radioOne, setRadioOne] = useState("Yes");

  let [selectedCode, setSelectedCode] = useState(dialingCodes[0]);

  let [phoneNumber, setPhoneNumber] = useState("");

  const handleInputChange = (e) => {
    const { name, value } = e.target || e || {};
    setInpOne({ ...inpOne, [name]: value });
  };

  return (
    // motor insurance banner starts here
    <div className="row motorInsuranceContainer pb-5">
      <div className="col-lg-7 col-12 pt-5">
        <div className="row">
          <div className="col-11">
            <p className="motorTitle fw-800 m-0 pt-5">
              Travel Insurance Program
            </p>
            <p className="motorPara fs-22 fw-400 pb-3">
              Travel insurance protects you from any unexpected losseswhile
              you’re travelling internationally or domestically.
            </p>
            <div>
              <p className="mangerPolicyHeading ">Manage your Policy</p>
              <p className="mangerPolicyHeading1">
                We provide the best and trusted service for our customers
              </p>
            </div>

            <InsuranceCard
              heathInsureCardData={insuranceMotorCardData}
              textWidth="insureCardTextWidth"
            />
          </div>
        </div>
      </div>
      <div className="col-lg-5 col-md-12 col-12 pt-5">
        <div className="insureMotorCard h-100">
          <div className="insureLayerOne">
            <p className="insureCardTitle fw-800 m-0">
              Insure Your Travel Now!
            </p>
          </div>
          <div className="insureLayerTwo">
            <div className="row insureSubMotorLayerTwo px-4 mx-1 pt-3">
              <div className="col-lg-6 col-12">
                <NormalButton
                  alignBtn={true}
                  label="Individual"
                  className="individualMotorBtn"
                  needNewBtnIcon={true}
                  newSrc={single}
                  adjustNewIcon="img-fluid pr-3"
                />
              </div>
              <div className="col-lg-6 col-12 pl-0">
                <NormalButton
                  label="Corporate & SMEs"
                  className="smeBtn"
                  needNewBtnIcon={true}
                  newSrc={group}
                  adjustNewIcon="img-fluid pr-3"
                />
              </div>
            </div>

            <div className="subInsureLayer p-4">
              <div className="row">
                <div className="col-12">
                  <NormalSearch
                    className="motorInputfieldOne"
                    name="userId"
                    value={inpOne.userId}
                    placeholder="Passport Number"
                    onChange={handleInputChange}
                    needLeftIcon={true}
                    leftIcon={iquamaIcon}
                  />
                </div>
                <div className="col-12 pt-4">
                  <PhoneNumberInput
                    className="motorPhoneInput"
                    selectInputClass="motorSelectInputWidth"
                    selectInputFlexType="motorFlexType"
                    dialingCodes={dialingCodes}
                    selectedCode={selectedCode}
                    setSelectedCode={setSelectedCode}
                    value={phoneNumber}
                    name="phoneNumber"
                    onChange={({ target: { value } }) => setPhoneNumber(value)}
                  />
                </div>
              </div>
              {/* <p className="fs-16 fw-400 pt-3">
                Do you want to insure vehicles for your company?
              </p> */}
              {/* <div className="d-flex flex-row">
                <div>
                  <NormalRadioButton
                    type="radio"
                    name="radioOne"
                    value="Yes"
                    onChange={(e) => setRadioOne(e.target.value)}
                    radioValue="Yes"
                    checked={radioOne === "Yes"}
                  />
                </div>
                <div className="pl-3">
                  <NormalRadioButton
                    type="radio"
                    name="radioOne"
                    value="No"
                    onChange={(e) => setRadioOne(e.target.value)}
                    radioValue="No"
                    checked={radioOne === "No"}
                  />
                </div>
              </div> */}
            </div>
            <div className="px-5">
              <p className="fs-16 fw-400 insureTerms pb-3">
                By continuing you give Tawuniya advance consent to obtain my
                and/or my dependents' information from the National Information
                Center.
              </p>
              <div className="pb-4 mb-3">
                <NormalButton
                  label="Buy Now"
                  className="insureNowBtn p-4"
                  adjustIcon="pl-3"
                  needBtnPic={true}
                  src={buynowArrow}
                />
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    // motor insurance banner ends here
  );
};
