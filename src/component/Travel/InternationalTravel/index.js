import React from "react";
import {
	AppBanner,
	BreakDownTable,
} from "../../MotorPage/MotorInsuranceComponent";
import { Insurance } from "../TravelComPonenet/InsuranceComponent";

// import { TableData } from "../MotorInsuranceComponent/MotorTableData";
import { TawuniyaAppSlider } from "component/common/TawuniyaAppSlider";
import { HomeServices } from "component/HomePage/LandingComponent";
import {
	headerBreakDownData,
	tableBreakDownData,
} from "component/common/MockData";
import "./style.scss";
import { MotorServicesData } from "../../MotorPage/MotorInsuranceComponent/schema/MotorServicesData";
import FrequentlyAsked from "component/common/FrequentlyAsked";
import { FAQTravel } from "../TravelSchema/FaqTravelData";
import { MotorBlogs } from "../../MotorPage/MotorInsuranceComponent/MotorBlogs";
import { MotorOtherProducts } from "../../MotorPage/MotorInsuranceComponent";
import { motorOtherProductsData } from "../../MotorPage/MotorInsuranceComponent/schema/MotorOtherProductsData";
import { FooterAbout } from "component/HomePage/LandingComponent";
import { CommonTable } from "component/common/CommonTable";
import carDamage from "assets/svg/damageCar.svg";
import xCircle from "assets/svg/xCircle.svg";
import annual1 from "../../../assets/svg/annual1.svg";
import Expenses from "../../../assets/svg/Expenses.svg";
import Cancellation from "../../../assets/svg/Cancellation.svg";
import Curtailment from "../../../assets/svg/Curtailment.svg";
import MDeparture from "../../../assets/svg/MDeparture.svg";
import MIBenefit from "../../../assets/svg/MIBenefit.svg";
import PProperty from "../../../assets/svg/PProperty.svg";
import DBaggage from "../../../assets/svg/DBaggage.svg";
import PPLiability from "../../../assets/svg/PPLiability.svg";

import {
	FraudBanner,
	FraudAssociates,
	Faq,
	LegendarySupport,
} from "component/common/FraudDetails";
import { Card, Row, Col } from "react-bootstrap";

export function InternationalTravel() {
  const cardData = [
    {
      id: "1",
      cardIcon: `${annual1}`,
      cardTitle: "SR 100,000",
      cardText: "Personal Accident",
    },
    {
      id: "2",
      cardIcon: `${Expenses}`,
      cardTitle: "SR 3,000,000",
      cardText: "Medical Emergency Expenses",
    },
    {
      id: "3",
      cardIcon: `${Cancellation}`,
      cardTitle: "SR 15,000",
      cardText: "Cancellation",
    },
    {
      id: "4",
      cardIcon: `${Curtailment}`,
      cardTitle: "SR 15,000",
      cardText: "Curtailment",
    },
    {
      id: "5",
      cardIcon: `${MDeparture}`,
      cardTitle: "SR 1,900",
      cardText: "Missed Departure",
    },
    {
      id: "6",
      cardIcon: `${MIBenefit}`,
      cardTitle: "SR 1,900",
      cardText: "Medical Inconvenience Benefit ",
    },
    {
      id: "7",
      cardIcon: `${PProperty}`,
      cardTitle: "SR 8,000  ",
      cardTitleText:"(Subject to sub-limit)",
      cardText: "Personal Accident",
    },
    {
      id: "8",
      cardIcon: `${DBaggage}`,
      cardTitle: "SR 920",
      cardText: "Delayed baggage",
    },
    {
      id: "9",
      cardIcon: `${PPLiability}`,
      cardTitle: "SR 2,000,000",
      cardText: "Personal public liability",
    },
  ];
  return (


      <div className="MotorInsuranceMainContainer">
        <AppBanner />

        <Insurance />
        <div>
          <div className="limitClaim">
            <div className="limitHeading">
             Maximum limit per claim in SR
            </div>
            <div>
              <div className="cardGrid">
                <Row xs={1} md={5} className="g-2">
                  {cardData.map((item) => (
                    <Col>
                      <Card
                        style={{
                          width: "13rem",
                          height: "10rem",
                          margin: "10px",
                          border:'none',
                          borderRadius:'8px'
                        }}
                      >
                        <Card.Img
                          className="cardIcon"
                          variant="top"
                          src={item.cardIcon}
                        />
                        <Card.Body>
                             <h2 className="cardTitle">{item.cardTitle}</h2>
                          <h6>{item.cardTitleText}</h6>
                          <Card.Text className="cardText">{item.cardText}</Card.Text>
                        </Card.Body>
                      </Card>
                    </Col>
                  ))}
                </Row>
              </div>
            </div>
          </div>
        </div>
        <div className="row" />
          <div className="col-12" />
            <p className="breakdownHeading fw-800 m-0">
              Check Out All The Perks You Get </p>
        {/* <Insurance /> */}

        <div className="row">
          <div className="col-12">

            <p className="breakingPara fs-18 fw-400">
              We provide the best and trusted service for our customers
            </p>
            <CommonTable
              headerData={headerBreakDownData}
              tableData={tableBreakDownData}
            />
          </div>
        </div>
        <TawuniyaAppSlider />
        <HomeServices HomeServicesData={MotorServicesData} />
        <FrequentlyAsked FAQ={FAQTravel} />
        <MotorBlogs />
        <MotorOtherProducts motorOtherProductsData={motorOtherProductsData} />
        <FooterAbout />
      </div>

  );
  }
